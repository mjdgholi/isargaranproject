﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <narges.kamran>
    /// Create date: <1393/08/20>
    /// Description: <ارتباطی وب با دسته بندی فرم>
    /// </summary>
    public class WebForm_WebFormCategorizeEntity
    {
           #region Properties :

            public Guid WebForm_WebFormCategorizeId { get; set; }
            public Guid WebFormCategorizeId { get; set; }

            public Guid WebFormId { get; set; }

            public int FormPriority { get; set; }

            public string ImagePath { get; set; }

            public string CreationDate { get; set; }

            public string ModificationDate { get; set; }

            public bool IsActive { get; set; }

            public string WebFormIdStr { get; set; }
        



            #endregion

            #region Constrauctors :

            public WebForm_WebFormCategorizeEntity()
            {
            }

            public WebForm_WebFormCategorizeEntity(Guid _WebForm_WebFormCategorizeId, Guid WebFormCategorizeId, Guid WebFormId, int FormPriority, string ImagePath, string CreationDate, string ModificationDate, bool IsActive)
            {
                WebForm_WebFormCategorizeId = _WebForm_WebFormCategorizeId;
                this.WebFormCategorizeId = WebFormCategorizeId;
                this.WebFormId = WebFormId;
                this.FormPriority = FormPriority;
                this.ImagePath = ImagePath;
                this.CreationDate = CreationDate;
                this.ModificationDate = ModificationDate;
                this.IsActive = IsActive;
                

            }

            #endregion
    }
}