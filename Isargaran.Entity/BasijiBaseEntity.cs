﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <پایگاه بسیج>
    /// </summary>
   public class BasijiBaseEntity
    {
        
        #region Properties :

        public Guid BasijiBaseId { get; set; }
        public string BasijiBaseTitle { get; set; }

        public Guid DistrictBasijiId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public BasijiBaseEntity()
        {
        }

        public BasijiBaseEntity(Guid _BasijiBaseId, string BasijiBaseTitle, Guid DistrictBasijiId, string CreationDate, string ModificationDate, bool IsActive)
        {
            BasijiBaseId = _BasijiBaseId;
            this.BasijiBaseTitle = BasijiBaseTitle;
            this.DistrictBasijiId = DistrictBasijiId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
