﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع اقدام>
    /// </summary>
    public class ActionTypeEntity
    {
        #region Properties :

            public Guid ActionTypeId { get; set; }
            public string ActionTypeTitle { get; set; }

            public string CreationDate { get; set; }

            public string ModificationDate { get; set; }

            public bool IsActive { get; set; }



            #endregion

            #region Constrauctors :

            public ActionTypeEntity()
            {
            }

            public ActionTypeEntity(Guid _ActionTypeId, string ActionTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
            {
                ActionTypeId = _ActionTypeId;
                this.ActionTypeTitle = ActionTypeTitle;
                this.CreationDate = CreationDate;
                this.ModificationDate = ModificationDate;
                this.IsActive = IsActive;

            }

            #endregion
 
    }
}