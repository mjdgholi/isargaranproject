﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/20>
    /// Description: <پرونده اطلاعات اردو یا مسافرت ایثارگر>
    /// </summary>
  public  class DossierSacrifice_TripLocationEntity
    {
                #region Properties :

        public Guid DossierSacrifice_TravelLocationId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid TripLocationId { get; set; }

        public string TripStartDate { get; set; }

        public string TripEndDate { get; set; }

        public int? TravelerNo { get; set; }

        public decimal? FinancialCost { get; set; }

        public Guid? VehicleType { get; set; }

        public string HotelName { get; set; }

        public Guid? OccasionId { get; set; }

        public string AlongPersonName { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_TripLocationEntity()
        {
        }

        public DossierSacrifice_TripLocationEntity(Guid _DossierSacrifice_TravelLocationId, Guid DossierSacrificeId, Guid TripLocationId, string TripStartDate, string TripEndDate, int? TravelerNo, decimal? FinancialCost, Guid? VehicleType, string HotelName, Guid? OccasionId, string AlongPersonName, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_TravelLocationId = _DossierSacrifice_TravelLocationId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.TripLocationId = TripLocationId;
            this.TripStartDate = TripStartDate;
            this.TripEndDate = TripEndDate;
            this.TravelerNo = TravelerNo;
            this.FinancialCost = FinancialCost;
            this.VehicleType = VehicleType;
            this.HotelName = HotelName;
            this.OccasionId = OccasionId;
            this.AlongPersonName = AlongPersonName;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
