﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: < مهارت نوشتن>
    /// </summary>
    public class WritingSkillEntity
    {
        #region Properties :

        public Guid WritingSkillId { get; set; }
        public string WritingSkillPersianTitle { get; set; }

        public string WritingSkillEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public WritingSkillEntity()
        {
        }

        public WritingSkillEntity(Guid _WritingSkillId, string WritingSkillPersianTitle, string WritingSkillEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            WritingSkillId = _WritingSkillId;
            this.WritingSkillPersianTitle = WritingSkillPersianTitle;
            this.WritingSkillEnglishTitle = WritingSkillEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}