﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <نوع پذیرش>
    /// </summary>
   public class AdmissionTypeEntity
    {
                #region Properties :

        public Guid AdmissionTypeId { get; set; }
        public string AdmissionTypeTitle { get; set; }

        public bool HasTuition { get; set; }

        public bool IsDaily { get; set; }

        public bool IsE_Lerning { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public AdmissionTypeEntity()
        {
        }

        public AdmissionTypeEntity(Guid _AdmissionTypeId, string AdmissionTypeTitle, bool HasTuition, bool IsDaily, bool IsE_Lerning, string CreationDate, string ModificationDate, bool IsActive)
        {
            AdmissionTypeId = _AdmissionTypeId;
            this.AdmissionTypeTitle = AdmissionTypeTitle;
            this.HasTuition = HasTuition;
            this.IsDaily = IsDaily;
            this.IsE_Lerning = IsE_Lerning;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
