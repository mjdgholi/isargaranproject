﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/18>
    /// Description: <پرونده مشخصات تسهیلات و خدمات پزشکی>
    /// </summary>  
  public  class DossierSacrifice_MedicalFacilitieAndServiceEntity
    {
          #region Properties :

        public Guid DossierSacrifice_MedicalFacilitieAndServiceId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid MedicalFacilitieAndServiceId { get; set; }

        public string MedicalFacilitieAndServiceDate { get; set; }

        public decimal? FinancialCost { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_MedicalFacilitieAndServiceEntity()
        {
        }

        public DossierSacrifice_MedicalFacilitieAndServiceEntity(Guid _DossierSacrifice_MedicalFacilitieAndServiceId, Guid DossierSacrificeId, Guid MedicalFacilitieAndServiceId, string MedicalFacilitieAndServiceDate, decimal? FinancialCost, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_MedicalFacilitieAndServiceId = _DossierSacrifice_MedicalFacilitieAndServiceId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.MedicalFacilitieAndServiceId = MedicalFacilitieAndServiceId;
            this.MedicalFacilitieAndServiceDate = MedicalFacilitieAndServiceDate;
            this.FinancialCost = FinancialCost;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

    }
}
