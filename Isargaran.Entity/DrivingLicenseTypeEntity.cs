﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{

    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	< نوع گواهینامه رانندگی(پایه)>
    /// </summary>
  public  class DrivingLicenseTypeEntity
    {
      
        #region Properties :

        public Guid DrivingLicenseTypeId { get; set; }
        public string DrivingLicenseTypeTitlePersian { get; set; }

        public string DrivingLicenseTypeTitleEnglish { get; set; }

        public int Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DrivingLicenseTypeEntity()
        {
        }

        public DrivingLicenseTypeEntity(Guid _DrivingLicenseTypeId, string DrivingLicenseTypeTitlePersian, string DrivingLicenseTypeTitleEnglish, int Priority, string CreationDate, string ModificationDate, bool IsActive)
        {
            DrivingLicenseTypeId = _DrivingLicenseTypeId;
            this.DrivingLicenseTypeTitlePersian = DrivingLicenseTypeTitlePersian;
            this.DrivingLicenseTypeTitleEnglish = DrivingLicenseTypeTitleEnglish;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
