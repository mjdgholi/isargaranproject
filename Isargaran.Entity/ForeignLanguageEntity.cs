﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <زبان خارجی>
    /// </summary>
    public class ForeignLanguageEntity
    {
         #region Properties :

        public Guid ForeignLanguageId { get; set; }
        public string ForeignLanguagePersianTitle { get; set; }

        public string ForeignLanguageEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ForeignLanguageEntity()
        {
        }

        public ForeignLanguageEntity(Guid _ForeignLanguageId, string ForeignLanguagePersianTitle, string ForeignLanguageEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ForeignLanguageId = _ForeignLanguageId;
            this.ForeignLanguagePersianTitle = ForeignLanguagePersianTitle;
            this.ForeignLanguageEnglishTitle = ForeignLanguageEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}