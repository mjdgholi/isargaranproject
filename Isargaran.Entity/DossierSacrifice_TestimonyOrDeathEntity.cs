﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/23>
    // Description:	<اطلاعات فوت یا شهادت>
    /// </summary>
    public class DossierSacrifice_TestimonyOrDeathEntity
    {


        #region Properties :

        public Guid DossierSacrifice_TestimonyOrDeathId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string DaethDate { get; set; }

        public Guid CityId { get; set; }

        public string VillageTitle { get; set; }

        public string TombTitle { get; set; }

        public int? PartNo { get; set; }

        public int? RowNo { get; set; }

        public int? GraveNo { get; set; }

        public string TestimonyLocation { get; set; }

        public int? DurationOfPresenceInBattlefield { get; set; }

        public Guid? EucationDegreeId { get; set; }

        public Guid? EducationCourseId { get; set; }

        public Guid? OrganizationPhysicalChartId { get; set; }

        public Guid? PostId { get; set; }

        public Guid MilitaryUnitDispatcherId { get; set; }

        public bool? IsWarDisappeared { get; set; }

        public Guid TestimonyPeriodTimeId { get; set; }
        public Guid? CauseEventTypeId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        #endregion

        #region Constrauctors :

        public DossierSacrifice_TestimonyOrDeathEntity()
        {
        }

        public DossierSacrifice_TestimonyOrDeathEntity(Guid _DossierSacrifice_TestimonyOrDeathId, Guid DossierSacrificeId, string DaethDate, Guid CityId, string VillageTitle, string TombTitle, int? PartNo, int? RowNo, int? GraveNo, string TestimonyLocation, int? DurationOfPresenceInBattlefield, Guid? EucationDegreeId, Guid? EducationCourseId, Guid? OrganizationPhysicalChartId, Guid? PostId, Guid MilitaryUnitDispatcherId, bool? IsWarDisappeared, Guid TestimonyPeriodTimeId, Guid? CauseEventTypeId, string CreationDate, string ModificationDate)
        {
            this.DossierSacrifice_TestimonyOrDeathId = _DossierSacrifice_TestimonyOrDeathId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.DaethDate = DaethDate;
            this.CityId = CityId;
            this.VillageTitle = VillageTitle;
            this.TombTitle = TombTitle;
            this.PartNo = PartNo;
            this.RowNo = RowNo;
            this.GraveNo = GraveNo;
            this.TestimonyLocation = TestimonyLocation;
            this.DurationOfPresenceInBattlefield = DurationOfPresenceInBattlefield;
            this.EucationDegreeId = EucationDegreeId;
            this.EducationCourseId = EducationCourseId;
            this.OrganizationPhysicalChartId = OrganizationPhysicalChartId;
            this.PostId = PostId;
            this.MilitaryUnitDispatcherId = MilitaryUnitDispatcherId;
            this.IsWarDisappeared = IsWarDisappeared;
            this.TestimonyPeriodTimeId = TestimonyPeriodTimeId;
            this.CauseEventTypeId = CauseEventTypeId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}