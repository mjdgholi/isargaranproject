﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/26>
    /// Description: <پرونده خدمات سربازی ایثارگر>
    /// </summary>
  public  class DossierSacrifice_MilitaryServiceEntity
    {
                #region Properties :

        public Guid DossierSacrifice_MilitaryServiceId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public bool HasMilitaryServiceCard { get; set; }

        public string MilitaryServiceLocation { get; set; }

        public string DistrictHandler { get; set; }

        public string ClassifiedNo { get; set; }

        public string CardIssueDate { get; set; }

        public string CardNo { get; set; }

        public Guid? MilitaryStatusId { get; set; }

        public Guid? CityId { get; set; }

        public string VillageTitle { get; set; }

        public string SectionTitle { get; set; }

        public string ZipCode { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string WebAddress { get; set; }

        public string TelNo { get; set; }

        public string FaxNo { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public Guid ProvinceId { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_MilitaryServiceEntity()
        {
        }

        public DossierSacrifice_MilitaryServiceEntity(Guid _DossierSacrifice_MilitaryServiceId, Guid DossierSacrificeId, bool HasMilitaryServiceCard, string MilitaryServiceLocation, string DistrictHandler, string ClassifiedNo, string CardIssueDate, string CardNo, Guid? MilitaryStatusId, Guid? CityId, string VillageTitle, string SectionTitle, string ZipCode, string Address, string Email, string WebAddress, string TelNo, string FaxNo, string StartDate, string EndDate, string CreationDate, string ModificationDate, Guid ProvinceId)
        {
            DossierSacrifice_MilitaryServiceId = _DossierSacrifice_MilitaryServiceId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.HasMilitaryServiceCard = HasMilitaryServiceCard;
            this.MilitaryServiceLocation = MilitaryServiceLocation;
            this.DistrictHandler = DistrictHandler;
            this.ClassifiedNo = ClassifiedNo;
            this.CardIssueDate = CardIssueDate;
            this.CardNo = CardNo;
            this.MilitaryStatusId = MilitaryStatusId;
            this.CityId = CityId;
            this.VillageTitle = VillageTitle;
            this.SectionTitle = SectionTitle;
            this.ZipCode = ZipCode;
            this.Address = Address;
            this.Email = Email;
            this.WebAddress = WebAddress;
            this.TelNo = TelNo;
            this.FaxNo = FaxNo;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.ProvinceId = ProvinceId;

        }

        #endregion
    }
}
