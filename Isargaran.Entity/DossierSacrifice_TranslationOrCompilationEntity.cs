﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/09>
    /// Description: <پرونده اطلاعات تالیف و ترجمه ایثارگر>
    /// </summary>
  public  class DossierSacrifice_TranslationOrCompilationEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_TranslationOrCompilationId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string BaseBookTitle { get; set; }

        public string TranslatedBookTitle { get; set; }

        public string BookSubject { get; set; }

        public string Publication { get; set; }

        public string FirstPublicationDate { get; set; }

        public int? TurnPublicationNo { get; set; }

        public Guid? WritingActivityTypeId { get; set; }

        public string Description { get; set; }

        public string ModificationDate { get; set; }

        public string CreationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_TranslationOrCompilationEntity()
        {
        }

        public DossierSacrifice_TranslationOrCompilationEntity(Guid _DossierSacrifice_TranslationOrCompilationId, Guid DossierSacrificeId, string BaseBookTitle, string TranslatedBookTitle, string BookSubject, string Publication, string FirstPublicationDate, int? TurnPublicationNo, Guid? WritingActivityTypeId, string Description, string ModificationDate, string CreationDate)
        {
            DossierSacrifice_TranslationOrCompilationId = _DossierSacrifice_TranslationOrCompilationId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.BaseBookTitle = BaseBookTitle;
            this.TranslatedBookTitle = TranslatedBookTitle;
            this.BookSubject = BookSubject;
            this.Publication = Publication;
            this.FirstPublicationDate = FirstPublicationDate;
            this.TurnPublicationNo = TurnPublicationNo;
            this.WritingActivityTypeId = WritingActivityTypeId;
            this.Description = Description;
            this.ModificationDate = ModificationDate;
            this.CreationDate = CreationDate;

        }

        #endregion
    }
}
