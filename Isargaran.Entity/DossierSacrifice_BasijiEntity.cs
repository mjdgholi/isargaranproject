﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <پرونده  بسیجی ایثارگر>
    /// </summary>
  public  class DossierSacrifice_BasijiEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_BasijiId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public bool IsBasiji { get; set; }

        public string MembershipDate { get; set; }

        public Guid BasijiMembershipTypeId { get; set; }

        public Guid? BasijiCategoryId { get; set; }

        public string BasijiCode { get; set; }

        public string BasijiCardNo { get; set; }

        public string InsuranceSerialNumberCard { get; set; }

        public string InsuranceDateCard { get; set; }

        public Guid? BasijiBaseId { get; set; }

        public Guid? CityId { get; set; }

        public string VillageTitle { get; set; }

        public string SectionTitle { get; set; }

        public string ZipCode { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string WebAddress { get; set; }

        public string TelNo { get; set; }

        public string FaxNo { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public Guid? ProvinceId { get; set; }

        #endregion

        #region Constrauctors :

        public DossierSacrifice_BasijiEntity()
        {
        }

        public DossierSacrifice_BasijiEntity(Guid _DossierSacrifice_BasijiId, Guid DossierSacrificeId, bool IsBasiji, string MembershipDate, Guid BasijiMembershipTypeId, Guid? BasijiCategoryId, string BasijiCode, string BasijiCardNo, string InsuranceSerialNumberCard, string InsuranceDateCard, Guid? BasijiBaseId, Guid? CityId, string VillageTitle, string SectionTitle, string ZipCode, string Address, string Email, string WebAddress, string TelNo, string FaxNo, string StartDate, string EndDate, string CreationDate, string ModificationDate, Guid? ProvinceId)
        {
            DossierSacrifice_BasijiId = _DossierSacrifice_BasijiId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.IsBasiji = IsBasiji;
            this.MembershipDate = MembershipDate;
            this.BasijiMembershipTypeId = BasijiMembershipTypeId;
            this.BasijiCategoryId = BasijiCategoryId;
            this.BasijiCode = BasijiCode;
            this.BasijiCardNo = BasijiCardNo;
            this.InsuranceSerialNumberCard = InsuranceSerialNumberCard;
            this.InsuranceDateCard = InsuranceDateCard;
            this.BasijiBaseId = BasijiBaseId;
            this.CityId = CityId;
            this.VillageTitle = VillageTitle;
            this.SectionTitle = SectionTitle;
            this.ZipCode = ZipCode;
            this.Address = Address;
            this.Email = Email;
            this.WebAddress = WebAddress;
            this.TelNo = TelNo;
            this.FaxNo = FaxNo;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.ProvinceId = ProvinceId;

        }

        #endregion

    }
}
