﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/21>
    // Description:	<اطلاعات  اطلاعات خدمات و تسهیلات نقدی>
    /// </summary>
  public  class DossierSacrifice_FacilityAndServiceCashEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_FacilityAndServiceCashId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid FacilityAndServiceCashId { get; set; }

        public string FacilityAndServiceCashDate { get; set; }

        public Guid? OccasionId { get; set; }

        public decimal? CashAmount { get; set; }

        public string RecipientPersonName { get; set; }

        public string RecipientPersonNationalNo { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_FacilityAndServiceCashEntity()
        {
        }

        public DossierSacrifice_FacilityAndServiceCashEntity(Guid _DossierSacrifice_FacilityAndServiceCashId, Guid DossierSacrificeId, Guid FacilityAndServiceCashId, string FacilityAndServiceCashDate, Guid? OccasionId, decimal? CashAmount, string RecipientPersonName, string RecipientPersonNationalNo, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_FacilityAndServiceCashId = _DossierSacrifice_FacilityAndServiceCashId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.FacilityAndServiceCashId = FacilityAndServiceCashId;
            this.FacilityAndServiceCashDate = FacilityAndServiceCashDate;
            this.OccasionId = OccasionId;
            this.CashAmount = CashAmount;
            this.RecipientPersonName = RecipientPersonName;
            this.RecipientPersonNationalNo = RecipientPersonNationalNo;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
