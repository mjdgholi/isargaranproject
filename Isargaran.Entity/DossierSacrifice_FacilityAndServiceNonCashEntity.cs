﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<اطلاعات  اطلاعات خدمات و تسهیلات غیر نقدی>
    /// </summary>
   public class DossierSacrifice_FacilityAndServiceNonCashEntity
    {
                #region Properties :

        public Guid DossierSacrifice_FacilityAndServiceNonCashId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid FacilityAndServiceNonCashId { get; set; }

        public string FacilityAndServiceNonCashDate { get; set; }

        public Guid? OccasionId { get; set; }

        public decimal? CashAmount { get; set; }

        public string RecipientPersonName { get; set; }

        public string RecipientPersonNationalNo { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_FacilityAndServiceNonCashEntity()
        {
        }

        public DossierSacrifice_FacilityAndServiceNonCashEntity(Guid _DossierSacrifice_FacilityAndServiceNonCashId, Guid DossierSacrificeId, Guid FacilityAndServiceNonCashId, string FacilityAndServiceNonCashDate, Guid? OccasionId, decimal? CashAmount, string RecipientPersonName, string RecipientPersonNationalNo, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_FacilityAndServiceNonCashId = _DossierSacrifice_FacilityAndServiceNonCashId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.FacilityAndServiceNonCashId = FacilityAndServiceNonCashId;
            this.FacilityAndServiceNonCashDate = FacilityAndServiceNonCashDate;
            this.OccasionId = OccasionId;
            this.CashAmount = CashAmount;
            this.RecipientPersonName = RecipientPersonName;
            this.RecipientPersonNationalNo = RecipientPersonNationalNo;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
