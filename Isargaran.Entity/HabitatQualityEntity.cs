﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<کیفیت محل اقامت>
    /// </summary>
   public class HabitatQualityEntity
    {
                #region Properties :

        public Guid HabitatQualityId { get; set; }
        public string HabitatQualityTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public HabitatQualityEntity()
        {
        }

        public HabitatQualityEntity(Guid _HabitatQualityId, string HabitatQualityTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            HabitatQualityId = _HabitatQualityId;
            this.HabitatQualityTitle = HabitatQualityTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
