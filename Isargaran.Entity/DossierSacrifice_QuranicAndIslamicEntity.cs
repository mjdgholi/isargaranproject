﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <پرونده معارف و علوم قرآنی ایثارگر>
    /// </summary>
   public class DossierSacrifice_QuranicAndIslamicEntity
    {
                #region Properties :

        public Guid DossierSacrifice_QuranicAndIslamicId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid QuranicAndIslamicCourseId { get; set; }

        public int? ExperienceYearNo { get; set; }

        public bool? HasEvidence { get; set; }

        public string PlaceOfGraduation { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_QuranicAndIslamicEntity()
        {
        }

        public DossierSacrifice_QuranicAndIslamicEntity(Guid _DossierSacrifice_QuranicAndIslamicId, Guid DossierSacrificeId, Guid QuranicAndIslamicCourseId, int? ExperienceYearNo, bool? HasEvidence, string PlaceOfGraduation, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_QuranicAndIslamicId = _DossierSacrifice_QuranicAndIslamicId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.QuranicAndIslamicCourseId = QuranicAndIslamicCourseId;
            this.ExperienceYearNo = ExperienceYearNo;
            this.HasEvidence = HasEvidence;
            this.PlaceOfGraduation = PlaceOfGraduation;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
