﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات زن یا مرد شاخص>
    /// </summary>
  public  class DossierSacrifice_MenOrWomenIndexEntity
    {
                #region Properties :

        public Guid DossierSacrifice_MenOrWomenIndexId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid OccasionId { get; set; }

        public string OccasionDate { get; set; }

        public string IndexFactors { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_MenOrWomenIndexEntity()
        {
        }

        public DossierSacrifice_MenOrWomenIndexEntity(Guid _DossierSacrifice_MenOrWomenIndexId, Guid DossierSacrificeId, Guid OccasionId, string OccasionDate, string IndexFactors, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_MenOrWomenIndexId = _DossierSacrifice_MenOrWomenIndexId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.OccasionId = OccasionId;
            this.OccasionDate = OccasionDate;
            this.IndexFactors = IndexFactors;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
