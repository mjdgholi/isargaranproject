﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات خدمات و تسهیلات آموزشی>
    /// </summary>
   public class DossierSacrifice_FacilitiyEducationEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_FacilitiyEducationId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid EucationDegreeId { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public Guid? PersonId { get; set; }

        public decimal? CashAmount { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

       public string FullName { get; set; }
        public Guid? EducationSystemId { get; set; }

        #endregion

        #region Constrauctors :

        public DossierSacrifice_FacilitiyEducationEntity()
        {
        }

        public DossierSacrifice_FacilitiyEducationEntity(Guid _DossierSacrifice_FacilitiyEducationId, Guid DossierSacrificeId, Guid EucationDegreeId, string StartDate, string EndDate, Guid? PersonId, decimal? CashAmount, string Description, string CreationDate, string ModificationDate, string FullName, Guid? EducationSystemId)
        {
            DossierSacrifice_FacilitiyEducationId = _DossierSacrifice_FacilitiyEducationId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.EucationDegreeId = EucationDegreeId;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.PersonId = PersonId;
            this.CashAmount = CashAmount;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.FullName = FullName;
            this.EducationSystemId = EducationSystemId;
        }

        #endregion

    }
}
