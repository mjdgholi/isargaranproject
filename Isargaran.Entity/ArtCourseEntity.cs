﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
        /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <رشته هنری>
    /// </summary>
 public   class ArtCourseEntity
    {
                #region Properties :

        public Guid ArtCourseId { get; set; }
        public string ArtCourseTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ArtCourseEntity()
        {
        }

        public ArtCourseEntity(Guid _ArtCourseId, string ArtCourseTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ArtCourseId = _ArtCourseId;
            this.ArtCourseTitle = ArtCourseTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
