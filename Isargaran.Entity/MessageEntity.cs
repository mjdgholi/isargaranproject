﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/25>
    /// Description: <مدیریت پیغام>
    /// </summary>
    public class MessageEntity
    {
         
        #region Properties :

        public Guid MessageId { get; set; }
        public Guid DossierSacrifice_RequestId { get; set; }

        public Guid MessageTypeId { get; set; }

        public string MessageText { get; set; }

        public bool IsViewed { get; set; }

        public string ViewedDate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public MessageEntity()
        {
        }

        public MessageEntity(Guid _MessageId, Guid DossierSacrifice_RequestId, Guid MessageTypeId, string MessageText, bool IsViewed, string ViewedDate, string CreationDate, string ModificationDate)
        {
            MessageId = _MessageId;
            this.DossierSacrifice_RequestId = DossierSacrifice_RequestId;
            this.MessageTypeId = MessageTypeId;
            this.MessageText = MessageText;
            this.IsViewed = IsViewed;
            this.ViewedDate = ViewedDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}