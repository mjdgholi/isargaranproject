﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/03/12>
    // Description:	<نوع تخلف>
    /// </summary>
    public class InfractionTypeEntity
    {
        
        #region Properties :

        public Guid InfractionTypeId { get; set; }
        public string InfractionTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public InfractionTypeEntity()
        {
        }

        public InfractionTypeEntity(Guid _InfractionTypeId, string InfractionTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            InfractionTypeId = _InfractionTypeId;
            this.InfractionTypeTitle = InfractionTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
