﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <majid.Gholibeygian>
    /// Create date: <1393/02/22>
    /// Description: <نوع وام>
    /// </summary>
    public class LoanTypeEntity
    {
        #region Properties :

        public Guid LoanTypeId { get; set; }
        public string LoanTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public LoanTypeEntity()
        {
        }

        public LoanTypeEntity(Guid _LoanTypeId, string LoanTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            LoanTypeId = _LoanTypeId;
            this.LoanTypeTitle = LoanTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
        }

        #endregion
    }
}