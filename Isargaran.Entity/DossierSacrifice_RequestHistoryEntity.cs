﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/24>
    /// Description: <پرونده اطلاعات اقدامات درخواست ایثارگر>
    /// </summary>
    public class DossierSacrifice_RequestHistoryEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_RequestHistoryId { get; set; }
        public Guid DossierSacrifice_RequestId { get; set; }

        public Guid StatusId { get; set; }

        public Guid ActionTypeId { get; set; }

        public string ActionDate { get; set; }

        public string ActionDescription { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsValid { get; set; }

        #endregion

        #region Constrauctors :

        public DossierSacrifice_RequestHistoryEntity()
        {
        }

        public DossierSacrifice_RequestHistoryEntity(Guid _DossierSacrifice_RequestHistoryId, Guid DossierSacrifice_RequestId, Guid StatusId, Guid ActionTypeId, string ActionDate, string ActionDescription, string CreationDate, string ModificationDate, bool IsValid)
        {
            DossierSacrifice_RequestHistoryId = _DossierSacrifice_RequestHistoryId;
            this.DossierSacrifice_RequestId = DossierSacrifice_RequestId;
            this.StatusId = StatusId;
            this.ActionTypeId = ActionTypeId;
            this.ActionDate = ActionDate;
            this.ActionDescription = ActionDescription;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsValid = IsValid;

        }

        #endregion 
    }
}