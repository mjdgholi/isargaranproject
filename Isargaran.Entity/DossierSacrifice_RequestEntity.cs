﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <پرونده اطلاعات درخواست ایثارگر>
    /// </summary>
    public class DossierSacrifice_RequestEntity
    {
             #region Properties :

        public Guid DossierSacrifice_RequestId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid RequestTypeId { get; set; }

        public Guid ImportanceTypeId { get; set; }
       
        public string RequestDescription { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public string DossierSacrificeNo { get; set; }
        public Guid? PersonId     { get; set; }
        public string FullName { get; set; }

        #endregion

        #region Constrauctors :

        public DossierSacrifice_RequestEntity()
        {
        }

        public DossierSacrifice_RequestEntity(Guid _DossierSacrifice_RequestId, Guid DossierSacrificeId, Guid RequestTypeId, Guid ImportanceTypeId, string RequestDescription, string CreationDate, string ModificationDate, string DossierSacrificeNo, Guid? PersonId, string FullName)
        {
            DossierSacrifice_RequestId = _DossierSacrifice_RequestId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.RequestTypeId = RequestTypeId;
            this.ImportanceTypeId = ImportanceTypeId;            
            this.RequestDescription = RequestDescription;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.DossierSacrificeNo = DossierSacrificeNo;
            this.PersonId = PersonId;
            this.FullName = FullName;
        }

        #endregion 
    }
}