﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/02>
    /// Description: <نوع حادثه>
    /// </summary>

    public class CauseEventTypeEntity
    {
      
        #region Properties :

        public Guid CauseEventTypeId { get; set; }
        public string CauseEventTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public CauseEventTypeEntity()
        {
        }

        public CauseEventTypeEntity(Guid _CauseEventTypeId, string CauseEventTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            CauseEventTypeId = _CauseEventTypeId;
            this.CauseEventTypeTitle = CauseEventTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion   
    }
}