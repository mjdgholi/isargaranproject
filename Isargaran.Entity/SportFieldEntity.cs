﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/27>
    // Description:	<رشته ورزشی>
    /// </summary>
   public class SportFieldEntity
    {
        
        #region Properties :

        public Guid SportFieldId { get; set; }
        public string SportFieldPersianTitle { get; set; }

        public string SportFieldEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SportFieldEntity()
        {
        }

        public SportFieldEntity(Guid _SportFieldId, string SportFieldPersianTitle, string SportFieldEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            SportFieldId = _SportFieldId;
            this.SportFieldPersianTitle = SportFieldPersianTitle;
            this.SportFieldEnglishTitle = SportFieldEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
