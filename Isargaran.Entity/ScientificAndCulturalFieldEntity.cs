﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/22>
    /// Description: <رشته علمی و فرهنگی>
    /// </summary>
 public   class ScientificAndCulturalFieldEntity
    {
           #region Properties :

        public Guid ScientificAndCulturalFieldId { get; set; }
        public string ScientificAndCulturalFieldPersianTitle { get; set; }

        public string ScientificAndCulturalFieldEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ScientificAndCulturalFieldEntity()
        {
        }

        public ScientificAndCulturalFieldEntity(Guid _ScientificAndCulturalFieldId, string ScientificAndCulturalFieldPersianTitle, string ScientificAndCulturalFieldEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ScientificAndCulturalFieldId = _ScientificAndCulturalFieldId;
            this.ScientificAndCulturalFieldPersianTitle = ScientificAndCulturalFieldPersianTitle;
            this.ScientificAndCulturalFieldEnglishTitle = ScientificAndCulturalFieldEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
