﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Update Author: <Narges.Kamran> <1393/08/20>
    /// Create date: <1393/03/11>
    /// 
    /// Description: <وب فرم ها>
    /// </summary>
    public class WebFormEntity
    {
         #region Properties :

        public Guid WebFormId { get; set; }
        public string WebFormTitle { get; set; }

        public string WebFormURL { get; set; }


        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public WebFormEntity()
        {
        }

        public WebFormEntity(Guid _WebFormId, string WebFormTitle, string WebFormURL, string CreationDate, string ModificationDate, bool IsActive)
        {
            WebFormId = _WebFormId;
            this.WebFormTitle = WebFormTitle;
            this.WebFormURL = WebFormURL;            
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}