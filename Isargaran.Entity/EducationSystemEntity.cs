﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <سیستم تحصیلی>
    /// </summary>
   public class EducationSystemEntity
    {
        
        #region Properties :

        public Guid EducationSystemId { get; set; }
        public string EducationSystemPersianTitle { get; set; }

        public string EducationSystemEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public EducationSystemEntity()
        {
        }

        public EducationSystemEntity(Guid _EducationSystemId, string EducationSystemPersianTitle, string EducationSystemEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            EducationSystemId = _EducationSystemId;
            this.EducationSystemPersianTitle = EducationSystemPersianTitle;
            this.EducationSystemEnglishTitle = EducationSystemEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
