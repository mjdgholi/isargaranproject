﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات  عناوین قهرمانی علمی و فرهنگی>
    /// </summary>
  public  class DossierSacrifice_ScientificAndCulturalChampionshipEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_ScientificAndCulturalChampionshipId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid ScientificAndCulturalFieldId { get; set; }

        public string PointOrMedal { get; set; }

        public string CompetitionDate { get; set; }

        public string CompetitionTitle { get; set; }

        public string CompetitionLocation { get; set; }

        public Guid? PersonId { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public string FullName { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_ScientificAndCulturalChampionshipEntity()
        {
        }

        public DossierSacrifice_ScientificAndCulturalChampionshipEntity(Guid _DossierSacrifice_ScientificAndCulturalChampionshipId, Guid DossierSacrificeId, Guid ScientificAndCulturalFieldId, string PointOrMedal, string CompetitionDate, string CompetitionTitle, string CompetitionLocation, Guid? PersonId, string Description, string CreationDate, string ModificationDate,string FullName)
        {
            DossierSacrifice_ScientificAndCulturalChampionshipId = _DossierSacrifice_ScientificAndCulturalChampionshipId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.ScientificAndCulturalFieldId = ScientificAndCulturalFieldId;
            this.PointOrMedal = PointOrMedal;
            this.CompetitionDate = CompetitionDate;
            this.CompetitionTitle = CompetitionTitle;
            this.CompetitionLocation = CompetitionLocation;
            this.PersonId = PersonId;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.FullName = FullName;
        }

        #endregion
    }
}
