﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<اطلاعات ظاهری>
    /// </summary>
    public class DossierSacrifice_AppearanceEntity
    {

        #region Properties :

        public Guid DossierSacrifice_AppearanceId { get; set; }

        public Guid DossierSacrificeId { get; set; }

        public Guid BloodGroupId { get; set; }

        public Guid? EyeColorId { get; set; }

        public Guid? HairColorId { get; set; }

        public Guid? SkinColorId { get; set; }

        public int? Height { get; set; }

        public decimal? Weight { get; set; }

        public int? ShoeSize { get; set; }

        public string CostumeSize { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_AppearanceEntity()
        {
        }

        public DossierSacrifice_AppearanceEntity(Guid _DossierSacrifice_AppearanceId, Guid DossierSacrificeId, Guid BloodGroupId, Guid? EyeColorId, Guid? HairColorId, Guid? SkinColorId, int? Height, decimal? Weight, int? ShoeSize, string CostumeSize, string CreationDate, string ModificationDate, bool IsActive)
        {
            DossierSacrifice_AppearanceId = _DossierSacrifice_AppearanceId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.BloodGroupId = BloodGroupId;
            this.EyeColorId = EyeColorId;
            this.HairColorId = HairColorId;
            this.SkinColorId = SkinColorId;
            this.Height = Height;
            this.Weight = Weight;
            this.ShoeSize = ShoeSize;
            this.CostumeSize = CostumeSize;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion


    }
}