﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/14>
    /// Description: <پرونده هنری ایثارگر>
    /// </summary>
   public class DossierSacrifice_ArtEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_ArtId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid ArtCourseId { get; set; }

        public bool? HasEvidence { get; set; }

        public string PlaceOfGraduation { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_ArtEntity()
        {
        }

        public DossierSacrifice_ArtEntity(Guid _DossierSacrifice_ArtId, Guid DossierSacrificeId, Guid ArtCourseId, bool? HasEvidence, string PlaceOfGraduation, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_ArtId = _DossierSacrifice_ArtId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.ArtCourseId = ArtCourseId;
            this.HasEvidence = HasEvidence;
            this.PlaceOfGraduation = PlaceOfGraduation;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
