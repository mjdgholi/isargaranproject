﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <حوزه بسیجی>
    /// </summary>
   public class DistrictBasijiEntity
    {
               #region Properties :

        public Guid DistrictBasijiId { get; set; }
        public string DistrictBasijiTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DistrictBasijiEntity()
        {
        }

        public DistrictBasijiEntity(Guid _DistrictBasijiId, string DistrictBasijiTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            DistrictBasijiId = _DistrictBasijiId;
            this.DistrictBasijiTitle = DistrictBasijiTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
