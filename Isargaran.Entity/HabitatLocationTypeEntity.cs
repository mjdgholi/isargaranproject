﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<موقیعت محل اقامت>
    /// </summary>
  public  class HabitatLocationTypeEntity
    {
        
        #region Properties :

        public Guid HabitatLocationTypeId { get; set; }
        public string HabitatLocationTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public HabitatLocationTypeEntity()
        {
        }

        public HabitatLocationTypeEntity(Guid _HabitatLocationTypeId, string HabitatLocationTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            HabitatLocationTypeId = _HabitatLocationTypeId;
            this.HabitatLocationTypeTitle = HabitatLocationTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
