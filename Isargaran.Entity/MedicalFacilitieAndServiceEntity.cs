﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/18>
    /// Description: <تسهیلات و خدمات پزشکی>
    /// </summary>
  public  class MedicalFacilitieAndServiceEntity
    {
                #region Properties :

        public Guid MedicalFacilitieAndServiceId { get; set; }
        public string MedicalFacilitieAndServiceTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public MedicalFacilitieAndServiceEntity()
        {
        }

        public MedicalFacilitieAndServiceEntity(Guid _MedicalFacilitieAndServiceId, string MedicalFacilitieAndServiceTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            MedicalFacilitieAndServiceId = _MedicalFacilitieAndServiceId;
            this.MedicalFacilitieAndServiceTitle = MedicalFacilitieAndServiceTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
