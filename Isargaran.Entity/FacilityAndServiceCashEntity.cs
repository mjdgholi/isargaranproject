﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/21>
    /// Description: <خدمات و تسهیلات نقدی>
    /// </summary>
   public class FacilityAndServiceCashEntity
    {
                #region Properties :

        public Guid FacilityAndServiceCashId { get; set; }
        public string FacilityAndServiceCashTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public FacilityAndServiceCashEntity()
        {
        }

        public FacilityAndServiceCashEntity(Guid _FacilityAndServiceCashId, string FacilityAndServiceCashTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            FacilityAndServiceCashId = _FacilityAndServiceCashId;
            this.FacilityAndServiceCashTitle = FacilityAndServiceCashTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
