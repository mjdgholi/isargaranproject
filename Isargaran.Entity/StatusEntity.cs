﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع وضیعت درخواست>
    /// </summary>
    public class StatusEntity
    {
         #region Properties :

        public Guid StatusId { get; set; }
        public string StatusTitle { get; set; }

        public int Priority { get; set; }        

        public bool IsFinal { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public StatusEntity()
        {
        }

        public StatusEntity(Guid _StatusId, string StatusTitle, int Priority, bool IsFinal, string CreationDate, string ModificationDate, bool IsActive)
        {
            StatusId = _StatusId;
            this.StatusTitle = StatusTitle;
            this.Priority = Priority;            
            this.IsFinal = IsFinal;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}