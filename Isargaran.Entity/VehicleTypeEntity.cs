﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/18>
    // Description:	<نوع وسیله حمل و نقل>
    /// </summary>
 public   class VehicleTypeEntity
    {
                #region Properties :

        public Guid VehicleTypeId { get; set; }
        public string VehicleTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public VehicleTypeEntity()
        {
        }

        public VehicleTypeEntity(Guid _VehicleTypeId, string VehicleTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            VehicleTypeId = _VehicleTypeId;
            this.VehicleTypeTitle = VehicleTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
