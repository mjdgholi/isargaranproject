﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<مناسبت>
    /// </summary>
  public  class OccasionEntity
    {
        
        #region Properties :

        public Guid OccasionId { get; set; }
        public string OccasionTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public OccasionEntity()
        {
        }

        public OccasionEntity(Guid _OccasionId, string OccasionTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            OccasionId = _OccasionId;
            this.OccasionTitle = OccasionTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
