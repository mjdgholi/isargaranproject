﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <مراکزتحصیلی>
    /// </summary>
   public class EducationCenterEntity
    {
    #region Properties :

        public Guid EducationCenterId { get; set; }
        public Guid EducationCenterTypeId { get; set; }

        public string EducationCenterPersianTitle { get; set; }

        public string EducationCenterEnglishTitle { get; set; }

        public string Email { get; set; }

        public string TelNo { get; set; }

        public string FaxNo { get; set; }

        public string ZipCode { get; set; }

        public string Address { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public EducationCenterEntity()
        {
        }

        public EducationCenterEntity(Guid _EducationCenterId, Guid EducationCenterTypeId, string EducationCenterPersianTitle, string EducationCenterEnglishTitle, string Email, string TelNo, string FaxNo, string ZipCode, string Address, string CreationDate, string ModificationDate, bool IsActive)
        {
            EducationCenterId = _EducationCenterId;
            this.EducationCenterTypeId = EducationCenterTypeId;
            this.EducationCenterPersianTitle = EducationCenterPersianTitle;
            this.EducationCenterEnglishTitle = EducationCenterEnglishTitle;
            this.Email = Email;
            this.TelNo = TelNo;
            this.FaxNo = FaxNo;
            this.ZipCode = ZipCode;
            this.Address = Address;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
