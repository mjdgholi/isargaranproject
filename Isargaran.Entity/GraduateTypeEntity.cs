﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <نوع فارغ تحصیل>
    /// </summary>
    public class GraduateTypeEntity
    {
        #region Properties :

        public Guid GraduateTypeId { get; set; }

        public string GraduateTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public GraduateTypeEntity()
        {
        }

        public GraduateTypeEntity(Guid _GraduateTypeId, string GraduateTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            GraduateTypeId = _GraduateTypeId;
            this.GraduateTypeTitle = GraduateTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}