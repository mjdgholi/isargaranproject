﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/17>
    /// Description: <وضیعت مالکیت مسکن>
    /// </summary>
   public class HabitatOwnershipStatusEntity
    {
        
        #region Properties :

        public Guid HabitatOwnershipStatusId { get; set; }
        public string HabitatOwnershipStatusTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public HabitatOwnershipStatusEntity()
        {
        }

        public HabitatOwnershipStatusEntity(Guid _HabitatOwnershipStatusId, string HabitatOwnershipStatusTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            HabitatOwnershipStatusId = _HabitatOwnershipStatusId;
            this.HabitatOwnershipStatusTitle = HabitatOwnershipStatusTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
