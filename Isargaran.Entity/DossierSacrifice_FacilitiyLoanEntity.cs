﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <majid.Gholibeygian>
    /// Create date: <1393/02/22>
    /// Description: <اطلاعات وام>
    /// </summary>
    public class DossierSacrifice_FacilitiyLoanEntity
    {
        #region Properties :

        public Guid DossierSacrifice_FacilitiyLoanId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid LoanTypeId { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string SourceBudgetPayment { get; set; }

        public decimal LoanPaymentAmount { get; set; }

        public decimal? LoanCommissionAmount { get; set; }

        public string RecipientPersonName { get; set; }

        public string RecipientPersonNationalNo { get; set; }

        public string Description { get; set; }

        public bool IsLoanSettlement { get; set; }

        public string ModificationDate { get; set; }

        public string CreationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_FacilitiyLoanEntity()
        {
        }

        public DossierSacrifice_FacilitiyLoanEntity(Guid _DossierSacrifice_FacilitiyLoanId, Guid DossierSacrificeId, Guid LoanTypeId, string StartDate, string EndDate, string SourceBudgetPayment, decimal LoanPaymentAmount, decimal? LoanCommissionAmount, string RecipientPersonName, string RecipientPersonNationalNo, string Description, bool IsLoanSettlement, string ModificationDate, string CreationDate)
        {
            DossierSacrifice_FacilitiyLoanId = _DossierSacrifice_FacilitiyLoanId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.LoanTypeId = LoanTypeId;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.SourceBudgetPayment = SourceBudgetPayment;
            this.LoanPaymentAmount = LoanPaymentAmount;
            this.LoanCommissionAmount = LoanCommissionAmount;
            this.RecipientPersonName = RecipientPersonName;
            this.RecipientPersonNationalNo = RecipientPersonNationalNo;
            this.Description = Description;
            this.IsLoanSettlement = IsLoanSettlement;
            this.ModificationDate = ModificationDate;
            this.CreationDate = CreationDate;

        }

        #endregion

    }
}
