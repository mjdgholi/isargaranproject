﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/02>
    /// Description: <نوع مدت>
    /// </summary>
    public class DurationTypeEntity
    {
                 #region Properties :

        public Guid DurationTypeId { get; set; }
        public string DurationTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DurationTypeEntity()
        {
        }

        public DurationTypeEntity(Guid _DurationTypeId, string DurationTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            DurationTypeId = _DurationTypeId;
            this.DurationTypeTitle = DurationTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}