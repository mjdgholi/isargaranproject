﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: < مهارت خواندن>
    /// </summary>
    public class ReadingSkillEntity
    {
        #region Properties :

        public Guid ReadingSkillId { get; set; }
        public string ReadingSkillPersianTitle { get; set; }

        public string ReadingSkillEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }


        #endregion

        #region Constrauctors :

        public ReadingSkillEntity()
        {
        }

        public ReadingSkillEntity(Guid _ReadingSkillId, string ReadingSkillPersianTitle, string ReadingSkillEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ReadingSkillId = _ReadingSkillId;
            this.ReadingSkillPersianTitle = ReadingSkillPersianTitle;
            this.ReadingSkillEnglishTitle = ReadingSkillEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}