﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <اطلاعات تحصیلی ایثارگر>
    /// </summary>
   public class DossierSacrifice_StudentEntity
    {
       #region Properties :

        public Guid DossierSacrifice_StudentId { get; set; }

        public Guid DossierSacrificeId { get; set; }

        public Guid? AdmissionTypeId { get; set; }

        public Guid? EducationCenterId { get; set; }

        public Guid EucationDegreeId { get; set; }

        public Guid? EducationCourseId { get; set; }

        public Guid? EducationOrientationId { get; set; }

        public Guid? CityId { get; set; }

        public Guid EducationModeId { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string StudentNo { get; set; }

        public Guid? GraduateTypeId { get; set; }

        public string InChargeOfEducation { get; set; }

        public string TechnicalDescription { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_StudentEntity()
        {
        }

        public DossierSacrifice_StudentEntity(Guid _DossierSacrifice_StudentId, Guid DossierSacrificeId, Guid? AdmissionTypeId, Guid? EducationCenterId, Guid EucationDegreeId, Guid? EducationCourseId, Guid? EducationOrientationId, Guid? CityId, Guid EducationModeId, string StartDate, string EndDate, string StudentNo, Guid? GraduateTypeId, string InChargeOfEducation, string TechnicalDescription, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_StudentId = _DossierSacrifice_StudentId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.AdmissionTypeId = AdmissionTypeId;
            this.EducationCenterId = EducationCenterId;
            this.EucationDegreeId = EucationDegreeId;
            this.EducationCourseId = EducationCourseId;
            this.EducationOrientationId = EducationOrientationId;
            this.CityId = CityId;
            this.EducationModeId = EducationModeId;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.StudentNo = StudentNo;
            this.GraduateTypeId = GraduateTypeId;
            this.InChargeOfEducation = InChargeOfEducation;
            this.TechnicalDescription = TechnicalDescription;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
