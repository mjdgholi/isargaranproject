﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/14>
    /// Description: <پرونده  تحصیلی ایثارگری>
    /// </summary>
  public  class DossierSacrifice_EducationEntity
    {
                #region Properties :

        public Guid DossierSacrifice_EducationId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid? AdmissionTypeId { get; set; }

        public Guid? EducationCenterId { get; set; }

        public Guid EucationDegreeId { get; set; }

        public Guid? EducationCourseId { get; set; }

        public Guid? EducationOrientationId { get; set; }

        public Guid? CityId { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string StudentNo { get; set; }

        public decimal? GradePointAverage { get; set; }

        public string FirstSupervisor { get; set; }

        public string SecondSupervisor { get; set; }

        public string FirstAdvisor { get; set; }

        public string SecondAdvisor { get; set; }

        public string ThesisSubject { get; set; }

        public string TechnicalDescription { get; set; }

        public string ModificationDate { get; set; }

        public string CreationDate { get; set; }

      public  Guid? ProvinceId { get; set; }
    
        #endregion

        #region Constrauctors :

        public DossierSacrifice_EducationEntity()
        {
        }

        public DossierSacrifice_EducationEntity(Guid _DossierSacrifice_EducationId, Guid DossierSacrificeId, Guid? AdmissionTypeId, Guid? EducationCenterId, Guid EucationDegreeId, Guid? EducationCourseId, Guid? EducationOrientationId, Guid? CityId, string StartDate, string EndDate, string StudentNo, decimal? GradePointAverage, string FirstSupervisor, string SecondSupervisor, string FirstAdvisor, string SecondAdvisor, string ThesisSubject, string TechnicalDescription, string ModificationDate, string CreationDate, Guid? ProvinceId)
        {
            DossierSacrifice_EducationId = _DossierSacrifice_EducationId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.AdmissionTypeId = AdmissionTypeId;
            this.EducationCenterId = EducationCenterId;
            this.EucationDegreeId = EucationDegreeId;
            this.EducationCourseId = EducationCourseId;
            this.EducationOrientationId = EducationOrientationId;
            this.CityId = CityId;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.StudentNo = StudentNo;
            this.GradePointAverage = GradePointAverage;
            this.FirstSupervisor = FirstSupervisor;
            this.SecondSupervisor = SecondSupervisor;
            this.FirstAdvisor = FirstAdvisor;
            this.SecondAdvisor = SecondAdvisor;
            this.ThesisSubject = ThesisSubject;
            this.TechnicalDescription = TechnicalDescription;
            this.ModificationDate = ModificationDate;
            this.CreationDate = CreationDate;
            this.ProvinceId = ProvinceId;
           

        }

        #endregion
    }
}
