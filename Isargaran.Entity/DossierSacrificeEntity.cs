﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <پرونده ایثارگری>
    /// </summary>
    public class DossierSacrificeEntity
    {
        #region Properties :

        public Guid DossierSacrificeId { get; set; }
        public string DossierSacrificeNo { get; set; }

        public string SacrificeNo { get; set; }

        public Guid PersonId { get; set; }

        public string PersonFirstName { get; set; }

        public string PersonLastName { get; set; }

        public Guid DossierStatusId { get; set; }

        public Guid NaturalizationId { get; set; }

        public Guid NationalityId { get; set; }

        public Guid FaithId { get; set; }

        public Guid ReligionId { get; set; }

        public Guid? MarriageStatusId { get; set; }

        public string MarriageDate { get; set; }

        public bool HasInsurance { get; set; }

        public string InsuranceNo { get; set; }

        public bool HasPassport { get; set; }

        public string PassportNo { get; set; }

        public string Email { get; set; }

        public string HomeAddress { get; set; }

        public string HomePhoneNo { get; set; }

        public string HomeFaxNo { get; set; }

        public string CellphoneNo { get; set; }

        public string ZipCode { get; set; }

        public Guid CityId { get; set; }

        public bool HasDependentSacrifice { get; set; }

        public Guid? DependentTypeId { get; set; }

        public bool HasDossierSacrifice { get; set; }

        public string ImagePath { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        public Guid ProvinceId { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrificeEntity()
        {
        }

        public DossierSacrificeEntity(Guid _DossierSacrificeId, string DossierSacrificeNo, string SacrificeNo, Guid PersonId,string personFirstName,string personLastName, Guid DossierStatusId, Guid NaturalizationId, Guid NationalityId, Guid FaithId, Guid ReligionId, Guid? MarriageStatusId, string MarriageDate, bool HasInsurance, string InsuranceNo, bool HasPassport, string PassportNo, string Email, string HomeAddress, string HomePhoneNo, string HomeFaxNo, string CellphoneNo, string ZipCode, Guid CityId, bool HasDependentSacrifice, Guid? DependentTypeId, bool HasDossierSacrifice, string ImagePath, string CreationDate, string ModificationDate, bool IsActive, Guid ProvinceId)
        {
            DossierSacrificeId = _DossierSacrificeId;
            this.DossierSacrificeNo = DossierSacrificeNo;
            this.SacrificeNo = SacrificeNo;
            this.PersonId = PersonId;
            this.PersonFirstName = personFirstName;
            this.PersonLastName = personLastName;
            this.DossierStatusId = DossierStatusId;
            this.NaturalizationId = NaturalizationId;
            this.NationalityId = NationalityId;
            this.FaithId = FaithId;
            this.ReligionId = ReligionId;
            this.MarriageStatusId = MarriageStatusId;
            this.MarriageDate = MarriageDate;
            this.HasInsurance = HasInsurance;
            this.InsuranceNo = InsuranceNo;
            this.HasPassport = HasPassport;
            this.PassportNo = PassportNo;
            this.Email = Email;
            this.HomeAddress = HomeAddress;
            this.HomePhoneNo = HomePhoneNo;
            this.HomeFaxNo = HomeFaxNo;
            this.CellphoneNo = CellphoneNo;
            this.ZipCode = ZipCode;
            this.CityId = CityId;
            this.HasDependentSacrifice = HasDependentSacrifice;
            this.DependentTypeId = DependentTypeId;
            this.HasDossierSacrifice = HasDossierSacrifice;
            this.ImagePath = ImagePath;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
            this.ProvinceId = ProvinceId;

        }

        #endregion
    }
}