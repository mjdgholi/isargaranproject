﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <مهارت شنیداری>
    /// </summary>
    public class ListeningSkillEntity
    {
        #region Properties :

        public Guid ListeningSkillId { get; set; }
        public string ListeningSkillPersianTitle { get; set; }

        public string ListeningSkillEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public ListeningSkillEntity()
        {
        }

        public ListeningSkillEntity(Guid _ListeningSkillId, string ListeningSkillPersianTitle, string ListeningSkillEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ListeningSkillId = _ListeningSkillId;
            this.ListeningSkillPersianTitle = ListeningSkillPersianTitle;
            this.ListeningSkillEnglishTitle = ListeningSkillEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}