﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/23>
    /// Description: <دوره زمانی شهادت>
    /// </summary>
    public class TestimonyPeriodTimeEntity
    {
        #region Properties :

        public Guid TestimonyPeriodTimeId { get; set; }

        public string TestimonyPeriodTimeTitle { get; set; }

        public int Periority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public TestimonyPeriodTimeEntity()
        {
        }

        public TestimonyPeriodTimeEntity(Guid _TestimonyPeriodTimeId, string TestimonyPeriodTimeTitle, int Periority, string CreationDate, string ModificationDate, bool IsActive)
        {
            TestimonyPeriodTimeId = _TestimonyPeriodTimeId;
            this.TestimonyPeriodTimeTitle = TestimonyPeriodTimeTitle;
            this.Periority = Periority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}