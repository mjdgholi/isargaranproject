﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <رشته تحصیلی>
    /// </summary>
   public class EducationCourseEntity
    {
        
        #region Properties :

        public Guid EducationCourseId { get; set; }
        public string EducationCoursePersianTitle { get; set; }

        public string EducationCourseEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public EducationCourseEntity()
        {
        }

        public EducationCourseEntity(Guid _EducationCourseId, string EducationCoursePersianTitle, string EducationCourseEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            EducationCourseId = _EducationCourseId;
            this.EducationCoursePersianTitle = EducationCoursePersianTitle;
            this.EducationCourseEnglishTitle = EducationCourseEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
