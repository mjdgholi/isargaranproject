﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <پرونده  ایثارگری ایثارگری>
    /// </summary>
   public class DossierSacrifice_SacrificeEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_SacrificeId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid SacrificeId { get; set; }

        public int? TotalValueInDay { get; set; }

        public int? ValueInYear { get; set; }

        public int? ValueInMonth { get; set; }

        public int? ValueInDay { get; set; }
        public Guid? DurationTypeId { get; set; }

        public decimal? PercentValue { get; set; }

        public string StartDate { get; set; }

        public string Endate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_SacrificeEntity()
        {
        }

        public DossierSacrifice_SacrificeEntity(Guid _DossierSacrifice_SacrificeId, Guid DossierSacrificeId, Guid sacrificeId, int? TotalValueInDay, int? ValueInYear, int? ValueInMonth, int? ValueInDay, Guid? durationTypeId, decimal? PercentValue, string StartDate, string Endate, string CreationDate, string ModificationDate, bool IsActive)
        {
            DossierSacrifice_SacrificeId = _DossierSacrifice_SacrificeId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.SacrificeId = sacrificeId;
            this.TotalValueInDay = TotalValueInDay;
            this.ValueInYear = ValueInYear;
            this.ValueInMonth = ValueInMonth;
            this.ValueInDay = ValueInDay;
            this.DurationTypeId = durationTypeId;
            this.PercentValue = PercentValue;
            this.StartDate = StartDate;
            this.Endate = Endate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
