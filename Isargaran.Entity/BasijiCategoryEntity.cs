﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <رسته بسیجی>
    /// </summary>
  public  class BasijiCategoryEntity
    {
        
        #region Properties :

        public Guid BasijiCategoryId { get; set; }
        public string BasijiCategoryTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public BasijiCategoryEntity()
        {
        }

        public BasijiCategoryEntity(Guid _BasijiCategoryId, string BasijiCategoryTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            BasijiCategoryId = _BasijiCategoryId;
            this.BasijiCategoryTitle = BasijiCategoryTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
