﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/07>
    // Description:	< نوع فعالیت نویسندگی >
    /// </summary>
   public class WritingActivityTypeEntity
    {
        
        #region Properties :

        public Guid WritingActivityTypeId { get; set; }
        public string WritingActivityTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public WritingActivityTypeEntity()
        {
        }

        public WritingActivityTypeEntity(Guid _WritingActivityTypeId, string WritingActivityTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            WritingActivityTypeId = _WritingActivityTypeId;
            this.WritingActivityTypeTitle = WritingActivityTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
