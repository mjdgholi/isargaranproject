﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <وضیعت پرونده>
    /// </summary>
  public  class DossierStatusEntity
    {
        
        #region Properties :

        public Guid DossierStatusId { get; set; }
        public string DossierStatusTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DossierStatusEntity()
        {
        }

        public DossierStatusEntity(Guid _DossierStatusId, string DossierStatusTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            DossierStatusId = _DossierStatusId;
            this.DossierStatusTitle = DossierStatusTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
