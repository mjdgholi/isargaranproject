﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <پرونده تخلف>
    /// </summary>
   public class DossierSacrifice_OfficeInfractionEntity
    {
                #region Properties :

        public Guid DossierSacrifice_OfficeInfractionId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string InfractionLocation { get; set; }

        public string InfractionDate { get; set; }

        public string InfractionTime { get; set; }

        public Guid InfractionTypeId { get; set; }

        public string InfractionContent { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_OfficeInfractionEntity()
        {
        }

        public DossierSacrifice_OfficeInfractionEntity(Guid _DossierSacrifice_OfficeInfractionId, Guid DossierSacrificeId, string InfractionLocation, string InfractionDate, string InfractionTime, Guid InfractionTypeId, string InfractionContent, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_OfficeInfractionId = _DossierSacrifice_OfficeInfractionId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.InfractionLocation = InfractionLocation;
            this.InfractionDate = InfractionDate;
            this.InfractionTime = InfractionTime;
            this.InfractionTypeId = InfractionTypeId;
            this.InfractionContent = InfractionContent;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
