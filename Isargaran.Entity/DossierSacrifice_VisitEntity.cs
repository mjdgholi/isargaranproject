﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/02/15>
    // Description:	<اطلاعات ملاقات>
    /// </summary>
   public class DossierSacrifice_VisitEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_VisitId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string VisitDate { get; set; }

        public Guid? OccasionId { get; set; }

        public string VisitLocation { get; set; }

        public string Visitors { get; set; }

        public bool HasRequest { get; set; }

        public string RequestContent { get; set; }

        public bool HasAction { get; set; }

        public string ActionDescription { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_VisitEntity()
        {
        }

        public DossierSacrifice_VisitEntity(Guid _DossierSacrifice_VisitId, Guid DossierSacrificeId, string VisitDate, Guid? OccasionId, string VisitLocation, string Visitors, bool HasRequest, string RequestContent, bool HasAction, string ActionDescription, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_VisitId = _DossierSacrifice_VisitId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.VisitDate = VisitDate;
            this.OccasionId = OccasionId;
            this.VisitLocation = VisitLocation;
            this.Visitors = Visitors;
            this.HasRequest = HasRequest;
            this.RequestContent = RequestContent;
            this.HasAction = HasAction;
            this.ActionDescription = ActionDescription;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
