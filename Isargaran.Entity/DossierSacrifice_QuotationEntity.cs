﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/02>
    /// Description: <نقل قول>
    /// </summary>
   public class DossierSacrifice_QuotationEntity
    {
                #region Properties :

        public Guid DossierSacrifice_QuotationId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string QuotationDate { get; set; }

        public string QuotationContent { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_QuotationEntity()
        {
        }

        public DossierSacrifice_QuotationEntity(Guid _DossierSacrifice_QuotationId, Guid DossierSacrificeId, string QuotationDate, string QuotationContent, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_QuotationId = _DossierSacrifice_QuotationId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.QuotationDate = QuotationDate;
            this.QuotationContent = QuotationContent;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
