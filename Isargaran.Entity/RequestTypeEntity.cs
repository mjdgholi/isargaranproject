﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع درخواست>
    /// </summary>
    public class RequestTypeEntity
    {
                #region Properties :

        public Guid RequestTypeId { get; set; }
        public string RequestTypeTitle { get; set; }

        public int Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public RequestTypeEntity()
        {
        }

        public RequestTypeEntity(Guid _RequestTypeId, string RequestTypeTitle, int Priority, string CreationDate, string ModificationDate, bool IsActive)
        {
            RequestTypeId = _RequestTypeId;
            this.RequestTypeTitle = RequestTypeTitle;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}