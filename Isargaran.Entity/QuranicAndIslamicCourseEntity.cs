﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <رشته قرانی و معارف>
    /// </summary>
   public class QuranicAndIslamicCourseEntity
    {
        
        #region Properties :

        public Guid QuranicAndIslamicCourseId { get; set; }
        public string QuranicAndIslamicCourseTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public QuranicAndIslamicCourseEntity()
        {
        }

        public QuranicAndIslamicCourseEntity(Guid _QuranicAndIslamicCourseId, string QuranicAndIslamicCourseTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            QuranicAndIslamicCourseId = _QuranicAndIslamicCourseId;
            this.QuranicAndIslamicCourseTitle = QuranicAndIslamicCourseTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
