﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <مهارت گفتاری>
    /// </summary>
    public class SpeakingSkillEntity
    {
        #region Properties :

        public Guid SpeakingSkillId { get; set; }
        public string SpeakingSkillPersianTitle { get; set; }

        public string SpeakingSkillEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public SpeakingSkillEntity()
        {
        }

        public SpeakingSkillEntity(Guid _SpeakingSkillId, string SpeakingSkillPersianTitle, string SpeakingSkillEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            SpeakingSkillId = _SpeakingSkillId;
            this.SpeakingSkillPersianTitle = SpeakingSkillPersianTitle;
            this.SpeakingSkillEnglishTitle = SpeakingSkillEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}