﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <نوع ایثارگری>
    /// </summary>
    public class SacrificeTypeEntity
    {
        #region Properties :

        public Guid SacrificeTypeId { get; set; }
        public string SacrificeTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SacrificeTypeEntity()
        {
        }

        public SacrificeTypeEntity(Guid _SacrificeTypeId, string SacrificeTypeTitle, string CreationDate,
                                   string ModificationDate, bool IsActive)
        {
            SacrificeTypeId = _SacrificeTypeId;
            this.SacrificeTypeTitle = SacrificeTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}

