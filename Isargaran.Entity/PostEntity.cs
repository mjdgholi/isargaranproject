﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <پست>
    /// </summary>
  public  class PostEntity
    {
        #region Properties :
        public Guid PostId { get; set; }
        public string PostTitle { get; set; }
        public string CreationDate { get; set; }
        public string ModificationDate { get; set; }
        public bool IsActive { get; set; }
        #endregion
        #region Constrauctors :
        public PostEntity()
        {
        }
        public PostEntity(Guid _PostId, string PostTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            PostId = _PostId;
            this.PostTitle = PostTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
        }
        #endregion
    }
}
