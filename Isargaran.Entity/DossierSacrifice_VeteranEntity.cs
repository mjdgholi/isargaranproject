﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/01/30>
    // Description:	<اطلاعات جانبازی>
    /// </summary>
   public class DossierSacrifice_VeteranEntity
    {
               #region Properties :

        public Guid DossierSacrifice_VeteranId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string VeteranCod { get; set; }

        public Guid? WarZoneId { get; set; }

        public string VeteranDate { get; set; }

        public Guid? VeteranBodyRegionId { get; set; }

        public decimal? VeteranPercent { get; set; }

        public int? DurationOfPresenceInBattlefield { get; set; }

        public string VeteranFoundationDossierNo { get; set; }

        public Guid? CityId { get; set; }

        public string RehabilitationEquipment { get; set; }

        public Guid? PreVeteranEucationDegreeId { get; set; }

        public Guid? EucationDegreeId { get; set; }

        public Guid? OrganizationPhysicalChartId { get; set; }

        public Guid? MilitaryUnitDispatcherId { get; set; }

        public Guid? PostId { get; set; }

        public string Description { get; set; }

        public Guid SacrificePeriodTimeId { get; set; }
        public string StartDate { get; set; }

        public string EndDate { get; set; }
        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public Guid? ProvinceId { get; set; }

        public string OrganizationPhysicalChartTitle { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_VeteranEntity()
        {
        }

        public DossierSacrifice_VeteranEntity(Guid _DossierSacrifice_VeteranId, Guid DossierSacrificeId, string VeteranCod, Guid? WarZoneId, string VeteranDate, Guid? VeteranBodyRegionId, decimal? VeteranPercent, int? DurationOfPresenceInBattlefield, string VeteranFoundationDossierNo, Guid? CityId, string RehabilitationEquipment, Guid? PreVeteranEucationDegreeId, Guid? EucationDegreeId, Guid? OrganizationPhysicalChartId, Guid? MilitaryUnitDispatcherId, Guid? PostId, string Description, Guid SacrificePeriodTimeId, string StartDate, string EndDate, string CreationDate, string ModificationDate, Guid? ProvinceId, string OrganizationPhysicalChartTitle)
        {
            DossierSacrifice_VeteranId = _DossierSacrifice_VeteranId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.VeteranCod = VeteranCod;
            this.WarZoneId = WarZoneId;
            this.VeteranDate = VeteranDate;
            this.VeteranBodyRegionId = VeteranBodyRegionId;
            this.VeteranPercent = VeteranPercent;
            this.DurationOfPresenceInBattlefield = DurationOfPresenceInBattlefield;
            this.VeteranFoundationDossierNo = VeteranFoundationDossierNo;
            this.CityId = CityId;
            this.RehabilitationEquipment = RehabilitationEquipment;
            this.PreVeteranEucationDegreeId = PreVeteranEucationDegreeId;
            this.EucationDegreeId = EucationDegreeId;
            this.OrganizationPhysicalChartId = OrganizationPhysicalChartId;
            this.MilitaryUnitDispatcherId = MilitaryUnitDispatcherId;
            this.PostId = PostId;
            this.Description = Description;
            this.SacrificePeriodTimeId = SacrificePeriodTimeId;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.ProvinceId = ProvinceId;
            this.OrganizationPhysicalChartTitle = OrganizationPhysicalChartTitle;

        }

        #endregion

    }
}
