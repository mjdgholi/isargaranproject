﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/03/11>
    /// Description: <دسته بندی فرم>
    /// </summary>
    public class WebFormCategorizeEntity
    {
         #region Properties :

        public Guid WebFormCategorizeId { get; set; }
        public string WebFormCategorizeTitle { get; set; }
        public int? Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public WebFormCategorizeEntity()
        {
        }

        public WebFormCategorizeEntity(Guid _WebFormCategorizeId, string WebFormCategorizeTitle, int? Priority, string CreationDate,
            string ModificationDate, bool IsActive)
        {
            WebFormCategorizeId = _WebFormCategorizeId;
            this.WebFormCategorizeTitle = WebFormCategorizeTitle;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}