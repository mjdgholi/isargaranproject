﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<اطلاعات گواهینامه رانندگی>
    /// </summary>
    public class DossierSacrifice_DrivingLicenseEntity
    {
        #region Properties :

        public Guid DossierSacrifice_DrivingLicenseId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public bool IsHasDrivingLicense { get; set; }

        public string CardNo { get; set; }

        public string CardIssueDate { get; set; }

        public string CardExpiredDate { get; set; }

        public Guid DrivingLicenseTypeId { get; set; }

        public Guid? DrivingRestrictionId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_DrivingLicenseEntity()
        {
        }

        public DossierSacrifice_DrivingLicenseEntity(Guid _DossierSacrifice_DrivingLicenseId, Guid DossierSacrificeId, bool IsHasDrivingLicense, string CardNo, string CardIssueDate, string CardExpiredDate, Guid DrivingLicenseTypeId, Guid? DrivingRestrictionId, string CreationDate, string ModificationDate, bool IsActive)
        {
            DossierSacrifice_DrivingLicenseId = _DossierSacrifice_DrivingLicenseId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.IsHasDrivingLicense = IsHasDrivingLicense;
            this.CardNo = CardNo;
            this.CardIssueDate = CardIssueDate;
            this.CardExpiredDate = CardExpiredDate;
            this.DrivingLicenseTypeId = DrivingLicenseTypeId;
            this.DrivingRestrictionId = DrivingRestrictionId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}