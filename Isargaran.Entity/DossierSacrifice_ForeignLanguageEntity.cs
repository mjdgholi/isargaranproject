﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <اطلاعات زبان های خارجی>
    /// </summary>
    public class DossierSacrifice_ForeignLanguageEntity
    {
        #region Properties :

        public Guid DossierSacrifice_ForeignLanguageId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid ForeignLanguageId { get; set; }

        public Guid? ListeningSkillId { get; set; }

        public Guid? SpeakingSkillId { get; set; }

        public Guid? ReadingSkillId { get; set; }

        public Guid? WritingSkillId { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        #endregion

        #region Constrauctors :

        public DossierSacrifice_ForeignLanguageEntity()
        {
        }

        public DossierSacrifice_ForeignLanguageEntity(Guid _DossierSacrifice_ForeignLanguageId, Guid DossierSacrificeId, Guid ForeignLanguageId, Guid? ListeningSkillId, Guid? SpeakingSkillId, Guid? ReadingSkillId, Guid? WritingSkillId, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_ForeignLanguageId = _DossierSacrifice_ForeignLanguageId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.ForeignLanguageId = ForeignLanguageId;
            this.ListeningSkillId = ListeningSkillId;
            this.SpeakingSkillId = SpeakingSkillId;
            this.ReadingSkillId = ReadingSkillId;
            this.WritingSkillId = WritingSkillId;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

    }
}