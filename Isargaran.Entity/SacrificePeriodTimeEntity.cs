﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/28>
    // Description:	<دوره زمانی ایثارگر>
    /// </summary>
   public class SacrificePeriodTimeEntity
    {
                #region Properties :

        public Guid SacrificePeriodTimeId { get; set; }
        public string SacrificePeriodTimeTitle { get; set; }

        public int Periority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SacrificePeriodTimeEntity()
        {
        }

        public SacrificePeriodTimeEntity(Guid _SacrificePeriodTimeId, string SacrificePeriodTimeTitle, int Periority, string CreationDate, string ModificationDate, bool IsActive)
        {
            SacrificePeriodTimeId = _SacrificePeriodTimeId;
            this.SacrificePeriodTimeTitle = SacrificePeriodTimeTitle;
            this.Periority = Periority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
