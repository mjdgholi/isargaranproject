﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: < نوع مراکز تحصیلی>
    /// </summary>
  public  class EducationCenterTypeEntity
    {
                #region Properties :

        public Guid EducationCenterTypeId { get; set; }
        public string EducationCenterTypePersianTitle { get; set; }

        public string EducationCenterTypeEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public EducationCenterTypeEntity()
        {
        }

        public EducationCenterTypeEntity(Guid _EducationCenterTypeId, string EducationCenterTypePersianTitle, string EducationCenterTypeEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            EducationCenterTypeId = _EducationCenterTypeId;
            this.EducationCenterTypePersianTitle = EducationCenterTypePersianTitle;
            this.EducationCenterTypeEnglishTitle = EducationCenterTypeEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
