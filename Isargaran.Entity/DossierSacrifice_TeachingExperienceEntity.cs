﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <اطلاعات سوابق تدریس>
    /// </summary>
  public  class DossierSacrifice_TeachingExperienceEntity
    {
                #region Properties :

        public Guid DossierSacrifice_TeachingExperienceId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid LessonId { get; set; }

        public int TeachingDurationYearNo { get; set; }

        public bool IsNowTeaching { get; set; }

        public string TeachingLocation { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_TeachingExperienceEntity()
        {
        }

        public DossierSacrifice_TeachingExperienceEntity(Guid _DossierSacrifice_TeachingExperienceId, Guid DossierSacrificeId, Guid LessonId, int TeachingDurationYearNo, bool IsNowTeaching, string TeachingLocation, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_TeachingExperienceId = _DossierSacrifice_TeachingExperienceId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.LessonId = LessonId;
            this.TeachingDurationYearNo = TeachingDurationYearNo;
            this.IsNowTeaching = IsNowTeaching;
            this.TeachingLocation = TeachingLocation;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
