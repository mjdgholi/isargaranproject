﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/02/16>
    // Description:	<داروی خاص>
    /// </summary>
  public  class SpecificMedicationEntity
    {
      
        #region Properties :

        public Guid SpecificMedicationId { get; set; }
        public string SpecificMedicationPersianTitle { get; set; }

        public string SpecificMedicationEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SpecificMedicationEntity()
        {
        }

        public SpecificMedicationEntity(Guid _SpecificMedicationId, string SpecificMedicationPersianTitle, string SpecificMedicationEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            SpecificMedicationId = _SpecificMedicationId;
            this.SpecificMedicationPersianTitle = SpecificMedicationPersianTitle;
            this.SpecificMedicationEnglishTitle = SpecificMedicationEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
