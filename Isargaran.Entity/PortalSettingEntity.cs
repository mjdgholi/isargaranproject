﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/14>
    // Description:	<تنظیمات پورتال>
    /// </summary>
    public class PortalSettingEntity
    {

        #region Properties :

        public Guid PortalSettingId { get; set; }
        public string PortalSettingPersianTitle { get; set; }

        public string PortalSettingEnglishTitle { get; set; }

        public string StyleTitle { get; set; }

        public string TelerikSkin { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }


        #endregion

        #region Constrauctors :

        public PortalSettingEntity()
        {
        }

        public PortalSettingEntity(Guid _PortalSettingId, string PortalSettingPersianTitle,
            string PortalSettingEnglishTitle, string StyleTitle, string TelerikSkin, string CreationDate,
            string ModificationDate, bool IsActive)
        {
            PortalSettingId = _PortalSettingId;
            this.PortalSettingPersianTitle = PortalSettingPersianTitle;
            this.PortalSettingEnglishTitle = PortalSettingEnglishTitle;
            this.StyleTitle = StyleTitle;
            this.TelerikSkin = TelerikSkin;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
        }

        #endregion
    }
}