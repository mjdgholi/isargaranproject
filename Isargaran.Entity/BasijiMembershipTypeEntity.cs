﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <نوع عضویت در بسیج>
    /// </summary>
   public class BasijiMembershipTypeEntity
    {
             #region Properties :

        public Guid BasijiMembershipTypeId { get; set; }
        public string BasijiMembershipTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public BasijiMembershipTypeEntity()
        {
        }

        public BasijiMembershipTypeEntity(Guid _BasijiMembershipTypeId, string BasijiMembershipTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            BasijiMembershipTypeId = _BasijiMembershipTypeId;
            this.BasijiMembershipTypeTitle = BasijiMembershipTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
