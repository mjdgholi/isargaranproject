﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/18>
    /// Description: <پرونده مشخصات محل اقامت ایثارگری>
    /// </summary>
   public class DossierSacrifice_HabitatEntity
    {
           #region Properties :

        public Guid DossierSacrifice_HabitatId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid HabitatOwnershipStatusId { get; set; }

        public Guid? HabitatEStateTypeId { get; set; }

        public Guid? HabitatLocationTypeId { get; set; }

        public Guid? HabitatQualityId { get; set; }

        public bool? IsBasicRepairNeed { get; set; }

        public decimal? AmountOfRent { get; set; }

        public decimal? HbitatArea { get; set; }

        public decimal? HabitatDeposit { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_HabitatEntity()
        {
        }

        public DossierSacrifice_HabitatEntity(Guid _DossierSacrifice_HabitatId, Guid DossierSacrificeId, Guid HabitatOwnershipStatusId, Guid? HabitatEStateTypeId, Guid? HabitatLocationTypeId, Guid? HabitatQualityId, bool? IsBasicRepairNeed, decimal? AmountOfRent, decimal? HbitatArea, decimal? HabitatDeposit, string StartDate, string EndDate, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_HabitatId = _DossierSacrifice_HabitatId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.HabitatOwnershipStatusId = HabitatOwnershipStatusId;
            this.HabitatEStateTypeId = HabitatEStateTypeId;
            this.HabitatLocationTypeId = HabitatLocationTypeId;
            this.HabitatQualityId = HabitatQualityId;
            this.IsBasicRepairNeed = IsBasicRepairNeed;
            this.AmountOfRent = AmountOfRent;
            this.HbitatArea = HbitatArea;
            this.HabitatDeposit = HabitatDeposit;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
