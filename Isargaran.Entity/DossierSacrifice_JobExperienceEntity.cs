﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <پرونده مشخصات سوابق کاری>
    /// </summary>   
 public   class DossierSacrifice_JobExperienceEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_JobExperienceId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid? OrganizationPhysicalChartId { get; set; }

        public Guid? EmploymentTypeId { get; set; }

        public string EmploymentDate { get; set; }

        public Guid? PostId { get; set; }

        public string PersonnelNo { get; set; }

        public string CommandmentNo { get; set; }

        public string FreeJob { get; set; }

        public Guid? DurationTimeAtWorkId { get; set; }

        public string DirectManager { get; set; }

        public Guid? CityId { get; set; }

        public string TelNo { get; set; }

        public string FaxNo { get; set; }

        public string Address { get; set; }

        public string PostalCod { get; set; }

        public string Email { get; set; }

        public string WebAddress { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public Guid? ProvinceId { get; set; }

        public string OrganizationPhysicalChartTitle { get; set; }
        #endregion

        #region Constrauctors :

        public DossierSacrifice_JobExperienceEntity()
        {
        }

        public DossierSacrifice_JobExperienceEntity(Guid _DossierSacrifice_JobExperienceId, Guid DossierSacrificeId, Guid? OrganizationPhysicalChartId, Guid? EmploymentTypeId, string EmploymentDate, Guid? PostId, string PersonnelNo, string CommandmentNo, string FreeJob, Guid? DurationTimeAtWorkId, string DirectManager, Guid? CityId, string TelNo, string FaxNo, string Address, string PostalCod, string Email, string WebAddress, string StartDate, string EndDate, string CreationDate, string ModificationDate, Guid? ProvinceId, string OrganizationPhysicalChartTitle)
        {
            DossierSacrifice_JobExperienceId = _DossierSacrifice_JobExperienceId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.OrganizationPhysicalChartId = OrganizationPhysicalChartId;
            this.EmploymentTypeId = EmploymentTypeId;
            this.EmploymentDate = EmploymentDate;
            this.PostId = PostId;
            this.PersonnelNo = PersonnelNo;
            this.CommandmentNo = CommandmentNo;
            this.FreeJob = FreeJob;
            this.DurationTimeAtWorkId = DurationTimeAtWorkId;
            this.DirectManager = DirectManager;
            this.CityId = CityId;
            this.TelNo = TelNo;
            this.FaxNo = FaxNo;
            this.Address = Address;
            this.PostalCod = PostalCod;
            this.Email = Email;
            this.WebAddress = WebAddress;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.ProvinceId = ProvinceId;
            this.OrganizationPhysicalChartTitle = OrganizationPhysicalChartTitle;
        }

        #endregion
    }
}
