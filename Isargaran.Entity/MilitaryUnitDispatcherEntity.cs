﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/23>
    // Description:	<یگان اعزام کننده>
    /// </summary>
    public class MilitaryUnitDispatcherEntity
    {
        #region Properties :

        public Guid MilitaryUnitDispatcherId { get; set; }
        public string MilitaryUnitDispatcherTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public MilitaryUnitDispatcherEntity()
        {
        }

        public MilitaryUnitDispatcherEntity(Guid _MilitaryUnitDispatcherId, string MilitaryUnitDispatcherTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            MilitaryUnitDispatcherId = _MilitaryUnitDispatcherId;
            this.MilitaryUnitDispatcherTitle = MilitaryUnitDispatcherTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}