﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/26>
    /// Description: <وضیعت سربازی>
    /// </summary>
   public class MilitaryStatusEntity
    {
        
        #region Properties :

        public Guid MilitaryStatusId { get; set; }
        public string MilitaryStatusTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public MilitaryStatusEntity()
        {
        }

        public MilitaryStatusEntity(Guid _MilitaryStatusId, string MilitaryStatusTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            MilitaryStatusId = _MilitaryStatusId;
            this.MilitaryStatusTitle = MilitaryStatusTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
