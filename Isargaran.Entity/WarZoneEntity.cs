﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/28>
    // Description:	<مناطق جنگی>
    /// </summary>
  public  class WarZoneEntity
    {
       #region Properties :

        public Guid WarZoneId { get; set; }
        public string WarZoneTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public WarZoneEntity()
        {
        }

        public WarZoneEntity(Guid _WarZoneId, string WarZoneTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            WarZoneId = _WarZoneId;
            this.WarZoneTitle = WarZoneTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
