﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{

    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<محدودیت رانندگی>
    /// </summary>
    public class DrivingRestrictionEntity
    {
        #region Properties :

        public Guid DrivingRestrictionId { get; set; }
        public string DrivingRestrictionTitlePersian { get; set; }

        public string DrivingRestrictionTitleEnglish { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public DrivingRestrictionEntity()
        {
        }

        public DrivingRestrictionEntity(Guid _DrivingRestrictionId, string DrivingRestrictionTitlePersian, string DrivingRestrictionTitleEnglish, string CreationDate, string ModificationDate, bool IsActive)
        {
            DrivingRestrictionId = _DrivingRestrictionId;
            this.DrivingRestrictionTitlePersian = DrivingRestrictionTitlePersian;
            this.DrivingRestrictionTitleEnglish = DrivingRestrictionTitleEnglish;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
        }

        #endregion

    }
}