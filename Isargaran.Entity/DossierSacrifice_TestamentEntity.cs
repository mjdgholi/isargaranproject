﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/31>
    /// Description: <اطلاعات وصیت ایثارگر>
    /// </summary>
   public class DossierSacrifice_TestamentEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_TestamentId { get; set; }
        public bool? IsHasTestament { get; set; }

        public string TestamentDate { get; set; }

        public string TestamentContent { get; set; }

        public Guid DossierSacrificeId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        



        #endregion

        #region Constrauctors :

        public DossierSacrifice_TestamentEntity()
        {
        }

        public DossierSacrifice_TestamentEntity(Guid _DossierSacrifice_TestamentId, bool? IsHasTestament, string TestamentDate, string TestamentContent, Guid DossierSacrificeId, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_TestamentId = _DossierSacrifice_TestamentId;
            this.IsHasTestament = IsHasTestament;
            this.TestamentDate = TestamentDate;
            this.TestamentContent = TestamentContent;
            this.DossierSacrificeId = DossierSacrificeId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;  

        }

        #endregion
    }
}
