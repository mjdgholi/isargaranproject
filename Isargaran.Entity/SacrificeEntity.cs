﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <نوع ایثارگری>
    /// </summary>
   public class SacrificeEntity
    {
        
        #region Properties :

        public Guid SacrificeId { get; set; }
        public string SacrificeTitle { get; set; }

        public Guid SacrificeTypeId { get; set; }

        public bool IsPercentValue { get; set; }

        public bool IsTimeValue { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SacrificeEntity()
        {
        }

        public SacrificeEntity(Guid _SacrificeId, string SacrificeTitle, Guid SacrificeTypeId, bool IsPercentValue, bool IsTimeValue, string CreationDate, string ModificationDate, bool IsActive)
        {
            SacrificeId = _SacrificeId;
            this.SacrificeTitle = SacrificeTitle;
            this.SacrificeTypeId = SacrificeTypeId;
            this.IsPercentValue = IsPercentValue;
            this.IsTimeValue = IsTimeValue;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
