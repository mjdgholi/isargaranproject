﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/10>
    /// Description: <پرونده  ااطلاعات مهارت علمی و فنی ایثارگری>
    /// </summary>
   public class DossierSacrifice_ScientificAndTechnicalSkillEntity
    {
                         #region Properties :

            public Guid DossierSacrifice_ScientificAndTechnicalSkillId { get; set; }
            public Guid DossierSacrificeId { get; set; }

            public string ScientificAndTechnicalSkillTitle { get; set; }

            public int? ExperienceYearNo { get; set; }

            public bool? HasEvidence { get; set; }

            public string PlaceOfGraduation { get; set; }

            public string Description { get; set; }

            public string CreationDate { get; set; }

            public string ModificationDate { get; set; }



            #endregion

            #region Constrauctors :

            public DossierSacrifice_ScientificAndTechnicalSkillEntity()
            {
            }

            public DossierSacrifice_ScientificAndTechnicalSkillEntity(Guid _DossierSacrifice_ScientificAndTechnicalSkillId, Guid DossierSacrificeId, string ScientificAndTechnicalSkillTitle, int? ExperienceYearNo, bool? HasEvidence, string PlaceOfGraduation, string Description, string CreationDate, string ModificationDate)
            {
                DossierSacrifice_ScientificAndTechnicalSkillId = _DossierSacrifice_ScientificAndTechnicalSkillId;
                this.DossierSacrificeId = DossierSacrificeId;
                this.ScientificAndTechnicalSkillTitle = ScientificAndTechnicalSkillTitle;
                this.ExperienceYearNo = ExperienceYearNo;
                this.HasEvidence = HasEvidence;
                this.PlaceOfGraduation = PlaceOfGraduation;
                this.Description = Description;
                this.CreationDate = CreationDate;
                this.ModificationDate = ModificationDate;

            }

            #endregion
    }
}
