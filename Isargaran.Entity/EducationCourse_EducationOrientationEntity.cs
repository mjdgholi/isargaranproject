﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/12>
    /// Description: <رشته تحصیلی -گرایش تحصیلی >
    /// </summary>
  public  class EducationCourse_EducationOrientationEntity
    {
                #region Properties :

        public Guid EducationCourse_EducationOrientationId { get; set; }
        public Guid? EducationCourseId { get; set; }

        public Guid EducationOreintionId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }





        #endregion

        #region Constrauctors :

        public EducationCourse_EducationOrientationEntity()
        {
        }

        public EducationCourse_EducationOrientationEntity(Guid _EducationCourse_EducationOrientationId, Guid? EducationCourseId, Guid EducationOreintionId, string CreationDate, string ModificationDate, bool IsActive)
        {
            EducationCourse_EducationOrientationId = _EducationCourse_EducationOrientationId;
            this.EducationCourseId = EducationCourseId;
            this.EducationOreintionId = EducationOreintionId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
