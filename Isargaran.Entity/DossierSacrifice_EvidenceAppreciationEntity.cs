﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات تقدیر کتبی>
    /// </summary>
  public  class DossierSacrifice_EvidenceAppreciationEntity
    {
        
        #region Properties :

        public Guid DossierSacrifice_EvidenceAppreciationId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid OccasionId { get; set; }

        public string OccasionDate { get; set; }

        public string EvidenceAppreciationContent { get; set; }

        public string EvidenceAppreciationDescription { get; set; }

        public bool IsForeignissued { get; set; }

        public Guid? OrganizationId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_EvidenceAppreciationEntity()
        {
        }

        public DossierSacrifice_EvidenceAppreciationEntity(Guid _DossierSacrifice_EvidenceAppreciationId, Guid DossierSacrificeId, Guid OccasionId, string OccasionDate, string EvidenceAppreciationContent, string EvidenceAppreciationDescription, bool IsForeignissued, Guid? OrganizationId, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_EvidenceAppreciationId = _DossierSacrifice_EvidenceAppreciationId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.OccasionId = OccasionId;
            this.OccasionDate = OccasionDate;
            this.EvidenceAppreciationContent = EvidenceAppreciationContent;
            this.EvidenceAppreciationDescription = EvidenceAppreciationDescription;
            this.IsForeignissued = IsForeignissued;
            this.OrganizationId = OrganizationId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
