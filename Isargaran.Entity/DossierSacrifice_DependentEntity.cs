﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/17>
    /// Description: <پرونده مشخصات وابستگان ایثارگری>
    /// </summary> 
    public class DossierSacrifice_DependentEntity
    {
        #region Properties :

        public Guid DossierSacrifice_DependentId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid PersonId { get; set; }

        public Guid DependentTypeId { get; set; }

        public string FromDate { get; set; }

        public string Todate { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public string FullName { get; set; }

        #endregion

        #region Constrauctors :

        public DossierSacrifice_DependentEntity()
        {
        }

        public DossierSacrifice_DependentEntity(Guid _DossierSacrifice_DependentId, Guid DossierSacrificeId,
            Guid PersonId, Guid DependentTypeId, string FromDate, string Todate, string CreationDate,
            string ModificationDate,string FullName)
        {
            DossierSacrifice_DependentId = _DossierSacrifice_DependentId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.PersonId = PersonId;
            this.DependentTypeId = DependentTypeId;
            this.FromDate = FromDate;
            this.Todate = Todate;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.FullName = FullName;

        }

        #endregion
    }
}
