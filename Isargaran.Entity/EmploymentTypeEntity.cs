﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <نوع استخدام>
    /// </summary>
  public  class EmploymentTypeEntity
    {
                #region Properties :

        public Guid EmploymentTypeId { get; set; }
        public string EmploymentTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public EmploymentTypeEntity()
        {
        }

        public EmploymentTypeEntity(Guid _EmploymentTypeId, string EmploymentTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            EmploymentTypeId = _EmploymentTypeId;
            this.EmploymentTypeTitle = EmploymentTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
