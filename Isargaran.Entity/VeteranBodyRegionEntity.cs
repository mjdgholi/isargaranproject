﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/27>
    // Description:	<فرم ناحیه جانبازی بدن>
    /// </summary>
    public class VeteranBodyRegionEntity
    {
        #region Properties :

        public Guid VeteranBodyRegionId { get; set; }
        public string VeteranBodyRegionTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public VeteranBodyRegionEntity()
        {
        }

        public VeteranBodyRegionEntity(Guid _VeteranBodyRegionId, string VeteranBodyRegionTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            VeteranBodyRegionId = _VeteranBodyRegionId;
            this.VeteranBodyRegionTitle = VeteranBodyRegionTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}