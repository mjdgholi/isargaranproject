﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<نوع سازه محل اقامت >
    /// </summary>
   public class HabitatEstateTypeEntity
    {
              #region Properties :

        public Guid HabitatEstateTypeId { get; set; }
        public string HabitatEstateTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public HabitatEstateTypeEntity()
        {
        }

        public HabitatEstateTypeEntity(Guid _HabitatEstateTypeId, string HabitatEstateTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            HabitatEstateTypeId = _HabitatEstateTypeId;
            this.HabitatEstateTypeTitle = HabitatEstateTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
