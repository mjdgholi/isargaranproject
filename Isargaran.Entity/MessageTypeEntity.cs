﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/25>
    /// Description: <نوع پیغام>
    /// </summary>
    public class MessageTypeEntity
    {
                 #region Properties :

        public Guid MessageTypeId { get; set; }
        public string MessageTypeTitle { get; set; }

        public bool IsInpout { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public MessageTypeEntity()
        {
        }

        public MessageTypeEntity(Guid _MessageTypeId, string MessageTypeTitle, bool IsInpout, string CreationDate, string ModificationDate, bool IsActive)
        {
            MessageTypeId = _MessageTypeId;
            this.MessageTypeTitle = MessageTypeTitle;
            this.IsInpout = IsInpout;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}