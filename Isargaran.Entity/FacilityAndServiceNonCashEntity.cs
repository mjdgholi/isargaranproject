﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/22>
    /// Description: <خدمات و تسهیلات  غیرنقدی>
    /// </summary>
  public  class FacilityAndServiceNonCashEntity
    {
                #region Properties :

        public Guid FacilityAndServiceNonCashId { get; set; }
        public string FacilityAndServiceNonCashTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public FacilityAndServiceNonCashEntity()
        {
        }

        public FacilityAndServiceNonCashEntity(Guid _FacilityAndServiceNonCashId, string FacilityAndServiceNonCashTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            FacilityAndServiceNonCashId = _FacilityAndServiceNonCashId;
            this.FacilityAndServiceNonCashTitle = FacilityAndServiceNonCashTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
