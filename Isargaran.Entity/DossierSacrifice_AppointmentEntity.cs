﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/16>
    /// Description: < اطلاعات ملاقات>
    /// </summary>
  public class DossierSacrifice_AppointmentEntity
    {
                #region Properties :

        public Guid DossierSacrifice_AppointmentId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string AppointmentDate { get; set; }

        public Guid AppointmentTypeId { get; set; }

        public string AppointmentPerson { get; set; }

        public Guid? DependentTypeId { get; set; }

        public bool HasRequest { get; set; }

        public string RequestContent { get; set; }

        public bool HasAction { get; set; }

        public string ActionDescription { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_AppointmentEntity()
        {
        }

        public DossierSacrifice_AppointmentEntity(Guid _DossierSacrifice_AppointmentId, Guid DossierSacrificeId, string AppointmentDate, Guid AppointmentTypeId, string AppointmentPerson, Guid? DependentTypeId, bool HasRequest, string RequestContent, bool HasAction, string ActionDescription, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_AppointmentId = _DossierSacrifice_AppointmentId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.AppointmentDate = AppointmentDate;
            this.AppointmentTypeId = AppointmentTypeId;
            this.AppointmentPerson = AppointmentPerson;
            this.DependentTypeId = DependentTypeId;
            this.HasRequest = HasRequest;
            this.RequestContent = RequestContent;
            this.HasAction = HasAction;
            this.ActionDescription = ActionDescription;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
