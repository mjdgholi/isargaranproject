﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <نوع تحصیل>
    /// </summary>
    public class EducationModeEntity
    {
        #region Properties :

        public Guid EducationModeId { get; set; }

        public string EducationModePersianTitle { get; set; }

        public string EducationModeEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public EducationModeEntity()
        {
        }

        public EducationModeEntity(Guid _EducationModeId, string EducationModePersianTitle, string EducationModeEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            EducationModeId = _EducationModeId;
            this.EducationModePersianTitle = EducationModePersianTitle;
            this.EducationModeEnglishTitle = EducationModeEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}