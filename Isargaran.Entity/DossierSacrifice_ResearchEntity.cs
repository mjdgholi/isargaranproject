﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/14>
    /// Description: <پرونده   اطلاعات پژوهشی ایثارگری>
    /// </summary>
   public class DossierSacrifice_ResearchEntity
    {
                #region Properties :

        public Guid DossierSacrifice_ResearchId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string ResearchTitle { get; set; }

        public string ResearchSubject { get; set; }

        public string ApplicationsOfResearch { get; set; }

        public string StartDate { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_ResearchEntity()
        {
        }

        public DossierSacrifice_ResearchEntity(Guid _DossierSacrifice_ResearchId, Guid DossierSacrificeId, string ResearchTitle, string ResearchSubject, string ApplicationsOfResearch, string StartDate, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_ResearchId = _DossierSacrifice_ResearchId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.ResearchTitle = ResearchTitle;
            this.ResearchSubject = ResearchSubject;
            this.ApplicationsOfResearch = ApplicationsOfResearch;
            this.StartDate = StartDate;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

    }
}
