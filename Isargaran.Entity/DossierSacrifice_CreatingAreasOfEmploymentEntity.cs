﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/02/14>
    // Description:	<اطلاعات ایجاد زمینه اشتغال>
    /// </summary>
    public class DossierSacrifice_CreatingAreasOfEmploymentEntity
    {
        #region Properties :

        public Guid DossierSacrifice_CreatingAreasOfEmploymentId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid EmployedPersonId { get; set; }

        public string EmploymentDate { get; set; }

        public Guid OrganizationId { get; set; }

        public Guid? DependentTypeId { get; set; }

        public bool IsItWorking { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_CreatingAreasOfEmploymentEntity()
        {
        }

        public DossierSacrifice_CreatingAreasOfEmploymentEntity(Guid _DossierSacrifice_CreatingAreasOfEmploymentId, Guid DossierSacrificeId, Guid EmployedPersonId, string EmploymentDate, Guid OrganizationId, Guid? DependentTypeId, bool IsItWorking, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_CreatingAreasOfEmploymentId = _DossierSacrifice_CreatingAreasOfEmploymentId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.EmployedPersonId = EmployedPersonId;
            this.EmploymentDate = EmploymentDate;
            this.OrganizationId = OrganizationId;
            this.DependentTypeId = DependentTypeId;
            this.IsItWorking = IsItWorking;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

    }
}