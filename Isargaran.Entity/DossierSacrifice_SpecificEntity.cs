﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/27>
    /// Description: <اطلاعات خاص>
    /// </summary>
  public  class DossierSacrifice_SpecificEntity
    {
      
        #region Properties :

        public Guid DossierSacrifice_SpecificId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public string Email { get; set; }

        public string WebAddress { get; set; }

        public string Description { get; set; }

        public Guid BankId { get; set; }

        public string BankBranchCode { get; set; }

        public string BankBranchTitle { get; set; }

        public string AccountNo { get; set; }

        public string OpeningAccountDate { get; set; }

        public bool IsHasSupplementaryInsurance { get; set; }

        public string SupplementaryInsuranceNo { get; set; }

        public bool IsAccountActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_SpecificEntity()
        {
        }

        public DossierSacrifice_SpecificEntity(Guid _DossierSacrifice_SpecificId, Guid DossierSacrificeId, string Email, string WebAddress, string Description, Guid BankId, string BankBranchCode, string BankBranchTitle, string AccountNo, string OpeningAccountDate, bool IsHasSupplementaryInsurance, string SupplementaryInsuranceNo, bool IsAccountActive, string CreationDate, string ModificationDate, bool IsActive)
        {
            DossierSacrifice_SpecificId = _DossierSacrifice_SpecificId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.Email = Email;
            this.WebAddress = WebAddress;
            this.Description = Description;
            this.BankId = BankId;
            this.BankBranchCode = BankBranchCode;
            this.BankBranchTitle = BankBranchTitle;
            this.AccountNo = AccountNo;
            this.OpeningAccountDate = OpeningAccountDate;
            this.IsHasSupplementaryInsurance = IsHasSupplementaryInsurance;
            this.SupplementaryInsuranceNo = SupplementaryInsuranceNo;
            this.IsAccountActive = IsAccountActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
