﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/18>
    // Description:	<محل مسافرت>
    /// </summary>
  public  class TripLocationEntity
    {
                #region Properties :

        public Guid TripLocationId { get; set; }
        public string TripLocationTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public TripLocationEntity()
        {
        }

        public TripLocationEntity(Guid _TripLocationId, string TripLocationTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            TripLocationId = _TripLocationId;
            this.TripLocationTitle = TripLocationTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
