﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/16>
    /// Description: <نوع مراجعه>
    /// </summary>
   public class AppointmentTypeEntity
    {
                #region Properties :

        public Guid AppointmentTypeId { get; set; }
        public string AppointmentTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public AppointmentTypeEntity()
        {
        }

        public AppointmentTypeEntity(Guid _AppointmentTypeId, string AppointmentTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            AppointmentTypeId = _AppointmentTypeId;
            this.AppointmentTypeTitle = AppointmentTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
