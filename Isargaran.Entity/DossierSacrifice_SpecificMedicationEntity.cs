﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/17>
    /// Description: <اطلاعات مصرف داروهای خاص>
    /// </summary>
    public class DossierSacrifice_SpecificMedicationEntity
    {
         #region Properties :

        public Guid DossierSacrifice_SpecificMedicationId { get; set; }
        public Guid DossierSacrificeId { get; set; }

        public Guid SpecificMedicationId { get; set; }

        public Guid? CountryId { get; set; }

        public string MedicationConsum { get; set; }

        public int? MedicationNo { get; set; }

        public decimal? MedicationPrize { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public bool IsMedicationFinished { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public DossierSacrifice_SpecificMedicationEntity()
        {
        }

        public DossierSacrifice_SpecificMedicationEntity(Guid _DossierSacrifice_SpecificMedicationId, Guid DossierSacrificeId, Guid SpecificMedicationId, Guid? CountryId, string MedicationConsum, int? MedicationNo, decimal? MedicationPrize, string StartDate, string EndDate, bool IsMedicationFinished, string Description, string CreationDate, string ModificationDate)
        {
            DossierSacrifice_SpecificMedicationId = _DossierSacrifice_SpecificMedicationId;
            this.DossierSacrificeId = DossierSacrificeId;
            this.SpecificMedicationId = SpecificMedicationId;
            this.CountryId = CountryId;
            this.MedicationConsum = MedicationConsum;
            this.MedicationNo = MedicationNo;
            this.MedicationPrize = MedicationPrize;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.IsMedicationFinished = IsMedicationFinished;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}