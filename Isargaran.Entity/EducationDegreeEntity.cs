﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <  مقطع تحصیلی>
    /// </summary>
   public class EducationDegreeEntity
    {
        
        #region Properties :

        public Guid EucationDegreeId { get; set; }
        public Guid? EducationSystemId { get; set; }

        public string EucationDegreePersianTitle { get; set; }

        public string EucationDegreeEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public EducationDegreeEntity()
        {
        }

        public EducationDegreeEntity(Guid _EucationDegreeId, Guid? EducationSystemId, string EucationDegreePersianTitle, string EucationDegreeEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            EucationDegreeId = _EucationDegreeId;
            this.EducationSystemId = EducationSystemId;
            this.EucationDegreePersianTitle = EucationDegreePersianTitle;
            this.EucationDegreeEnglishTitle = EucationDegreeEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
