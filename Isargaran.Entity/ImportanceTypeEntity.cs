﻿using System;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع اهمیت>
    /// </summary>
    public class ImportanceTypeEntity
    {
                 #region Properties :

        public Guid ImportanceTypeId { get; set; }
        public string ImportanceTypeTitle { get; set; }

        public int Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ImportanceTypeEntity()
        {
        }

        public ImportanceTypeEntity(Guid _ImportanceTypeId, string ImportanceTypeTitle, int Priority, string CreationDate, string ModificationDate, bool IsActive)
        {
            ImportanceTypeId = _ImportanceTypeId;
            this.ImportanceTypeTitle = ImportanceTypeTitle;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}