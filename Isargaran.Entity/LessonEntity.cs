﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <مشخصات درس>
    /// </summary>
  public  class LessonEntity
    {
                #region Properties :

        public Guid LessonId { get; set; }
        public string LessonTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public LessonEntity()
        {
        }

        public LessonEntity(Guid _LessonId, string LessonTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            LessonId = _LessonId;
            this.LessonTitle = LessonTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
