﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/12>
    /// Description: <گرایش تحصیلی>
    /// </summary>
   public class EducationOrientationEntity
    {
        
              #region Properties :

        public Guid EducationOrientationId { get; set; }


        public string EducationOrientationPersianTitle { get; set; }

        public string EducationOrientationEnglishTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public EducationOrientationEntity()
        {
        }

        public EducationOrientationEntity(Guid _EducationOrientationId, string EducationOrientationPersianTitle, string EducationOrientationEnglishTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            EducationOrientationId = _EducationOrientationId;

            this.EducationOrientationPersianTitle = EducationOrientationPersianTitle;
            this.EducationOrientationEnglishTitle = EducationOrientationEnglishTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
