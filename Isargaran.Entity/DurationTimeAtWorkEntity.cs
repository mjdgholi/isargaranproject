﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <میزان حضور در سرکار>
    /// </summary>
   public class DurationTimeAtWorkEntity
    {
                #region Properties :

        public Guid DurationTimeAtWorkId { get; set; }
        public string DurationTimeAtWorkTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DurationTimeAtWorkEntity()
        {
        }

        public DurationTimeAtWorkEntity(Guid _DurationTimeAtWorkId, string DurationTimeAtWorkTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            DurationTimeAtWorkId = _DurationTimeAtWorkId;
            this.DurationTimeAtWorkTitle = DurationTimeAtWorkTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
