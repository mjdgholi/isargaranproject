﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <نوع استخدام>
    /// </summary>

    //-----------------------------------------------------
    #region Class "EmploymentTypeDB"

    public class EmploymentTypeDB
    {



        #region Methods :

        public void AddEmploymentTypeDB(EmploymentTypeEntity EmploymentTypeEntityParam, out Guid EmploymentTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EmploymentTypeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(EmploymentTypeEntityParam.EmploymentTypeTitle.Trim());
            parameters[2].Value = EmploymentTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EmploymentTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EmploymentTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEmploymentTypeDB(EmploymentTypeEntity EmploymentTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EmploymentTypeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EmploymentTypeEntityParam.EmploymentTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(EmploymentTypeEntityParam.EmploymentTypeTitle.Trim());
            parameters[2].Value = EmploymentTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EmploymentTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEmploymentTypeDB(EmploymentTypeEntity EmploymentTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EmploymentTypeEntityParam.EmploymentTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EmploymentTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EmploymentTypeEntity GetSingleEmploymentTypeDB(EmploymentTypeEntity EmploymentTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EmploymentTypeEntityParam.EmploymentTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EmploymentTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEmploymentTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EmploymentTypeEntity> GetAllEmploymentTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEmploymentTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EmploymentTypeGetAll", new IDataParameter[] { }));
        }
        public List<EmploymentTypeEntity> GetAllEmploymentTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEmploymentTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EmploymentTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<EmploymentTypeEntity> GetPageEmploymentTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EmploymentType";
            parameters[5].Value = "EmploymentTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEmploymentTypeDBCollectionFromDataSet(ds, out count);
        }

        public EmploymentTypeEntity GetEmploymentTypeDBFromDataReader(IDataReader reader)
        {
            return new EmploymentTypeEntity(Guid.Parse(reader["EmploymentTypeId"].ToString()),
                                    reader["EmploymentTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EmploymentTypeEntity> GetEmploymentTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EmploymentTypeEntity> lst = new List<EmploymentTypeEntity>();
                while (reader.Read())
                    lst.Add(GetEmploymentTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EmploymentTypeEntity> GetEmploymentTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEmploymentTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
