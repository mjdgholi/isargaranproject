﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/25>
    /// Description: <مدیریت پیغام>
    /// </summary>

    //-----------------------------------------------------
    #region Class "MessageDB"

    public class MessageDB
    {


        #region Methods :

        public void AddMessageDB(MessageEntity messageEntityParam, out Guid MessageId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MessageId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,-1) ,											  										  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = messageEntityParam.DossierSacrifice_RequestId;
            parameters[2].Value = messageEntityParam.MessageTypeId;
            parameters[3].Value = FarsiToArabic.ToArabic(messageEntityParam.MessageText.Trim());            

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_MessageAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MessageId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMessageDB(MessageEntity MessageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MessageId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MessageText", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsViewed", SqlDbType.Bit) ,
											  new SqlParameter("@ViewedDate", SqlDbType.DateTime) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = MessageEntityParam.MessageId;
            parameters[1].Value = MessageEntityParam.DossierSacrifice_RequestId;
            parameters[2].Value = MessageEntityParam.MessageTypeId;
            parameters[3].Value = MessageEntityParam.MessageText;
            parameters[4].Value = MessageEntityParam.IsViewed;
            parameters[5].Value = MessageEntityParam.ViewedDate;
            

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_MessageUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMessageDB(MessageEntity MessageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MessageId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MessageEntityParam.MessageId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_MessageDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MessageEntity GetSingleMessageDB(MessageEntity MessageEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@MessageId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = MessageEntityParam.MessageId;

                reader = _intranetDB.RunProcedureReader("isar.p_MessageGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMessageDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessageEntity> GetAllMessageDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMessageDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_MessageGetAll", new IDataParameter[] { }));
        }

        public List<MessageEntity> GetPageMessageDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Message";
            parameters[5].Value = "MessageId";
            DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
            return GetMessageDBCollectionFromDataSet(ds, out count);
        }

        public MessageEntity GetMessageDBFromDataReader(IDataReader reader)
        {
            return new MessageEntity(Guid.Parse(reader["MessageId"].ToString()),
                                    Guid.Parse(reader["DossierSacrifice_RequestId"].ToString()),
                                    Guid.Parse(reader["MessageTypeId"].ToString()),
                                    reader["MessageText"].ToString(),
                                    bool.Parse(reader["IsViewed"].ToString()),
                                    reader["ViewedDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<MessageEntity> GetMessageDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MessageEntity> lst = new List<MessageEntity>();
                while (reader.Read())
                    lst.Add(GetMessageDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessageEntity> GetMessageDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMessageDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<MessageEntity> GetMessageDBCollectionByMessageTypeIdDB(MessageEntity messageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MessageTypeId", SqlDbType.Int);
            parameter.Value = messageEntityParam.MessageTypeId;
            return GetMessageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_MessageGetByMessageTypeId", new[] { parameter }));
        }

        public List<MessageEntity> GetMessageDBCollectionByDossierSacrifice_RequestDB(MessageEntity messageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.Int);
            parameter.Value = messageEntityParam.DossierSacrifice_RequestId;
            return GetMessageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_MessageGetByDossierSacrifice_Request", new[] { parameter }));
        }


        #endregion

        public void UpdateIsViewDB(MessageEntity MessageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MessageId", SqlDbType.UniqueIdentifier),						 											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = MessageEntityParam.MessageId;                        


            parameters[1].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_MessageIsViewUpdate", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }
    }

    #endregion
}