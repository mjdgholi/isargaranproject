﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/21>
    // Description:	<اطلاعات  اطلاعات خدمات و تسهیلات نقدی>
    /// </summary>


    public class DossierSacrifice_FacilityAndServiceCashDB
    {



        public void AddDossierSacrifice_FacilityAndServiceCashDB(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam,
            out Guid DossierSacrifice_FacilityAndServiceCashId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@DossierSacrifice_FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@FacilityAndServiceCashDate", SqlDbType.SmallDateTime),
                new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@CashAmount", SqlDbType.Decimal),
                new SqlParameter("@RecipientPersonName", SqlDbType.NVarChar, 200),
                new SqlParameter("@RecipientPersonNationalNo", SqlDbType.NVarChar, 200),
                new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_FacilityAndServiceCashEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilityAndServiceCashEntityParam.FacilityAndServiceCashId;
            parameters[3].Value = DossierSacrifice_FacilityAndServiceCashEntityParam.FacilityAndServiceCashDate;
            parameters[4].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.OccasionId == null
                ? System.DBNull.Value
                : (object) DossierSacrifice_FacilityAndServiceCashEntityParam.OccasionId);
            parameters[5].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.CashAmount == null
                ? System.DBNull.Value
                : (object) DossierSacrifice_FacilityAndServiceCashEntityParam.CashAmount);
            parameters[6].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonName == ""
                ? System.DBNull.Value
                : (object)
                    FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonName.Trim()));
            parameters[7].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonNationalNo == ""
                ? System.DBNull.Value
                : (object)
                    FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonNationalNo.Trim()));
            parameters[8].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.Description == ""
                ? System.DBNull.Value
                : (object) FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceCashEntityParam.Description.Trim()));

            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilityAndServiceCashAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_FacilityAndServiceCashId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_FacilityAndServiceCashDB(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@DossierSacrifice_FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@FacilityAndServiceCashDate", SqlDbType.SmallDateTime),
                new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@CashAmount", SqlDbType.Decimal),
                new SqlParameter("@RecipientPersonName", SqlDbType.NVarChar, 200),
                new SqlParameter("@RecipientPersonNationalNo", SqlDbType.NVarChar, 200),
                new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value =
                DossierSacrifice_FacilityAndServiceCashEntityParam.DossierSacrifice_FacilityAndServiceCashId;
            parameters[1].Value = DossierSacrifice_FacilityAndServiceCashEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilityAndServiceCashEntityParam.FacilityAndServiceCashId;
            parameters[3].Value = DossierSacrifice_FacilityAndServiceCashEntityParam.FacilityAndServiceCashDate;
            parameters[4].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.OccasionId == null
                ? System.DBNull.Value
                : (object) DossierSacrifice_FacilityAndServiceCashEntityParam.OccasionId);
            parameters[5].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.CashAmount == null
                ? System.DBNull.Value
                : (object) DossierSacrifice_FacilityAndServiceCashEntityParam.CashAmount);
            parameters[6].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonName == ""
                        ? System.DBNull.Value
                        : (object)
                            FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonName.Trim()));
            parameters[7].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonNationalNo == ""
                ? System.DBNull.Value
                : (object)
                    FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceCashEntityParam.RecipientPersonNationalNo.Trim()));
            parameters[8].Value = (DossierSacrifice_FacilityAndServiceCashEntityParam.Description == ""
                ? System.DBNull.Value
                : (object)FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceCashEntityParam.Description.Trim()));

            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilityAndServiceCashUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_FacilityAndServiceCashDB(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@DossierSacrifice_FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value =
                DossierSacrifice_FacilityAndServiceCashEntityParam.DossierSacrifice_FacilityAndServiceCashId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilityAndServiceCashDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_FacilityAndServiceCashEntity GetSingleDossierSacrifice_FacilityAndServiceCashDB(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@DossierSacrifice_FacilityAndServiceCashId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value =
                    DossierSacrifice_FacilityAndServiceCashEntityParam.DossierSacrifice_FacilityAndServiceCashId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilityAndServiceCashGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_FacilityAndServiceCashDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity> GetAllDossierSacrifice_FacilityAndServiceCashDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DossierSacrifice_FacilityAndServiceCashGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity> GetPageDossierSacrifice_FacilityAndServiceCashDB(
            int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_FacilityAndServiceCash";
            parameters[5].Value = "DossierSacrifice_FacilityAndServiceCashId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_FacilityAndServiceCashEntity GetDossierSacrifice_FacilityAndServiceCashDBFromDataReader(
            IDataReader reader)
        {
            return
                new DossierSacrifice_FacilityAndServiceCashEntity(
                    Guid.Parse(reader["DossierSacrifice_FacilityAndServiceCashId"].ToString()),
                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                    Guid.Parse(reader["FacilityAndServiceCashId"].ToString()),
                    reader["FacilityAndServiceCashDate"].ToString(),
                    Convert.IsDBNull(reader["OccasionId"]) ? null : (Guid?)reader["OccasionId"],                       
                    Convert.IsDBNull(reader["CashAmount"]) ? null : (decimal?)reader["CashAmount"],                    
                    reader["RecipientPersonName"].ToString(),
                    reader["RecipientPersonNationalNo"].ToString(),
                    reader["Description"].ToString(),
                    reader["CreationDate"].ToString(),
                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_FacilityAndServiceCashEntity> lst =
                    new List<DossierSacrifice_FacilityAndServiceCashEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_FacilityAndServiceCashDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashDBCollectionByDossierSacrificeDB(DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_FacilityAndServiceCashEntityParam.DossierSacrificeId;
            return
                GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataReader(
                    _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilityAndServiceCashGetByDossierSacrifice",
                        new[] {parameter}));
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashDBCollectionByFacilityAndServiceCashDB(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@FacilityAndServiceCashId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_FacilityAndServiceCashEntityParam.FacilityAndServiceCashId;
            return
                GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataReader(
                    _intranetDB.RunProcedureReader(
                        "Isar.p_DossierSacrifice_FacilityAndServiceCashGetByFacilityAndServiceCash", new[] {parameter}));
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashDBCollectionByOccasionDB(DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_FacilityAndServiceCashEntityParam.OccasionId;
            return
                GetDossierSacrifice_FacilityAndServiceCashDBCollectionFromDataReader(
                    _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilityAndServiceCashGetByOccasion",
                        new[] {parameter}));
        }





    }

}
