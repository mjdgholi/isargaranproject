﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع اهمیت>
    /// </summary>

    //-----------------------------------------------------
    #region Class "ImportanceTypeDB"

    public class ImportanceTypeDB
    {



        #region Methods :

        public void AddImportanceTypeDB(ImportanceTypeEntity importanceTypeEntityParam, out Guid ImportanceTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ImportanceTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ImportanceTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(importanceTypeEntityParam.ImportanceTypeTitle);
            parameters[2].Value = importanceTypeEntityParam.Priority;
            parameters[3].Value = importanceTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_ImportanceTypeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ImportanceTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateImportanceTypeDB(ImportanceTypeEntity importanceTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ImportanceTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ImportanceTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = importanceTypeEntityParam.ImportanceTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(importanceTypeEntityParam.ImportanceTypeTitle);
            parameters[2].Value = importanceTypeEntityParam.Priority;
            parameters[3].Value = importanceTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_ImportanceTypeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteImportanceTypeDB(ImportanceTypeEntity ImportanceTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ImportanceTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ImportanceTypeEntityParam.ImportanceTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_ImportanceTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ImportanceTypeEntity GetSingleImportanceTypeDB(ImportanceTypeEntity ImportanceTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ImportanceTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ImportanceTypeEntityParam.ImportanceTypeId;

                reader = _intranetDB.RunProcedureReader("isar.p_ImportanceTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetImportanceTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ImportanceTypeEntity> GetAllImportanceTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetImportanceTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_ImportanceTypeGetAll", new IDataParameter[] { }));
        }
        public List<ImportanceTypeEntity> GetAllImportanceTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetImportanceTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_ImportanceTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<ImportanceTypeEntity> GetPageImportanceTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ImportanceType";
            parameters[5].Value = "ImportanceTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
            return GetImportanceTypeDBCollectionFromDataSet(ds, out count);
        }

        public ImportanceTypeEntity GetImportanceTypeDBFromDataReader(IDataReader reader)
        {
            return new ImportanceTypeEntity(Guid.Parse(reader["ImportanceTypeId"].ToString()),
                                    reader["ImportanceTypeTitle"].ToString(),
                                    int.Parse(reader["Priority"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ImportanceTypeEntity> GetImportanceTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ImportanceTypeEntity> lst = new List<ImportanceTypeEntity>();
                while (reader.Read())
                    lst.Add(GetImportanceTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ImportanceTypeEntity> GetImportanceTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetImportanceTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion


    }

    #endregion
}