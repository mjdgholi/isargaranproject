﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <پایگاه بسیج>
    /// </summary>

    public class BasijiBaseDB
    {

        #region Methods :

        public void AddBasijiBaseDB(BasijiBaseEntity BasijiBaseEntityParam, out Guid BasijiBaseId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@BasijiBaseId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@BasijiBaseTitle", SqlDbType.NVarChar,400) ,
											  new SqlParameter("@DistrictBasijiId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(BasijiBaseEntityParam.BasijiBaseTitle.Trim());
            parameters[2].Value = BasijiBaseEntityParam.DistrictBasijiId;
            parameters[3].Value = BasijiBaseEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_BasijiBaseAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            BasijiBaseId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateBasijiBaseDB(BasijiBaseEntity BasijiBaseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@BasijiBaseId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@BasijiBaseTitle", SqlDbType.NVarChar,400) ,
											  new SqlParameter("@DistrictBasijiId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = BasijiBaseEntityParam.BasijiBaseId;
            parameters[1].Value = FarsiToArabic.ToArabic(BasijiBaseEntityParam.BasijiBaseTitle.Trim());
            parameters[2].Value = BasijiBaseEntityParam.DistrictBasijiId;
            parameters[3].Value = BasijiBaseEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_BasijiBaseUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteBasijiBaseDB(BasijiBaseEntity BasijiBaseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@BasijiBaseId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = BasijiBaseEntityParam.BasijiBaseId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_BasijiBaseDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public BasijiBaseEntity GetSingleBasijiBaseDB(BasijiBaseEntity BasijiBaseEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@BasijiBaseId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = BasijiBaseEntityParam.BasijiBaseId;

                reader = _intranetDB.RunProcedureReader("Isar.p_BasijiBaseGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetBasijiBaseDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BasijiBaseEntity> GetAllBasijiBaseDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBasijiBaseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_BasijiBaseGetAll", new IDataParameter[] { }));
        }
        public List<BasijiBaseEntity> GetAllBasijiBaseIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBasijiBaseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_BasijiBaseIsActiveGetAll", new IDataParameter[] { }));
        }
        

        public List<BasijiBaseEntity> GetPageBasijiBaseDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_BasijiBase";
            parameters[5].Value = "BasijiBaseId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetBasijiBaseDBCollectionFromDataSet(ds, out count);
        }

        public BasijiBaseEntity GetBasijiBaseDBFromDataReader(IDataReader reader)
        {
            return new BasijiBaseEntity(Guid.Parse(reader["BasijiBaseId"].ToString()),
                                    reader["BasijiBaseTitle"].ToString(),
                                    Guid.Parse(reader["DistrictBasijiId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<BasijiBaseEntity> GetBasijiBaseDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<BasijiBaseEntity> lst = new List<BasijiBaseEntity>();
                while (reader.Read())
                    lst.Add(GetBasijiBaseDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BasijiBaseEntity> GetBasijiBaseDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetBasijiBaseDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<BasijiBaseEntity> GetBasijiBaseDBCollectionByDistrictBasijiDB(BasijiBaseEntity BasijiBaseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DistrictBasijiId", SqlDbType.UniqueIdentifier);
            parameter.Value = BasijiBaseEntityParam.DistrictBasijiId;
            return GetBasijiBaseDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_BasijiBaseGetByDistrictBasiji", new[] { parameter }));
        }


        #endregion



    }
}
