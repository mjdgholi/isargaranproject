﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/02>
    /// Description: <نوع مدت>
    /// </summary>

    public class DurationTypeDB
    {



        #region Methods :

        public void AddDurationTypeDB(DurationTypeEntity durationTypeEntityParam, out Guid DurationTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DurationTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DurationTypeTitle", SqlDbType.NVarChar,150) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(durationTypeEntityParam.DurationTypeTitle.Trim());            
            parameters[2].Value = durationTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DurationTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DurationTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDurationTypeDB(DurationTypeEntity durationTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DurationTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DurationTypeTitle", SqlDbType.NVarChar,150) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = durationTypeEntityParam.DurationTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(durationTypeEntityParam.DurationTypeTitle.Trim());            
            parameters[2].Value = durationTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DurationTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDurationTypeDB(DurationTypeEntity DurationTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DurationTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DurationTypeEntityParam.DurationTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DurationTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DurationTypeEntity GetSingleDurationTypeDB(DurationTypeEntity DurationTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DurationTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DurationTypeEntityParam.DurationTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DurationTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDurationTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DurationTypeEntity> GetAllDurationTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDurationTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DurationTypeGetAll", new IDataParameter[] { }));
        }
        public List<DurationTypeEntity> GetAllDurationTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDurationTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DurationTypeIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<DurationTypeEntity> GetPageDurationTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DurationType";
            parameters[5].Value = "DurationTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDurationTypeDBCollectionFromDataSet(ds, out count);
        }

        public DurationTypeEntity GetDurationTypeDBFromDataReader(IDataReader reader)
        {
            return new DurationTypeEntity(Guid.Parse(reader["DurationTypeId"].ToString()),
                                    reader["DurationTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DurationTypeEntity> GetDurationTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DurationTypeEntity> lst = new List<DurationTypeEntity>();
                while (reader.Read())
                    lst.Add(GetDurationTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DurationTypeEntity> GetDurationTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDurationTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

        
    }
    
}