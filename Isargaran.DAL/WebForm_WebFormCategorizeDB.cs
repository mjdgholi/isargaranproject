﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <narges.kamran>
    /// Create date: <1393/08/20>
    /// Description: <ارتباطی وب با دسته بندی فرم>
    /// </summary>

        public class WebForm_WebFormCategorizeDB
        {         
            #region Methods :

            public void AddWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntity webFormWebFormCategorizeEntityParam, out Guid WebForm_WebFormCategorizeId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = 	{
								 new SqlParameter("@WebForm_WebFormCategorizeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@WebFormIdStr", SqlDbType.NVarChar,500) ,
											  new SqlParameter("@FormPriority", SqlDbType.Int) ,
											  new SqlParameter("@ImagePath", SqlDbType.NVarChar,-1) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
                parameters[0].Direction = ParameterDirection.Output;
                parameters[1].Value = webFormWebFormCategorizeEntityParam.WebFormCategorizeId;
                parameters[2].Value = webFormWebFormCategorizeEntityParam.WebFormIdStr;
                parameters[3].Value = webFormWebFormCategorizeEntityParam.FormPriority;
                parameters[4].Value = (webFormWebFormCategorizeEntityParam.ImagePath == "" ? System.DBNull.Value : (object)webFormWebFormCategorizeEntityParam.ImagePath);
                parameters[5].Value = webFormWebFormCategorizeEntityParam.IsActive;

                parameters[6].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("isar.p_WebForm_WebFormCategorizeAdd", parameters);
                var messageError = parameters[6].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
                WebForm_WebFormCategorizeId = new Guid(parameters[0].Value.ToString());
            }

            public void UpdateWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntity webFormWebFormCategorizeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
			                            	new SqlParameter("@WebForm_WebFormCategorizeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@WebFormId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FormPriority", SqlDbType.Int) ,
											  new SqlParameter("@ImagePath", SqlDbType.NVarChar,-1) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

                parameters[0].Value = webFormWebFormCategorizeEntityParam.WebForm_WebFormCategorizeId;
                parameters[1].Value = webFormWebFormCategorizeEntityParam.WebFormCategorizeId;
                parameters[2].Value = webFormWebFormCategorizeEntityParam.WebFormId;
                parameters[3].Value = webFormWebFormCategorizeEntityParam.FormPriority;
                parameters[4].Value = (webFormWebFormCategorizeEntityParam.ImagePath==""?System.DBNull.Value:(object)webFormWebFormCategorizeEntityParam.ImagePath);
                parameters[5].Value = webFormWebFormCategorizeEntityParam.IsActive;

                parameters[6].Direction = ParameterDirection.Output;

                _intranetDB.RunProcedure("isar.p_WebForm_WebFormCategorizeUpdate", parameters);
                var messageError = parameters[6].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public void DeleteWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntity WebForm_WebFormCategorizeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@WebForm_WebFormCategorizeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
                parameters[0].Value = WebForm_WebFormCategorizeEntityParam.WebForm_WebFormCategorizeId;
                parameters[1].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("isar.p_WebForm_WebFormCategorizeDel", parameters);
                var messageError = parameters[1].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public WebForm_WebFormCategorizeEntity GetSingleWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntity WebForm_WebFormCategorizeEntityParam)
            {
                IDataReader reader = null;
                try
                {

                    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                    IDataParameter[] parameters = {
													new SqlParameter("@WebForm_WebFormCategorizeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                    parameters[0].Value = WebForm_WebFormCategorizeEntityParam.WebForm_WebFormCategorizeId;

                    reader = _intranetDB.RunProcedureReader("isar.p_WebForm_WebFormCategorizeGetSingle", parameters);
                    if (reader != null)
                        if (reader.Read())
                            return GetWebForm_WebFormCategorizeDBFromDataReader(reader);
                    return null;
                }

                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<WebForm_WebFormCategorizeEntity> GetAllWebForm_WebFormCategorizeDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetWebForm_WebFormCategorizeDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("isar.p_WebForm_WebFormCategorizeGetAll", new IDataParameter[] { }));
            }

            public List<WebForm_WebFormCategorizeEntity> GetPageWebForm_WebFormCategorizeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
                parameters[0].Value = PageSize;
                parameters[1].Value = CurrentPage;
                parameters[2].Value = WhereClause;
                parameters[3].Value = OrderBy;
                parameters[4].Value = "t_WebForm_WebFormCategorize";
                parameters[5].Value = "WebForm_WebFormCategorizeId";
                DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
                return GetWebForm_WebFormCategorizeDBCollectionFromDataSet(ds, out count);
            }

            public WebForm_WebFormCategorizeEntity GetWebForm_WebFormCategorizeDBFromDataReader(IDataReader reader)
            {
                return new WebForm_WebFormCategorizeEntity(Guid.Parse(reader["WebForm_WebFormCategorizeId"].ToString()),
                                        Guid.Parse(reader["WebFormCategorizeId"].ToString()),
                                        Guid.Parse(reader["WebFormId"].ToString()),
                                        int.Parse(reader["FormPriority"].ToString()),
                                        reader["ImagePath"].ToString(),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString(),
                                        bool.Parse(reader["IsActive"].ToString()));
            }

            public List<WebForm_WebFormCategorizeEntity> GetWebForm_WebFormCategorizeDBCollectionFromDataReader(IDataReader reader)
            {
                try
                {
                    List<WebForm_WebFormCategorizeEntity> lst = new List<WebForm_WebFormCategorizeEntity>();
                    while (reader.Read())
                        lst.Add(GetWebForm_WebFormCategorizeDBFromDataReader(reader));
                    return lst;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<WebForm_WebFormCategorizeEntity> GetWebForm_WebFormCategorizeDBCollectionFromDataSet(DataSet ds, out int count)
            {
                count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                return GetWebForm_WebFormCategorizeDBCollectionFromDataReader(ds.CreateDataReader());
            }

            public List<WebForm_WebFormCategorizeEntity> GetWebForm_WebFormCategorizeDBCollectionByWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntity webFormWebFormCategorizeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter parameter = new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier);
                parameter.Value = webFormWebFormCategorizeEntityParam.WebFormCategorizeId;
                return GetWebForm_WebFormCategorizeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_WebForm_WebFormCategorizeGetByWebFormCategorize", new[] { parameter }));
            }

            public List<WebForm_WebFormCategorizeEntity> GetWebForm_WebFormCategorizeDBCollectionByWebFormDB(WebForm_WebFormCategorizeEntity webFormWebFormCategorizeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter parameter = new SqlParameter("@WebFormId", SqlDbType.UniqueIdentifier);
                parameter.Value = webFormWebFormCategorizeEntityParam.WebFormId;
                return GetWebForm_WebFormCategorizeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_WebForm_WebFormCategorizeGetByWebForm", new[] { parameter }));
            }

            public List<WebFormEntity> GetWebForm_WebFormCategorizeCollectionByWebForm_WebFormCategorizeWithoutDuplicateDB(Guid WebFormCategorizeId,Guid WebFormId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier),
                                    
                  new SqlParameter("@WebFormId", SqlDbType.UniqueIdentifier),
                };
                                          
                parameters[0].Value = WebFormCategorizeId;
                parameters[1].Value = (WebFormId == new Guid() ? System.DBNull.Value : (object) WebFormId);
                WebFormDB webFormDB = new WebFormDB();
                return webFormDB.GetWebFormDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_WebForm_WebFormCategorizeGetByWebFormWithDuplicate", parameters));
            }
            #endregion

        
        }

    
}