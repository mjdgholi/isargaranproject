﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/27>
    // Description:	<رشته ورزشی پرونده ایثارگر>
    /// </summary>
    public class DossierSacrifice_SportChampionshipDB
    {
        #region Methods :

        public void AddDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam, out Guid DossierSacrifice_SportChampionshipId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_SportChampionshipId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@SportFieldId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PointOrMedal", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CompetitionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@CompetitionTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@CompetitionLocation", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,					
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_SportChampionshipEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_SportChampionshipEntityParam.SportFieldId;
            parameters[3].Value = (DossierSacrifice_SportChampionshipEntityParam.PointOrMedal==""?System.DBNull.Value:(object)DossierSacrifice_SportChampionshipEntityParam.PointOrMedal);
            parameters[4].Value = (DossierSacrifice_SportChampionshipEntityParam.CompetitionDate==""?System.DBNull.Value:(object)DossierSacrifice_SportChampionshipEntityParam.CompetitionDate);
            parameters[5].Value = (DossierSacrifice_SportChampionshipEntityParam.CompetitionTitle==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_SportChampionshipEntityParam.CompetitionTitle.Trim()));
            parameters[6].Value = (DossierSacrifice_SportChampionshipEntityParam.CompetitionLocation==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_SportChampionshipEntityParam.CompetitionLocation.Trim()));
            parameters[7].Value = (DossierSacrifice_SportChampionshipEntityParam.PersonId==null?System.DBNull.Value:(object)DossierSacrifice_SportChampionshipEntityParam.PersonId);
            parameters[8].Value = (DossierSacrifice_SportChampionshipEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_SportChampionshipEntityParam.Description.Trim()));            

            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SportChampionshipAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_SportChampionshipId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_SportChampionshipId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@SportFieldId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PointOrMedal", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CompetitionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@CompetitionTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@CompetitionLocation", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_SportChampionshipEntityParam.DossierSacrifice_SportChampionshipId;
            parameters[1].Value = DossierSacrifice_SportChampionshipEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_SportChampionshipEntityParam.SportFieldId;
            parameters[3].Value = (DossierSacrifice_SportChampionshipEntityParam.PointOrMedal == "" ? System.DBNull.Value : (object)DossierSacrifice_SportChampionshipEntityParam.PointOrMedal);
            parameters[4].Value = (DossierSacrifice_SportChampionshipEntityParam.CompetitionDate == "" ? System.DBNull.Value : (object)DossierSacrifice_SportChampionshipEntityParam.CompetitionDate);
            parameters[5].Value = (DossierSacrifice_SportChampionshipEntityParam.CompetitionTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_SportChampionshipEntityParam.CompetitionTitle.Trim()));
            parameters[6].Value = (DossierSacrifice_SportChampionshipEntityParam.CompetitionLocation == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_SportChampionshipEntityParam.CompetitionLocation.Trim()));
            parameters[7].Value = (DossierSacrifice_SportChampionshipEntityParam.PersonId == null ? System.DBNull.Value : (object)DossierSacrifice_SportChampionshipEntityParam.PersonId);
            parameters[8].Value = (DossierSacrifice_SportChampionshipEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_SportChampionshipEntityParam.Description.Trim()));           

            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SportChampionshipUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_SportChampionshipId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_SportChampionshipEntityParam.DossierSacrifice_SportChampionshipId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SportChampionshipDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_SportChampionshipEntity GetSingleDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_SportChampionshipId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_SportChampionshipEntityParam.DossierSacrifice_SportChampionshipId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SportChampionshipGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_SportChampionshipDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_SportChampionshipEntity> GetAllDossierSacrifice_SportChampionshipDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_SportChampionshipDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_SportChampionshipGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_SportChampionshipEntity> GetPageDossierSacrifice_SportChampionshipDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_SportChampionship";
            parameters[5].Value = "DossierSacrifice_SportChampionshipId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_SportChampionshipDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_SportChampionshipEntity GetDossierSacrifice_SportChampionshipDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_SportChampionshipEntity(Guid.Parse(reader["DossierSacrifice_SportChampionshipId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["SportFieldId"].ToString()),
                                    reader["PointOrMedal"].ToString(),
                                    reader["CompetitionDate"].ToString(),
                                    reader["CompetitionTitle"].ToString(),
                                    reader["CompetitionLocation"].ToString(),
                                    Convert.IsDBNull(reader["PersonId"]) ? (Guid?)null : Guid.Parse(reader["PersonId"].ToString()),                                    
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["FullName"].ToString());
        }

        public List<DossierSacrifice_SportChampionshipEntity> GetDossierSacrifice_SportChampionshipDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_SportChampionshipEntity> lst = new List<DossierSacrifice_SportChampionshipEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_SportChampionshipDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_SportChampionshipEntity> GetDossierSacrifice_SportChampionshipDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_SportChampionshipDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_SportChampionshipEntity> GetDossierSacrifice_SportChampionshipDBCollectionByDossierSacrificeDB(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_SportChampionshipEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_SportChampionshipDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SportChampionshipGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_SportChampionshipEntity> GetDossierSacrifice_SportChampionshipDBCollectionBySportFieldDB(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SportFieldId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_SportChampionshipEntityParam.SportFieldId;
            return GetDossierSacrifice_SportChampionshipDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SportChampionshipGetBySportField", new[] { parameter }));
        }


        #endregion



    }


}
