﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <رشته هنری>
    /// </summary>

    public class ArtCourseDB
    {
        #region Methods :

        public void AddArtCourseDB(ArtCourseEntity ArtCourseEntityParam, out Guid ArtCourseId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ArtCourseId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ArtCourseTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(ArtCourseEntityParam.ArtCourseTitle.Trim());            
            parameters[2].Value = ArtCourseEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_ArtCourseAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ArtCourseId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateArtCourseDB(ArtCourseEntity ArtCourseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ArtCourseId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ArtCourseTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = ArtCourseEntityParam.ArtCourseId;
            parameters[1].Value = FarsiToArabic.ToArabic(ArtCourseEntityParam.ArtCourseTitle.Trim());            
            parameters[2].Value = ArtCourseEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_ArtCourseUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteArtCourseDB(ArtCourseEntity ArtCourseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ArtCourseId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ArtCourseEntityParam.ArtCourseId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_ArtCourseDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ArtCourseEntity GetSingleArtCourseDB(ArtCourseEntity ArtCourseEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ArtCourseId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ArtCourseEntityParam.ArtCourseId;

                reader = _intranetDB.RunProcedureReader("Isar.p_ArtCourseGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetArtCourseDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ArtCourseEntity> GetAllArtCourseDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetArtCourseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_ArtCourseGetAll", new IDataParameter[] { }));
        }

        public List<ArtCourseEntity> GetAllArtCourseIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetArtCourseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_ArtCourseIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<ArtCourseEntity> GetPageArtCourseDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ArtCourse";
            parameters[5].Value = "ArtCourseId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetArtCourseDBCollectionFromDataSet(ds, out count);
        }

        public ArtCourseEntity GetArtCourseDBFromDataReader(IDataReader reader)
        {
            return new ArtCourseEntity(Guid.Parse(reader["ArtCourseId"].ToString()),
                                    reader["ArtCourseTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ArtCourseEntity> GetArtCourseDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ArtCourseEntity> lst = new List<ArtCourseEntity>();
                while (reader.Read())
                    lst.Add(GetArtCourseDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ArtCourseEntity> GetArtCourseDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetArtCourseDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion
    }    
}
