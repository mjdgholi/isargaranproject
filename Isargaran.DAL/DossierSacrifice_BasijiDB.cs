﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <پرونده  بسیجی ایثارگر>
    /// </summary>



    public class DossierSacrifice_BasijiDB
    {
        public void AddDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam, out Guid DossierSacrifice_BasijiId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_BasijiId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsBasiji", SqlDbType.Bit) ,
											  new SqlParameter("@MembershipDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@BasijiMembershipTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@BasijiCategoryId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@BasijiCode", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@BasijiCardNo", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@InsuranceSerialNumberCard", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@InsuranceDateCard", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@BasijiBaseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VillageTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SectionTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_BasijiEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_BasijiEntityParam.IsBasiji;
            parameters[3].Value = (DossierSacrifice_BasijiEntityParam.MembershipDate == "" ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.MembershipDate);
            parameters[4].Value = DossierSacrifice_BasijiEntityParam.BasijiMembershipTypeId;
            parameters[5].Value = (DossierSacrifice_BasijiEntityParam.BasijiCategoryId == null ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.BasijiCategoryId);
            parameters[6].Value = (DossierSacrifice_BasijiEntityParam.BasijiCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.BasijiCode.Trim()));
            parameters[7].Value = (DossierSacrifice_BasijiEntityParam.BasijiCardNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.BasijiCardNo.Trim()));
            parameters[8].Value = (DossierSacrifice_BasijiEntityParam.InsuranceSerialNumberCard == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.InsuranceSerialNumberCard.Trim()));
            parameters[9].Value = (DossierSacrifice_BasijiEntityParam.InsuranceDateCard == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.InsuranceDateCard.Trim()));
            parameters[10].Value = (DossierSacrifice_BasijiEntityParam.BasijiBaseId == null ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.BasijiBaseId);
            parameters[11].Value = (DossierSacrifice_BasijiEntityParam.CityId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.CityId);
            parameters[12].Value = (DossierSacrifice_BasijiEntityParam.VillageTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.VillageTitle.Trim()));
            parameters[13].Value = (DossierSacrifice_BasijiEntityParam.SectionTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.SectionTitle.Trim()));
            parameters[14].Value = (DossierSacrifice_BasijiEntityParam.ZipCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.ZipCode.Trim()));
            parameters[15].Value = (DossierSacrifice_BasijiEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.Address.Trim()));
            parameters[16].Value = (DossierSacrifice_BasijiEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.Email.Trim()));
            parameters[17].Value = (DossierSacrifice_BasijiEntityParam.WebAddress == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.WebAddress.Trim()));
            parameters[18].Value = (DossierSacrifice_BasijiEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.TelNo.Trim()));
            parameters[19].Value = (DossierSacrifice_BasijiEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.FaxNo.Trim()));
            parameters[20].Value = (DossierSacrifice_BasijiEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.StartDate);
            parameters[21].Value = (DossierSacrifice_BasijiEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.EndDate);                

            parameters[22].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_BasijiAdd", parameters);
            var messageError = parameters[22].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_BasijiId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_BasijiId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsBasiji", SqlDbType.Bit) ,
											  new SqlParameter("@MembershipDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@BasijiMembershipTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@BasijiCategoryId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@BasijiCode", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@BasijiCardNo", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@InsuranceSerialNumberCard", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@InsuranceDateCard", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@BasijiBaseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VillageTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SectionTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_BasijiEntityParam.DossierSacrifice_BasijiId;
            parameters[1].Value = DossierSacrifice_BasijiEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_BasijiEntityParam.IsBasiji;
            parameters[3].Value = (DossierSacrifice_BasijiEntityParam.MembershipDate == "" ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.MembershipDate);
            parameters[4].Value = DossierSacrifice_BasijiEntityParam.BasijiMembershipTypeId;
            parameters[5].Value = (DossierSacrifice_BasijiEntityParam.BasijiCategoryId==null?System.DBNull.Value:(object)DossierSacrifice_BasijiEntityParam.BasijiCategoryId);
            parameters[6].Value = (DossierSacrifice_BasijiEntityParam.BasijiCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.BasijiCode.Trim()));
            parameters[7].Value = (DossierSacrifice_BasijiEntityParam.BasijiCardNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.BasijiCardNo.Trim()));
            parameters[8].Value = (DossierSacrifice_BasijiEntityParam.InsuranceSerialNumberCard == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.InsuranceSerialNumberCard.Trim()));
            parameters[9].Value = (DossierSacrifice_BasijiEntityParam.InsuranceDateCard == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.InsuranceDateCard.Trim()));
            parameters[10].Value = (DossierSacrifice_BasijiEntityParam.BasijiBaseId==null?System.DBNull.Value:(object)DossierSacrifice_BasijiEntityParam.BasijiBaseId);
            parameters[11].Value = (DossierSacrifice_BasijiEntityParam.CityId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.CityId);
            parameters[12].Value = (DossierSacrifice_BasijiEntityParam.VillageTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.VillageTitle.Trim()));
            parameters[13].Value = (DossierSacrifice_BasijiEntityParam.SectionTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.SectionTitle.Trim()));
            parameters[14].Value = (DossierSacrifice_BasijiEntityParam.ZipCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.ZipCode.Trim()));
            parameters[15].Value = (DossierSacrifice_BasijiEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.Address.Trim()));
            parameters[16].Value = (DossierSacrifice_BasijiEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.Email.Trim()));
            parameters[17].Value = (DossierSacrifice_BasijiEntityParam.WebAddress == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.WebAddress.Trim()));
            parameters[18].Value = (DossierSacrifice_BasijiEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.TelNo.Trim()));
            parameters[19].Value = (DossierSacrifice_BasijiEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_BasijiEntityParam.FaxNo.Trim()));
            parameters[20].Value = (DossierSacrifice_BasijiEntityParam.StartDate==""?System.DBNull.Value:(object)DossierSacrifice_BasijiEntityParam.StartDate);
            parameters[21].Value = (DossierSacrifice_BasijiEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_BasijiEntityParam.EndDate);        

            parameters[22].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_BasijiUpdate", parameters);
            var messageError = parameters[22].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_BasijiId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_BasijiEntityParam.DossierSacrifice_BasijiId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_BasijiDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_BasijiEntity GetSingleDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_BasijiId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_BasijiEntityParam.DossierSacrifice_BasijiId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_BasijiGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_BasijiDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_BasijiEntity> GetAllDossierSacrifice_BasijiDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_BasijiDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_BasijiGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_BasijiEntity> GetPageDossierSacrifice_BasijiDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Basiji";
            parameters[5].Value = "DossierSacrifice_BasijiId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_BasijiDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_BasijiEntity GetDossierSacrifice_BasijiDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_BasijiEntity(Guid.Parse(reader["DossierSacrifice_BasijiId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    bool.Parse(reader["IsBasiji"].ToString()),
                                    reader["MembershipDate"].ToString(),
                                    Guid.Parse(reader["BasijiMembershipTypeId"].ToString()),
                                    Convert.IsDBNull(reader["BasijiCategoryId"]) ? null : (Guid?)reader["BasijiCategoryId"],                                           
                                    reader["BasijiCode"].ToString(),
                                    reader["BasijiCardNo"].ToString(),
                                    reader["InsuranceSerialNumberCard"].ToString(),
                                    reader["InsuranceDateCard"].ToString(),
                                    Convert.IsDBNull(reader["BasijiBaseId"]) ? null : (Guid?)reader["BasijiBaseId"],                                           
                                    Convert.IsDBNull(reader["CityId"]) ? null : (Guid?)reader["CityId"],                                     
                                    reader["VillageTitle"].ToString(),
                                    reader["SectionTitle"].ToString(),
                                    reader["ZipCode"].ToString(),
                                    reader["Address"].ToString(),
                                    reader["Email"].ToString(),
                                    reader["WebAddress"].ToString(),
                                    reader["TelNo"].ToString(),
                                    reader["FaxNo"].ToString(),
                                    reader["StartDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    Convert.IsDBNull(reader["ProvinceId"]) ? null : (Guid?)reader["ProvinceId"]);
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_BasijiEntity> lst = new List<DossierSacrifice_BasijiEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_BasijiDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_BasijiDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiDBCollectionByBasijiBaseDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@BasijiBaseId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_BasijiEntityParam.BasijiBaseId;
            return GetDossierSacrifice_BasijiDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_BasijiGetByBasijiBase", new[] { parameter }));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiDBCollectionByBasijiCategoryDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@BasijiCategoryId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_BasijiEntityParam.BasijiCategoryId;
            return GetDossierSacrifice_BasijiDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_BasijiGetByBasijiCategory", new[] { parameter }));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiDBCollectionByBasijiMembershipTypeDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@BasijiMembershipTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_BasijiEntityParam.BasijiMembershipTypeId;
            return GetDossierSacrifice_BasijiDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_BasijiGetByBasijiMembershipType", new[] { parameter }));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiDBCollectionByCityDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CityId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_BasijiEntityParam.CityId;
            return GetDossierSacrifice_BasijiDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_BasijiGetByCity", new[] { parameter }));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiDBCollectionByDossierSacrificeDB(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_BasijiEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_BasijiDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_BasijiGetByDossierSacrifice", new[] { parameter }));
        }


    }


}
