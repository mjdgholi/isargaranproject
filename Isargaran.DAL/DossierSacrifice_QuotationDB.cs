﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/02>
    /// Description: <نقل قول>
    /// </summary>



    public class DossierSacrifice_QuotationDB
    {
        #region Methods :

        public void AddDossierSacrifice_QuotationDB(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam, out Guid DossierSacrifice_QuotationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_QuotationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@QuotationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@QuotationContent", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_QuotationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_QuotationEntityParam.QuotationDate;
            parameters[3].Value = DossierSacrifice_QuotationEntityParam.QuotationContent.Trim();            

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_QuotationAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_QuotationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_QuotationDB(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_QuotationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@QuotationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@QuotationContent", SqlDbType.NVarChar,-1) ,		
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_QuotationEntityParam.DossierSacrifice_QuotationId;
            parameters[1].Value = DossierSacrifice_QuotationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_QuotationEntityParam.QuotationDate;
            parameters[3].Value = DossierSacrifice_QuotationEntityParam.QuotationContent.Trim();
            

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_QuotationUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_QuotationDB(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_QuotationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_QuotationEntityParam.DossierSacrifice_QuotationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_QuotationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_QuotationEntity GetSingleDossierSacrifice_QuotationDB(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_QuotationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_QuotationEntityParam.DossierSacrifice_QuotationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_QuotationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_QuotationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_QuotationEntity> GetAllDossierSacrifice_QuotationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_QuotationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_QuotationGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_QuotationEntity> GetPageDossierSacrifice_QuotationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Quotation";
            parameters[5].Value = "DossierSacrifice_QuotationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_QuotationDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_QuotationEntity GetDossierSacrifice_QuotationDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_QuotationEntity(Guid.Parse(reader["DossierSacrifice_QuotationId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["QuotationDate"].ToString(),
                                    reader["QuotationContent"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_QuotationEntity> GetDossierSacrifice_QuotationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_QuotationEntity> lst = new List<DossierSacrifice_QuotationEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_QuotationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_QuotationEntity> GetDossierSacrifice_QuotationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_QuotationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_QuotationEntity> GetDossierSacrifice_QuotationDBCollectionByDossierSacrificeDB(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_QuotationEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_QuotationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_QuotationGetByDossierSacrifice", new[] { parameter }));
        }


        #endregion



    }

   
}
