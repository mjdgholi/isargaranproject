﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/26>
    /// Description: <وضیعت سربازی>
    /// </summary>

    public class MilitaryStatusDB
    {

        #region Methods :

        public void AddMilitaryStatusDB(MilitaryStatusEntity MilitaryStatusEntityParam, out Guid MilitaryStatusId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MilitaryStatusId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@MilitaryStatusTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(MilitaryStatusEntityParam.MilitaryStatusTitle.Trim());
            parameters[2].Value = MilitaryStatusEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_MilitaryStatusAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MilitaryStatusId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMilitaryStatusDB(MilitaryStatusEntity MilitaryStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MilitaryStatusId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@MilitaryStatusTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = MilitaryStatusEntityParam.MilitaryStatusId;
            parameters[1].Value = FarsiToArabic.ToArabic(MilitaryStatusEntityParam.MilitaryStatusTitle.Trim());
            parameters[2].Value = MilitaryStatusEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_MilitaryStatusUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMilitaryStatusDB(MilitaryStatusEntity MilitaryStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MilitaryStatusId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MilitaryStatusEntityParam.MilitaryStatusId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_MilitaryStatusDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MilitaryStatusEntity GetSingleMilitaryStatusDB(MilitaryStatusEntity MilitaryStatusEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@MilitaryStatusId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = MilitaryStatusEntityParam.MilitaryStatusId;

                reader = _intranetDB.RunProcedureReader("Isar.p_MilitaryStatusGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMilitaryStatusDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MilitaryStatusEntity> GetAllMilitaryStatusDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMilitaryStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_MilitaryStatusGetAll", new IDataParameter[] { }));
        }
        public List<MilitaryStatusEntity> GetAllMilitaryStatusIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMilitaryStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_MilitaryStatusIsActiveGetAll", new IDataParameter[] { }));
        }
        

        public List<MilitaryStatusEntity> GetPageMilitaryStatusDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MilitaryStatus";
            parameters[5].Value = "MilitaryStatusId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetMilitaryStatusDBCollectionFromDataSet(ds, out count);
        }

        public MilitaryStatusEntity GetMilitaryStatusDBFromDataReader(IDataReader reader)
        {
            return new MilitaryStatusEntity(Guid.Parse(reader["MilitaryStatusId"].ToString()),
                                    reader["MilitaryStatusTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<MilitaryStatusEntity> GetMilitaryStatusDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MilitaryStatusEntity> lst = new List<MilitaryStatusEntity>();
                while (reader.Read())
                    lst.Add(GetMilitaryStatusDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MilitaryStatusEntity> GetMilitaryStatusDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMilitaryStatusDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
