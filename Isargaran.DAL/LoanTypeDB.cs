﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <majid.Gholibeygian>
    /// Create date: <1393/02/22>
    /// Description: <نوع وام>
    /// </summary>

    #region Class "LoanTypeDB"

    public class LoanTypeDB
    {
        #region Methods :

        public void AddLoanTypeDB(LoanTypeEntity LoanTypeEntityParam, out Guid LoanTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@LoanTypeTitle", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(LoanTypeEntityParam.LoanTypeTitle.Trim());
            parameters[2].Value = LoanTypeEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_LoanTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            LoanTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateLoanTypeDB(LoanTypeEntity LoanTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@LoanTypeTitle", SqlDbType.NVarChar, 100),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = LoanTypeEntityParam.LoanTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(LoanTypeEntityParam.LoanTypeTitle.Trim());
            parameters[2].Value = LoanTypeEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_LoanTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteLoanTypeDB(LoanTypeEntity LoanTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = LoanTypeEntityParam.LoanTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_LoanTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public LoanTypeEntity GetSingleLoanTypeDB(LoanTypeEntity LoanTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@LoanTypeId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = LoanTypeEntityParam.LoanTypeId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_LoanTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetLoanTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<LoanTypeEntity> GetAllLoanTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetLoanTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_LoanTypeGetAll", new IDataParameter[] {}));
        }

        public List<LoanTypeEntity> GetAllIsActiveLoanTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetLoanTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_LoanTypeGetAllIsActive", new IDataParameter[] { }));
        }

        public List<LoanTypeEntity> GetPageLoanTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_LoanType";
            parameters[5].Value = "LoanTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetLoanTypeDBCollectionFromDataSet(ds, out count);
        }

        public LoanTypeEntity GetLoanTypeDBFromDataReader(IDataReader reader)
        {
            return new LoanTypeEntity(new Guid(reader["LoanTypeId"].ToString()), 
                                      reader["LoanTypeTitle"].ToString(),
                                      reader["CreationDate"].ToString(),
                                      reader["ModificationDate"].ToString(),
                                      bool.Parse(reader["IsActive"].ToString()));
        }

        public List<LoanTypeEntity> GetLoanTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<LoanTypeEntity> lst = new List<LoanTypeEntity>();
                while (reader.Read())
                    lst.Add(GetLoanTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<LoanTypeEntity> GetLoanTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetLoanTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}