﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/22>
    /// Description: <رشته علمی و فرهنگی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "ScientificAndCulturalFieldDB"

    public class ScientificAndCulturalFieldDB
    {

     
        #region Methods :

        public void AddScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntity ScientificAndCulturalFieldEntityParam, out Guid ScientificAndCulturalFieldId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ScientificAndCulturalFieldId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ScientificAndCulturalFieldPersianTitle", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ScientificAndCulturalFieldEnglishTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldPersianTitle.Trim());
            parameters[2].Value = (ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldEnglishTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldEnglishTitle.Trim()));
            parameters[3].Value = ScientificAndCulturalFieldEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_ScientificAndCulturalFieldAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ScientificAndCulturalFieldId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntity ScientificAndCulturalFieldEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ScientificAndCulturalFieldId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ScientificAndCulturalFieldPersianTitle", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ScientificAndCulturalFieldEnglishTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldId;
            parameters[1].Value = FarsiToArabic.ToArabic(ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldPersianTitle.Trim());
            parameters[2].Value = (ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldEnglishTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldEnglishTitle.Trim()));
            parameters[3].Value = ScientificAndCulturalFieldEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_ScientificAndCulturalFieldUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntity ScientificAndCulturalFieldEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ScientificAndCulturalFieldId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_ScientificAndCulturalFieldDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ScientificAndCulturalFieldEntity GetSingleScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntity ScientificAndCulturalFieldEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ScientificAndCulturalFieldId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ScientificAndCulturalFieldEntityParam.ScientificAndCulturalFieldId;

                reader = _intranetDB.RunProcedureReader("Isar.p_ScientificAndCulturalFieldGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetScientificAndCulturalFieldDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ScientificAndCulturalFieldEntity> GetAllScientificAndCulturalFieldDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetScientificAndCulturalFieldDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_ScientificAndCulturalFieldGetAll", new IDataParameter[] { }));
        }

        public List<ScientificAndCulturalFieldEntity> GetAllScientificAndCulturalFieldIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetScientificAndCulturalFieldDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_ScientificAndCulturalFieldIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<ScientificAndCulturalFieldEntity> GetPageScientificAndCulturalFieldDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ScientificAndCulturalField";
            parameters[5].Value = "ScientificAndCulturalFieldId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetScientificAndCulturalFieldDBCollectionFromDataSet(ds, out count);
        }

        public ScientificAndCulturalFieldEntity GetScientificAndCulturalFieldDBFromDataReader(IDataReader reader)
        {
            return new ScientificAndCulturalFieldEntity(Guid.Parse(reader["ScientificAndCulturalFieldId"].ToString()),
                                    reader["ScientificAndCulturalFieldPersianTitle"].ToString(),
                                    reader["ScientificAndCulturalFieldEnglishTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ScientificAndCulturalFieldEntity> GetScientificAndCulturalFieldDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ScientificAndCulturalFieldEntity> lst = new List<ScientificAndCulturalFieldEntity>();
                while (reader.Read())
                    lst.Add(GetScientificAndCulturalFieldDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ScientificAndCulturalFieldEntity> GetScientificAndCulturalFieldDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetScientificAndCulturalFieldDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
