﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: < نوع مراکز تحصیلی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "EducationCenterTypeDB"

    public class EducationCenterTypeDB
    {



        #region Methods :

        public void AddEducationCenterTypeDB(EducationCenterTypeEntity EducationCenterTypeEntityParam, out Guid EducationCenterTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EducationCenterTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EducationCenterTypePersianTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@EducationCenterTypeEnglishTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationCenterTypeEntityParam.EducationCenterTypePersianTitle.Trim());
            parameters[2].Value =  FarsiToArabic.ToArabic(EducationCenterTypeEntityParam.EducationCenterTypeEnglishTitle.Trim());
            parameters[3].Value = EducationCenterTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCenterTypeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EducationCenterTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationCenterTypeDB(EducationCenterTypeEntity EducationCenterTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EducationCenterTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EducationCenterTypePersianTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@EducationCenterTypeEnglishTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EducationCenterTypeEntityParam.EducationCenterTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationCenterTypeEntityParam.EducationCenterTypePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationCenterTypeEntityParam.EducationCenterTypeEnglishTitle.Trim());
            parameters[3].Value = EducationCenterTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationCenterTypeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationCenterTypeDB(EducationCenterTypeEntity EducationCenterTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EducationCenterTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EducationCenterTypeEntityParam.EducationCenterTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCenterTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationCenterTypeEntity GetSingleEducationCenterTypeDB(EducationCenterTypeEntity EducationCenterTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EducationCenterTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EducationCenterTypeEntityParam.EducationCenterTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationCenterTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationCenterTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCenterTypeEntity> GetAllEducationCenterTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationCenterTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationCenterTypeGetAll", new IDataParameter[] { }));
        }

        public List<EducationCenterTypeEntity> GetAllEducationCenterTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationCenterTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationCenterTypeIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<EducationCenterTypeEntity> GetPageEducationCenterTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EducationCenterType";
            parameters[5].Value = "EducationCenterTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationCenterTypeDBCollectionFromDataSet(ds, out count);
        }

        public EducationCenterTypeEntity GetEducationCenterTypeDBFromDataReader(IDataReader reader)
        {
            return new EducationCenterTypeEntity(Guid.Parse(reader["EducationCenterTypeId"].ToString()),
                                    reader["EducationCenterTypePersianTitle"].ToString(),
                                    reader["EducationCenterTypeEnglishTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationCenterTypeEntity> GetEducationCenterTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationCenterTypeEntity> lst = new List<EducationCenterTypeEntity>();
                while (reader.Read())
                    lst.Add(GetEducationCenterTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCenterTypeEntity> GetEducationCenterTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationCenterTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
