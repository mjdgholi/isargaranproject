﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <پست>
    /// </summary>

    public class PostDB
    {
        #region Methods :

        public void AddPostDB(PostEntity PostEntityParam, out Guid PostId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@PostId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@PostTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(PostEntityParam.PostTitle.Trim());            
            parameters[2].Value = PostEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_PostAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            PostId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdatePostDB(PostEntity PostEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@PostId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@PostTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = PostEntityParam.PostId;
            parameters[1].Value = FarsiToArabic.ToArabic(PostEntityParam.PostTitle.Trim());           
            parameters[2].Value = PostEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_PostUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeletePostDB(PostEntity PostEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@PostId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = PostEntityParam.PostId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_PostDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public PostEntity GetSinglePostDB(PostEntity PostEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@PostId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = PostEntityParam.PostId;

                reader = _intranetDB.RunProcedureReader("Isar.p_PostGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetPostDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PostEntity> GetAllPostDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetPostDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_PostGetAll", new IDataParameter[] { }));
        }

        public List<PostEntity> GetAllPostIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetPostDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_PostIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<PostEntity> GetPagePostDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Post";
            parameters[5].Value = "PostId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetPostDBCollectionFromDataSet(ds, out count);
        }

        public PostEntity GetPostDBFromDataReader(IDataReader reader)
        {
            return new PostEntity(Guid.Parse(reader["PostId"].ToString()),
                                    reader["PostTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<PostEntity> GetPostDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<PostEntity> lst = new List<PostEntity>();
                while (reader.Read())
                    lst.Add(GetPostDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PostEntity> GetPostDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetPostDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    
}
