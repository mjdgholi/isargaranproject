﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <پرونده معارف و علوم قرآنی ایثارگر>
    /// </summary>


    public class DossierSacrifice_QuranicAndIslamicDB
    {
        #region Methods :

        public void AddDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam, out Guid DossierSacrifice_QuranicAndIslamicId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_QuranicAndIslamicId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@QuranicAndIslamicCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ExperienceYearNo", SqlDbType.Int) ,
											  new SqlParameter("@HasEvidence", SqlDbType.Bit) ,
											  new SqlParameter("@PlaceOfGraduation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_QuranicAndIslamicEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_QuranicAndIslamicEntityParam.QuranicAndIslamicCourseId;
            parameters[3].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.ExperienceYearNo==null?System.DBNull.Value:(object)DossierSacrifice_QuranicAndIslamicEntityParam.ExperienceYearNo);
            parameters[4].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.HasEvidence==null?System.DBNull.Value:(object)DossierSacrifice_QuranicAndIslamicEntityParam.HasEvidence);
            parameters[5].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.PlaceOfGraduation == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_QuranicAndIslamicEntityParam.PlaceOfGraduation.Trim()));
            parameters[6].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_QuranicAndIslamicEntityParam.Description.Trim()));  


            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_QuranicAndIslamicAdd", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_QuranicAndIslamicId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_QuranicAndIslamicId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@QuranicAndIslamicCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ExperienceYearNo", SqlDbType.Int) ,
											  new SqlParameter("@HasEvidence", SqlDbType.Bit) ,
											  new SqlParameter("@PlaceOfGraduation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_QuranicAndIslamicEntityParam.DossierSacrifice_QuranicAndIslamicId;
            parameters[1].Value = DossierSacrifice_QuranicAndIslamicEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_QuranicAndIslamicEntityParam.QuranicAndIslamicCourseId;
            parameters[3].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.ExperienceYearNo==null?System.DBNull.Value:(object)DossierSacrifice_QuranicAndIslamicEntityParam.ExperienceYearNo);
            parameters[4].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.HasEvidence == null ? System.DBNull.Value : (object)DossierSacrifice_QuranicAndIslamicEntityParam.HasEvidence);
            parameters[5].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.PlaceOfGraduation==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_QuranicAndIslamicEntityParam.PlaceOfGraduation.Trim()));
            parameters[6].Value = (DossierSacrifice_QuranicAndIslamicEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_QuranicAndIslamicEntityParam.Description.Trim()));  

            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_QuranicAndIslamicUpdate", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_QuranicAndIslamicId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_QuranicAndIslamicEntityParam.DossierSacrifice_QuranicAndIslamicId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_QuranicAndIslamicDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_QuranicAndIslamicEntity GetSingleDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_QuranicAndIslamicId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_QuranicAndIslamicEntityParam.DossierSacrifice_QuranicAndIslamicId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_QuranicAndIslamicGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_QuranicAndIslamicDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_QuranicAndIslamicEntity> GetAllDossierSacrifice_QuranicAndIslamicDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_QuranicAndIslamicDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_QuranicAndIslamicGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_QuranicAndIslamicEntity> GetPageDossierSacrifice_QuranicAndIslamicDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_QuranicAndIslamic";
            parameters[5].Value = "DossierSacrifice_QuranicAndIslamicId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_QuranicAndIslamicDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_QuranicAndIslamicEntity GetDossierSacrifice_QuranicAndIslamicDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_QuranicAndIslamicEntity(Guid.Parse(reader["DossierSacrifice_QuranicAndIslamicId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["QuranicAndIslamicCourseId"].ToString()),
                                    Convert.IsDBNull(reader["ExperienceYearNo"]) ? null : (int?) reader["ExperienceYearNo"],    
                                      Convert.IsDBNull(reader["HasEvidence"]) ? null : (bool?) reader["HasEvidence"],                                               
                                    reader["PlaceOfGraduation"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_QuranicAndIslamicEntity> GetDossierSacrifice_QuranicAndIslamicDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_QuranicAndIslamicEntity> lst = new List<DossierSacrifice_QuranicAndIslamicEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_QuranicAndIslamicDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_QuranicAndIslamicEntity> GetDossierSacrifice_QuranicAndIslamicDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_QuranicAndIslamicDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_QuranicAndIslamicEntity> GetDossierSacrifice_QuranicAndIslamicDBCollectionByDossierSacrificeDB(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_QuranicAndIslamicEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_QuranicAndIslamicDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_QuranicAndIslamicGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_QuranicAndIslamicEntity> GetDossierSacrifice_QuranicAndIslamicDBCollectionByQuranicAndIslamicCourseDB(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@QuranicAndIslamicCourseId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_QuranicAndIslamicEntityParam.QuranicAndIslamicCourseId;
            return GetDossierSacrifice_QuranicAndIslamicDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_QuranicAndIslamicGetByQuranicAndIslamicCourse", new[] { parameter }));
        }


        #endregion



    }
  
}
