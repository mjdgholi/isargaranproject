﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/10>
    /// Description: <پرونده  ااطلاعات مهارت علمی و فنی ایثارگری>
    /// </summary>

        public class DossierSacrifice_ScientificAndTechnicalSkillDB
        {
            #region Methods :

            public void AddDossierSacrifice_ScientificAndTechnicalSkillDB(DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam, out Guid DossierSacrifice_ScientificAndTechnicalSkillId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_ScientificAndTechnicalSkillId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ScientificAndTechnicalSkillTitle", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ExperienceYearNo", SqlDbType.Int) ,
											  new SqlParameter("@HasEvidence", SqlDbType.Bit) ,
											  new SqlParameter("@PlaceOfGraduation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
                parameters[0].Direction = ParameterDirection.Output;
                parameters[1].Value = DossierSacrifice_ScientificAndTechnicalSkillEntityParam.DossierSacrificeId;
                parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndTechnicalSkillEntityParam.ScientificAndTechnicalSkillTitle.Trim());
                parameters[3].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.ExperienceYearNo==null?System.DBNull.Value:(object)DossierSacrifice_ScientificAndTechnicalSkillEntityParam.ExperienceYearNo);
                parameters[4].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.HasEvidence == null ? System.DBNull.Value : (object)DossierSacrifice_ScientificAndTechnicalSkillEntityParam.HasEvidence);
                parameters[5].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.PlaceOfGraduation==""?System.DBNull.Value:(object)DossierSacrifice_ScientificAndTechnicalSkillEntityParam.PlaceOfGraduation);
                parameters[6].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndTechnicalSkillEntityParam.Description.Trim()));                
                parameters[7].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ScientificAndTechnicalSkillAdd", parameters);
                var messageError = parameters[7].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
                DossierSacrifice_ScientificAndTechnicalSkillId = new Guid(parameters[0].Value.ToString());
            }

            public void UpdateDossierSacrifice_ScientificAndTechnicalSkillDB(DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_ScientificAndTechnicalSkillId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ScientificAndTechnicalSkillTitle", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ExperienceYearNo", SqlDbType.Int) ,
											  new SqlParameter("@HasEvidence", SqlDbType.Bit) ,
											  new SqlParameter("@PlaceOfGraduation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

                parameters[0].Value = DossierSacrifice_ScientificAndTechnicalSkillEntityParam.DossierSacrifice_ScientificAndTechnicalSkillId;
                parameters[1].Value = DossierSacrifice_ScientificAndTechnicalSkillEntityParam.DossierSacrificeId;
                parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndTechnicalSkillEntityParam.ScientificAndTechnicalSkillTitle.Trim());
                parameters[3].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.ExperienceYearNo == null ? System.DBNull.Value : (object)DossierSacrifice_ScientificAndTechnicalSkillEntityParam.ExperienceYearNo);
                parameters[4].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.HasEvidence == null ? System.DBNull.Value : (object)DossierSacrifice_ScientificAndTechnicalSkillEntityParam.HasEvidence);
                parameters[5].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.PlaceOfGraduation == "" ? System.DBNull.Value : (object)DossierSacrifice_ScientificAndTechnicalSkillEntityParam.PlaceOfGraduation);
                parameters[6].Value = (DossierSacrifice_ScientificAndTechnicalSkillEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndTechnicalSkillEntityParam.Description.Trim()));                 
                parameters[7].Direction = ParameterDirection.Output;

                _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ScientificAndTechnicalSkillUpdate", parameters);
                var messageError = parameters[7].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public void DeleteDossierSacrifice_ScientificAndTechnicalSkillDB(DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_ScientificAndTechnicalSkillId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
                parameters[0].Value = DossierSacrifice_ScientificAndTechnicalSkillEntityParam.DossierSacrifice_ScientificAndTechnicalSkillId;
                parameters[1].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ScientificAndTechnicalSkillDel", parameters);
                var messageError = parameters[1].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public DossierSacrifice_ScientificAndTechnicalSkillEntity GetSingleDossierSacrifice_ScientificAndTechnicalSkillDB(DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
            {
                IDataReader reader = null;
                try
                {

                    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                    IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_ScientificAndTechnicalSkillId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                    parameters[0].Value = DossierSacrifice_ScientificAndTechnicalSkillEntityParam.DossierSacrifice_ScientificAndTechnicalSkillId;

                    reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ScientificAndTechnicalSkillGetSingle", parameters);
                    if (reader != null)
                        if (reader.Read())
                            return GetDossierSacrifice_ScientificAndTechnicalSkillDBFromDataReader(reader);
                    return null;
                }

                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<DossierSacrifice_ScientificAndTechnicalSkillEntity> GetAllDossierSacrifice_ScientificAndTechnicalSkillDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("Isar.p_DossierSacrifice_ScientificAndTechnicalSkillGetAll", new IDataParameter[] { }));
            }

            public List<DossierSacrifice_ScientificAndTechnicalSkillEntity> GetPageDossierSacrifice_ScientificAndTechnicalSkillDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
                parameters[0].Value = PageSize;
                parameters[1].Value = CurrentPage;
                parameters[2].Value = WhereClause;
                parameters[3].Value = OrderBy;
                parameters[4].Value = "t_DossierSacrifice_ScientificAndTechnicalSkill";
                parameters[5].Value = "DossierSacrifice_ScientificAndTechnicalSkillId";
                DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
                return GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionFromDataSet(ds, out count);
            }

            public DossierSacrifice_ScientificAndTechnicalSkillEntity GetDossierSacrifice_ScientificAndTechnicalSkillDBFromDataReader(IDataReader reader)
            {
                return new DossierSacrifice_ScientificAndTechnicalSkillEntity(Guid.Parse(reader["DossierSacrifice_ScientificAndTechnicalSkillId"].ToString()),
                                        Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                        reader["ScientificAndTechnicalSkillTitle"].ToString(),
                                            Convert.IsDBNull(reader["ExperienceYearNo"]) ? null : (int?)reader["ExperienceYearNo"],
                                            Convert.IsDBNull(reader["HasEvidence"]) ? null : (bool?)reader["HasEvidence"],                                        
                                        reader["PlaceOfGraduation"].ToString(),
                                        reader["Description"].ToString(),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString());
            }

            public List<DossierSacrifice_ScientificAndTechnicalSkillEntity> GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionFromDataReader(IDataReader reader)
            {
                try
                {
                    List<DossierSacrifice_ScientificAndTechnicalSkillEntity> lst = new List<DossierSacrifice_ScientificAndTechnicalSkillEntity>();
                    while (reader.Read())
                        lst.Add(GetDossierSacrifice_ScientificAndTechnicalSkillDBFromDataReader(reader));
                    return lst;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<DossierSacrifice_ScientificAndTechnicalSkillEntity> GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionFromDataSet(DataSet ds, out int count)
            {
                count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                return GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionFromDataReader(ds.CreateDataReader());
            }

            public List<DossierSacrifice_ScientificAndTechnicalSkillEntity> GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionByDossierSacrificeDB(DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
                parameter.Value = DossierSacrifice_ScientificAndTechnicalSkillEntityParam.DossierSacrificeId;
                return GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ScientificAndTechnicalSkillGetByDossierSacrifice", new[] { parameter }));
            }


            #endregion



        }


    }


