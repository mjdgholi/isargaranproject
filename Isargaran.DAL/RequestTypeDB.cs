﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع درخواست>
    /// </summary>

    //-----------------------------------------------------
    #region Class "RequestTypeDB"

    public class RequestTypeDB
    {

 

        #region Methods :

        public void AddRequestTypeDB(RequestTypeEntity requestTypeEntityParam, out Guid RequestTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@RequestTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@RequestTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(requestTypeEntityParam.RequestTypeTitle);
            parameters[2].Value = requestTypeEntityParam.Priority;
            parameters[3].Value = requestTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_RequestTypeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            RequestTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateRequestTypeDB(RequestTypeEntity requestTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@RequestTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@RequestTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,									
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = requestTypeEntityParam.RequestTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(requestTypeEntityParam.RequestTypeTitle);
            parameters[2].Value = requestTypeEntityParam.Priority;
            parameters[3].Value = requestTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_RequestTypeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteRequestTypeDB(RequestTypeEntity RequestTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@RequestTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = RequestTypeEntityParam.RequestTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_RequestTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public RequestTypeEntity GetSingleRequestTypeDB(RequestTypeEntity RequestTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@RequestTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = RequestTypeEntityParam.RequestTypeId;

                reader = _intranetDB.RunProcedureReader("isar.p_RequestTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetRequestTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<RequestTypeEntity> GetAllRequestTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetRequestTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_RequestTypeGetAll", new IDataParameter[] { }));
        }
        public List<RequestTypeEntity> GetAllRequestTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetRequestTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_RequestTypeISActiveGetAll", new IDataParameter[] { }));
        }
        public List<RequestTypeEntity> GetPageRequestTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_RequestType";
            parameters[5].Value = "RequestTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
            return GetRequestTypeDBCollectionFromDataSet(ds, out count);
        }

        public RequestTypeEntity GetRequestTypeDBFromDataReader(IDataReader reader)
        {
            return new RequestTypeEntity(Guid.Parse(reader["RequestTypeId"].ToString()),
                                    reader["RequestTypeTitle"].ToString(),
                                    int.Parse(reader["Priority"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<RequestTypeEntity> GetRequestTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<RequestTypeEntity> lst = new List<RequestTypeEntity>();
                while (reader.Read())
                    lst.Add(GetRequestTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<RequestTypeEntity> GetRequestTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetRequestTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

        
    }

    #endregion
}