﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <حوزه بسیجی>
    /// </summary>

    public class DistrictBasijiDB
    {
        #region Methods :

        public void AddDistrictBasijiDB(DistrictBasijiEntity DistrictBasijiEntityParam, out Guid DistrictBasijiId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DistrictBasijiId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DistrictBasijiTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(DistrictBasijiEntityParam.DistrictBasijiTitle.Trim());
            parameters[2].Value = DistrictBasijiEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DistrictBasijiAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DistrictBasijiId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDistrictBasijiDB(DistrictBasijiEntity DistrictBasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DistrictBasijiId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DistrictBasijiTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DistrictBasijiEntityParam.DistrictBasijiId;
            parameters[1].Value = FarsiToArabic.ToArabic(DistrictBasijiEntityParam.DistrictBasijiTitle.Trim());
            parameters[2].Value = DistrictBasijiEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DistrictBasijiUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDistrictBasijiDB(DistrictBasijiEntity DistrictBasijiEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DistrictBasijiId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DistrictBasijiEntityParam.DistrictBasijiId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DistrictBasijiDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DistrictBasijiEntity GetSingleDistrictBasijiDB(DistrictBasijiEntity DistrictBasijiEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DistrictBasijiId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DistrictBasijiEntityParam.DistrictBasijiId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DistrictBasijiGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDistrictBasijiDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DistrictBasijiEntity> GetAllDistrictBasijiDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDistrictBasijiDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DistrictBasijiGetAll", new IDataParameter[] { }));
        }
        public List<DistrictBasijiEntity> GetAllDistrictBasijiIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDistrictBasijiDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DistrictBasijiIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<DistrictBasijiEntity> GetPageDistrictBasijiDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DistrictBasiji";
            parameters[5].Value = "DistrictBasijiId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDistrictBasijiDBCollectionFromDataSet(ds, out count);
        }

        public DistrictBasijiEntity GetDistrictBasijiDBFromDataReader(IDataReader reader)
        {
            return new DistrictBasijiEntity(Guid.Parse(reader["DistrictBasijiId"].ToString()),
                                    reader["DistrictBasijiTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DistrictBasijiEntity> GetDistrictBasijiDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DistrictBasijiEntity> lst = new List<DistrictBasijiEntity>();
                while (reader.Read())
                    lst.Add(GetDistrictBasijiDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DistrictBasijiEntity> GetDistrictBasijiDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDistrictBasijiDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

}
