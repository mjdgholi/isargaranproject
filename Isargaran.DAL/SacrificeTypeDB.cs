﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <نوع ایثارگری>
    /// </summary>


        public class SacrificeTypeDB
        {
            #region Methods :

            public void AddSacrificeTypeDB(SacrificeTypeEntity SacrificeTypeEntityParam, out Guid SacrificeTypeId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = 	{
								 new SqlParameter("@SacrificeTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SacrificeTypeTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
                parameters[0].Direction = ParameterDirection.Output;
                parameters[1].Value = FarsiToArabic.ToArabic(SacrificeTypeEntityParam.SacrificeTypeTitle.Trim());
                parameters[2].Value = SacrificeTypeEntityParam.IsActive;

                parameters[3].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("Isar.p_SacrificeTypeAdd", parameters);
                var messageError = parameters[3].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
                SacrificeTypeId = new Guid(parameters[0].Value.ToString());
            }

            public void UpdateSacrificeTypeDB(SacrificeTypeEntity SacrificeTypeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
			                            	new SqlParameter("@SacrificeTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@SacrificeTypeTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

                parameters[0].Value = SacrificeTypeEntityParam.SacrificeTypeId;
                parameters[1].Value = FarsiToArabic.ToArabic(SacrificeTypeEntityParam.SacrificeTypeTitle.Trim());
                parameters[2].Value = SacrificeTypeEntityParam.IsActive;

                parameters[3].Direction = ParameterDirection.Output;

                _intranetDB.RunProcedure("Isar.p_SacrificeTypeUpdate", parameters);
                var messageError = parameters[3].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public void DeleteSacrificeTypeDB(SacrificeTypeEntity SacrificeTypeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@SacrificeTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
                parameters[0].Value = SacrificeTypeEntityParam.SacrificeTypeId;
                parameters[1].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("Isar.p_SacrificeTypeDel", parameters);
                var messageError = parameters[1].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public SacrificeTypeEntity GetSingleSacrificeTypeDB(SacrificeTypeEntity SacrificeTypeEntityParam)
            {
                IDataReader reader = null;
                try
                {

                    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                    IDataParameter[] parameters = {
													new SqlParameter("@SacrificeTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                    parameters[0].Value = SacrificeTypeEntityParam.SacrificeTypeId;

                    reader = _intranetDB.RunProcedureReader("Isar.p_SacrificeTypeGetSingle", parameters);
                    if (reader != null)
                        if (reader.Read())
                            return GetSacrificeTypeDBFromDataReader(reader);
                    return null;
                }

                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<SacrificeTypeEntity> GetAllSacrificeTypeDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetSacrificeTypeDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("Isar.p_SacrificeTypeGetAll", new IDataParameter[] { }));
            }

            public List<SacrificeTypeEntity> GetAllSacrificeTypeIsActiveDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetSacrificeTypeDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("Isar.p_SacrificeTypeIsActiveGetAll", new IDataParameter[] { }));
            }

            public List<SacrificeTypeEntity> GetPageSacrificeTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
                parameters[0].Value = PageSize;
                parameters[1].Value = CurrentPage;
                parameters[2].Value = WhereClause;
                parameters[3].Value = OrderBy;
                parameters[4].Value = "t_SacrificeType";
                parameters[5].Value = "SacrificeTypeId";
                DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
                return GetSacrificeTypeDBCollectionFromDataSet(ds, out count);
            }

            public SacrificeTypeEntity GetSacrificeTypeDBFromDataReader(IDataReader reader)
            {
                return new SacrificeTypeEntity(Guid.Parse(reader["SacrificeTypeId"].ToString()),
                                        reader["SacrificeTypeTitle"].ToString(),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString(),
                                        bool.Parse(reader["IsActive"].ToString()));
            }

            public List<SacrificeTypeEntity> GetSacrificeTypeDBCollectionFromDataReader(IDataReader reader)
            {
                try
                {
                    List<SacrificeTypeEntity> lst = new List<SacrificeTypeEntity>();
                    while (reader.Read())
                        lst.Add(GetSacrificeTypeDBFromDataReader(reader));
                    return lst;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<SacrificeTypeEntity> GetSacrificeTypeDBCollectionFromDataSet(DataSet ds, out int count)
            {
                count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                return GetSacrificeTypeDBCollectionFromDataReader(ds.CreateDataReader());
            }


            #endregion



        }


}
