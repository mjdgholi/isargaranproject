﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	< نوع گواهینامه رانندگی(پایه)>
    /// </summary>
    #region Class "DrivingLicenseTypeDB"

    public class DrivingLicenseTypeDB
    {
        #region Methods :

        public void AddDrivingLicenseTypeDB(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam, out Guid DrivingLicenseTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DrivingLicenseTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DrivingLicenseTypeTitlePersian", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@DrivingLicenseTypeTitleEnglish", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@Priority", SqlDbType.Int),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(DrivingLicenseTypeEntityParam.DrivingLicenseTypeTitlePersian.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(DrivingLicenseTypeEntityParam.DrivingLicenseTypeTitleEnglish.Trim());
            parameters[3].Value = DrivingLicenseTypeEntityParam.Priority;
            parameters[4].Value = DrivingLicenseTypeEntityParam.IsActive;
            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DrivingLicenseTypeAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DrivingLicenseTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDrivingLicenseTypeDB(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DrivingLicenseTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DrivingLicenseTypeTitlePersian", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@DrivingLicenseTypeTitleEnglish", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@Priority", SqlDbType.Int),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DrivingLicenseTypeEntityParam.DrivingLicenseTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(DrivingLicenseTypeEntityParam.DrivingLicenseTypeTitlePersian.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(DrivingLicenseTypeEntityParam.DrivingLicenseTypeTitleEnglish.Trim());
            parameters[3].Value = DrivingLicenseTypeEntityParam.Priority;
            parameters[4].Value = DrivingLicenseTypeEntityParam.IsActive;
            parameters[5].Direction = ParameterDirection.Output;     

            _intranetDB.RunProcedure("Isar.p_DrivingLicenseTypeUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDrivingLicenseTypeDB(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DrivingLicenseTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DrivingLicenseTypeEntityParam.DrivingLicenseTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DrivingLicenseTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DrivingLicenseTypeEntity GetSingleDrivingLicenseTypeDB(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DrivingLicenseTypeId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DrivingLicenseTypeEntityParam.DrivingLicenseTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DrivingLicenseTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDrivingLicenseTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DrivingLicenseTypeEntity> GetAllDrivingLicenseTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDrivingLicenseTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DrivingLicenseTypeGetAll", new IDataParameter[] {}));
        }

        public List<DrivingLicenseTypeEntity> GetAllIsActiveDrivingLicenseTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDrivingLicenseTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DrivingLicenseTypeGetAllIsActive", new IDataParameter[] { }));
        }


        public List<DrivingLicenseTypeEntity> GetPageDrivingLicenseTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_DrivingLicenseType";
            parameters[5].Value = "DrivingLicenseTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDrivingLicenseTypeDBCollectionFromDataSet(ds, out count);
        }

        public DrivingLicenseTypeEntity GetDrivingLicenseTypeDBFromDataReader(IDataReader reader)
        {
            return new DrivingLicenseTypeEntity(new Guid(reader["DrivingLicenseTypeId"].ToString()),
                                                reader["DrivingLicenseTypeTitlePersian"].ToString(),
                                                reader["DrivingLicenseTypeTitleEnglish"].ToString(),
                                                int.Parse(reader["Priority"].ToString()),
                                                reader["CreationDate"].ToString(),
                                                reader["ModificationDate"].ToString(),
                                                bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DrivingLicenseTypeEntity> GetDrivingLicenseTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DrivingLicenseTypeEntity> lst = new List<DrivingLicenseTypeEntity>();
                while (reader.Read())
                    lst.Add(GetDrivingLicenseTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DrivingLicenseTypeEntity> GetDrivingLicenseTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDrivingLicenseTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}