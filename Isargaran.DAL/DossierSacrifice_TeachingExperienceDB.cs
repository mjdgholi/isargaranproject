﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{

        /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <اطلاعات سوابق تدریس>
    /// </summary>
    public class DossierSacrifice_TeachingExperienceDB
    {
        #region Methods :

        public void AddDossierSacrifice_TeachingExperienceDB(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam, out Guid DossierSacrifice_TeachingExperienceId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_TeachingExperienceId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@LessonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TeachingDurationYearNo", SqlDbType.Int) ,
											  new SqlParameter("@IsNowTeaching", SqlDbType.Bit) ,
											  new SqlParameter("@TeachingLocation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value =DossierSacrifice_TeachingExperienceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_TeachingExperienceEntityParam.LessonId;
            parameters[3].Value = DossierSacrifice_TeachingExperienceEntityParam.TeachingDurationYearNo;
            parameters[4].Value = DossierSacrifice_TeachingExperienceEntityParam.IsNowTeaching;
            parameters[5].Value = FarsiToArabic.ToArabic(DossierSacrifice_TeachingExperienceEntityParam.TeachingLocation.Trim());
            parameters[6].Value = FarsiToArabic.ToArabic(DossierSacrifice_TeachingExperienceEntityParam.Description.Trim());            

            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TeachingExperienceAdd", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_TeachingExperienceId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_TeachingExperienceDB(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_TeachingExperienceId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@LessonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TeachingDurationYearNo", SqlDbType.Int) ,
											  new SqlParameter("@IsNowTeaching", SqlDbType.Bit) ,
											  new SqlParameter("@TeachingLocation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_TeachingExperienceEntityParam.DossierSacrifice_TeachingExperienceId;
            parameters[1].Value = DossierSacrifice_TeachingExperienceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_TeachingExperienceEntityParam.LessonId;
            parameters[3].Value = DossierSacrifice_TeachingExperienceEntityParam.TeachingDurationYearNo;
            parameters[4].Value = DossierSacrifice_TeachingExperienceEntityParam.IsNowTeaching;
            parameters[5].Value = FarsiToArabic.ToArabic(DossierSacrifice_TeachingExperienceEntityParam.TeachingLocation.Trim());
            parameters[6].Value = FarsiToArabic.ToArabic(DossierSacrifice_TeachingExperienceEntityParam.Description.Trim());     

            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TeachingExperienceUpdate", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_TeachingExperienceDB(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_TeachingExperienceId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_TeachingExperienceEntityParam.DossierSacrifice_TeachingExperienceId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TeachingExperienceDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_TeachingExperienceEntity GetSingleDossierSacrifice_TeachingExperienceDB(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_TeachingExperienceId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_TeachingExperienceEntityParam.DossierSacrifice_TeachingExperienceId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TeachingExperienceGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_TeachingExperienceDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TeachingExperienceEntity> GetAllDossierSacrifice_TeachingExperienceDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_TeachingExperienceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_TeachingExperienceGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_TeachingExperienceEntity> GetPageDossierSacrifice_TeachingExperienceDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_TeachingExperience";
            parameters[5].Value = "DossierSacrifice_TeachingExperienceId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_TeachingExperienceDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_TeachingExperienceEntity GetDossierSacrifice_TeachingExperienceDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_TeachingExperienceEntity(Guid.Parse(reader["DossierSacrifice_TeachingExperienceId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["LessonId"].ToString()),
                                    int.Parse(reader["TeachingDurationYearNo"].ToString()),
                                    bool.Parse(reader["IsNowTeaching"].ToString()),
                                    reader["TeachingLocation"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_TeachingExperienceEntity> GetDossierSacrifice_TeachingExperienceDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_TeachingExperienceEntity> lst = new List<DossierSacrifice_TeachingExperienceEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_TeachingExperienceDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TeachingExperienceEntity> GetDossierSacrifice_TeachingExperienceDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_TeachingExperienceDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_TeachingExperienceEntity> GetDossierSacrifice_TeachingExperienceDBCollectionByDossierSacrificeDB(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TeachingExperienceEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_TeachingExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TeachingExperienceGetByDossierSacrifice", new[] { parameter }));
        }

        public  List<DossierSacrifice_TeachingExperienceEntity> GetDossierSacrifice_TeachingExperienceDBCollectionByLessonDB(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@LessonId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TeachingExperienceEntityParam.LessonId;
            return GetDossierSacrifice_TeachingExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TeachingExperienceGetByLesson", new[] { parameter }));
        }


        #endregion

    }

}
