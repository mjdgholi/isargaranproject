﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/18>
    /// Description: <پرونده مشخصات تسهیلات و خدمات پزشکی>
    /// </summary>  


    public class DossierSacrifice_MedicalFacilitieAndServiceDB
    {

      
        #region Methods :

        public void AddDossierSacrifice_MedicalFacilitieAndServiceDB(DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam, out Guid DossierSacrifice_MedicalFacilitieAndServiceId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MedicalFacilitieAndServiceDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@FinancialCost", SqlDbType.Decimal) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceId;
            parameters[3].Value = (DossierSacrifice_MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceDate==""?System.DBNull.Value:(object)DossierSacrifice_MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceDate);
            parameters[4].Value = (DossierSacrifice_MedicalFacilitieAndServiceEntityParam.FinancialCost==null?System.DBNull.Value:(object)DossierSacrifice_MedicalFacilitieAndServiceEntityParam.FinancialCost);
            parameters[5].Value = (DossierSacrifice_MedicalFacilitieAndServiceEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_MedicalFacilitieAndServiceEntityParam.Description.Trim()));            

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MedicalFacilitieAndServiceAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_MedicalFacilitieAndServiceId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_MedicalFacilitieAndServiceDB(DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MedicalFacilitieAndServiceDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@FinancialCost", SqlDbType.Decimal) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.DossierSacrifice_MedicalFacilitieAndServiceId;
parameters[1].Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceId;
            parameters[3].Value = (DossierSacrifice_MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceDate==""?System.DBNull.Value:(object)DossierSacrifice_MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceDate);
            parameters[4].Value = (DossierSacrifice_MedicalFacilitieAndServiceEntityParam.FinancialCost==null?System.DBNull.Value:(object)DossierSacrifice_MedicalFacilitieAndServiceEntityParam.FinancialCost);
            parameters[5].Value = (DossierSacrifice_MedicalFacilitieAndServiceEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MedicalFacilitieAndServiceEntityParam.Description.Trim()));            

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MedicalFacilitieAndServiceUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_MedicalFacilitieAndServiceDB(DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.DossierSacrifice_MedicalFacilitieAndServiceId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MedicalFacilitieAndServiceDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_MedicalFacilitieAndServiceEntity GetSingleDossierSacrifice_MedicalFacilitieAndServiceDB(DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.DossierSacrifice_MedicalFacilitieAndServiceId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MedicalFacilitieAndServiceGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_MedicalFacilitieAndServiceDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetAllDossierSacrifice_MedicalFacilitieAndServiceDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_MedicalFacilitieAndServiceGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetPageDossierSacrifice_MedicalFacilitieAndServiceDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_MedicalFacilitieAndService";
            parameters[5].Value = "DossierSacrifice_MedicalFacilitieAndServiceId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_MedicalFacilitieAndServiceEntity GetDossierSacrifice_MedicalFacilitieAndServiceDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_MedicalFacilitieAndServiceEntity(Guid.Parse(reader["DossierSacrifice_MedicalFacilitieAndServiceId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["MedicalFacilitieAndServiceId"].ToString()),
                                    reader["MedicalFacilitieAndServiceDate"].ToString(),
                                    Convert.IsDBNull(reader["FinancialCost"]) ? null : (decimal?)reader["FinancialCost"],                                    
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_MedicalFacilitieAndServiceEntity> lst = new List<DossierSacrifice_MedicalFacilitieAndServiceEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_MedicalFacilitieAndServiceDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionByDossierSacrificeDB(DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MedicalFacilitieAndServiceGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionByMedicalFacilitieAndServiceDB(DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceId;
            return GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MedicalFacilitieAndServiceGetByMedicalFacilitieAndService", new[] { parameter }));
        }


        #endregion



    }

    
}
