﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: < مهارت خواندن>
    /// </summary>
    #region Class "ReadingSkillDB"

    public class ReadingSkillDB
    {

        #region methods

        public void AddReadingSkillDB(ReadingSkillEntity ReadingSkillEntityParam, out Guid ReadingSkillId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ReadingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ReadingSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@ReadingSkillEnglishTitle", SqlDbType.NVarChar, 200),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(ReadingSkillEntityParam.ReadingSkillPersianTitle.Trim());
            parameters[2].Value = ReadingSkillEntityParam.ReadingSkillEnglishTitle;
            parameters[3].Value = ReadingSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_ReadingSkillAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ReadingSkillId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateReadingSkillDB(ReadingSkillEntity ReadingSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ReadingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ReadingSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@ReadingSkillEnglishTitle", SqlDbType.NVarChar, 200),                                          
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = ReadingSkillEntityParam.ReadingSkillId;
            parameters[1].Value = FarsiToArabic.ToArabic(ReadingSkillEntityParam.ReadingSkillPersianTitle.Trim());
            parameters[2].Value = ReadingSkillEntityParam.ReadingSkillEnglishTitle;
            parameters[3].Value = ReadingSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;          

            _intranetDB.RunProcedure("[Isar].p_ReadingSkillUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteReadingSkillDB(ReadingSkillEntity ReadingSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ReadingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = ReadingSkillEntityParam.ReadingSkillId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_ReadingSkillDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ReadingSkillEntity GetSingleReadingSkillDB(ReadingSkillEntity ReadingSkillEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@ReadingSkillId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = ReadingSkillEntityParam.ReadingSkillId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_ReadingSkillGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetReadingSkillDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ReadingSkillEntity> GetAllReadingSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetReadingSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_ReadingSkillGetAll", new IDataParameter[] {}));
        }

        public List<ReadingSkillEntity> GetAllIsActiveReadingSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetReadingSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_ReadingSkillGetAllIsActive", new IDataParameter[] { }));
        }

        public List<ReadingSkillEntity> GetPageReadingSkillDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_ReadingSkill";
            parameters[5].Value = "ReadingSkillId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetReadingSkillDBCollectionFromDataSet(ds, out count);
        }


        public ReadingSkillEntity GetReadingSkillDBFromDataReader(IDataReader reader)
        {
            return new ReadingSkillEntity(new Guid(reader["ReadingSkillId"].ToString()),
                                          reader["ReadingSkillPersianTitle"].ToString(),
                                          reader["ReadingSkillEnglishTitle"].ToString(),
                                          reader["CreationDate"].ToString(),
                                          reader["ModificationDate"].ToString(),
                                          bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ReadingSkillEntity> GetReadingSkillDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ReadingSkillEntity> lst = new List<ReadingSkillEntity>();
                while (reader.Read())
                    lst.Add(GetReadingSkillDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ReadingSkillEntity> GetReadingSkillDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetReadingSkillDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

    #endregion
}