﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع اقدام>
    /// </summary>
    public class ActionTypeDB
    {            
            #region Methods :

            public void AddActionTypeDB(ActionTypeEntity actionTypeEntityParam, out Guid ActionTypeId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = 	{
								 new SqlParameter("@ActionTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ActionTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
                parameters[0].Direction = ParameterDirection.Output;
                parameters[1].Value = FarsiToArabic.ToArabic(actionTypeEntityParam.ActionTypeTitle);                
                parameters[2].Value = actionTypeEntityParam.IsActive;

                parameters[3].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("isar.p_ActionTypeAdd", parameters);
                var messageError = parameters[3].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
                ActionTypeId = new Guid(parameters[0].Value.ToString());
            }

            public void UpdateActionTypeDB(ActionTypeEntity actionTypeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
			                            	new SqlParameter("@ActionTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ActionTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

                parameters[0].Value = actionTypeEntityParam.ActionTypeId;
                parameters[1].Value = FarsiToArabic.ToArabic(actionTypeEntityParam.ActionTypeTitle);                
                parameters[2].Value = actionTypeEntityParam.IsActive;

                parameters[3].Direction = ParameterDirection.Output;

                _intranetDB.RunProcedure("isar.p_ActionTypeUpdate", parameters);
                var messageError = parameters[3].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public void DeleteActionTypeDB(ActionTypeEntity ActionTypeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@ActionTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
                parameters[0].Value = ActionTypeEntityParam.ActionTypeId;
                parameters[1].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("isar.p_ActionTypeDel", parameters);
                var messageError = parameters[1].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public ActionTypeEntity GetSingleActionTypeDB(ActionTypeEntity ActionTypeEntityParam)
            {
                IDataReader reader = null;
                try
                {

                    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                    IDataParameter[] parameters = {
													new SqlParameter("@ActionTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                    parameters[0].Value = ActionTypeEntityParam.ActionTypeId;

                    reader = _intranetDB.RunProcedureReader("isar.p_ActionTypeGetSingle", parameters);
                    if (reader != null)
                        if (reader.Read())
                            return GetActionTypeDBFromDataReader(reader);
                    return null;
                }

                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<ActionTypeEntity> GetAllActionTypeDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetActionTypeDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("isar.p_ActionTypeGetAll", new IDataParameter[] { }));
            }
            public List<ActionTypeEntity> GetAllActionTypeIsActiveDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetActionTypeDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("isar.p_ActionTypeIsActiveGetAll", new IDataParameter[] { }));
            }
            public List<ActionTypeEntity> GetPageActionTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
                parameters[0].Value = PageSize;
                parameters[1].Value = CurrentPage;
                parameters[2].Value = WhereClause;
                parameters[3].Value = OrderBy;
                parameters[4].Value = "t_ActionType";
                parameters[5].Value = "ActionTypeId";
                DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
                return GetActionTypeDBCollectionFromDataSet(ds, out count);
            }

            public ActionTypeEntity GetActionTypeDBFromDataReader(IDataReader reader)
            {
                return new ActionTypeEntity(Guid.Parse(reader["ActionTypeId"].ToString()),
                                        reader["ActionTypeTitle"].ToString(),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString(),
                                        bool.Parse(reader["IsActive"].ToString()));
            }

            public List<ActionTypeEntity> GetActionTypeDBCollectionFromDataReader(IDataReader reader)
            {
                try
                {
                    List<ActionTypeEntity> lst = new List<ActionTypeEntity>();
                    while (reader.Read())
                        lst.Add(GetActionTypeDBFromDataReader(reader));
                    return lst;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<ActionTypeEntity> GetActionTypeDBCollectionFromDataSet(DataSet ds, out int count)
            {
                count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                return GetActionTypeDBCollectionFromDataReader(ds.CreateDataReader());
            }


            #endregion


    }

        
    }
