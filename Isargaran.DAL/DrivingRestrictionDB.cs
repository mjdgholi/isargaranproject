﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{

    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<محدودیت رانندگی>
    /// </summary>
    public class DrivingRestrictionDB
    {
        #region Methods :

        public void AddDrivingRestrictionDB(DrivingRestrictionEntity DrivingRestrictionEntityParam, out Guid DrivingRestrictionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DrivingRestrictionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DrivingRestrictionTitlePersian", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@DrivingRestrictionTitleEnglish", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(DrivingRestrictionEntityParam.DrivingRestrictionTitlePersian.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(DrivingRestrictionEntityParam.DrivingRestrictionTitleEnglish.Trim());
            parameters[3].Value = DrivingRestrictionEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DrivingRestrictionAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DrivingRestrictionId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDrivingRestrictionDB(DrivingRestrictionEntity DrivingRestrictionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DrivingRestrictionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DrivingRestrictionTitlePersian", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@DrivingRestrictionTitleEnglish", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DrivingRestrictionEntityParam.DrivingRestrictionId;
            parameters[1].Value = FarsiToArabic.ToArabic(DrivingRestrictionEntityParam.DrivingRestrictionTitlePersian.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(DrivingRestrictionEntityParam.DrivingRestrictionTitleEnglish.Trim());
            parameters[3].Value = DrivingRestrictionEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;


            _intranetDB.RunProcedure("Isar.p_DrivingRestrictionUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDrivingRestrictionDB(DrivingRestrictionEntity DrivingRestrictionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DrivingRestrictionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DrivingRestrictionEntityParam.DrivingRestrictionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DrivingRestrictionDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DrivingRestrictionEntity GetSingleDrivingRestrictionDB(DrivingRestrictionEntity DrivingRestrictionEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DrivingRestrictionId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DrivingRestrictionEntityParam.DrivingRestrictionId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DrivingRestrictionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDrivingRestrictionDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DrivingRestrictionEntity> GetAllDrivingRestrictionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDrivingRestrictionDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DrivingRestrictionGetAll", new IDataParameter[] {}));
        }

        public List<DrivingRestrictionEntity> GetAllIsActiveDrivingRestrictionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDrivingRestrictionDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DrivingRestrictionGetAllIsActive", new IDataParameter[] { }));
        }

        public List<DrivingRestrictionEntity> GetPageDrivingRestrictionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_DrivingRestriction";
            parameters[5].Value = "DrivingRestrictionId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDrivingRestrictionDBCollectionFromDataSet(ds, out count);
        }

        public DrivingRestrictionEntity GetDrivingRestrictionDBFromDataReader(IDataReader reader)
        {
            return new DrivingRestrictionEntity(new Guid(reader["DrivingRestrictionId"].ToString()), 
                                                reader["DrivingRestrictionTitlePersian"].ToString(),
                                                reader["DrivingRestrictionTitleEnglish"].ToString(),
                                                reader["CreationDate"].ToString(),
                                                reader["ModificationDate"].ToString(),
                                                bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DrivingRestrictionEntity> GetDrivingRestrictionDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DrivingRestrictionEntity> lst = new List<DrivingRestrictionEntity>();
                while (reader.Read())
                    lst.Add(GetDrivingRestrictionDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DrivingRestrictionEntity> GetDrivingRestrictionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDrivingRestrictionDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }
}