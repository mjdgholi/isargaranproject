﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<اطلاعات  اطلاعات خدمات و تسهیلات غیر نقدی>
    /// </summary>


    public class DossierSacrifice_FacilityAndServiceNonCashDB
    {

        #region Methods :

        public void AddDossierSacrifice_FacilityAndServiceNonCashDB(DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam, out Guid DossierSacrifice_FacilityAndServiceNonCashId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FacilityAndServiceNonCashDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CashAmount", SqlDbType.Decimal) ,
											  new SqlParameter("@RecipientPersonName", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@RecipientPersonNationalNo", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashId;
            parameters[3].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashDate;
            parameters[4].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.OccasionId == null? System.DBNull.Value: (object) DossierSacrifice_FacilityAndServiceNonCashEntityParam.OccasionId);
            parameters[5].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.CashAmount == null? System.DBNull.Value: (object) DossierSacrifice_FacilityAndServiceNonCashEntityParam.CashAmount);
            parameters[6].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonName == ""? System.DBNull.Value: (object)FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonName.Trim()));
            parameters[7].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonNationalNo == ""? System.DBNull.Value: (object)FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonNationalNo.Trim()));
            parameters[8].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.Description == ""? System.DBNull.Value: (object) FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceNonCashEntityParam.Description.Trim()));          

            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilityAndServiceNonCashAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_FacilityAndServiceNonCashId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_FacilityAndServiceNonCashDB(DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FacilityAndServiceNonCashDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CashAmount", SqlDbType.Decimal) ,
											  new SqlParameter("@RecipientPersonName", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@RecipientPersonNationalNo", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.DossierSacrifice_FacilityAndServiceNonCashId;
            parameters[1].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashId;
            parameters[3].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashDate;
            parameters[4].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.OccasionId == null ? System.DBNull.Value : (object)DossierSacrifice_FacilityAndServiceNonCashEntityParam.OccasionId);
            parameters[5].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.CashAmount == null ? System.DBNull.Value : (object)DossierSacrifice_FacilityAndServiceNonCashEntityParam.CashAmount);
            parameters[6].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonName == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonName.Trim()));
            parameters[7].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonNationalNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceNonCashEntityParam.RecipientPersonNationalNo.Trim()));
            parameters[8].Value = (DossierSacrifice_FacilityAndServiceNonCashEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_FacilityAndServiceNonCashEntityParam.Description.Trim()));          

            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilityAndServiceNonCashUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_FacilityAndServiceNonCashDB(DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.DossierSacrifice_FacilityAndServiceNonCashId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilityAndServiceNonCashDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_FacilityAndServiceNonCashEntity GetSingleDossierSacrifice_FacilityAndServiceNonCashDB(DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.DossierSacrifice_FacilityAndServiceNonCashId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilityAndServiceNonCashGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_FacilityAndServiceNonCashDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetAllDossierSacrifice_FacilityAndServiceNonCashDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_FacilityAndServiceNonCashGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetPageDossierSacrifice_FacilityAndServiceNonCashDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_FacilityAndServiceNonCash";
            parameters[5].Value = "DossierSacrifice_FacilityAndServiceNonCashId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_FacilityAndServiceNonCashEntity GetDossierSacrifice_FacilityAndServiceNonCashDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_FacilityAndServiceNonCashEntity(Guid.Parse(reader["DossierSacrifice_FacilityAndServiceNonCashId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["FacilityAndServiceNonCashId"].ToString()),
                                    reader["FacilityAndServiceNonCashDate"].ToString(),
                                    Convert.IsDBNull(reader["OccasionId"]) ? null : (Guid?)reader["OccasionId"],                       
                                    Convert.IsDBNull(reader["CashAmount"]) ? null : (decimal?)reader["CashAmount"],   
                                    reader["RecipientPersonName"].ToString(),
                                    reader["RecipientPersonNationalNo"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_FacilityAndServiceNonCashEntity> lst = new List<DossierSacrifice_FacilityAndServiceNonCashEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_FacilityAndServiceNonCashDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionByDossierSacrificeDB(DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilityAndServiceNonCashGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionByFacilityAndServiceNonCashDB(DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashId;
            return GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilityAndServiceNonCashGetByFacilityAndServiceNonCash", new[] { parameter }));
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionByOccasionDB(DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier);
            parameter.Value =DossierSacrifice_FacilityAndServiceNonCashEntityParam. OccasionId;
            return GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilityAndServiceNonCashGetByOccasion", new[] { parameter }));
        }


        #endregion



    }


}
