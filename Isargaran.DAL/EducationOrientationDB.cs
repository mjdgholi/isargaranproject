﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/12>
    /// Description: <گرایش تحصیلی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "EducationOrientationDB"

    public class EducationOrientationDB
    {



        #region Methods :

        public void AddEducationOrientationDB(EducationOrientationEntity EducationOrientationEntityParam,
            out Guid EducationOrientationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@EducationOrientationPersianTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@EducationOrientationEnglishTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationOrientationEntityParam.EducationOrientationPersianTitle.Trim());
            parameters[2].Value =FarsiToArabic.ToArabic(EducationOrientationEntityParam.EducationOrientationEnglishTitle.Trim());
            parameters[3].Value = EducationOrientationEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationOrientationAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EducationOrientationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationOrientationDB(EducationOrientationEntity EducationOrientationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier),
											  new SqlParameter("@EducationOrientationPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EducationOrientationEnglishTitle", SqlDbType.NVarChar,200) ,									
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EducationOrientationEntityParam.EducationOrientationId;      
            parameters[1].Value = FarsiToArabic.ToArabic(EducationOrientationEntityParam.EducationOrientationPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationOrientationEntityParam.EducationOrientationEnglishTitle.Trim());
            parameters[3].Value = EducationOrientationEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationOrientationUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationOrientationDB(EducationOrientationEntity EducationOrientationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EducationOrientationEntityParam.EducationOrientationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationOrientationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationOrientationEntity GetSingleEducationOrientationDB(EducationOrientationEntity EducationOrientationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EducationOrientationEntityParam.EducationOrientationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationOrientationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationOrientationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationOrientationEntity> GetAllEducationOrientationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationOrientationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationOrientationGetAll", new IDataParameter[] { }));
        }
        public List<EducationOrientationEntity> GetAllEducationOrientationIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationOrientationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationOrientationIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<EducationOrientationEntity> GetPageEducationOrientationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EducationOrientation";
            parameters[5].Value = "EducationOrientationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationOrientationDBCollectionFromDataSet(ds, out count);
        }

        public EducationOrientationEntity GetEducationOrientationDBFromDataReader(IDataReader reader)
        {
            return new EducationOrientationEntity(Guid.Parse(reader["EducationOrientationId"].ToString()),
                                    reader["EducationOrientationPersianTitle"].ToString(),
                                    reader["EducationOrientationEnglishTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationOrientationEntity> GetEducationOrientationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationOrientationEntity> lst = new List<EducationOrientationEntity>();
                while (reader.Read())
                    lst.Add(GetEducationOrientationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationOrientationEntity> GetEducationOrientationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationOrientationDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
