﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <پرونده  ایثارگری ایثارگری>
    /// </summary>



    public class DossierSacrifice_SacrificeDB
    {

        #region Methods :

        public void AddDossierSacrifice_SacrificeDB(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam, out Guid DossierSacrifice_SacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_SacrificeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@sacrificeId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@ValueInYear", SqlDbType.Int) ,
											  new SqlParameter("@ValueInMonth", SqlDbType.Int) ,
											  new SqlParameter("@ValueInDay", SqlDbType.Int) ,
                                              new SqlParameter("@DurationTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PercentValue", SqlDbType.Decimal) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@Endate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_SacrificeEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_SacrificeEntityParam.SacrificeId;
            parameters[3].Value = (DossierSacrifice_SacrificeEntityParam.ValueInYear.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.ValueInYear);
            parameters[4].Value = (DossierSacrifice_SacrificeEntityParam.ValueInMonth.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.ValueInMonth);
            parameters[5].Value = (DossierSacrifice_SacrificeEntityParam.ValueInDay.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.ValueInDay);
            parameters[6].Value = (DossierSacrifice_SacrificeEntityParam.DurationTypeId == null ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.DurationTypeId);
            parameters[7].Value = (DossierSacrifice_SacrificeEntityParam.PercentValue.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.PercentValue);
            parameters[8].Value = (DossierSacrifice_SacrificeEntityParam.StartDate==""?System.DBNull.Value:(object)DossierSacrifice_SacrificeEntityParam.StartDate);
            parameters[9].Value = (DossierSacrifice_SacrificeEntityParam.Endate==""?System.DBNull.Value:(object)DossierSacrifice_SacrificeEntityParam.Endate);            
            parameters[10].Value = DossierSacrifice_SacrificeEntityParam.IsActive;

            parameters[11].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SacrificeAdd", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_SacrificeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_SacrificeDB(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_SacrificeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@sacrificeId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@ValueInYear", SqlDbType.Int) ,
											  new SqlParameter("@ValueInMonth", SqlDbType.Int) ,
											  new SqlParameter("@ValueInDay", SqlDbType.Int) ,
                                              new SqlParameter("@DurationTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PercentValue", SqlDbType.Decimal) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@Endate", SqlDbType.SmallDateTime) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_SacrificeEntityParam.DossierSacrifice_SacrificeId;
            parameters[1].Value = DossierSacrifice_SacrificeEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_SacrificeEntityParam.SacrificeId;
            parameters[3].Value = (DossierSacrifice_SacrificeEntityParam.ValueInYear.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.ValueInYear);
            parameters[4].Value = (DossierSacrifice_SacrificeEntityParam.ValueInMonth.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.ValueInMonth);
            parameters[5].Value = (DossierSacrifice_SacrificeEntityParam.ValueInDay.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.ValueInDay);
            parameters[6].Value = (DossierSacrifice_SacrificeEntityParam.DurationTypeId == null ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.DurationTypeId);
            parameters[7].Value = (DossierSacrifice_SacrificeEntityParam.PercentValue.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.PercentValue);
            parameters[8].Value = (DossierSacrifice_SacrificeEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.StartDate);
            parameters[9].Value = (DossierSacrifice_SacrificeEntityParam.Endate == "" ? System.DBNull.Value : (object)DossierSacrifice_SacrificeEntityParam.Endate);
            parameters[10].Value = DossierSacrifice_SacrificeEntityParam.IsActive;

            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SacrificeUpdate", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_SacrificeDB(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_SacrificeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_SacrificeEntityParam.DossierSacrifice_SacrificeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SacrificeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_SacrificeEntity GetSingleDossierSacrifice_SacrificeDB(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_SacrificeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_SacrificeEntityParam.DossierSacrifice_SacrificeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SacrificeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_SacrificeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_SacrificeEntity> GetAllDossierSacrifice_SacrificeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_SacrificeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_SacrificeGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_SacrificeEntity> GetPageDossierSacrifice_SacrificeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Sacrifice";
            parameters[5].Value = "DossierSacrifice_SacrificeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_SacrificeDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_SacrificeEntity GetDossierSacrifice_SacrificeDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_SacrificeEntity(Guid.Parse(reader["DossierSacrifice_SacrificeId"].ToString()),
                        Guid.Parse(reader["DossierSacrificeId"].ToString()),
                        Guid.Parse(reader["sacrificeId"].ToString()),
                        Convert.IsDBNull(reader["TotalValueInDay"]) ? null : (int?)reader["TotalValueInDay"],
                        Convert.IsDBNull(reader["ValueInYear"]) ? null : (int?)reader["ValueInYear"],
                        Convert.IsDBNull(reader["ValueInMonth"]) ? null : (int?)reader["ValueInMonth"],
                        Convert.IsDBNull(reader["ValueInDay"]) ? null : (int?)reader["ValueInDay"],
                        Convert.IsDBNull(reader["DurationTypeId"]) ? null : (Guid?)reader["DurationTypeId"],
                        Convert.IsDBNull(reader["PercentValue"]) ? null : (decimal?)reader["PercentValue"],
                        reader["StartDate"].ToString(),
                        reader["Endate"].ToString(),
                        reader["CreationDate"].ToString(),
                        reader["ModificationDate"].ToString(),
                        bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DossierSacrifice_SacrificeEntity> GetDossierSacrifice_SacrificeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_SacrificeEntity> lst = new List<DossierSacrifice_SacrificeEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_SacrificeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_SacrificeEntity> GetDossierSacrifice_SacrificeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_SacrificeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_SacrificeEntity> GetDossierSacrifice_SacrificeDBCollectionByDossierSacrificeDB(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_SacrificeEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_SacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SacrificeGetByDossierSacrifice", new[] { parameter }));
        }

        public  List<DossierSacrifice_SacrificeEntity> GetDossierSacrifice_SacrificeDBCollectionBySacrificeDB(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@sacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_SacrificeEntityParam.SacrificeId;
            return GetDossierSacrifice_SacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SacrificeGetBySacrifice", new[] { parameter }));
        }


        #endregion



    }


}
