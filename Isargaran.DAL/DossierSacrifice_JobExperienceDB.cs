﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <پرونده مشخصات سوابق کاری>
    /// </summary>   


    public class DossierSacrifice_JobExperienceDB
    {
        #region Methods :

        public void AddDossierSacrifice_JobExperienceDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam, out Guid DossierSacrifice_JobExperienceId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_JobExperienceId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@PostId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PersonnelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CommandmentNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FreeJob", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DirectManager", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@PostalCod", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_JobExperienceEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_JobExperienceEntityParam.OrganizationPhysicalChartId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.OrganizationPhysicalChartId);
            parameters[3].Value = (DossierSacrifice_JobExperienceEntityParam.EmploymentTypeId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.EmploymentTypeId);
            parameters[4].Value = (DossierSacrifice_JobExperienceEntityParam.EmploymentDate==""?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.EmploymentDate);
            parameters[5].Value = (DossierSacrifice_JobExperienceEntityParam.PostId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.PostId);
            parameters[6].Value = (DossierSacrifice_JobExperienceEntityParam.PersonnelNo==""?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.PersonnelNo);
            parameters[7].Value = (DossierSacrifice_JobExperienceEntityParam.CommandmentNo==""?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.CommandmentNo);
            parameters[8].Value = (DossierSacrifice_JobExperienceEntityParam.FreeJob==""?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.FreeJob);
            parameters[9].Value = (DossierSacrifice_JobExperienceEntityParam.DurationTimeAtWorkId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.DurationTimeAtWorkId);
            parameters[10].Value = (DossierSacrifice_JobExperienceEntityParam.DirectManager==""?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.DirectManager);
            parameters[11].Value = (DossierSacrifice_JobExperienceEntityParam.CityId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_JobExperienceEntityParam.CityId);
            parameters[12].Value = (DossierSacrifice_JobExperienceEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.TelNo.Trim()));
            parameters[13].Value = (DossierSacrifice_JobExperienceEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.FaxNo.Trim()));
            parameters[14].Value = (DossierSacrifice_JobExperienceEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.Address.Trim()));
            parameters[15].Value = (DossierSacrifice_JobExperienceEntityParam.PostalCod == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.PostalCod.Trim()));
            parameters[16].Value = (DossierSacrifice_JobExperienceEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.Email.Trim()));
            parameters[17].Value = (DossierSacrifice_JobExperienceEntityParam.WebAddress==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.WebAddress.Trim()));
            parameters[18].Value = DossierSacrifice_JobExperienceEntityParam.StartDate;
            parameters[19].Value = (DossierSacrifice_JobExperienceEntityParam.EndDate==""?System.DBNull.Value:(object)(DossierSacrifice_JobExperienceEntityParam.EndDate));            

            parameters[20].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_JobExperienceAdd", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_JobExperienceId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_JobExperienceDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_JobExperienceId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@PostId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PersonnelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CommandmentNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FreeJob", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DirectManager", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@PostalCod", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_JobExperienceEntityParam.DossierSacrifice_JobExperienceId;
            parameters[1].Value = DossierSacrifice_JobExperienceEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_JobExperienceEntityParam.OrganizationPhysicalChartId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.OrganizationPhysicalChartId);
            parameters[3].Value = (DossierSacrifice_JobExperienceEntityParam.EmploymentTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.EmploymentTypeId);
            parameters[4].Value = (DossierSacrifice_JobExperienceEntityParam.EmploymentDate == "" ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.EmploymentDate);
            parameters[5].Value = (DossierSacrifice_JobExperienceEntityParam.PostId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.PostId);
            parameters[6].Value = (DossierSacrifice_JobExperienceEntityParam.PersonnelNo == "" ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.PersonnelNo);
            parameters[7].Value = (DossierSacrifice_JobExperienceEntityParam.CommandmentNo == "" ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.CommandmentNo);
            parameters[8].Value = (DossierSacrifice_JobExperienceEntityParam.FreeJob == "" ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.FreeJob);
            parameters[9].Value = (DossierSacrifice_JobExperienceEntityParam.DurationTimeAtWorkId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.DurationTimeAtWorkId);
            parameters[10].Value = (DossierSacrifice_JobExperienceEntityParam.DirectManager == "" ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.DirectManager);
            parameters[11].Value = (DossierSacrifice_JobExperienceEntityParam.CityId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_JobExperienceEntityParam.CityId);
            parameters[12].Value = (DossierSacrifice_JobExperienceEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.TelNo.Trim()));
            parameters[13].Value = (DossierSacrifice_JobExperienceEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.FaxNo.Trim()));
            parameters[14].Value = (DossierSacrifice_JobExperienceEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.Address.Trim()));
            parameters[15].Value = (DossierSacrifice_JobExperienceEntityParam.PostalCod == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.PostalCod.Trim()));
            parameters[16].Value = (DossierSacrifice_JobExperienceEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.Email.Trim()));
            parameters[17].Value = (DossierSacrifice_JobExperienceEntityParam.WebAddress == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_JobExperienceEntityParam.WebAddress.Trim()));
            parameters[18].Value = DossierSacrifice_JobExperienceEntityParam.StartDate;
            parameters[19].Value = (DossierSacrifice_JobExperienceEntityParam.EndDate == "" ? System.DBNull.Value : (object)(DossierSacrifice_JobExperienceEntityParam.EndDate));     

            parameters[20].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_JobExperienceUpdate", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_JobExperienceDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_JobExperienceId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_JobExperienceEntityParam.DossierSacrifice_JobExperienceId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_JobExperienceDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_JobExperienceEntity GetSingleDossierSacrifice_JobExperienceDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_JobExperienceId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_JobExperienceEntityParam.DossierSacrifice_JobExperienceId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_JobExperienceGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_JobExperienceDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_JobExperienceEntity> GetAllDossierSacrifice_JobExperienceDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_JobExperienceGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_JobExperienceEntity> GetPageDossierSacrifice_JobExperienceDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_JobExperience";
            parameters[5].Value = "DossierSacrifice_JobExperienceId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_JobExperienceEntity GetDossierSacrifice_JobExperienceDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_JobExperienceEntity(Guid.Parse(reader["DossierSacrifice_JobExperienceId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Convert.IsDBNull(reader["OrganizationPhysicalChartId"]) ? null : (Guid?)reader["OrganizationPhysicalChartId"],
                                    Convert.IsDBNull(reader["EmploymentTypeId"]) ? null : (Guid?)reader["EmploymentTypeId"],                                    
                                    reader["EmploymentDate"].ToString(),
                                    Convert.IsDBNull(reader["PostId"]) ? null : (Guid?)reader["PostId"],                                    
                                    reader["PersonnelNo"].ToString(),
                                    reader["CommandmentNo"].ToString(),
                                    reader["FreeJob"].ToString(),
                                    Convert.IsDBNull(reader["DurationTimeAtWorkId"]) ? null : (Guid?)reader["DurationTimeAtWorkId"],                                    
                                    reader["DirectManager"].ToString(),
                                    Convert.IsDBNull(reader["CityId"]) ? null : (Guid?)reader["CityId"],                                    
                                    reader["TelNo"].ToString(),
                                    reader["FaxNo"].ToString(),
                                    reader["Address"].ToString(),
                                    reader["PostalCod"].ToString(),
                                    reader["Email"].ToString(),
                                    reader["WebAddress"].ToString(),
                                    reader["StartDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    Convert.IsDBNull(reader["ProvinceId"]) ? null : (Guid?)reader["ProvinceId"],
                                     reader["OrganizationPhysicalChartTitle"].ToString());
        }

        public List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_JobExperienceEntity> lst = new List<DossierSacrifice_JobExperienceEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_JobExperienceDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionByCityDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CityId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_JobExperienceEntityParam.CityId;
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_JobExperienceGetByCity", new[] { parameter }));
        }

        public  List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionByDossierSacrificeDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_JobExperienceEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_JobExperienceGetByDossierSacrifice", new[] { parameter }));
        }

        public  List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionByDurationTimeAtWorkDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_JobExperienceEntityParam.DurationTimeAtWorkId;
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_JobExperienceGetByDurationTimeAtWork", new[] { parameter }));
        }

        public  List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionByEmploymentTypeDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value =DossierSacrifice_JobExperienceEntityParam.EmploymentTypeId;
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_JobExperienceGetByEmploymentType", new[] { parameter }));
        }

        public  List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionByOrganizationPhysicalChartDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_JobExperienceEntityParam.OrganizationPhysicalChartId;
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_JobExperienceGetByOrganizationPhysicalChart", new[] { parameter }));
        }

        public  List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceDBCollectionByPostDB(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PostId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_JobExperienceEntityParam.PostId;
            return GetDossierSacrifice_JobExperienceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_JobExperienceGetByPost", new[] { parameter }));
        }


        #endregion



    }

    
}
