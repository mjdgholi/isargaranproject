﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات تقدیر کتبی>
    /// </summary>


    public class DossierSacrifice_EvidenceAppreciationDB
    {


        #region Methods :

        public void AddDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam, out Guid DossierSacrifice_EvidenceAppreciationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_EvidenceAppreciationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EvidenceAppreciationContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@EvidenceAppreciationDescription", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsForeignissued", SqlDbType.Bit) ,
											  new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_EvidenceAppreciationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_EvidenceAppreciationEntityParam.OccasionId;
            parameters[3].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.OccasionDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EvidenceAppreciationEntityParam.OccasionDate);
            parameters[4].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationContent == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationContent.Trim()));
            parameters[5].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationDescription == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationDescription.Trim()));
            parameters[6].Value = DossierSacrifice_EvidenceAppreciationEntityParam.IsForeignissued;
            parameters[7].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.OrganizationId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_EvidenceAppreciationEntityParam.OrganizationId);            

            parameters[8].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EvidenceAppreciationAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_EvidenceAppreciationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_EvidenceAppreciationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EvidenceAppreciationContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@EvidenceAppreciationDescription", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsForeignissued", SqlDbType.Bit) ,
											  new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_EvidenceAppreciationEntityParam.DossierSacrifice_EvidenceAppreciationId;
            parameters[1].Value = DossierSacrifice_EvidenceAppreciationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_EvidenceAppreciationEntityParam.OccasionId;
            parameters[3].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.OccasionDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EvidenceAppreciationEntityParam.OccasionDate);
            parameters[4].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationContent == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationContent.Trim()));
            parameters[5].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationDescription == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EvidenceAppreciationEntityParam.EvidenceAppreciationDescription.Trim()));
            parameters[6].Value = DossierSacrifice_EvidenceAppreciationEntityParam.IsForeignissued;
            parameters[7].Value = (DossierSacrifice_EvidenceAppreciationEntityParam.OrganizationId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_EvidenceAppreciationEntityParam.OrganizationId);  

            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EvidenceAppreciationUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_EvidenceAppreciationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_EvidenceAppreciationEntityParam.DossierSacrifice_EvidenceAppreciationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EvidenceAppreciationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_EvidenceAppreciationEntity GetSingleDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_EvidenceAppreciationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_EvidenceAppreciationEntityParam.DossierSacrifice_EvidenceAppreciationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EvidenceAppreciationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_EvidenceAppreciationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetAllDossierSacrifice_EvidenceAppreciationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_EvidenceAppreciationGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetPageDossierSacrifice_EvidenceAppreciationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_EvidenceAppreciation";
            parameters[5].Value = "DossierSacrifice_EvidenceAppreciationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_EvidenceAppreciationEntity GetDossierSacrifice_EvidenceAppreciationDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_EvidenceAppreciationEntity(Guid.Parse(reader["DossierSacrifice_EvidenceAppreciationId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["OccasionId"].ToString()),
                                    reader["OccasionDate"].ToString(),
                                    reader["EvidenceAppreciationContent"].ToString(),
                                    reader["EvidenceAppreciationDescription"].ToString(),
                                    bool.Parse(reader["IsForeignissued"].ToString()),
                                    Convert.IsDBNull(reader["OrganizationId"]) ? null : (Guid?)reader["OrganizationId"],                                    
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_EvidenceAppreciationEntity> lst = new List<DossierSacrifice_EvidenceAppreciationEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_EvidenceAppreciationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationDBCollectionByDossierSacrificeDB(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EvidenceAppreciationEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EvidenceAppreciationGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationDBCollectionByOccasionDB(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EvidenceAppreciationEntityParam.OccasionId;
            return GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EvidenceAppreciationGetByOccasion", new[] { parameter }));
        }

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationDBCollectionByOrganizationDB(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EvidenceAppreciationEntityParam.OrganizationId;
            return GetDossierSacrifice_EvidenceAppreciationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EvidenceAppreciationGetByOrganization", new[] { parameter }));
        }


        #endregion



    }

}
