﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/28>
    // Description:	<مناطق جنگی>
    /// </summary>



    public class WarZoneDB
    {

   

        #region Methods :

        public void AddWarZoneDB(WarZoneEntity WarZoneEntityParam, out Guid WarZoneId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@WarZoneId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@WarZoneTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(WarZoneEntityParam.WarZoneTitle.Trim());            
            parameters[2].Value = WarZoneEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_WarZoneAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            WarZoneId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateWarZoneDB(WarZoneEntity WarZoneEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@WarZoneId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@WarZoneTitle", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = WarZoneEntityParam.WarZoneId;
            parameters[1].Value = FarsiToArabic.ToArabic(WarZoneEntityParam.WarZoneTitle.Trim());            
            parameters[2].Value = WarZoneEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_WarZoneUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteWarZoneDB(WarZoneEntity WarZoneEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@WarZoneId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = WarZoneEntityParam.WarZoneId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_WarZoneDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public WarZoneEntity GetSingleWarZoneDB(WarZoneEntity WarZoneEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@WarZoneId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = WarZoneEntityParam.WarZoneId;

                reader = _intranetDB.RunProcedureReader("Isar.p_WarZoneGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetWarZoneDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WarZoneEntity> GetAllWarZoneDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWarZoneDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_WarZoneGetAll", new IDataParameter[] { }));
        }
        public List<WarZoneEntity> GetAllWarZoneIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWarZoneDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_WarZoneIsActiveGetAll", new IDataParameter[] { }));
        }
        
        public List<WarZoneEntity> GetPageWarZoneDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_WarZone";
            parameters[5].Value = "WarZoneId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetWarZoneDBCollectionFromDataSet(ds, out count);
        }

        public WarZoneEntity GetWarZoneDBFromDataReader(IDataReader reader)
        {
            return new WarZoneEntity(Guid.Parse(reader["WarZoneId"].ToString()),
                                    reader["WarZoneTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<WarZoneEntity> GetWarZoneDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<WarZoneEntity> lst = new List<WarZoneEntity>();
                while (reader.Read())
                    lst.Add(GetWarZoneDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WarZoneEntity> GetWarZoneDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetWarZoneDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }
}
