﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/23>
    // Description:	<یگان اعزام کننده>
    /// </summary>

    #region Class "MilitaryUnitDispatcherDB"

    public class MilitaryUnitDispatcherDB
    { 
        #region Methods :

        public void AddMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam, out Guid MilitaryUnitDispatcherId)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MilitaryUnitDispatcherTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value =FarsiToArabic.ToArabic(MilitaryUnitDispatcherEntityParam.MilitaryUnitDispatcherTitle.Trim());
            parameters[2].Value = MilitaryUnitDispatcherEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;
            intranetDB.RunProcedure("Isar.p_MilitaryUnitDispatcherAdd", parameters);

            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MilitaryUnitDispatcherId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MilitaryUnitDispatcherTitle", SqlDbType.NVarChar, 200),                                         
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = MilitaryUnitDispatcherEntityParam.MilitaryUnitDispatcherId;
            parameters[1].Value = FarsiToArabic.ToArabic(MilitaryUnitDispatcherEntityParam.MilitaryUnitDispatcherTitle.Trim());
            parameters[2].Value = MilitaryUnitDispatcherEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            intranetDB.RunProcedure("Isar.p_MilitaryUnitDispatcherUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam)
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = MilitaryUnitDispatcherEntityParam.MilitaryUnitDispatcherId;
            parameters[1].Direction = ParameterDirection.Output;
            intranetDB.RunProcedure("Isar.p_MilitaryUnitDispatcherDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MilitaryUnitDispatcherEntity GetSingleMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = MilitaryUnitDispatcherEntityParam.MilitaryUnitDispatcherId;

                reader = intranetDB.RunProcedureReader("Isar.p_MilitaryUnitDispatcherGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMilitaryUnitDispatcherDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MilitaryUnitDispatcherEntity> GetAllMilitaryUnitDispatcherDB()
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            return GetMilitaryUnitDispatcherDBCollectionFromDataReader(
                intranetDB.RunProcedureReader
                    ("Isar.p_MilitaryUnitDispatcherGetAll", new IDataParameter[] {}));
        }

        public List<MilitaryUnitDispatcherEntity> GetAllIsActiveMilitaryUnitDispatcherDB()
        {
            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            return GetMilitaryUnitDispatcherDBCollectionFromDataReader(
                intranetDB.RunProcedureReader
                    ("Isar.p_MilitaryUnitDispatcherGetAllIsActive", new IDataParameter[] { }));
        }

        public List<MilitaryUnitDispatcherEntity> GetPageMilitaryUnitDispatcherDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MilitaryUnitDispatcher";
            parameters[5].Value = "MilitaryUnitDispatcherId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetMilitaryUnitDispatcherDBCollectionFromDataSet(ds, out count);
        }

        public MilitaryUnitDispatcherEntity GetMilitaryUnitDispatcherDBFromDataReader(IDataReader reader)
        {
            return new MilitaryUnitDispatcherEntity(new Guid(reader["MilitaryUnitDispatcherId"].ToString()), 
                                                    reader["MilitaryUnitDispatcherTitle"].ToString(),
                                                    reader["CreationDate"].ToString(),
                                                    reader["ModificationDate"].ToString(),
                                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<MilitaryUnitDispatcherEntity> GetMilitaryUnitDispatcherDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MilitaryUnitDispatcherEntity> lst = new List<MilitaryUnitDispatcherEntity>();
                while (reader.Read())
                    lst.Add(GetMilitaryUnitDispatcherDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MilitaryUnitDispatcherEntity> GetMilitaryUnitDispatcherDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMilitaryUnitDispatcherDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

    #endregion
}