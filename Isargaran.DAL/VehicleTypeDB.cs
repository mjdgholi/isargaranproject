﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/18>
    // Description:	<نوع وسیله حمل و نقل>
    /// </summary>

    //-----------------------------------------------------
    #region Class "VehicleTypeDB"

    public class VehicleTypeDB
    {



        #region Methods :

        public void AddVehicleTypeDB(VehicleTypeEntity VehicleTypeEntityParam, out Guid VehicleTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@VehicleTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@VehicleTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(VehicleTypeEntityParam.VehicleTypeTitle.Trim());
            parameters[2].Value = VehicleTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_VehicleTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            VehicleTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateVehicleTypeDB(VehicleTypeEntity VehicleTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@VehicleTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@VehicleTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = VehicleTypeEntityParam.VehicleTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(VehicleTypeEntityParam.VehicleTypeTitle.Trim());
            parameters[2].Value = VehicleTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_VehicleTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteVehicleTypeDB(VehicleTypeEntity VehicleTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@VehicleTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = VehicleTypeEntityParam.VehicleTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_VehicleTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public VehicleTypeEntity GetSingleVehicleTypeDB(VehicleTypeEntity VehicleTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@VehicleTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = VehicleTypeEntityParam.VehicleTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_VehicleTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetVehicleTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<VehicleTypeEntity> GetAllVehicleTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetVehicleTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_VehicleTypeGetAll", new IDataParameter[] { }));
        }
        public List<VehicleTypeEntity> GetAllVehicleTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetVehicleTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_VehicleTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<VehicleTypeEntity> GetPageVehicleTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_VehicleType";
            parameters[5].Value = "VehicleTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetVehicleTypeDBCollectionFromDataSet(ds, out count);
        }

        public VehicleTypeEntity GetVehicleTypeDBFromDataReader(IDataReader reader)
        {
            return new VehicleTypeEntity(Guid.Parse(reader["VehicleTypeId"].ToString()),
                                    reader["VehicleTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<VehicleTypeEntity> GetVehicleTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<VehicleTypeEntity> lst = new List<VehicleTypeEntity>();
                while (reader.Read())
                    lst.Add(GetVehicleTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<VehicleTypeEntity> GetVehicleTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetVehicleTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
