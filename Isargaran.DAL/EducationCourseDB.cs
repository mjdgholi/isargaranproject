﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <رشته تحصیلی>
    /// </summary>



    public class EducationCourseDB
    {

        #region Methods :

        public void AddEducationCourseDB(EducationCourseEntity EducationCourseEntityParam, out Guid EducationCourseId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EducationCoursePersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EducationCourseEnglishTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationCourseEntityParam.EducationCoursePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationCourseEntityParam.EducationCourseEnglishTitle.Trim());
            parameters[3].Value = EducationCourseEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCourseAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EducationCourseId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationCourseDB(EducationCourseEntity EducationCourseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EducationCoursePersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EducationCourseEnglishTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EducationCourseEntityParam.EducationCourseId;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationCourseEntityParam.EducationCoursePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationCourseEntityParam.EducationCourseEnglishTitle.Trim());
            parameters[3].Value = EducationCourseEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationCourseUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationCourseDB(EducationCourseEntity EducationCourseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EducationCourseEntityParam.EducationCourseId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCourseDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationCourseEntity GetSingleEducationCourseDB(EducationCourseEntity EducationCourseEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EducationCourseEntityParam.EducationCourseId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationCourseGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationCourseDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCourseEntity> GetAllEducationCourseDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationCourseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationCourseGetAll", new IDataParameter[] { }));
        }
        public List<EducationCourseEntity> GetAllEducationCourseIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationCourseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationCourseIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<EducationCourseEntity> GetPageEducationCourseDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EducationCourse";
            parameters[5].Value = "EducationCourseId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationCourseDBCollectionFromDataSet(ds, out count);
        }

        public EducationCourseEntity GetEducationCourseDBFromDataReader(IDataReader reader)
        {
            return new EducationCourseEntity(Guid.Parse(reader["EducationCourseId"].ToString()),
                                    reader["EducationCoursePersianTitle"].ToString(),
                                    reader["EducationCourseEnglishTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationCourseEntity> GetEducationCourseDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationCourseEntity> lst = new List<EducationCourseEntity>();
                while (reader.Read())
                    lst.Add(GetEducationCourseDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCourseEntity> GetEducationCourseDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationCourseDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }
}
