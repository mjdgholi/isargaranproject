﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/28>
    // Description:	<دوره زمانی ایثارگر>
    /// </summary>
    public class SacrificePeriodTimeDB
    {
        #region Methods :

        public void AddSacrificePeriodTimeDB(SacrificePeriodTimeEntity SacrificePeriodTimeEntityParam, out Guid SacrificePeriodTimeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SacrificePeriodTimeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SacrificePeriodTimeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Periority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(SacrificePeriodTimeEntityParam.SacrificePeriodTimeTitle.Trim());
            parameters[2].Value = SacrificePeriodTimeEntityParam.Periority;
            parameters[3].Value = SacrificePeriodTimeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_SacrificePeriodTimeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SacrificePeriodTimeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSacrificePeriodTimeDB(SacrificePeriodTimeEntity SacrificePeriodTimeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SacrificePeriodTimeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@SacrificePeriodTimeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Periority", SqlDbType.Int) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = SacrificePeriodTimeEntityParam.SacrificePeriodTimeId;
            parameters[1].Value = FarsiToArabic.ToArabic(SacrificePeriodTimeEntityParam.SacrificePeriodTimeTitle.Trim());
            parameters[2].Value = SacrificePeriodTimeEntityParam.Periority;
            parameters[3].Value = SacrificePeriodTimeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_SacrificePeriodTimeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSacrificePeriodTimeDB(SacrificePeriodTimeEntity SacrificePeriodTimeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SacrificePeriodTimeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SacrificePeriodTimeEntityParam.SacrificePeriodTimeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_SacrificePeriodTimeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SacrificePeriodTimeEntity GetSingleSacrificePeriodTimeDB(SacrificePeriodTimeEntity SacrificePeriodTimeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@SacrificePeriodTimeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = SacrificePeriodTimeEntityParam.SacrificePeriodTimeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_SacrificePeriodTimeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSacrificePeriodTimeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SacrificePeriodTimeEntity> GetAllSacrificePeriodTimeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSacrificePeriodTimeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_SacrificePeriodTimeGetAll", new IDataParameter[] { }));
        }
        public List<SacrificePeriodTimeEntity> GetAllSacrificePeriodTimeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSacrificePeriodTimeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_SacrificePeriodTimeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<SacrificePeriodTimeEntity> GetPageSacrificePeriodTimeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_SacrificePeriodTime";
            parameters[5].Value = "SacrificePeriodTimeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetSacrificePeriodTimeDBCollectionFromDataSet(ds, out count);
        }

        public SacrificePeriodTimeEntity GetSacrificePeriodTimeDBFromDataReader(IDataReader reader)
        {
            return new SacrificePeriodTimeEntity(Guid.Parse(reader["SacrificePeriodTimeId"].ToString()),
                                    reader["SacrificePeriodTimeTitle"].ToString(),
                                    int.Parse(reader["Periority"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SacrificePeriodTimeEntity> GetSacrificePeriodTimeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SacrificePeriodTimeEntity> lst = new List<SacrificePeriodTimeEntity>();
                while (reader.Read())
                    lst.Add(GetSacrificePeriodTimeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SacrificePeriodTimeEntity> GetSacrificePeriodTimeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSacrificePeriodTimeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
