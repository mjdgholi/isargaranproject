﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/14>
    /// Description: <پرونده هنری ایثارگر>
    /// </summary>



    public class DossierSacrifice_ArtDB
    {
        #region Methods :

        public void AddDossierSacrifice_ArtDB(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam, out Guid DossierSacrifice_ArtId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_ArtId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ArtCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasEvidence", SqlDbType.Bit) ,
											  new SqlParameter("@PlaceOfGraduation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_ArtEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_ArtEntityParam.ArtCourseId;
            parameters[3].Value = (DossierSacrifice_ArtEntityParam.HasEvidence==null?System.DBNull.Value:(object)DossierSacrifice_ArtEntityParam.HasEvidence);
            parameters[4].Value = (DossierSacrifice_ArtEntityParam.PlaceOfGraduation==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_ArtEntityParam.PlaceOfGraduation.Trim()));
            parameters[5].Value = (DossierSacrifice_ArtEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_ArtEntityParam.Description.Trim()));            

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ArtAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_ArtId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_ArtDB(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_ArtId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ArtCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasEvidence", SqlDbType.Bit) ,
											  new SqlParameter("@PlaceOfGraduation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_ArtEntityParam.DossierSacrifice_ArtId;
            parameters[1].Value = DossierSacrifice_ArtEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_ArtEntityParam.ArtCourseId;
            parameters[3].Value = (DossierSacrifice_ArtEntityParam.HasEvidence==null?System.DBNull.Value:(object)DossierSacrifice_ArtEntityParam.HasEvidence);
            parameters[4].Value = (DossierSacrifice_ArtEntityParam.PlaceOfGraduation == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ArtEntityParam.PlaceOfGraduation.Trim()));
            parameters[5].Value = (DossierSacrifice_ArtEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ArtEntityParam.Description.Trim()));  
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ArtUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_ArtDB(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_ArtId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_ArtEntityParam.DossierSacrifice_ArtId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ArtDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_ArtEntity GetSingleDossierSacrifice_ArtDB(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_ArtId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_ArtEntityParam.DossierSacrifice_ArtId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ArtGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_ArtDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ArtEntity> GetAllDossierSacrifice_ArtDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_ArtDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_ArtGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_ArtEntity> GetPageDossierSacrifice_ArtDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Art";
            parameters[5].Value = "DossierSacrifice_ArtId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_ArtDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_ArtEntity GetDossierSacrifice_ArtDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_ArtEntity(Guid.Parse(reader["DossierSacrifice_ArtId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["ArtCourseId"].ToString()),
                                      Convert.IsDBNull(reader["HasEvidence"]) ? null : (bool?)reader["HasEvidence"],                                       
                                    reader["PlaceOfGraduation"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_ArtEntity> GetDossierSacrifice_ArtDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_ArtEntity> lst = new List<DossierSacrifice_ArtEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_ArtDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ArtEntity> GetDossierSacrifice_ArtDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_ArtDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_ArtEntity> GetDossierSacrifice_ArtDBCollectionByArtCourseDB(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ArtCourseId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_ArtEntityParam.ArtCourseId;
            return GetDossierSacrifice_ArtDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ArtGetByArtCourse", new[] { parameter }));
        }

        public List<DossierSacrifice_ArtEntity> GetDossierSacrifice_ArtDBCollectionByDossierSacrificeDB(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_ArtEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_ArtDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ArtGetByDossierSacrifice", new[] { parameter }));
        }


        #endregion



    }

}
