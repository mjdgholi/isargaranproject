﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/27>
    /// Description: <اطلاعات خاص>
    /// </summary>
    public class DossierSacrifice_SpecificDB
    {

        public void AddDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam, out Guid DossierSacrifice_SpecificId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_SpecificId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@Email", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@WebAddress", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@BankId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BankBranchCode", SqlDbType.NVarChar,20),
                                            new SqlParameter("@BankBranchTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@AccountNo", SqlDbType.NVarChar, 20),
                                            new SqlParameter("@OpeningAccountDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@IsHasSupplementaryInsurance", SqlDbType.Bit),
                                            new SqlParameter("@SupplementaryInsuranceNo", SqlDbType.NVarChar,20),
                                            new SqlParameter("@IsAccountActive", SqlDbType.Bit),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_SpecificEntityParam.DossierSacrificeId;
            parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.Email.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.WebAddress.Trim());
            parameters[4].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.Description.Trim());
            parameters[5].Value = DossierSacrifice_SpecificEntityParam.BankId;
            parameters[6].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.BankBranchCode.Trim());
            parameters[7].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.BankBranchTitle.Trim());
            parameters[8].Value = DossierSacrifice_SpecificEntityParam.AccountNo;
            parameters[9].Value = (DossierSacrifice_SpecificEntityParam.OpeningAccountDate=="")?DBNull.Value:(object)DossierSacrifice_SpecificEntityParam.OpeningAccountDate;
            parameters[10].Value = (DossierSacrifice_SpecificEntityParam.IsHasSupplementaryInsurance);
            parameters[11].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.SupplementaryInsuranceNo.Trim());
            parameters[12].Value = DossierSacrifice_SpecificEntityParam.IsAccountActive;
            parameters[13].Value = DossierSacrifice_SpecificEntityParam.IsActive;
            parameters[14].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SpecificAdd", parameters);
            var messageError = parameters[14].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_SpecificId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_SpecificId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@Email", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@WebAddress", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@BankId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BankBranchCode", SqlDbType.NVarChar,20),
                                            new SqlParameter("@BankBranchTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@AccountNo", SqlDbType.NVarChar, 20),
                                            new SqlParameter("@OpeningAccountDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@IsHasSupplementaryInsurance", SqlDbType.Bit),
                                            new SqlParameter("@SupplementaryInsuranceNo", SqlDbType.NVarChar,20),
                                            new SqlParameter("@IsAccountActive", SqlDbType.Bit),                                          
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_SpecificEntityParam.DossierSacrifice_SpecificId;
            parameters[1].Value = DossierSacrifice_SpecificEntityParam.DossierSacrificeId;
            parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.Email.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.WebAddress.Trim());
            parameters[4].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.Description.Trim());
            parameters[5].Value = DossierSacrifice_SpecificEntityParam.BankId;
            parameters[6].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.BankBranchCode.Trim());
            parameters[7].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.BankBranchTitle.Trim());
            parameters[8].Value = DossierSacrifice_SpecificEntityParam.AccountNo;
            parameters[9].Value = (DossierSacrifice_SpecificEntityParam.OpeningAccountDate == "") ? DBNull.Value : (object)DossierSacrifice_SpecificEntityParam.OpeningAccountDate;
            parameters[10].Value = (DossierSacrifice_SpecificEntityParam.IsHasSupplementaryInsurance);
            parameters[11].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificEntityParam.SupplementaryInsuranceNo.Trim());
            parameters[12].Value = DossierSacrifice_SpecificEntityParam.IsAccountActive;
            parameters[13].Value = DossierSacrifice_SpecificEntityParam.IsActive;
            parameters[14].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SpecificUpdate", parameters);
            var messageError = parameters[14].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_SpecificId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_SpecificEntityParam.DossierSacrifice_SpecificId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_SpecificDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_SpecificEntity GetSingleDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_SpecificId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_SpecificEntityParam.DossierSacrifice_SpecificId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SpecificGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_SpecificDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_SpecificEntity> GetAllDossierSacrifice_SpecificDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_SpecificDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DossierSacrifice_SpecificGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_SpecificEntity> GetPageDossierSacrifice_SpecificDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_DossierSacrifice_Specific";
            parameters[5].Value = "DossierSacrifice_SpecificId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_SpecificDBCollectionFromDataSet(ds, out count);
        }

        public static DossierSacrifice_SpecificEntity GetDossierSacrifice_SpecificDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_SpecificEntity(new Guid(reader["DossierSacrifice_SpecificId"].ToString()),
                                                       Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                       reader["Email"].ToString(),
                                                       reader["WebAddress"].ToString(),
                                                       reader["Description"].ToString(),
                                                       Guid.Parse(reader["BankId"].ToString()),
                                                       reader["BankBranchCode"].ToString(),
                                                       reader["BankBranchTitle"].ToString(),
                                                       reader["AccountNo"].ToString(),
                                                       reader["OpeningAccountDate"].ToString(),
                                                       bool.Parse(reader["IsHasSupplementaryInsurance"].ToString()),
                                                       reader["SupplementaryInsuranceNo"].ToString(),
                                                       bool.Parse(reader["IsAccountActive"].ToString()),
                                                       reader["CreationDate"].ToString(),
                                                       reader["ModificationDate"].ToString(),
                                                       bool.Parse(reader["IsActive"].ToString()));
        }

        public static List<DossierSacrifice_SpecificEntity> GetDossierSacrifice_SpecificDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_SpecificEntity> lst = new List<DossierSacrifice_SpecificEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_SpecificDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public static List<DossierSacrifice_SpecificEntity> GetDossierSacrifice_SpecificDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_SpecificDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<DossierSacrifice_SpecificEntity> GetDossierSacrifice_SpecificDBCollectionByBankDB(BankEntity bankEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@BankId", SqlDbType.UniqueIdentifier);
            parameter.Value = bankEntity.BankId;
            return GetDossierSacrifice_SpecificDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SpecificGetByBank", new[] {parameter}));
        }

        public static List<DossierSacrifice_SpecificEntity> GetDossierSacrifice_SpecificDBCollectionByDossierSacrificeDB(DossierSacrificeEntity dossierSacrificeEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeEntity.DossierSacrificeId;
            return GetDossierSacrifice_SpecificDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_SpecificGetByDossierSacrifice", new[] {parameter}));
        }
    }
}