﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <مشخصات درس>
    /// </summary>

    public class LessonDB
    {
        #region Methods :

        public void AddLessonDB(LessonEntity LessonEntityParam, out Guid LessonId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@LessonId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@LessonTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(LessonEntityParam.LessonTitle.Trim());
            parameters[2].Value = LessonEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_LessonAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            LessonId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateLessonDB(LessonEntity LessonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@LessonId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@LessonTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = LessonEntityParam.LessonId;
            parameters[1].Value = FarsiToArabic.ToArabic(LessonEntityParam.LessonTitle.Trim());
            parameters[2].Value = LessonEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_LessonUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteLessonDB(LessonEntity LessonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@LessonId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = LessonEntityParam.LessonId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_LessonDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public LessonEntity GetSingleLessonDB(LessonEntity LessonEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@LessonId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = LessonEntityParam.LessonId;

                reader = _intranetDB.RunProcedureReader("Isar.p_LessonGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetLessonDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<LessonEntity> GetAllLessonDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetLessonDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_LessonGetAll", new IDataParameter[] { }));
        }
        public List<LessonEntity> GetAllIsActiveLessonDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetLessonDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_LessonIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<LessonEntity> GetPageLessonDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Lesson";
            parameters[5].Value = "LessonId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetLessonDBCollectionFromDataSet(ds, out count);
        }

        public LessonEntity GetLessonDBFromDataReader(IDataReader reader)
        {
            return new LessonEntity(Guid.Parse(reader["LessonId"].ToString()),
                                    reader["LessonTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<LessonEntity> GetLessonDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<LessonEntity> lst = new List<LessonEntity>();
                while (reader.Read())
                    lst.Add(GetLessonDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<LessonEntity> GetLessonDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetLessonDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    
}
