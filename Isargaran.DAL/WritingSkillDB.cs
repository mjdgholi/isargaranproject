﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: < مهارت نوشتن>
    /// </summary>
    #region Class "WritingSkillDB"

    public class WritingSkillDB
    {
        #region Methods :

        public void AddWritingSkillDB(WritingSkillEntity WritingSkillEntityParam, out Guid WritingSkillId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@WritingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@WritingSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@WritingSkillEnglishTitle", SqlDbType.NVarChar, 200),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(WritingSkillEntityParam.WritingSkillPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(WritingSkillEntityParam.WritingSkillEnglishTitle.Trim());
            parameters[3].Value = WritingSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_WritingSkillAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            WritingSkillId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateWritingSkillDB(WritingSkillEntity WritingSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@WritingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@WritingSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@WritingSkillEnglishTitle", SqlDbType.NVarChar, 200),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = WritingSkillEntityParam.WritingSkillId;
            parameters[1].Value = FarsiToArabic.ToArabic(WritingSkillEntityParam.WritingSkillPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(WritingSkillEntityParam.WritingSkillEnglishTitle.Trim());
            parameters[3].Value = WritingSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_WritingSkillUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteWritingSkillDB(WritingSkillEntity WritingSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@WritingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = WritingSkillEntityParam.WritingSkillId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_WritingSkillDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public WritingSkillEntity GetSingleWritingSkillDB(WritingSkillEntity WritingSkillEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@WritingSkillId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = WritingSkillEntityParam.WritingSkillId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_WritingSkillGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetWritingSkillDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WritingSkillEntity> GetAllWritingSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWritingSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_WritingSkillGetAll", new IDataParameter[] {}));
        }

        public List<WritingSkillEntity> GetAllIsActiveWritingSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWritingSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_WritingSkillGetAllIsActive", new IDataParameter[] { }));
        }

        public List<WritingSkillEntity> GetPageWritingSkillDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_WritingSkill";
            parameters[5].Value = "WritingSkillId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetWritingSkillDBCollectionFromDataSet(ds, out count);
        }

        public WritingSkillEntity GetWritingSkillDBFromDataReader(IDataReader reader)
        {
            return new WritingSkillEntity(new Guid(reader["WritingSkillId"].ToString()), 
                                          reader["WritingSkillPersianTitle"].ToString(),
                                          reader["WritingSkillEnglishTitle"].ToString(),
                                          reader["CreationDate"].ToString(),
                                          reader["ModificationDate"].ToString(),
                                          bool.Parse(reader["IsActive"].ToString()));
        }

        public List<WritingSkillEntity> GetWritingSkillDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<WritingSkillEntity> lst = new List<WritingSkillEntity>();
                while (reader.Read())
                    lst.Add(GetWritingSkillDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WritingSkillEntity> GetWritingSkillDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetWritingSkillDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }

    #endregion
}