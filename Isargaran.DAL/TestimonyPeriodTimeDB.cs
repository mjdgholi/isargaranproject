﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{

    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/23>
    /// Description: <دوره زمانی شهادت>
    /// </summary>

    #region Class "TestimonyPeriodTimeDB"

    public class TestimonyPeriodTimeDB
    {

        #region Methods :

        public void AddTestimonyPeriodTimeDB(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam, out Guid TestimonyPeriodTimeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@TestimonyPeriodTimeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@TestimonyPeriodTimeTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@Periority", SqlDbType.Int),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(TestimonyPeriodTimeEntityParam.TestimonyPeriodTimeTitle.Trim());
            parameters[2].Value = TestimonyPeriodTimeEntityParam.Periority;
            parameters[3].Value = TestimonyPeriodTimeEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_TestimonyPeriodTimeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            TestimonyPeriodTimeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateTestimonyPeriodTimeDB(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@TestimonyPeriodTimeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@TestimonyPeriodTimeTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@Periority", SqlDbType.Int),                                         
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = TestimonyPeriodTimeEntityParam.TestimonyPeriodTimeId;
            parameters[1].Value = FarsiToArabic.ToArabic(TestimonyPeriodTimeEntityParam.TestimonyPeriodTimeTitle.Trim());
            parameters[2].Value = TestimonyPeriodTimeEntityParam.Periority;
            parameters[3].Value = TestimonyPeriodTimeEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;


            _intranetDB.RunProcedure("Isar.p_TestimonyPeriodTimeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteTestimonyPeriodTimeDB(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@TestimonyPeriodTimeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = TestimonyPeriodTimeEntityParam.TestimonyPeriodTimeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_TestimonyPeriodTimeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public TestimonyPeriodTimeEntity GetSingleTestimonyPeriodTimeDB(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@TestimonyPeriodTimeId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = TestimonyPeriodTimeEntityParam.TestimonyPeriodTimeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_TestimonyPeriodTimeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetTestimonyPeriodTimeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<TestimonyPeriodTimeEntity> GetAllTestimonyPeriodTimeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetTestimonyPeriodTimeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_TestimonyPeriodTimeGetAll", new IDataParameter[] {}));
        }

        public List<TestimonyPeriodTimeEntity> GetAllIsActiveTestimonyPeriodTimeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetTestimonyPeriodTimeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_TestimonyPeriodTimeGetAllIsActive", new IDataParameter[] { }));
        }

        public List<TestimonyPeriodTimeEntity> GetPageTestimonyPeriodTimeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "v_TestimonyPeriodTime";
            parameters[5].Value = "TestimonyPeriodTimeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetTestimonyPeriodTimeDBCollectionFromDataSet(ds, out count);
        }

        public TestimonyPeriodTimeEntity GetTestimonyPeriodTimeDBFromDataReader(IDataReader reader)
        {
            return new TestimonyPeriodTimeEntity(new Guid(reader["TestimonyPeriodTimeId"].ToString()), 
                                                 reader["TestimonyPeriodTimeTitle"].ToString(),
                                                 int.Parse(reader["Periority"].ToString()),
                                                 reader["CreationDate"].ToString(),
                                                 reader["ModificationDate"].ToString(),
                                                 bool.Parse(reader["IsActive"].ToString()));
        }

        public List<TestimonyPeriodTimeEntity> GetTestimonyPeriodTimeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<TestimonyPeriodTimeEntity> lst = new List<TestimonyPeriodTimeEntity>();
                while (reader.Read())
                    lst.Add(GetTestimonyPeriodTimeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<TestimonyPeriodTimeEntity> GetTestimonyPeriodTimeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetTestimonyPeriodTimeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

    #endregion
}