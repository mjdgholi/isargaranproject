﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <رشته قرانی و معارف>
    /// </summary>


    public class QuranicAndIslamicCourseDB
    {
        #region Methods :

        public void AddQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam, out Guid QuranicAndIslamicCourseId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@QuranicAndIslamicCourseId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@QuranicAndIslamicCourseTitle", SqlDbType.NChar,300) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(QuranicAndIslamicCourseEntityParam.QuranicAndIslamicCourseTitle.Trim());
            parameters[2].Value = QuranicAndIslamicCourseEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_QuranicAndIslamicCourseAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            QuranicAndIslamicCourseId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@QuranicAndIslamicCourseId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@QuranicAndIslamicCourseTitle", SqlDbType.NChar,300) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = QuranicAndIslamicCourseEntityParam.QuranicAndIslamicCourseId;
            parameters[1].Value = FarsiToArabic.ToArabic(QuranicAndIslamicCourseEntityParam.QuranicAndIslamicCourseTitle.Trim());
            parameters[2].Value = QuranicAndIslamicCourseEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_QuranicAndIslamicCourseUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@QuranicAndIslamicCourseId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = QuranicAndIslamicCourseEntityParam.QuranicAndIslamicCourseId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_QuranicAndIslamicCourseDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public QuranicAndIslamicCourseEntity GetSingleQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@QuranicAndIslamicCourseId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = QuranicAndIslamicCourseEntityParam.QuranicAndIslamicCourseId;

                reader = _intranetDB.RunProcedureReader("Isar.p_QuranicAndIslamicCourseGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetQuranicAndIslamicCourseDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<QuranicAndIslamicCourseEntity> GetAllQuranicAndIslamicCourseDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetQuranicAndIslamicCourseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_QuranicAndIslamicCourseGetAll", new IDataParameter[] { }));
        }
        public List<QuranicAndIslamicCourseEntity> GetAllIsActiveQuranicAndIslamicCourseDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetQuranicAndIslamicCourseDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_QuranicAndIslamicCourseIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<QuranicAndIslamicCourseEntity> GetPageQuranicAndIslamicCourseDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_QuranicAndIslamicCourse";
            parameters[5].Value = "QuranicAndIslamicCourseId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetQuranicAndIslamicCourseDBCollectionFromDataSet(ds, out count);
        }

        public QuranicAndIslamicCourseEntity GetQuranicAndIslamicCourseDBFromDataReader(IDataReader reader)
        {
            return new QuranicAndIslamicCourseEntity(Guid.Parse(reader["QuranicAndIslamicCourseId"].ToString()),
                                    reader["QuranicAndIslamicCourseTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<QuranicAndIslamicCourseEntity> GetQuranicAndIslamicCourseDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<QuranicAndIslamicCourseEntity> lst = new List<QuranicAndIslamicCourseEntity>();
                while (reader.Read())
                    lst.Add(GetQuranicAndIslamicCourseDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<QuranicAndIslamicCourseEntity> GetQuranicAndIslamicCourseDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetQuranicAndIslamicCourseDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
