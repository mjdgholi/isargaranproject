﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/16>
    /// Description: < اطلاعات ملاقات>
    /// </summary>


    public class DossierSacrifice_AppointmentDB
    {



        #region Methods :

        public void AddDossierSacrifice_AppointmentDB(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam, out Guid DossierSacrifice_AppointmentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_AppointmentId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AppointmentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@AppointmentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AppointmentPerson", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasRequest", SqlDbType.Bit) ,
											  new SqlParameter("@RequestContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@HasAction", SqlDbType.Bit) ,
											  new SqlParameter("@ActionDescription", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_AppointmentEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_AppointmentEntityParam.AppointmentDate==""?System.DBNull.Value:(object)DossierSacrifice_AppointmentEntityParam.AppointmentDate);
            parameters[3].Value = DossierSacrifice_AppointmentEntityParam.AppointmentTypeId;
            parameters[4].Value = (DossierSacrifice_AppointmentEntityParam.AppointmentPerson==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.AppointmentPerson));
            parameters[5].Value = (DossierSacrifice_AppointmentEntityParam.DependentTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_AppointmentEntityParam.DependentTypeId);
            parameters[6].Value = DossierSacrifice_AppointmentEntityParam.HasRequest;
            parameters[7].Value = (DossierSacrifice_AppointmentEntityParam.RequestContent==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.RequestContent));
            parameters[8].Value = DossierSacrifice_AppointmentEntityParam.HasAction;
            parameters[9].Value = (DossierSacrifice_AppointmentEntityParam.ActionDescription==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.ActionDescription));
            parameters[10].Value =(DossierSacrifice_AppointmentEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.Description));            
            parameters[11].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_AppointmentAdd", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_AppointmentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_AppointmentDB(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_AppointmentId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AppointmentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@AppointmentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AppointmentPerson", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasRequest", SqlDbType.Bit) ,
											  new SqlParameter("@RequestContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@HasAction", SqlDbType.Bit) ,
											  new SqlParameter("@ActionDescription", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_AppointmentEntityParam.DossierSacrifice_AppointmentId;
            parameters[1].Value = DossierSacrifice_AppointmentEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_AppointmentEntityParam.AppointmentDate == "" ? System.DBNull.Value : (object)DossierSacrifice_AppointmentEntityParam.AppointmentDate);
            parameters[3].Value = DossierSacrifice_AppointmentEntityParam.AppointmentTypeId;
            parameters[4].Value = (DossierSacrifice_AppointmentEntityParam.AppointmentPerson == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.AppointmentPerson));
            parameters[5].Value = (DossierSacrifice_AppointmentEntityParam.DependentTypeId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_AppointmentEntityParam.DependentTypeId);
            parameters[6].Value = DossierSacrifice_AppointmentEntityParam.HasRequest;
            parameters[7].Value = (DossierSacrifice_AppointmentEntityParam.RequestContent == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.RequestContent));
            parameters[8].Value = DossierSacrifice_AppointmentEntityParam.HasAction;
            parameters[9].Value = (DossierSacrifice_AppointmentEntityParam.ActionDescription == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.ActionDescription));
            parameters[10].Value = (DossierSacrifice_AppointmentEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_AppointmentEntityParam.Description));                     

            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_AppointmentUpdate", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_AppointmentDB(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_AppointmentId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_AppointmentEntityParam.DossierSacrifice_AppointmentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_AppointmentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_AppointmentEntity GetSingleDossierSacrifice_AppointmentDB(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_AppointmentId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_AppointmentEntityParam.DossierSacrifice_AppointmentId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_AppointmentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_AppointmentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_AppointmentEntity> GetAllDossierSacrifice_AppointmentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_AppointmentDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_AppointmentGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_AppointmentEntity> GetPageDossierSacrifice_AppointmentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Appointment";
            parameters[5].Value = "DossierSacrifice_AppointmentId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_AppointmentDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_AppointmentEntity GetDossierSacrifice_AppointmentDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_AppointmentEntity(Guid.Parse(reader["DossierSacrifice_AppointmentId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["AppointmentDate"].ToString(),
                                    Guid.Parse(reader["AppointmentTypeId"].ToString()),
                                    reader["AppointmentPerson"].ToString(),
                                    Convert.IsDBNull(reader["DependentTypeId"]) ? null : (Guid?)reader["DependentTypeId"],                                      
                                    bool.Parse(reader["HasRequest"].ToString()),
                                    reader["RequestContent"].ToString(),
                                    bool.Parse(reader["HasAction"].ToString()),
                                    reader["ActionDescription"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_AppointmentEntity> lst = new List<DossierSacrifice_AppointmentEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_AppointmentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_AppointmentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentDBCollectionByDependentTypeDB(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_AppointmentEntityParam.DependentTypeId;
            return GetDossierSacrifice_AppointmentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_AppointmentGetByDependentType", new[] { parameter }));
        }

        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentDBCollectionByAppointmentTypeDB(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@AppointmentTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_AppointmentEntityParam.AppointmentTypeId;
            return GetDossierSacrifice_AppointmentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_AppointmentGetByAppointmentType", new[] { parameter }));
        }



        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentDBCollectionByDossierSacrificeDB(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_AppointmentEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_AppointmentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_AppointmentGetByDossierSacrifice", new[] { parameter }));
        }


        #endregion



    }


}
