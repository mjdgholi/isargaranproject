﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/18>
    /// Description: <تسهیلات و خدمات پزشکی>
    /// </summary>

    public class MedicalFacilitieAndServiceDB
    {
        #region Methods :

        public void AddMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntity MedicalFacilitieAndServiceEntityParam, out Guid MedicalFacilitieAndServiceId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@MedicalFacilitieAndServiceTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceTitle.Trim());            
            parameters[2].Value = MedicalFacilitieAndServiceEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_MedicalFacilitieAndServiceAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MedicalFacilitieAndServiceId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntity MedicalFacilitieAndServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@MedicalFacilitieAndServiceTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceId;
            parameters[1].Value = FarsiToArabic.ToArabic(MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceTitle.Trim());            
            parameters[2].Value = MedicalFacilitieAndServiceEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_MedicalFacilitieAndServiceUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntity MedicalFacilitieAndServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_MedicalFacilitieAndServiceDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MedicalFacilitieAndServiceEntity GetSingleMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntity MedicalFacilitieAndServiceEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@MedicalFacilitieAndServiceId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = MedicalFacilitieAndServiceEntityParam.MedicalFacilitieAndServiceId;

                reader = _intranetDB.RunProcedureReader("Isar.p_MedicalFacilitieAndServiceGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMedicalFacilitieAndServiceDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MedicalFacilitieAndServiceEntity> GetAllMedicalFacilitieAndServiceDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMedicalFacilitieAndServiceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_MedicalFacilitieAndServiceGetAll", new IDataParameter[] { }));
        }

        public List<MedicalFacilitieAndServiceEntity> GetAllMedicalFacilitieAndServiceIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMedicalFacilitieAndServiceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_MedicalFacilitieAndServiceIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<MedicalFacilitieAndServiceEntity> GetPageMedicalFacilitieAndServiceDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MedicalFacilitieAndService";
            parameters[5].Value = "MedicalFacilitieAndServiceId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetMedicalFacilitieAndServiceDBCollectionFromDataSet(ds, out count);
        }

        public MedicalFacilitieAndServiceEntity GetMedicalFacilitieAndServiceDBFromDataReader(IDataReader reader)
        {
            return new MedicalFacilitieAndServiceEntity(Guid.Parse(reader["MedicalFacilitieAndServiceId"].ToString()),
                                    reader["MedicalFacilitieAndServiceTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<MedicalFacilitieAndServiceEntity> GetMedicalFacilitieAndServiceDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MedicalFacilitieAndServiceEntity> lst = new List<MedicalFacilitieAndServiceEntity>();
                while (reader.Read())
                    lst.Add(GetMedicalFacilitieAndServiceDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MedicalFacilitieAndServiceEntity> GetMedicalFacilitieAndServiceDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMedicalFacilitieAndServiceDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

}
