﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/27>
    // Description:	<رشته ورزشی>
    /// </summary>


    public class SportFieldDB
    {


        #region Methods :

        public void AddSportFieldDB(SportFieldEntity SportFieldEntityParam, out Guid SportFieldId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SportFieldId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SportFieldPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SportFieldEnglishTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(SportFieldEntityParam.SportFieldPersianTitle.Trim());
            parameters[2].Value = (SportFieldEntityParam.SportFieldEnglishTitle==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(SportFieldEntityParam.SportFieldEnglishTitle.Trim()));
            parameters[3].Value = SportFieldEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_SportFieldAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SportFieldId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSportFieldDB(SportFieldEntity SportFieldEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SportFieldId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@SportFieldPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SportFieldEnglishTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = SportFieldEntityParam.SportFieldId;
            parameters[1].Value = FarsiToArabic.ToArabic(SportFieldEntityParam.SportFieldPersianTitle.Trim());
            parameters[2].Value = (SportFieldEntityParam.SportFieldEnglishTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(SportFieldEntityParam.SportFieldEnglishTitle.Trim()));
            parameters[3].Value = SportFieldEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_SportFieldUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSportFieldDB(SportFieldEntity SportFieldEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SportFieldId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SportFieldEntityParam.SportFieldId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_SportFieldDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SportFieldEntity GetSingleSportFieldDB(SportFieldEntity SportFieldEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@SportFieldId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = SportFieldEntityParam.SportFieldId;

                reader = _intranetDB.RunProcedureReader("Isar.p_SportFieldGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSportFieldDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SportFieldEntity> GetAllSportFieldDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSportFieldDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_SportFieldGetAll", new IDataParameter[] { }));
        }
        public List<SportFieldEntity> GetAllSportFieldIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSportFieldDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_SportFieldIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<SportFieldEntity> GetPageSportFieldDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_SportField";
            parameters[5].Value = "SportFieldId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetSportFieldDBCollectionFromDataSet(ds, out count);
        }

        public SportFieldEntity GetSportFieldDBFromDataReader(IDataReader reader)
        {
            return new SportFieldEntity(Guid.Parse(reader["SportFieldId"].ToString()),
                                    reader["SportFieldPersianTitle"].ToString(),
                                    reader["SportFieldEnglishTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SportFieldEntity> GetSportFieldDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SportFieldEntity> lst = new List<SportFieldEntity>();
                while (reader.Read())
                    lst.Add(GetSportFieldDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SportFieldEntity> GetSportFieldDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSportFieldDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
