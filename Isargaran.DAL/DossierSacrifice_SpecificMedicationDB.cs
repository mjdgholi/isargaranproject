﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/17>
    /// Description: <اطلاعات مصرف داروهای خاص>
    /// </summary>
    #region Class "DossierSacrifice_SpecificMedicationDB"

    public class DossierSacrifice_SpecificMedicationDB
    {       
        #region Methods :

        public void AddDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam, out Guid DossierSacrifice_SpecificMedicationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MedicationConsum", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@MedicationNo", SqlDbType.Int),
                                            new SqlParameter("@MedicationPrize", SqlDbType.Decimal),
                                            new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@IsMedicationFinished", SqlDbType.Bit),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),                                           
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_SpecificMedicationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_SpecificMedicationEntityParam.SpecificMedicationId;
            parameters[3].Value = (DossierSacrifice_SpecificMedicationEntityParam.CountryId == new Guid() ? DBNull.Value : (object)DossierSacrifice_SpecificMedicationEntityParam.CountryId);
            parameters[4].Value = DossierSacrifice_SpecificMedicationEntityParam.MedicationConsum;
            parameters[5].Value = (DossierSacrifice_SpecificMedicationEntityParam.MedicationNo==null)?DBNull.Value:(object)(DossierSacrifice_SpecificMedicationEntityParam.MedicationNo);
            parameters[6].Value = (DossierSacrifice_SpecificMedicationEntityParam.MedicationPrize == null) ? DBNull.Value : (object)DossierSacrifice_SpecificMedicationEntityParam.MedicationPrize;
            parameters[7].Value = (DossierSacrifice_SpecificMedicationEntityParam.StartDate==""?System.DBNull.Value:(object)DossierSacrifice_SpecificMedicationEntityParam.StartDate);
            parameters[8].Value = (DossierSacrifice_SpecificMedicationEntityParam.EndDate==""?System.DBNull.Value:(object)DossierSacrifice_SpecificMedicationEntityParam.EndDate);
            parameters[9].Value = DossierSacrifice_SpecificMedicationEntityParam.IsMedicationFinished;
            parameters[10].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificMedicationEntityParam.Description.Trim());           
            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_SpecificMedicationAdd", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_SpecificMedicationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MedicationConsum", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@MedicationNo", SqlDbType.Int),
                                            new SqlParameter("@MedicationPrize", SqlDbType.Decimal),
                                            new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@IsMedicationFinished", SqlDbType.Bit),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),                                      
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_SpecificMedicationEntityParam.DossierSacrifice_SpecificMedicationId;
            parameters[1].Value = DossierSacrifice_SpecificMedicationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_SpecificMedicationEntityParam.SpecificMedicationId;
            parameters[3].Value = (DossierSacrifice_SpecificMedicationEntityParam.CountryId==new Guid()?DBNull.Value:(object)DossierSacrifice_SpecificMedicationEntityParam.CountryId);
            parameters[4].Value = DossierSacrifice_SpecificMedicationEntityParam.MedicationConsum;
            parameters[5].Value = (DossierSacrifice_SpecificMedicationEntityParam.MedicationNo == null) ? DBNull.Value : (object)DossierSacrifice_SpecificMedicationEntityParam.MedicationNo;
            parameters[6].Value = (DossierSacrifice_SpecificMedicationEntityParam.MedicationPrize == null) ? DBNull.Value : (object)DossierSacrifice_SpecificMedicationEntityParam.MedicationPrize;
            parameters[7].Value = (DossierSacrifice_SpecificMedicationEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_SpecificMedicationEntityParam.StartDate);
            parameters[8].Value = (DossierSacrifice_SpecificMedicationEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_SpecificMedicationEntityParam.EndDate);
            parameters[9].Value = DossierSacrifice_SpecificMedicationEntityParam.IsMedicationFinished;
            parameters[10].Value = FarsiToArabic.ToArabic(DossierSacrifice_SpecificMedicationEntityParam.Description.Trim());
            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_SpecificMedicationUpdate", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_SpecificMedicationEntityParam.DossierSacrifice_SpecificMedicationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_SpecificMedicationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_SpecificMedicationEntity GetSingleDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_SpecificMedicationId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_SpecificMedicationEntityParam.DossierSacrifice_SpecificMedicationId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_SpecificMedicationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_SpecificMedicationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetAllDossierSacrifice_SpecificMedicationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_SpecificMedicationDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_DossierSacrifice_SpecificMedicationGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetPageDossierSacrifice_SpecificMedicationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_SpecificMedication";
            parameters[5].Value = "DossierSacrifice_SpecificMedicationId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetDossierSacrifice_SpecificMedicationDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_SpecificMedicationEntity GetDossierSacrifice_SpecificMedicationDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_SpecificMedicationEntity(new Guid(reader["DossierSacrifice_SpecificMedicationId"].ToString()),
                                                                 Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                                 Guid.Parse(reader["SpecificMedicationId"].ToString()),
                                                                 Convert.IsDBNull(reader["CountryId"]) ? (Guid?)null : Guid.Parse(reader["CountryId"].ToString()),                                                                 
                                                                 reader["MedicationConsum"].ToString(),
                                                                 Convert.IsDBNull(reader["MedicationNo"]) ? (int?) null : int.Parse(reader["MedicationNo"].ToString()),
                                                                 Convert.IsDBNull(reader["MedicationPrize"]) ? (decimal?) null : decimal.Parse(reader["MedicationPrize"].ToString()),
                                                                 reader["StartDate"].ToString(),
                                                                 reader["EndDate"].ToString(),
                                                                 bool.Parse(reader["IsMedicationFinished"].ToString()),
                                                                 reader["Description"].ToString(),
                                                                 reader["CreationDate"].ToString(),
                                                                 reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_SpecificMedicationEntity> lst = new List<DossierSacrifice_SpecificMedicationEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_SpecificMedicationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_SpecificMedicationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationDBCollectionByCountryDB(Guid CountryId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier);
            parameter.Value = CountryId;
            return GetDossierSacrifice_SpecificMedicationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_SpecificMedicationGetByCountry", new[] {parameter}));
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationDBCollectionByDossierSacrificeDB(Guid DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeId;
            return GetDossierSacrifice_SpecificMedicationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_SpecificMedicationGetByDossierSacrifice", new[] {parameter}));
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationDBCollectionBySpecificMedicationDB(Guid SpecificMedicationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SpecificMedicationId", SqlDbType.UniqueIdentifier);
            parameter.Value = SpecificMedicationId;
            return GetDossierSacrifice_SpecificMedicationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_SpecificMedicationGetBySpecificMedication", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}