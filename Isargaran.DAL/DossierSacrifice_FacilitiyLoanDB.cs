﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <majid.Gholibeygian>
    /// Create date: <1393/02/22>
    /// Description: <اطلاعات وام>
    /// </summary>

    //-----------------------------------------------------

    #region Class "DossierSacrifice_FacilitiyLoanDB"

    public class DossierSacrifice_FacilitiyLoanDB
    {


        #region Methods :

        public void AddDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam, out Guid DossierSacrifice_FacilitiyLoanId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_FacilitiyLoanId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@LoanTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@SourceBudgetPayment", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@LoanPaymentAmount", SqlDbType.Decimal),
                                            new SqlParameter("@LoanCommissionAmount", SqlDbType.Decimal),
                                            new SqlParameter("@RecipientPersonName", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@RecipientPersonNationalNo", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@IsLoanSettlement", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_FacilitiyLoanEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilitiyLoanEntityParam.LoanTypeId;
            parameters[3].Value = DossierSacrifice_FacilitiyLoanEntityParam.StartDate;
            parameters[4].Value = (DossierSacrifice_FacilitiyLoanEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_FacilitiyLoanEntityParam.EndDate);
            parameters[5].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.SourceBudgetPayment.Trim());
            parameters[6].Value = DossierSacrifice_FacilitiyLoanEntityParam.LoanPaymentAmount;
            parameters[7].Value = (DossierSacrifice_FacilitiyLoanEntityParam.LoanCommissionAmount)==null?(object)DBNull.Value:DossierSacrifice_FacilitiyLoanEntityParam.LoanCommissionAmount;
            parameters[8].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.RecipientPersonName.Trim());
            parameters[9].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.RecipientPersonNationalNo.Trim());
            parameters[10].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.Description.Trim());
            parameters[11].Value = DossierSacrifice_FacilitiyLoanEntityParam.IsLoanSettlement;
            parameters[12].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_FacilitiyLoanAdd", parameters);
            var messageError = parameters[12].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_FacilitiyLoanId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_FacilitiyLoanId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@LoanTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@SourceBudgetPayment", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@LoanPaymentAmount", SqlDbType.Decimal),
                                            new SqlParameter("@LoanCommissionAmount", SqlDbType.Decimal),
                                            new SqlParameter("@RecipientPersonName", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@RecipientPersonNationalNo", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@IsLoanSettlement", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_FacilitiyLoanEntityParam.DossierSacrifice_FacilitiyLoanId;
            parameters[1].Value = DossierSacrifice_FacilitiyLoanEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilitiyLoanEntityParam.LoanTypeId;
            parameters[3].Value = DossierSacrifice_FacilitiyLoanEntityParam.StartDate;
            parameters[4].Value = (DossierSacrifice_FacilitiyLoanEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_FacilitiyLoanEntityParam.EndDate);
            parameters[5].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.SourceBudgetPayment.Trim());
            parameters[6].Value = DossierSacrifice_FacilitiyLoanEntityParam.LoanPaymentAmount;
            parameters[7].Value = (DossierSacrifice_FacilitiyLoanEntityParam.LoanCommissionAmount) == null ? (object)DBNull.Value : DossierSacrifice_FacilitiyLoanEntityParam.LoanCommissionAmount;
            parameters[8].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.RecipientPersonName.Trim());
            parameters[9].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.RecipientPersonNationalNo.Trim());
            parameters[10].Value = FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyLoanEntityParam.Description.Trim());
            parameters[11].Value = DossierSacrifice_FacilitiyLoanEntityParam.IsLoanSettlement;
            parameters[12].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_FacilitiyLoanUpdate", parameters);
            var messageError = parameters[12].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_FacilitiyLoanId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_FacilitiyLoanEntityParam.DossierSacrifice_FacilitiyLoanId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_FacilitiyLoanDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_FacilitiyLoanEntity GetSingleDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_FacilitiyLoanId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_FacilitiyLoanEntityParam.DossierSacrifice_FacilitiyLoanId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_FacilitiyLoanGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_FacilitiyLoanDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetAllDossierSacrifice_FacilitiyLoanDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_FacilitiyLoanDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_DossierSacrifice_FacilitiyLoanGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetPageDossierSacrifice_FacilitiyLoanDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_FacilitiyLoan";
            parameters[5].Value = "DossierSacrifice_FacilitiyLoanId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetDossierSacrifice_FacilitiyLoanDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_FacilitiyLoanEntity GetDossierSacrifice_FacilitiyLoanDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_FacilitiyLoanEntity(new Guid(reader["DossierSacrifice_FacilitiyLoanId"].ToString()),
                                                            Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                            Guid.Parse(reader["LoanTypeId"].ToString()),
                                                            reader["StartDate"].ToString(),
                                                            reader["EndDate"].ToString(),
                                                            reader["SourceBudgetPayment"].ToString(),
                                                            decimal.Parse(reader["LoanPaymentAmount"].ToString()),
                                                           Convert.IsDBNull(reader["LoanCommissionAmount"]) ? (decimal?)null : decimal.Parse(reader["LoanCommissionAmount"].ToString()),
                                                            reader["RecipientPersonName"].ToString(),
                                                            reader["RecipientPersonNationalNo"].ToString(),
                                                            reader["Description"].ToString(),
                                                            bool.Parse(reader["IsLoanSettlement"].ToString()),
                                                            reader["ModificationDate"].ToString(),
                                                            reader["CreationDate"].ToString());
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetDossierSacrifice_FacilitiyLoanDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_FacilitiyLoanEntity> lst = new List<DossierSacrifice_FacilitiyLoanEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_FacilitiyLoanDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetDossierSacrifice_FacilitiyLoanDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_FacilitiyLoanDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetDossierSacrifice_FacilitiyLoanDBCollectionByDossierSacrificeDB(Guid DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeId;
            return GetDossierSacrifice_FacilitiyLoanDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_FacilitiyLoanGetByDossierSacrifice", new[] {parameter}));
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetDossierSacrifice_FacilitiyLoanDBCollectionByLoanTypeDB(Guid LoanTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@LoanTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = LoanTypeId;
            return GetDossierSacrifice_FacilitiyLoanDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_FacilitiyLoanGetByLoanType", new[] {parameter}));
        }

        #endregion
    }

    #endregion
}