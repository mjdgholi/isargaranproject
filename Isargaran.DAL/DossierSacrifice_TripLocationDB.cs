﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/20>
    /// Description: <پرونده اطلاعات اردو یا مسافرت ایثارگر>
    /// </summary>

    //-----------------------------------------------------
    #region Class "DossierSacrifice_TripLocationDB"

    public class DossierSacrifice_TripLocationDB
    {



        #region Methods :

        public void AddDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam, out Guid DossierSacrifice_TravelLocationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_TravelLocationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TripLocationId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TripStartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TripEndDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TravelerNo", SqlDbType.Int) ,
											  new SqlParameter("@FinancialCost", SqlDbType.Decimal) ,
											  new SqlParameter("@VehicleType", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HotelName", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AlongPersonName", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_TripLocationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_TripLocationEntityParam.TripLocationId;
            parameters[3].Value = (DossierSacrifice_TripLocationEntityParam.TripStartDate==""?System.DBNull.Value:(object)DossierSacrifice_TripLocationEntityParam.TripStartDate);
            parameters[4].Value = (DossierSacrifice_TripLocationEntityParam.TripEndDate==""?System.DBNull.Value:(object)DossierSacrifice_TripLocationEntityParam.TripEndDate);
            parameters[5].Value = (DossierSacrifice_TripLocationEntityParam.TravelerNo==null?System.DBNull.Value:(object)DossierSacrifice_TripLocationEntityParam.TravelerNo);
            parameters[6].Value = (DossierSacrifice_TripLocationEntityParam.FinancialCost==null?System.DBNull.Value:(object)DossierSacrifice_TripLocationEntityParam.FinancialCost);
            parameters[7].Value = (DossierSacrifice_TripLocationEntityParam.VehicleType==null?System.DBNull.Value:(object)DossierSacrifice_TripLocationEntityParam.VehicleType);
            parameters[8].Value = (DossierSacrifice_TripLocationEntityParam.HotelName==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TripLocationEntityParam.HotelName.Trim()));
            parameters[9].Value = (DossierSacrifice_TripLocationEntityParam.OccasionId == null ? System.DBNull.Value : (object)DossierSacrifice_TripLocationEntityParam.OccasionId);
            parameters[10].Value = (DossierSacrifice_TripLocationEntityParam.AlongPersonName==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TripLocationEntityParam.AlongPersonName.Trim()));
            parameters[11].Value = (DossierSacrifice_TripLocationEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TripLocationEntityParam.Description.Trim()));


            parameters[12].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TripLocationAdd", parameters);
            var messageError = parameters[12].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_TravelLocationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_TravelLocationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TripLocationId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TripStartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TripEndDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TravelerNo", SqlDbType.Int) ,
											  new SqlParameter("@FinancialCost", SqlDbType.Decimal) ,
											  new SqlParameter("@VehicleType", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HotelName", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AlongPersonName", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_TripLocationEntityParam.DossierSacrifice_TravelLocationId;
            parameters[1].Value = DossierSacrifice_TripLocationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_TripLocationEntityParam.TripLocationId;
            parameters[3].Value = (DossierSacrifice_TripLocationEntityParam.TripStartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_TripLocationEntityParam.TripStartDate);
            parameters[4].Value = (DossierSacrifice_TripLocationEntityParam.TripEndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_TripLocationEntityParam.TripEndDate);
            parameters[5].Value = (DossierSacrifice_TripLocationEntityParam.TravelerNo == null ? System.DBNull.Value : (object)DossierSacrifice_TripLocationEntityParam.TravelerNo);
            parameters[6].Value = (DossierSacrifice_TripLocationEntityParam.FinancialCost == null ? System.DBNull.Value : (object)DossierSacrifice_TripLocationEntityParam.FinancialCost);
            parameters[7].Value = (DossierSacrifice_TripLocationEntityParam.VehicleType == null ? System.DBNull.Value : (object)DossierSacrifice_TripLocationEntityParam.VehicleType);
            parameters[8].Value = (DossierSacrifice_TripLocationEntityParam.HotelName == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TripLocationEntityParam.HotelName.Trim()));
            parameters[9].Value = (DossierSacrifice_TripLocationEntityParam.OccasionId==null?System.DBNull.Value:(object)DossierSacrifice_TripLocationEntityParam.OccasionId);
            parameters[10].Value = (DossierSacrifice_TripLocationEntityParam.AlongPersonName == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TripLocationEntityParam.AlongPersonName.Trim()));
            parameters[11].Value = (DossierSacrifice_TripLocationEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TripLocationEntityParam.Description.Trim()));


            parameters[12].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TripLocationUpdate", parameters);
            var messageError = parameters[12].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_TravelLocationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_TripLocationEntityParam.DossierSacrifice_TravelLocationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TripLocationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_TripLocationEntity GetSingleDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_TravelLocationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_TripLocationEntityParam.DossierSacrifice_TravelLocationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TripLocationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_TripLocationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TripLocationEntity> GetAllDossierSacrifice_TripLocationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_TripLocationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_TripLocationGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_TripLocationEntity> GetPageDossierSacrifice_TripLocationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_TripLocation";
            parameters[5].Value = "DossierSacrifice_TravelLocationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_TripLocationDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_TripLocationEntity GetDossierSacrifice_TripLocationDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_TripLocationEntity(Guid.Parse(reader["DossierSacrifice_TravelLocationId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["TripLocationId"].ToString()),
                                    reader["TripStartDate"].ToString(),
                                    reader["TripEndDate"].ToString(),
                                    Convert.IsDBNull(reader["TravelerNo"]) ? null : (int?)reader["TravelerNo"],
                                    Convert.IsDBNull(reader["FinancialCost"]) ? null : (decimal?)reader["FinancialCost"],
                                    Convert.IsDBNull(reader["VehicleType"]) ? null : (Guid?)reader["VehicleType"],                                    
                                    reader["HotelName"].ToString(),
                                    Convert.IsDBNull(reader["OccasionId"]) ? null : (Guid?)reader["OccasionId"],                                                                        
                                    reader["AlongPersonName"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_TripLocationEntity> lst = new List<DossierSacrifice_TripLocationEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_TripLocationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_TripLocationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationDBCollectionByDossierSacrificeDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TripLocationEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_TripLocationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TripLocationGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationDBCollectionByOccasionDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TripLocationEntityParam.OccasionId;
            return GetDossierSacrifice_TripLocationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TripLocationGetByOccasion", new[] { parameter }));
        }

        public List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationDBCollectionByTripLocationDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@TripLocationId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TripLocationEntityParam.TripLocationId;
            return GetDossierSacrifice_TripLocationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TripLocationGetByTripLocation", new[] { parameter }));
        }

        public List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationDBCollectionByVehicleTypeDB(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@VehicleType", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TripLocationEntityParam.VehicleType;
            return GetDossierSacrifice_TripLocationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TripLocationGetByVehicleType", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}
