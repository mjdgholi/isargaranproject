﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<اطلاعات گواهینامه رانندگی>
    /// </summary>

    #region Class "DossierSacrifice_DrivingLicenseDB"

    public class DossierSacrifice_DrivingLicenseDB
    {
        #region Methods :

        public void AddDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam, out Guid DossierSacrifice_DrivingLicenseId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_DrivingLicenseId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsHasDrivingLicense", SqlDbType.Bit),
                                            new SqlParameter("@CardNo", SqlDbType.NVarChar, 20),
                                            new SqlParameter("@CardIssueDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CardExpiredDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@DrivingLicenseTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DrivingRestrictionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_DrivingLicenseEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_DrivingLicenseEntityParam.IsHasDrivingLicense;
            parameters[3].Value =FarsiToArabic.ToArabic(DossierSacrifice_DrivingLicenseEntityParam.CardNo.Trim());
            parameters[4].Value = (DossierSacrifice_DrivingLicenseEntityParam.CardIssueDate.Trim() == "") ? DBNull.Value : (object) DossierSacrifice_DrivingLicenseEntityParam.CardIssueDate;
            parameters[5].Value = (DossierSacrifice_DrivingLicenseEntityParam.CardExpiredDate.Trim() == "") ? DBNull.Value : (object) DossierSacrifice_DrivingLicenseEntityParam.CardExpiredDate;
            parameters[6].Value = DossierSacrifice_DrivingLicenseEntityParam.DrivingLicenseTypeId;
            parameters[7].Value = (DossierSacrifice_DrivingLicenseEntityParam.DrivingRestrictionId==new Guid()?DBNull.Value:(object)DossierSacrifice_DrivingLicenseEntityParam.DrivingRestrictionId);
            parameters[8].Value = DossierSacrifice_DrivingLicenseEntityParam.IsActive;
            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_DrivingLicenseAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_DrivingLicenseId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_DrivingLicenseId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsHasDrivingLicense", SqlDbType.Bit),
                                            new SqlParameter("@CardNo", SqlDbType.NVarChar, 20),
                                            new SqlParameter("@CardIssueDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CardExpiredDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@DrivingLicenseTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DrivingRestrictionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_DrivingLicenseEntityParam.DossierSacrifice_DrivingLicenseId;
            parameters[1].Value = DossierSacrifice_DrivingLicenseEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_DrivingLicenseEntityParam.IsHasDrivingLicense;
            parameters[3].Value = FarsiToArabic.ToArabic(DossierSacrifice_DrivingLicenseEntityParam.CardNo.Trim());
            parameters[4].Value = (DossierSacrifice_DrivingLicenseEntityParam.CardIssueDate.Trim() == "") ? DBNull.Value : (object) DossierSacrifice_DrivingLicenseEntityParam.CardIssueDate;
            parameters[5].Value = (DossierSacrifice_DrivingLicenseEntityParam.CardExpiredDate.Trim() == "") ? DBNull.Value : (object) DossierSacrifice_DrivingLicenseEntityParam.CardExpiredDate;
            parameters[6].Value = DossierSacrifice_DrivingLicenseEntityParam.DrivingLicenseTypeId;
            parameters[7].Value = (DossierSacrifice_DrivingLicenseEntityParam.DrivingRestrictionId == new Guid() ? DBNull.Value : (object)DossierSacrifice_DrivingLicenseEntityParam.DrivingRestrictionId);
            parameters[8].Value = DossierSacrifice_DrivingLicenseEntityParam.IsActive;
            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_DrivingLicenseUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_DrivingLicenseId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_DrivingLicenseEntityParam.DossierSacrifice_DrivingLicenseId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_DrivingLicenseDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_DrivingLicenseEntity GetSingleDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_DrivingLicenseId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_DrivingLicenseEntityParam.DossierSacrifice_DrivingLicenseId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_DrivingLicenseGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_DrivingLicenseDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_DrivingLicenseEntity> GetAllDossierSacrifice_DrivingLicenseDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_DrivingLicenseDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DossierSacrifice_DrivingLicenseGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_DrivingLicenseEntity> GetPageDossierSacrifice_DrivingLicenseDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_DossierSacrifice_DrivingLicense";
            parameters[5].Value = "DossierSacrifice_DrivingLicenseId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_DrivingLicenseDBCollectionFromDataSet(ds, out count);
        }

        public static DossierSacrifice_DrivingLicenseEntity GetDossierSacrifice_DrivingLicenseDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_DrivingLicenseEntity(new Guid(reader["DossierSacrifice_DrivingLicenseId"].ToString()),
                                                             Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                             bool.Parse(reader["IsHasDrivingLicense"].ToString()),
                                                             reader["CardNo"].ToString(),
                                                             reader["CardIssueDate"].ToString(),
                                                             reader["CardExpiredDate"].ToString(),
                                                             Guid.Parse(reader["DrivingLicenseTypeId"].ToString()),
                                                             Convert.IsDBNull(reader["DrivingRestrictionId"]) ? null : (Guid?)reader["DrivingRestrictionId"],                                                             
                                                             reader["CreationDate"].ToString(),
                                                             reader["ModificationDate"].ToString(),
                                                             bool.Parse(reader["IsActive"].ToString()));
        }

        public static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_DrivingLicenseEntity> lst = new List<DossierSacrifice_DrivingLicenseEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_DrivingLicenseDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_DrivingLicenseDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseDBCollectionByDossierSacrificeDB(Guid DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeId;
            return GetDossierSacrifice_DrivingLicenseDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_DrivingLicenseGetByDossierSacrifice", new[] {parameter}));
        }

        public static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseDBCollectionByDrivingLicenseTypeDB(Guid DrivingLicenseTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DrivingLicenseTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DrivingLicenseTypeId;
            return GetDossierSacrifice_DrivingLicenseDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_DrivingLicenseGetByDrivingLicenseType", new[] {parameter}));
        }

        public static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseDBCollectionByDrivingRestrictionDB(Guid DrivingRestrictionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DrivingRestrictionId", SqlDbType.UniqueIdentifier);
            parameter.Value = DrivingRestrictionId;
            return GetDossierSacrifice_DrivingLicenseDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_DrivingLicenseGetByDrivingRestriction", new[] {parameter}));
        }

        #endregion
    }

    #endregion
}