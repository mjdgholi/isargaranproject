﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <  مقطع تحصیلی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "EducationDegreeDB"

    public class EducationDegreeDB
    {


        #region Methods :

        public void AddEducationDegreeDB(EducationDegreeEntity EducationDegreeEntityParam, out Guid EucationDegreeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EducationSystemId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreePersianTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@EucationDegreeEnglishTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = EducationDegreeEntityParam.EducationSystemId;
            parameters[2].Value = FarsiToArabic.ToArabic(EducationDegreeEntityParam.EucationDegreePersianTitle.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(EducationDegreeEntityParam.EucationDegreeEnglishTitle.Trim());
            parameters[4].Value = EducationDegreeEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationDegreeAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EucationDegreeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationDegreeDB(EducationDegreeEntity EducationDegreeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EducationSystemId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreePersianTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@EucationDegreeEnglishTitle", SqlDbType.NVarChar,100) ,
											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EducationDegreeEntityParam.EucationDegreeId;
            parameters[1].Value = EducationDegreeEntityParam.EducationSystemId;
            parameters[2].Value = FarsiToArabic.ToArabic(EducationDegreeEntityParam.EucationDegreePersianTitle.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(EducationDegreeEntityParam.EucationDegreeEnglishTitle.Trim());
            parameters[4].Value = EducationDegreeEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationDegreeUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationDegreeDB(EducationDegreeEntity EducationDegreeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EducationDegreeEntityParam.EucationDegreeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationDegreeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationDegreeEntity GetSingleEducationDegreeDB(EducationDegreeEntity EducationDegreeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EducationDegreeEntityParam.EucationDegreeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationDegreeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationDegreeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationDegreeEntity> GetAllEducationDegreeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationDegreeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationDegreeGetAll", new IDataParameter[] { }));
        }
        public List<EducationDegreeEntity> GetAllEducationDegreeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationDegreeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationDegreeIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<EducationDegreeEntity> GetPageEducationDegreeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EducationDegree";
            parameters[5].Value = "EucationDegreeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationDegreeDBCollectionFromDataSet(ds, out count);
        }

        public EducationDegreeEntity GetEducationDegreeDBFromDataReader(IDataReader reader)
        {
            return new EducationDegreeEntity(Guid.Parse(reader["EucationDegreeId"].ToString()),
                                    Guid.Parse(reader["EducationSystemId"].ToString()),
                                    reader["EucationDegreePersianTitle"].ToString(),
                                    reader["EucationDegreeEnglishTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationDegreeEntity> GetEducationDegreeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationDegreeEntity> lst = new List<EducationDegreeEntity>();
                while (reader.Read())
                    lst.Add(GetEducationDegreeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationDegreeEntity> GetEducationDegreeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationDegreeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<EducationDegreeEntity> GetEducationDegreeDBCollectionByEducationSystemDB(EducationDegreeEntity EducationDegreeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationSystemId", SqlDbType.UniqueIdentifier);
            parameter.Value = EducationDegreeEntityParam.EducationSystemId;
            return GetEducationDegreeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_EducationDegreeGetByEducationSystem", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}
