﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <وضیعت پرونده>
    /// </summary>



    public class DossierStatusDB
    {

        #region Methods :

        public void AddDossierStatusDB(DossierStatusEntity DossierStatusEntityParam, out Guid DossierStatusId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierStatusId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierStatusTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(DossierStatusEntityParam.DossierStatusTitle.Trim());
            parameters[2].Value = DossierStatusEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierStatusAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierStatusId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierStatusDB(DossierStatusEntity DossierStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierStatusId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierStatusTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierStatusEntityParam.DossierStatusId;
            parameters[1].Value = FarsiToArabic.ToArabic(DossierStatusEntityParam.DossierStatusTitle.Trim());
            parameters[2].Value = DossierStatusEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierStatusUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierStatusDB(DossierStatusEntity DossierStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierStatusId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierStatusEntityParam.DossierStatusId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierStatusDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierStatusEntity GetSingleDossierStatusDB(DossierStatusEntity DossierStatusEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierStatusId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierStatusEntityParam.DossierStatusId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierStatusGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierStatusDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierStatusEntity> GetAllDossierStatusDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierStatusGetAll", new IDataParameter[] { }));
        }
        public List<DossierStatusEntity> GetAllDossierStatusIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierStatusIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<DossierStatusEntity> GetPageDossierStatusDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierStatus";
            parameters[5].Value = "DossierStatusId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierStatusDBCollectionFromDataSet(ds, out count);
        }

        public DossierStatusEntity GetDossierStatusDBFromDataReader(IDataReader reader)
        {
            return new DossierStatusEntity(Guid.Parse(reader["DossierStatusId"].ToString()),
                                    reader["DossierStatusTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DossierStatusEntity> GetDossierStatusDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierStatusEntity> lst = new List<DossierStatusEntity>();
                while (reader.Read())
                    lst.Add(GetDossierStatusDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierStatusEntity> GetDossierStatusDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierStatusDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    
}
