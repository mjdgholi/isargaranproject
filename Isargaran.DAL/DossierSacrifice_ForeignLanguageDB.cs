﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <اطلاعات زبان های خارجی>
    /// </summary>

    #region Class "DossierSacrifice_ForeignLanguageDB"

    public class DossierSacrifice_ForeignLanguageDB
    {        
        #region Methods :

        public void AddDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam, out Guid DossierSacrifice_ForeignLanguageId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ListeningSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpeakingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ReadingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@WritingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),                              
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_ForeignLanguageEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_ForeignLanguageEntityParam.ForeignLanguageId;
            parameters[3].Value = (DossierSacrifice_ForeignLanguageEntityParam.ListeningSkillId == null)?DBNull.Value:(object)DossierSacrifice_ForeignLanguageEntityParam.ListeningSkillId;
            parameters[4].Value = (DossierSacrifice_ForeignLanguageEntityParam.SpeakingSkillId == null) ? DBNull.Value : (object)DossierSacrifice_ForeignLanguageEntityParam.SpeakingSkillId;
            parameters[5].Value = (DossierSacrifice_ForeignLanguageEntityParam.ReadingSkillId == null) ? DBNull.Value : (object)DossierSacrifice_ForeignLanguageEntityParam.ReadingSkillId; 
            parameters[6].Value = (DossierSacrifice_ForeignLanguageEntityParam.WritingSkillId == null) ? DBNull.Value : (object)DossierSacrifice_ForeignLanguageEntityParam.WritingSkillId;
            parameters[7].Value = DossierSacrifice_ForeignLanguageEntityParam.Description;    
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_ForeignLanguageAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_ForeignLanguageId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ListeningSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpeakingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ReadingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@WritingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),                                           
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_ForeignLanguageEntityParam.DossierSacrifice_ForeignLanguageId;
            parameters[1].Value = DossierSacrifice_ForeignLanguageEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_ForeignLanguageEntityParam.ForeignLanguageId;
            parameters[3].Value = (DossierSacrifice_ForeignLanguageEntityParam.ListeningSkillId == null) ? DBNull.Value : (object)DossierSacrifice_ForeignLanguageEntityParam.ListeningSkillId;
            parameters[4].Value = (DossierSacrifice_ForeignLanguageEntityParam.SpeakingSkillId == null) ? DBNull.Value : (object)DossierSacrifice_ForeignLanguageEntityParam.SpeakingSkillId;
            parameters[5].Value = (DossierSacrifice_ForeignLanguageEntityParam.ReadingSkillId == null) ? DBNull.Value : (object)DossierSacrifice_ForeignLanguageEntityParam.ReadingSkillId;
            parameters[6].Value = (DossierSacrifice_ForeignLanguageEntityParam.WritingSkillId == null) ? DBNull.Value : (object)DossierSacrifice_ForeignLanguageEntityParam.WritingSkillId;
            parameters[7].Value = DossierSacrifice_ForeignLanguageEntityParam.Description;
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_ForeignLanguageUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_ForeignLanguageEntityParam.DossierSacrifice_ForeignLanguageId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_ForeignLanguageDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_ForeignLanguageEntity GetSingleDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_ForeignLanguageId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_ForeignLanguageEntityParam.DossierSacrifice_ForeignLanguageId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_ForeignLanguageGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_ForeignLanguageDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetAllDossierSacrifice_ForeignLanguageDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_DossierSacrifice_ForeignLanguageGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetPageDossierSacrifice_ForeignLanguageDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_DossierSacrifice_ForeignLanguage";
            parameters[5].Value = "DossierSacrifice_ForeignLanguageId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_ForeignLanguageEntity GetDossierSacrifice_ForeignLanguageDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_ForeignLanguageEntity(new Guid(reader["DossierSacrifice_ForeignLanguageId"].ToString()), 
                                                              Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                              Guid.Parse(reader["ForeignLanguageId"].ToString()),
                                                              Convert.IsDBNull(reader["ListeningSkillId"])?(Guid?)null:Guid.Parse(reader["ListeningSkillId"].ToString()),
                                                              Convert.IsDBNull(reader["SpeakingSkillId"]) ? (Guid?)null : Guid.Parse(reader["SpeakingSkillId"].ToString()),
                                                              Convert.IsDBNull(reader["ReadingSkillId"]) ? (Guid?)null : Guid.Parse(reader["ReadingSkillId"].ToString()),
                                                              Convert.IsDBNull(reader["WritingSkillId"]) ? (Guid?)null : Guid.Parse(reader["WritingSkillId"].ToString()),                                                             
                                                              reader["Description"].ToString(),
                                                              reader["CreationDate"].ToString(),
                                                              reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_ForeignLanguageEntity> lst = new List<DossierSacrifice_ForeignLanguageEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_ForeignLanguageDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionByDossierSacrificeDB(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeForeignLanguageEntity.DossierSacrificeId;
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_ForeignLanguageGetByDossierSacrifice", new[] {parameter}));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionByForeignLanguageDB(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ForeignLanguageId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeForeignLanguageEntity.ForeignLanguageId;
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_ForeignLanguageGetByForeignLanguage", new[] {parameter}));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionByListeningSkillDB(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ListeningSkillId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeForeignLanguageEntity.ListeningSkillId;
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_ForeignLanguageGetByListeningSkill", new[] {parameter}));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionByReadingSkillDB(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ReadingSkillId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeForeignLanguageEntity.ReadingSkillId;
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_ForeignLanguageGetByReadingSkill", new[] {parameter}));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionBySpeakingSkillDB(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SpeakingSkillId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeForeignLanguageEntity.SpeakingSkillId;
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_ForeignLanguageGetBySpeakingSkill", new[] {parameter}));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageDBCollectionByWritingSkillDB(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@WritingSkillId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeForeignLanguageEntity.WritingSkillId;
            return GetDossierSacrifice_ForeignLanguageDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_ForeignLanguageGetByWritingSkill", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}