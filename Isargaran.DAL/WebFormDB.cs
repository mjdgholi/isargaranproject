﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Intranet.Security;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Update Author: <Narges.Kamran> <1393/08/20>
    /// Create date: <1393/03/11>
    /// 
    /// Description: <وب فرم ها>
    /// </summary>

    #region Class "WebFormDB"

    public class WebFormDB
    {



        #region Methods :

        public void AddWebFormDB(WebFormEntity WebFormEntityParam, out Guid WebFormId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@WebFormId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@WebFormTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@WebFormURL", SqlDbType.NVarChar, -1),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(WebFormEntityParam.WebFormTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(WebFormEntityParam.WebFormURL.Trim());
            parameters[3].Value = WebFormEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_WebFormAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            WebFormId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateWebFormDB(WebFormEntity WebFormEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@WebFormId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@WebFormTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@WebFormURL", SqlDbType.NVarChar, -1),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = WebFormEntityParam.WebFormId;
            parameters[1].Value = FarsiToArabic.ToArabic(WebFormEntityParam.WebFormTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(WebFormEntityParam.WebFormURL.Trim());
            parameters[3].Value = WebFormEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_WebFormUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteWebFormDB(WebFormEntity WebFormEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@WebFormId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = WebFormEntityParam.WebFormId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_WebFormDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public WebFormEntity GetSingleWebFormDB(WebFormEntity WebFormEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@WebFormId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value = WebFormEntityParam.WebFormId;

                reader = _intranetDB.RunProcedureReader("Isar.p_WebFormGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetWebFormDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WebFormEntity> GetAllWebFormDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWebFormDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_WebFormGetAll", new IDataParameter[] {}));
        }

        public List<WebFormEntity> GetPageWebFormDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy,
            out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_WebForm";
            parameters[5].Value = "WebFormId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetWebFormDBCollectionFromDataSet(ds, out count);
        }

        public WebFormEntity GetWebFormDBFromDataReader(IDataReader reader)
        {
            return new WebFormEntity(new Guid(reader["WebFormId"].ToString()),
                reader["WebFormTitle"].ToString(),
                reader["WebFormURL"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString(),
                bool.Parse(reader["IsActive"].ToString()));
        }

        public List<WebFormEntity> GetWebFormDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<WebFormEntity> lst = new List<WebFormEntity>();
                while (reader.Read())
                    lst.Add(GetWebFormDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WebFormEntity> GetWebFormDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetWebFormDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<WebFormEntity> GetWebFormDBCollectionByWebFormCategorizeDB(Guid WebFormCategorizeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier);
            parameter.Value = WebFormCategorizeId;
            return
                GetWebFormDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_WebFormGetByWebFormCategorize", new[] {parameter}));
        }


        #endregion

        public List<WebFormEntity> GetWebFormDBCollectionByWebFormCategorizePerRowAccessDB(Guid formCategoryId, string userId, string objectTitleStr)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("WebFormCategorizeId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@WebUserId", SqlDbType.Int),
                new SqlParameter("@ObjectTitlesForRowAccess_Str", SqlDbType.NVarChar, -1)

            };
            parameters[0].Value = formCategoryId;
            parameters[1].Value = userId;
            parameters[2].Value = objectTitleStr;
            return GetWebFormDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_WebFormGetByWebFormCategorizePerRowAccess", parameters));
        }
    }

    #endregion
}