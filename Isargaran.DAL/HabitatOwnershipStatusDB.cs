﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/17>
    /// Description: <وضیعت مالکیت مسکن>
    /// </summary>


    public class HabitatOwnershipStatusDB
    {


        #region Methods :

        public void AddHabitatOwnershipStatusDB(HabitatOwnershipStatusEntity HabitatOwnershipStatusEntityParam, out Guid HabitatOwnershipStatusId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@HabitatOwnershipStatusId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@HabitatOwnershipStatusTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value =FarsiToArabic.ToArabic(HabitatOwnershipStatusEntityParam.HabitatOwnershipStatusTitle.Trim());
            parameters[2].Value = HabitatOwnershipStatusEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatOwnershipStatusAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            HabitatOwnershipStatusId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateHabitatOwnershipStatusDB(HabitatOwnershipStatusEntity HabitatOwnershipStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@HabitatOwnershipStatusId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@HabitatOwnershipStatusTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = HabitatOwnershipStatusEntityParam.HabitatOwnershipStatusId;
            parameters[1].Value = FarsiToArabic.ToArabic(HabitatOwnershipStatusEntityParam.HabitatOwnershipStatusTitle.Trim());
            parameters[2].Value = HabitatOwnershipStatusEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_HabitatOwnershipStatusUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteHabitatOwnershipStatusDB(HabitatOwnershipStatusEntity HabitatOwnershipStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@HabitatOwnershipStatusId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = HabitatOwnershipStatusEntityParam.HabitatOwnershipStatusId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatOwnershipStatusDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public HabitatOwnershipStatusEntity GetSingleHabitatOwnershipStatusDB(HabitatOwnershipStatusEntity HabitatOwnershipStatusEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@HabitatOwnershipStatusId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = HabitatOwnershipStatusEntityParam.HabitatOwnershipStatusId;

                reader = _intranetDB.RunProcedureReader("Isar.p_HabitatOwnershipStatusGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetHabitatOwnershipStatusDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatOwnershipStatusEntity> GetAllHabitatOwnershipStatusDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatOwnershipStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatOwnershipStatusGetAll", new IDataParameter[] { }));
        }
        public List<HabitatOwnershipStatusEntity> GetAllHabitatOwnershipStatusIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatOwnershipStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatOwnershipStatusIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<HabitatOwnershipStatusEntity> GetPageHabitatOwnershipStatusDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_HabitatOwnershipStatus";
            parameters[5].Value = "HabitatOwnershipStatusId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetHabitatOwnershipStatusDBCollectionFromDataSet(ds, out count);
        }

        public HabitatOwnershipStatusEntity GetHabitatOwnershipStatusDBFromDataReader(IDataReader reader)
        {
            return new HabitatOwnershipStatusEntity(Guid.Parse(reader["HabitatOwnershipStatusId"].ToString()),
                                    reader["HabitatOwnershipStatusTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<HabitatOwnershipStatusEntity> GetHabitatOwnershipStatusDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<HabitatOwnershipStatusEntity> lst = new List<HabitatOwnershipStatusEntity>();
                while (reader.Read())
                    lst.Add(GetHabitatOwnershipStatusDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatOwnershipStatusEntity> GetHabitatOwnershipStatusDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetHabitatOwnershipStatusDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }


}
