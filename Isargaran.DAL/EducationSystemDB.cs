﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <سیستم تحصیلی>
    /// </summary>



    public class EducationSystemDB
    {

        #region Methods :

        public void AddEducationSystemDB(EducationSystemEntity EducationSystemEntityParam, out Guid EducationSystemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EducationSystemId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EducationSystemPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EducationSystemEnglishTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationSystemEntityParam.EducationSystemPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationSystemEntityParam.EducationSystemEnglishTitle.Trim());            
            parameters[3].Value = EducationSystemEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationSystemAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EducationSystemId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationSystemDB(EducationSystemEntity EducationSystemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EducationSystemId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EducationSystemPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EducationSystemEnglishTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EducationSystemEntityParam.EducationSystemId;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationSystemEntityParam.EducationSystemPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationSystemEntityParam.EducationSystemEnglishTitle.Trim());              
            parameters[3].Value = EducationSystemEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationSystemUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationSystemDB(EducationSystemEntity EducationSystemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EducationSystemId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EducationSystemEntityParam.EducationSystemId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationSystemDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationSystemEntity GetSingleEducationSystemDB(EducationSystemEntity EducationSystemEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EducationSystemId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EducationSystemEntityParam.EducationSystemId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationSystemGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationSystemDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationSystemEntity> GetAllEducationSystemDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationSystemDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationSystemGetAll", new IDataParameter[] { }));
        }
        public List<EducationSystemEntity> GetAllEducationSystemIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationSystemDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationSystemIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<EducationSystemEntity> GetPageEducationSystemDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EducationSystem";
            parameters[5].Value = "EducationSystemId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationSystemDBCollectionFromDataSet(ds, out count);
        }

        public EducationSystemEntity GetEducationSystemDBFromDataReader(IDataReader reader)
        {
            return new EducationSystemEntity(Guid.Parse(reader["EducationSystemId"].ToString()),
                                    reader["EducationSystemPersianTitle"].ToString(),
                                    reader["EducationSystemEnglishTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationSystemEntity> GetEducationSystemDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationSystemEntity> lst = new List<EducationSystemEntity>();
                while (reader.Read())
                    lst.Add(GetEducationSystemDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationSystemEntity> GetEducationSystemDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationSystemDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
