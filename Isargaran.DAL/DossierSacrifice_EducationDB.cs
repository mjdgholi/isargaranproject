﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/14>
    /// Description: <پرونده  تحصیلی ایثارگری>
    /// </summary>
    #region Class "DossierSacrifice_EducationDB"

    public class DossierSacrifice_EducationDB
    {
        #region Methods :

        public void AddDossierSacrifice_EducationDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam, out Guid DossierSacrifice_EducationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_EducationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@StudentNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@GradePointAverage", SqlDbType.Decimal) ,
											  new SqlParameter("@FirstSupervisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SecondSupervisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@FirstAdvisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SecondAdvisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ThesisSubject", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@TechnicalDescription", SqlDbType.NVarChar,-1) ,				
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_EducationEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_EducationEntityParam.AdmissionTypeId==null?System.DBNull.Value:(object)DossierSacrifice_EducationEntityParam.AdmissionTypeId);
            parameters[3].Value = (DossierSacrifice_EducationEntityParam.EducationCenterId==null?System.DBNull.Value:(object)DossierSacrifice_EducationEntityParam.EducationCenterId);
            parameters[4].Value = DossierSacrifice_EducationEntityParam.EucationDegreeId;
            parameters[5].Value = (DossierSacrifice_EducationEntityParam.EducationCourseId==null?System.DBNull.Value:(object)DossierSacrifice_EducationEntityParam.EducationCourseId);
            parameters[6].Value = (DossierSacrifice_EducationEntityParam.EducationOrientationId==null?System.DBNull.Value:(object)DossierSacrifice_EducationEntityParam.EducationOrientationId);
            parameters[7].Value = (DossierSacrifice_EducationEntityParam.CityId==null?System.DBNull.Value:(object)DossierSacrifice_EducationEntityParam.CityId);
            parameters[8].Value = (DossierSacrifice_EducationEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.StartDate);
            parameters[9].Value = (DossierSacrifice_EducationEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.EndDate); 
            parameters[10].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.StudentNo.Trim());
            parameters[11].Value = (DossierSacrifice_EducationEntityParam.GradePointAverage.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.GradePointAverage);
            parameters[12].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.FirstSupervisor.Trim());
            parameters[13].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.SecondSupervisor.Trim());
            parameters[14].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.FirstAdvisor.Trim());
            parameters[15].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.SecondAdvisor.Trim());
            parameters[16].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.ThesisSubject.Trim());
            parameters[17].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.TechnicalDescription.Trim());
            

            parameters[18].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EducationAdd", parameters);
            var messageError = parameters[18].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_EducationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_EducationDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_EducationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@StudentNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@GradePointAverage", SqlDbType.Decimal) ,
											  new SqlParameter("@FirstSupervisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SecondSupervisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@FirstAdvisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SecondAdvisor", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ThesisSubject", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@TechnicalDescription", SqlDbType.NVarChar,-1) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_EducationEntityParam.DossierSacrifice_EducationId;
            parameters[1].Value = DossierSacrifice_EducationEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_EducationEntityParam.AdmissionTypeId == null ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.AdmissionTypeId);
            parameters[3].Value = (DossierSacrifice_EducationEntityParam.EducationCenterId == null ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.EducationCenterId);
            parameters[4].Value = DossierSacrifice_EducationEntityParam.EucationDegreeId;
            parameters[5].Value = (DossierSacrifice_EducationEntityParam.EducationCourseId == null ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.EducationCourseId);
            parameters[6].Value = (DossierSacrifice_EducationEntityParam.EducationOrientationId == null ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.EducationOrientationId);
            parameters[7].Value = (DossierSacrifice_EducationEntityParam.CityId == null ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.CityId);
            parameters[8].Value = (DossierSacrifice_EducationEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.StartDate);
            parameters[9].Value = (DossierSacrifice_EducationEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.EndDate);
            parameters[10].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.StudentNo.Trim());
            parameters[11].Value = (DossierSacrifice_EducationEntityParam.GradePointAverage.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_EducationEntityParam.GradePointAverage);
            parameters[12].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.FirstSupervisor.Trim());
            parameters[13].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.SecondSupervisor.Trim());
            parameters[14].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.FirstAdvisor.Trim());
            parameters[15].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.SecondAdvisor.Trim());
            parameters[16].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.ThesisSubject.Trim());
            parameters[17].Value = FarsiToArabic.ToArabic(DossierSacrifice_EducationEntityParam.TechnicalDescription.Trim());


            parameters[18].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EducationUpdate", parameters);
            var messageError = parameters[18].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_EducationDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_EducationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_EducationEntityParam.DossierSacrifice_EducationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EducationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_EducationEntity GetSingleDossierSacrifice_EducationDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_EducationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_EducationEntityParam.DossierSacrifice_EducationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_EducationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_EducationEntity> GetAllDossierSacrifice_EducationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_EducationGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_EducationEntity> GetPageDossierSacrifice_EducationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Education";
            parameters[5].Value = "DossierSacrifice_EducationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_EducationDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_EducationEntity GetDossierSacrifice_EducationDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_EducationEntity(Guid.Parse(reader["DossierSacrifice_EducationId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Convert.IsDBNull(reader["AdmissionTypeId"]) ? null : (Guid?)reader["AdmissionTypeId"],                                    
                                    Convert.IsDBNull(reader["EducationCenterId"]) ? null : (Guid?)reader["EducationCenterId"],                                    
                                    Guid.Parse(reader["EucationDegreeId"].ToString()),                                    
                                    Convert.IsDBNull(reader["EducationCourseId"]) ? null : (Guid?)reader["EducationCourseId"],
                                     Convert.IsDBNull(reader["EducationOrientationId"]) ? null : (Guid?)reader["EducationOrientationId"],
                                       Convert.IsDBNull(reader["CityId"]) ? null : (Guid?)reader["CityId"],                                    
                                    reader["StartDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["StudentNo"].ToString(),
                                    Convert.IsDBNull(reader["GradePointAverage"]) ? null : (decimal?)reader["GradePointAverage"],          
                                    reader["FirstSupervisor"].ToString(),
                                    reader["SecondSupervisor"].ToString(),
                                    reader["FirstAdvisor"].ToString(),
                                    reader["SecondAdvisor"].ToString(),
                                    reader["ThesisSubject"].ToString(),
                                    reader["TechnicalDescription"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    Convert.IsDBNull(reader["ProvinceId"]) ? null : (Guid?)reader["ProvinceId"]);
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_EducationEntity> lst = new List<DossierSacrifice_EducationEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_EducationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionByAdmissionTypeDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@AdmissionTypeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_EducationEntityParam.AdmissionTypeId;
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetByAdmissionType", new[] { parameter }));
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionByCityDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CityId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_EducationEntityParam.CityId;
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetByCity", new[] { parameter }));
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionByDossierSacrificeDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_EducationEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionByEducationCenterDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationCenterId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_EducationEntityParam.EducationCenterId;
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetByEducationCenter", new[] { parameter }));
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionByEducationCourseDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationCourseId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_EducationEntityParam.EducationCourseId;
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetByEducationCourse", new[] { parameter }));
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionByEducationDegreeDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EucationDegreeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_EducationEntityParam.EucationDegreeId;
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetByEducationDegree", new[] { parameter }));
        }

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationDBCollectionByEducationOrientationDB(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationOrientationId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_EducationEntityParam.EducationOrientationId;
            return GetDossierSacrifice_EducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EducationGetByEducationOrientation", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}
