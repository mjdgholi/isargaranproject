﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/09>
    /// Description: <پرونده اطلاعات تالیف و ترجمه ایثارگر>
    /// </summary>


    public class DossierSacrifice_TranslationOrCompilationDB
    {
        #region Methods :

        public void AddDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam, out Guid DossierSacrifice_TranslationOrCompilationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_TranslationOrCompilationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@BaseBookTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@TranslatedBookTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@BookSubject", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@Publication", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@FirstPublicationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TurnPublicationNo", SqlDbType.Int) ,
											  new SqlParameter("@WritingActivityTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_TranslationOrCompilationEntityParam.DossierSacrificeId;
            parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.BaseBookTitle.Trim());
            parameters[3].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.TranslatedBookTitle==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.TranslatedBookTitle.Trim()));
            parameters[4].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.BookSubject==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.BookSubject.Trim()));
            parameters[5].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.Publication==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.Publication.Trim()));
            parameters[6].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.FirstPublicationDate==""?System.DBNull.Value:(object)DossierSacrifice_TranslationOrCompilationEntityParam.FirstPublicationDate);
            parameters[7].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.TurnPublicationNo==null?System.DBNull.Value:(object)DossierSacrifice_TranslationOrCompilationEntityParam.TurnPublicationNo);
            parameters[8].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.WritingActivityTypeId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_TranslationOrCompilationEntityParam.WritingActivityTypeId);
            parameters[9].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.Description.Trim()));

            parameters[10].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TranslationOrCompilationAdd", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_TranslationOrCompilationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_TranslationOrCompilationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@BaseBookTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@TranslatedBookTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@BookSubject", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@Publication", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@FirstPublicationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TurnPublicationNo", SqlDbType.Int) ,
											  new SqlParameter("@WritingActivityTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_TranslationOrCompilationEntityParam.DossierSacrifice_TranslationOrCompilationId;
            parameters[1].Value = DossierSacrifice_TranslationOrCompilationEntityParam.DossierSacrificeId;
            parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.BaseBookTitle.Trim());
            parameters[3].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.TranslatedBookTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.TranslatedBookTitle.Trim()));
            parameters[4].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.BookSubject == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.BookSubject.Trim()));
            parameters[5].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.Publication == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.Publication.Trim()));
            parameters[6].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.FirstPublicationDate == "" ? System.DBNull.Value : (object)DossierSacrifice_TranslationOrCompilationEntityParam.FirstPublicationDate);
            parameters[7].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.TurnPublicationNo == null ? System.DBNull.Value : (object)DossierSacrifice_TranslationOrCompilationEntityParam.TurnPublicationNo);
            parameters[8].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.WritingActivityTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_TranslationOrCompilationEntityParam.WritingActivityTypeId);
            parameters[9].Value = (DossierSacrifice_TranslationOrCompilationEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TranslationOrCompilationEntityParam.Description.Trim()));

            parameters[10].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TranslationOrCompilationUpdate", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_TranslationOrCompilationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_TranslationOrCompilationEntityParam.DossierSacrifice_TranslationOrCompilationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TranslationOrCompilationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_TranslationOrCompilationEntity GetSingleDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_TranslationOrCompilationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_TranslationOrCompilationEntityParam.DossierSacrifice_TranslationOrCompilationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TranslationOrCompilationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_TranslationOrCompilationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TranslationOrCompilationEntity> GetAllDossierSacrifice_TranslationOrCompilationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_TranslationOrCompilationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_TranslationOrCompilationGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_TranslationOrCompilationEntity> GetPageDossierSacrifice_TranslationOrCompilationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_TranslationOrCompilation";
            parameters[5].Value = "DossierSacrifice_TranslationOrCompilationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_TranslationOrCompilationDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_TranslationOrCompilationEntity GetDossierSacrifice_TranslationOrCompilationDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_TranslationOrCompilationEntity(Guid.Parse(reader["DossierSacrifice_TranslationOrCompilationId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["BaseBookTitle"].ToString(),
                                    reader["TranslatedBookTitle"].ToString(),
                                    reader["BookSubject"].ToString(),
                                    reader["Publication"].ToString(),
                                    reader["FirstPublicationDate"].ToString(),
                                    Convert.IsDBNull(reader["TurnPublicationNo"]) ? null : (int?)reader["TurnPublicationNo"],
                                    Convert.IsDBNull(reader["WritingActivityTypeId"]) ? null : (Guid?)reader["WritingActivityTypeId"],                                    
                                    reader["Description"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["CreationDate"].ToString());
        }

        public List<DossierSacrifice_TranslationOrCompilationEntity> GetDossierSacrifice_TranslationOrCompilationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_TranslationOrCompilationEntity> lst = new List<DossierSacrifice_TranslationOrCompilationEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_TranslationOrCompilationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TranslationOrCompilationEntity> GetDossierSacrifice_TranslationOrCompilationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_TranslationOrCompilationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_TranslationOrCompilationEntity> GetDossierSacrifice_TranslationOrCompilationDBCollectionByDossierSacrificeDB(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TranslationOrCompilationEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_TranslationOrCompilationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TranslationOrCompilationGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_TranslationOrCompilationEntity> GetDossierSacrifice_TranslationOrCompilationDBCollectionByWritingActivityTypeDB(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@WritingActivityTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_TranslationOrCompilationEntityParam.WritingActivityTypeId;
            return GetDossierSacrifice_TranslationOrCompilationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TranslationOrCompilationGetByWritingActivityType", new[] { parameter }));
        }


        #endregion



    }

}
