﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
        /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <پرونده ایثارگری>
    /// </summary>


    public class DossierSacrificeDB
    {
        #region Methods :

        public void AddDossierSacrificeDB(DossierSacrificeEntity DossierSacrificeEntityParam, out Guid DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB(ConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString.ToString() + String.Format(";Workstation ID={0}", HttpContext.Current.User.Identity.Name));
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@SacrificeNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DossierStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@NaturalizationId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@NationalityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FaithId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ReligionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MarriageStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MarriageDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@HasInsurance", SqlDbType.Bit) ,
											  new SqlParameter("@InsuranceNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@HasPassport", SqlDbType.Bit) ,
											  new SqlParameter("@PassportNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@HomeAddress", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@HomePhoneNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@HomeFaxNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@CellphoneNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasDependentSacrifice", SqlDbType.Bit) ,
											  new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasDossierSacrifice", SqlDbType.Bit) ,
											  new SqlParameter("@ImagePath", SqlDbType.NVarChar,-1) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrificeEntityParam.DossierSacrificeNo;
            parameters[2].Value = DossierSacrificeEntityParam.SacrificeNo;
            parameters[3].Value = DossierSacrificeEntityParam.PersonId;
            parameters[4].Value = DossierSacrificeEntityParam.DossierStatusId;
            parameters[5].Value = DossierSacrificeEntityParam.NaturalizationId;
            parameters[6].Value = DossierSacrificeEntityParam.NationalityId;
            parameters[7].Value = DossierSacrificeEntityParam.FaithId;
            parameters[8].Value = DossierSacrificeEntityParam.ReligionId;
            parameters[9].Value = (DossierSacrificeEntityParam.MarriageStatusId==new Guid()?System.DBNull.Value:(object)DossierSacrificeEntityParam.MarriageStatusId);
            parameters[10].Value = (DossierSacrificeEntityParam.MarriageDate == "" ? System.DBNull.Value : (object)DossierSacrificeEntityParam.MarriageDate);
            parameters[11].Value = DossierSacrificeEntityParam.HasInsurance;
            parameters[12].Value = (DossierSacrificeEntityParam.InsuranceNo.Trim() == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.InsuranceNo.Trim()));
            parameters[13].Value = DossierSacrificeEntityParam.HasPassport;
            parameters[14].Value =(DossierSacrificeEntityParam.PassportNo.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.PassportNo.Trim()));
            parameters[15].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.Email.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.Email.Trim()));
            parameters[16].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeAddress.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeAddress.Trim()));
            parameters[17].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomePhoneNo.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomePhoneNo.Trim()));
            parameters[18].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeFaxNo.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeFaxNo.Trim()));
            parameters[19].Value =  (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.CellphoneNo.Trim())==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.CellphoneNo.Trim()));
            parameters[20].Value =  (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ZipCode.Trim())==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ZipCode.Trim()));
            parameters[21].Value = DossierSacrificeEntityParam.CityId;
            parameters[22].Value = DossierSacrificeEntityParam.HasDependentSacrifice;
            parameters[23].Value = (DossierSacrificeEntityParam.DependentTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrificeEntityParam.DependentTypeId);
            parameters[24].Value = DossierSacrificeEntityParam.HasDossierSacrifice;
            parameters[25].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ImagePath.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ImagePath.Trim()));            
            parameters[26].Value = DossierSacrificeEntityParam.IsActive;

            parameters[27].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrificeAdd", parameters);
            var messageError = parameters[27].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrificeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrificeDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB(ConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString.ToString() + String.Format(";Workstation ID={0}", HttpContext.Current.User.Identity.Name));
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@SacrificeNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DossierStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@NaturalizationId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@NationalityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FaithId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ReligionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MarriageStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MarriageDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@HasInsurance", SqlDbType.Bit) ,
											  new SqlParameter("@InsuranceNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@HasPassport", SqlDbType.Bit) ,
											  new SqlParameter("@PassportNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@HomeAddress", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@HomePhoneNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@HomeFaxNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@CellphoneNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasDependentSacrifice", SqlDbType.Bit) ,
											  new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasDossierSacrifice", SqlDbType.Bit) ,
											  new SqlParameter("@ImagePath", SqlDbType.NVarChar,-1) ,							
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrificeEntityParam.DossierSacrificeId;            
            parameters[1].Value = DossierSacrificeEntityParam.DossierSacrificeNo;
            parameters[2].Value = DossierSacrificeEntityParam.SacrificeNo;
            parameters[3].Value = DossierSacrificeEntityParam.PersonId;
            parameters[4].Value = DossierSacrificeEntityParam.DossierStatusId;
            parameters[5].Value = DossierSacrificeEntityParam.NaturalizationId;
            parameters[6].Value = DossierSacrificeEntityParam.NationalityId;
            parameters[7].Value = DossierSacrificeEntityParam.FaithId;
            parameters[8].Value = DossierSacrificeEntityParam.ReligionId;
            parameters[9].Value = (DossierSacrificeEntityParam.MarriageStatusId == new Guid() ? System.DBNull.Value : (object)DossierSacrificeEntityParam.MarriageStatusId);
            parameters[10].Value = (DossierSacrificeEntityParam.MarriageDate == "" ? System.DBNull.Value : (object)DossierSacrificeEntityParam.MarriageDate);
            parameters[11].Value = DossierSacrificeEntityParam.HasInsurance;
            parameters[12].Value = (DossierSacrificeEntityParam.InsuranceNo.Trim() == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.InsuranceNo.Trim()));
            parameters[13].Value = DossierSacrificeEntityParam.HasPassport;
            parameters[14].Value = (DossierSacrificeEntityParam.PassportNo.Trim() == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.PassportNo.Trim()));
            parameters[15].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.Email.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.Email.Trim()));
            parameters[16].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeAddress.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeAddress.Trim()));
            parameters[17].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomePhoneNo.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomePhoneNo.Trim()));
            parameters[18].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeFaxNo.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.HomeFaxNo.Trim()));
            parameters[19].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.CellphoneNo.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.CellphoneNo.Trim()));
            parameters[20].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ZipCode.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ZipCode.Trim()));
            parameters[21].Value = DossierSacrificeEntityParam.CityId;
            parameters[22].Value = DossierSacrificeEntityParam.HasDependentSacrifice;
            parameters[23].Value = (DossierSacrificeEntityParam.DependentTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrificeEntityParam.DependentTypeId);
            parameters[24].Value = DossierSacrificeEntityParam.HasDossierSacrifice;
            parameters[25].Value = (FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ImagePath.Trim()) == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrificeEntityParam.ImagePath.Trim()));
            parameters[26].Value = DossierSacrificeEntityParam.IsActive;

            parameters[27].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrificeUpdate", parameters);
            var messageError = parameters[27].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrificeDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB(ConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString.ToString() + String.Format(";Workstation ID={0}", HttpContext.Current.User.Identity.Name));
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrificeEntityParam.DossierSacrificeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrificeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrificeEntity GetSingleDossierSacrificeDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrificeEntityParam.DossierSacrificeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrificeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrificeEntity> GetAllDossierSacrificeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrificeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrificeGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrificeEntity> GetPageDossierSacrificeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice";
            parameters[5].Value = "DossierSacrificeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrificeDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrificeEntity GetDossierSacrificeDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrificeEntity(Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["DossierSacrificeNo"].ToString(),
                                    reader["SacrificeNo"].ToString(),
                                    Guid.Parse(reader["PersonId"].ToString()),
                                    reader["PersonFirstName"].ToString(),
                                    reader["PersonLastName"].ToString(),
                                    Guid.Parse(reader["DossierStatusId"].ToString()),
                                    Guid.Parse(reader["NaturalizationId"].ToString()),
                                    Guid.Parse(reader["NationalityId"].ToString()),
                                    Guid.Parse(reader["FaithId"].ToString()),
                                    Guid.Parse(reader["ReligionId"].ToString()),
                                    Convert.IsDBNull(reader["MarriageStatusId"]) ? null : (Guid?)reader["MarriageStatusId"],                                           
                                    reader["MarriageDate"].ToString(),
                                    bool.Parse(reader["HasInsurance"].ToString()),
                                    reader["InsuranceNo"].ToString(),
                                    bool.Parse(reader["HasPassport"].ToString()),
                                    reader["PassportNo"].ToString(),
                                    reader["Email"].ToString(),
                                    reader["HomeAddress"].ToString(),
                                    reader["HomePhoneNo"].ToString(),
                                    reader["HomeFaxNo"].ToString(),
                                    reader["CellphoneNo"].ToString(),
                                    reader["ZipCode"].ToString(),
                                    Guid.Parse(reader["CityId"].ToString()),
                                    bool.Parse(reader["HasDependentSacrifice"].ToString()),
                                    Convert.IsDBNull(reader["DependentTypeId"]) ? null : (Guid?)reader["DependentTypeId"],                                                                                          
                                    bool.Parse(reader["HasDossierSacrifice"].ToString()),
                                    reader["ImagePath"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    Guid.Parse(reader["ProvinceId"].ToString()));
        }

        public List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrificeEntity> lst = new List<DossierSacrificeEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrificeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrificeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByCityDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CityId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.CityId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByCity", new[] { parameter }));
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByCountryDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@NaturalizationId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.NaturalizationId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByCountry", new[] { parameter }));
        }


        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByDependentTypeDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.DependentTypeId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByDependentType", new[] { parameter }));
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByDossierStatusDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierStatusId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.DossierStatusId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByDossierStatus", new[] { parameter }));
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByFaithDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@FaithId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.FaithId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByFaith", new[] { parameter }));
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByMarriageStatusDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MarriageStatusId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.MarriageStatusId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByMarriageStatus", new[] { parameter }));
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByNationalityDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@NationalityId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.NationalityId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByNationality", new[] { parameter }));
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByPersonDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.PersonId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByPerson", new[] { parameter }));
        }

        public  List<DossierSacrificeEntity> GetDossierSacrificeDBCollectionByReligionDB(DossierSacrificeEntity DossierSacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ReligionId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeEntityParam.ReligionId;
            return GetDossierSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrificeGetByReligion", new[] { parameter }));
        }


        #endregion



    }

    

}
