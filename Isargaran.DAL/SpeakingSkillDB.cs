﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <مهارت گفتاری>
    /// </summary>

    #region Class "SpeakingSkillDB"

    public class SpeakingSkillDB
    {        
        #region Methods :

        public void AddSpeakingSkillDB(SpeakingSkillEntity SpeakingSkillEntityParam, out Guid SpeakingSkillId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@SpeakingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpeakingSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@SpeakingSkillEnglishTitle", SqlDbType.NVarChar, 200),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(SpeakingSkillEntityParam.SpeakingSkillPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(SpeakingSkillEntityParam.SpeakingSkillEnglishTitle.Trim());
            parameters[3].Value = SpeakingSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_SpeakingSkillAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SpeakingSkillId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSpeakingSkillDB(SpeakingSkillEntity SpeakingSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@SpeakingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpeakingSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@SpeakingSkillEnglishTitle", SqlDbType.NVarChar, 200),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = SpeakingSkillEntityParam.SpeakingSkillId;
            parameters[1].Value = FarsiToArabic.ToArabic(SpeakingSkillEntityParam.SpeakingSkillPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(SpeakingSkillEntityParam.SpeakingSkillEnglishTitle.Trim());
            parameters[3].Value = SpeakingSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_SpeakingSkillUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSpeakingSkillDB(SpeakingSkillEntity SpeakingSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@SpeakingSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = SpeakingSkillEntityParam.SpeakingSkillId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_SpeakingSkillDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SpeakingSkillEntity GetSingleSpeakingSkillDB(SpeakingSkillEntity SpeakingSkillEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@SpeakingSkillId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = SpeakingSkillEntityParam.SpeakingSkillId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_SpeakingSkillGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSpeakingSkillDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SpeakingSkillEntity> GetAllSpeakingSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSpeakingSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_SpeakingSkillGetAll", new IDataParameter[] {}));
        }

        public List<SpeakingSkillEntity> GetAllIsActiveSpeakingSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSpeakingSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_SpeakingSkillGetAllIsActive", new IDataParameter[] { }));
        }

        public List<SpeakingSkillEntity> GetPageSpeakingSkillDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_SpeakingSkill";
            parameters[5].Value = "SpeakingSkillId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetSpeakingSkillDBCollectionFromDataSet(ds, out count);
        }

        public SpeakingSkillEntity GetSpeakingSkillDBFromDataReader(IDataReader reader)
        {
            return new SpeakingSkillEntity(new Guid(reader["SpeakingSkillId"].ToString()), 
                                           reader["SpeakingSkillPersianTitle"].ToString(),
                                           reader["SpeakingSkillEnglishTitle"].ToString(),
                                           reader["CreationDate"].ToString(),
                                           reader["ModificationDate"].ToString(),
                                           bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SpeakingSkillEntity> GetSpeakingSkillDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SpeakingSkillEntity> lst = new List<SpeakingSkillEntity>();
                while (reader.Read())
                    lst.Add(GetSpeakingSkillDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SpeakingSkillEntity> GetSpeakingSkillDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSpeakingSkillDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

    #endregion
}