﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/25>
    /// Description: <نوع پیغام>
    /// </summary>

    public class MessageTypeDB
    {
        #region Methods :

        public void AddMessageTypeDB(MessageTypeEntity messageTypeEntityParam, out Guid MessageTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MessageTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@MessageTypeTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsInpout", SqlDbType.Bit) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = messageTypeEntityParam.MessageTypeTitle;
            parameters[2].Value = messageTypeEntityParam.IsInpout;
            parameters[3].Value = messageTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("p_MessageTypeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MessageTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMessageTypeDB(MessageTypeEntity messageTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MessageTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@MessageTypeTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsInpout", SqlDbType.Bit) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = messageTypeEntityParam.MessageTypeId;
            parameters[1].Value = messageTypeEntityParam.MessageTypeTitle;
            parameters[2].Value = messageTypeEntityParam.IsInpout;

            parameters[3].Value = messageTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("p_MessageTypeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMessageTypeDB(MessageTypeEntity MessageTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MessageTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MessageTypeEntityParam.MessageTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("p_MessageTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MessageTypeEntity GetSingleMessageTypeDB(MessageTypeEntity MessageTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@MessageTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = MessageTypeEntityParam.MessageTypeId;

                reader = _intranetDB.RunProcedureReader("p_MessageTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMessageTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessageTypeEntity> GetAllMessageTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMessageTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("p_MessageTypeGetAll", new IDataParameter[] { }));
        }

        public List<MessageTypeEntity> GetPageMessageTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MessageType";
            parameters[5].Value = "MessageTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("p_TablesGetPage", parameters);
            return GetMessageTypeDBCollectionFromDataSet(ds, out count);
        }

        public MessageTypeEntity GetMessageTypeDBFromDataReader(IDataReader reader)
        {
            return new MessageTypeEntity(Guid.Parse(reader["MessageTypeId"].ToString()),
                                    reader["MessageTypeTitle"].ToString(),
                                    bool.Parse(reader["IsInpout"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<MessageTypeEntity> GetMessageTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MessageTypeEntity> lst = new List<MessageTypeEntity>();
                while (reader.Read())
                    lst.Add(GetMessageTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MessageTypeEntity> GetMessageTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMessageTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}