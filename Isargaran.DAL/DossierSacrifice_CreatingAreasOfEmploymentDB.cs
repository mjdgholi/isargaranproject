﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/02/14>
    // Description:	<اطلاعات ایجاد زمینه اشتغال>
    /// </summary>

    #region Class "DossierSacrifice_CreatingAreasOfEmploymentDB"

    public class DossierSacrifice_CreatingAreasOfEmploymentDB
    {


        #region Methods :

        public void AddDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam, out Guid DossierSacrifice_CreatingAreasOfEmploymentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_CreatingAreasOfEmploymentId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EmployedPersonId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EmploymentDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsItWorking", SqlDbType.Bit),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.EmployedPersonId;
            parameters[3].Value = (DossierSacrifice_CreatingAreasOfEmploymentEntityParam.EmploymentDate == "") ? (object)DBNull.Value : DossierSacrifice_CreatingAreasOfEmploymentEntityParam.EmploymentDate;
            parameters[4].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.OrganizationId;
            parameters[5].Value = (DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DependentTypeId == null) ? DBNull.Value : (object) DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DependentTypeId;
            parameters[6].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.IsItWorking;
            parameters[7].Value = FarsiToArabic.ToArabic(DossierSacrifice_CreatingAreasOfEmploymentEntityParam.Description.Trim());
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_CreatingAreasOfEmploymentAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_CreatingAreasOfEmploymentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_CreatingAreasOfEmploymentId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EmployedPersonId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EmploymentDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsItWorking", SqlDbType.Bit),
                                            new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DossierSacrifice_CreatingAreasOfEmploymentId;
            parameters[1].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.EmployedPersonId;
            parameters[3].Value = (DossierSacrifice_CreatingAreasOfEmploymentEntityParam.EmploymentDate == "") ? (object)DBNull.Value : DossierSacrifice_CreatingAreasOfEmploymentEntityParam.EmploymentDate;
            parameters[4].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.OrganizationId;
            parameters[5].Value = (DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DependentTypeId == null) ? DBNull.Value : (object) DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DependentTypeId;
            parameters[6].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.IsItWorking;
            parameters[7].Value = FarsiToArabic.ToArabic(DossierSacrifice_CreatingAreasOfEmploymentEntityParam.Description.Trim());
            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_CreatingAreasOfEmploymentUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_CreatingAreasOfEmploymentId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DossierSacrifice_CreatingAreasOfEmploymentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_DossierSacrifice_CreatingAreasOfEmploymentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_CreatingAreasOfEmploymentEntity GetSingleDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_CreatingAreasOfEmploymentId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_CreatingAreasOfEmploymentEntityParam.DossierSacrifice_CreatingAreasOfEmploymentId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_CreatingAreasOfEmploymentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_CreatingAreasOfEmploymentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetAllDossierSacrifice_CreatingAreasOfEmploymentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_DossierSacrifice_CreatingAreasOfEmploymentGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetPageDossierSacrifice_CreatingAreasOfEmploymentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_CreatingAreasOfEmployment";
            parameters[5].Value = "DossierSacrifice_CreatingAreasOfEmploymentId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_CreatingAreasOfEmploymentEntity GetDossierSacrifice_CreatingAreasOfEmploymentDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_CreatingAreasOfEmploymentEntity(new Guid(reader["DossierSacrifice_CreatingAreasOfEmploymentId"].ToString()),
                                                                        Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                                        Guid.Parse(reader["EmployedPersonId"].ToString()),
                                                                        reader["EmploymentDate"].ToString(),
                                                                        Guid.Parse(reader["OrganizationId"].ToString()),
                                                                        Convert.IsDBNull(reader["DependentTypeId"]) ? (Guid?)null : Guid.Parse(reader["DependentTypeId"].ToString()),
                                                                        bool.Parse(reader["IsItWorking"].ToString()),
                                                                        reader["Description"].ToString(),
                                                                        reader["CreationDate"].ToString(),
                                                                        reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_CreatingAreasOfEmploymentEntity> lst = new List<DossierSacrifice_CreatingAreasOfEmploymentEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_CreatingAreasOfEmploymentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionByDependentTypeDB(Guid DependentTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DependentTypeId", SqlDbType.Int);
            parameter.Value = DependentTypeId;
            return GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_CreatingAreasOfEmploymentGetByDependentType", new[] {parameter}));
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionByDossierSacrificeDB(Guid DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrificeId;
            return GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("[Isar].p_DossierSacrifice_CreatingAreasOfEmploymentGetByDossierSacrifice", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}