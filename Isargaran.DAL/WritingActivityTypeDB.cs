﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/07>
    // Description:	< نوع فعالیت نویسندگی >
    /// </summary>
    public class WritingActivityTypeDB
    {
        #region Methods :

        public void AddWritingActivityTypeDB(WritingActivityTypeEntity WritingActivityTypeEntityParam, out Guid WritingActivityTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@WritingActivityTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@WritingActivityTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(WritingActivityTypeEntityParam.WritingActivityTypeTitle);
            parameters[2].Value = WritingActivityTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_WritingActivityTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            WritingActivityTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateWritingActivityTypeDB(WritingActivityTypeEntity WritingActivityTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@WritingActivityTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@WritingActivityTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = WritingActivityTypeEntityParam.WritingActivityTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(WritingActivityTypeEntityParam.WritingActivityTypeTitle);          
            parameters[2].Value = WritingActivityTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_WritingActivityTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteWritingActivityTypeDB(WritingActivityTypeEntity WritingActivityTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@WritingActivityTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = WritingActivityTypeEntityParam.WritingActivityTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_WritingActivityTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public WritingActivityTypeEntity GetSingleWritingActivityTypeDB(WritingActivityTypeEntity WritingActivityTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@WritingActivityTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = WritingActivityTypeEntityParam.WritingActivityTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_WritingActivityTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetWritingActivityTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WritingActivityTypeEntity> GetAllWritingActivityTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWritingActivityTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_WritingActivityTypeGetAll", new IDataParameter[] { }));
        }

        public List<WritingActivityTypeEntity> GetAllWritingActivityTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWritingActivityTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_WritingActivityTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<WritingActivityTypeEntity> GetPageWritingActivityTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_WritingActivityType";
            parameters[5].Value = "WritingActivityTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetWritingActivityTypeDBCollectionFromDataSet(ds, out count);
        }

        public WritingActivityTypeEntity GetWritingActivityTypeDBFromDataReader(IDataReader reader)
        {
            return new WritingActivityTypeEntity(Guid.Parse(reader["WritingActivityTypeId"].ToString()),
                                    reader["WritingActivityTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<WritingActivityTypeEntity> GetWritingActivityTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<WritingActivityTypeEntity> lst = new List<WritingActivityTypeEntity>();
                while (reader.Read())
                    lst.Add(GetWritingActivityTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WritingActivityTypeEntity> GetWritingActivityTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetWritingActivityTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }
}
