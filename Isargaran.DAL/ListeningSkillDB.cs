﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <مهارت شنیداری>
    /// </summary>

    #region Class "ListeningSkillDB"

    public class ListeningSkillDB
    {
        #region Methods :

        public void AddListeningSkillDB(ListeningSkillEntity ListeningSkillEntityParam, out Guid ListeningSkillId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ListeningSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ListeningSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@ListeningSkillEnglishTitle", SqlDbType.NVarChar, 200),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(ListeningSkillEntityParam.ListeningSkillPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(ListeningSkillEntityParam.ListeningSkillEnglishTitle.Trim());
            parameters[3].Value = ListeningSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_ListeningSkillAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ListeningSkillId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateListeningSkillDB(ListeningSkillEntity ListeningSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ListeningSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ListeningSkillPersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@ListeningSkillEnglishTitle", SqlDbType.NVarChar, 200),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = ListeningSkillEntityParam.ListeningSkillId;
            parameters[1].Value = FarsiToArabic.ToArabic(ListeningSkillEntityParam.ListeningSkillPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(ListeningSkillEntityParam.ListeningSkillEnglishTitle.Trim());
            parameters[3].Value = ListeningSkillEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_ListeningSkillUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteListeningSkillDB(ListeningSkillEntity ListeningSkillEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ListeningSkillId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = ListeningSkillEntityParam.ListeningSkillId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_ListeningSkillDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ListeningSkillEntity GetSingleListeningSkillDB(ListeningSkillEntity ListeningSkillEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@ListeningSkillId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = ListeningSkillEntityParam.ListeningSkillId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_ListeningSkillGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetListeningSkillDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ListeningSkillEntity> GetAllListeningSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetListeningSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_ListeningSkillGetAll", new IDataParameter[] {}));
        }

        public List<ListeningSkillEntity> GetAllIsActiveListeningSkillDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetListeningSkillDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_ListeningSkillGetAllIsActive", new IDataParameter[] { }));
        }

        public List<ListeningSkillEntity> GetPageListeningSkillDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_ListeningSkill";
            parameters[5].Value = "ListeningSkillId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetListeningSkillDBCollectionFromDataSet(ds, out count);
        }

        public ListeningSkillEntity GetListeningSkillDBFromDataReader(IDataReader reader)
        {
            return new ListeningSkillEntity(new Guid(reader["ListeningSkillId"].ToString()), 
                                            reader["ListeningSkillPersianTitle"].ToString(),
                                            reader["ListeningSkillEnglishTitle"].ToString(),
                                            reader["CreationDate"].ToString(),
                                            reader["ModificationDate"].ToString(),
                                            bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ListeningSkillEntity> GetListeningSkillDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ListeningSkillEntity> lst = new List<ListeningSkillEntity>();
                while (reader.Read())
                    lst.Add(GetListeningSkillDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ListeningSkillEntity> GetListeningSkillDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetListeningSkillDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion
    }

    #endregion
}