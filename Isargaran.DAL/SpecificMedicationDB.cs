﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/02/16>
    // Description:	<داروی خاص>
    /// </summary>

    #region Class "SpecificMedicationDB"

    public class SpecificMedicationDB
    {
        #region Methods :

        public void AddSpecificMedicationDB(SpecificMedicationEntity SpecificMedicationEntityParam, out Guid SpecificMedicationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpecificMedicationPersianTitle", SqlDbType.NVarChar, 300),
                                            new SqlParameter("@SpecificMedicationEnglishTitle", SqlDbType.NVarChar, 300),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(SpecificMedicationEntityParam.SpecificMedicationPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(SpecificMedicationEntityParam.SpecificMedicationEnglishTitle.Trim());
            parameters[3].Value = SpecificMedicationEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_SpecificMedicationAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SpecificMedicationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSpecificMedicationDB(SpecificMedicationEntity SpecificMedicationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SpecificMedicationPersianTitle", SqlDbType.NVarChar, 300),
                                            new SqlParameter("@SpecificMedicationEnglishTitle", SqlDbType.NVarChar, 300),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = SpecificMedicationEntityParam.SpecificMedicationId;
            parameters[1].Value = FarsiToArabic.ToArabic(SpecificMedicationEntityParam.SpecificMedicationPersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(SpecificMedicationEntityParam.SpecificMedicationEnglishTitle.Trim());
            parameters[3].Value = SpecificMedicationEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_SpecificMedicationUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSpecificMedicationDB(SpecificMedicationEntity SpecificMedicationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@SpecificMedicationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = SpecificMedicationEntityParam.SpecificMedicationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_SpecificMedicationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SpecificMedicationEntity GetSingleSpecificMedicationDB(SpecificMedicationEntity SpecificMedicationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@SpecificMedicationId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = SpecificMedicationEntityParam.SpecificMedicationId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_SpecificMedicationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSpecificMedicationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SpecificMedicationEntity> GetAllSpecificMedicationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSpecificMedicationDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_SpecificMedicationGetAll", new IDataParameter[] {}));
        }

        public List<SpecificMedicationEntity> GetAllIsActiveSpecificMedicationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSpecificMedicationDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_SpecificMedicationGetAllIsActive", new IDataParameter[] { }));
        }

        public List<SpecificMedicationEntity> GetPageSpecificMedicationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_SpecificMedication";
            parameters[5].Value = "SpecificMedicationId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetSpecificMedicationDBCollectionFromDataSet(ds, out count);
        }

        public SpecificMedicationEntity GetSpecificMedicationDBFromDataReader(IDataReader reader)
        {
            return new SpecificMedicationEntity(new Guid(reader["SpecificMedicationId"].ToString()), 
                                                reader["SpecificMedicationPersianTitle"].ToString(),
                                                reader["SpecificMedicationEnglishTitle"].ToString(),
                                                reader["CreationDate"].ToString(),
                                                reader["ModificationDate"].ToString(),
                                                bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SpecificMedicationEntity> GetSpecificMedicationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SpecificMedicationEntity> lst = new List<SpecificMedicationEntity>();
                while (reader.Read())
                    lst.Add(GetSpecificMedicationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SpecificMedicationEntity> GetSpecificMedicationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSpecificMedicationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }

    #endregion
}