﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/02/15>
    // Description:	<اطلاعات ملاقات>
    /// </summary>

    public class DossierSacrifice_VisitDB
    {
        #region Methods :

        public void AddDossierSacrifice_VisitDB(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam, out Guid DossierSacrifice_VisitId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_VisitId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VisitDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VisitLocation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Visitors", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@HasRequest", SqlDbType.Bit) ,
											  new SqlParameter("@RequestContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@HasAction", SqlDbType.Bit) ,
											  new SqlParameter("@ActionDescription", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_VisitEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_VisitEntityParam.VisitDate == "" ? System.DBNull.Value : (object)DossierSacrifice_VisitEntityParam.VisitDate);
            parameters[3].Value = (DossierSacrifice_VisitEntityParam.OccasionId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VisitEntityParam.OccasionId);
            parameters[4].Value = (DossierSacrifice_VisitEntityParam.VisitLocation==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.VisitLocation.Trim()));
            parameters[5].Value = (DossierSacrifice_VisitEntityParam.Visitors==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.Visitors.Trim()));
            parameters[6].Value = DossierSacrifice_VisitEntityParam.HasRequest;
            parameters[7].Value = (DossierSacrifice_VisitEntityParam.RequestContent==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.RequestContent.Trim()));
            parameters[8].Value = DossierSacrifice_VisitEntityParam.HasAction;
            parameters[9].Value = (DossierSacrifice_VisitEntityParam.ActionDescription==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.ActionDescription.Trim()));
            parameters[10].Value = (DossierSacrifice_VisitEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.Description.Trim()));
            

            parameters[11].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_VisitAdd", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_VisitId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_VisitDB(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_VisitId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VisitDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VisitLocation", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@Visitors", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@HasRequest", SqlDbType.Bit) ,
											  new SqlParameter("@RequestContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@HasAction", SqlDbType.Bit) ,
											  new SqlParameter("@ActionDescription", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_VisitEntityParam.DossierSacrifice_VisitId;
            parameters[1].Value = DossierSacrifice_VisitEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_VisitEntityParam.VisitDate == "" ? System.DBNull.Value : (object)DossierSacrifice_VisitEntityParam.VisitDate);
            parameters[3].Value = (DossierSacrifice_VisitEntityParam.OccasionId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VisitEntityParam.OccasionId);
            parameters[4].Value = (DossierSacrifice_VisitEntityParam.VisitLocation == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.VisitLocation.Trim()));
            parameters[5].Value = (DossierSacrifice_VisitEntityParam.Visitors == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.Visitors.Trim()));
            parameters[6].Value = DossierSacrifice_VisitEntityParam.HasRequest;
            parameters[7].Value = (DossierSacrifice_VisitEntityParam.RequestContent == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.RequestContent.Trim()));
            parameters[8].Value = DossierSacrifice_VisitEntityParam.HasAction;
            parameters[9].Value = (DossierSacrifice_VisitEntityParam.ActionDescription == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.ActionDescription.Trim()));
            parameters[10].Value = (DossierSacrifice_VisitEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_VisitEntityParam.Description.Trim()));

            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_VisitUpdate", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_VisitDB(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_VisitId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_VisitEntityParam.DossierSacrifice_VisitId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_VisitDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_VisitEntity GetSingleDossierSacrifice_VisitDB(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_VisitId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_VisitEntityParam.DossierSacrifice_VisitId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VisitGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_VisitDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_VisitEntity> GetAllDossierSacrifice_VisitDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_VisitDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_VisitGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_VisitEntity> GetPageDossierSacrifice_VisitDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Visit";
            parameters[5].Value = "DossierSacrifice_VisitId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_VisitDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_VisitEntity GetDossierSacrifice_VisitDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_VisitEntity(Guid.Parse(reader["DossierSacrifice_VisitId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),                                    
                                    reader["VisitDate"].ToString(),
                                    Convert.IsDBNull(reader["OccasionId"]) ? null : (Guid?)reader["OccasionId"],
                                    reader["VisitLocation"].ToString(),
                                    reader["Visitors"].ToString(),
                                    bool.Parse(reader["HasRequest"].ToString()),
                                    reader["RequestContent"].ToString(),
                                    bool.Parse(reader["HasAction"].ToString()),
                                    reader["ActionDescription"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_VisitEntity> GetDossierSacrifice_VisitDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_VisitEntity> lst = new List<DossierSacrifice_VisitEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_VisitDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_VisitEntity> GetDossierSacrifice_VisitDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_VisitDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_VisitEntity> GetDossierSacrifice_VisitDBCollectionByDossierSacrificeDB(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_VisitEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_VisitDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VisitGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_VisitEntity> GetDossierSacrifice_VisitDBCollectionByOccasionDB(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_VisitEntityParam.OccasionId;
            return GetDossierSacrifice_VisitDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VisitGetByOccasion", new[] { parameter }));
        }


        #endregion



    }


}
