﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/18>
    // Description:	<محل مسافرت>
    /// </summary>

    public class TripLocationDB
    {
        #region Methods :

        public void AddTripLocationDB(TripLocationEntity TripLocationEntityParam, out Guid TripLocationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@TripLocationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@TripLocationTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(TripLocationEntityParam.TripLocationTitle.Trim());
            parameters[2].Value = TripLocationEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_TripLocationAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            TripLocationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateTripLocationDB(TripLocationEntity TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@TripLocationId", SqlDbType.UniqueIdentifier),						 
					 new SqlParameter("@TripLocationTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = TripLocationEntityParam.TripLocationId;
            parameters[1].Value = FarsiToArabic.ToArabic(TripLocationEntityParam.TripLocationTitle.Trim());
            parameters[2].Value = TripLocationEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_TripLocationUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteTripLocationDB(TripLocationEntity TripLocationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@TripLocationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = TripLocationEntityParam.TripLocationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_TripLocationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public TripLocationEntity GetSingleTripLocationDB(TripLocationEntity TripLocationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@TripLocationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = TripLocationEntityParam.TripLocationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_TripLocationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetTripLocationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<TripLocationEntity> GetAllTripLocationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetTripLocationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_TripLocationGetAll", new IDataParameter[] { }));
        }

        public List<TripLocationEntity> GetAllTripLocationIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetTripLocationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_TripLocationIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<TripLocationEntity> GetPageTripLocationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_TripLocation";
            parameters[5].Value = "TripLocationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetTripLocationDBCollectionFromDataSet(ds, out count);
        }

        public TripLocationEntity GetTripLocationDBFromDataReader(IDataReader reader)
        {
            return new TripLocationEntity(Guid.Parse(reader["TripLocationId"].ToString()),
                                    reader["TripLocationTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<TripLocationEntity> GetTripLocationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<TripLocationEntity> lst = new List<TripLocationEntity>();
                while (reader.Read())
                    lst.Add(GetTripLocationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<TripLocationEntity> GetTripLocationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetTripLocationDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

}
