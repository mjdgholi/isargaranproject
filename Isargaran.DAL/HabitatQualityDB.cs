﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<کیفیت محل اقامت>
    /// </summary>



    public class HabitatQualityDB
    {
        #region Methods :

        public void AddHabitatQualityDB(HabitatQualityEntity HabitatQualityEntityParam, out Guid HabitatQualityId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@HabitatQualityId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@HabitatQualityTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(HabitatQualityEntityParam.HabitatQualityTitle.Trim());            
            parameters[2].Value = HabitatQualityEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatQualityAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            HabitatQualityId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateHabitatQualityDB(HabitatQualityEntity HabitatQualityEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@HabitatQualityId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@HabitatQualityTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = HabitatQualityEntityParam.HabitatQualityId;
            parameters[1].Value = FarsiToArabic.ToArabic(HabitatQualityEntityParam.HabitatQualityTitle.Trim());            
            parameters[2].Value = HabitatQualityEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_HabitatQualityUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteHabitatQualityDB(HabitatQualityEntity HabitatQualityEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@HabitatQualityId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = HabitatQualityEntityParam.HabitatQualityId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatQualityDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public HabitatQualityEntity GetSingleHabitatQualityDB(HabitatQualityEntity HabitatQualityEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@HabitatQualityId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = HabitatQualityEntityParam.HabitatQualityId;

                reader = _intranetDB.RunProcedureReader("Isar.p_HabitatQualityGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetHabitatQualityDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatQualityEntity> GetAllHabitatQualityDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatQualityDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatQualityGetAll", new IDataParameter[] { }));
        }
        public List<HabitatQualityEntity> GetAllHabitatQualityIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatQualityDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatQualityIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<HabitatQualityEntity> GetPageHabitatQualityDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_HabitatQuality";
            parameters[5].Value = "HabitatQualityId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetHabitatQualityDBCollectionFromDataSet(ds, out count);
        }

        public HabitatQualityEntity GetHabitatQualityDBFromDataReader(IDataReader reader)
        {
            return new HabitatQualityEntity(Guid.Parse(reader["HabitatQualityId"].ToString()),
                                    reader["HabitatQualityTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<HabitatQualityEntity> GetHabitatQualityDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<HabitatQualityEntity> lst = new List<HabitatQualityEntity>();
                while (reader.Read())
                    lst.Add(GetHabitatQualityDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatQualityEntity> GetHabitatQualityDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetHabitatQualityDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
