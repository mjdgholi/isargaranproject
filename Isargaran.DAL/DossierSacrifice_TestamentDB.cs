﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/31>
    /// Description: <اطلاعات وصیت ایثارگر>
    /// </summary>

    public class DossierSacrifice_TestamentDB
    {
        #region Methods :

        public void AddDossierSacrifice_TestamentDB(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam, out Guid DossierSacrifice_TestamentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_TestamentId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@IsHasTestament", SqlDbType.Bit) ,
											  new SqlParameter("@TestamentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TestamentContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,											  											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (DossierSacrifice_TestamentEntityParam.IsHasTestament==null?System.DBNull.Value:(object)DossierSacrifice_TestamentEntityParam.IsHasTestament);
            parameters[2].Value = (DossierSacrifice_TestamentEntityParam.TestamentDate==""?System.DBNull.Value:(object)DossierSacrifice_TestamentEntityParam.TestamentDate);
            parameters[3].Value = (DossierSacrifice_TestamentEntityParam.TestamentContent == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_TestamentEntityParam.TestamentContent.Trim()));
            parameters[4].Value = DossierSacrifice_TestamentEntityParam.DossierSacrificeId;                        

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TestamentAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_TestamentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_TestamentDB(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_TestamentId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@IsHasTestament", SqlDbType.Bit) ,
											  new SqlParameter("@TestamentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@TestamentContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,											  											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_TestamentEntityParam.DossierSacrifice_TestamentId;
            parameters[1].Value = (DossierSacrifice_TestamentEntityParam.IsHasTestament == null ? System.DBNull.Value : (object)DossierSacrifice_TestamentEntityParam.IsHasTestament);
            parameters[2].Value = (DossierSacrifice_TestamentEntityParam.TestamentDate == "" ? System.DBNull.Value : (object)DossierSacrifice_TestamentEntityParam.TestamentDate);
            parameters[3].Value = (DossierSacrifice_TestamentEntityParam.TestamentContent==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_TestamentEntityParam.TestamentContent.Trim()));
            parameters[4].Value = DossierSacrifice_TestamentEntityParam.DossierSacrificeId;            


            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TestamentUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_TestamentDB(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_TestamentId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_TestamentEntityParam.DossierSacrifice_TestamentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TestamentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_TestamentEntity GetSingleDossierSacrifice_TestamentDB(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_TestamentId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_TestamentEntityParam.DossierSacrifice_TestamentId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TestamentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_TestamentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TestamentEntity> GetAllDossierSacrifice_TestamentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_TestamentDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_TestamentGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_TestamentEntity> GetPageDossierSacrifice_TestamentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Testament";
            parameters[5].Value = "DossierSacrifice_TestamentId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_TestamentDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_TestamentEntity GetDossierSacrifice_TestamentDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_TestamentEntity(Guid.Parse(reader["DossierSacrifice_TestamentId"].ToString()),
                                     Convert.IsDBNull(reader["IsHasTestament"]) ? null : (bool?)reader["IsHasTestament"],                                    
                                    reader["TestamentDate"].ToString(),
                                    reader["TestamentContent"].ToString(),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_TestamentEntity> GetDossierSacrifice_TestamentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_TestamentEntity> lst = new List<DossierSacrifice_TestamentEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_TestamentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TestamentEntity> GetDossierSacrifice_TestamentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_TestamentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_TestamentEntity> GetDossierSacrifice_TestamentDBCollectionByDossierSacrificeDB(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_TestamentEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_TestamentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TestamentGetByDossierSacrifice", new[] { parameter }));
        }


        #endregion



    }
    
}
