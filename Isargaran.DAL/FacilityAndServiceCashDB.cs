﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/21>
    /// Description: <خدمات و تسهیلات نقدی>
    /// </summary>

    public class FacilityAndServiceCashDB
    {
        #region Methods :

        public void AddFacilityAndServiceCashDB(FacilityAndServiceCashEntity FacilityAndServiceCashEntityParam, out Guid FacilityAndServiceCashId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@FacilityAndServiceCashTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(FacilityAndServiceCashEntityParam.FacilityAndServiceCashTitle.Trim());            
            parameters[2].Value = FacilityAndServiceCashEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_FacilityAndServiceCashAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            FacilityAndServiceCashId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateFacilityAndServiceCashDB(FacilityAndServiceCashEntity FacilityAndServiceCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@FacilityAndServiceCashTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = FacilityAndServiceCashEntityParam.FacilityAndServiceCashId;
           parameters[1].Value = FarsiToArabic.ToArabic(FacilityAndServiceCashEntityParam.FacilityAndServiceCashTitle.Trim());            
            parameters[2].Value = FacilityAndServiceCashEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_FacilityAndServiceCashUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteFacilityAndServiceCashDB(FacilityAndServiceCashEntity FacilityAndServiceCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@FacilityAndServiceCashId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = FacilityAndServiceCashEntityParam.FacilityAndServiceCashId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_FacilityAndServiceCashDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public FacilityAndServiceCashEntity GetSingleFacilityAndServiceCashDB(FacilityAndServiceCashEntity FacilityAndServiceCashEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@FacilityAndServiceCashId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = FacilityAndServiceCashEntityParam.FacilityAndServiceCashId;

                reader = _intranetDB.RunProcedureReader("Isar.p_FacilityAndServiceCashGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetFacilityAndServiceCashDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<FacilityAndServiceCashEntity> GetAllFacilityAndServiceCashDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetFacilityAndServiceCashDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_FacilityAndServiceCashGetAll", new IDataParameter[] { }));
        }
        public List<FacilityAndServiceCashEntity> GetAllFacilityAndServiceCashIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetFacilityAndServiceCashDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_FacilityAndServiceCashIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<FacilityAndServiceCashEntity> GetPageFacilityAndServiceCashDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_FacilityAndServiceCash";
            parameters[5].Value = "FacilityAndServiceCashId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetFacilityAndServiceCashDBCollectionFromDataSet(ds, out count);
        }

        public FacilityAndServiceCashEntity GetFacilityAndServiceCashDBFromDataReader(IDataReader reader)
        {
            return new FacilityAndServiceCashEntity(Guid.Parse(reader["FacilityAndServiceCashId"].ToString()),
                                    reader["FacilityAndServiceCashTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<FacilityAndServiceCashEntity> GetFacilityAndServiceCashDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<FacilityAndServiceCashEntity> lst = new List<FacilityAndServiceCashEntity>();
                while (reader.Read())
                    lst.Add(GetFacilityAndServiceCashDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<FacilityAndServiceCashEntity> GetFacilityAndServiceCashDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetFacilityAndServiceCashDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    
}
