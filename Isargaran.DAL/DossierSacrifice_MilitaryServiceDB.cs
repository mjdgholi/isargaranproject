﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/26>
    /// Description: <پرونده خدمات سربازی ایثارگر>
    /// </summary>

    public class DossierSacrifice_MilitaryServiceDB
    {
        #region Methods :

        public void AddDossierSacrifice_MilitaryServiceDB(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam, out Guid DossierSacrifice_MilitaryServiceId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_MilitaryServiceId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasMilitaryServiceCard", SqlDbType.Bit) ,
											  new SqlParameter("@MilitaryServiceLocation", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@DistrictHandler", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ClassifiedNo", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CardIssueDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@CardNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@MilitaryStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VillageTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SectionTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_MilitaryServiceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_MilitaryServiceEntityParam.HasMilitaryServiceCard;
            parameters[3].Value = (DossierSacrifice_MilitaryServiceEntityParam.MilitaryServiceLocation == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.MilitaryServiceLocation.Trim()));
            parameters[4].Value = (DossierSacrifice_MilitaryServiceEntityParam.DistrictHandler == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.DistrictHandler.Trim()));
            parameters[5].Value = (DossierSacrifice_MilitaryServiceEntityParam.ClassifiedNo==""?System.DBNull.Value:(object)DossierSacrifice_MilitaryServiceEntityParam.ClassifiedNo);
            parameters[6].Value = (DossierSacrifice_MilitaryServiceEntityParam.CardIssueDate==""?System.DBNull.Value:(object)DossierSacrifice_MilitaryServiceEntityParam.CardIssueDate);
            parameters[7].Value = (DossierSacrifice_MilitaryServiceEntityParam.CardNo==""?System.DBNull.Value:(object)DossierSacrifice_MilitaryServiceEntityParam.CardNo);
            parameters[8].Value = (DossierSacrifice_MilitaryServiceEntityParam.MilitaryStatusId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_MilitaryServiceEntityParam.MilitaryStatusId);
            parameters[9].Value = (DossierSacrifice_MilitaryServiceEntityParam.CityId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_MilitaryServiceEntityParam.CityId);
            parameters[10].Value = (DossierSacrifice_MilitaryServiceEntityParam.VillageTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.VillageTitle.Trim()));
            parameters[11].Value = (DossierSacrifice_MilitaryServiceEntityParam.SectionTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.SectionTitle.Trim()));
            parameters[12].Value = (DossierSacrifice_MilitaryServiceEntityParam.ZipCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.ZipCode.Trim()));
            parameters[13].Value = (DossierSacrifice_MilitaryServiceEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.Address.Trim()));
            parameters[14].Value = (DossierSacrifice_MilitaryServiceEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.Email.Trim()));
            parameters[15].Value = (DossierSacrifice_MilitaryServiceEntityParam.WebAddress == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.WebAddress.Trim()));
            parameters[16].Value = (DossierSacrifice_MilitaryServiceEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.TelNo.Trim()));
            parameters[17].Value = (DossierSacrifice_MilitaryServiceEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.FaxNo.Trim()));
            parameters[18].Value = DossierSacrifice_MilitaryServiceEntityParam.StartDate;
            parameters[19].Value = (DossierSacrifice_MilitaryServiceEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_MilitaryServiceEntityParam.EndDate);            

            parameters[20].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MilitaryServiceAdd", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_MilitaryServiceId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_MilitaryServiceDB(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_MilitaryServiceId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HasMilitaryServiceCard", SqlDbType.Bit) ,
											  new SqlParameter("@MilitaryServiceLocation", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@DistrictHandler", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ClassifiedNo", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CardIssueDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@CardNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@MilitaryStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VillageTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@SectionTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
						                    new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_MilitaryServiceEntityParam.DossierSacrifice_MilitaryServiceId;
            parameters[1].Value = DossierSacrifice_MilitaryServiceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_MilitaryServiceEntityParam.HasMilitaryServiceCard;
            parameters[3].Value = (DossierSacrifice_MilitaryServiceEntityParam.MilitaryServiceLocation == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.MilitaryServiceLocation.Trim()));
            parameters[4].Value = (DossierSacrifice_MilitaryServiceEntityParam.DistrictHandler == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.DistrictHandler.Trim()));
            parameters[5].Value = (DossierSacrifice_MilitaryServiceEntityParam.ClassifiedNo == "" ? System.DBNull.Value : (object)DossierSacrifice_MilitaryServiceEntityParam.ClassifiedNo);
            parameters[6].Value = (DossierSacrifice_MilitaryServiceEntityParam.CardIssueDate == "" ? System.DBNull.Value : (object)DossierSacrifice_MilitaryServiceEntityParam.CardIssueDate);
            parameters[7].Value = (DossierSacrifice_MilitaryServiceEntityParam.CardNo == "" ? System.DBNull.Value : (object)DossierSacrifice_MilitaryServiceEntityParam.CardNo);
            parameters[8].Value = (DossierSacrifice_MilitaryServiceEntityParam.MilitaryStatusId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_MilitaryServiceEntityParam.MilitaryStatusId);
            parameters[9].Value = (DossierSacrifice_MilitaryServiceEntityParam.CityId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_MilitaryServiceEntityParam.CityId);
            parameters[10].Value = (DossierSacrifice_MilitaryServiceEntityParam.VillageTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.VillageTitle.Trim()));
            parameters[11].Value = (DossierSacrifice_MilitaryServiceEntityParam.SectionTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.SectionTitle.Trim()));
            parameters[12].Value = (DossierSacrifice_MilitaryServiceEntityParam.ZipCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.ZipCode.Trim()));
            parameters[13].Value = (DossierSacrifice_MilitaryServiceEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.Address.Trim()));
            parameters[14].Value = (DossierSacrifice_MilitaryServiceEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.Email.Trim()));
            parameters[15].Value = (DossierSacrifice_MilitaryServiceEntityParam.WebAddress == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.WebAddress.Trim()));
            parameters[16].Value = (DossierSacrifice_MilitaryServiceEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.TelNo.Trim()));
            parameters[17].Value = (DossierSacrifice_MilitaryServiceEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_MilitaryServiceEntityParam.FaxNo.Trim()));
            parameters[18].Value = DossierSacrifice_MilitaryServiceEntityParam.StartDate;
            parameters[19].Value = (DossierSacrifice_MilitaryServiceEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_MilitaryServiceEntityParam.EndDate);                

            parameters[20].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MilitaryServiceUpdate", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_MilitaryServiceDB(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_MilitaryServiceId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_MilitaryServiceEntityParam.DossierSacrifice_MilitaryServiceId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MilitaryServiceDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_MilitaryServiceEntity GetSingleDossierSacrifice_MilitaryServiceDB(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_MilitaryServiceId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_MilitaryServiceEntityParam.DossierSacrifice_MilitaryServiceId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MilitaryServiceGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_MilitaryServiceDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetAllDossierSacrifice_MilitaryServiceDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_MilitaryServiceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_MilitaryServiceGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetPageDossierSacrifice_MilitaryServiceDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_MilitaryService";
            parameters[5].Value = "DossierSacrifice_MilitaryServiceId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_MilitaryServiceDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_MilitaryServiceEntity GetDossierSacrifice_MilitaryServiceDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_MilitaryServiceEntity(Guid.Parse(reader["DossierSacrifice_MilitaryServiceId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    bool.Parse(reader["HasMilitaryServiceCard"].ToString()),
                                    reader["MilitaryServiceLocation"].ToString(),
                                    reader["DistrictHandler"].ToString(),
                                    reader["ClassifiedNo"].ToString(),
                                    reader["CardIssueDate"].ToString(),
                                    reader["CardNo"].ToString(),
                                    Convert.IsDBNull(reader["MilitaryStatusId"]) ? null : (Guid?)reader["MilitaryStatusId"],
                                    Convert.IsDBNull(reader["CityId"]) ? null : (Guid?)reader["CityId"],                                       
                                    reader["VillageTitle"].ToString(),
                                    reader["SectionTitle"].ToString(),
                                    reader["ZipCode"].ToString(),
                                    reader["Address"].ToString(),
                                    reader["Email"].ToString(),
                                    reader["WebAddress"].ToString(),
                                    reader["TelNo"].ToString(),
                                    reader["FaxNo"].ToString(),
                                    reader["StartDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    Guid.Parse(reader["ProvinceId"].ToString()));
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetDossierSacrifice_MilitaryServiceDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_MilitaryServiceEntity> lst = new List<DossierSacrifice_MilitaryServiceEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_MilitaryServiceDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetDossierSacrifice_MilitaryServiceDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_MilitaryServiceDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetDossierSacrifice_MilitaryServiceDBCollectionByDossierSacrificeDB(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_MilitaryServiceEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_MilitaryServiceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MilitaryServiceGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetDossierSacrifice_MilitaryServiceDBCollectionByMilitaryStatusDB(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MilitaryStatusId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_MilitaryServiceEntityParam.MilitaryStatusId;
            return GetDossierSacrifice_MilitaryServiceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MilitaryServiceGetByMilitaryStatus", new[] { parameter }));
        }


        #endregion



    }

}
