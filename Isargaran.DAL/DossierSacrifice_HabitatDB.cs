﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/18>
    /// Description: <پرونده مشخصات محل اقامت ایثارگری>
    /// </summary>

    public class DossierSacrifice_HabitatDB
    {
        #region Methods :

        public void AddDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam, out Guid DossierSacrifice_HabitatId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_HabitatId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatOwnershipStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatEStateTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatLocationTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatQualityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsBasicRepairNeed", SqlDbType.Bit) ,
											  new SqlParameter("@AmountOfRent", SqlDbType.Decimal) ,
											  new SqlParameter("@HbitatArea", SqlDbType.Decimal) ,
											  new SqlParameter("@HabitatDeposit", SqlDbType.Decimal) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_HabitatEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_HabitatEntityParam.HabitatOwnershipStatusId;
            parameters[3].Value = (DossierSacrifice_HabitatEntityParam.HabitatEStateTypeId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_HabitatEntityParam.HabitatEStateTypeId);
            parameters[4].Value = (DossierSacrifice_HabitatEntityParam.HabitatLocationTypeId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_HabitatEntityParam.HabitatLocationTypeId);
            parameters[5].Value = (DossierSacrifice_HabitatEntityParam.HabitatQualityId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_HabitatEntityParam.HabitatQualityId);
            parameters[6].Value = (DossierSacrifice_HabitatEntityParam.IsBasicRepairNeed==null?System.DBNull.Value:(object)DossierSacrifice_HabitatEntityParam.IsBasicRepairNeed);
            parameters[7].Value = (DossierSacrifice_HabitatEntityParam.AmountOfRent.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.AmountOfRent);
            parameters[8].Value = (DossierSacrifice_HabitatEntityParam.HbitatArea.ToString()==""?System.DBNull.Value:(object)DossierSacrifice_HabitatEntityParam.HbitatArea);
            parameters[9].Value = (DossierSacrifice_HabitatEntityParam.HabitatDeposit.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.HabitatDeposit);
            parameters[10].Value = (DossierSacrifice_HabitatEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.StartDate);
            parameters[11].Value = (DossierSacrifice_HabitatEntityParam.EndDate==""?System.DBNull.Value:(object)DossierSacrifice_HabitatEntityParam.EndDate);

            parameters[12].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_HabitatAdd", parameters);
            var messageError = parameters[12].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_HabitatId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_HabitatId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatOwnershipStatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatEStateTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatLocationTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@HabitatQualityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsBasicRepairNeed", SqlDbType.Bit) ,
											  new SqlParameter("@AmountOfRent", SqlDbType.Decimal) ,
											  new SqlParameter("@HbitatArea", SqlDbType.Decimal) ,
											  new SqlParameter("@HabitatDeposit", SqlDbType.Decimal) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_HabitatEntityParam.DossierSacrifice_HabitatId;
            parameters[1].Value = DossierSacrifice_HabitatEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_HabitatEntityParam.HabitatOwnershipStatusId;
            parameters[3].Value = (DossierSacrifice_HabitatEntityParam.HabitatEStateTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.HabitatEStateTypeId);
            parameters[4].Value = (DossierSacrifice_HabitatEntityParam.HabitatLocationTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.HabitatLocationTypeId);
            parameters[5].Value = (DossierSacrifice_HabitatEntityParam.HabitatQualityId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.HabitatQualityId);
            parameters[6].Value = (DossierSacrifice_HabitatEntityParam.IsBasicRepairNeed == null ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.IsBasicRepairNeed);
            parameters[7].Value = (DossierSacrifice_HabitatEntityParam.AmountOfRent.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.AmountOfRent);
            parameters[8].Value = (DossierSacrifice_HabitatEntityParam.HbitatArea.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.HbitatArea);
            parameters[9].Value = (DossierSacrifice_HabitatEntityParam.HabitatDeposit.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.HabitatDeposit);
            parameters[10].Value = (DossierSacrifice_HabitatEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.StartDate);
            parameters[11].Value = (DossierSacrifice_HabitatEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_HabitatEntityParam.EndDate);
            parameters[12].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_HabitatUpdate", parameters);
            var messageError = parameters[12].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_HabitatId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_HabitatEntityParam.DossierSacrifice_HabitatId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_HabitatDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_HabitatEntity GetSingleDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_HabitatId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_HabitatEntityParam.DossierSacrifice_HabitatId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_HabitatGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_HabitatDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_HabitatEntity> GetAllDossierSacrifice_HabitatDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_HabitatDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_HabitatGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_HabitatEntity> GetPageDossierSacrifice_HabitatDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Habitat";
            parameters[5].Value = "DossierSacrifice_HabitatId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_HabitatDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_HabitatEntity GetDossierSacrifice_HabitatDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_HabitatEntity(Guid.Parse(reader["DossierSacrifice_HabitatId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["HabitatOwnershipStatusId"].ToString()),
                                    Convert.IsDBNull(reader["HabitatEStateTypeId"]) ? null : (Guid?)reader["HabitatEStateTypeId"],
                                    Convert.IsDBNull(reader["HabitatLocationTypeId"]) ? null : (Guid?)reader["HabitatLocationTypeId"],
                                    Convert.IsDBNull(reader["HabitatQualityId"]) ? null : (Guid?)reader["HabitatQualityId"],
                                     Convert.IsDBNull(reader["IsBasicRepairNeed"]) ? null : (bool?)reader["IsBasicRepairNeed"],                                    
                                    Convert.IsDBNull(reader["AmountOfRent"]) ? null : (decimal?)reader["AmountOfRent"],
                                    Convert.IsDBNull(reader["HbitatArea"]) ? null : (decimal?)reader["HbitatArea"],
                                    Convert.IsDBNull(reader["HabitatDeposit"]) ? null : (decimal?)reader["HabitatDeposit"],                                          
                                    reader["StartDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_HabitatEntity> lst = new List<DossierSacrifice_HabitatEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_HabitatDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_HabitatDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatDBCollectionByDossierSacrificeDB(int DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrificeId;
            return GetDossierSacrifice_HabitatDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_HabitatGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatDBCollectionByHabitatEstateTypeDB(int HabitatEStateTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@HabitatEStateTypeId", SqlDbType.Int);
            parameter.Value = HabitatEStateTypeId;
            return GetDossierSacrifice_HabitatDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_HabitatGetByHabitatEstateType", new[] { parameter }));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatDBCollectionByHabitatLocationTypeDB(int HabitatLocationTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@HabitatLocationTypeId", SqlDbType.Int);
            parameter.Value = HabitatLocationTypeId;
            return GetDossierSacrifice_HabitatDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_HabitatGetByHabitatLocationType", new[] { parameter }));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatDBCollectionByHabitatOwnershipStatusDB(int HabitatOwnershipStatusId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@HabitatOwnershipStatusId", SqlDbType.Int);
            parameter.Value = HabitatOwnershipStatusId;
            return GetDossierSacrifice_HabitatDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_HabitatGetByHabitatOwnershipStatus", new[] { parameter }));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatDBCollectionByHabitatQualityDB(int HabitatQualityId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@HabitatQualityId", SqlDbType.Int);
            parameter.Value = HabitatQualityId;
            return GetDossierSacrifice_HabitatDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_HabitatGetByHabitatQuality", new[] { parameter }));
        }


        #endregion



    }


}
