﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <مراکزتحصیلی>
    /// </summary>



    public class EducationCenterDB
    {



        #region Methods :

        public void AddEducationCenterDB(EducationCenterEntity EducationCenterEntityParam, out Guid EducationCenterId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EducationCenterTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationCenterPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EducationCenterEnglishTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = EducationCenterEntityParam.EducationCenterTypeId;
            parameters[2].Value = FarsiToArabic.ToArabic(EducationCenterEntityParam.EducationCenterPersianTitle.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(EducationCenterEntityParam.EducationCenterEnglishTitle.Trim());
            parameters[4].Value = (EducationCenterEntityParam.Email.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.Email.Trim()));
            parameters[5].Value = (EducationCenterEntityParam.TelNo.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.TelNo.Trim()));
            parameters[6].Value = (EducationCenterEntityParam.FaxNo.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.FaxNo.Trim()));
            parameters[7].Value = (EducationCenterEntityParam.ZipCode.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.ZipCode.Trim()));
            parameters[8].Value = (EducationCenterEntityParam.Address.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.Address.Trim()));            
            parameters[9].Value = EducationCenterEntityParam.IsActive;

            parameters[10].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCenterAdd", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EducationCenterId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationCenterDB(EducationCenterEntity EducationCenterEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EducationCenterTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationCenterPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EducationCenterEnglishTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EducationCenterEntityParam.EducationCenterId;
            parameters[1].Value = EducationCenterEntityParam.EducationCenterTypeId;
            parameters[2].Value = FarsiToArabic.ToArabic(EducationCenterEntityParam.EducationCenterPersianTitle.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(EducationCenterEntityParam.EducationCenterEnglishTitle.Trim());
            parameters[4].Value = (EducationCenterEntityParam.Email.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.Email.Trim()));
            parameters[5].Value = (EducationCenterEntityParam.TelNo.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.TelNo.Trim()));
            parameters[6].Value = (EducationCenterEntityParam.FaxNo.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.FaxNo.Trim()));
            parameters[7].Value = (EducationCenterEntityParam.ZipCode.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.ZipCode.Trim()));
            parameters[8].Value = (EducationCenterEntityParam.Address.Trim()==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(EducationCenterEntityParam.Address.Trim()));            
            parameters[9].Value = EducationCenterEntityParam.IsActive;

            parameters[10].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationCenterUpdate", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationCenterDB(EducationCenterEntity EducationCenterEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EducationCenterEntityParam.EducationCenterId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCenterDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationCenterEntity GetSingleEducationCenterDB(EducationCenterEntity EducationCenterEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EducationCenterEntityParam.EducationCenterId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationCenterGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationCenterDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCenterEntity> GetAllEducationCenterDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationCenterDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationCenterGetAll", new IDataParameter[] { }));
        }
        public List<EducationCenterEntity> GetAllEducationCenterIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationCenterDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationCenterIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<EducationCenterEntity> GetPageEducationCenterDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EducationCenter";
            parameters[5].Value = "EducationCenterId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationCenterDBCollectionFromDataSet(ds, out count);
        }

        public EducationCenterEntity GetEducationCenterDBFromDataReader(IDataReader reader)
        {
            return new EducationCenterEntity(Guid.Parse(reader["EducationCenterId"].ToString()),
                                    Guid.Parse(reader["EducationCenterTypeId"].ToString()),
                                    reader["EducationCenterPersianTitle"].ToString(),
                                    reader["EducationCenterEnglishTitle"].ToString(),
                                    reader["Email"].ToString(),
                                    reader["TelNo"].ToString(),
                                    reader["FaxNo"].ToString(),
                                    reader["ZipCode"].ToString(),
                                    reader["Address"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationCenterEntity> GetEducationCenterDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationCenterEntity> lst = new List<EducationCenterEntity>();
                while (reader.Read())
                    lst.Add(GetEducationCenterDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCenterEntity> GetEducationCenterDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationCenterDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<EducationCenterEntity> GetEducationCenterDBCollectionByEducationCenterTypeDB(EducationCenterEntity EducationCenterEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationCenterTypeId", SqlDbType.Int);
            parameter.Value = EducationCenterEntityParam.EducationCenterTypeId;
            return GetEducationCenterDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_EducationCenterGetByEducationCenterType", new[] { parameter }));
        }


        #endregion



    }

}
