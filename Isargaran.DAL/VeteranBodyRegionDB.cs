﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/27>
    // Description:	<فرم ناحیه جانبازی بدن>
    /// </summary>

    //-----------------------------------------------------

    #region Class "VeteranBodyRegionDB"

    public class VeteranBodyRegionDB
    {



        #region Methods :

        public void AddVeteranBodyRegionDB(VeteranBodyRegionEntity VeteranBodyRegionEntityParam, out Guid VeteranBodyRegionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@VeteranBodyRegionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@VeteranBodyRegionTitle", SqlDbType.NVarChar, 200),                                          
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(VeteranBodyRegionEntityParam.VeteranBodyRegionTitle.Trim());
            parameters[2].Value = VeteranBodyRegionEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_VeteranBodyRegionAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            VeteranBodyRegionId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateVeteranBodyRegionDB(VeteranBodyRegionEntity VeteranBodyRegionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@VeteranBodyRegionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@VeteranBodyRegionTitle", SqlDbType.NVarChar, 200),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = VeteranBodyRegionEntityParam.VeteranBodyRegionId;
            parameters[1].Value = FarsiToArabic.ToArabic(VeteranBodyRegionEntityParam.VeteranBodyRegionTitle.Trim());
            parameters[2].Value = VeteranBodyRegionEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;          

            _intranetDB.RunProcedure("Isar.p_VeteranBodyRegionUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteVeteranBodyRegionDB(VeteranBodyRegionEntity VeteranBodyRegionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@VeteranBodyRegionId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = VeteranBodyRegionEntityParam.VeteranBodyRegionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_VeteranBodyRegionDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public VeteranBodyRegionEntity GetSingleVeteranBodyRegionDB(VeteranBodyRegionEntity VeteranBodyRegionEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@VeteranBodyRegionId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = VeteranBodyRegionEntityParam.VeteranBodyRegionId;

                reader = _intranetDB.RunProcedureReader("Isar.p_VeteranBodyRegionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetVeteranBodyRegionDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<VeteranBodyRegionEntity> GetAllVeteranBodyRegionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetVeteranBodyRegionDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_VeteranBodyRegionGetAll", new IDataParameter[] {}));
        }

        public List<VeteranBodyRegionEntity> GetAllIsActiveVeteranBodyRegionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetVeteranBodyRegionDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_VeteranBodyRegionGetAllIsActive", new IDataParameter[] { }));
        }

        public List<VeteranBodyRegionEntity> GetPageVeteranBodyRegionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_VeteranBodyRegion";
            parameters[5].Value = "VeteranBodyRegionId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetVeteranBodyRegionDBCollectionFromDataSet(ds, out count);
        }

        public VeteranBodyRegionEntity GetVeteranBodyRegionDBFromDataReader(IDataReader reader)
        {
            return new VeteranBodyRegionEntity(new Guid(reader["VeteranBodyRegionId"].ToString()),
                                               reader["VeteranBodyRegionTitle"].ToString(),
                                               reader["CreationDate"].ToString(),
                                               reader["ModificationDate"].ToString(),
                                               bool.Parse(reader["IsActive"].ToString()));
        }

        public List<VeteranBodyRegionEntity> GetVeteranBodyRegionDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<VeteranBodyRegionEntity> lst = new List<VeteranBodyRegionEntity>();
                while (reader.Read())
                    lst.Add(GetVeteranBodyRegionDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<VeteranBodyRegionEntity> GetVeteranBodyRegionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetVeteranBodyRegionDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}