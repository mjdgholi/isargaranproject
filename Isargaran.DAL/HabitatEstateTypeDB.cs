﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<نوع سازه محل اقامت >
    /// </summary>


    public class HabitatEstateTypeDB
    {

        #region Methods :

        public void AddHabitatEstateTypeDB(HabitatEstateTypeEntity HabitatEstateTypeEntityParam, out Guid HabitatEstateTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@HabitatEstateTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@HabitatEstateTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value =FarsiToArabic.ToArabic(HabitatEstateTypeEntityParam.HabitatEstateTypeTitle.Trim());
            parameters[2].Value = HabitatEstateTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatEstateTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            HabitatEstateTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateHabitatEstateTypeDB(HabitatEstateTypeEntity HabitatEstateTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@HabitatEstateTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@HabitatEstateTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = HabitatEstateTypeEntityParam.HabitatEstateTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(HabitatEstateTypeEntityParam.HabitatEstateTypeTitle.Trim());
            parameters[2].Value = HabitatEstateTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_HabitatEstateTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteHabitatEstateTypeDB(HabitatEstateTypeEntity HabitatEstateTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@HabitatEstateTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = HabitatEstateTypeEntityParam.HabitatEstateTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatEstateTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public HabitatEstateTypeEntity GetSingleHabitatEstateTypeDB(HabitatEstateTypeEntity HabitatEstateTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@HabitatEstateTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = HabitatEstateTypeEntityParam.HabitatEstateTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_HabitatEstateTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetHabitatEstateTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatEstateTypeEntity> GetAllHabitatEstateTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatEstateTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatEstateTypeGetAll", new IDataParameter[] { }));
        }
        public List<HabitatEstateTypeEntity> GetAllHabitatEstateTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatEstateTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatEstateTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<HabitatEstateTypeEntity> GetPageHabitatEstateTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_HabitatEstateType";
            parameters[5].Value = "HabitatEstateTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetHabitatEstateTypeDBCollectionFromDataSet(ds, out count);
        }

        public HabitatEstateTypeEntity GetHabitatEstateTypeDBFromDataReader(IDataReader reader)
        {
            return new HabitatEstateTypeEntity(Guid.Parse(reader["HabitatEstateTypeId"].ToString()),
                                    reader["HabitatEstateTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<HabitatEstateTypeEntity> GetHabitatEstateTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<HabitatEstateTypeEntity> lst = new List<HabitatEstateTypeEntity>();
                while (reader.Read())
                    lst.Add(GetHabitatEstateTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatEstateTypeEntity> GetHabitatEstateTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetHabitatEstateTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }


}
