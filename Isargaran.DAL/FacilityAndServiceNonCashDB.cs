﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/22>
    /// Description: <خدمات و تسهیلات  غیرنقدی>
    /// </summary>


    public class FacilityAndServiceNonCashDB
    {

        #region Methods :

        public void AddFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntity FacilityAndServiceNonCashEntityParam, out Guid FacilityAndServiceNonCashId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@FacilityAndServiceNonCashTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashTitle.Trim());
            parameters[2].Value = FacilityAndServiceNonCashEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_FacilityAndServiceNonCashAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            FacilityAndServiceNonCashId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntity FacilityAndServiceNonCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@FacilityAndServiceNonCashTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashId;
            parameters[1].Value = FarsiToArabic.ToArabic(FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashTitle.Trim());
            parameters[2].Value = FacilityAndServiceNonCashEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_FacilityAndServiceNonCashUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntity FacilityAndServiceNonCashEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_FacilityAndServiceNonCashDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public FacilityAndServiceNonCashEntity GetSingleFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntity FacilityAndServiceNonCashEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@FacilityAndServiceNonCashId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = FacilityAndServiceNonCashEntityParam.FacilityAndServiceNonCashId;

                reader = _intranetDB.RunProcedureReader("Isar.p_FacilityAndServiceNonCashGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetFacilityAndServiceNonCashDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<FacilityAndServiceNonCashEntity> GetAllFacilityAndServiceNonCashDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetFacilityAndServiceNonCashDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_FacilityAndServiceNonCashGetAll", new IDataParameter[] { }));
        }
        public List<FacilityAndServiceNonCashEntity> GetAllFacilityAndServiceNonCashIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetFacilityAndServiceNonCashDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_FacilityAndServiceNonCashIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<FacilityAndServiceNonCashEntity> GetPageFacilityAndServiceNonCashDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_FacilityAndServiceNonCash";
            parameters[5].Value = "FacilityAndServiceNonCashId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetFacilityAndServiceNonCashDBCollectionFromDataSet(ds, out count);
        }

        public FacilityAndServiceNonCashEntity GetFacilityAndServiceNonCashDBFromDataReader(IDataReader reader)
        {
            return new FacilityAndServiceNonCashEntity(Guid.Parse(reader["FacilityAndServiceNonCashId"].ToString()),
                                    reader["FacilityAndServiceNonCashTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<FacilityAndServiceNonCashEntity> GetFacilityAndServiceNonCashDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<FacilityAndServiceNonCashEntity> lst = new List<FacilityAndServiceNonCashEntity>();
                while (reader.Read())
                    lst.Add(GetFacilityAndServiceNonCashDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<FacilityAndServiceNonCashEntity> GetFacilityAndServiceNonCashDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetFacilityAndServiceNonCashDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
