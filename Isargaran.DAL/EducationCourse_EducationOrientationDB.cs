﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/12>
    /// Description: <رشته تحصیلی -گرایش تحصیلی >
    /// </summary>



    public class EducationCourse_EducationOrientationDB
    {



        #region Methods :

        public void AddEducationCourse_EducationOrientationDB(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam, out Guid EducationCourse_EducationOrientationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EducationCourse_EducationOrientationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationOreintionId", SqlDbType.UniqueIdentifier) ,
										
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = EducationCourse_EducationOrientationEntityParam.EducationCourseId;
            parameters[2].Value = EducationCourse_EducationOrientationEntityParam.EducationOreintionId;
            parameters[3].Value = EducationCourse_EducationOrientationEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCourse_EducationOrientationAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EducationCourse_EducationOrientationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationCourse_EducationOrientationDB(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EducationCourse_EducationOrientationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EducationOreintionId", SqlDbType.UniqueIdentifier) ,
								new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EducationCourse_EducationOrientationEntityParam.EducationCourse_EducationOrientationId;
            parameters[1].Value = EducationCourse_EducationOrientationEntityParam.EducationCourseId;
            parameters[2].Value = EducationCourse_EducationOrientationEntityParam.EducationOreintionId;
            parameters[3].Value = EducationCourse_EducationOrientationEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationCourse_EducationOrientationUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationCourse_EducationOrientationDB(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EducationCourse_EducationOrientationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EducationCourse_EducationOrientationEntityParam.EducationCourse_EducationOrientationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationCourse_EducationOrientationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationCourse_EducationOrientationEntity GetSingleEducationCourse_EducationOrientationDB(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EducationCourse_EducationOrientationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EducationCourse_EducationOrientationEntityParam.EducationCourse_EducationOrientationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationCourse_EducationOrientationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationCourse_EducationOrientationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCourse_EducationOrientationEntity> GetAllEducationCourse_EducationOrientationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationCourse_EducationOrientationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_EducationCourse_EducationOrientationGetAll", new IDataParameter[] { }));
        }

        public List<EducationCourse_EducationOrientationEntity> GetPageEducationCourse_EducationOrientationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EducationCourse_EducationOrientation";
            parameters[5].Value = "EducationCourse_EducationOrientationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationCourse_EducationOrientationDBCollectionFromDataSet(ds, out count);
        }

        public EducationCourse_EducationOrientationEntity GetEducationCourse_EducationOrientationDBFromDataReader(IDataReader reader)
        {
            return new EducationCourse_EducationOrientationEntity(Guid.Parse(reader["EducationCourse_EducationOrientationId"].ToString()),
                                    Guid.Parse(reader["EducationCourseId"].ToString()),
                                    Guid.Parse(reader["EducationOreintionId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationCourse_EducationOrientationEntity> GetEducationCourse_EducationOrientationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationCourse_EducationOrientationEntity> lst = new List<EducationCourse_EducationOrientationEntity>();
                while (reader.Read())
                    lst.Add(GetEducationCourse_EducationOrientationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationCourse_EducationOrientationEntity> GetEducationCourse_EducationOrientationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationCourse_EducationOrientationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public DataSet GetEducationCourse_EducationOrientationDBCollectionByEducationCourseDB(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier);
            parameter.Value = (EducationCourse_EducationOrientationEntityParam.EducationCourseId==null?System.DBNull.Value:(object)EducationCourse_EducationOrientationEntityParam.EducationCourseId);
            return _intranetDB.RunProcedureDS("Isar.p_EducationCourse_EducationOrientationGetByEducationCourse", new[] { parameter });
        }

        public  List<EducationCourse_EducationOrientationEntity> GetEducationCourse_EducationOrientationDBCollectionByEducationOrientationDB(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationOreintionId", SqlDbType.UniqueIdentifier);
            parameter.Value = EducationCourse_EducationOrientationEntityParam.EducationOreintionId;
            return GetEducationCourse_EducationOrientationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_EducationCourse_EducationOrientationGetByEducationOrientation", new[] { parameter }));
        }


        #endregion



    }

}
