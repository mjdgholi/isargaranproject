﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/24>
    /// Description: <پرونده مشخصات استخدام ایثارگری>
    /// </summary>  


    public class DossierSacrifice_EmploymentDB
    {
        #region Methods :

        public void AddDossierSacrifice_EmploymentDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam, out Guid DossierSacrifice_EmploymentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_EmploymentId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@PostId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PersonnelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CommandmentNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FreeJob", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DirectManager", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@PostalCod", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,							
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_EmploymentEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_EmploymentEntityParam.OrganizationPhysicalChartId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_EmploymentEntityParam.OrganizationPhysicalChartId);
            parameters[3].Value = (DossierSacrifice_EmploymentEntityParam.EmploymentTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.EmploymentTypeId);
            parameters[4].Value = (DossierSacrifice_EmploymentEntityParam.EmploymentDate==""?System.DBNull.Value:(object)DossierSacrifice_EmploymentEntityParam.EmploymentDate);
            parameters[5].Value = (DossierSacrifice_EmploymentEntityParam.PostId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.PostId);
            parameters[6].Value = (DossierSacrifice_EmploymentEntityParam.PersonnelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.PersonnelNo.Trim()));
            parameters[7].Value = (DossierSacrifice_EmploymentEntityParam.CommandmentNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.CommandmentNo.Trim()));
            parameters[8].Value = (DossierSacrifice_EmploymentEntityParam.FreeJob == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.FreeJob.Trim()));
            parameters[9].Value = (DossierSacrifice_EmploymentEntityParam.DurationTimeAtWorkId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_EmploymentEntityParam.DurationTimeAtWorkId);
            parameters[10].Value = (DossierSacrifice_EmploymentEntityParam.DirectManager == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.DirectManager.Trim()));
            parameters[11].Value = DossierSacrifice_EmploymentEntityParam.CityId;
            parameters[12].Value = (DossierSacrifice_EmploymentEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.TelNo.Trim()));
            parameters[13].Value = (DossierSacrifice_EmploymentEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.FaxNo.Trim()));
            parameters[14].Value = (DossierSacrifice_EmploymentEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.Address.Trim()));
            parameters[15].Value = (DossierSacrifice_EmploymentEntityParam.PostalCod == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.PostalCod.Trim()));
            parameters[16].Value = (DossierSacrifice_EmploymentEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.Email.Trim()));
            parameters[17].Value = (DossierSacrifice_EmploymentEntityParam.WebAddress==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.WebAddress.Trim()));
            parameters[18].Value = DossierSacrifice_EmploymentEntityParam.StartDate;
            parameters[19].Value = (DossierSacrifice_EmploymentEntityParam.EndDate==""?System.DBNull.Value:(object)DossierSacrifice_EmploymentEntityParam.EndDate);            

            parameters[20].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EmploymentAdd", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_EmploymentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_EmploymentDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_EmploymentId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EmploymentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@PostId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PersonnelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CommandmentNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FreeJob", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DirectManager", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@TelNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@FaxNo", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Address", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@PostalCod", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@WebAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_EmploymentEntityParam.DossierSacrifice_EmploymentId;
            parameters[1].Value = DossierSacrifice_EmploymentEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_EmploymentEntityParam.OrganizationPhysicalChartId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.OrganizationPhysicalChartId);
            parameters[3].Value = (DossierSacrifice_EmploymentEntityParam.EmploymentTypeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.EmploymentTypeId);
            parameters[4].Value = (DossierSacrifice_EmploymentEntityParam.EmploymentDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.EmploymentDate);
            parameters[5].Value = (DossierSacrifice_EmploymentEntityParam.PostId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.PostId);
            parameters[6].Value = (DossierSacrifice_EmploymentEntityParam.PersonnelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.PersonnelNo.Trim()));
            parameters[7].Value = (DossierSacrifice_EmploymentEntityParam.CommandmentNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.CommandmentNo.Trim()));
            parameters[8].Value = (DossierSacrifice_EmploymentEntityParam.FreeJob == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.FreeJob.Trim()));
            parameters[9].Value = (DossierSacrifice_EmploymentEntityParam.DurationTimeAtWorkId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.DurationTimeAtWorkId);
            parameters[10].Value = (DossierSacrifice_EmploymentEntityParam.DirectManager == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.DirectManager.Trim()));
            parameters[11].Value = DossierSacrifice_EmploymentEntityParam.CityId;
            parameters[12].Value = (DossierSacrifice_EmploymentEntityParam.TelNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.TelNo.Trim()));
            parameters[13].Value = (DossierSacrifice_EmploymentEntityParam.FaxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.FaxNo.Trim()));
            parameters[14].Value = (DossierSacrifice_EmploymentEntityParam.Address == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.Address.Trim()));
            parameters[15].Value = (DossierSacrifice_EmploymentEntityParam.PostalCod == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.PostalCod.Trim()));
            parameters[16].Value = (DossierSacrifice_EmploymentEntityParam.Email == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.Email.Trim()));
            parameters[17].Value = (DossierSacrifice_EmploymentEntityParam.WebAddress == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_EmploymentEntityParam.WebAddress.Trim()));
            parameters[18].Value = DossierSacrifice_EmploymentEntityParam.StartDate;
            parameters[19].Value = (DossierSacrifice_EmploymentEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_EmploymentEntityParam.EndDate);                    

            parameters[20].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EmploymentUpdate", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_EmploymentDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_EmploymentId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_EmploymentEntityParam.DossierSacrifice_EmploymentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_EmploymentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_EmploymentEntity GetSingleDossierSacrifice_EmploymentDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_EmploymentId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_EmploymentEntityParam.DossierSacrifice_EmploymentId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EmploymentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_EmploymentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_EmploymentEntity> GetAllDossierSacrifice_EmploymentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_EmploymentDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_EmploymentGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_EmploymentEntity> GetPageDossierSacrifice_EmploymentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Employment";
            parameters[5].Value = "DossierSacrifice_EmploymentId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_EmploymentDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_EmploymentEntity GetDossierSacrifice_EmploymentDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_EmploymentEntity(Guid.Parse(reader["DossierSacrifice_EmploymentId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Convert.IsDBNull(reader["OrganizationPhysicalChartId"]) ? null : (Guid?)reader["OrganizationPhysicalChartId"],
                                    Convert.IsDBNull(reader["EmploymentTypeId"]) ? null : (Guid?)reader["EmploymentTypeId"],                                    
                                    reader["EmploymentDate"].ToString(),
                                    Convert.IsDBNull(reader["PostId"]) ? null : (Guid?)reader["PostId"],                                         
                                    reader["PersonnelNo"].ToString(),
                                    reader["CommandmentNo"].ToString(),
                                    reader["FreeJob"].ToString(),
                                    Convert.IsDBNull(reader["DurationTimeAtWorkId"]) ? null : (Guid?)reader["DurationTimeAtWorkId"],                                     
                                    reader["DirectManager"].ToString(),
                                    Guid.Parse(reader["CityId"].ToString()),
                                    reader["TelNo"].ToString(),
                                    reader["FaxNo"].ToString(),
                                    reader["Address"].ToString(),
                                    reader["PostalCod"].ToString(),
                                    reader["Email"].ToString(),
                                    reader["WebAddress"].ToString(),
                                    reader["StartDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    Guid.Parse(reader["ProvinceId"].ToString()),
                                    reader["OrganizationPhysicalChartTitle"].ToString());
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_EmploymentEntity> lst = new List<DossierSacrifice_EmploymentEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_EmploymentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_EmploymentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentDBCollectionByDossierSacrificeDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EmploymentEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_EmploymentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EmploymentGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentDBCollectionByDurationTimeAtWorkDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EmploymentEntityParam.DurationTimeAtWorkId;
            return GetDossierSacrifice_EmploymentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EmploymentGetByDurationTimeAtWork", new[] { parameter }));
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentDBCollectionByEmploymentTypeDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EmploymentTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EmploymentEntityParam.EmploymentTypeId;
            return GetDossierSacrifice_EmploymentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EmploymentGetByEmploymentType", new[] { parameter }));
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentDBCollectionByOrganizationPhysicalChartDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EmploymentEntityParam.OrganizationPhysicalChartId;
            return GetDossierSacrifice_EmploymentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EmploymentGetByOrganizationPhysicalChart", new[] { parameter }));
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentDBCollectionByPostDB(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PostId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_EmploymentEntityParam.PostId;
            return GetDossierSacrifice_EmploymentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_EmploymentGetByPost", new[] { parameter }));
        }


        #endregion



    }

  
}
