﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <نوع فارغ تحصیل>
    /// </summary>

    #region Class "GraduateTypeDB"

    public class GraduateTypeDB
    {

        #region Methods :

        public void AddGraduateTypeDB(GraduateTypeEntity GraduateTypeEntityParam, out Guid GraduateTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@GraduateTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@GraduateTypeTitle", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(GraduateTypeEntityParam.GraduateTypeTitle.Trim());
            parameters[2].Value = GraduateTypeEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_GraduateTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            GraduateTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateGraduateTypeDB(GraduateTypeEntity GraduateTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@GraduateTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@GraduateTypeTitle", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = GraduateTypeEntityParam.GraduateTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(GraduateTypeEntityParam.GraduateTypeTitle.Trim());
            parameters[2].Value = GraduateTypeEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_GraduateTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGraduateTypeDB(GraduateTypeEntity GraduateTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@GraduateTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = GraduateTypeEntityParam.GraduateTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_GraduateTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public GraduateTypeEntity GetSingleGraduateTypeDB(GraduateTypeEntity GraduateTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@GraduateTypeId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = GraduateTypeEntityParam.GraduateTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_GraduateTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGraduateTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GraduateTypeEntity> GetAllGraduateTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGraduateTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_GraduateTypeGetAll", new IDataParameter[] {}));
        }

        public List<GraduateTypeEntity> GetAllIsActiveGraduateTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGraduateTypeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_GraduateTypeGetAllIsActive", new IDataParameter[] { }));
        }

        public List<GraduateTypeEntity> GetPageGraduateTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_GraduateType";
            parameters[5].Value = "GraduateTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetGraduateTypeDBCollectionFromDataSet(ds, out count);
        }

        public GraduateTypeEntity GetGraduateTypeDBFromDataReader(IDataReader reader)
        {
            return new GraduateTypeEntity(new Guid(reader["GraduateTypeId"].ToString()), 
                                          reader["GraduateTypeTitle"].ToString(),
                                          reader["CreationDate"].ToString(),
                                          reader["ModificationDate"].ToString(),
                                          bool.Parse(reader["IsActive"].ToString()));
        }

        public List<GraduateTypeEntity> GetGraduateTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<GraduateTypeEntity> lst = new List<GraduateTypeEntity>();
                while (reader.Read())
                    lst.Add(GetGraduateTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GraduateTypeEntity> GetGraduateTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGraduateTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}