﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <اطلاعات تحصیلی ایثارگر>
    /// </summary>

    #region Class "DossierSacrifice_StudentDB"

    public class DossierSacrifice_StudentDB
    {
        #region Methods :

        public void AddDossierSacrifice_StudentDB(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam, out Guid DossierSacrifice_StudentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_StudentId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationModeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@StudentNo", SqlDbType.NVarChar, 20),
                                            new SqlParameter("@GraduateTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@InChargeOfEducation", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@TechnicalDescription", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_StudentEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_StudentEntityParam.AdmissionTypeId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.AdmissionTypeId);
            parameters[3].Value = (DossierSacrifice_StudentEntityParam.EducationCenterId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EducationCenterId);
            parameters[4].Value = DossierSacrifice_StudentEntityParam.EucationDegreeId;
            parameters[5].Value = (DossierSacrifice_StudentEntityParam.EducationCourseId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EducationCourseId);
            parameters[6].Value = (DossierSacrifice_StudentEntityParam.EducationOrientationId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EducationOrientationId);
            parameters[7].Value = (DossierSacrifice_StudentEntityParam.CityId==null?System.DBNull.Value:(object)DossierSacrifice_StudentEntityParam.CityId);
            parameters[8].Value = DossierSacrifice_StudentEntityParam.EducationModeId;
            parameters[9].Value = (DossierSacrifice_StudentEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.StartDate);
            parameters[10].Value = (DossierSacrifice_StudentEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EndDate);
            parameters[11].Value = DossierSacrifice_StudentEntityParam.StudentNo;
            parameters[12].Value = (DossierSacrifice_StudentEntityParam.GraduateTypeId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.GraduateTypeId);
            parameters[13].Value = DossierSacrifice_StudentEntityParam.InChargeOfEducation;
            parameters[14].Value = FarsiToArabic.ToArabic(DossierSacrifice_StudentEntityParam.TechnicalDescription.Trim());
            parameters[15].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_StudentAdd", parameters);
            var messageError = parameters[15].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_StudentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_StudentDB(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_StudentId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationModeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@StudentNo", SqlDbType.NVarChar, 20),
                                            new SqlParameter("@GraduateTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@InChargeOfEducation", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@TechnicalDescription", SqlDbType.NVarChar, -1),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_StudentEntityParam.DossierSacrifice_StudentId;
            parameters[1].Value = DossierSacrifice_StudentEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_StudentEntityParam.AdmissionTypeId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.AdmissionTypeId);
            parameters[3].Value = (DossierSacrifice_StudentEntityParam.EducationCenterId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EducationCenterId);
            parameters[4].Value = DossierSacrifice_StudentEntityParam.EucationDegreeId;
            parameters[5].Value = (DossierSacrifice_StudentEntityParam.EducationCourseId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EducationCourseId);
            parameters[6].Value = (DossierSacrifice_StudentEntityParam.EducationOrientationId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EducationOrientationId);
            parameters[7].Value = (DossierSacrifice_StudentEntityParam.CityId == null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.CityId);
            parameters[8].Value = DossierSacrifice_StudentEntityParam.EducationModeId;
            parameters[9].Value = (DossierSacrifice_StudentEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.StartDate);
            parameters[10].Value = (DossierSacrifice_StudentEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.EndDate);
            parameters[11].Value = DossierSacrifice_StudentEntityParam.StudentNo;
            parameters[12].Value = (DossierSacrifice_StudentEntityParam.GraduateTypeId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_StudentEntityParam.GraduateTypeId);
            parameters[13].Value = DossierSacrifice_StudentEntityParam.InChargeOfEducation;
            parameters[14].Value = FarsiToArabic.ToArabic(DossierSacrifice_StudentEntityParam.TechnicalDescription.Trim());
            parameters[15].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_StudentUpdate", parameters);
            var messageError = parameters[15].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_StudentDB(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_StudentId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_StudentEntityParam.DossierSacrifice_StudentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_StudentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_StudentEntity GetSingleDossierSacrifice_StudentDB(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_StudentId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_StudentEntityParam.DossierSacrifice_StudentId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_StudentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_StudentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_StudentEntity> GetAllDossierSacrifice_StudentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DossierSacrifice_StudentGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_StudentEntity> GetPageDossierSacrifice_StudentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_DossierSacrifice_Student";
            parameters[5].Value = "DossierSacrifice_StudentId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_StudentDBCollectionFromDataSet(ds, out count);
        }

        public static DossierSacrifice_StudentEntity GetDossierSacrifice_StudentDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_StudentEntity(new Guid(reader["DossierSacrifice_StudentId"].ToString()),
                                                      Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                      Convert.IsDBNull(reader["AdmissionTypeId"]) ? null : (Guid?) reader["AdmissionTypeId"],
                                                      Convert.IsDBNull(reader["EducationCenterId"]) ? null : (Guid?) reader["EducationCenterId"],
                                                      Guid.Parse(reader["EucationDegreeId"].ToString()),
                                                      Convert.IsDBNull(reader["EducationCourseId"]) ? null : (Guid?) reader["EducationCourseId"],
                                                      Convert.IsDBNull(reader["EducationOrientationId"]) ? null : (Guid?) reader["EducationOrientationId"],
                                                      Convert.IsDBNull(reader["CityId"]) ? null : (Guid?)reader["CityId"],                                                      
                                                      Guid.Parse(reader["EducationModeId"].ToString()),
                                                      reader["StartDate"].ToString(),
                                                      reader["EndDate"].ToString(),
                                                      reader["StudentNo"].ToString(),
                                                      Convert.IsDBNull(reader["GraduateTypeId"]) ? null : (Guid?) reader["GraduateTypeId"],
                                                      reader["InChargeOfEducation"].ToString(),
                                                      reader["TechnicalDescription"].ToString(),
                                                      reader["CreationDate"].ToString(),
                                                      reader["ModificationDate"].ToString());
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_StudentEntity> lst = new List<DossierSacrifice_StudentEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_StudentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionByAdmissionTypeDB(Guid AdmissionTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = AdmissionTypeId;
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_StudentGetByAdmissionType", new[] {parameter}));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionByDossierSacrificeDB(Guid DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeId;
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_StudentGetByDossierSacrifice", new[] {parameter}));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionByEducationCenterDB(Guid EducationCenterId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationCenterId", SqlDbType.UniqueIdentifier);
            parameter.Value = EducationCenterId;
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_StudentGetByEducationCenter", new[] {parameter}));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionByEducationCourseDB(Guid EducationCourseId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier);
            parameter.Value = EducationCourseId;
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_StudentGetByEducationCourse", new[] {parameter}));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionByEducationDegreeDB(Guid EucationDegreeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier);
            parameter.Value = EucationDegreeId;
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_StudentGetByEducationDegree", new[] {parameter}));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentDBCollectionByEducationOrientationDB(Guid EducationOrientationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EducationOrientationId", SqlDbType.UniqueIdentifier);
            parameter.Value = EducationOrientationId;
            return GetDossierSacrifice_StudentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_StudentGetByEducationOrientation", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}