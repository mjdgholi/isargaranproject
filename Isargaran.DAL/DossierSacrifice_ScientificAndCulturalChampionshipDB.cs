﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات  عناوین قهرمانی علمی و فرهنگی>
    /// </summary>


    public class DossierSacrifice_ScientificAndCulturalChampionshipDB
    {
        #region Methods :

        public void AddDossierSacrifice_ScientificAndCulturalChampionshipDB(DossierSacrifice_ScientificAndCulturalChampionshipEntity DossierSacrifice_ScientificAndCulturalChampionshipEntityParam, out Guid DossierSacrifice_ScientificAndCulturalChampionshipId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_ScientificAndCulturalChampionshipId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ScientificAndCulturalFieldId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PointOrMedal", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CompetitionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@CompetitionTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@CompetitionLocation", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.ScientificAndCulturalFieldId;
            parameters[3].Value =(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PointOrMedal==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic( DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PointOrMedal.Trim()));
            parameters[4].Value =(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionDate==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic( DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionDate));
            parameters[5].Value =(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionTitle==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionTitle.Trim()));
            parameters[6].Value =DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionLocation==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic( DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionLocation.Trim());
            parameters[7].Value = (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PersonId == null ? System.DBNull.Value : (object)DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PersonId);
            parameters[8].Value = (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.Description.Trim()));            

            parameters[9].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ScientificAndCulturalChampionshipAdd", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_ScientificAndCulturalChampionshipId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_ScientificAndCulturalChampionshipDB(DossierSacrifice_ScientificAndCulturalChampionshipEntity DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_ScientificAndCulturalChampionshipId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ScientificAndCulturalFieldId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PointOrMedal", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CompetitionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@CompetitionTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@CompetitionLocation", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.DossierSacrifice_ScientificAndCulturalChampionshipId;
            parameters[1].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.ScientificAndCulturalFieldId;
            parameters[3].Value = (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PointOrMedal == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PointOrMedal.Trim()));
            parameters[4].Value = (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionDate == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionDate));
            parameters[5].Value = (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionTitle == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionTitle.Trim()));
            parameters[6].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionLocation == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.CompetitionLocation.Trim());
            parameters[7].Value = (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PersonId == null ? System.DBNull.Value : (object)DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PersonId);
            parameters[8].Value = (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.Description.Trim()));            

            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ScientificAndCulturalChampionshipUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_ScientificAndCulturalChampionshipDB(DossierSacrifice_ScientificAndCulturalChampionshipEntity DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_ScientificAndCulturalChampionshipId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.DossierSacrifice_ScientificAndCulturalChampionshipId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ScientificAndCulturalChampionshipDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_ScientificAndCulturalChampionshipEntity GetSingleDossierSacrifice_ScientificAndCulturalChampionshipDB(DossierSacrifice_ScientificAndCulturalChampionshipEntity DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_ScientificAndCulturalChampionshipId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.DossierSacrifice_ScientificAndCulturalChampionshipId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ScientificAndCulturalChampionshipGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_ScientificAndCulturalChampionshipDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetAllDossierSacrifice_ScientificAndCulturalChampionshipDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_ScientificAndCulturalChampionshipGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetPageDossierSacrifice_ScientificAndCulturalChampionshipDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_ScientificAndCulturalChampionship";
            parameters[5].Value = "DossierSacrifice_ScientificAndCulturalChampionshipId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_ScientificAndCulturalChampionshipEntity GetDossierSacrifice_ScientificAndCulturalChampionshipDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_ScientificAndCulturalChampionshipEntity(Guid.Parse(reader["DossierSacrifice_ScientificAndCulturalChampionshipId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["ScientificAndCulturalFieldId"].ToString()),
                                    reader["PointOrMedal"].ToString(),
                                    reader["CompetitionDate"].ToString(),
                                    reader["CompetitionTitle"].ToString(),
                                    reader["CompetitionLocation"].ToString(),
                                     Convert.IsDBNull(reader["PersonId"]) ? null : (Guid?)reader["PersonId"],                                                                            
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["FullName"].ToString());
        }

        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> lst = new List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_ScientificAndCulturalChampionshipDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionByDossierSacrificeDB(DossierSacrifice_ScientificAndCulturalChampionshipEntity DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ScientificAndCulturalChampionshipGetByDossierSacrifice", new[] { parameter }));
        }

        public  List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionByScientificAndCulturalFieldDB(DossierSacrifice_ScientificAndCulturalChampionshipEntity DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ScientificAndCulturalFieldId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.ScientificAndCulturalFieldId;
            return GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ScientificAndCulturalChampionshipGetByScientificAndCulturalField", new[] { parameter }));
        }


        #endregion



    }

}
