﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/24>
    /// Description: <پرونده اطلاعات اقدامات درخواست ایثارگر>
    /// </summary>
    #region Class "DossierSacrifice_RequestHistoryDB"

    public class DossierSacrifice_RequestHistoryDB
    {


        #region Methods :

        public void AddDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntity dossierSacrificeRequestHistoryEntityParam, out Guid DossierSacrifice_RequestHistoryId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB(ConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString.ToString() + String.Format(";Workstation ID={0}", HttpContext.Current.User.Identity.Name));
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_RequestHistoryId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ActionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ActionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ActionDescription", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = dossierSacrificeRequestHistoryEntityParam.DossierSacrifice_RequestId;
            parameters[2].Value = dossierSacrificeRequestHistoryEntityParam.StatusId;
            parameters[3].Value = dossierSacrificeRequestHistoryEntityParam.ActionTypeId;
            parameters[4].Value = dossierSacrificeRequestHistoryEntityParam.ActionDate;
            parameters[5].Value = (dossierSacrificeRequestHistoryEntityParam.ActionDescription==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(dossierSacrificeRequestHistoryEntityParam.ActionDescription));            

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_DossierSacrifice_RequestHistoryAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_RequestHistoryId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntity dossierSacrificeRequestHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB(ConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString.ToString() + String.Format(";Workstation ID={0}", HttpContext.Current.User.Identity.Name));
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_RequestHistoryId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ActionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ActionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ActionDescription", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Value = dossierSacrificeRequestHistoryEntityParam.DossierSacrifice_RequestHistoryId;
            parameters[1].Value = dossierSacrificeRequestHistoryEntityParam.DossierSacrifice_RequestId;
            parameters[2].Value = dossierSacrificeRequestHistoryEntityParam.StatusId;
            parameters[3].Value = dossierSacrificeRequestHistoryEntityParam.ActionTypeId;
            parameters[4].Value = dossierSacrificeRequestHistoryEntityParam.ActionDate;
            parameters[5].Value = (dossierSacrificeRequestHistoryEntityParam.ActionDescription == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(dossierSacrificeRequestHistoryEntityParam.ActionDescription));

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_DossierSacrifice_RequestHistoryUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntity DossierSacrifice_RequestHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB(ConfigurationManager.ConnectionStrings["SqlconstrSelf"].ConnectionString.ToString() + String.Format(";Workstation ID={0}", HttpContext.Current.User.Identity.Name));
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_RequestHistoryId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_RequestHistoryEntityParam.DossierSacrifice_RequestHistoryId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_DossierSacrifice_RequestHistoryDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_RequestHistoryEntity GetSingleDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntity DossierSacrifice_RequestHistoryEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_RequestHistoryId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_RequestHistoryEntityParam.DossierSacrifice_RequestHistoryId;

                reader = _intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestHistoryGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_RequestHistoryDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_RequestHistoryEntity> GetAllDossierSacrifice_RequestHistoryDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_RequestHistoryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_DossierSacrifice_RequestHistoryGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_RequestHistoryEntity> GetPageDossierSacrifice_RequestHistoryDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_RequestHistory";
            parameters[5].Value = "DossierSacrifice_RequestHistoryId";
            DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_RequestHistoryDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_RequestHistoryEntity GetDossierSacrifice_RequestHistoryDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_RequestHistoryEntity(Guid.Parse(reader["DossierSacrifice_RequestHistoryId"].ToString()),
                                    Guid.Parse(reader["DossierSacrifice_RequestId"].ToString()),
                                    Guid.Parse(reader["StatusId"].ToString()),
                                    Guid.Parse(reader["ActionTypeId"].ToString()),
                                    reader["ActionDate"].ToString(),
                                    reader["ActionDescription"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsValid"].ToString()));
        }

        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_RequestHistoryEntity> lst = new List<DossierSacrifice_RequestHistoryEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_RequestHistoryDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_RequestHistoryDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryDBCollectionByActionTypeDB(DossierSacrifice_RequestHistoryEntity dossierSacrificeRequestHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ActionTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeRequestHistoryEntityParam.ActionTypeId;
            return GetDossierSacrifice_RequestHistoryDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestHistoryGetByActionType", new[] { parameter }));
        }

        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryDBCollectionByDossierSacrifice_RequestDB(DossierSacrifice_RequestHistoryEntity dossierSacrificeRequestHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeRequestHistoryEntityParam.DossierSacrifice_RequestId;
            return GetDossierSacrifice_RequestHistoryDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestHistoryGetByDossierSacrifice_Request", new[] { parameter }));
        }

        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryDBCollectionByStatusDB(DossierSacrifice_RequestHistoryEntity dossierSacrificeRequestHistoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeRequestHistoryEntityParam.StatusId;
            return GetDossierSacrifice_RequestHistoryDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestHistoryGetByStatus", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}