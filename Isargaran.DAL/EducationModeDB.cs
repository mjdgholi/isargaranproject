﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{

    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <نوع تحصیل>
    /// </summary>

    #region Class "EducationModeDB"

    public class EducationModeDB
    {

        #region Methods :

        public void AddEducationModeDB(EducationModeEntity EducationModeEntityParam, out Guid EducationModeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@EducationModeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationModePersianTitle", SqlDbType.NVarChar, 150),
                                            new SqlParameter("@EducationModeEnglishTitle", SqlDbType.NVarChar, 150),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationModeEntityParam.EducationModePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationModeEntityParam.EducationModeEnglishTitle.Trim());
            parameters[3].Value = EducationModeEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationModeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EducationModeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEducationModeDB(EducationModeEntity EducationModeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@EducationModeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationModePersianTitle", SqlDbType.NVarChar, 150),
                                            new SqlParameter("@EducationModeEnglishTitle", SqlDbType.NVarChar, 150),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = EducationModeEntityParam.EducationModeId;
            parameters[1].Value = FarsiToArabic.ToArabic(EducationModeEntityParam.EducationModePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(EducationModeEntityParam.EducationModeEnglishTitle.Trim());
            parameters[3].Value = EducationModeEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_EducationModeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEducationModeDB(EducationModeEntity EducationModeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@EducationModeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = EducationModeEntityParam.EducationModeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_EducationModeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EducationModeEntity GetSingleEducationModeDB(EducationModeEntity EducationModeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@EducationModeId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = EducationModeEntityParam.EducationModeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_EducationModeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEducationModeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationModeEntity> GetAllEducationModeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationModeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_EducationModeGetAll", new IDataParameter[] {}));
        }

        public List<EducationModeEntity> GetAllIsActiveEducationModeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEducationModeDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_EducationModeGetAllIsActive", new IDataParameter[] { }));
        }

        public List<EducationModeEntity> GetPageEducationModeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_EducationMode";
            parameters[5].Value = "EducationModeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetEducationModeDBCollectionFromDataSet(ds, out count);
        }

        public EducationModeEntity GetEducationModeDBFromDataReader(IDataReader reader)
        {
            return new EducationModeEntity(new Guid(reader["EducationModeId"].ToString()),
                                           reader["EducationModePersianTitle"].ToString(),
                                           reader["EducationModeEnglishTitle"].ToString(),
                                           reader["CreationDate"].ToString(),
                                           reader["ModificationDate"].ToString(),
                                           bool.Parse(reader["IsActive"].ToString()));
        }

        public List<EducationModeEntity> GetEducationModeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EducationModeEntity> lst = new List<EducationModeEntity>();
                while (reader.Read())
                    lst.Add(GetEducationModeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EducationModeEntity> GetEducationModeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEducationModeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

    #endregion
}