﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<اطلاعات ظاهری>
    /// </summary>
    #region Class "DossierSacrifice_AppearanceDB"

    public class DossierSacrifice_AppearanceDB
    {
        #region Methods :

        public void AddDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam, out Guid DossierSacrifice_AppearanceId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_AppearanceId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EyeColorId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@HairColorId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SkinColorId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@Height", SqlDbType.Int),
                                            new SqlParameter("@Weight", SqlDbType.Decimal),
                                            new SqlParameter("@ShoeSize", SqlDbType.Int),
                                            new SqlParameter("@CostumeSize", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_AppearanceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_AppearanceEntityParam.BloodGroupId;
            parameters[3].Value = (DossierSacrifice_AppearanceEntityParam.EyeColorId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_AppearanceEntityParam.EyeColorId);
            parameters[4].Value = (DossierSacrifice_AppearanceEntityParam.HairColorId==new Guid() ? System.DBNull.Value : (object)DossierSacrifice_AppearanceEntityParam.HairColorId);
            parameters[5].Value = (DossierSacrifice_AppearanceEntityParam.SkinColorId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_AppearanceEntityParam.SkinColorId);
            parameters[6].Value = (DossierSacrifice_AppearanceEntityParam.Height  == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_AppearanceEntityParam.Height);
            parameters[7].Value = DossierSacrifice_AppearanceEntityParam.Weight   == (decimal?) null ? DBNull.Value : (object) DossierSacrifice_AppearanceEntityParam.Weight;
            parameters[8].Value = DossierSacrifice_AppearanceEntityParam.ShoeSize == (int?) null ? DBNull.Value : (object) DossierSacrifice_AppearanceEntityParam.ShoeSize;
            parameters[9].Value = DossierSacrifice_AppearanceEntityParam.CostumeSize;
            parameters[10].Value = DossierSacrifice_AppearanceEntityParam.IsActive;
            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_AppearanceAdd", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_AppearanceId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_AppearanceId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EyeColorId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@HairColorId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@SkinColorId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@Height", SqlDbType.Int),
                                            new SqlParameter("@Weight", SqlDbType.Decimal),
                                            new SqlParameter("@ShoeSize", SqlDbType.Int),
                                            new SqlParameter("@CostumeSize", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_AppearanceEntityParam.DossierSacrifice_AppearanceId;
            parameters[1].Value = DossierSacrifice_AppearanceEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_AppearanceEntityParam.BloodGroupId;
            parameters[3].Value = (DossierSacrifice_AppearanceEntityParam.EyeColorId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_AppearanceEntityParam.EyeColorId);
            parameters[4].Value = (DossierSacrifice_AppearanceEntityParam.HairColorId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_AppearanceEntityParam.HairColorId);
            parameters[5].Value = (DossierSacrifice_AppearanceEntityParam.SkinColorId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_AppearanceEntityParam.SkinColorId);
            parameters[6].Value = (DossierSacrifice_AppearanceEntityParam.Height == (int?)null ? System.DBNull.Value : (object)DossierSacrifice_AppearanceEntityParam.Height);
            parameters[7].Value = DossierSacrifice_AppearanceEntityParam.Weight == (decimal?)null ? DBNull.Value : (object)DossierSacrifice_AppearanceEntityParam.Weight;
            parameters[8].Value = DossierSacrifice_AppearanceEntityParam.ShoeSize == (int?)null ? DBNull.Value : (object)DossierSacrifice_AppearanceEntityParam.ShoeSize;
            parameters[9].Value = DossierSacrifice_AppearanceEntityParam.CostumeSize;
            parameters[10].Value = DossierSacrifice_AppearanceEntityParam.IsActive;
            parameters[11].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_AppearanceUpdate", parameters);
            var messageError = parameters[11].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_AppearanceId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_AppearanceEntityParam.DossierSacrifice_AppearanceId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_AppearanceDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_AppearanceEntity GetSingleDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_AppearanceId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_AppearanceEntityParam.DossierSacrifice_AppearanceId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_AppearanceGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_AppearanceDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_AppearanceEntity> GetAllDossierSacrifice_AppearanceDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_AppearanceDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DossierSacrifice_AppearanceGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_AppearanceEntity> GetPageDossierSacrifice_AppearanceDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_DossierSacrifice_Appearance";
            parameters[5].Value = "DossierSacrifice_AppearanceId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_AppearanceDBCollectionFromDataSet(ds, out count);
        }

        public static DossierSacrifice_AppearanceEntity GetDossierSacrifice_AppearanceDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_AppearanceEntity(new Guid(reader["DossierSacrifice_AppearanceId"].ToString()),
                                                         Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                         Guid.Parse(reader["BloodGroupId"].ToString()),                                                         
                                                         Convert.IsDBNull(reader["EyeColorId"]) ? null : (Guid?)reader["EyeColorId"],
                                                         Convert.IsDBNull(reader["HairColorId"]) ? null : (Guid?)reader["HairColorId"],
                                                         Convert.IsDBNull(reader["SkinColorId"]) ? null : (Guid?)reader["SkinColorId"],
                                                         Convert.IsDBNull(reader["Height"]) ? null : (int?)reader["Height"],
                                                         Convert.IsDBNull(reader["Weight"]) ? null : (decimal?)reader["Weight"],                                                             
                                                         Convert.IsDBNull(reader["ShoeSize"])? null : (int?)reader["ShoeSize"],
                                                         reader["CostumeSize"].ToString(),
                                                         reader["CreationDate"].ToString(),
                                                         reader["ModificationDate"].ToString(),
                                                         bool.Parse(reader["IsActive"].ToString()));
        }

        public static List<DossierSacrifice_AppearanceEntity> GetDossierSacrifice_AppearanceDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_AppearanceEntity> lst = new List<DossierSacrifice_AppearanceEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_AppearanceDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public static List<DossierSacrifice_AppearanceEntity> GetDossierSacrifice_AppearanceDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_AppearanceDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<DossierSacrifice_AppearanceEntity> GetDossierSacrifice_AppearanceDBCollectionByBloodGroupDB(int BloodGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@BloodGroupId", SqlDbType.Int);
            parameter.Value = BloodGroupId;
            return GetDossierSacrifice_AppearanceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_AppearanceGetByBloodGroup", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}