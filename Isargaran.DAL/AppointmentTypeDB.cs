﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/16>
    /// Description: <نوع مراجعه>
    /// </summary>

    public class AppointmentTypeDB
    {
        #region Methods :

        public void AddAppointmentTypeDB(AppointmentTypeEntity AppointmentTypeEntityParam, out Guid AppointmentTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@AppointmentTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@AppointmentTypeTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(AppointmentTypeEntityParam.AppointmentTypeTitle.Trim());
            parameters[2].Value = AppointmentTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_AppointmentTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            AppointmentTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateAppointmentTypeDB(AppointmentTypeEntity AppointmentTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@AppointmentTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@AppointmentTypeTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = AppointmentTypeEntityParam.AppointmentTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(AppointmentTypeEntityParam.AppointmentTypeTitle.Trim());
            parameters[2].Value = AppointmentTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_AppointmentTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteAppointmentTypeDB(AppointmentTypeEntity AppointmentTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@AppointmentTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = AppointmentTypeEntityParam.AppointmentTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_AppointmentTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public AppointmentTypeEntity GetSingleAppointmentTypeDB(AppointmentTypeEntity AppointmentTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@AppointmentTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = AppointmentTypeEntityParam.AppointmentTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_AppointmentTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAppointmentTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<AppointmentTypeEntity> GetAllAppointmentTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAppointmentTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_AppointmentTypeGetAll", new IDataParameter[] { }));
        }

        public List<AppointmentTypeEntity> GetAllAppointmentTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAppointmentTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_AppointmentTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<AppointmentTypeEntity> GetPageAppointmentTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AppointmentType";
            parameters[5].Value = "AppointmentTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetAppointmentTypeDBCollectionFromDataSet(ds, out count);
        }

        public AppointmentTypeEntity GetAppointmentTypeDBFromDataReader(IDataReader reader)
        {
            return new AppointmentTypeEntity(Guid.Parse(reader["AppointmentTypeId"].ToString()),
                                    reader["AppointmentTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<AppointmentTypeEntity> GetAppointmentTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<AppointmentTypeEntity> lst = new List<AppointmentTypeEntity>();
                while (reader.Read())
                    lst.Add(GetAppointmentTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<AppointmentTypeEntity> GetAppointmentTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAppointmentTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}
