﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <پرونده تخلف>
    /// </summary>

    public class DossierSacrifice_OfficeInfractionDB
    {
        #region Methods :

        public void AddDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntity dossierSacrificeOfficeInfractionEntityParam, out Guid DossierSacrifice_OfficeInfractionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_OfficeInfractionId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@InfractionLocation", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@InfractionDate", SqlDbType.Date) ,
											  new SqlParameter("@InfractionTime", SqlDbType.Time,7) ,
											  new SqlParameter("@InfractionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@InfractionContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = dossierSacrificeOfficeInfractionEntityParam.DossierSacrificeId;
            parameters[2].Value = dossierSacrificeOfficeInfractionEntityParam.InfractionLocation;
            parameters[3].Value = dossierSacrificeOfficeInfractionEntityParam.InfractionDate;
            parameters[4].Value = (dossierSacrificeOfficeInfractionEntityParam.InfractionTime==":"?System.DBNull.Value:(object)dossierSacrificeOfficeInfractionEntityParam.InfractionTime.Trim());
            parameters[5].Value = dossierSacrificeOfficeInfractionEntityParam.InfractionTypeId;
            parameters[6].Value = (dossierSacrificeOfficeInfractionEntityParam.InfractionContent==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(dossierSacrificeOfficeInfractionEntityParam.InfractionContent.Trim()));
            parameters[7].Value = (dossierSacrificeOfficeInfractionEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(dossierSacrificeOfficeInfractionEntityParam.Description.Trim()));            

            parameters[8].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_OfficeInfractionAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_OfficeInfractionId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntity dossierSacrificeOfficeInfractionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_OfficeInfractionId", SqlDbType.UniqueIdentifier),
		 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@InfractionLocation", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@InfractionDate", SqlDbType.Date) ,
											  new SqlParameter("@InfractionTime", SqlDbType.Time,7) ,
											  new SqlParameter("@InfractionTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@InfractionContent", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = dossierSacrificeOfficeInfractionEntityParam.DossierSacrifice_OfficeInfractionId;
            parameters[1].Value = dossierSacrificeOfficeInfractionEntityParam.DossierSacrificeId;
            parameters[2].Value = dossierSacrificeOfficeInfractionEntityParam.InfractionLocation;
            parameters[3].Value = dossierSacrificeOfficeInfractionEntityParam.InfractionDate;
            parameters[4].Value = (dossierSacrificeOfficeInfractionEntityParam.InfractionTime==":"?System.DBNull.Value:(object)dossierSacrificeOfficeInfractionEntityParam.InfractionTime.Trim());
            parameters[5].Value = dossierSacrificeOfficeInfractionEntityParam.InfractionTypeId;
            parameters[6].Value = (dossierSacrificeOfficeInfractionEntityParam.InfractionContent==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(dossierSacrificeOfficeInfractionEntityParam.InfractionContent.Trim()));
            parameters[7].Value = (dossierSacrificeOfficeInfractionEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(dossierSacrificeOfficeInfractionEntityParam.Description.Trim())); 

            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_OfficeInfractionUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntity DossierSacrifice_OfficeInfractionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_OfficeInfractionId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_OfficeInfractionEntityParam.DossierSacrifice_OfficeInfractionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_OfficeInfractionDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_OfficeInfractionEntity GetSingleDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntity DossierSacrifice_OfficeInfractionEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_OfficeInfractionId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_OfficeInfractionEntityParam.DossierSacrifice_OfficeInfractionId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_OfficeInfractionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_OfficeInfractionDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_OfficeInfractionEntity> GetAllDossierSacrifice_OfficeInfractionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_OfficeInfractionDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_OfficeInfractionGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_OfficeInfractionEntity> GetPageDossierSacrifice_OfficeInfractionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_OfficeInfraction";
            parameters[5].Value = "DossierSacrifice_OfficeInfractionId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_OfficeInfractionDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_OfficeInfractionEntity GetDossierSacrifice_OfficeInfractionDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_OfficeInfractionEntity(Guid.Parse(reader["DossierSacrifice_OfficeInfractionId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["InfractionLocation"].ToString(),
                                    reader["InfractionDate"].ToString(),
                                    reader["InfractionTime"].ToString(),
                                    Guid.Parse(reader["InfractionTypeId"].ToString()),
                                    reader["InfractionContent"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_OfficeInfractionEntity> GetDossierSacrifice_OfficeInfractionDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_OfficeInfractionEntity> lst = new List<DossierSacrifice_OfficeInfractionEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_OfficeInfractionDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_OfficeInfractionEntity> GetDossierSacrifice_OfficeInfractionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_OfficeInfractionDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_OfficeInfractionEntity> GetDossierSacrifice_OfficeInfractionDBCollectionByDossierSacrificeDB(DossierSacrifice_OfficeInfractionEntity dossierSacrificeOfficeInfractionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeOfficeInfractionEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_OfficeInfractionDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_OfficeInfractionGetByDossierSacrifice", new[] { parameter }));
        }

        public  List<DossierSacrifice_OfficeInfractionEntity> GetDossierSacrifice_OfficeInfractionDBCollectionByInfractionTypeDB(DossierSacrifice_OfficeInfractionEntity dossierSacrificeOfficeInfractionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@InfractionTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeOfficeInfractionEntityParam.InfractionTypeId;
            return GetDossierSacrifice_OfficeInfractionDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_OfficeInfractionGetByInfractionType", new[] { parameter }));
        }


        #endregion



    }

    
}
