﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<موقیعت محل اقامت>
    /// </summary>



    public class HabitatLocationTypeDB
    {


        #region Methods :

        public void AddHabitatLocationTypeDB(HabitatLocationTypeEntity HabitatLocationTypeEntityParam, out Guid HabitatLocationTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@HabitatLocationTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@HabitatLocationTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(HabitatLocationTypeEntityParam.HabitatLocationTypeTitle.Trim());            
            parameters[2].Value = HabitatLocationTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatLocationTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            HabitatLocationTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateHabitatLocationTypeDB(HabitatLocationTypeEntity HabitatLocationTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@HabitatLocationTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@HabitatLocationTypeTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = HabitatLocationTypeEntityParam.HabitatLocationTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(HabitatLocationTypeEntityParam.HabitatLocationTypeTitle.Trim());            
            parameters[2].Value = HabitatLocationTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_HabitatLocationTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteHabitatLocationTypeDB(HabitatLocationTypeEntity HabitatLocationTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@HabitatLocationTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = HabitatLocationTypeEntityParam.HabitatLocationTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_HabitatLocationTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public HabitatLocationTypeEntity GetSingleHabitatLocationTypeDB(HabitatLocationTypeEntity HabitatLocationTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@HabitatLocationTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = HabitatLocationTypeEntityParam.HabitatLocationTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_HabitatLocationTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetHabitatLocationTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatLocationTypeEntity> GetAllHabitatLocationTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatLocationTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatLocationTypeGetAll", new IDataParameter[] { }));
        }
        public List<HabitatLocationTypeEntity> GetAllHabitatLocationTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetHabitatLocationTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_HabitatLocationTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<HabitatLocationTypeEntity> GetPageHabitatLocationTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_HabitatLocationType";
            parameters[5].Value = "HabitatLocationTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetHabitatLocationTypeDBCollectionFromDataSet(ds, out count);
        }

        public HabitatLocationTypeEntity GetHabitatLocationTypeDBFromDataReader(IDataReader reader)
        {
            return new HabitatLocationTypeEntity(Guid.Parse(reader["HabitatLocationTypeId"].ToString()),
                                    reader["HabitatLocationTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<HabitatLocationTypeEntity> GetHabitatLocationTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<HabitatLocationTypeEntity> lst = new List<HabitatLocationTypeEntity>();
                while (reader.Read())
                    lst.Add(GetHabitatLocationTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<HabitatLocationTypeEntity> GetHabitatLocationTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetHabitatLocationTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    
}
