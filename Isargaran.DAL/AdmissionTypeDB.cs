﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <نوع پذیرش>
    /// </summary>


    public class AdmissionTypeDB
    {



        #region Methods :

        public void AddAdmissionTypeDB(AdmissionTypeEntity AdmissionTypeEntityParam, out Guid AdmissionTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@AdmissionTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@HasTuition", SqlDbType.Bit) ,
											  new SqlParameter("@IsDaily", SqlDbType.Bit) ,
											  new SqlParameter("@IsE_Lerning", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(AdmissionTypeEntityParam.AdmissionTypeTitle.Trim());
            parameters[2].Value = AdmissionTypeEntityParam.HasTuition;
            parameters[3].Value = AdmissionTypeEntityParam.IsDaily;
            parameters[4].Value = AdmissionTypeEntityParam.IsE_Lerning;
            parameters[5].Value = AdmissionTypeEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_AdmissionTypeAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            AdmissionTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateAdmissionTypeDB(AdmissionTypeEntity AdmissionTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@AdmissionTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@HasTuition", SqlDbType.Bit) ,
											  new SqlParameter("@IsDaily", SqlDbType.Bit) ,
											  new SqlParameter("@IsE_Lerning", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = AdmissionTypeEntityParam.AdmissionTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(AdmissionTypeEntityParam.AdmissionTypeTitle.Trim());
            parameters[2].Value = AdmissionTypeEntityParam.HasTuition;
            parameters[3].Value = AdmissionTypeEntityParam.IsDaily;
            parameters[4].Value = AdmissionTypeEntityParam.IsE_Lerning;
            parameters[5].Value = AdmissionTypeEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_AdmissionTypeUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteAdmissionTypeDB(AdmissionTypeEntity AdmissionTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = AdmissionTypeEntityParam.AdmissionTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_AdmissionTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public AdmissionTypeEntity GetSingleAdmissionTypeDB(AdmissionTypeEntity AdmissionTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@AdmissionTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = AdmissionTypeEntityParam.AdmissionTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_AdmissionTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAdmissionTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<AdmissionTypeEntity> GetAllAdmissionTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAdmissionTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_AdmissionTypeGetAll", new IDataParameter[] { }));
        }
        public List<AdmissionTypeEntity> GetAllAdmissionTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAdmissionTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_AdmissionTypeIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<AdmissionTypeEntity> GetPageAdmissionTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AdmissionType";
            parameters[5].Value = "AdmissionTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetAdmissionTypeDBCollectionFromDataSet(ds, out count);
        }

        public AdmissionTypeEntity GetAdmissionTypeDBFromDataReader(IDataReader reader)
        {
            return new AdmissionTypeEntity(Guid.Parse(reader["AdmissionTypeId"].ToString()),
                                    reader["AdmissionTypeTitle"].ToString(),
                                    bool.Parse(reader["HasTuition"].ToString()),
                                    bool.Parse(reader["IsDaily"].ToString()),
                                    bool.Parse(reader["IsE_Lerning"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<AdmissionTypeEntity> GetAdmissionTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<AdmissionTypeEntity> lst = new List<AdmissionTypeEntity>();
                while (reader.Read())
                    lst.Add(GetAdmissionTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<AdmissionTypeEntity> GetAdmissionTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAdmissionTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }
}
