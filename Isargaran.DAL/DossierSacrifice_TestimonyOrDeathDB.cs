﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/23>
    // Description:	<اطلاعات فوت یا شهادت>
    /// </summary>

    #region Class "DossierSacrifice_TestimonyOrDeathDB"

    public class DossierSacrifice_TestimonyOrDeathDB
    {
        #region Methods :

        public void AddDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam, out Guid DossierSacrifice_TestimonyOrDeathId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_TestimonyOrDeathId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DaethDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@VillageTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@TombTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@PartNo", SqlDbType.Int),
                                            new SqlParameter("@RowNo", SqlDbType.Int),
                                            new SqlParameter("@GraveNo", SqlDbType.Int),
                                            new SqlParameter("@TestimonyLocation", SqlDbType.NVarChar, 300),
                                            new SqlParameter("@DurationOfPresenceInBattlefield", SqlDbType.Int),
                                            new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@PostId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsWarDisappeared", SqlDbType.Bit),
                                            new SqlParameter("@TestimonyPeriodTimeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CauseEventTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_TestimonyOrDeathEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_TestimonyOrDeathEntityParam.DaethDate;
            parameters[3].Value = DossierSacrifice_TestimonyOrDeathEntityParam.CityId;
            parameters[4].Value = FarsiToArabic.ToArabic(DossierSacrifice_TestimonyOrDeathEntityParam.VillageTitle.Trim());
            parameters[5].Value = FarsiToArabic.ToArabic(DossierSacrifice_TestimonyOrDeathEntityParam.TombTitle.Trim());
            parameters[6].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.PartNo == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.PartNo);
            parameters[7].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.RowNo == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.RowNo);
            parameters[8].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.GraveNo == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.GraveNo);
            parameters[9].Value = FarsiToArabic.ToArabic(DossierSacrifice_TestimonyOrDeathEntityParam.TestimonyLocation.Trim());
            parameters[10].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.DurationOfPresenceInBattlefield == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.DurationOfPresenceInBattlefield);
            parameters[11].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.EucationDegreeId==null?System.DBNull.Value:(object)DossierSacrifice_TestimonyOrDeathEntityParam.EucationDegreeId);
            parameters[12].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.EducationCourseId==null?System.DBNull.Value:(object)DossierSacrifice_TestimonyOrDeathEntityParam.EducationCourseId);
            parameters[13].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.OrganizationPhysicalChartId==(Guid?)null?System.DBNull.Value:(object)DossierSacrifice_TestimonyOrDeathEntityParam.OrganizationPhysicalChartId);
            parameters[14].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.PostId == (Guid?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.PostId);
            parameters[15].Value = DossierSacrifice_TestimonyOrDeathEntityParam.MilitaryUnitDispatcherId;
            parameters[16].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.IsWarDisappeared == null ? System.DBNull.Value : (object)DossierSacrifice_TestimonyOrDeathEntityParam.IsWarDisappeared);
            parameters[17].Value = DossierSacrifice_TestimonyOrDeathEntityParam.TestimonyPeriodTimeId;
            parameters[18].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.CauseEventTypeId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_TestimonyOrDeathEntityParam.CauseEventTypeId);
            parameters[19].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TestimonyOrDeathAdd", parameters);
            var messageError = parameters[19].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_TestimonyOrDeathId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_TestimonyOrDeathId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@DaethDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@VillageTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@TombTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@PartNo", SqlDbType.Int),
                                            new SqlParameter("@RowNo", SqlDbType.Int),
                                            new SqlParameter("@GraveNo", SqlDbType.Int),
                                            new SqlParameter("@TestimonyLocation", SqlDbType.NVarChar, 300),
                                            new SqlParameter("@DurationOfPresenceInBattlefield", SqlDbType.Int),
                                            new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@EducationCourseId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@PostId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IsWarDisappeared", SqlDbType.Bit),
                                            new SqlParameter("@TestimonyPeriodTimeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CauseEventTypeId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = DossierSacrifice_TestimonyOrDeathEntityParam.DossierSacrifice_TestimonyOrDeathId;
            parameters[1].Value = DossierSacrifice_TestimonyOrDeathEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_TestimonyOrDeathEntityParam.DaethDate;
            parameters[3].Value = DossierSacrifice_TestimonyOrDeathEntityParam.CityId;
            parameters[4].Value = FarsiToArabic.ToArabic(DossierSacrifice_TestimonyOrDeathEntityParam.VillageTitle.Trim());
            parameters[5].Value = FarsiToArabic.ToArabic(DossierSacrifice_TestimonyOrDeathEntityParam.TombTitle.Trim());
            parameters[6].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.PartNo == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.PartNo);
            parameters[7].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.RowNo == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.RowNo);
            parameters[8].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.GraveNo == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.GraveNo);
            parameters[9].Value = FarsiToArabic.ToArabic(DossierSacrifice_TestimonyOrDeathEntityParam.TestimonyLocation.Trim());
            parameters[10].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.DurationOfPresenceInBattlefield == (int?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.DurationOfPresenceInBattlefield);
            parameters[11].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.EucationDegreeId == null ? System.DBNull.Value : (object)DossierSacrifice_TestimonyOrDeathEntityParam.EucationDegreeId);
            parameters[12].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.EducationCourseId == null ? System.DBNull.Value : (object)DossierSacrifice_TestimonyOrDeathEntityParam.EducationCourseId);
            parameters[13].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.OrganizationPhysicalChartId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_TestimonyOrDeathEntityParam.OrganizationPhysicalChartId);
            parameters[14].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.PostId == (Guid?) null ? System.DBNull.Value : (object) DossierSacrifice_TestimonyOrDeathEntityParam.PostId);
            parameters[15].Value = DossierSacrifice_TestimonyOrDeathEntityParam.MilitaryUnitDispatcherId;
            parameters[16].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.IsWarDisappeared==null?System.DBNull.Value:(object)DossierSacrifice_TestimonyOrDeathEntityParam.IsWarDisappeared);
            parameters[17].Value = DossierSacrifice_TestimonyOrDeathEntityParam.TestimonyPeriodTimeId;
            parameters[18].Value = (DossierSacrifice_TestimonyOrDeathEntityParam.CauseEventTypeId == (Guid?)null ? System.DBNull.Value : (object)DossierSacrifice_TestimonyOrDeathEntityParam.CauseEventTypeId);
            parameters[19].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TestimonyOrDeathUpdate", parameters);
            var messageError = parameters[19].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DossierSacrifice_TestimonyOrDeathId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = DossierSacrifice_TestimonyOrDeathEntityParam.DossierSacrifice_TestimonyOrDeathId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_TestimonyOrDeathDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_TestimonyOrDeathEntity GetSingleDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@DossierSacrifice_TestimonyOrDeathId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = DossierSacrifice_TestimonyOrDeathEntityParam.DossierSacrifice_TestimonyOrDeathId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TestimonyOrDeathGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_TestimonyOrDeathDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_TestimonyOrDeathEntity> GetAllDossierSacrifice_TestimonyOrDeathDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_DossierSacrifice_TestimonyOrDeathGetAll", new IDataParameter[] {}));
        }

        public List<DossierSacrifice_TestimonyOrDeathEntity> GetPageDossierSacrifice_TestimonyOrDeathDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_TestimonyOrDeath";
            parameters[5].Value = "DossierSacrifice_TestimonyOrDeathId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataSet(ds, out count);
        }

        public static DossierSacrifice_TestimonyOrDeathEntity GetDossierSacrifice_TestimonyOrDeathDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_TestimonyOrDeathEntity(new Guid(reader["DossierSacrifice_TestimonyOrDeathId"].ToString()),
                                                               Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                                               reader["DaethDate"].ToString(),
                                                               Guid.Parse(reader["CityId"].ToString()),
                                                               reader["VillageTitle"].ToString(),
                                                               reader["TombTitle"].ToString(),
                                                               Convert.IsDBNull(reader["PartNo"]) ? null : (int?) reader["PartNo"],
                                                               Convert.IsDBNull(reader["RowNo"]) ? null : (int?) (reader["RowNo"]),
                                                               Convert.IsDBNull(reader["GraveNo"]) ? null : (int?) (reader["GraveNo"]),
                                                               reader["TestimonyLocation"].ToString(),
                                                               Convert.IsDBNull(reader["DurationOfPresenceInBattlefield"]) ? null : (int?) reader["DurationOfPresenceInBattlefield"],
                                                               Convert.IsDBNull(reader["EucationDegreeId"]) ? null : (Guid?)reader["EucationDegreeId"],
                                                               Convert.IsDBNull(reader["EducationCourseId"]) ? null : (Guid?)reader["EducationCourseId"],                                                                
                                                               Convert.IsDBNull(reader["OrganizationPhysicalChartId"]) ? null : (Guid?)reader["OrganizationPhysicalChartId"],                                                               
                                                               Convert.IsDBNull(reader["PostId"]) ? null : (Guid?) reader["PostId"],
                                                               Guid.Parse(reader["MilitaryUnitDispatcherId"].ToString()),
                                                               Convert.IsDBNull(reader["IsWarDisappeared"]) ? null : (bool?) (reader["IsWarDisappeared"]),                                                               
                                                               Guid.Parse(reader["TestimonyPeriodTimeId"].ToString()),
                                                               Convert.IsDBNull(reader["CauseEventTypeId"]) ? null : (Guid?)reader["CauseEventTypeId"],
                                                               reader["CreationDate"].ToString(),
                                                               reader["ModificationDate"].ToString());
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_TestimonyOrDeathEntity> lst = new List<DossierSacrifice_TestimonyOrDeathEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_TestimonyOrDeathDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathDBCollectionByDossierSacrificeDB(Guid DossierSacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrificeId;
            return GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TestimonyOrDeathGetByDossierSacrifice", new[] {parameter}));
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathDBCollectionByEducationDegreeDB(Guid EucationDegreeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier);
            parameter.Value = EucationDegreeId;
            return GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TestimonyOrDeathGetByEducationDegree", new[] {parameter}));
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathDBCollectionByMilitaryUnitDispatcherDB(Guid MilitaryUnitDispatcherId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier);
            parameter.Value = MilitaryUnitDispatcherId;
            return GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TestimonyOrDeathGetByMilitaryUnitDispatcher", new[] {parameter}));
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathDBCollectionByTestimonyPeriodTimeDB(Guid TestimonyPeriodTimeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@TestimonyPeriodTimeId", SqlDbType.UniqueIdentifier);
            parameter.Value = TestimonyPeriodTimeId;
            return GetDossierSacrifice_TestimonyOrDeathDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_TestimonyOrDeathGetByTestimonyPeriodTime", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}