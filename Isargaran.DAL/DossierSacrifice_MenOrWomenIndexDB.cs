﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات زن یا مرد شاخص>
    /// </summary>


    public class DossierSacrifice_MenOrWomenIndexDB
    {



        #region Methods :

        public void AddDossierSacrifice_MenOrWomenIndexDB(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam, out Guid DossierSacrifice_MenOrWomenIndexId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_MenOrWomenIndexId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@IndexFactors", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_MenOrWomenIndexEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_MenOrWomenIndexEntityParam.OccasionId;
            parameters[3].Value = (DossierSacrifice_MenOrWomenIndexEntityParam.OccasionDate==""?System.DBNull.Value:(object)DossierSacrifice_MenOrWomenIndexEntityParam.OccasionDate);
            parameters[4].Value = (DossierSacrifice_MenOrWomenIndexEntityParam.IndexFactors==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_MenOrWomenIndexEntityParam.IndexFactors));
            parameters[5].Value = (DossierSacrifice_MenOrWomenIndexEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_MenOrWomenIndexEntityParam.Description.Trim()));            

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MenOrWomenIndexAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_MenOrWomenIndexId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_MenOrWomenIndexDB(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_MenOrWomenIndexId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OccasionDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@IndexFactors", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_MenOrWomenIndexEntityParam.DossierSacrifice_MenOrWomenIndexId;
            parameters[1].Value = DossierSacrifice_MenOrWomenIndexEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_MenOrWomenIndexEntityParam.OccasionId;
            parameters[3].Value = (DossierSacrifice_MenOrWomenIndexEntityParam.OccasionDate==""?System.DBNull.Value:(object)DossierSacrifice_MenOrWomenIndexEntityParam.OccasionDate);
            parameters[4].Value = (DossierSacrifice_MenOrWomenIndexEntityParam.IndexFactors==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_MenOrWomenIndexEntityParam.IndexFactors));
            parameters[5].Value = (DossierSacrifice_MenOrWomenIndexEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_MenOrWomenIndexEntityParam.Description.Trim()));   

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MenOrWomenIndexUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_MenOrWomenIndexDB(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_MenOrWomenIndexId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_MenOrWomenIndexEntityParam.DossierSacrifice_MenOrWomenIndexId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_MenOrWomenIndexDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_MenOrWomenIndexEntity GetSingleDossierSacrifice_MenOrWomenIndexDB(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_MenOrWomenIndexId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_MenOrWomenIndexEntityParam.DossierSacrifice_MenOrWomenIndexId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MenOrWomenIndexGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_MenOrWomenIndexDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_MenOrWomenIndexEntity> GetAllDossierSacrifice_MenOrWomenIndexDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_MenOrWomenIndexDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_MenOrWomenIndexGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_MenOrWomenIndexEntity> GetPageDossierSacrifice_MenOrWomenIndexDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_MenOrWomenIndex";
            parameters[5].Value = "DossierSacrifice_MenOrWomenIndexId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_MenOrWomenIndexDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_MenOrWomenIndexEntity GetDossierSacrifice_MenOrWomenIndexDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_MenOrWomenIndexEntity(Guid.Parse(reader["DossierSacrifice_MenOrWomenIndexId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["OccasionId"].ToString()),
                                    reader["OccasionDate"].ToString(),
                                    reader["IndexFactors"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_MenOrWomenIndexEntity> GetDossierSacrifice_MenOrWomenIndexDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_MenOrWomenIndexEntity> lst = new List<DossierSacrifice_MenOrWomenIndexEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_MenOrWomenIndexDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_MenOrWomenIndexEntity> GetDossierSacrifice_MenOrWomenIndexDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_MenOrWomenIndexDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_MenOrWomenIndexEntity> GetDossierSacrifice_MenOrWomenIndexDBCollectionByOccasionDB(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_MenOrWomenIndexEntityParam.OccasionId;
            return GetDossierSacrifice_MenOrWomenIndexDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MenOrWomenIndexGetByOccasion", new[] { parameter }));
        }

        public  List<DossierSacrifice_MenOrWomenIndexEntity> GetDossierSacrifice_MenOrWomenIndexDBCollectionByDossierSacrificeDB(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_MenOrWomenIndexEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_MenOrWomenIndexDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_MenOrWomenIndexGetByDossierSacrifice", new[] { parameter }));
        }


        #endregion



    }
    
}
