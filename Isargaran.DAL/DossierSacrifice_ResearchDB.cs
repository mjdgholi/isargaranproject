﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/14>
    /// Description: <پرونده   اطلاعات پژوهشی ایثارگری>
    /// </summary>

    //-----------------------------------------------------
    #region Class "DossierSacrifice_ResearchDB"

    public class DossierSacrifice_ResearchDB
    {


        #region Methods :

        public void AddDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntity DossierSacrifice_ResearchEntityParam, out Guid DossierSacrifice_ResearchId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_ResearchId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ResearchTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@ResearchSubject", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@ApplicationsOfResearch", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_ResearchEntityParam.DossierSacrificeId;
            parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.ResearchTitle.Trim());
            parameters[3].Value = (DossierSacrifice_ResearchEntityParam.ResearchSubject==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.ResearchSubject.Trim()));
            parameters[4].Value = (DossierSacrifice_ResearchEntityParam.ApplicationsOfResearch == ""? System.DBNull.Value: (object)FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.ApplicationsOfResearch.Trim()));
            parameters[5].Value = (DossierSacrifice_ResearchEntityParam.StartDate==""?System.DBNull.Value:(object)DossierSacrifice_ResearchEntityParam.StartDate);
            parameters[6].Value = (DossierSacrifice_ResearchEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.Description.Trim()));

            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ResearchAdd", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_ResearchId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntity DossierSacrifice_ResearchEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_ResearchId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ResearchTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@ResearchSubject", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@ApplicationsOfResearch", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,

						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_ResearchEntityParam.DossierSacrifice_ResearchId;
            parameters[1].Value = DossierSacrifice_ResearchEntityParam.DossierSacrificeId;
            parameters[2].Value = FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.ResearchTitle.Trim());
            parameters[3].Value = (DossierSacrifice_ResearchEntityParam.ResearchSubject == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.ResearchSubject.Trim()));
            parameters[4].Value = (DossierSacrifice_ResearchEntityParam.ApplicationsOfResearch == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.ApplicationsOfResearch.Trim()));
            parameters[5].Value = (DossierSacrifice_ResearchEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_ResearchEntityParam.StartDate);
            parameters[6].Value = (DossierSacrifice_ResearchEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_ResearchEntityParam.Description.Trim()));


            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ResearchUpdate", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntity DossierSacrifice_ResearchEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_ResearchId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_ResearchEntityParam.DossierSacrifice_ResearchId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_ResearchDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_ResearchEntity GetSingleDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntity DossierSacrifice_ResearchEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_ResearchId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_ResearchEntityParam.DossierSacrifice_ResearchId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ResearchGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_ResearchDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ResearchEntity> GetAllDossierSacrifice_ResearchDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_ResearchDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_ResearchGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_ResearchEntity> GetPageDossierSacrifice_ResearchDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Research";
            parameters[5].Value = "DossierSacrifice_ResearchId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_ResearchDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_ResearchEntity GetDossierSacrifice_ResearchDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_ResearchEntity(Guid.Parse(reader["DossierSacrifice_ResearchId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["ResearchTitle"].ToString(),
                                    reader["ResearchSubject"].ToString(),
                                    reader["ApplicationsOfResearch"].ToString(),
                                    reader["StartDate"].ToString(),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<DossierSacrifice_ResearchEntity> GetDossierSacrifice_ResearchDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_ResearchEntity> lst = new List<DossierSacrifice_ResearchEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_ResearchDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_ResearchEntity> GetDossierSacrifice_ResearchDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_ResearchDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<DossierSacrifice_ResearchEntity> GetDossierSacrifice_ResearchDBCollectionByDossierSacrificeDB(DossierSacrifice_ResearchEntity DossierSacrifice_ResearchEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_ResearchEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_ResearchDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_ResearchGetByDossierSacrifice", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}
