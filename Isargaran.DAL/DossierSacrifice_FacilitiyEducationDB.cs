﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات خدمات و تسهیلات آموزشی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "DossierSacrifice_FacilitiyEducationDB"

    public class DossierSacrifice_FacilitiyEducationDB
    {

        #region Methods :

        public void AddDossierSacrifice_FacilitiyEducationDB(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam, out Guid DossierSacrifice_FacilitiyEducationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_FacilitiyEducationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,	
										      	  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,	
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CashAmount", SqlDbType.Decimal) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_FacilitiyEducationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilitiyEducationEntityParam.EucationDegreeId;
            parameters[3].Value = DossierSacrifice_FacilitiyEducationEntityParam.StartDate;
            parameters[4].Value = (DossierSacrifice_FacilitiyEducationEntityParam.EndDate==""?System.DBNull.Value:(object)DossierSacrifice_FacilitiyEducationEntityParam.EndDate);
            parameters[5].Value = (DossierSacrifice_FacilitiyEducationEntityParam.PersonId==null?System.DBNull.Value:(object)DossierSacrifice_FacilitiyEducationEntityParam.PersonId);
            parameters[6].Value = (DossierSacrifice_FacilitiyEducationEntityParam.CashAmount==null?System.DBNull.Value:(object)DossierSacrifice_FacilitiyEducationEntityParam.CashAmount);
            parameters[7].Value = (DossierSacrifice_FacilitiyEducationEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyEducationEntityParam.Description.Trim()));            

            parameters[8].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilitiyEducationAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_FacilitiyEducationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_FacilitiyEducationDB(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_FacilitiyEducationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StartDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@EndDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CashAmount", SqlDbType.Decimal) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_FacilitiyEducationEntityParam.DossierSacrifice_FacilitiyEducationId;
            parameters[1].Value = DossierSacrifice_FacilitiyEducationEntityParam.DossierSacrificeId;
            parameters[2].Value = DossierSacrifice_FacilitiyEducationEntityParam.EucationDegreeId;
            parameters[3].Value = DossierSacrifice_FacilitiyEducationEntityParam.StartDate;
            parameters[4].Value = (DossierSacrifice_FacilitiyEducationEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_FacilitiyEducationEntityParam.EndDate);
            parameters[5].Value = (DossierSacrifice_FacilitiyEducationEntityParam.PersonId == null? System.DBNull.Value : (object)DossierSacrifice_FacilitiyEducationEntityParam.PersonId);
            parameters[6].Value = (DossierSacrifice_FacilitiyEducationEntityParam.CashAmount == null ? System.DBNull.Value : (object)DossierSacrifice_FacilitiyEducationEntityParam.CashAmount);
            parameters[7].Value = (DossierSacrifice_FacilitiyEducationEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_FacilitiyEducationEntityParam.Description.Trim()));          

            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilitiyEducationUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_FacilitiyEducationDB(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_FacilitiyEducationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_FacilitiyEducationEntityParam.DossierSacrifice_FacilitiyEducationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_FacilitiyEducationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_FacilitiyEducationEntity GetSingleDossierSacrifice_FacilitiyEducationDB(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_FacilitiyEducationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_FacilitiyEducationEntityParam.DossierSacrifice_FacilitiyEducationId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilitiyEducationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_FacilitiyEducationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetAllDossierSacrifice_FacilitiyEducationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_FacilitiyEducationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_FacilitiyEducationGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetPageDossierSacrifice_FacilitiyEducationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_FacilitiyEducation";
            parameters[5].Value = "DossierSacrifice_FacilitiyEducationId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_FacilitiyEducationDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_FacilitiyEducationEntity GetDossierSacrifice_FacilitiyEducationDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_FacilitiyEducationEntity(Guid.Parse(reader["DossierSacrifice_FacilitiyEducationId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["EucationDegreeId"].ToString()),
                                    reader["StartDate"].ToString(),
                                    reader["EndDate"].ToString(),
                                    Convert.IsDBNull(reader["PersonId"]) ? null : (Guid?)reader["PersonId"],
                                    Convert.IsDBNull(reader["CashAmount"]) ? null : (decimal?)reader["CashAmount"],   
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                     reader["FullName"].ToString(), 
                                     Convert.IsDBNull(reader["EducationSystemId"]) ? null : (Guid?)reader["EducationSystemId"]);
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetDossierSacrifice_FacilitiyEducationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_FacilitiyEducationEntity> lst = new List<DossierSacrifice_FacilitiyEducationEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_FacilitiyEducationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetDossierSacrifice_FacilitiyEducationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_FacilitiyEducationDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetDossierSacrifice_FacilitiyEducationDBCollectionByDossierSacrificeDB(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_FacilitiyEducationEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_FacilitiyEducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilitiyEducationGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetDossierSacrifice_FacilitiyEducationDBCollectionByEducationDegreeDB(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier);
            parameter.Value = DossierSacrifice_FacilitiyEducationEntityParam.EucationDegreeId;
            return GetDossierSacrifice_FacilitiyEducationDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_FacilitiyEducationGetByEducationDegree", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}
