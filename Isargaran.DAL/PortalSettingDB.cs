﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/14>
    // Description:	<تنظیمات پورتال>
    /// </summary>

    #region Class "PortalSettingDB"

    public class PortalSettingDB
    {

        #region Methods :

        public void AddPortalSettingDB(PortalSettingEntity PortalSettingEntityParam, out Guid PortalSettingId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@PortalSettingId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@PortalSettingPersianTitle", SqlDbType.NVarChar, 150),
                new SqlParameter("@PortalSettingEnglishTitle", SqlDbType.NVarChar, 150),
                new SqlParameter("@StyleTitle", SqlDbType.NVarChar, 50),
                new SqlParameter("@TelerikSkin", SqlDbType.NVarChar, 50),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(PortalSettingEntityParam.PortalSettingPersianTitle.Trim());
            parameters[2].Value = PortalSettingEntityParam.PortalSettingEnglishTitle;
            parameters[3].Value = FarsiToArabic.ToArabic(PortalSettingEntityParam.StyleTitle.Trim());
            parameters[4].Value = FarsiToArabic.ToArabic(PortalSettingEntityParam.TelerikSkin.Trim());
            parameters[5].Value = PortalSettingEntityParam.IsActive;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_PortalSettingAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            PortalSettingId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdatePortalSettingDB(PortalSettingEntity PortalSettingEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@PortalSettingId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@PortalSettingPersianTitle", SqlDbType.NVarChar, 150),
                new SqlParameter("@PortalSettingEnglishTitle", SqlDbType.NVarChar, 150),
                new SqlParameter("@StyleTitle", SqlDbType.NVarChar, 50),
                new SqlParameter("@TelerikSkin", SqlDbType.NVarChar, 50),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = PortalSettingEntityParam.PortalSettingId;
            parameters[1].Value = FarsiToArabic.ToArabic(PortalSettingEntityParam.PortalSettingPersianTitle.Trim());
            parameters[2].Value = PortalSettingEntityParam.PortalSettingEnglishTitle;
            parameters[3].Value = FarsiToArabic.ToArabic(PortalSettingEntityParam.StyleTitle.Trim());
            parameters[4].Value = FarsiToArabic.ToArabic(PortalSettingEntityParam.TelerikSkin.Trim());
            parameters[5].Value = PortalSettingEntityParam.IsActive;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_PortalSettingUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeletePortalSettingDB(PortalSettingEntity PortalSettingEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@PortalSettingId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = PortalSettingEntityParam.PortalSettingId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_PortalSettingDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public PortalSettingEntity GetSinglePortalSettingDB(PortalSettingEntity PortalSettingEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@PortalSettingId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value = PortalSettingEntityParam.PortalSettingId;

                reader = _intranetDB.RunProcedureReader("Isar.p_PortalSettingGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetPortalSettingDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PortalSettingEntity> GetAllPortalSettingDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetPortalSettingDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_PortalSettingGetAll", new IDataParameter[] { }));
        }

        public List<PortalSettingEntity> GetAllIaActivePortalSettingDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetPortalSettingDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("Isar.p_PortalSettingGetAllIsActive", new IDataParameter[] { }));
        }

        public List<PortalSettingEntity> GetPagePortalSettingDB(int PageSize, int CurrentPage, string WhereClause,
            string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_PortalSetting";
            parameters[5].Value = "PortalSettingId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetPortalSettingDBCollectionFromDataSet(ds, out count);
        }

        public PortalSettingEntity GetPortalSettingDBFromDataReader(IDataReader reader)
        {
            return new PortalSettingEntity(new Guid(reader["PortalSettingId"].ToString()),
                reader["PortalSettingPersianTitle"].ToString(),
                reader["PortalSettingEnglishTitle"].ToString(),
                reader["StyleTitle"].ToString(),
                reader["TelerikSkin"].ToString(),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString(),
                bool.Parse(reader["IsActive"].ToString()));
        }

        public List<PortalSettingEntity> GetPortalSettingDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<PortalSettingEntity> lst = new List<PortalSettingEntity>();
                while (reader.Read())
                    lst.Add(GetPortalSettingDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PortalSettingEntity> GetPortalSettingDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetPortalSettingDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }

    #endregion
}
