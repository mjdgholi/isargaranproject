﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <نوع عضویت در بسیج>
    /// </summary>


    public class BasijiMembershipTypeDB
    {

        #region Methods :

        public void AddBasijiMembershipTypeDB(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam, out Guid BasijiMembershipTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@BasijiMembershipTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@BasijiMembershipTypeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(BasijiMembershipTypeEntityParam.BasijiMembershipTypeTitle.Trim());
            parameters[2].Value = BasijiMembershipTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_BasijiMembershipTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            BasijiMembershipTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateBasijiMembershipTypeDB(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@BasijiMembershipTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@BasijiMembershipTypeTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = BasijiMembershipTypeEntityParam.BasijiMembershipTypeId;
             parameters[1].Value = FarsiToArabic.ToArabic(BasijiMembershipTypeEntityParam.BasijiMembershipTypeTitle.Trim());
            parameters[2].Value = BasijiMembershipTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_BasijiMembershipTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteBasijiMembershipTypeDB(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@BasijiMembershipTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = BasijiMembershipTypeEntityParam.BasijiMembershipTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_BasijiMembershipTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public BasijiMembershipTypeEntity GetSingleBasijiMembershipTypeDB(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@BasijiMembershipTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = BasijiMembershipTypeEntityParam.BasijiMembershipTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_BasijiMembershipTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetBasijiMembershipTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BasijiMembershipTypeEntity> GetAllBasijiMembershipTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBasijiMembershipTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_BasijiMembershipTypeGetAll", new IDataParameter[] { }));
        }
        public List<BasijiMembershipTypeEntity> GetAllBasijiMembershipTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBasijiMembershipTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_BasijiMembershipTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<BasijiMembershipTypeEntity> GetPageBasijiMembershipTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_BasijiMembershipType";
            parameters[5].Value = "BasijiMembershipTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetBasijiMembershipTypeDBCollectionFromDataSet(ds, out count);
        }

        public BasijiMembershipTypeEntity GetBasijiMembershipTypeDBFromDataReader(IDataReader reader)
        {
            return new BasijiMembershipTypeEntity(Guid.Parse(reader["BasijiMembershipTypeId"].ToString()),
                                    reader["BasijiMembershipTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<BasijiMembershipTypeEntity> GetBasijiMembershipTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<BasijiMembershipTypeEntity> lst = new List<BasijiMembershipTypeEntity>();
                while (reader.Read())
                    lst.Add(GetBasijiMembershipTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BasijiMembershipTypeEntity> GetBasijiMembershipTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetBasijiMembershipTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

}
