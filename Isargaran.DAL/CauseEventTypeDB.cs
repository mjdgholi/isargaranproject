﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/02>
    /// Description: <نوع حادثه>
    /// </summary>

    #region Class "CauseEventTypeDB"

    public class CauseEventTypeDB
    {
        #region Methods :

        public void AddCauseEventTypeDB(CauseEventTypeEntity causeEventTypeEntityParam, out Guid CauseEventTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@CauseEventTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@CauseEventTypeTitle", SqlDbType.NVarChar,150) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(causeEventTypeEntityParam.CauseEventTypeTitle);
            parameters[2].Value = causeEventTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_CauseEventTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            CauseEventTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateCauseEventTypeDB(CauseEventTypeEntity causeEventTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@CauseEventTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@CauseEventTypeTitle", SqlDbType.NVarChar,150) ,						
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = causeEventTypeEntityParam.CauseEventTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(causeEventTypeEntityParam.CauseEventTypeTitle);
            parameters[2].Value = causeEventTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_CauseEventTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteCauseEventTypeDB(CauseEventTypeEntity CauseEventTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@CauseEventTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = CauseEventTypeEntityParam.CauseEventTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_CauseEventTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public CauseEventTypeEntity GetSingleCauseEventTypeDB(CauseEventTypeEntity CauseEventTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@CauseEventTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = CauseEventTypeEntityParam.CauseEventTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_CauseEventTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetCauseEventTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CauseEventTypeEntity> GetAllCauseEventTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCauseEventTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_CauseEventTypeGetAll", new IDataParameter[] { }));
        }

        public List<CauseEventTypeEntity> GetAllCauseEventTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCauseEventTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_CauseEventTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<CauseEventTypeEntity> GetPageCauseEventTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_CauseEventType";
            parameters[5].Value = "CauseEventTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetCauseEventTypeDBCollectionFromDataSet(ds, out count);
        }

        public CauseEventTypeEntity GetCauseEventTypeDBFromDataReader(IDataReader reader)
        {
            return new CauseEventTypeEntity(Guid.Parse(reader["CauseEventTypeId"].ToString()),
                                    reader["CauseEventTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<CauseEventTypeEntity> GetCauseEventTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<CauseEventTypeEntity> lst = new List<CauseEventTypeEntity>();
                while (reader.Read())
                    lst.Add(GetCauseEventTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CauseEventTypeEntity> GetCauseEventTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetCauseEventTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}