﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Intranet.Security;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/03/11>
    /// Description: <دسته بندی فرم>
    /// </summary>

    #region Class "WebFormCategorizeDB"

    public class WebFormCategorizeDB
    {
        #region Methods :

        public void AddWebFormCategorizeDB(WebFormCategorizeEntity WebFormCategorizeEntityParam, out Guid WebFormCategorizeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@WebFormCategorizeTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@Priority", SqlDbType.Int),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(WebFormCategorizeEntityParam.WebFormCategorizeTitle.Trim());
            parameters[2].Value = (WebFormCategorizeEntityParam.Priority == null ? System.DBNull.Value : (object) WebFormCategorizeEntityParam.Priority);
            parameters[3].Value = WebFormCategorizeEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_WebFormCategorizeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            WebFormCategorizeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateWebFormCategorizeDB(WebFormCategorizeEntity WebFormCategorizeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@WebFormCategorizeTitle", SqlDbType.NVarChar, 200),
                new SqlParameter("@Priority", SqlDbType.Int),
                new SqlParameter("@IsActive", SqlDbType.Bit),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = WebFormCategorizeEntityParam.WebFormCategorizeId;
            parameters[1].Value = FarsiToArabic.ToArabic(WebFormCategorizeEntityParam.WebFormCategorizeTitle.Trim());
            parameters[2].Value = (WebFormCategorizeEntityParam.Priority == null ? System.DBNull.Value : (object) WebFormCategorizeEntityParam.Priority);
            parameters[3].Value = WebFormCategorizeEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_WebFormCategorizeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteWebFormCategorizeDB(WebFormCategorizeEntity WebFormCategorizeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = WebFormCategorizeEntityParam.WebFormCategorizeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_WebFormCategorizeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public WebFormCategorizeEntity GetSingleWebFormCategorizeDB(WebFormCategorizeEntity WebFormCategorizeEntityParam)
        {
            IDataReader reader = null;
            try
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@WebFormCategorizeId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value = WebFormCategorizeEntityParam.WebFormCategorizeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_WebFormCategorizeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetWebFormCategorizeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WebFormCategorizeEntity> GetAllWebFormCategorizeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWebFormCategorizeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_WebFormCategorizeGetAll", new IDataParameter[] {}));
        }

        public List<WebFormCategorizeEntity> GetAllIsActiveWebFormCategorizeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWebFormCategorizeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_WebFormCategorizeGetAllIsActive", new IDataParameter[] {}));
        }

        public List<WebFormCategorizeEntity> GetPageWebFormCategorizeDB(int PageSize, int CurrentPage,
            string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "Isar.t_WebFormCategorize";
            parameters[5].Value = "WebFormCategorizeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetWebFormCategorizeDBCollectionFromDataSet(ds, out count);
        }

        public WebFormCategorizeEntity GetWebFormCategorizeDBFromDataReader(IDataReader reader)
        {
            return new WebFormCategorizeEntity(new Guid(reader["WebFormCategorizeId"].ToString()),
                reader["WebFormCategorizeTitle"].ToString(),
                Convert.IsDBNull(reader["Priority"]) ? (int?) null : int.Parse(reader["Priority"].ToString()),
                reader["CreationDate"].ToString(),
                reader["ModificationDate"].ToString(),
                bool.Parse(reader["IsActive"].ToString()));
        }

        public List<WebFormCategorizeEntity> GetWebFormCategorizeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<WebFormCategorizeEntity> lst = new List<WebFormCategorizeEntity>();
                while (reader.Read())
                    lst.Add(GetWebFormCategorizeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WebFormCategorizeEntity> GetWebFormCategorizeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetWebFormCategorizeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<WebFormCategorizeEntity> GetAllWebformCategorizePerRowAccess(string objectTitleStr, int userId)
        {

            IntranetDB intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                 new SqlParameter("@WebUserId", SqlDbType.Int),
                new SqlParameter("@ObjectTitlesForRowAccess_Str", SqlDbType.NVarChar, -1)
               
            };
            parameters[0].Value = userId;
            parameters[1].Value = objectTitleStr;
            var reader = intranetDB.RunProcedureReader("Isar.p_WebFormCategorizeGetAllPerRowAccess", parameters);

            return GetWebFormCategorizeDBCollectionFromDataReader(reader);

        }

        #endregion

    }

    #endregion
}