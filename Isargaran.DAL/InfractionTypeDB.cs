﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/03/12>
    // Description:	<نوع تخلف>
    /// </summary>
    
    public class InfractionTypeDB
    {
        #region Methods :

        public void AddInfractionTypeDB(InfractionTypeEntity infractionTypeEntityParam, out Guid InfractionTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@InfractionTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@InfractionTypeTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(infractionTypeEntityParam.InfractionTypeTitle.Trim());
            parameters[2].Value = infractionTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_InfractionTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            InfractionTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateInfractionTypeDB(InfractionTypeEntity infractionTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@InfractionTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@InfractionTypeTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = infractionTypeEntityParam.InfractionTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(infractionTypeEntityParam.InfractionTypeTitle.Trim());
            parameters[2].Value = infractionTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_InfractionTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteInfractionTypeDB(InfractionTypeEntity InfractionTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@InfractionTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = InfractionTypeEntityParam.InfractionTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_InfractionTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public InfractionTypeEntity GetSingleInfractionTypeDB(InfractionTypeEntity InfractionTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@InfractionTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = InfractionTypeEntityParam.InfractionTypeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_InfractionTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetInfractionTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<InfractionTypeEntity> GetAllInfractionTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetInfractionTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_InfractionTypeGetAll", new IDataParameter[] { }));
        }
        public List<InfractionTypeEntity> GetAllInfractionTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetInfractionTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_InfractionTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<InfractionTypeEntity> GetPageInfractionTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_InfractionType";
            parameters[5].Value = "InfractionTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetInfractionTypeDBCollectionFromDataSet(ds, out count);
        }

        public InfractionTypeEntity GetInfractionTypeDBFromDataReader(IDataReader reader)
        {
            return new InfractionTypeEntity(Guid.Parse(reader["InfractionTypeId"].ToString()),
                                    reader["InfractionTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<InfractionTypeEntity> GetInfractionTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<InfractionTypeEntity> lst = new List<InfractionTypeEntity>();
                while (reader.Read())
                    lst.Add(GetInfractionTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<InfractionTypeEntity> GetInfractionTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetInfractionTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

}
