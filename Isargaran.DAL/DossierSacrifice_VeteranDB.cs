﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/01/30>
    // Description:	<اطلاعات جانبازی>
    /// </summary>


    public class DossierSacrifice_VeteranDB
    {

 
        #region Methods :

        public void AddDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam, out Guid DossierSacrifice_VeteranId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_VeteranId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VeteranCod", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@WarZoneId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VeteranDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@VeteranBodyRegionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VeteranPercent", SqlDbType.Decimal) ,
											  new SqlParameter("@DurationOfPresenceInBattlefield", SqlDbType.Int) ,
											  new SqlParameter("@VeteranFoundationDossierNo", SqlDbType.NChar,10) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@RehabilitationEquipment", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@PreVeteranEucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PostId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@SacrificePeriodTimeId", SqlDbType.UniqueIdentifier) ,	
										                         new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = DossierSacrifice_VeteranEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_VeteranEntityParam.VeteranCod==""?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.VeteranCod);
            parameters[3].Value = (DossierSacrifice_VeteranEntityParam.WarZoneId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.WarZoneId);
            parameters[4].Value = (DossierSacrifice_VeteranEntityParam.VeteranDate==""?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.VeteranDate);
            parameters[5].Value = (DossierSacrifice_VeteranEntityParam.VeteranBodyRegionId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.VeteranBodyRegionId);
            parameters[6].Value = (DossierSacrifice_VeteranEntityParam.VeteranPercent.ToString()==""?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.VeteranPercent);
            parameters[7].Value = (DossierSacrifice_VeteranEntityParam.DurationOfPresenceInBattlefield.ToString()==""?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.DurationOfPresenceInBattlefield);
            parameters[8].Value = (DossierSacrifice_VeteranEntityParam.VeteranFoundationDossierNo==""?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.VeteranFoundationDossierNo);
            parameters[9].Value = (DossierSacrifice_VeteranEntityParam.CityId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.CityId);
            parameters[10].Value = (DossierSacrifice_VeteranEntityParam.RehabilitationEquipment==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_VeteranEntityParam.RehabilitationEquipment.Trim()));
            parameters[11].Value = (DossierSacrifice_VeteranEntityParam.PreVeteranEucationDegreeId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.PreVeteranEucationDegreeId);
            parameters[12].Value = (DossierSacrifice_VeteranEntityParam.EucationDegreeId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.EucationDegreeId);
            parameters[13].Value = (DossierSacrifice_VeteranEntityParam.OrganizationPhysicalChartId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.OrganizationPhysicalChartId);
            parameters[14].Value = (DossierSacrifice_VeteranEntityParam.MilitaryUnitDispatcherId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.MilitaryUnitDispatcherId);
            parameters[15].Value = (DossierSacrifice_VeteranEntityParam.PostId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.PostId);
            parameters[16].Value = (DossierSacrifice_VeteranEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(DossierSacrifice_VeteranEntityParam.Description.Trim()));
            parameters[17].Value = DossierSacrifice_VeteranEntityParam.SacrificePeriodTimeId;
            parameters[18].Value = (DossierSacrifice_VeteranEntityParam.StartDate==""?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.StartDate);
            parameters[19].Value = (DossierSacrifice_VeteranEntityParam.EndDate==""?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.EndDate);
            parameters[20].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_VeteranAdd", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_VeteranId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_VeteranId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VeteranCod", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@WarZoneId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VeteranDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@VeteranBodyRegionId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@VeteranPercent", SqlDbType.Decimal) ,
											  new SqlParameter("@DurationOfPresenceInBattlefield", SqlDbType.Int) ,
											  new SqlParameter("@VeteranFoundationDossierNo", SqlDbType.NChar,10) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@RehabilitationEquipment", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@PreVeteranEucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EucationDegreeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@MilitaryUnitDispatcherId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PostId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@SacrificePeriodTimeId", SqlDbType.UniqueIdentifier) ,
											new SqlParameter("@StartDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@EndDate", SqlDbType.SmallDateTime),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DossierSacrifice_VeteranEntityParam.DossierSacrifice_VeteranId;
            parameters[1].Value = DossierSacrifice_VeteranEntityParam.DossierSacrificeId;
            parameters[2].Value = (DossierSacrifice_VeteranEntityParam.VeteranCod == "" ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.VeteranCod);
            parameters[3].Value = (DossierSacrifice_VeteranEntityParam.WarZoneId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.WarZoneId);
            parameters[4].Value = (DossierSacrifice_VeteranEntityParam.VeteranDate == "" ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.VeteranDate);
            parameters[5].Value = (DossierSacrifice_VeteranEntityParam.VeteranBodyRegionId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.VeteranBodyRegionId);
            parameters[6].Value = (DossierSacrifice_VeteranEntityParam.VeteranPercent.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.VeteranPercent);
            parameters[7].Value = (DossierSacrifice_VeteranEntityParam.DurationOfPresenceInBattlefield.ToString() == "" ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.DurationOfPresenceInBattlefield);
            parameters[8].Value = (DossierSacrifice_VeteranEntityParam.VeteranFoundationDossierNo == "" ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.VeteranFoundationDossierNo);
            parameters[9].Value = (DossierSacrifice_VeteranEntityParam.CityId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.CityId);
            parameters[10].Value = (DossierSacrifice_VeteranEntityParam.RehabilitationEquipment == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_VeteranEntityParam.RehabilitationEquipment.Trim()));
            parameters[11].Value = (DossierSacrifice_VeteranEntityParam.PreVeteranEucationDegreeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.PreVeteranEucationDegreeId);
            parameters[12].Value = (DossierSacrifice_VeteranEntityParam.EucationDegreeId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.EucationDegreeId);
            parameters[13].Value = (DossierSacrifice_VeteranEntityParam.OrganizationPhysicalChartId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.OrganizationPhysicalChartId);
            parameters[14].Value = (DossierSacrifice_VeteranEntityParam.MilitaryUnitDispatcherId==new Guid()?System.DBNull.Value:(object)DossierSacrifice_VeteranEntityParam.MilitaryUnitDispatcherId);
            parameters[15].Value = (DossierSacrifice_VeteranEntityParam.PostId == new Guid() ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.PostId);
            parameters[16].Value = (DossierSacrifice_VeteranEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(DossierSacrifice_VeteranEntityParam.Description.Trim()));
            parameters[17].Value = DossierSacrifice_VeteranEntityParam.SacrificePeriodTimeId;
            parameters[18].Value = (DossierSacrifice_VeteranEntityParam.StartDate == "" ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.StartDate);
            parameters[19].Value = (DossierSacrifice_VeteranEntityParam.EndDate == "" ? System.DBNull.Value : (object)DossierSacrifice_VeteranEntityParam.EndDate);

            parameters[20].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_VeteranUpdate", parameters);
            var messageError = parameters[20].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_VeteranId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_VeteranEntityParam.DossierSacrifice_VeteranId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DossierSacrifice_VeteranDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_VeteranEntity GetSingleDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_VeteranId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_VeteranEntityParam.DossierSacrifice_VeteranId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VeteranGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_VeteranDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_VeteranEntity> GetAllDossierSacrifice_VeteranDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_VeteranDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DossierSacrifice_VeteranGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_VeteranEntity> GetPageDossierSacrifice_VeteranDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Veteran";
            parameters[5].Value = "DossierSacrifice_VeteranId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_VeteranDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_VeteranEntity GetDossierSacrifice_VeteranDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_VeteranEntity(Guid.Parse(reader["DossierSacrifice_VeteranId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    reader["VeteranCod"].ToString(),
                                     Convert.IsDBNull(reader["WarZoneId"]) ? null : (Guid?)reader["WarZoneId"],                                       
                                    reader["VeteranDate"].ToString(),
                                     Convert.IsDBNull(reader["VeteranBodyRegionId"]) ? null : (Guid?)reader["VeteranBodyRegionId"],     
                                    Convert.IsDBNull(reader["VeteranPercent"]) ? null : (decimal?)reader["VeteranPercent"],                                         
                                    Convert.IsDBNull(reader["DurationOfPresenceInBattlefield"]) ? null : (int?)reader["DurationOfPresenceInBattlefield"],   
                                    reader["VeteranFoundationDossierNo"].ToString(),
                                    Convert.IsDBNull(reader["CityId"]) ? null : (Guid?)reader["CityId"],                                         
                                    reader["RehabilitationEquipment"].ToString(),
                                    Convert.IsDBNull(reader["PreVeteranEucationDegreeId"]) ? null : (Guid?)reader["PreVeteranEucationDegreeId"],    
                                    Convert.IsDBNull(reader["EucationDegreeId"]) ? null : (Guid?)reader["EucationDegreeId"],                                     
                                    Convert.IsDBNull(reader["OrganizationPhysicalChartId"]) ? null : (Guid?)reader["OrganizationPhysicalChartId"],
                                    Convert.IsDBNull(reader["MilitaryUnitDispatcherId"]) ? null : (Guid?)reader["MilitaryUnitDispatcherId"],                                                                      
                                    Convert.IsDBNull(reader["PostId"]) ? null : (Guid?)reader["PostId"],                                     
                                    reader["Description"].ToString(),
                                    Guid.Parse(reader["SacrificePeriodTimeId"].ToString()),
                                    reader["StartDate"].ToString(),
                                   reader["EndDate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                     Convert.IsDBNull(reader["ProvinceId"]) ? null : (Guid?)reader["ProvinceId"],
                                    reader["OrganizationPhysicalChartTitle"].ToString());
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_VeteranEntity> lst = new List<DossierSacrifice_VeteranEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_VeteranDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_VeteranDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranDBCollectionByDossierSacrificeDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_VeteranEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_VeteranDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VeteranGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranDBCollectionByEducationDegreeDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PreVeteranEucationDegreeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_VeteranEntityParam.PreVeteranEucationDegreeId;
            return GetDossierSacrifice_VeteranDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VeteranGetByEducationDegree", new[] { parameter }));
        }


        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranDBCollectionBySacrificePeriodTimeDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SacrificePeriodTimeId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_VeteranEntityParam.SacrificePeriodTimeId;
            return GetDossierSacrifice_VeteranDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VeteranGetBySacrificePeriodTime", new[] { parameter }));
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranDBCollectionByVeteranBodyRegionDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@VeteranBodyRegionId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_VeteranEntityParam.VeteranBodyRegionId;
            return GetDossierSacrifice_VeteranDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VeteranGetByVeteranBodyRegion", new[] { parameter }));
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranDBCollectionByWarZoneDB(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@WarZoneId", SqlDbType.Int);
            parameter.Value = DossierSacrifice_VeteranEntityParam.WarZoneId;
            return GetDossierSacrifice_VeteranDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_DossierSacrifice_VeteranGetByWarZone", new[] { parameter }));
        }


        #endregion



    }


}
