﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <زبان خارجی>
    /// </summary>
    #region Class "ForeignLanguageDB"

    public class ForeignLanguageDB
    {
        #region Methods :

        public void AddForeignLanguageDB(ForeignLanguageEntity ForeignLanguageEntityParam, out Guid ForeignLanguageId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ForeignLanguagePersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@ForeignLanguageEnglishTitle", SqlDbType.NVarChar, 200),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(ForeignLanguageEntityParam.ForeignLanguagePersianTitle.Trim());
            parameters[2].Value = ForeignLanguageEntityParam.ForeignLanguageEnglishTitle;           
            parameters[3].Value = ForeignLanguageEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[Isar].p_ForeignLanguageAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ForeignLanguageId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateForeignLanguageDB(ForeignLanguageEntity ForeignLanguageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@ForeignLanguagePersianTitle", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@ForeignLanguageEnglishTitle", SqlDbType.NVarChar, 200),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = ForeignLanguageEntityParam.ForeignLanguageId;
            parameters[1].Value = FarsiToArabic.ToArabic(ForeignLanguageEntityParam.ForeignLanguagePersianTitle.Trim());
            parameters[2].Value = ForeignLanguageEntityParam.ForeignLanguageEnglishTitle;
            parameters[3].Value = ForeignLanguageEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;           

            _intranetDB.RunProcedure("[Isar].p_ForeignLanguageUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteForeignLanguageDB(ForeignLanguageEntity ForeignLanguageEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@ForeignLanguageId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = ForeignLanguageEntityParam.ForeignLanguageId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Isar].p_ForeignLanguageDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ForeignLanguageEntity GetSingleForeignLanguageDB(ForeignLanguageEntity ForeignLanguageEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@ForeignLanguageId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = ForeignLanguageEntityParam.ForeignLanguageId;

                reader = _intranetDB.RunProcedureReader("[Isar].p_ForeignLanguageGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetForeignLanguageDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ForeignLanguageEntity> GetAllForeignLanguageDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetForeignLanguageDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_ForeignLanguageGetAll", new IDataParameter[] {}));
        }

        public List<ForeignLanguageEntity> GetAllIsActiveForeignLanguageDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetForeignLanguageDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("[Isar].p_ForeignLanguageGetAlIsActive", new IDataParameter[] { }));
        }

        public List<ForeignLanguageEntity> GetPageForeignLanguageDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "[Isar].t_ForeignLanguage";
            parameters[5].Value = "ForeignLanguageId";
            DataSet ds = _intranetDB.RunProcedureDS("[Isar].p_TablesGetPage", parameters);
            return GetForeignLanguageDBCollectionFromDataSet(ds, out count);
        }

        public ForeignLanguageEntity GetForeignLanguageDBFromDataReader(IDataReader reader)
        {
            return new ForeignLanguageEntity(new Guid(reader["ForeignLanguageId"].ToString()),
                                             reader["ForeignLanguagePersianTitle"].ToString(),
                                             reader["ForeignLanguageEnglishTitle"].ToString(),
                                             reader["CreationDate"].ToString(),
                                             reader["ModificationDate"].ToString(),
                                             bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ForeignLanguageEntity> GetForeignLanguageDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ForeignLanguageEntity> lst = new List<ForeignLanguageEntity>();
                while (reader.Read())
                    lst.Add(GetForeignLanguageDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ForeignLanguageEntity> GetForeignLanguageDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetForeignLanguageDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}