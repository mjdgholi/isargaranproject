﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <رسته بسیجی>
    /// </summary>
    public class BasijiCategoryDB
    {

        #region Methods :

        public void AddBasijiCategoryDB(BasijiCategoryEntity BasijiCategoryEntityParam, out Guid BasijiCategoryId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@BasijiCategoryId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@BasijiCategoryTitle", SqlDbType.NVarChar,300) ,	
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(BasijiCategoryEntityParam.BasijiCategoryTitle.Trim());
            parameters[2].Value = BasijiCategoryEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_BasijiCategoryAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            BasijiCategoryId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateBasijiCategoryDB(BasijiCategoryEntity BasijiCategoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@BasijiCategoryId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@BasijiCategoryTitle", SqlDbType.NVarChar,300) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = BasijiCategoryEntityParam.BasijiCategoryId;
            parameters[1].Value = FarsiToArabic.ToArabic(BasijiCategoryEntityParam.BasijiCategoryTitle.Trim());
            parameters[2].Value = BasijiCategoryEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_BasijiCategoryUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteBasijiCategoryDB(BasijiCategoryEntity BasijiCategoryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@BasijiCategoryId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = BasijiCategoryEntityParam.BasijiCategoryId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_BasijiCategoryDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public BasijiCategoryEntity GetSingleBasijiCategoryDB(BasijiCategoryEntity BasijiCategoryEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@BasijiCategoryId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = BasijiCategoryEntityParam.BasijiCategoryId;

                reader = _intranetDB.RunProcedureReader("Isar.p_BasijiCategoryGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetBasijiCategoryDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BasijiCategoryEntity> GetAllBasijiCategoryDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBasijiCategoryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_BasijiCategoryGetAll", new IDataParameter[] { }));
        }
                public List<BasijiCategoryEntity> GetAllBasijiCategoryIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBasijiCategoryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_BasijiCategoryIsActiveGetAll", new IDataParameter[] { }));
        }
        
        public List<BasijiCategoryEntity> GetPageBasijiCategoryDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_BasijiCategory";
            parameters[5].Value = "BasijiCategoryId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetBasijiCategoryDBCollectionFromDataSet(ds, out count);
        }

        public BasijiCategoryEntity GetBasijiCategoryDBFromDataReader(IDataReader reader)
        {
            return new BasijiCategoryEntity(Guid.Parse(reader["BasijiCategoryId"].ToString()),
                                    reader["BasijiCategoryTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<BasijiCategoryEntity> GetBasijiCategoryDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<BasijiCategoryEntity> lst = new List<BasijiCategoryEntity>();
                while (reader.Read())
                    lst.Add(GetBasijiCategoryDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BasijiCategoryEntity> GetBasijiCategoryDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetBasijiCategoryDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }
}
