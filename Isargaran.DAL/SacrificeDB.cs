﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <نوع ایثارگری>
    /// </summary>


    public class SacrificeDB
    {

        #region Methods :

        public void AddSacrificeDB(SacrificeEntity SacrificeEntityParam, out Guid SacrificeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SacrificeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SacrificeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@SacrificeTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsPercentValue", SqlDbType.Bit) ,
											  new SqlParameter("@IsTimeValue", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(SacrificeEntityParam.SacrificeTitle.Trim());
            parameters[2].Value = SacrificeEntityParam.SacrificeTypeId;
            parameters[3].Value = SacrificeEntityParam.IsPercentValue;
            parameters[4].Value = SacrificeEntityParam.IsTimeValue;
            parameters[5].Value = SacrificeEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_SacrificeAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SacrificeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSacrificeDB(SacrificeEntity SacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SacrificeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@SacrificeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@SacrificeTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@IsPercentValue", SqlDbType.Bit) ,
											  new SqlParameter("@IsTimeValue", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = SacrificeEntityParam.SacrificeId;
            parameters[1].Value = FarsiToArabic.ToArabic(SacrificeEntityParam.SacrificeTitle.Trim());
            parameters[2].Value = SacrificeEntityParam.SacrificeTypeId;
            parameters[3].Value = SacrificeEntityParam.IsPercentValue;
            parameters[4].Value = SacrificeEntityParam.IsTimeValue;
            parameters[5].Value = SacrificeEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_SacrificeUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSacrificeDB(SacrificeEntity SacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SacrificeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SacrificeEntityParam.SacrificeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_SacrificeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SacrificeEntity GetSingleSacrificeDB(SacrificeEntity SacrificeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@SacrificeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = SacrificeEntityParam.SacrificeId;

                reader = _intranetDB.RunProcedureReader("Isar.p_SacrificeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSacrificeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SacrificeEntity> GetAllSacrificeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSacrificeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_SacrificeGetAll", new IDataParameter[] { }));
        }
        public List<SacrificeEntity> GetAllSacrificeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSacrificeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_SacrificeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<SacrificeEntity> GetPageSacrificeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Sacrifice";
            parameters[5].Value = "SacrificeId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetSacrificeDBCollectionFromDataSet(ds, out count);
        }

        public SacrificeEntity GetSacrificeDBFromDataReader(IDataReader reader)
        {
            return new SacrificeEntity(Guid.Parse(reader["SacrificeId"].ToString()),
                                    reader["SacrificeTitle"].ToString(),
                                    Guid.Parse(reader["SacrificeTypeId"].ToString()),
                                    bool.Parse(reader["IsPercentValue"].ToString()),
                                    bool.Parse(reader["IsTimeValue"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SacrificeEntity> GetSacrificeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SacrificeEntity> lst = new List<SacrificeEntity>();
                while (reader.Read())
                    lst.Add(GetSacrificeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SacrificeEntity> GetSacrificeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSacrificeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<SacrificeEntity> GetSacrificeDBCollectionBySacrificeTypeDB(SacrificeEntity SacrificeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SacrificeTypeId", SqlDbType.Int);
            parameter.Value = SacrificeEntityParam.SacrificeTypeId;
            return GetSacrificeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Isar.p_SacrificeGetBySacrificeType", new[] { parameter }));
        }


        #endregion



    }

}
