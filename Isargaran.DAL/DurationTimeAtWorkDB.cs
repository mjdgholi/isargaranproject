﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ITC.Library.Classes;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <میزان حضور در سرکار>
    /// </summary>

    //-----------------------------------------------------
    #region Class "DurationTimeAtWorkDB"

    public class DurationTimeAtWorkDB
    {



        #region Methods :

        public void AddDurationTimeAtWorkDB(DurationTimeAtWorkEntity DurationTimeAtWorkEntityParam, out Guid DurationTimeAtWorkId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DurationTimeAtWorkTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(DurationTimeAtWorkEntityParam.DurationTimeAtWorkTitle.Trim());
            parameters[2].Value = DurationTimeAtWorkEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DurationTimeAtWorkAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DurationTimeAtWorkId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDurationTimeAtWorkDB(DurationTimeAtWorkEntity DurationTimeAtWorkEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DurationTimeAtWorkTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DurationTimeAtWorkEntityParam.DurationTimeAtWorkId;
            parameters[1].Value = FarsiToArabic.ToArabic(DurationTimeAtWorkEntityParam.DurationTimeAtWorkTitle.Trim());
            parameters[2].Value = DurationTimeAtWorkEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Isar.p_DurationTimeAtWorkUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDurationTimeAtWorkDB(DurationTimeAtWorkEntity DurationTimeAtWorkEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DurationTimeAtWorkEntityParam.DurationTimeAtWorkId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Isar.p_DurationTimeAtWorkDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DurationTimeAtWorkEntity GetSingleDurationTimeAtWorkDB(DurationTimeAtWorkEntity DurationTimeAtWorkEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DurationTimeAtWorkId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DurationTimeAtWorkEntityParam.DurationTimeAtWorkId;

                reader = _intranetDB.RunProcedureReader("Isar.p_DurationTimeAtWorkGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDurationTimeAtWorkDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DurationTimeAtWorkEntity> GetAllDurationTimeAtWorkDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDurationTimeAtWorkDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DurationTimeAtWorkGetAll", new IDataParameter[] { }));
        }

        public List<DurationTimeAtWorkEntity> GetAllDurationTimeAtWorkIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDurationTimeAtWorkDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Isar.p_DurationTimeAtWorkIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<DurationTimeAtWorkEntity> GetPageDurationTimeAtWorkDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DurationTimeAtWork";
            parameters[5].Value = "DurationTimeAtWorkId";
            DataSet ds = _intranetDB.RunProcedureDS("Isar.p_TablesGetPage", parameters);
            return GetDurationTimeAtWorkDBCollectionFromDataSet(ds, out count);
        }

        public DurationTimeAtWorkEntity GetDurationTimeAtWorkDBFromDataReader(IDataReader reader)
        {
            return new DurationTimeAtWorkEntity(Guid.Parse(reader["DurationTimeAtWorkId"].ToString()),
                                    reader["DurationTimeAtWorkTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DurationTimeAtWorkEntity> GetDurationTimeAtWorkDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DurationTimeAtWorkEntity> lst = new List<DurationTimeAtWorkEntity>();
                while (reader.Read())
                    lst.Add(GetDurationTimeAtWorkDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DurationTimeAtWorkEntity> GetDurationTimeAtWorkDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDurationTimeAtWorkDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
