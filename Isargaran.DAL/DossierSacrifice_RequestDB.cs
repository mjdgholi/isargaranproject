﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <پرونده اطلاعات درخواست ایثارگر>
    /// </summary>

    public class DossierSacrifice_RequestDB
    {

   

        #region Methods :

        public void AddDossierSacrifice_RequestDB(DossierSacrifice_RequestEntity dossierSacrificeRequestEntityParam, out Guid DossierSacrifice_RequestId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@RequestTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ImportanceTypeId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@RequestDescription", SqlDbType.NVarChar,-1) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = dossierSacrificeRequestEntityParam.DossierSacrificeId;
            parameters[2].Value = dossierSacrificeRequestEntityParam.RequestTypeId;
            parameters[3].Value = dossierSacrificeRequestEntityParam.ImportanceTypeId;            
            parameters[4].Value = (dossierSacrificeRequestEntityParam.RequestDescription==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(dossierSacrificeRequestEntityParam.RequestDescription));            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_DossierSacrifice_RequestAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_RequestId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_RequestDB(DossierSacrifice_RequestEntity dossierSacrificeRequestEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@RequestTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ImportanceTypeId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@RequestDescription", SqlDbType.NVarChar,-1) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = dossierSacrificeRequestEntityParam.DossierSacrifice_RequestId;
            parameters[1].Value = dossierSacrificeRequestEntityParam.DossierSacrificeId;
            parameters[2].Value = dossierSacrificeRequestEntityParam.RequestTypeId;
            parameters[3].Value = dossierSacrificeRequestEntityParam.ImportanceTypeId;            
            parameters[4].Value = (dossierSacrificeRequestEntityParam.RequestDescription == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(dossierSacrificeRequestEntityParam.RequestDescription)); 

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_DossierSacrifice_RequestUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_RequestDB(DossierSacrifice_RequestEntity DossierSacrifice_RequestEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_RequestEntityParam.DossierSacrifice_RequestId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_DossierSacrifice_RequestDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_RequestEntity GetSingleDossierSacrifice_RequestDB(DossierSacrifice_RequestEntity DossierSacrifice_RequestEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_RequestEntityParam.DossierSacrifice_RequestId;

                reader = _intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_RequestDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_RequestEntity> GetAllDossierSacrifice_RequestDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_RequestDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_DossierSacrifice_RequestGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_RequestEntity> GetPageDossierSacrifice_RequestDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Request";
            parameters[5].Value = "DossierSacrifice_RequestId";
            DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_RequestDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_RequestEntity GetDossierSacrifice_RequestDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_RequestEntity(Guid.Parse(reader["DossierSacrifice_RequestId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["RequestTypeId"].ToString()),
                                    Guid.Parse(reader["ImportanceTypeId"].ToString()),                                    
                                    reader["RequestDescription"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["DossierSacrificeNo"].ToString(),
                                    Convert.IsDBNull(reader["PersonId"]) ? null : (Guid?)reader["PersonId"],                                       
                                    reader["FullName"].ToString());
        }

        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_RequestEntity> lst = new List<DossierSacrifice_RequestEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_RequestDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_RequestDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestDBCollectionByDossierSacrificeDB(DossierSacrifice_RequestEntity dossierSacrificeRequestEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeRequestEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_RequestDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestGetByDossierSacrifice", new[] { parameter }));
        }

        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestDBCollectionByImportanceTypeDB(DossierSacrifice_RequestEntity dossierSacrificeRequestEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ImportanceTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeRequestEntityParam.ImportanceTypeId;
            return GetDossierSacrifice_RequestDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestGetByImportanceType", new[] { parameter }));
        }

        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestDBCollectionByRequestTypeDB(DossierSacrifice_RequestEntity dossierSacrificeRequestEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@RequestTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeRequestEntityParam.RequestTypeId;
            return GetDossierSacrifice_RequestDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestGetByRequestType", new[] { parameter }));
        }

        //public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestDBCollectionByStatusDB(DossierSacrifice_RequestEntity dossierSacrificeRequestEntityParam)
        //{
        //    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
        //    IDataParameter parameter = new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier);
        //    parameter.Value = dossierSacrificeRequestEntityParam.StatusId;
        //    return GetDossierSacrifice_RequestDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_RequestGetByStatus", new[] { parameter }));
        //}

        public void UpdateIsViewDB(DossierSacrifice_RequestEntity dossierSacrificeRequestEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_RequestId", SqlDbType.UniqueIdentifier),						 
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = dossierSacrificeRequestEntityParam.DossierSacrifice_RequestId;            
            parameters[1].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_DossierSacrifice_RequestIsViewUpdate", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }
        #endregion

        
    }


}