﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/17>
    /// Description: <پرونده مشخصات وابستگان ایثارگری>
    /// </summary> 

    //-----------------------------------------------------
    #region Class "DossierSacrifice_DependentDB"

    public class DossierSacrifice_DependentDB
    {



        #region Methods :

        public void AddDossierSacrifice_DependentDB(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam, out Guid DossierSacrifice_DependentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_DependentId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FromDate", SqlDbType.SmallDateTime) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = dossierSacrificeDependentEntityParam.DossierSacrificeId;
            parameters[2].Value = dossierSacrificeDependentEntityParam.PersonId;
            parameters[3].Value = dossierSacrificeDependentEntityParam.DependentTypeId;
            parameters[4].Value = dossierSacrificeDependentEntityParam.FromDate;

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_DossierSacrifice_DependentAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DossierSacrifice_DependentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDossierSacrifice_DependentDB(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DossierSacrifice_DependentId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@FromDate", SqlDbType.SmallDateTime) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = dossierSacrificeDependentEntityParam.DossierSacrifice_DependentId;
            parameters[1].Value = dossierSacrificeDependentEntityParam.DossierSacrificeId;
            parameters[2].Value = dossierSacrificeDependentEntityParam.PersonId;
            parameters[3].Value = dossierSacrificeDependentEntityParam.DependentTypeId;
            parameters[4].Value = dossierSacrificeDependentEntityParam.FromDate;

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("isar.p_DossierSacrifice_DependentUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDossierSacrifice_DependentDB(DossierSacrifice_DependentEntity DossierSacrifice_DependentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DossierSacrifice_DependentId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DossierSacrifice_DependentEntityParam.DossierSacrifice_DependentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_DossierSacrifice_DependentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DossierSacrifice_DependentEntity GetSingleDossierSacrifice_DependentDB(DossierSacrifice_DependentEntity DossierSacrifice_DependentEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DossierSacrifice_DependentId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DossierSacrifice_DependentEntityParam.DossierSacrifice_DependentId;

                reader = _intranetDB.RunProcedureReader("isar.p_DossierSacrifice_DependentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDossierSacrifice_DependentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_DependentEntity> GetAllDossierSacrifice_DependentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDossierSacrifice_DependentDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("isar.p_DossierSacrifice_DependentGetAll", new IDataParameter[] { }));
        }

        public List<DossierSacrifice_DependentEntity> GetPageDossierSacrifice_DependentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DossierSacrifice_Dependent";
            parameters[5].Value = "DossierSacrifice_DependentId";
            DataSet ds = _intranetDB.RunProcedureDS("isar.p_TablesGetPage", parameters);
            return GetDossierSacrifice_DependentDBCollectionFromDataSet(ds, out count);
        }

        public DossierSacrifice_DependentEntity GetDossierSacrifice_DependentDBFromDataReader(IDataReader reader)
        {
            return new DossierSacrifice_DependentEntity(Guid.Parse(reader["DossierSacrifice_DependentId"].ToString()),
                                    Guid.Parse(reader["DossierSacrificeId"].ToString()),
                                    Guid.Parse(reader["PersonId"].ToString()),
                                    Guid.Parse(reader["DependentTypeId"].ToString()),
                                    reader["FromDate"].ToString(),
                                    reader["Todate"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["FullName"].ToString());
        }

        public List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DossierSacrifice_DependentEntity> lst = new List<DossierSacrifice_DependentEntity>();
                while (reader.Read())
                    lst.Add(GetDossierSacrifice_DependentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDossierSacrifice_DependentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentDBCollectionByDependentTypeDB(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeDependentEntityParam.DependentTypeId;
            return GetDossierSacrifice_DependentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_DependentGetByDependentType", new[] { parameter }));
        }

        public List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentDBCollectionByPersonDB(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeDependentEntityParam.PersonId;
            return GetDossierSacrifice_DependentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_DependentGetByPerson", new[] { parameter }));
        }

        public List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentDBCollectionByDossierSacrificeDB(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@DossierSacrificeId", SqlDbType.UniqueIdentifier);
            parameter.Value = dossierSacrificeDependentEntityParam.DossierSacrificeId;
            return GetDossierSacrifice_DependentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("isar.p_DossierSacrifice_DependentGetByDossierSacrifice", new[] { parameter }));
        }


        #endregion

        public void CloseTodossierSacrificeDependentDB(DossierSacrifice_DependentEntity dossierSacrificeDependentEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DossierSacrifice_DependentId", SqlDbType.UniqueIdentifier),
		
											  new SqlParameter("@ToDate", SqlDbType.SmallDateTime) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Value = dossierSacrificeDependentEntity.DossierSacrifice_DependentId;
            parameters[1].Value = (dossierSacrificeDependentEntity.Todate==""?System.DBNull.Value:(object)dossierSacrificeDependentEntity.Todate);

            parameters[2].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("isar.p_DossierSacrifice_DependentToDateClose", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
   
        }
    }

    #endregion
}







