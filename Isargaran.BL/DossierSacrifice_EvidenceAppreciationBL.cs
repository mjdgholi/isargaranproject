﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات تقدیر کتبی>
    /// </summary>

	public class DossierSacrifice_EvidenceAppreciationBL 
	{	
	  	 private readonly DossierSacrifice_EvidenceAppreciationDB _DossierSacrifice_EvidenceAppreciationDB;					
			
		public DossierSacrifice_EvidenceAppreciationBL()
		{
			_DossierSacrifice_EvidenceAppreciationDB = new DossierSacrifice_EvidenceAppreciationDB();
		}			
	
		public  void Add(DossierSacrifice_EvidenceAppreciationEntity  DossierSacrifice_EvidenceAppreciationEntityParam, out Guid DossierSacrifice_EvidenceAppreciationId)
		{ 
            //if(DossierSacrifice_EvidenceAppreciationEntityParam.IsForeignissued && DossierSacrifice_EvidenceAppreciationEntityParam.OrganizationId==new Guid())
            //    throw new ItcApplicationErrorManagerException("سازمان را وارد نمایید");
			_DossierSacrifice_EvidenceAppreciationDB.AddDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntityParam,out DossierSacrifice_EvidenceAppreciationId);			
		}

		public  void Update(DossierSacrifice_EvidenceAppreciationEntity  DossierSacrifice_EvidenceAppreciationEntityParam)
		{
            //if (DossierSacrifice_EvidenceAppreciationEntityParam.IsForeignissued && DossierSacrifice_EvidenceAppreciationEntityParam.OrganizationId == new Guid())
            //    throw new ItcApplicationErrorManagerException("سازمان را وارد نمایید");
			_DossierSacrifice_EvidenceAppreciationDB.UpdateDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntityParam);		
		}

		public  void Delete(DossierSacrifice_EvidenceAppreciationEntity  DossierSacrifice_EvidenceAppreciationEntityParam)
		{
			 _DossierSacrifice_EvidenceAppreciationDB.DeleteDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntityParam);			
		}

		public  DossierSacrifice_EvidenceAppreciationEntity GetSingleById(DossierSacrifice_EvidenceAppreciationEntity  DossierSacrifice_EvidenceAppreciationEntityParam)
		{
			DossierSacrifice_EvidenceAppreciationEntity o = GetDossierSacrifice_EvidenceAppreciationFromDossierSacrifice_EvidenceAppreciationDB(
			_DossierSacrifice_EvidenceAppreciationDB.GetSingleDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_EvidenceAppreciationEntity> GetAll()
		{
			List<DossierSacrifice_EvidenceAppreciationEntity> lst = new List<DossierSacrifice_EvidenceAppreciationEntity>();
			//string key = "DossierSacrifice_EvidenceAppreciation_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_EvidenceAppreciationEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_EvidenceAppreciationCollectionFromDossierSacrifice_EvidenceAppreciationDBList(
				_DossierSacrifice_EvidenceAppreciationDB.GetAllDossierSacrifice_EvidenceAppreciationDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_EvidenceAppreciationEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_EvidenceAppreciation_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_EvidenceAppreciation_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_EvidenceAppreciationEntity> lst = new List<DossierSacrifice_EvidenceAppreciationEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_EvidenceAppreciationEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_EvidenceAppreciationCollectionFromDossierSacrifice_EvidenceAppreciationDBList(
				_DossierSacrifice_EvidenceAppreciationDB.GetPageDossierSacrifice_EvidenceAppreciationDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_EvidenceAppreciationEntity GetDossierSacrifice_EvidenceAppreciationFromDossierSacrifice_EvidenceAppreciationDB(DossierSacrifice_EvidenceAppreciationEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_EvidenceAppreciationEntity ret = new DossierSacrifice_EvidenceAppreciationEntity(o.DossierSacrifice_EvidenceAppreciationId ,o.DossierSacrificeId ,o.OccasionId ,o.OccasionDate ,o.EvidenceAppreciationContent ,o.EvidenceAppreciationDescription ,o.IsForeignissued ,o.OrganizationId ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationCollectionFromDossierSacrifice_EvidenceAppreciationDBList( List<DossierSacrifice_EvidenceAppreciationEntity> lst)
		{
			List<DossierSacrifice_EvidenceAppreciationEntity> RetLst = new List<DossierSacrifice_EvidenceAppreciationEntity>();
			foreach(DossierSacrifice_EvidenceAppreciationEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_EvidenceAppreciationFromDossierSacrifice_EvidenceAppreciationDB(o));
			}
			return RetLst;
			
		}



        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationCollectionByDossierSacrifice(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
{
    return GetDossierSacrifice_EvidenceAppreciationCollectionFromDossierSacrifice_EvidenceAppreciationDBList(_DossierSacrifice_EvidenceAppreciationDB.GetDossierSacrifice_EvidenceAppreciationDBCollectionByDossierSacrificeDB(DossierSacrifice_EvidenceAppreciationEntityParam));
}

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationCollectionByOccasion(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
{
    return GetDossierSacrifice_EvidenceAppreciationCollectionFromDossierSacrifice_EvidenceAppreciationDBList(_DossierSacrifice_EvidenceAppreciationDB.GetDossierSacrifice_EvidenceAppreciationDBCollectionByOccasionDB(DossierSacrifice_EvidenceAppreciationEntityParam));
}

        public List<DossierSacrifice_EvidenceAppreciationEntity> GetDossierSacrifice_EvidenceAppreciationCollectionByOrganization(DossierSacrifice_EvidenceAppreciationEntity DossierSacrifice_EvidenceAppreciationEntityParam)
{
    return GetDossierSacrifice_EvidenceAppreciationCollectionFromDossierSacrifice_EvidenceAppreciationDBList(_DossierSacrifice_EvidenceAppreciationDB.GetDossierSacrifice_EvidenceAppreciationDBCollectionByOrganizationDB(DossierSacrifice_EvidenceAppreciationEntityParam));
}




}

}
