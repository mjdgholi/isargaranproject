﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/26>
    /// Description: <وضیعت سربازی>
    /// </summary>

    public class MilitaryStatusBL
    {
        private readonly MilitaryStatusDB _MilitaryStatusDB;

        public MilitaryStatusBL()
        {
            _MilitaryStatusDB = new MilitaryStatusDB();
        }

        public void Add(MilitaryStatusEntity MilitaryStatusEntityParam, out Guid MilitaryStatusId)
        {
            _MilitaryStatusDB.AddMilitaryStatusDB(MilitaryStatusEntityParam, out MilitaryStatusId);
        }

        public void Update(MilitaryStatusEntity MilitaryStatusEntityParam)
        {
            _MilitaryStatusDB.UpdateMilitaryStatusDB(MilitaryStatusEntityParam);
        }

        public void Delete(MilitaryStatusEntity MilitaryStatusEntityParam)
        {
            _MilitaryStatusDB.DeleteMilitaryStatusDB(MilitaryStatusEntityParam);
        }

        public MilitaryStatusEntity GetSingleById(MilitaryStatusEntity MilitaryStatusEntityParam)
        {
            MilitaryStatusEntity o = GetMilitaryStatusFromMilitaryStatusDB(
                _MilitaryStatusDB.GetSingleMilitaryStatusDB(MilitaryStatusEntityParam));

            return o;
        }

        public List<MilitaryStatusEntity> GetAll()
        {
            List<MilitaryStatusEntity> lst = new List<MilitaryStatusEntity>();
            //string key = "MilitaryStatus_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MilitaryStatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMilitaryStatusCollectionFromMilitaryStatusDBList(
                _MilitaryStatusDB.GetAllMilitaryStatusDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<MilitaryStatusEntity> GetAllIsActive()
        {
            List<MilitaryStatusEntity> lst = new List<MilitaryStatusEntity>();
            //string key = "MilitaryStatus_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MilitaryStatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMilitaryStatusCollectionFromMilitaryStatusDBList(
                _MilitaryStatusDB.GetAllMilitaryStatusIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<MilitaryStatusEntity> GetAllPaging(int currentPage, int pageSize,
                                                       string

                                                           sortExpression, out int count, string whereClause)
        {
            //string key = "MilitaryStatus_List_Page_" + currentPage ;
            //string countKey = "MilitaryStatus_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<MilitaryStatusEntity> lst = new List<MilitaryStatusEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MilitaryStatusEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetMilitaryStatusCollectionFromMilitaryStatusDBList(
                _MilitaryStatusDB.GetPageMilitaryStatusDB(pageSize, currentPage,
                                                          whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}
        }

        private MilitaryStatusEntity GetMilitaryStatusFromMilitaryStatusDB(MilitaryStatusEntity o)
        {
            if (o == null)
                return null;
            MilitaryStatusEntity ret = new MilitaryStatusEntity(o.MilitaryStatusId, o.MilitaryStatusTitle,
                                                                o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<MilitaryStatusEntity> GetMilitaryStatusCollectionFromMilitaryStatusDBList(
            List<MilitaryStatusEntity> lst)
        {
            List<MilitaryStatusEntity> RetLst = new List<MilitaryStatusEntity>();
            foreach (MilitaryStatusEntity o in lst)
            {
                RetLst.Add(GetMilitaryStatusFromMilitaryStatusDB(o));
            }
            return RetLst;

        }

    }

}
