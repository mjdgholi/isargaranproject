﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<اطلاعات  اطلاعات خدمات و تسهیلات غیر نقدی>
    /// </summary>

    public class DossierSacrifice_FacilityAndServiceNonCashBL
    {
        private readonly DossierSacrifice_FacilityAndServiceNonCashDB _DossierSacrifice_FacilityAndServiceNonCashDB;

        public DossierSacrifice_FacilityAndServiceNonCashBL()
        {
            _DossierSacrifice_FacilityAndServiceNonCashDB = new DossierSacrifice_FacilityAndServiceNonCashDB();
        }

        public void Add(
            DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam,
            out Guid DossierSacrifice_FacilityAndServiceNonCashId)
        {
            _DossierSacrifice_FacilityAndServiceNonCashDB.AddDossierSacrifice_FacilityAndServiceNonCashDB(
                DossierSacrifice_FacilityAndServiceNonCashEntityParam, out DossierSacrifice_FacilityAndServiceNonCashId);
        }

        public void Update(
            DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            _DossierSacrifice_FacilityAndServiceNonCashDB.UpdateDossierSacrifice_FacilityAndServiceNonCashDB(
                DossierSacrifice_FacilityAndServiceNonCashEntityParam);
        }

        public void Delete(
            DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            _DossierSacrifice_FacilityAndServiceNonCashDB.DeleteDossierSacrifice_FacilityAndServiceNonCashDB(
                DossierSacrifice_FacilityAndServiceNonCashEntityParam);
        }

        public DossierSacrifice_FacilityAndServiceNonCashEntity GetSingleById(
            DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            DossierSacrifice_FacilityAndServiceNonCashEntity o = GetDossierSacrifice_FacilityAndServiceNonCashFromDossierSacrifice_FacilityAndServiceNonCashDB
                (
                    _DossierSacrifice_FacilityAndServiceNonCashDB.GetSingleDossierSacrifice_FacilityAndServiceNonCashDB(
                        DossierSacrifice_FacilityAndServiceNonCashEntityParam));

            return o;
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetAll()
        {
            List<DossierSacrifice_FacilityAndServiceNonCashEntity> lst =
                new List<DossierSacrifice_FacilityAndServiceNonCashEntity>();
            //string key = "DossierSacrifice_FacilityAndServiceNonCash_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilityAndServiceNonCashEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilityAndServiceNonCashCollectionFromDossierSacrifice_FacilityAndServiceNonCashDBList
                (
                    _DossierSacrifice_FacilityAndServiceNonCashDB.GetAllDossierSacrifice_FacilityAndServiceNonCashDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_FacilityAndServiceNonCash_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_FacilityAndServiceNonCash_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_FacilityAndServiceNonCashEntity> lst =
                new List<DossierSacrifice_FacilityAndServiceNonCashEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilityAndServiceNonCashEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilityAndServiceNonCashCollectionFromDossierSacrifice_FacilityAndServiceNonCashDBList
                (
                    _DossierSacrifice_FacilityAndServiceNonCashDB.GetPageDossierSacrifice_FacilityAndServiceNonCashDB(
                        pageSize, currentPage,
                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_FacilityAndServiceNonCashEntity
            GetDossierSacrifice_FacilityAndServiceNonCashFromDossierSacrifice_FacilityAndServiceNonCashDB(
            DossierSacrifice_FacilityAndServiceNonCashEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_FacilityAndServiceNonCashEntity ret =
                new DossierSacrifice_FacilityAndServiceNonCashEntity(o.DossierSacrifice_FacilityAndServiceNonCashId,
                    o.DossierSacrificeId, o.FacilityAndServiceNonCashId, o.FacilityAndServiceNonCashDate, o.OccasionId,
                    o.CashAmount, o.RecipientPersonName, o.RecipientPersonNationalNo, o.Description, o.CreationDate,
                    o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_FacilityAndServiceNonCashEntity>
            GetDossierSacrifice_FacilityAndServiceNonCashCollectionFromDossierSacrifice_FacilityAndServiceNonCashDBList(
            List<DossierSacrifice_FacilityAndServiceNonCashEntity> lst)
        {
            List<DossierSacrifice_FacilityAndServiceNonCashEntity> RetLst =
                new List<DossierSacrifice_FacilityAndServiceNonCashEntity>();
            foreach (DossierSacrifice_FacilityAndServiceNonCashEntity o in lst)
            {
                RetLst.Add(
                    GetDossierSacrifice_FacilityAndServiceNonCashFromDossierSacrifice_FacilityAndServiceNonCashDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_FacilityAndServiceNonCashEntity>
            GetDossierSacrifice_FacilityAndServiceNonCashCollectionByDossierSacrifice(
            DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            return
                GetDossierSacrifice_FacilityAndServiceNonCashCollectionFromDossierSacrifice_FacilityAndServiceNonCashDBList
                    (_DossierSacrifice_FacilityAndServiceNonCashDB
                        .GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionByDossierSacrificeDB(
                            DossierSacrifice_FacilityAndServiceNonCashEntityParam));
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity>
            GetDossierSacrifice_FacilityAndServiceNonCashCollectionByFacilityAndServiceNonCash(
            DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            return
                GetDossierSacrifice_FacilityAndServiceNonCashCollectionFromDossierSacrifice_FacilityAndServiceNonCashDBList
                    (_DossierSacrifice_FacilityAndServiceNonCashDB
                        .GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionByFacilityAndServiceNonCashDB(
                            DossierSacrifice_FacilityAndServiceNonCashEntityParam));
        }

        public List<DossierSacrifice_FacilityAndServiceNonCashEntity>
            GetDossierSacrifice_FacilityAndServiceNonCashCollectionByOccasion(
            DossierSacrifice_FacilityAndServiceNonCashEntity DossierSacrifice_FacilityAndServiceNonCashEntityParam)
        {
            return
                GetDossierSacrifice_FacilityAndServiceNonCashCollectionFromDossierSacrifice_FacilityAndServiceNonCashDBList
                    (_DossierSacrifice_FacilityAndServiceNonCashDB
                        .GetDossierSacrifice_FacilityAndServiceNonCashDBCollectionByOccasionDB(
                            DossierSacrifice_FacilityAndServiceNonCashEntityParam));
        }

    }

}
