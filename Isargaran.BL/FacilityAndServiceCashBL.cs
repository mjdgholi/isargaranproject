﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/21>
    /// Description: <خدمات و تسهیلات نقدی>
    /// </summary>

	public class FacilityAndServiceCashBL 
	{	
	  	 private readonly FacilityAndServiceCashDB _FacilityAndServiceCashDB;					
			
		public FacilityAndServiceCashBL()
		{
			_FacilityAndServiceCashDB = new FacilityAndServiceCashDB();
		}			
	
		public  void Add(FacilityAndServiceCashEntity  FacilityAndServiceCashEntityParam, out Guid FacilityAndServiceCashId)
		{ 
			_FacilityAndServiceCashDB.AddFacilityAndServiceCashDB(FacilityAndServiceCashEntityParam,out FacilityAndServiceCashId);			
		}

		public  void Update(FacilityAndServiceCashEntity  FacilityAndServiceCashEntityParam)
		{
			_FacilityAndServiceCashDB.UpdateFacilityAndServiceCashDB(FacilityAndServiceCashEntityParam);		
		}

		public  void Delete(FacilityAndServiceCashEntity  FacilityAndServiceCashEntityParam)
		{
			 _FacilityAndServiceCashDB.DeleteFacilityAndServiceCashDB(FacilityAndServiceCashEntityParam);			
		}

		public  FacilityAndServiceCashEntity GetSingleById(FacilityAndServiceCashEntity  FacilityAndServiceCashEntityParam)
		{
			FacilityAndServiceCashEntity o = GetFacilityAndServiceCashFromFacilityAndServiceCashDB(
			_FacilityAndServiceCashDB.GetSingleFacilityAndServiceCashDB(FacilityAndServiceCashEntityParam));
			
			return o;
		}

		public  List<FacilityAndServiceCashEntity> GetAll()
		{
			List<FacilityAndServiceCashEntity> lst = new List<FacilityAndServiceCashEntity>();
			//string key = "FacilityAndServiceCash_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<FacilityAndServiceCashEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetFacilityAndServiceCashCollectionFromFacilityAndServiceCashDBList(
				_FacilityAndServiceCashDB.GetAllFacilityAndServiceCashDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<FacilityAndServiceCashEntity> GetAllIsActive()
        {
            List<FacilityAndServiceCashEntity> lst = new List<FacilityAndServiceCashEntity>();
            //string key = "FacilityAndServiceCash_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<FacilityAndServiceCashEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetFacilityAndServiceCashCollectionFromFacilityAndServiceCashDBList(
            _FacilityAndServiceCashDB.GetAllFacilityAndServiceCashIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<FacilityAndServiceCashEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "FacilityAndServiceCash_List_Page_" + currentPage ;
			//string countKey = "FacilityAndServiceCash_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<FacilityAndServiceCashEntity> lst = new List<FacilityAndServiceCashEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<FacilityAndServiceCashEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetFacilityAndServiceCashCollectionFromFacilityAndServiceCashDBList(
				_FacilityAndServiceCashDB.GetPageFacilityAndServiceCashDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  FacilityAndServiceCashEntity GetFacilityAndServiceCashFromFacilityAndServiceCashDB(FacilityAndServiceCashEntity o)
		{
	if(o == null)
                return null;
			FacilityAndServiceCashEntity ret = new FacilityAndServiceCashEntity(o.FacilityAndServiceCashId ,o.FacilityAndServiceCashTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<FacilityAndServiceCashEntity> GetFacilityAndServiceCashCollectionFromFacilityAndServiceCashDBList( List<FacilityAndServiceCashEntity> lst)
		{
			List<FacilityAndServiceCashEntity> RetLst = new List<FacilityAndServiceCashEntity>();
			foreach(FacilityAndServiceCashEntity o in lst)
			{
				RetLst.Add(GetFacilityAndServiceCashFromFacilityAndServiceCashDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}

