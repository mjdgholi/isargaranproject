﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <مراکزتحصیلی>
    /// </summary>


    public class EducationCenterBL
    {
        private readonly EducationCenterDB _EducationCenterDB;

        public EducationCenterBL()
        {
            _EducationCenterDB = new EducationCenterDB();
        }

        public void Add(EducationCenterEntity EducationCenterEntityParam, out Guid EducationCenterId)
        {
            _EducationCenterDB.AddEducationCenterDB(EducationCenterEntityParam, out EducationCenterId);
        }

        public void Update(EducationCenterEntity EducationCenterEntityParam)
        {
            _EducationCenterDB.UpdateEducationCenterDB(EducationCenterEntityParam);
        }

        public void Delete(EducationCenterEntity EducationCenterEntityParam)
        {
            _EducationCenterDB.DeleteEducationCenterDB(EducationCenterEntityParam);
        }

        public EducationCenterEntity GetSingleById(EducationCenterEntity EducationCenterEntityParam)
        {
            EducationCenterEntity o = GetEducationCenterFromEducationCenterDB(
                _EducationCenterDB.GetSingleEducationCenterDB(EducationCenterEntityParam));

            return o;
        }

        public List<EducationCenterEntity> GetAll()
        {
            List<EducationCenterEntity> lst = new List<EducationCenterEntity>();
            //string key = "EducationCenter_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCenterEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationCenterCollectionFromEducationCenterDBList(
                _EducationCenterDB.GetAllEducationCenterDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<EducationCenterEntity> GetAllIsActive()
        {
            List<EducationCenterEntity> lst = new List<EducationCenterEntity>();
            //string key = "EducationCenter_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCenterEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationCenterCollectionFromEducationCenterDBList(
                _EducationCenterDB.GetAllEducationCenterIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<EducationCenterEntity> GetAllPaging(int currentPage, int pageSize,
                                                        string

                                                            sortExpression, out int count, string whereClause)
        {
            //string key = "EducationCenter_List_Page_" + currentPage ;
            //string countKey = "EducationCenter_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<EducationCenterEntity> lst = new List<EducationCenterEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCenterEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetEducationCenterCollectionFromEducationCenterDBList(
                _EducationCenterDB.GetPageEducationCenterDB(pageSize, currentPage,
                                                            whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private EducationCenterEntity GetEducationCenterFromEducationCenterDB(EducationCenterEntity o)
        {
            if (o == null)
                return null;
            EducationCenterEntity ret = new EducationCenterEntity(o.EducationCenterId, o.EducationCenterTypeId,
                                                                  o.EducationCenterPersianTitle,
                                                                  o.EducationCenterEnglishTitle, o.Email, o.TelNo,
                                                                  o.FaxNo, o.ZipCode, o.Address, o.CreationDate,
                                                                  o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<EducationCenterEntity> GetEducationCenterCollectionFromEducationCenterDBList(
            List<EducationCenterEntity> lst)
        {
            List<EducationCenterEntity> RetLst = new List<EducationCenterEntity>();
            foreach (EducationCenterEntity o in lst)
            {
                RetLst.Add(GetEducationCenterFromEducationCenterDB(o));
            }
            return RetLst;

        }



        public List<EducationCenterEntity> GetEducationCenterCollectionByEducationCenterType(
            EducationCenterEntity EducationCenterEntityParam)
        {
            return
                GetEducationCenterCollectionFromEducationCenterDBList(
                    _EducationCenterDB.GetEducationCenterDBCollectionByEducationCenterTypeDB(EducationCenterEntityParam));
        }




    }

}
