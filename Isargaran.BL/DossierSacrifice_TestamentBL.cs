﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/31>
    /// Description: <اطلاعات وصیت ایثارگر>
    /// </summary>

    public class DossierSacrifice_TestamentBL
    {
        private readonly DossierSacrifice_TestamentDB _DossierSacrifice_TestamentDB;

        public DossierSacrifice_TestamentBL()
        {
            _DossierSacrifice_TestamentDB = new DossierSacrifice_TestamentDB();
        }

        public void Add(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam,
                        out Guid DossierSacrifice_TestamentId)
        {
            _DossierSacrifice_TestamentDB.AddDossierSacrifice_TestamentDB(DossierSacrifice_TestamentEntityParam,
                                                                          out DossierSacrifice_TestamentId);
        }

        public void Update(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            _DossierSacrifice_TestamentDB.UpdateDossierSacrifice_TestamentDB(DossierSacrifice_TestamentEntityParam);
        }

        public void Delete(DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            _DossierSacrifice_TestamentDB.DeleteDossierSacrifice_TestamentDB(DossierSacrifice_TestamentEntityParam);
        }

        public DossierSacrifice_TestamentEntity GetSingleById(
            DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            DossierSacrifice_TestamentEntity o = GetDossierSacrifice_TestamentFromDossierSacrifice_TestamentDB(
                _DossierSacrifice_TestamentDB.GetSingleDossierSacrifice_TestamentDB(
                    DossierSacrifice_TestamentEntityParam));

            return o;
        }

        public List<DossierSacrifice_TestamentEntity> GetAll()
        {
            List<DossierSacrifice_TestamentEntity> lst = new List<DossierSacrifice_TestamentEntity>();
            //string key = "DossierSacrifice_Testament_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_TestamentEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_TestamentCollectionFromDossierSacrifice_TestamentDBList(
                _DossierSacrifice_TestamentDB.GetAllDossierSacrifice_TestamentDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_TestamentEntity> GetAllPaging(int currentPage, int pageSize,
                                                                   string

                                                                       sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Testament_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Testament_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_TestamentEntity> lst = new List<DossierSacrifice_TestamentEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_TestamentEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_TestamentCollectionFromDossierSacrifice_TestamentDBList(
                _DossierSacrifice_TestamentDB.GetPageDossierSacrifice_TestamentDB(pageSize, currentPage,
                                                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_TestamentEntity GetDossierSacrifice_TestamentFromDossierSacrifice_TestamentDB(
            DossierSacrifice_TestamentEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_TestamentEntity ret = new DossierSacrifice_TestamentEntity(o.DossierSacrifice_TestamentId,
                                                                                        o.IsHasTestament,
                                                                                        o.TestamentDate,
                                                                                        o.TestamentContent,
                                                                                        o.DossierSacrificeId,
                                                                                        o.CreationDate,
                                                                                        o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_TestamentEntity>
            GetDossierSacrifice_TestamentCollectionFromDossierSacrifice_TestamentDBList(
            List<DossierSacrifice_TestamentEntity> lst)
        {
            List<DossierSacrifice_TestamentEntity> RetLst = new List<DossierSacrifice_TestamentEntity>();
            foreach (DossierSacrifice_TestamentEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_TestamentFromDossierSacrifice_TestamentDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_TestamentEntity> GetDossierSacrifice_TestamentCollectionByDossierSacrifice(
            DossierSacrifice_TestamentEntity DossierSacrifice_TestamentEntityParam)
        {
            return
                GetDossierSacrifice_TestamentCollectionFromDossierSacrifice_TestamentDBList(
                    _DossierSacrifice_TestamentDB.GetDossierSacrifice_TestamentDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_TestamentEntityParam));
        }

    }
}


