﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <سیستم تحصیلی>
    /// </summary>


	public class EducationSystemBL 
	{	
	  	 private readonly EducationSystemDB _EducationSystemDB;					
			
		public EducationSystemBL()
		{
			_EducationSystemDB = new EducationSystemDB();
		}			
	
		public  void Add(EducationSystemEntity  EducationSystemEntityParam, out Guid EducationSystemId)
		{ 
			_EducationSystemDB.AddEducationSystemDB(EducationSystemEntityParam,out EducationSystemId);			
		}

		public  void Update(EducationSystemEntity  EducationSystemEntityParam)
		{
			_EducationSystemDB.UpdateEducationSystemDB(EducationSystemEntityParam);		
		}

		public  void Delete(EducationSystemEntity  EducationSystemEntityParam)
		{
			 _EducationSystemDB.DeleteEducationSystemDB(EducationSystemEntityParam);			
		}

		public  EducationSystemEntity GetSingleById(EducationSystemEntity  EducationSystemEntityParam)
		{
			EducationSystemEntity o = GetEducationSystemFromEducationSystemDB(
			_EducationSystemDB.GetSingleEducationSystemDB(EducationSystemEntityParam));
			
			return o;
		}

		public  List<EducationSystemEntity> GetAll()
		{
			List<EducationSystemEntity> lst = new List<EducationSystemEntity>();
			//string key = "EducationSystem_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationSystemEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetEducationSystemCollectionFromEducationSystemDBList(
				_EducationSystemDB.GetAllEducationSystemDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<EducationSystemEntity> GetAllIsActive()
        {
            List<EducationSystemEntity> lst = new List<EducationSystemEntity>();
            //string key = "EducationSystem_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationSystemEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationSystemCollectionFromEducationSystemDBList(
            _EducationSystemDB.GetAllEducationSystemIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<EducationSystemEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "EducationSystem_List_Page_" + currentPage ;
			//string countKey = "EducationSystem_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<EducationSystemEntity> lst = new List<EducationSystemEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationSystemEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetEducationSystemCollectionFromEducationSystemDBList(
				_EducationSystemDB.GetPageEducationSystemDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  EducationSystemEntity GetEducationSystemFromEducationSystemDB(EducationSystemEntity o)
		{
	if(o == null)
                return null;
			EducationSystemEntity ret = new EducationSystemEntity(o.EducationSystemId ,o.EducationSystemPersianTitle ,o.EducationSystemEnglishTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<EducationSystemEntity> GetEducationSystemCollectionFromEducationSystemDBList( List<EducationSystemEntity> lst)
		{
			List<EducationSystemEntity> RetLst = new List<EducationSystemEntity>();
			foreach(EducationSystemEntity o in lst)
			{
				RetLst.Add(GetEducationSystemFromEducationSystemDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}


