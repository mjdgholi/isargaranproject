﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع وضیعت درخواست>
    /// </summary>

	public class StatusBL 
	{	
	  	 private readonly StatusDB _StatusDB;					
			
		public StatusBL()
		{
			_StatusDB = new StatusDB();
		}			
	
		public  void Add(StatusEntity  StatusEntityParam, out Guid StatusId)
		{ 
			_StatusDB.AddStatusDB(StatusEntityParam,out StatusId);			
		}

		public  void Update(StatusEntity  StatusEntityParam)
		{
			_StatusDB.UpdateStatusDB(StatusEntityParam);		
		}

		public  void Delete(StatusEntity  StatusEntityParam)
		{
			 _StatusDB.DeleteStatusDB(StatusEntityParam);			
		}

		public  StatusEntity GetSingleById(StatusEntity  StatusEntityParam)
		{
			StatusEntity o = GetStatusFromStatusDB(
			_StatusDB.GetSingleStatusDB(StatusEntityParam));
			
			return o;
		}

		public  List<StatusEntity> GetAll()
		{
			List<StatusEntity> lst = new List<StatusEntity>();
			//string key = "Status_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<StatusEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetStatusCollectionFromStatusDBList(
				_StatusDB.GetAllStatusDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public object GetAllIsActive()
        {
            List<StatusEntity> lst = new List<StatusEntity>();
            //string key = "Status_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<StatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetStatusCollectionFromStatusDBList(
            _StatusDB.GetAllStatusIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<StatusEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Status_List_Page_" + currentPage ;
			//string countKey = "Status_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<StatusEntity> lst = new List<StatusEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<StatusEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetStatusCollectionFromStatusDBList(
				_StatusDB.GetPageStatusDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  StatusEntity GetStatusFromStatusDB(StatusEntity o)
		{
	if(o == null)
                return null;
			StatusEntity ret = new StatusEntity(o.StatusId ,o.StatusTitle ,o.Priority ,o.IsFinal ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<StatusEntity> GetStatusCollectionFromStatusDBList( List<StatusEntity> lst)
		{
			List<StatusEntity> RetLst = new List<StatusEntity>();
			foreach(StatusEntity o in lst)
			{
				RetLst.Add(GetStatusFromStatusDB(o));
			}
			return RetLst;
			
		}


        
	}
}

        
    
