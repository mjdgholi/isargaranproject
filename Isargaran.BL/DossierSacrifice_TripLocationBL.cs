﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/20>
    /// Description: <پرونده اطلاعات اردو یا مسافرت ایثارگر>
    /// </summary>

	public class DossierSacrifice_TripLocationBL 
	{	
	  	 private readonly DossierSacrifice_TripLocationDB _DossierSacrifice_TripLocationDB;					
			
		public DossierSacrifice_TripLocationBL()
		{
			_DossierSacrifice_TripLocationDB = new DossierSacrifice_TripLocationDB();
		}			
	
		public  void Add(DossierSacrifice_TripLocationEntity  DossierSacrifice_TripLocationEntityParam, out Guid DossierSacrifice_TravelLocationId)
		{ 
			_DossierSacrifice_TripLocationDB.AddDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntityParam,out DossierSacrifice_TravelLocationId);			
		}

		public  void Update(DossierSacrifice_TripLocationEntity  DossierSacrifice_TripLocationEntityParam)
		{
			_DossierSacrifice_TripLocationDB.UpdateDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntityParam);		
		}

		public  void Delete(DossierSacrifice_TripLocationEntity  DossierSacrifice_TripLocationEntityParam)
		{
			 _DossierSacrifice_TripLocationDB.DeleteDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntityParam);			
		}

		public  DossierSacrifice_TripLocationEntity GetSingleById(DossierSacrifice_TripLocationEntity  DossierSacrifice_TripLocationEntityParam)
		{
			DossierSacrifice_TripLocationEntity o = GetDossierSacrifice_TripLocationFromDossierSacrifice_TripLocationDB(
			_DossierSacrifice_TripLocationDB.GetSingleDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_TripLocationEntity> GetAll()
		{
			List<DossierSacrifice_TripLocationEntity> lst = new List<DossierSacrifice_TripLocationEntity>();
			//string key = "DossierSacrifice_TripLocation_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_TripLocationEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_TripLocationCollectionFromDossierSacrifice_TripLocationDBList(
				_DossierSacrifice_TripLocationDB.GetAllDossierSacrifice_TripLocationDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_TripLocationEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_TripLocation_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_TripLocation_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_TripLocationEntity> lst = new List<DossierSacrifice_TripLocationEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_TripLocationEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_TripLocationCollectionFromDossierSacrifice_TripLocationDBList(
				_DossierSacrifice_TripLocationDB.GetPageDossierSacrifice_TripLocationDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_TripLocationEntity GetDossierSacrifice_TripLocationFromDossierSacrifice_TripLocationDB(DossierSacrifice_TripLocationEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_TripLocationEntity ret = new DossierSacrifice_TripLocationEntity(o.DossierSacrifice_TravelLocationId ,o.DossierSacrificeId ,o.TripLocationId ,o.TripStartDate ,o.TripEndDate ,o.TravelerNo ,o.FinancialCost ,o.VehicleType ,o.HotelName ,o.OccasionId ,o.AlongPersonName ,o.Description ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationCollectionFromDossierSacrifice_TripLocationDBList( List<DossierSacrifice_TripLocationEntity> lst)
		{
			List<DossierSacrifice_TripLocationEntity> RetLst = new List<DossierSacrifice_TripLocationEntity>();
			foreach(DossierSacrifice_TripLocationEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_TripLocationFromDossierSacrifice_TripLocationDB(o));
			}
			return RetLst;
			
		}


        public  List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationCollectionByDossierSacrifice(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
{
    return GetDossierSacrifice_TripLocationCollectionFromDossierSacrifice_TripLocationDBList(_DossierSacrifice_TripLocationDB.GetDossierSacrifice_TripLocationDBCollectionByDossierSacrificeDB(DossierSacrifice_TripLocationEntityParam));
}

        public  List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationCollectionByOccasion(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
{
    return GetDossierSacrifice_TripLocationCollectionFromDossierSacrifice_TripLocationDBList(_DossierSacrifice_TripLocationDB.GetDossierSacrifice_TripLocationDBCollectionByOccasionDB(DossierSacrifice_TripLocationEntityParam));
}

        public  List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationCollectionByTripLocation(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
{
    return GetDossierSacrifice_TripLocationCollectionFromDossierSacrifice_TripLocationDBList(_DossierSacrifice_TripLocationDB.GetDossierSacrifice_TripLocationDBCollectionByTripLocationDB(DossierSacrifice_TripLocationEntityParam));
}

        public  List<DossierSacrifice_TripLocationEntity> GetDossierSacrifice_TripLocationCollectionByVehicleType(DossierSacrifice_TripLocationEntity DossierSacrifice_TripLocationEntityParam)
{
    return GetDossierSacrifice_TripLocationCollectionFromDossierSacrifice_TripLocationDBList(_DossierSacrifice_TripLocationDB.GetDossierSacrifice_TripLocationDBCollectionByVehicleTypeDB(DossierSacrifice_TripLocationEntityParam));
}



}

}
