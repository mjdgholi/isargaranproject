﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <پرونده اطلاعات درخواست ایثارگر>
    /// </summary>
	public class DossierSacrifice_RequestBL 
	{	
	  	 private readonly DossierSacrifice_RequestDB _DossierSacrifice_RequestDB;					
			
		public DossierSacrifice_RequestBL()
		{
			_DossierSacrifice_RequestDB = new DossierSacrifice_RequestDB();
		}			
	
		public  void Add(DossierSacrifice_RequestEntity  DossierSacrifice_RequestEntityParam, out Guid DossierSacrifice_RequestId)
		{ 
			_DossierSacrifice_RequestDB.AddDossierSacrifice_RequestDB(DossierSacrifice_RequestEntityParam,out DossierSacrifice_RequestId);			
		}

		public  void Update(DossierSacrifice_RequestEntity  DossierSacrifice_RequestEntityParam)
		{
			_DossierSacrifice_RequestDB.UpdateDossierSacrifice_RequestDB(DossierSacrifice_RequestEntityParam);		
		}

		public  void Delete(DossierSacrifice_RequestEntity  DossierSacrifice_RequestEntityParam)
		{
			 _DossierSacrifice_RequestDB.DeleteDossierSacrifice_RequestDB(DossierSacrifice_RequestEntityParam);			
		}

		public  DossierSacrifice_RequestEntity GetSingleById(DossierSacrifice_RequestEntity  DossierSacrifice_RequestEntityParam)
		{
			DossierSacrifice_RequestEntity o = GetDossierSacrifice_RequestFromDossierSacrifice_RequestDB(
			_DossierSacrifice_RequestDB.GetSingleDossierSacrifice_RequestDB(DossierSacrifice_RequestEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_RequestEntity> GetAll()
		{
			List<DossierSacrifice_RequestEntity> lst = new List<DossierSacrifice_RequestEntity>();
			//string key = "DossierSacrifice_Request_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_RequestEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_RequestCollectionFromDossierSacrifice_RequestDBList(
				_DossierSacrifice_RequestDB.GetAllDossierSacrifice_RequestDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_RequestEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_Request_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_Request_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_RequestEntity> lst = new List<DossierSacrifice_RequestEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_RequestEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_RequestCollectionFromDossierSacrifice_RequestDBList(
				_DossierSacrifice_RequestDB.GetPageDossierSacrifice_RequestDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_RequestEntity GetDossierSacrifice_RequestFromDossierSacrifice_RequestDB(DossierSacrifice_RequestEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_RequestEntity ret = new DossierSacrifice_RequestEntity(o.DossierSacrifice_RequestId ,o.DossierSacrificeId ,o.RequestTypeId ,o.ImportanceTypeId  ,o.RequestDescription ,o.CreationDate ,o.ModificationDate,o.DossierSacrificeNo,o.PersonId,o.FullName );
			return ret;
		}
		
		private  List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestCollectionFromDossierSacrifice_RequestDBList( List<DossierSacrifice_RequestEntity> lst)
		{
			List<DossierSacrifice_RequestEntity> RetLst = new List<DossierSacrifice_RequestEntity>();
			foreach(DossierSacrifice_RequestEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_RequestFromDossierSacrifice_RequestDB(o));
			}
			return RetLst;
			
		}




        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestCollectionByDossierSacrifice(DossierSacrifice_RequestEntity DossierSacrifice_RequestEntityParam)
{
    return GetDossierSacrifice_RequestCollectionFromDossierSacrifice_RequestDBList(_DossierSacrifice_RequestDB.GetDossierSacrifice_RequestDBCollectionByDossierSacrificeDB(DossierSacrifice_RequestEntityParam));
}

        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestCollectionByImportanceType(DossierSacrifice_RequestEntity DossierSacrifice_RequestEntityParam)
{
    return GetDossierSacrifice_RequestCollectionFromDossierSacrifice_RequestDBList(_DossierSacrifice_RequestDB.GetDossierSacrifice_RequestDBCollectionByImportanceTypeDB(DossierSacrifice_RequestEntityParam));
}

        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestCollectionByRequestType(DossierSacrifice_RequestEntity DossierSacrifice_RequestEntityParam)
{
    return GetDossierSacrifice_RequestCollectionFromDossierSacrifice_RequestDBList(_DossierSacrifice_RequestDB.GetDossierSacrifice_RequestDBCollectionByRequestTypeDB(DossierSacrifice_RequestEntityParam));
}

//        public List<DossierSacrifice_RequestEntity> GetDossierSacrifice_RequestCollectionByStatus(DossierSacrifice_RequestEntity DossierSacrifice_RequestEntityParam)
//{
//    return GetDossierSacrifice_RequestCollectionFromDossierSacrifice_RequestDBList(_DossierSacrifice_RequestDB.GetDossierSacrifice_RequestDBCollectionByStatusDB(DossierSacrifice_RequestEntityParam));
//}


        public void UpDateIsView(DossierSacrifice_RequestEntity dossierSacrificeRequestEntity)
        {
            _DossierSacrifice_RequestDB.UpdateIsViewDB(dossierSacrificeRequestEntity);
        }
	}

}