﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/16>
    /// Description: < اطلاعات ملاقات>
    /// </summary>
    public class DossierSacrifice_AppointmentBL
    {
        private readonly DossierSacrifice_AppointmentDB _DossierSacrifice_AppointmentDB;

        public DossierSacrifice_AppointmentBL()
        {
            _DossierSacrifice_AppointmentDB = new DossierSacrifice_AppointmentDB();
        }

        public void Add(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam,
                        out Guid DossierSacrifice_AppointmentId)
        {
            if (DossierSacrifice_AppointmentEntityParam.HasAction && DossierSacrifice_AppointmentEntityParam.ActionDescription == "")
            {
                throw new ItcApplicationErrorManagerException("شرح اقدام را وارد نمایید");
            }
            if (DossierSacrifice_AppointmentEntityParam.HasRequest && DossierSacrifice_AppointmentEntityParam.RequestContent == "")
            {
                throw new ItcApplicationErrorManagerException("متن درخواست  را وارد نمایید");
            }
            _DossierSacrifice_AppointmentDB.AddDossierSacrifice_AppointmentDB(DossierSacrifice_AppointmentEntityParam,
                                                                              out DossierSacrifice_AppointmentId);
        }

        public void Update(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            if (DossierSacrifice_AppointmentEntityParam.HasAction && DossierSacrifice_AppointmentEntityParam.ActionDescription == "")
            {
                throw new ItcApplicationErrorManagerException("شرح اقدام را وارد نمایید");
            }
            if (DossierSacrifice_AppointmentEntityParam.HasRequest && DossierSacrifice_AppointmentEntityParam.RequestContent == "")
            {
                throw new ItcApplicationErrorManagerException("متن درخواست  را وارد نمایید");
            }
            _DossierSacrifice_AppointmentDB.UpdateDossierSacrifice_AppointmentDB(DossierSacrifice_AppointmentEntityParam);
        }

        public void Delete(DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            _DossierSacrifice_AppointmentDB.DeleteDossierSacrifice_AppointmentDB(DossierSacrifice_AppointmentEntityParam);
        }

        public DossierSacrifice_AppointmentEntity GetSingleById(
            DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            DossierSacrifice_AppointmentEntity o = GetDossierSacrifice_AppointmentFromDossierSacrifice_AppointmentDB(
                _DossierSacrifice_AppointmentDB.GetSingleDossierSacrifice_AppointmentDB(
                    DossierSacrifice_AppointmentEntityParam));

            return o;
        }

        public List<DossierSacrifice_AppointmentEntity> GetAll()
        {
            List<DossierSacrifice_AppointmentEntity> lst = new List<DossierSacrifice_AppointmentEntity>();
            //string key = "DossierSacrifice_Appointment_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_AppointmentEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_AppointmentCollectionFromDossierSacrifice_AppointmentDBList(
                _DossierSacrifice_AppointmentDB.GetAllDossierSacrifice_AppointmentDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_AppointmentEntity> GetAllPaging(int currentPage, int pageSize,
                                                                     string

                                                                         sortExpression, out int count,
                                                                     string whereClause)
        {
            //string key = "DossierSacrifice_Appointment_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Appointment_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_AppointmentEntity> lst = new List<DossierSacrifice_AppointmentEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_AppointmentEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_AppointmentCollectionFromDossierSacrifice_AppointmentDBList(
                _DossierSacrifice_AppointmentDB.GetPageDossierSacrifice_AppointmentDB(pageSize, currentPage,
                                                                                      whereClause, sortExpression,
                                                                                      out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_AppointmentEntity GetDossierSacrifice_AppointmentFromDossierSacrifice_AppointmentDB(
            DossierSacrifice_AppointmentEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_AppointmentEntity ret =
                new DossierSacrifice_AppointmentEntity(o.DossierSacrifice_AppointmentId, o.DossierSacrificeId,
                                                       o.AppointmentDate, o.AppointmentTypeId, o.AppointmentPerson,
                                                       o.DependentTypeId, o.HasRequest, o.RequestContent, o.HasAction,
                                                       o.ActionDescription, o.Description, o.CreationDate,
                                                       o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_AppointmentEntity>
            GetDossierSacrifice_AppointmentCollectionFromDossierSacrifice_AppointmentDBList(
            List<DossierSacrifice_AppointmentEntity> lst)
        {
            List<DossierSacrifice_AppointmentEntity> RetLst = new List<DossierSacrifice_AppointmentEntity>();
            foreach (DossierSacrifice_AppointmentEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_AppointmentFromDossierSacrifice_AppointmentDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentCollectionByDependentType(
            DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            return
                GetDossierSacrifice_AppointmentCollectionFromDossierSacrifice_AppointmentDBList(
                    _DossierSacrifice_AppointmentDB.GetDossierSacrifice_AppointmentDBCollectionByDependentTypeDB(
                        DossierSacrifice_AppointmentEntityParam));
        }

        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentCollectionByAppointmentType(
            DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            return
                GetDossierSacrifice_AppointmentCollectionFromDossierSacrifice_AppointmentDBList(
                    _DossierSacrifice_AppointmentDB.GetDossierSacrifice_AppointmentDBCollectionByAppointmentTypeDB(
                        DossierSacrifice_AppointmentEntityParam));
        }



        public List<DossierSacrifice_AppointmentEntity> GetDossierSacrifice_AppointmentCollectionByDossierSacrifice(
            DossierSacrifice_AppointmentEntity DossierSacrifice_AppointmentEntityParam)
        {
            return
                GetDossierSacrifice_AppointmentCollectionFromDossierSacrifice_AppointmentDBList(
                    _DossierSacrifice_AppointmentDB.GetDossierSacrifice_AppointmentDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_AppointmentEntityParam));
        }



    }

}
