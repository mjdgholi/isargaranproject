﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/23>
    // Description:	<یگان اعزام کننده>
    /// </summary>
    public class MilitaryUnitDispatcherBL
    {
        private readonly MilitaryUnitDispatcherDB _MilitaryUnitDispatcherDB;

        public MilitaryUnitDispatcherBL()
        {
            _MilitaryUnitDispatcherDB = new MilitaryUnitDispatcherDB();
        }

        public void Add(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam, out Guid MilitaryUnitDispatcherId)
        {
            _MilitaryUnitDispatcherDB.AddMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntityParam, out MilitaryUnitDispatcherId);
        }

        public void Update(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam)
        {
            _MilitaryUnitDispatcherDB.UpdateMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntityParam);
        }

        public void Delete(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam)
        {
            _MilitaryUnitDispatcherDB.DeleteMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntityParam);
        }

        public MilitaryUnitDispatcherEntity GetSingleById(MilitaryUnitDispatcherEntity MilitaryUnitDispatcherEntityParam)
        {
            MilitaryUnitDispatcherEntity o = GetMilitaryUnitDispatcherFromMilitaryUnitDispatcherDB(
                _MilitaryUnitDispatcherDB.GetSingleMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntityParam));

            return o;
        }

        public List<MilitaryUnitDispatcherEntity> GetAll()
        {
            List<MilitaryUnitDispatcherEntity> lst = new List<MilitaryUnitDispatcherEntity>();
            //string key = "MilitaryUnitDispatcher_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MilitaryUnitDispatcherEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMilitaryUnitDispatcherCollectionFromMilitaryUnitDispatcherDBList(
                _MilitaryUnitDispatcherDB.GetAllMilitaryUnitDispatcherDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }


        public List<MilitaryUnitDispatcherEntity> GetAllIsActive()
        {
            List<MilitaryUnitDispatcherEntity> lst = new List<MilitaryUnitDispatcherEntity>();
            //string key = "MilitaryUnitDispatcher_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MilitaryUnitDispatcherEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMilitaryUnitDispatcherCollectionFromMilitaryUnitDispatcherDBList(
                _MilitaryUnitDispatcherDB.GetAllIsActiveMilitaryUnitDispatcherDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<MilitaryUnitDispatcherEntity> GetAllPaging(int currentPage, int pageSize,
                                                               string

                                                                   sortExpression, out int count, string whereClause)
        {
            //string key = "MilitaryUnitDispatcher_List_Page_" + currentPage ;
            //string countKey = "MilitaryUnitDispatcher_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<MilitaryUnitDispatcherEntity> lst = new List<MilitaryUnitDispatcherEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MilitaryUnitDispatcherEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetMilitaryUnitDispatcherCollectionFromMilitaryUnitDispatcherDBList(
                _MilitaryUnitDispatcherDB.GetPageMilitaryUnitDispatcherDB(pageSize, currentPage,
                                                                          whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private MilitaryUnitDispatcherEntity GetMilitaryUnitDispatcherFromMilitaryUnitDispatcherDB(MilitaryUnitDispatcherEntity o)
        {
            if (o == null)
                return null;
            MilitaryUnitDispatcherEntity ret = new MilitaryUnitDispatcherEntity(o.MilitaryUnitDispatcherId, o.MilitaryUnitDispatcherTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<MilitaryUnitDispatcherEntity> GetMilitaryUnitDispatcherCollectionFromMilitaryUnitDispatcherDBList(List<MilitaryUnitDispatcherEntity> lst)
        {
            List<MilitaryUnitDispatcherEntity> RetLst = new List<MilitaryUnitDispatcherEntity>();
            foreach (MilitaryUnitDispatcherEntity o in lst)
            {
                RetLst.Add(GetMilitaryUnitDispatcherFromMilitaryUnitDispatcherDB(o));
            }
            return RetLst;

        }


    }
}