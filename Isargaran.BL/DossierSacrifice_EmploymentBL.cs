﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/24>
    /// Description: <پرونده مشخصات استخدام ایثارگری>
    /// </summary>  


    public class DossierSacrifice_EmploymentBL
    {
        private readonly DossierSacrifice_EmploymentDB _DossierSacrifice_EmploymentDB;

        public DossierSacrifice_EmploymentBL()
        {
            _DossierSacrifice_EmploymentDB = new DossierSacrifice_EmploymentDB();
        }

        public void Add(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam,
                        out Guid DossierSacrifice_EmploymentId)
        {
            _DossierSacrifice_EmploymentDB.AddDossierSacrifice_EmploymentDB(DossierSacrifice_EmploymentEntityParam,
                                                                            out DossierSacrifice_EmploymentId);
        }

        public void Update(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            _DossierSacrifice_EmploymentDB.UpdateDossierSacrifice_EmploymentDB(DossierSacrifice_EmploymentEntityParam);
        }

        public void Delete(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            _DossierSacrifice_EmploymentDB.DeleteDossierSacrifice_EmploymentDB(DossierSacrifice_EmploymentEntityParam);
        }

        public DossierSacrifice_EmploymentEntity GetSingleById(
            DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            DossierSacrifice_EmploymentEntity o = GetDossierSacrifice_EmploymentFromDossierSacrifice_EmploymentDB(
                _DossierSacrifice_EmploymentDB.GetSingleDossierSacrifice_EmploymentDB(
                    DossierSacrifice_EmploymentEntityParam));

            return o;
        }

        public List<DossierSacrifice_EmploymentEntity> GetAll()
        {
            List<DossierSacrifice_EmploymentEntity> lst = new List<DossierSacrifice_EmploymentEntity>();
            //string key = "DossierSacrifice_Employment_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_EmploymentEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
                _DossierSacrifice_EmploymentDB.GetAllDossierSacrifice_EmploymentDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_EmploymentEntity> GetAllPaging(int currentPage, int pageSize,
                                                                    string

                                                                        sortExpression, out int count,
                                                                    string whereClause)
        {
            //string key = "DossierSacrifice_Employment_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Employment_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_EmploymentEntity> lst = new List<DossierSacrifice_EmploymentEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_EmploymentEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
                _DossierSacrifice_EmploymentDB.GetPageDossierSacrifice_EmploymentDB(pageSize, currentPage,
                                                                                    whereClause, sortExpression,
                                                                                    out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_EmploymentEntity GetDossierSacrifice_EmploymentFromDossierSacrifice_EmploymentDB(
            DossierSacrifice_EmploymentEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_EmploymentEntity ret =
                new DossierSacrifice_EmploymentEntity(o.DossierSacrifice_EmploymentId, o.DossierSacrificeId,
                                                      o.OrganizationPhysicalChartId, o.EmploymentTypeId,
                                                      o.EmploymentDate, o.PostId, o.PersonnelNo, o.CommandmentNo,
                                                      o.FreeJob, o.DurationTimeAtWorkId, o.DirectManager, o.CityId,
                                                      o.TelNo, o.FaxNo, o.Address, o.PostalCod, o.Email, o.WebAddress,
                                                      o.StartDate, o.EndDate, o.CreationDate, o.ModificationDate,o.ProvinceId,o.OrganizationPhysicalChartTitle);
            return ret;
        }

        private List<DossierSacrifice_EmploymentEntity>
            GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
            List<DossierSacrifice_EmploymentEntity> lst)
        {
            List<DossierSacrifice_EmploymentEntity> RetLst = new List<DossierSacrifice_EmploymentEntity>();
            foreach (DossierSacrifice_EmploymentEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_EmploymentFromDossierSacrifice_EmploymentDB(o));
            }
            return RetLst;

        }



        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentCollectionByDossierSacrifice(
            DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            return
                GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
                    _DossierSacrifice_EmploymentDB.GetDossierSacrifice_EmploymentDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_EmploymentEntityParam));
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentCollectionByDurationTimeAtWork(
            DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            return
                GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
                    _DossierSacrifice_EmploymentDB.GetDossierSacrifice_EmploymentDBCollectionByDurationTimeAtWorkDB(
                        DossierSacrifice_EmploymentEntityParam));
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentCollectionByEmploymentType(
            DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            return
                GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
                    _DossierSacrifice_EmploymentDB.GetDossierSacrifice_EmploymentDBCollectionByEmploymentTypeDB(
                        DossierSacrifice_EmploymentEntityParam));
        }

        public List<DossierSacrifice_EmploymentEntity>
            GetDossierSacrifice_EmploymentCollectionByOrganizationPhysicalChart(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            return
                GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
                    _DossierSacrifice_EmploymentDB.
                        GetDossierSacrifice_EmploymentDBCollectionByOrganizationPhysicalChartDB(
                            DossierSacrifice_EmploymentEntityParam));
        }

        public List<DossierSacrifice_EmploymentEntity> GetDossierSacrifice_EmploymentCollectionByPost(DossierSacrifice_EmploymentEntity DossierSacrifice_EmploymentEntityParam)
        {
            return
                GetDossierSacrifice_EmploymentCollectionFromDossierSacrifice_EmploymentDBList(
                    _DossierSacrifice_EmploymentDB.GetDossierSacrifice_EmploymentDBCollectionByPostDB(DossierSacrifice_EmploymentEntityParam));
        }




    }

}
