﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<مناسبت>
    /// </summary>
	public class OccasionBL 
	{	
	  	 private readonly OccasionDB _OccasionDB;					
			
		public OccasionBL()
		{
			_OccasionDB = new OccasionDB();
		}			
	
		public  void Add(OccasionEntity  OccasionEntityParam, out Guid OccasionId)
		{ 
			_OccasionDB.AddOccasionDB(OccasionEntityParam,out OccasionId);			
		}

		public  void Update(OccasionEntity  OccasionEntityParam)
		{
			_OccasionDB.UpdateOccasionDB(OccasionEntityParam);		
		}

		public  void Delete(OccasionEntity  OccasionEntityParam)
		{
			 _OccasionDB.DeleteOccasionDB(OccasionEntityParam);			
		}

		public  OccasionEntity GetSingleById(OccasionEntity  OccasionEntityParam)
		{
			OccasionEntity o = GetOccasionFromOccasionDB(
			_OccasionDB.GetSingleOccasionDB(OccasionEntityParam));
			
			return o;
		}

		public  List<OccasionEntity> GetAll()
		{
			List<OccasionEntity> lst = new List<OccasionEntity>();
			//string key = "Occasion_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OccasionEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetOccasionCollectionFromOccasionDBList(
				_OccasionDB.GetAllOccasionDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<OccasionEntity> GetAllIsActive()
        {
            List<OccasionEntity> lst = new List<OccasionEntity>();
            //string key = "Occasion_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OccasionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOccasionCollectionFromOccasionDBList(
            _OccasionDB.GetAllOccasionIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<OccasionEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Occasion_List_Page_" + currentPage ;
			//string countKey = "Occasion_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<OccasionEntity> lst = new List<OccasionEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OccasionEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetOccasionCollectionFromOccasionDBList(
				_OccasionDB.GetPageOccasionDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  OccasionEntity GetOccasionFromOccasionDB(OccasionEntity o)
		{
	if(o == null)
                return null;
			OccasionEntity ret = new OccasionEntity(o.OccasionId ,o.OccasionTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<OccasionEntity> GetOccasionCollectionFromOccasionDBList( List<OccasionEntity> lst)
		{
			List<OccasionEntity> RetLst = new List<OccasionEntity>();
			foreach(OccasionEntity o in lst)
			{
				RetLst.Add(GetOccasionFromOccasionDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}


