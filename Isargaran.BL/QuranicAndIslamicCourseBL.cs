﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <رشته قرانی و معارف>
    /// </summary>

    public class QuranicAndIslamicCourseBL
    {
        private readonly QuranicAndIslamicCourseDB _QuranicAndIslamicCourseDB;

        public QuranicAndIslamicCourseBL()
        {
            _QuranicAndIslamicCourseDB = new QuranicAndIslamicCourseDB();
        }

        public void Add(QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam,
                        out Guid QuranicAndIslamicCourseId)
        {
            _QuranicAndIslamicCourseDB.AddQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntityParam,
                                                                    out QuranicAndIslamicCourseId);
        }

        public void Update(QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam)
        {
            _QuranicAndIslamicCourseDB.UpdateQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntityParam);
        }

        public void Delete(QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam)
        {
            _QuranicAndIslamicCourseDB.DeleteQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntityParam);
        }

        public QuranicAndIslamicCourseEntity GetSingleById(
            QuranicAndIslamicCourseEntity QuranicAndIslamicCourseEntityParam)
        {
            QuranicAndIslamicCourseEntity o = GetQuranicAndIslamicCourseFromQuranicAndIslamicCourseDB(
                _QuranicAndIslamicCourseDB.GetSingleQuranicAndIslamicCourseDB(QuranicAndIslamicCourseEntityParam));

            return o;
        }

        public List<QuranicAndIslamicCourseEntity> GetAll()
        {
            List<QuranicAndIslamicCourseEntity> lst = new List<QuranicAndIslamicCourseEntity>();
            //string key = "QuranicAndIslamicCourse_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<QuranicAndIslamicCourseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetQuranicAndIslamicCourseCollectionFromQuranicAndIslamicCourseDBList(
                _QuranicAndIslamicCourseDB.GetAllQuranicAndIslamicCourseDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<QuranicAndIslamicCourseEntity> GetAllIsActive()
        {
            List<QuranicAndIslamicCourseEntity> lst = new List<QuranicAndIslamicCourseEntity>();
            //string key = "QuranicAndIslamicCourse_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<QuranicAndIslamicCourseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetQuranicAndIslamicCourseCollectionFromQuranicAndIslamicCourseDBList(
                _QuranicAndIslamicCourseDB.GetAllIsActiveQuranicAndIslamicCourseDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<QuranicAndIslamicCourseEntity> GetAllPaging(int currentPage, int pageSize,
                                                                string

                                                                    sortExpression, out int count, string whereClause)
        {
            //string key = "QuranicAndIslamicCourse_List_Page_" + currentPage ;
            //string countKey = "QuranicAndIslamicCourse_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<QuranicAndIslamicCourseEntity> lst = new List<QuranicAndIslamicCourseEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<QuranicAndIslamicCourseEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetQuranicAndIslamicCourseCollectionFromQuranicAndIslamicCourseDBList(
                _QuranicAndIslamicCourseDB.GetPageQuranicAndIslamicCourseDB(pageSize, currentPage,
                                                                            whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private QuranicAndIslamicCourseEntity GetQuranicAndIslamicCourseFromQuranicAndIslamicCourseDB(
            QuranicAndIslamicCourseEntity o)
        {
            if (o == null)
                return null;
            QuranicAndIslamicCourseEntity ret = new QuranicAndIslamicCourseEntity(o.QuranicAndIslamicCourseId,
                                                                                  o.QuranicAndIslamicCourseTitle,
                                                                                  o.CreationDate, o.ModificationDate,
                                                                                  o.IsActive);
            return ret;
        }

        private List<QuranicAndIslamicCourseEntity>
            GetQuranicAndIslamicCourseCollectionFromQuranicAndIslamicCourseDBList(
            List<QuranicAndIslamicCourseEntity> lst)
        {
            List<QuranicAndIslamicCourseEntity> RetLst = new List<QuranicAndIslamicCourseEntity>();
            foreach (QuranicAndIslamicCourseEntity o in lst)
            {
                RetLst.Add(GetQuranicAndIslamicCourseFromQuranicAndIslamicCourseDB(o));
            }
            return RetLst;
        }
    }
}


