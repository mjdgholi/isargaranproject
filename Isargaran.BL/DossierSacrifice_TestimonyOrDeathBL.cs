﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/23>
    // Description:	<اطلاعات فوت یا شهادت>
    /// </summary>

    //-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class DossierSacrifice_TestimonyOrDeathBL
    {
        private readonly DossierSacrifice_TestimonyOrDeathDB _DossierSacrifice_TestimonyOrDeathDB;        

        public DossierSacrifice_TestimonyOrDeathBL()
        {
            _DossierSacrifice_TestimonyOrDeathDB = new DossierSacrifice_TestimonyOrDeathDB();
        }


        public void Add(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam, out Guid DossierSacrifice_TestimonyOrDeathId)
        {
            _DossierSacrifice_TestimonyOrDeathDB.AddDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntityParam, out DossierSacrifice_TestimonyOrDeathId);
        }

        public void Update(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam)
        {
            _DossierSacrifice_TestimonyOrDeathDB.UpdateDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntityParam);
        }

        public void Delete(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam)
        {
            _DossierSacrifice_TestimonyOrDeathDB.DeleteDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntityParam);
        }

        public DossierSacrifice_TestimonyOrDeathEntity GetSingleById(DossierSacrifice_TestimonyOrDeathEntity DossierSacrifice_TestimonyOrDeathEntityParam)
        {
            DossierSacrifice_TestimonyOrDeathEntity o = GetDossierSacrifice_TestimonyOrDeathFromDossierSacrifice_TestimonyOrDeathDB(
                _DossierSacrifice_TestimonyOrDeathDB.GetSingleDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntityParam));

            return o;
        }

        public List<DossierSacrifice_TestimonyOrDeathEntity> GetAll()
        {
            List<DossierSacrifice_TestimonyOrDeathEntity> lst = new List<DossierSacrifice_TestimonyOrDeathEntity>();
            //string key = "DossierSacrifice_TestimonyOrDeath_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_TestimonyOrDeathEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_TestimonyOrDeathCollectionFromDossierSacrifice_TestimonyOrDeathDBList(
                _DossierSacrifice_TestimonyOrDeathDB.GetAllDossierSacrifice_TestimonyOrDeathDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_TestimonyOrDeathEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_TestimonyOrDeath_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_TestimonyOrDeath_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_TestimonyOrDeathEntity> lst = new List<DossierSacrifice_TestimonyOrDeathEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_TestimonyOrDeathEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_TestimonyOrDeathCollectionFromDossierSacrifice_TestimonyOrDeathDBList(
                _DossierSacrifice_TestimonyOrDeathDB.GetPageDossierSacrifice_TestimonyOrDeathDB(pageSize, currentPage,
                                                                                                whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static DossierSacrifice_TestimonyOrDeathEntity GetDossierSacrifice_TestimonyOrDeathFromDossierSacrifice_TestimonyOrDeathDB(DossierSacrifice_TestimonyOrDeathEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_TestimonyOrDeathEntity ret = new DossierSacrifice_TestimonyOrDeathEntity(o.DossierSacrifice_TestimonyOrDeathId, o.DossierSacrificeId, o.DaethDate, o.CityId, o.VillageTitle, o.TombTitle, o.PartNo, o.RowNo, o.GraveNo, o.TestimonyLocation, o.DurationOfPresenceInBattlefield, o.EucationDegreeId, o.EducationCourseId, o.OrganizationPhysicalChartId, o.PostId, o.MilitaryUnitDispatcherId, o.IsWarDisappeared, o.TestimonyPeriodTimeId,o.CauseEventTypeId ,o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathCollectionFromDossierSacrifice_TestimonyOrDeathDBList(List<DossierSacrifice_TestimonyOrDeathEntity> lst)
        {
            List<DossierSacrifice_TestimonyOrDeathEntity> RetLst = new List<DossierSacrifice_TestimonyOrDeathEntity>();
            foreach (DossierSacrifice_TestimonyOrDeathEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_TestimonyOrDeathFromDossierSacrifice_TestimonyOrDeathDB(o));
            }
            return RetLst;

        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathCollectionByDossierSacrifice(Guid DossierSacrificeId)
        {
            return GetDossierSacrifice_TestimonyOrDeathCollectionFromDossierSacrifice_TestimonyOrDeathDBList(DossierSacrifice_TestimonyOrDeathDB.GetDossierSacrifice_TestimonyOrDeathDBCollectionByDossierSacrificeDB(DossierSacrificeId));
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathCollectionByEducationDegree(Guid EucationDegreeId)
        {
            return GetDossierSacrifice_TestimonyOrDeathCollectionFromDossierSacrifice_TestimonyOrDeathDBList(DossierSacrifice_TestimonyOrDeathDB.GetDossierSacrifice_TestimonyOrDeathDBCollectionByEducationDegreeDB(EucationDegreeId));
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathCollectionByMilitaryUnitDispatcher(Guid MilitaryUnitDispatcherId)
        {
            return GetDossierSacrifice_TestimonyOrDeathCollectionFromDossierSacrifice_TestimonyOrDeathDBList(DossierSacrifice_TestimonyOrDeathDB.GetDossierSacrifice_TestimonyOrDeathDBCollectionByMilitaryUnitDispatcherDB(MilitaryUnitDispatcherId));
        }

        public static List<DossierSacrifice_TestimonyOrDeathEntity> GetDossierSacrifice_TestimonyOrDeathCollectionByTestimonyPeriodTime(Guid TestimonyPeriodTimeId)
        {
            return GetDossierSacrifice_TestimonyOrDeathCollectionFromDossierSacrifice_TestimonyOrDeathDBList(DossierSacrifice_TestimonyOrDeathDB.GetDossierSacrifice_TestimonyOrDeathDBCollectionByTestimonyPeriodTimeDB(TestimonyPeriodTimeId));
        }
    }
}