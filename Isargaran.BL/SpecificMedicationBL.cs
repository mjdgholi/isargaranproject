﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/02/16>
    // Description:	<داروی خاص>
    /// </summary>
    public class SpecificMedicationBL
    {
        private readonly SpecificMedicationDB _SpecificMedicationDB;

        public SpecificMedicationBL()
        {
            _SpecificMedicationDB = new SpecificMedicationDB();
        }

        public void Add(SpecificMedicationEntity SpecificMedicationEntityParam, out Guid SpecificMedicationId)
        {
            _SpecificMedicationDB.AddSpecificMedicationDB(SpecificMedicationEntityParam, out SpecificMedicationId);
        }

        public void Update(SpecificMedicationEntity SpecificMedicationEntityParam)
        {
            _SpecificMedicationDB.UpdateSpecificMedicationDB(SpecificMedicationEntityParam);
        }

        public void Delete(SpecificMedicationEntity SpecificMedicationEntityParam)
        {
            _SpecificMedicationDB.DeleteSpecificMedicationDB(SpecificMedicationEntityParam);
        }

        public SpecificMedicationEntity GetSingleById(SpecificMedicationEntity SpecificMedicationEntityParam)
        {
            SpecificMedicationEntity o = GetSpecificMedicationFromSpecificMedicationDB(
                _SpecificMedicationDB.GetSingleSpecificMedicationDB(SpecificMedicationEntityParam));

            return o;
        }

        public List<SpecificMedicationEntity> GetAll()
        {
            List<SpecificMedicationEntity> lst = new List<SpecificMedicationEntity>();
            //string key = "SpecificMedication_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SpecificMedicationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSpecificMedicationCollectionFromSpecificMedicationDBList(
                _SpecificMedicationDB.GetAllSpecificMedicationDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<SpecificMedicationEntity> GetAllIsActive()
        {
            List<SpecificMedicationEntity> lst = new List<SpecificMedicationEntity>();
            //string key = "SpecificMedication_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SpecificMedicationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSpecificMedicationCollectionFromSpecificMedicationDBList(
                _SpecificMedicationDB.GetAllIsActiveSpecificMedicationDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<SpecificMedicationEntity> GetAllPaging(int currentPage, int pageSize,
                                                           string

                                                               sortExpression, out int count, string whereClause)
        {
            //string key = "SpecificMedication_List_Page_" + currentPage ;
            //string countKey = "SpecificMedication_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<SpecificMedicationEntity> lst = new List<SpecificMedicationEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SpecificMedicationEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetSpecificMedicationCollectionFromSpecificMedicationDBList(
                _SpecificMedicationDB.GetPageSpecificMedicationDB(pageSize, currentPage,
                                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private SpecificMedicationEntity GetSpecificMedicationFromSpecificMedicationDB(SpecificMedicationEntity o)
        {
            if (o == null)
                return null;
            SpecificMedicationEntity ret = new SpecificMedicationEntity(o.SpecificMedicationId, o.SpecificMedicationPersianTitle, o.SpecificMedicationEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<SpecificMedicationEntity> GetSpecificMedicationCollectionFromSpecificMedicationDBList(List<SpecificMedicationEntity> lst)
        {
            List<SpecificMedicationEntity> RetLst = new List<SpecificMedicationEntity>();
            foreach (SpecificMedicationEntity o in lst)
            {
                RetLst.Add(GetSpecificMedicationFromSpecificMedicationDB(o));
            }
            return RetLst;

        }


    }

}