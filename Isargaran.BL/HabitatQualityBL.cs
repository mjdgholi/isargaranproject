﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<کیفیت محل اقامت>
    /// </summary>

	public class HabitatQualityBL 
	{	
	  	 private readonly HabitatQualityDB _HabitatQualityDB;					
			
		public HabitatQualityBL()
		{
			_HabitatQualityDB = new HabitatQualityDB();
		}			
	
		public  void Add(HabitatQualityEntity  HabitatQualityEntityParam, out Guid HabitatQualityId)
		{ 
			_HabitatQualityDB.AddHabitatQualityDB(HabitatQualityEntityParam,out HabitatQualityId);			
		}

		public  void Update(HabitatQualityEntity  HabitatQualityEntityParam)
		{
			_HabitatQualityDB.UpdateHabitatQualityDB(HabitatQualityEntityParam);		
		}

		public  void Delete(HabitatQualityEntity  HabitatQualityEntityParam)
		{
			 _HabitatQualityDB.DeleteHabitatQualityDB(HabitatQualityEntityParam);			
		}

		public  HabitatQualityEntity GetSingleById(HabitatQualityEntity  HabitatQualityEntityParam)
		{
			HabitatQualityEntity o = GetHabitatQualityFromHabitatQualityDB(
			_HabitatQualityDB.GetSingleHabitatQualityDB(HabitatQualityEntityParam));
			
			return o;
		}

		public  List<HabitatQualityEntity> GetAll()
		{
			List<HabitatQualityEntity> lst = new List<HabitatQualityEntity>();
			//string key = "HabitatQuality_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<HabitatQualityEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetHabitatQualityCollectionFromHabitatQualityDBList(
				_HabitatQualityDB.GetAllHabitatQualityDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<HabitatQualityEntity> GetAllIsActive()
        {
            List<HabitatQualityEntity> lst = new List<HabitatQualityEntity>();
            //string key = "HabitatQuality_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<HabitatQualityEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetHabitatQualityCollectionFromHabitatQualityDBList(
            _HabitatQualityDB.GetAllHabitatQualityIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<HabitatQualityEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "HabitatQuality_List_Page_" + currentPage ;
			//string countKey = "HabitatQuality_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<HabitatQualityEntity> lst = new List<HabitatQualityEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<HabitatQualityEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetHabitatQualityCollectionFromHabitatQualityDBList(
				_HabitatQualityDB.GetPageHabitatQualityDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  HabitatQualityEntity GetHabitatQualityFromHabitatQualityDB(HabitatQualityEntity o)
		{
	if(o == null)
                return null;
			HabitatQualityEntity ret = new HabitatQualityEntity(o.HabitatQualityId ,o.HabitatQualityTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<HabitatQualityEntity> GetHabitatQualityCollectionFromHabitatQualityDBList( List<HabitatQualityEntity> lst)
		{
			List<HabitatQualityEntity> RetLst = new List<HabitatQualityEntity>();
			foreach(HabitatQualityEntity o in lst)
			{
				RetLst.Add(GetHabitatQualityFromHabitatQualityDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}

