﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <  مقطع تحصیلی>
    /// </summary>


	public class EducationDegreeBL 
	{	
	  	 private readonly EducationDegreeDB _EducationDegreeDB;					
			
		public EducationDegreeBL()
		{
			_EducationDegreeDB = new EducationDegreeDB();
		}			
	
		public  void Add(EducationDegreeEntity  EducationDegreeEntityParam, out Guid EucationDegreeId)
		{ 
			_EducationDegreeDB.AddEducationDegreeDB(EducationDegreeEntityParam,out EucationDegreeId);			
		}

		public  void Update(EducationDegreeEntity  EducationDegreeEntityParam)
		{
			_EducationDegreeDB.UpdateEducationDegreeDB(EducationDegreeEntityParam);		
		}

		public  void Delete(EducationDegreeEntity  EducationDegreeEntityParam)
		{
			 _EducationDegreeDB.DeleteEducationDegreeDB(EducationDegreeEntityParam);			
		}

		public  EducationDegreeEntity GetSingleById(EducationDegreeEntity  EducationDegreeEntityParam)
		{
			EducationDegreeEntity o = GetEducationDegreeFromEducationDegreeDB(
			_EducationDegreeDB.GetSingleEducationDegreeDB(EducationDegreeEntityParam));
			
			return o;
		}

		public  List<EducationDegreeEntity> GetAll()
		{
			List<EducationDegreeEntity> lst = new List<EducationDegreeEntity>();
			//string key = "EducationDegree_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationDegreeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetEducationDegreeCollectionFromEducationDegreeDBList(
				_EducationDegreeDB.GetAllEducationDegreeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<EducationDegreeEntity> GetAllIsActive()
        {
            List<EducationDegreeEntity> lst = new List<EducationDegreeEntity>();
            //string key = "EducationDegree_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationDegreeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationDegreeCollectionFromEducationDegreeDBList(
            _EducationDegreeDB.GetAllEducationDegreeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<EducationDegreeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "EducationDegree_List_Page_" + currentPage ;
			//string countKey = "EducationDegree_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<EducationDegreeEntity> lst = new List<EducationDegreeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationDegreeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetEducationDegreeCollectionFromEducationDegreeDBList(
				_EducationDegreeDB.GetPageEducationDegreeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  EducationDegreeEntity GetEducationDegreeFromEducationDegreeDB(EducationDegreeEntity o)
		{
	if(o == null)
                return null;
			EducationDegreeEntity ret = new EducationDegreeEntity(o.EucationDegreeId ,o.EducationSystemId ,o.EucationDegreePersianTitle ,o.EucationDegreeEnglishTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<EducationDegreeEntity> GetEducationDegreeCollectionFromEducationDegreeDBList( List<EducationDegreeEntity> lst)
		{
			List<EducationDegreeEntity> RetLst = new List<EducationDegreeEntity>();
			foreach(EducationDegreeEntity o in lst)
			{
				RetLst.Add(GetEducationDegreeFromEducationDegreeDB(o));
			}
			return RetLst;
			
		}




        public List<EducationDegreeEntity> GetEducationDegreeCollectionByEducationSystem(EducationDegreeEntity EducationDegreeEntityParam)
{
    return GetEducationDegreeCollectionFromEducationDegreeDBList(_EducationDegreeDB.GetEducationDegreeDBCollectionByEducationSystemDB(EducationDegreeEntityParam));
}



}

}
