﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<نوع سازه محل اقامت >
    /// </summary>


    public class HabitatEstateTypeBL
    {
        private readonly HabitatEstateTypeDB _HabitatEstateTypeDB;

        public HabitatEstateTypeBL()
        {
            _HabitatEstateTypeDB = new HabitatEstateTypeDB();
        }

        public void Add(HabitatEstateTypeEntity HabitatEstateTypeEntityParam, out Guid HabitatEstateTypeId)
        {
            _HabitatEstateTypeDB.AddHabitatEstateTypeDB(HabitatEstateTypeEntityParam, out HabitatEstateTypeId);
        }

        public void Update(HabitatEstateTypeEntity HabitatEstateTypeEntityParam)
        {
            _HabitatEstateTypeDB.UpdateHabitatEstateTypeDB(HabitatEstateTypeEntityParam);
        }

        public void Delete(HabitatEstateTypeEntity HabitatEstateTypeEntityParam)
        {
            _HabitatEstateTypeDB.DeleteHabitatEstateTypeDB(HabitatEstateTypeEntityParam);
        }

        public HabitatEstateTypeEntity GetSingleById(HabitatEstateTypeEntity HabitatEstateTypeEntityParam)
        {
            HabitatEstateTypeEntity o = GetHabitatEstateTypeFromHabitatEstateTypeDB(
                _HabitatEstateTypeDB.GetSingleHabitatEstateTypeDB(HabitatEstateTypeEntityParam));

            return o;
        }

        public List<HabitatEstateTypeEntity> GetAll()
        {
            List<HabitatEstateTypeEntity> lst = new List<HabitatEstateTypeEntity>();
            //string key = "HabitatEstateType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<HabitatEstateTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetHabitatEstateTypeCollectionFromHabitatEstateTypeDBList(
                _HabitatEstateTypeDB.GetAllHabitatEstateTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<HabitatEstateTypeEntity> GetAllIsActive()
        {
            List<HabitatEstateTypeEntity> lst = new List<HabitatEstateTypeEntity>();
            //string key = "HabitatEstateType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<HabitatEstateTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetHabitatEstateTypeCollectionFromHabitatEstateTypeDBList(
                _HabitatEstateTypeDB.GetAllHabitatEstateTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<HabitatEstateTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                          string

                                                              sortExpression, out int count, string whereClause)
        {
            //string key = "HabitatEstateType_List_Page_" + currentPage ;
            //string countKey = "HabitatEstateType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<HabitatEstateTypeEntity> lst = new List<HabitatEstateTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<HabitatEstateTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetHabitatEstateTypeCollectionFromHabitatEstateTypeDBList(
                _HabitatEstateTypeDB.GetPageHabitatEstateTypeDB(pageSize, currentPage,
                                                                whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private HabitatEstateTypeEntity GetHabitatEstateTypeFromHabitatEstateTypeDB(HabitatEstateTypeEntity o)
        {
            if (o == null)
                return null;
            HabitatEstateTypeEntity ret = new HabitatEstateTypeEntity(o.HabitatEstateTypeId, o.HabitatEstateTypeTitle,
                                                                      o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<HabitatEstateTypeEntity> GetHabitatEstateTypeCollectionFromHabitatEstateTypeDBList(
            List<HabitatEstateTypeEntity> lst)
        {
            List<HabitatEstateTypeEntity> RetLst = new List<HabitatEstateTypeEntity>();
            foreach (HabitatEstateTypeEntity o in lst)
            {
                RetLst.Add(GetHabitatEstateTypeFromHabitatEstateTypeDB(o));
            }
            return RetLst;

        }


    }
}
