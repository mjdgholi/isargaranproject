﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/02>
    /// Description: <نوع حادثه>
    /// </summary>

	public class CauseEventTypeBL 
	{	
	  	 private readonly CauseEventTypeDB _CauseEventTypeDB;					
			
		public CauseEventTypeBL()
		{
			_CauseEventTypeDB = new CauseEventTypeDB();
		}			
	
		public  void Add(CauseEventTypeEntity  CauseEventTypeEntityParam, out Guid CauseEventTypeId)
		{ 
			_CauseEventTypeDB.AddCauseEventTypeDB(CauseEventTypeEntityParam,out CauseEventTypeId);			
		}

		public  void Update(CauseEventTypeEntity  CauseEventTypeEntityParam)
		{
			_CauseEventTypeDB.UpdateCauseEventTypeDB(CauseEventTypeEntityParam);		
		}

		public  void Delete(CauseEventTypeEntity  CauseEventTypeEntityParam)
		{
			 _CauseEventTypeDB.DeleteCauseEventTypeDB(CauseEventTypeEntityParam);			
		}

		public  CauseEventTypeEntity GetSingleById(CauseEventTypeEntity  CauseEventTypeEntityParam)
		{
			CauseEventTypeEntity o = GetCauseEventTypeFromCauseEventTypeDB(
			_CauseEventTypeDB.GetSingleCauseEventTypeDB(CauseEventTypeEntityParam));
			
			return o;
		}

		public  List<CauseEventTypeEntity> GetAll()
		{
			List<CauseEventTypeEntity> lst = new List<CauseEventTypeEntity>();
			//string key = "CauseEventType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CauseEventTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetCauseEventTypeCollectionFromCauseEventTypeDBList(
				_CauseEventTypeDB.GetAllCauseEventTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<CauseEventTypeEntity> GetAllIsActive()
        {
            List<CauseEventTypeEntity> lst = new List<CauseEventTypeEntity>();
            //string key = "CauseEventType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CauseEventTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetCauseEventTypeCollectionFromCauseEventTypeDBList(
            _CauseEventTypeDB.GetAllCauseEventTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<CauseEventTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "CauseEventType_List_Page_" + currentPage ;
			//string countKey = "CauseEventType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<CauseEventTypeEntity> lst = new List<CauseEventTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CauseEventTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetCauseEventTypeCollectionFromCauseEventTypeDBList(
				_CauseEventTypeDB.GetPageCauseEventTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  CauseEventTypeEntity GetCauseEventTypeFromCauseEventTypeDB(CauseEventTypeEntity o)
		{
	if(o == null)
                return null;
			CauseEventTypeEntity ret = new CauseEventTypeEntity(o.CauseEventTypeId ,o.CauseEventTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<CauseEventTypeEntity> GetCauseEventTypeCollectionFromCauseEventTypeDBList( List<CauseEventTypeEntity> lst)
		{
			List<CauseEventTypeEntity> RetLst = new List<CauseEventTypeEntity>();
			foreach(CauseEventTypeEntity o in lst)
			{
				RetLst.Add(GetCauseEventTypeFromCauseEventTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
}

}
