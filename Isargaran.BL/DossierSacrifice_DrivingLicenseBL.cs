﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<اطلاعات گواهینامه رانندگی>
    /// </summary>


    //-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class DossierSacrifice_DrivingLicenseBL
    {
        private readonly DossierSacrifice_DrivingLicenseDB _DossierSacrifice_DrivingLicenseDB;

        private void Validation(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam)
        {
            if (DossierSacrifice_DrivingLicenseEntityParam.IsHasDrivingLicense && DossierSacrifice_DrivingLicenseEntityParam.CardNo.Trim()=="")
            {
                throw new ItcApplicationErrorManagerException("شماره گواهینامه را وارد نمایید");
            }
        }

        public DossierSacrifice_DrivingLicenseBL()
        {
            _DossierSacrifice_DrivingLicenseDB = new DossierSacrifice_DrivingLicenseDB();
        }

        public void Add(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam, out Guid DossierSacrifice_DrivingLicenseId)
        {
            Validation(DossierSacrifice_DrivingLicenseEntityParam);
            _DossierSacrifice_DrivingLicenseDB.AddDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntityParam, out DossierSacrifice_DrivingLicenseId);
        }

        public void Update(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam)
        {
            Validation(DossierSacrifice_DrivingLicenseEntityParam);
            _DossierSacrifice_DrivingLicenseDB.UpdateDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntityParam);
        }

        public void Delete(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam)
        {
            _DossierSacrifice_DrivingLicenseDB.DeleteDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntityParam);
        }

        public DossierSacrifice_DrivingLicenseEntity GetSingleById(DossierSacrifice_DrivingLicenseEntity DossierSacrifice_DrivingLicenseEntityParam)
        {
            DossierSacrifice_DrivingLicenseEntity o = GetDossierSacrifice_DrivingLicenseFromDossierSacrifice_DrivingLicenseDB(
                _DossierSacrifice_DrivingLicenseDB.GetSingleDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntityParam));

            return o;
        }

        public List<DossierSacrifice_DrivingLicenseEntity> GetAll()
        {
            List<DossierSacrifice_DrivingLicenseEntity> lst = new List<DossierSacrifice_DrivingLicenseEntity>();
            //string key = "DossierSacrifice_DrivingLicense_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_DrivingLicenseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_DrivingLicenseCollectionFromDossierSacrifice_DrivingLicenseDBList(
                _DossierSacrifice_DrivingLicenseDB.GetAllDossierSacrifice_DrivingLicenseDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_DrivingLicenseEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_DrivingLicense_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_DrivingLicense_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_DrivingLicenseEntity> lst = new List<DossierSacrifice_DrivingLicenseEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_DrivingLicenseEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_DrivingLicenseCollectionFromDossierSacrifice_DrivingLicenseDBList(
                _DossierSacrifice_DrivingLicenseDB.GetPageDossierSacrifice_DrivingLicenseDB(pageSize, currentPage,
                                                                                            whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static DossierSacrifice_DrivingLicenseEntity GetDossierSacrifice_DrivingLicenseFromDossierSacrifice_DrivingLicenseDB(DossierSacrifice_DrivingLicenseEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_DrivingLicenseEntity ret = new DossierSacrifice_DrivingLicenseEntity(o.DossierSacrifice_DrivingLicenseId, o.DossierSacrificeId, o.IsHasDrivingLicense, o.CardNo, o.CardIssueDate, o.CardExpiredDate, o.DrivingLicenseTypeId, o.DrivingRestrictionId, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseCollectionFromDossierSacrifice_DrivingLicenseDBList(List<DossierSacrifice_DrivingLicenseEntity> lst)
        {
            List<DossierSacrifice_DrivingLicenseEntity> RetLst = new List<DossierSacrifice_DrivingLicenseEntity>();
            foreach (DossierSacrifice_DrivingLicenseEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_DrivingLicenseFromDossierSacrifice_DrivingLicenseDB(o));
            }
            return RetLst;

        }

        public static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseCollectionByDossierSacrifice(Guid DossierSacrificeId)
        {
            return GetDossierSacrifice_DrivingLicenseCollectionFromDossierSacrifice_DrivingLicenseDBList(DossierSacrifice_DrivingLicenseDB.GetDossierSacrifice_DrivingLicenseDBCollectionByDossierSacrificeDB(DossierSacrificeId));
        }

        public static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseCollectionByDrivingLicenseType(Guid DrivingLicenseTypeId)
        {
            return GetDossierSacrifice_DrivingLicenseCollectionFromDossierSacrifice_DrivingLicenseDBList(DossierSacrifice_DrivingLicenseDB.GetDossierSacrifice_DrivingLicenseDBCollectionByDrivingLicenseTypeDB(DrivingLicenseTypeId));
        }

        public static List<DossierSacrifice_DrivingLicenseEntity> GetDossierSacrifice_DrivingLicenseCollectionByDrivingRestriction(Guid DrivingRestrictionId)
        {
            return GetDossierSacrifice_DrivingLicenseCollectionFromDossierSacrifice_DrivingLicenseDBList(DossierSacrifice_DrivingLicenseDB.GetDossierSacrifice_DrivingLicenseDBCollectionByDrivingRestrictionDB(DrivingRestrictionId));
        }
    }
}