﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/18>
    // Description:	<نوع وسیله حمل و نقل>
    /// </summary>

    public class VehicleTypeBL
    {
        private readonly VehicleTypeDB _VehicleTypeDB;

        public VehicleTypeBL()
        {
            _VehicleTypeDB = new VehicleTypeDB();
        }

        public void Add(VehicleTypeEntity VehicleTypeEntityParam, out Guid VehicleTypeId)
        {
            _VehicleTypeDB.AddVehicleTypeDB(VehicleTypeEntityParam, out VehicleTypeId);
        }

        public void Update(VehicleTypeEntity VehicleTypeEntityParam)
        {
            _VehicleTypeDB.UpdateVehicleTypeDB(VehicleTypeEntityParam);
        }

        public void Delete(VehicleTypeEntity VehicleTypeEntityParam)
        {
            _VehicleTypeDB.DeleteVehicleTypeDB(VehicleTypeEntityParam);
        }

        public VehicleTypeEntity GetSingleById(VehicleTypeEntity VehicleTypeEntityParam)
        {
            VehicleTypeEntity o = GetVehicleTypeFromVehicleTypeDB(
                _VehicleTypeDB.GetSingleVehicleTypeDB(VehicleTypeEntityParam));

            return o;
        }

        public List<VehicleTypeEntity> GetAll()
        {
            List<VehicleTypeEntity> lst = new List<VehicleTypeEntity>();
            //string key = "VehicleType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<VehicleTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetVehicleTypeCollectionFromVehicleTypeDBList(
                _VehicleTypeDB.GetAllVehicleTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<VehicleTypeEntity> GetAllIsActive()
        {
            List<VehicleTypeEntity> lst = new List<VehicleTypeEntity>();
            //string key = "VehicleType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<VehicleTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetVehicleTypeCollectionFromVehicleTypeDBList(
                _VehicleTypeDB.GetAllVehicleTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<VehicleTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                    string

                                                        sortExpression, out int count, string whereClause)
        {
            //string key = "VehicleType_List_Page_" + currentPage ;
            //string countKey = "VehicleType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<VehicleTypeEntity> lst = new List<VehicleTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<VehicleTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetVehicleTypeCollectionFromVehicleTypeDBList(
                _VehicleTypeDB.GetPageVehicleTypeDB(pageSize, currentPage,
                                                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private VehicleTypeEntity GetVehicleTypeFromVehicleTypeDB(VehicleTypeEntity o)
        {
            if (o == null)
                return null;
            VehicleTypeEntity ret = new VehicleTypeEntity(o.VehicleTypeId, o.VehicleTypeTitle, o.CreationDate,
                                                          o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<VehicleTypeEntity> GetVehicleTypeCollectionFromVehicleTypeDBList(List<VehicleTypeEntity> lst)
        {
            List<VehicleTypeEntity> RetLst = new List<VehicleTypeEntity>();
            foreach (VehicleTypeEntity o in lst)
            {
                RetLst.Add(GetVehicleTypeFromVehicleTypeDB(o));
            }
            return RetLst;

        }


    }
}
