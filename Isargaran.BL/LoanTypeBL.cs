﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <majid.Gholibeygian>
    /// Create date: <1393/02/22>
    /// Description: <نوع وام>
    /// </summary>
    public class LoanTypeBL
    {
        private readonly LoanTypeDB _LoanTypeDB;

        public LoanTypeBL()
        {
            _LoanTypeDB = new LoanTypeDB();
        }

        public void Add(LoanTypeEntity LoanTypeEntityParam, out Guid LoanTypeId)
        {
            _LoanTypeDB.AddLoanTypeDB(LoanTypeEntityParam, out LoanTypeId);
        }

        public void Update(LoanTypeEntity LoanTypeEntityParam)
        {
            _LoanTypeDB.UpdateLoanTypeDB(LoanTypeEntityParam);
        }

        public void Delete(LoanTypeEntity LoanTypeEntityParam)
        {
            _LoanTypeDB.DeleteLoanTypeDB(LoanTypeEntityParam);
        }

        public LoanTypeEntity GetSingleById(LoanTypeEntity LoanTypeEntityParam)
        {
            LoanTypeEntity o = GetLoanTypeFromLoanTypeDB(
                _LoanTypeDB.GetSingleLoanTypeDB(LoanTypeEntityParam));

            return o;
        }

        public List<LoanTypeEntity> GetAll()
        {
            List<LoanTypeEntity> lst = new List<LoanTypeEntity>();
            //string key = "LoanType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<LoanTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetLoanTypeCollectionFromLoanTypeDBList(
                _LoanTypeDB.GetAllLoanTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<LoanTypeEntity> GetAllIsActive()
        {
            List<LoanTypeEntity> lst = new List<LoanTypeEntity>();
            //string key = "LoanType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<LoanTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetLoanTypeCollectionFromLoanTypeDBList(
                _LoanTypeDB.GetAllIsActiveLoanTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<LoanTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                 string

                                                     sortExpression, out int count, string whereClause)
        {
            //string key = "LoanType_List_Page_" + currentPage ;
            //string countKey = "LoanType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<LoanTypeEntity> lst = new List<LoanTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<LoanTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetLoanTypeCollectionFromLoanTypeDBList(
                _LoanTypeDB.GetPageLoanTypeDB(pageSize, currentPage,
                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private LoanTypeEntity GetLoanTypeFromLoanTypeDB(LoanTypeEntity o)
        {
            if (o == null)
                return null;
            LoanTypeEntity ret = new LoanTypeEntity(o.LoanTypeId, o.LoanTypeTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<LoanTypeEntity> GetLoanTypeCollectionFromLoanTypeDBList(List<LoanTypeEntity> lst)
        {
            List<LoanTypeEntity> RetLst = new List<LoanTypeEntity>();
            foreach (LoanTypeEntity o in lst)
            {
                RetLst.Add(GetLoanTypeFromLoanTypeDB(o));
            }
            return RetLst;

        }


    }

}