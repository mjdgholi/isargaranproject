﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <اطلاعات سوابق تدریس>
    /// </summary>

    public class DossierSacrifice_TeachingExperienceBL
    {
        private readonly DossierSacrifice_TeachingExperienceDB _DossierSacrifice_TeachingExperienceDB;

        public DossierSacrifice_TeachingExperienceBL()
        {
            _DossierSacrifice_TeachingExperienceDB = new DossierSacrifice_TeachingExperienceDB();
        }

        public void Add(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam,
                        out Guid DossierSacrifice_TeachingExperienceId)
        {
            _DossierSacrifice_TeachingExperienceDB.AddDossierSacrifice_TeachingExperienceDB(
                DossierSacrifice_TeachingExperienceEntityParam, out DossierSacrifice_TeachingExperienceId);
        }

        public void Update(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            _DossierSacrifice_TeachingExperienceDB.UpdateDossierSacrifice_TeachingExperienceDB(
                DossierSacrifice_TeachingExperienceEntityParam);
        }

        public void Delete(DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            _DossierSacrifice_TeachingExperienceDB.DeleteDossierSacrifice_TeachingExperienceDB(
                DossierSacrifice_TeachingExperienceEntityParam);
        }

        public DossierSacrifice_TeachingExperienceEntity GetSingleById(
            DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            DossierSacrifice_TeachingExperienceEntity o = GetDossierSacrifice_TeachingExperienceFromDossierSacrifice_TeachingExperienceDB
                (
                    _DossierSacrifice_TeachingExperienceDB.GetSingleDossierSacrifice_TeachingExperienceDB(
                        DossierSacrifice_TeachingExperienceEntityParam));

            return o;
        }

        public List<DossierSacrifice_TeachingExperienceEntity> GetAll()
        {
            List<DossierSacrifice_TeachingExperienceEntity> lst = new List<DossierSacrifice_TeachingExperienceEntity>();
            //string key = "DossierSacrifice_TeachingExperience_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_TeachingExperienceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_TeachingExperienceCollectionFromDossierSacrifice_TeachingExperienceDBList(
                _DossierSacrifice_TeachingExperienceDB.GetAllDossierSacrifice_TeachingExperienceDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_TeachingExperienceEntity> GetAllPaging(int currentPage, int pageSize,
                                                                            string

                                                                                sortExpression, out int count,
                                                                            string whereClause)
        {
            //string key = "DossierSacrifice_TeachingExperience_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_TeachingExperience_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_TeachingExperienceEntity> lst = new List<DossierSacrifice_TeachingExperienceEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_TeachingExperienceEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_TeachingExperienceCollectionFromDossierSacrifice_TeachingExperienceDBList(
                _DossierSacrifice_TeachingExperienceDB.GetPageDossierSacrifice_TeachingExperienceDB(pageSize,
                                                                                                    currentPage,
                                                                                                    whereClause,
                                                                                                    sortExpression,
                                                                                                    out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_TeachingExperienceEntity
            GetDossierSacrifice_TeachingExperienceFromDossierSacrifice_TeachingExperienceDB(
            DossierSacrifice_TeachingExperienceEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_TeachingExperienceEntity ret =
                new DossierSacrifice_TeachingExperienceEntity(o.DossierSacrifice_TeachingExperienceId,
                                                              o.DossierSacrificeId, o.LessonId, o.TeachingDurationYearNo,
                                                              o.IsNowTeaching, o.TeachingLocation, o.Description,
                                                              o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_TeachingExperienceEntity>
            GetDossierSacrifice_TeachingExperienceCollectionFromDossierSacrifice_TeachingExperienceDBList(
            List<DossierSacrifice_TeachingExperienceEntity> lst)
        {
            List<DossierSacrifice_TeachingExperienceEntity> RetLst =
                new List<DossierSacrifice_TeachingExperienceEntity>();
            foreach (DossierSacrifice_TeachingExperienceEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_TeachingExperienceFromDossierSacrifice_TeachingExperienceDB(o));
            }
            return RetLst;

        }



        public List<DossierSacrifice_TeachingExperienceEntity>
            GetDossierSacrifice_TeachingExperienceCollectionByDossierSacrifice(
            DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            return
                GetDossierSacrifice_TeachingExperienceCollectionFromDossierSacrifice_TeachingExperienceDBList(
                    _DossierSacrifice_TeachingExperienceDB.
                        GetDossierSacrifice_TeachingExperienceDBCollectionByDossierSacrificeDB(
                            DossierSacrifice_TeachingExperienceEntityParam));
        }

        public List<DossierSacrifice_TeachingExperienceEntity> GetDossierSacrifice_TeachingExperienceCollectionByLesson(
            DossierSacrifice_TeachingExperienceEntity DossierSacrifice_TeachingExperienceEntityParam)
        {
            return
                GetDossierSacrifice_TeachingExperienceCollectionFromDossierSacrifice_TeachingExperienceDBList(
                    _DossierSacrifice_TeachingExperienceDB.GetDossierSacrifice_TeachingExperienceDBCollectionByLessonDB(
                        DossierSacrifice_TeachingExperienceEntityParam));
        }


    }

}
