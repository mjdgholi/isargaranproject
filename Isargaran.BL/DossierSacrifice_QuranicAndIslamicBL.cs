﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <پرونده معارف و علوم قرآنی ایثارگر>
    /// </summary>


	public class DossierSacrifice_QuranicAndIslamicBL 
	{	
	  	 private readonly DossierSacrifice_QuranicAndIslamicDB _DossierSacrifice_QuranicAndIslamicDB;					
			
		public DossierSacrifice_QuranicAndIslamicBL()
		{
			_DossierSacrifice_QuranicAndIslamicDB = new DossierSacrifice_QuranicAndIslamicDB();
		}			
	
		public  void Add(DossierSacrifice_QuranicAndIslamicEntity  DossierSacrifice_QuranicAndIslamicEntityParam, out Guid DossierSacrifice_QuranicAndIslamicId)
		{ 
			_DossierSacrifice_QuranicAndIslamicDB.AddDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntityParam,out DossierSacrifice_QuranicAndIslamicId);			
		}

		public  void Update(DossierSacrifice_QuranicAndIslamicEntity  DossierSacrifice_QuranicAndIslamicEntityParam)
		{
			_DossierSacrifice_QuranicAndIslamicDB.UpdateDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntityParam);		
		}

		public  void Delete(DossierSacrifice_QuranicAndIslamicEntity  DossierSacrifice_QuranicAndIslamicEntityParam)
		{
			 _DossierSacrifice_QuranicAndIslamicDB.DeleteDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntityParam);			
		}

		public  DossierSacrifice_QuranicAndIslamicEntity GetSingleById(DossierSacrifice_QuranicAndIslamicEntity  DossierSacrifice_QuranicAndIslamicEntityParam)
		{
			DossierSacrifice_QuranicAndIslamicEntity o = GetDossierSacrifice_QuranicAndIslamicFromDossierSacrifice_QuranicAndIslamicDB(
			_DossierSacrifice_QuranicAndIslamicDB.GetSingleDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_QuranicAndIslamicEntity> GetAll()
		{
			List<DossierSacrifice_QuranicAndIslamicEntity> lst = new List<DossierSacrifice_QuranicAndIslamicEntity>();
			//string key = "DossierSacrifice_QuranicAndIslamic_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_QuranicAndIslamicEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_QuranicAndIslamicCollectionFromDossierSacrifice_QuranicAndIslamicDBList(
				_DossierSacrifice_QuranicAndIslamicDB.GetAllDossierSacrifice_QuranicAndIslamicDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_QuranicAndIslamicEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_QuranicAndIslamic_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_QuranicAndIslamic_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_QuranicAndIslamicEntity> lst = new List<DossierSacrifice_QuranicAndIslamicEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_QuranicAndIslamicEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_QuranicAndIslamicCollectionFromDossierSacrifice_QuranicAndIslamicDBList(
				_DossierSacrifice_QuranicAndIslamicDB.GetPageDossierSacrifice_QuranicAndIslamicDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_QuranicAndIslamicEntity GetDossierSacrifice_QuranicAndIslamicFromDossierSacrifice_QuranicAndIslamicDB(DossierSacrifice_QuranicAndIslamicEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_QuranicAndIslamicEntity ret = new DossierSacrifice_QuranicAndIslamicEntity(o.DossierSacrifice_QuranicAndIslamicId ,o.DossierSacrificeId ,o.QuranicAndIslamicCourseId ,o.ExperienceYearNo ,o.HasEvidence ,o.PlaceOfGraduation ,o.Description ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<DossierSacrifice_QuranicAndIslamicEntity> GetDossierSacrifice_QuranicAndIslamicCollectionFromDossierSacrifice_QuranicAndIslamicDBList( List<DossierSacrifice_QuranicAndIslamicEntity> lst)
		{
			List<DossierSacrifice_QuranicAndIslamicEntity> RetLst = new List<DossierSacrifice_QuranicAndIslamicEntity>();
			foreach(DossierSacrifice_QuranicAndIslamicEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_QuranicAndIslamicFromDossierSacrifice_QuranicAndIslamicDB(o));
			}
			return RetLst;
			
		}


        public List<DossierSacrifice_QuranicAndIslamicEntity> GetDossierSacrifice_QuranicAndIslamicCollectionByDossierSacrifice(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam)
{
    return GetDossierSacrifice_QuranicAndIslamicCollectionFromDossierSacrifice_QuranicAndIslamicDBList(_DossierSacrifice_QuranicAndIslamicDB.GetDossierSacrifice_QuranicAndIslamicDBCollectionByDossierSacrificeDB(DossierSacrifice_QuranicAndIslamicEntityParam));
}

        public List<DossierSacrifice_QuranicAndIslamicEntity> GetDossierSacrifice_QuranicAndIslamicCollectionByQuranicAndIslamicCourse(DossierSacrifice_QuranicAndIslamicEntity DossierSacrifice_QuranicAndIslamicEntityParam)
{
    return GetDossierSacrifice_QuranicAndIslamicCollectionFromDossierSacrifice_QuranicAndIslamicDBList(_DossierSacrifice_QuranicAndIslamicDB.GetDossierSacrifice_QuranicAndIslamicDBCollectionByQuranicAndIslamicCourseDB(DossierSacrifice_QuranicAndIslamicEntityParam));
}




}

}
