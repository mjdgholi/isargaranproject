﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <مشخصات درس>
    /// </summary>

	public class LessonBL 
	{	
	  	 private readonly LessonDB _LessonDB;					
			
		public LessonBL()
		{
			_LessonDB = new LessonDB();
		}			
	
		public  void Add(LessonEntity  LessonEntityParam, out Guid LessonId)
		{ 
			_LessonDB.AddLessonDB(LessonEntityParam,out LessonId);			
		}

		public  void Update(LessonEntity  LessonEntityParam)
		{
			_LessonDB.UpdateLessonDB(LessonEntityParam);		
		}

		public  void Delete(LessonEntity  LessonEntityParam)
		{
			 _LessonDB.DeleteLessonDB(LessonEntityParam);			
		}

		public  LessonEntity GetSingleById(LessonEntity  LessonEntityParam)
		{
			LessonEntity o = GetLessonFromLessonDB(
			_LessonDB.GetSingleLessonDB(LessonEntityParam));
			
			return o;
		}

		public  List<LessonEntity> GetAll()
		{
			List<LessonEntity> lst = new List<LessonEntity>();
			//string key = "Lesson_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<LessonEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetLessonCollectionFromLessonDBList(
				_LessonDB.GetAllLessonDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<LessonEntity> GetAllIsActive()
        {
            List<LessonEntity> lst = new List<LessonEntity>();
            //string key = "Lesson_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<LessonEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetLessonCollectionFromLessonDBList(
            _LessonDB.GetAllIsActiveLessonDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<LessonEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Lesson_List_Page_" + currentPage ;
			//string countKey = "Lesson_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<LessonEntity> lst = new List<LessonEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<LessonEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetLessonCollectionFromLessonDBList(
				_LessonDB.GetPageLessonDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  LessonEntity GetLessonFromLessonDB(LessonEntity o)
		{
	if(o == null)
                return null;
			LessonEntity ret = new LessonEntity(o.LessonId ,o.LessonTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<LessonEntity> GetLessonCollectionFromLessonDBList( List<LessonEntity> lst)
		{
			List<LessonEntity> RetLst = new List<LessonEntity>();
			foreach(LessonEntity o in lst)
			{
				RetLst.Add(GetLessonFromLessonDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}
