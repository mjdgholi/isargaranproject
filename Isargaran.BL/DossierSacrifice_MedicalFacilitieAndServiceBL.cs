﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/18>
    /// Description: <پرونده مشخصات تسهیلات و خدمات پزشکی>
    /// </summary>  

    public class DossierSacrifice_MedicalFacilitieAndServiceBL
    {
        private readonly DossierSacrifice_MedicalFacilitieAndServiceDB _DossierSacrifice_MedicalFacilitieAndServiceDB;

        public DossierSacrifice_MedicalFacilitieAndServiceBL()
        {
            _DossierSacrifice_MedicalFacilitieAndServiceDB = new DossierSacrifice_MedicalFacilitieAndServiceDB();
        }

        public void Add(
            DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam,
            out Guid DossierSacrifice_MedicalFacilitieAndServiceId)
        {
            _DossierSacrifice_MedicalFacilitieAndServiceDB.AddDossierSacrifice_MedicalFacilitieAndServiceDB(
                DossierSacrifice_MedicalFacilitieAndServiceEntityParam,
                out DossierSacrifice_MedicalFacilitieAndServiceId);
        }

        public void Update(
            DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            _DossierSacrifice_MedicalFacilitieAndServiceDB.UpdateDossierSacrifice_MedicalFacilitieAndServiceDB(
                DossierSacrifice_MedicalFacilitieAndServiceEntityParam);
        }

        public void Delete(
            DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            _DossierSacrifice_MedicalFacilitieAndServiceDB.DeleteDossierSacrifice_MedicalFacilitieAndServiceDB(
                DossierSacrifice_MedicalFacilitieAndServiceEntityParam);
        }

        public DossierSacrifice_MedicalFacilitieAndServiceEntity GetSingleById(
            DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            DossierSacrifice_MedicalFacilitieAndServiceEntity o = GetDossierSacrifice_MedicalFacilitieAndServiceFromDossierSacrifice_MedicalFacilitieAndServiceDB
                (
                    _DossierSacrifice_MedicalFacilitieAndServiceDB.
                        GetSingleDossierSacrifice_MedicalFacilitieAndServiceDB(
                            DossierSacrifice_MedicalFacilitieAndServiceEntityParam));

            return o;
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetAll()
        {
            List<DossierSacrifice_MedicalFacilitieAndServiceEntity> lst =
                new List<DossierSacrifice_MedicalFacilitieAndServiceEntity>();
            //string key = "DossierSacrifice_MedicalFacilitieAndService_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_MedicalFacilitieAndServiceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_MedicalFacilitieAndServiceCollectionFromDossierSacrifice_MedicalFacilitieAndServiceDBList
                (
                    _DossierSacrifice_MedicalFacilitieAndServiceDB.GetAllDossierSacrifice_MedicalFacilitieAndServiceDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity> GetAllPaging(int currentPage, int pageSize,
                                                                                    string

                                                                                        sortExpression, out int count,
                                                                                    string whereClause)
        {
            //string key = "DossierSacrifice_MedicalFacilitieAndService_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_MedicalFacilitieAndService_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_MedicalFacilitieAndServiceEntity> lst =
                new List<DossierSacrifice_MedicalFacilitieAndServiceEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_MedicalFacilitieAndServiceEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_MedicalFacilitieAndServiceCollectionFromDossierSacrifice_MedicalFacilitieAndServiceDBList
                (
                    _DossierSacrifice_MedicalFacilitieAndServiceDB.GetPageDossierSacrifice_MedicalFacilitieAndServiceDB(
                        pageSize, currentPage,
                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_MedicalFacilitieAndServiceEntity
            GetDossierSacrifice_MedicalFacilitieAndServiceFromDossierSacrifice_MedicalFacilitieAndServiceDB(
            DossierSacrifice_MedicalFacilitieAndServiceEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_MedicalFacilitieAndServiceEntity ret =
                new DossierSacrifice_MedicalFacilitieAndServiceEntity(o.DossierSacrifice_MedicalFacilitieAndServiceId,
                                                                      o.DossierSacrificeId,
                                                                      o.MedicalFacilitieAndServiceId,
                                                                      o.MedicalFacilitieAndServiceDate, o.FinancialCost,
                                                                      o.Description, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_MedicalFacilitieAndServiceEntity>
            GetDossierSacrifice_MedicalFacilitieAndServiceCollectionFromDossierSacrifice_MedicalFacilitieAndServiceDBList
            (List<DossierSacrifice_MedicalFacilitieAndServiceEntity> lst)
        {
            List<DossierSacrifice_MedicalFacilitieAndServiceEntity> RetLst =
                new List<DossierSacrifice_MedicalFacilitieAndServiceEntity>();
            foreach (DossierSacrifice_MedicalFacilitieAndServiceEntity o in lst)
            {
                RetLst.Add(
                    GetDossierSacrifice_MedicalFacilitieAndServiceFromDossierSacrifice_MedicalFacilitieAndServiceDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity>
            GetDossierSacrifice_MedicalFacilitieAndServiceCollectionByDossierSacrifice(
            DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            return
                GetDossierSacrifice_MedicalFacilitieAndServiceCollectionFromDossierSacrifice_MedicalFacilitieAndServiceDBList
                    (_DossierSacrifice_MedicalFacilitieAndServiceDB.
                         GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionByDossierSacrificeDB(
                             DossierSacrifice_MedicalFacilitieAndServiceEntityParam));
        }

        public List<DossierSacrifice_MedicalFacilitieAndServiceEntity>
            GetDossierSacrifice_MedicalFacilitieAndServiceCollectionByMedicalFacilitieAndService(
            DossierSacrifice_MedicalFacilitieAndServiceEntity DossierSacrifice_MedicalFacilitieAndServiceEntityParam)
        {
            return
                GetDossierSacrifice_MedicalFacilitieAndServiceCollectionFromDossierSacrifice_MedicalFacilitieAndServiceDBList
                    (_DossierSacrifice_MedicalFacilitieAndServiceDB.
                         GetDossierSacrifice_MedicalFacilitieAndServiceDBCollectionByMedicalFacilitieAndServiceDB(
                             DossierSacrifice_MedicalFacilitieAndServiceEntityParam));
        }




    }

}
