﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<محدودیت رانندگی>
    /// </summary>
    public class DrivingRestrictionBL
    {
        private readonly DrivingRestrictionDB _DrivingRestrictionDB;

        public DrivingRestrictionBL()
        {
            _DrivingRestrictionDB = new DrivingRestrictionDB();
        }

        public void Add(DrivingRestrictionEntity DrivingRestrictionEntityParam, out Guid DrivingRestrictionId)
        {
            _DrivingRestrictionDB.AddDrivingRestrictionDB(DrivingRestrictionEntityParam, out DrivingRestrictionId);
        }

        public void Update(DrivingRestrictionEntity DrivingRestrictionEntityParam)
        {
            _DrivingRestrictionDB.UpdateDrivingRestrictionDB(DrivingRestrictionEntityParam);
        }

        public void Delete(DrivingRestrictionEntity DrivingRestrictionEntityParam)
        {
            _DrivingRestrictionDB.DeleteDrivingRestrictionDB(DrivingRestrictionEntityParam);
        }

        public DrivingRestrictionEntity GetSingleById(DrivingRestrictionEntity DrivingRestrictionEntityParam)
        {
            DrivingRestrictionEntity o = GetDrivingRestrictionFromDrivingRestrictionDB(
                _DrivingRestrictionDB.GetSingleDrivingRestrictionDB(DrivingRestrictionEntityParam));

            return o;
        }

        public List<DrivingRestrictionEntity> GetAll()
        {
            List<DrivingRestrictionEntity> lst = new List<DrivingRestrictionEntity>();
            //string key = "DrivingRestriction_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DrivingRestrictionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDrivingRestrictionCollectionFromDrivingRestrictionDBList(
                _DrivingRestrictionDB.GetAllDrivingRestrictionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DrivingRestrictionEntity> GetAllIsActive()
        {
            List<DrivingRestrictionEntity> lst = new List<DrivingRestrictionEntity>();
            //string key = "DrivingRestriction_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DrivingRestrictionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDrivingRestrictionCollectionFromDrivingRestrictionDBList(
                _DrivingRestrictionDB.GetAllIsActiveDrivingRestrictionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DrivingRestrictionEntity> GetAllPaging(int currentPage, int pageSize,
                                                           string

                                                               sortExpression, out int count, string whereClause)
        {
            //string key = "DrivingRestriction_List_Page_" + currentPage ;
            //string countKey = "DrivingRestriction_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DrivingRestrictionEntity> lst = new List<DrivingRestrictionEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DrivingRestrictionEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDrivingRestrictionCollectionFromDrivingRestrictionDBList(
                _DrivingRestrictionDB.GetPageDrivingRestrictionDB(pageSize, currentPage,
                                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DrivingRestrictionEntity GetDrivingRestrictionFromDrivingRestrictionDB(DrivingRestrictionEntity o)
        {
            if (o == null)
                return null;
            DrivingRestrictionEntity ret = new DrivingRestrictionEntity(o.DrivingRestrictionId, o.DrivingRestrictionTitlePersian, o.DrivingRestrictionTitleEnglish, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<DrivingRestrictionEntity> GetDrivingRestrictionCollectionFromDrivingRestrictionDBList(List<DrivingRestrictionEntity> lst)
        {
            List<DrivingRestrictionEntity> RetLst = new List<DrivingRestrictionEntity>();
            foreach (DrivingRestrictionEntity o in lst)
            {
                RetLst.Add(GetDrivingRestrictionFromDrivingRestrictionDB(o));
            }
            return RetLst;

        }


    }

}