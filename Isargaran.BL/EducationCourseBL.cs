﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <رشته تحصیلی>
    /// </summary>


    public class EducationCourseBL
    {
        private readonly EducationCourseDB _EducationCourseDB;

        public EducationCourseBL()
        {
            _EducationCourseDB = new EducationCourseDB();
        }

        public void Add(EducationCourseEntity EducationCourseEntityParam, out Guid EducationCourseId)
        {
            _EducationCourseDB.AddEducationCourseDB(EducationCourseEntityParam, out EducationCourseId);
        }

        public void Update(EducationCourseEntity EducationCourseEntityParam)
        {
            _EducationCourseDB.UpdateEducationCourseDB(EducationCourseEntityParam);
        }

        public void Delete(EducationCourseEntity EducationCourseEntityParam)
        {
            _EducationCourseDB.DeleteEducationCourseDB(EducationCourseEntityParam);
        }

        public EducationCourseEntity GetSingleById(EducationCourseEntity EducationCourseEntityParam)
        {
            EducationCourseEntity o = GetEducationCourseFromEducationCourseDB(
                _EducationCourseDB.GetSingleEducationCourseDB(EducationCourseEntityParam));

            return o;
        }

        public List<EducationCourseEntity> GetAll()
        {
            List<EducationCourseEntity> lst = new List<EducationCourseEntity>();
            //string key = "EducationCourse_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCourseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationCourseCollectionFromEducationCourseDBList(
                _EducationCourseDB.GetAllEducationCourseDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<EducationCourseEntity> GetAllIsActive()
        {
            List<EducationCourseEntity> lst = new List<EducationCourseEntity>();
            //string key = "EducationCourse_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCourseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationCourseCollectionFromEducationCourseDBList(
                _EducationCourseDB.GetAllEducationCourseDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<EducationCourseEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "EducationCourse_List_Page_" + currentPage ;
            //string countKey = "EducationCourse_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<EducationCourseEntity> lst = new List<EducationCourseEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCourseEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetEducationCourseCollectionFromEducationCourseDBList(
                _EducationCourseDB.GetPageEducationCourseDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private EducationCourseEntity GetEducationCourseFromEducationCourseDB(EducationCourseEntity o)
        {
            if (o == null)
                return null;
            EducationCourseEntity ret = new EducationCourseEntity(o.EducationCourseId, o.EducationCoursePersianTitle,
                o.EducationCourseEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<EducationCourseEntity> GetEducationCourseCollectionFromEducationCourseDBList(
            List<EducationCourseEntity> lst)
        {
            List<EducationCourseEntity> RetLst = new List<EducationCourseEntity>();
            foreach (EducationCourseEntity o in lst)
            {
                RetLst.Add(GetEducationCourseFromEducationCourseDB(o));
            }
            return RetLst;

        }


    }

}
