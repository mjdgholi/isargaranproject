﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	< نوع گواهینامه رانندگی(پایه)>
    /// </summary>

//-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class DrivingLicenseTypeBL
    {
        private readonly DrivingLicenseTypeDB _DrivingLicenseTypeDB;

        public DrivingLicenseTypeBL()
        {
            _DrivingLicenseTypeDB = new DrivingLicenseTypeDB();
        }

        public void Add(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam, out Guid DrivingLicenseTypeId)
        {
            _DrivingLicenseTypeDB.AddDrivingLicenseTypeDB(DrivingLicenseTypeEntityParam, out DrivingLicenseTypeId);
        }

        public void Update(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam)
        {
            _DrivingLicenseTypeDB.UpdateDrivingLicenseTypeDB(DrivingLicenseTypeEntityParam);
        }

        public void Delete(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam)
        {
            _DrivingLicenseTypeDB.DeleteDrivingLicenseTypeDB(DrivingLicenseTypeEntityParam);
        }

        public DrivingLicenseTypeEntity GetSingleById(DrivingLicenseTypeEntity DrivingLicenseTypeEntityParam)
        {
            DrivingLicenseTypeEntity o = GetDrivingLicenseTypeFromDrivingLicenseTypeDB(
                _DrivingLicenseTypeDB.GetSingleDrivingLicenseTypeDB(DrivingLicenseTypeEntityParam));

            return o;
        }

        public List<DrivingLicenseTypeEntity> GetAll()
        {
            List<DrivingLicenseTypeEntity> lst = new List<DrivingLicenseTypeEntity>();
            //string key = "DrivingLicenseType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DrivingLicenseTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDrivingLicenseTypeCollectionFromDrivingLicenseTypeDBList(
                _DrivingLicenseTypeDB.GetAllDrivingLicenseTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DrivingLicenseTypeEntity> GetAllIsActive()
        {
            List<DrivingLicenseTypeEntity> lst = new List<DrivingLicenseTypeEntity>();
            //string key = "DrivingLicenseType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DrivingLicenseTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDrivingLicenseTypeCollectionFromDrivingLicenseTypeDBList(
                _DrivingLicenseTypeDB.GetAllIsActiveDrivingLicenseTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DrivingLicenseTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                           string

                                                               sortExpression, out int count, string whereClause)
        {
            //string key = "DrivingLicenseType_List_Page_" + currentPage ;
            //string countKey = "DrivingLicenseType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DrivingLicenseTypeEntity> lst = new List<DrivingLicenseTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DrivingLicenseTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDrivingLicenseTypeCollectionFromDrivingLicenseTypeDBList(
                _DrivingLicenseTypeDB.GetPageDrivingLicenseTypeDB(pageSize, currentPage,
                                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DrivingLicenseTypeEntity GetDrivingLicenseTypeFromDrivingLicenseTypeDB(DrivingLicenseTypeEntity o)
        {
            if (o == null)
                return null;
            DrivingLicenseTypeEntity ret = new DrivingLicenseTypeEntity(o.DrivingLicenseTypeId, o.DrivingLicenseTypeTitlePersian, o.DrivingLicenseTypeTitleEnglish, o.Priority, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<DrivingLicenseTypeEntity> GetDrivingLicenseTypeCollectionFromDrivingLicenseTypeDBList(List<DrivingLicenseTypeEntity> lst)
        {
            List<DrivingLicenseTypeEntity> RetLst = new List<DrivingLicenseTypeEntity>();
            foreach (DrivingLicenseTypeEntity o in lst)
            {
                RetLst.Add(GetDrivingLicenseTypeFromDrivingLicenseTypeDB(o));
            }
            return RetLst;

        }


    }

}