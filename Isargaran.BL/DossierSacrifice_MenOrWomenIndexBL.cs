﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات زن یا مرد شاخص>
    /// </summary>

    //-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class DossierSacrifice_MenOrWomenIndexBL
    {
        private readonly DossierSacrifice_MenOrWomenIndexDB _DossierSacrifice_MenOrWomenIndexDB;

        public DossierSacrifice_MenOrWomenIndexBL()
        {
            _DossierSacrifice_MenOrWomenIndexDB = new DossierSacrifice_MenOrWomenIndexDB();
        }

        public void Add(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam,
                        out Guid DossierSacrifice_MenOrWomenIndexId)
        {
            _DossierSacrifice_MenOrWomenIndexDB.AddDossierSacrifice_MenOrWomenIndexDB(
                DossierSacrifice_MenOrWomenIndexEntityParam, out DossierSacrifice_MenOrWomenIndexId);
        }

        public void Update(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            _DossierSacrifice_MenOrWomenIndexDB.UpdateDossierSacrifice_MenOrWomenIndexDB(
                DossierSacrifice_MenOrWomenIndexEntityParam);
        }

        public void Delete(DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            _DossierSacrifice_MenOrWomenIndexDB.DeleteDossierSacrifice_MenOrWomenIndexDB(
                DossierSacrifice_MenOrWomenIndexEntityParam);
        }

        public DossierSacrifice_MenOrWomenIndexEntity GetSingleById(
            DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            DossierSacrifice_MenOrWomenIndexEntity o = GetDossierSacrifice_MenOrWomenIndexFromDossierSacrifice_MenOrWomenIndexDB
                (
                    _DossierSacrifice_MenOrWomenIndexDB.GetSingleDossierSacrifice_MenOrWomenIndexDB(
                        DossierSacrifice_MenOrWomenIndexEntityParam));

            return o;
        }

        public List<DossierSacrifice_MenOrWomenIndexEntity> GetAll()
        {
            List<DossierSacrifice_MenOrWomenIndexEntity> lst = new List<DossierSacrifice_MenOrWomenIndexEntity>();
            //string key = "DossierSacrifice_MenOrWomenIndex_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_MenOrWomenIndexEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_MenOrWomenIndexCollectionFromDossierSacrifice_MenOrWomenIndexDBList(
                _DossierSacrifice_MenOrWomenIndexDB.GetAllDossierSacrifice_MenOrWomenIndexDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_MenOrWomenIndexEntity> GetAllPaging(int currentPage, int pageSize,
                                                                         string

                                                                             sortExpression, out int count,
                                                                         string whereClause)
        {
            //string key = "DossierSacrifice_MenOrWomenIndex_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_MenOrWomenIndex_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_MenOrWomenIndexEntity> lst = new List<DossierSacrifice_MenOrWomenIndexEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_MenOrWomenIndexEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_MenOrWomenIndexCollectionFromDossierSacrifice_MenOrWomenIndexDBList(
                _DossierSacrifice_MenOrWomenIndexDB.GetPageDossierSacrifice_MenOrWomenIndexDB(pageSize, currentPage,
                                                                                              whereClause,
                                                                                              sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_MenOrWomenIndexEntity
            GetDossierSacrifice_MenOrWomenIndexFromDossierSacrifice_MenOrWomenIndexDB(
            DossierSacrifice_MenOrWomenIndexEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_MenOrWomenIndexEntity ret =
                new DossierSacrifice_MenOrWomenIndexEntity(o.DossierSacrifice_MenOrWomenIndexId, o.DossierSacrificeId,
                                                           o.OccasionId, o.OccasionDate, o.IndexFactors, o.Description,
                                                           o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_MenOrWomenIndexEntity>
            GetDossierSacrifice_MenOrWomenIndexCollectionFromDossierSacrifice_MenOrWomenIndexDBList(
            List<DossierSacrifice_MenOrWomenIndexEntity> lst)
        {
            List<DossierSacrifice_MenOrWomenIndexEntity> RetLst = new List<DossierSacrifice_MenOrWomenIndexEntity>();
            foreach (DossierSacrifice_MenOrWomenIndexEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_MenOrWomenIndexFromDossierSacrifice_MenOrWomenIndexDB(o));
            }
            return RetLst;

        }



        public List<DossierSacrifice_MenOrWomenIndexEntity> GetDossierSacrifice_MenOrWomenIndexCollectionByOccasion(
            DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            return
                GetDossierSacrifice_MenOrWomenIndexCollectionFromDossierSacrifice_MenOrWomenIndexDBList(
                    _DossierSacrifice_MenOrWomenIndexDB.GetDossierSacrifice_MenOrWomenIndexDBCollectionByOccasionDB(
                        DossierSacrifice_MenOrWomenIndexEntityParam));
        }

        public List<DossierSacrifice_MenOrWomenIndexEntity>
            GetDossierSacrifice_MenOrWomenIndexCollectionByDossierSacrifice(
            DossierSacrifice_MenOrWomenIndexEntity DossierSacrifice_MenOrWomenIndexEntityParam)
        {
            return
                GetDossierSacrifice_MenOrWomenIndexCollectionFromDossierSacrifice_MenOrWomenIndexDBList(
                    _DossierSacrifice_MenOrWomenIndexDB.
                        GetDossierSacrifice_MenOrWomenIndexDBCollectionByDossierSacrificeDB(
                            DossierSacrifice_MenOrWomenIndexEntityParam));
        }




    }

}
