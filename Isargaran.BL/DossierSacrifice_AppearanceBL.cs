﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<اطلاعات ظاهری>
    /// </summary>
    public class DossierSacrifice_AppearanceBL
    {
        private readonly DossierSacrifice_AppearanceDB _DossierSacrifice_AppearanceDB;

        public DossierSacrifice_AppearanceBL()
        {
            _DossierSacrifice_AppearanceDB = new DossierSacrifice_AppearanceDB();
        }

        public void Add(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam, out Guid DossierSacrifice_AppearanceId)
        {
            _DossierSacrifice_AppearanceDB.AddDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntityParam, out DossierSacrifice_AppearanceId);
        }

        public void Update(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam)
        {
            _DossierSacrifice_AppearanceDB.UpdateDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntityParam);
        }

        public void Delete(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam)
        {
            _DossierSacrifice_AppearanceDB.DeleteDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntityParam);
        }

        public DossierSacrifice_AppearanceEntity GetSingleById(DossierSacrifice_AppearanceEntity DossierSacrifice_AppearanceEntityParam)
        {
            DossierSacrifice_AppearanceEntity o = GetDossierSacrifice_AppearanceFromDossierSacrifice_AppearanceDB(
                _DossierSacrifice_AppearanceDB.GetSingleDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntityParam));

            return o;
        }

        public List<DossierSacrifice_AppearanceEntity> GetAll()
        {
            List<DossierSacrifice_AppearanceEntity> lst = new List<DossierSacrifice_AppearanceEntity>();
            //string key = "DossierSacrifice_Appearance_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_AppearanceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_AppearanceCollectionFromDossierSacrifice_AppearanceDBList(
                _DossierSacrifice_AppearanceDB.GetAllDossierSacrifice_AppearanceDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_AppearanceEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Appearance_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Appearance_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_AppearanceEntity> lst = new List<DossierSacrifice_AppearanceEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_AppearanceEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_AppearanceCollectionFromDossierSacrifice_AppearanceDBList(
                _DossierSacrifice_AppearanceDB.GetPageDossierSacrifice_AppearanceDB(pageSize, currentPage,
                                                                                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static DossierSacrifice_AppearanceEntity GetDossierSacrifice_AppearanceFromDossierSacrifice_AppearanceDB(DossierSacrifice_AppearanceEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_AppearanceEntity ret = new DossierSacrifice_AppearanceEntity(o.DossierSacrifice_AppearanceId, o.DossierSacrificeId, o.BloodGroupId, o.EyeColorId, o.HairColorId, o.SkinColorId, o.Height, o.Weight, o.ShoeSize, o.CostumeSize, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private static List<DossierSacrifice_AppearanceEntity> GetDossierSacrifice_AppearanceCollectionFromDossierSacrifice_AppearanceDBList(List<DossierSacrifice_AppearanceEntity> lst)
        {
            List<DossierSacrifice_AppearanceEntity> RetLst = new List<DossierSacrifice_AppearanceEntity>();
            foreach (DossierSacrifice_AppearanceEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_AppearanceFromDossierSacrifice_AppearanceDB(o));
            }
            return RetLst;

        }

        public static List<DossierSacrifice_AppearanceEntity> GetDossierSacrifice_AppearanceCollectionByBloodGroup(int BloodGroupId)
        {
            return GetDossierSacrifice_AppearanceCollectionFromDossierSacrifice_AppearanceDBList(DossierSacrifice_AppearanceDB.GetDossierSacrifice_AppearanceDBCollectionByBloodGroupDB(BloodGroupId));
        }
    }
}