﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/10>
    /// Description: <پرونده  اطلاعات مهارت علمی و فنی ایثارگری>
    /// </summary>

    public class DossierSacrifice_ScientificAndTechnicalSkillBL
    {
        private readonly DossierSacrifice_ScientificAndTechnicalSkillDB _DossierSacrifice_ScientificAndTechnicalSkillDB;

        public DossierSacrifice_ScientificAndTechnicalSkillBL()
        {
            _DossierSacrifice_ScientificAndTechnicalSkillDB = new DossierSacrifice_ScientificAndTechnicalSkillDB();
        }

        public void Add(
            DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam,
            out Guid DossierSacrifice_ScientificAndTechnicalSkillId)
        {
            _DossierSacrifice_ScientificAndTechnicalSkillDB.AddDossierSacrifice_ScientificAndTechnicalSkillDB(
                DossierSacrifice_ScientificAndTechnicalSkillEntityParam,
                out DossierSacrifice_ScientificAndTechnicalSkillId);
        }

        public void Update(
            DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
        {
            _DossierSacrifice_ScientificAndTechnicalSkillDB.UpdateDossierSacrifice_ScientificAndTechnicalSkillDB(
                DossierSacrifice_ScientificAndTechnicalSkillEntityParam);
        }

        public void Delete(
            DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
        {
            _DossierSacrifice_ScientificAndTechnicalSkillDB.DeleteDossierSacrifice_ScientificAndTechnicalSkillDB(
                DossierSacrifice_ScientificAndTechnicalSkillEntityParam);
        }

        public DossierSacrifice_ScientificAndTechnicalSkillEntity GetSingleById(
            DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
        {
            DossierSacrifice_ScientificAndTechnicalSkillEntity o = GetDossierSacrifice_ScientificAndTechnicalSkillFromDossierSacrifice_ScientificAndTechnicalSkillDB
                (
                    _DossierSacrifice_ScientificAndTechnicalSkillDB.
                        GetSingleDossierSacrifice_ScientificAndTechnicalSkillDB(
                            DossierSacrifice_ScientificAndTechnicalSkillEntityParam));

            return o;
        }

        public List<DossierSacrifice_ScientificAndTechnicalSkillEntity> GetAll()
        {
            List<DossierSacrifice_ScientificAndTechnicalSkillEntity> lst =
                new List<DossierSacrifice_ScientificAndTechnicalSkillEntity>();
            //string key = "DossierSacrifice_ScientificAndTechnicalSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ScientificAndTechnicalSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ScientificAndTechnicalSkillCollectionFromDossierSacrifice_ScientificAndTechnicalSkillDBList
                (
                    _DossierSacrifice_ScientificAndTechnicalSkillDB.GetAllDossierSacrifice_ScientificAndTechnicalSkillDB
                        ());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_ScientificAndTechnicalSkillEntity> GetAllPaging(int currentPage, int pageSize,
                                                                                     string

                                                                                         sortExpression, out int count,
                                                                                     string whereClause)
        {
            //string key = "DossierSacrifice_ScientificAndTechnicalSkill_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_ScientificAndTechnicalSkill_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_ScientificAndTechnicalSkillEntity> lst =
                new List<DossierSacrifice_ScientificAndTechnicalSkillEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ScientificAndTechnicalSkillEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ScientificAndTechnicalSkillCollectionFromDossierSacrifice_ScientificAndTechnicalSkillDBList
                (
                    _DossierSacrifice_ScientificAndTechnicalSkillDB.
                        GetPageDossierSacrifice_ScientificAndTechnicalSkillDB(pageSize, currentPage,
                                                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_ScientificAndTechnicalSkillEntity
            GetDossierSacrifice_ScientificAndTechnicalSkillFromDossierSacrifice_ScientificAndTechnicalSkillDB(
            DossierSacrifice_ScientificAndTechnicalSkillEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_ScientificAndTechnicalSkillEntity ret =
                new DossierSacrifice_ScientificAndTechnicalSkillEntity(
                    o.DossierSacrifice_ScientificAndTechnicalSkillId, o.DossierSacrificeId,
                    o.ScientificAndTechnicalSkillTitle, o.ExperienceYearNo, o.HasEvidence, o.PlaceOfGraduation,
                    o.Description, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_ScientificAndTechnicalSkillEntity>
            GetDossierSacrifice_ScientificAndTechnicalSkillCollectionFromDossierSacrifice_ScientificAndTechnicalSkillDBList
            (List<DossierSacrifice_ScientificAndTechnicalSkillEntity> lst)
        {
            List<DossierSacrifice_ScientificAndTechnicalSkillEntity> RetLst =
                new List<DossierSacrifice_ScientificAndTechnicalSkillEntity>();
            foreach (DossierSacrifice_ScientificAndTechnicalSkillEntity o in lst)
            {
                RetLst.Add(
                    GetDossierSacrifice_ScientificAndTechnicalSkillFromDossierSacrifice_ScientificAndTechnicalSkillDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_ScientificAndTechnicalSkillEntity>
            GetDossierSacrifice_ScientificAndTechnicalSkillCollectionByDossierSacrifice(DossierSacrifice_ScientificAndTechnicalSkillEntity DossierSacrifice_ScientificAndTechnicalSkillEntityParam)
        {
            return
                GetDossierSacrifice_ScientificAndTechnicalSkillCollectionFromDossierSacrifice_ScientificAndTechnicalSkillDBList
                    (_DossierSacrifice_ScientificAndTechnicalSkillDB.
                         GetDossierSacrifice_ScientificAndTechnicalSkillDBCollectionByDossierSacrificeDB(
                             DossierSacrifice_ScientificAndTechnicalSkillEntityParam));
        }



    }

}
