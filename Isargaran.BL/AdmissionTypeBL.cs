﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <نوع پذیرش>
    /// </summary>

	public class AdmissionTypeBL 
	{	
	  	 private readonly AdmissionTypeDB _AdmissionTypeDB;					
			
		public AdmissionTypeBL()
		{
			_AdmissionTypeDB = new AdmissionTypeDB();
		}			
	
		public  void Add(AdmissionTypeEntity  AdmissionTypeEntityParam, out Guid AdmissionTypeId)
		{ 
			_AdmissionTypeDB.AddAdmissionTypeDB(AdmissionTypeEntityParam,out AdmissionTypeId);			
		}

		public  void Update(AdmissionTypeEntity  AdmissionTypeEntityParam)
		{
			_AdmissionTypeDB.UpdateAdmissionTypeDB(AdmissionTypeEntityParam);		
		}

		public  void Delete(AdmissionTypeEntity  AdmissionTypeEntityParam)
		{
			 _AdmissionTypeDB.DeleteAdmissionTypeDB(AdmissionTypeEntityParam);			
		}

		public  AdmissionTypeEntity GetSingleById(AdmissionTypeEntity  AdmissionTypeEntityParam)
		{
			AdmissionTypeEntity o = GetAdmissionTypeFromAdmissionTypeDB(
			_AdmissionTypeDB.GetSingleAdmissionTypeDB(AdmissionTypeEntityParam));
			
			return o;
		}

		public  List<AdmissionTypeEntity> GetAll()
		{
			List<AdmissionTypeEntity> lst = new List<AdmissionTypeEntity>();
			//string key = "AdmissionType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<AdmissionTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetAdmissionTypeCollectionFromAdmissionTypeDBList(
				_AdmissionTypeDB.GetAllAdmissionTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<AdmissionTypeEntity> GetAllIsActive()
        {
            List<AdmissionTypeEntity> lst = new List<AdmissionTypeEntity>();
            //string key = "AdmissionType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AdmissionTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAdmissionTypeCollectionFromAdmissionTypeDBList(
            _AdmissionTypeDB.GetAllAdmissionTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<AdmissionTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "AdmissionType_List_Page_" + currentPage ;
			//string countKey = "AdmissionType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<AdmissionTypeEntity> lst = new List<AdmissionTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<AdmissionTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetAdmissionTypeCollectionFromAdmissionTypeDBList(
				_AdmissionTypeDB.GetPageAdmissionTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  AdmissionTypeEntity GetAdmissionTypeFromAdmissionTypeDB(AdmissionTypeEntity o)
		{
	if(o == null)
                return null;
			AdmissionTypeEntity ret = new AdmissionTypeEntity(o.AdmissionTypeId ,o.AdmissionTypeTitle ,o.HasTuition ,o.IsDaily ,o.IsE_Lerning ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<AdmissionTypeEntity> GetAdmissionTypeCollectionFromAdmissionTypeDBList( List<AdmissionTypeEntity> lst)
		{
			List<AdmissionTypeEntity> RetLst = new List<AdmissionTypeEntity>();
			foreach(AdmissionTypeEntity o in lst)
			{
				RetLst.Add(GetAdmissionTypeFromAdmissionTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}


