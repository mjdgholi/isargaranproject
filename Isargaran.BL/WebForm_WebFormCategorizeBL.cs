﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <narges.kamran>
    /// Create date: <1393/08/20>
    /// Description: <ارتباطی وب با دسته بندی فرم>
    /// </summary>

    public class WebForm_WebFormCategorizeBL
    {
        private readonly WebForm_WebFormCategorizeDB _WebForm_WebFormCategorizeDB;

        public WebForm_WebFormCategorizeBL()
        {
            _WebForm_WebFormCategorizeDB = new WebForm_WebFormCategorizeDB();
        }

        public void Add(WebForm_WebFormCategorizeEntity WebForm_WebFormCategorizeEntityParam,
            out Guid WebForm_WebFormCategorizeId)
        {
            _WebForm_WebFormCategorizeDB.AddWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntityParam,
                out WebForm_WebFormCategorizeId);
        }

        public void Update(WebForm_WebFormCategorizeEntity WebForm_WebFormCategorizeEntityParam)
        {
            _WebForm_WebFormCategorizeDB.UpdateWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntityParam);
        }

        public void Delete(WebForm_WebFormCategorizeEntity WebForm_WebFormCategorizeEntityParam)
        {
            _WebForm_WebFormCategorizeDB.DeleteWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntityParam);
        }

        public WebForm_WebFormCategorizeEntity GetSingleById(
            WebForm_WebFormCategorizeEntity WebForm_WebFormCategorizeEntityParam)
        {
            WebForm_WebFormCategorizeEntity o = GetWebForm_WebFormCategorizeFromWebForm_WebFormCategorizeDB(
                _WebForm_WebFormCategorizeDB.GetSingleWebForm_WebFormCategorizeDB(WebForm_WebFormCategorizeEntityParam));

            return o;
        }

        public List<WebForm_WebFormCategorizeEntity> GetAll()
        {
            List<WebForm_WebFormCategorizeEntity> lst = new List<WebForm_WebFormCategorizeEntity>();
            //string key = "WebForm_WebFormCategorize_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WebForm_WebFormCategorizeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWebForm_WebFormCategorizeCollectionFromWebForm_WebFormCategorizeDBList(
                _WebForm_WebFormCategorizeDB.GetAllWebForm_WebFormCategorizeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<WebForm_WebFormCategorizeEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "WebForm_WebFormCategorize_List_Page_" + currentPage ;
            //string countKey = "WebForm_WebFormCategorize_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<WebForm_WebFormCategorizeEntity> lst = new List<WebForm_WebFormCategorizeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WebForm_WebFormCategorizeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetWebForm_WebFormCategorizeCollectionFromWebForm_WebFormCategorizeDBList(
                _WebForm_WebFormCategorizeDB.GetPageWebForm_WebFormCategorizeDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private WebForm_WebFormCategorizeEntity GetWebForm_WebFormCategorizeFromWebForm_WebFormCategorizeDB(
            WebForm_WebFormCategorizeEntity o)
        {
            if (o == null)
                return null;
            WebForm_WebFormCategorizeEntity ret = new WebForm_WebFormCategorizeEntity(o.WebForm_WebFormCategorizeId,
                o.WebFormCategorizeId, o.WebFormId, o.FormPriority, o.ImagePath, o.CreationDate, o.ModificationDate,
                o.IsActive);
            return ret;
        }

        private List<WebForm_WebFormCategorizeEntity>
            GetWebForm_WebFormCategorizeCollectionFromWebForm_WebFormCategorizeDBList(
            List<WebForm_WebFormCategorizeEntity> lst)
        {
            List<WebForm_WebFormCategorizeEntity> RetLst = new List<WebForm_WebFormCategorizeEntity>();
            foreach (WebForm_WebFormCategorizeEntity o in lst)
            {
                RetLst.Add(GetWebForm_WebFormCategorizeFromWebForm_WebFormCategorizeDB(o));
            }
            return RetLst;

        }


        public List<WebForm_WebFormCategorizeEntity> GetWebForm_WebFormCategorizeCollectionByWebForm_WebFormCategorize(
            WebForm_WebFormCategorizeEntity webFormWebFormCategorizeEntityParam)
        {
            return
                GetWebForm_WebFormCategorizeCollectionFromWebForm_WebFormCategorizeDBList(
                    _WebForm_WebFormCategorizeDB.GetWebForm_WebFormCategorizeDBCollectionByWebForm_WebFormCategorizeDB(
                        webFormWebFormCategorizeEntityParam));
        }

        public List<WebForm_WebFormCategorizeEntity> GetWebForm_WebFormCategorizeCollectionByWebForm(
            WebForm_WebFormCategorizeEntity webFormWebFormCategorizeEntityParam)
        {
            return
                GetWebForm_WebFormCategorizeCollectionFromWebForm_WebFormCategorizeDBList(
                    _WebForm_WebFormCategorizeDB.GetWebForm_WebFormCategorizeDBCollectionByWebFormDB(
                        webFormWebFormCategorizeEntityParam));
        }

        public List<WebFormEntity> GetWebForm_WebFormCategorizeCollectionByWebForm_WebFormCategorizeWithoutDuplicate(Guid WebFormCategorizeId,Guid WebFormId)
        {
            var webFormDB = new WebFormBL();
            return webFormDB.GetWebFormCollectionFromWebFormDBList(_WebForm_WebFormCategorizeDB.GetWebForm_WebFormCategorizeCollectionByWebForm_WebFormCategorizeWithoutDuplicateDB(WebFormCategorizeId,WebFormId));

        }
    }
}



