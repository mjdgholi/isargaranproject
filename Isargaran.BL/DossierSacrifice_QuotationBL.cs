﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/02>
    /// Description: <نقل قول>
    /// </summary>

    public class DossierSacrifice_QuotationBL
    {
        private readonly DossierSacrifice_QuotationDB _DossierSacrifice_QuotationDB;

        public DossierSacrifice_QuotationBL()
        {
            _DossierSacrifice_QuotationDB = new DossierSacrifice_QuotationDB();
        }

        public void Add(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam,
                        out Guid DossierSacrifice_QuotationId)
        {
            _DossierSacrifice_QuotationDB.AddDossierSacrifice_QuotationDB(DossierSacrifice_QuotationEntityParam,
                                                                          out DossierSacrifice_QuotationId);
        }

        public void Update(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            _DossierSacrifice_QuotationDB.UpdateDossierSacrifice_QuotationDB(DossierSacrifice_QuotationEntityParam);
        }

        public void Delete(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            _DossierSacrifice_QuotationDB.DeleteDossierSacrifice_QuotationDB(DossierSacrifice_QuotationEntityParam);
        }

        public DossierSacrifice_QuotationEntity GetSingleById(
            DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            DossierSacrifice_QuotationEntity o = GetDossierSacrifice_QuotationFromDossierSacrifice_QuotationDB(
                _DossierSacrifice_QuotationDB.GetSingleDossierSacrifice_QuotationDB(
                    DossierSacrifice_QuotationEntityParam));

            return o;
        }

        public List<DossierSacrifice_QuotationEntity> GetAll()
        {
            List<DossierSacrifice_QuotationEntity> lst = new List<DossierSacrifice_QuotationEntity>();
            //string key = "DossierSacrifice_Quotation_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_QuotationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_QuotationCollectionFromDossierSacrifice_QuotationDBList(
                _DossierSacrifice_QuotationDB.GetAllDossierSacrifice_QuotationDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_QuotationEntity> GetAllPaging(int currentPage, int pageSize,
                                                                   string

                                                                       sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Quotation_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Quotation_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_QuotationEntity> lst = new List<DossierSacrifice_QuotationEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_QuotationEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_QuotationCollectionFromDossierSacrifice_QuotationDBList(
                _DossierSacrifice_QuotationDB.GetPageDossierSacrifice_QuotationDB(pageSize, currentPage,
                                                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_QuotationEntity GetDossierSacrifice_QuotationFromDossierSacrifice_QuotationDB(
            DossierSacrifice_QuotationEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_QuotationEntity ret = new DossierSacrifice_QuotationEntity(o.DossierSacrifice_QuotationId,
                                                                                        o.DossierSacrificeId,
                                                                                        o.QuotationDate,
                                                                                        o.QuotationContent,
                                                                                        o.CreationDate,
                                                                                        o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_QuotationEntity>
            GetDossierSacrifice_QuotationCollectionFromDossierSacrifice_QuotationDBList(
            List<DossierSacrifice_QuotationEntity> lst)
        {
            List<DossierSacrifice_QuotationEntity> RetLst = new List<DossierSacrifice_QuotationEntity>();
            foreach (DossierSacrifice_QuotationEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_QuotationFromDossierSacrifice_QuotationDB(o));
            }
            return RetLst;

        }




        public List<DossierSacrifice_QuotationEntity> GetDossierSacrifice_QuotationCollectionByDossierSacrifice(DossierSacrifice_QuotationEntity DossierSacrifice_QuotationEntityParam)
        {
            return
                GetDossierSacrifice_QuotationCollectionFromDossierSacrifice_QuotationDBList(
                    _DossierSacrifice_QuotationDB.GetDossierSacrifice_QuotationDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_QuotationEntityParam));
        }

    }
}
