﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Update Author: <Narges.Kamran> <1393/08/20>
    /// Create date: <1393/03/11>
    /// 
    /// Description: <وب فرم ها>
    /// </summary>
    public class WebFormBL
    {
        private readonly WebFormDB _WebFormDB;

        public WebFormBL()
        {
            _WebFormDB = new WebFormDB();
        }

        public void Add(WebFormEntity WebFormEntityParam, out Guid WebFormId)
        {
            _WebFormDB.AddWebFormDB(WebFormEntityParam, out WebFormId);
        }

        public void Update(WebFormEntity WebFormEntityParam)
        {
            _WebFormDB.UpdateWebFormDB(WebFormEntityParam);
        }

        public void Delete(WebFormEntity WebFormEntityParam)
        {
            _WebFormDB.DeleteWebFormDB(WebFormEntityParam);
        }

        public WebFormEntity GetSingleById(WebFormEntity WebFormEntityParam)
        {
            WebFormEntity o = GetWebFormFromWebFormDB(
                _WebFormDB.GetSingleWebFormDB(WebFormEntityParam));

            return o;
        }

        public List<WebFormEntity> GetAll()
        {
            List<WebFormEntity> lst = new List<WebFormEntity>();
            //string key = "WebForm_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WebFormEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWebFormCollectionFromWebFormDBList(
                _WebFormDB.GetAllWebFormDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<WebFormEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "WebForm_List_Page_" + currentPage ;
            //string countKey = "WebForm_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<WebFormEntity> lst = new List<WebFormEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WebFormEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetWebFormCollectionFromWebFormDBList(
                _WebFormDB.GetPageWebFormDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private WebFormEntity GetWebFormFromWebFormDB(WebFormEntity o)
        {
            if (o == null)
                return null;
            WebFormEntity ret = new WebFormEntity(o.WebFormId, o.WebFormTitle, o.WebFormURL, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        public List<WebFormEntity> GetWebFormCollectionFromWebFormDBList(List<WebFormEntity> lst)
        {
            List<WebFormEntity> RetLst = new List<WebFormEntity>();
            foreach (WebFormEntity o in lst)
            {
                RetLst.Add(GetWebFormFromWebFormDB(o));
            }
            return RetLst;

        }


        public  List<WebFormEntity> GetWebFormCollectionByWebFormCategorize(Guid WebFormCategorizeId)
        {
            return
                GetWebFormCollectionFromWebFormDBList(
                    _WebFormDB.GetWebFormDBCollectionByWebFormCategorizeDB(WebFormCategorizeId));
        }

        public List<WebFormEntity> GetWebFormCollectionByWebFormCategorizePerRowAccess(Guid formCategoryId, string userId, string objectTitleStr)
        {
            return
              GetWebFormCollectionFromWebFormDBList(
                  _WebFormDB.GetWebFormDBCollectionByWebFormCategorizePerRowAccessDB(formCategoryId, userId, objectTitleStr));
        }
    }
}