﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/13>
    /// Description: <رشته هنری>
    /// </summary>


	public class ArtCourseBL 
	{	
	  	 private readonly ArtCourseDB _ArtCourseDB;					
			
		public ArtCourseBL()
		{
			_ArtCourseDB = new ArtCourseDB();
		}			
	
		public  void Add(ArtCourseEntity  ArtCourseEntityParam, out Guid ArtCourseId)
		{ 
			_ArtCourseDB.AddArtCourseDB(ArtCourseEntityParam,out ArtCourseId);			
		}

		public  void Update(ArtCourseEntity  ArtCourseEntityParam)
		{
			_ArtCourseDB.UpdateArtCourseDB(ArtCourseEntityParam);		
		}

		public  void Delete(ArtCourseEntity  ArtCourseEntityParam)
		{
			 _ArtCourseDB.DeleteArtCourseDB(ArtCourseEntityParam);			
		}

		public  ArtCourseEntity GetSingleById(ArtCourseEntity  ArtCourseEntityParam)
		{
			ArtCourseEntity o = GetArtCourseFromArtCourseDB(
			_ArtCourseDB.GetSingleArtCourseDB(ArtCourseEntityParam));
			
			return o;
		}

		public  List<ArtCourseEntity> GetAll()
		{
			List<ArtCourseEntity> lst = new List<ArtCourseEntity>();
			//string key = "ArtCourse_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ArtCourseEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetArtCourseCollectionFromArtCourseDBList(
				_ArtCourseDB.GetAllArtCourseDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<ArtCourseEntity> GetAllIsActive()
        {
            List<ArtCourseEntity> lst = new List<ArtCourseEntity>();
            //string key = "ArtCourse_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ArtCourseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetArtCourseCollectionFromArtCourseDBList(
            _ArtCourseDB.GetAllArtCourseIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<ArtCourseEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ArtCourse_List_Page_" + currentPage ;
			//string countKey = "ArtCourse_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ArtCourseEntity> lst = new List<ArtCourseEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ArtCourseEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetArtCourseCollectionFromArtCourseDBList(
				_ArtCourseDB.GetPageArtCourseDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ArtCourseEntity GetArtCourseFromArtCourseDB(ArtCourseEntity o)
		{
	if(o == null)
                return null;
			ArtCourseEntity ret = new ArtCourseEntity(o.ArtCourseId ,o.ArtCourseTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ArtCourseEntity> GetArtCourseCollectionFromArtCourseDBList( List<ArtCourseEntity> lst)
		{
			List<ArtCourseEntity> RetLst = new List<ArtCourseEntity>();
			foreach(ArtCourseEntity o in lst)
			{
				RetLst.Add(GetArtCourseFromArtCourseDB(o));
			}
			return RetLst;
			
		} 						
	}

}


