﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <نوع ایثارگری>
    /// </summary>


    public class SacrificeTypeBL
    {
        private readonly SacrificeTypeDB _SacrificeTypeDB;

        public SacrificeTypeBL()
        {
            _SacrificeTypeDB = new SacrificeTypeDB();
        }

        public void Add(SacrificeTypeEntity SacrificeTypeEntityParam, out Guid SacrificeTypeId)
        {
            _SacrificeTypeDB.AddSacrificeTypeDB(SacrificeTypeEntityParam, out SacrificeTypeId);
        }

        public void Update(SacrificeTypeEntity SacrificeTypeEntityParam)
        {
            _SacrificeTypeDB.UpdateSacrificeTypeDB(SacrificeTypeEntityParam);
        }

        public void Delete(SacrificeTypeEntity SacrificeTypeEntityParam)
        {
            _SacrificeTypeDB.DeleteSacrificeTypeDB(SacrificeTypeEntityParam);
        }

        public SacrificeTypeEntity GetSingleById(SacrificeTypeEntity SacrificeTypeEntityParam)
        {
            SacrificeTypeEntity o = GetSacrificeTypeFromSacrificeTypeDB(
                _SacrificeTypeDB.GetSingleSacrificeTypeDB(SacrificeTypeEntityParam));

            return o;
        }

        public List<SacrificeTypeEntity> GetAll()
        {
            List<SacrificeTypeEntity> lst = new List<SacrificeTypeEntity>();
            //string key = "SacrificeType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SacrificeTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSacrificeTypeCollectionFromSacrificeTypeDBList(
                _SacrificeTypeDB.GetAllSacrificeTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }


        public List<SacrificeTypeEntity> GetAllIsActive()
        {
            List<SacrificeTypeEntity> lst = new List<SacrificeTypeEntity>();
            //string key = "SacrificeType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SacrificeTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSacrificeTypeCollectionFromSacrificeTypeDBList(
                _SacrificeTypeDB.GetAllSacrificeTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<SacrificeTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                      string

                                                          sortExpression, out int count, string whereClause)
        {
            //string key = "SacrificeType_List_Page_" + currentPage ;
            //string countKey = "SacrificeType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<SacrificeTypeEntity> lst = new List<SacrificeTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SacrificeTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetSacrificeTypeCollectionFromSacrificeTypeDBList(
                _SacrificeTypeDB.GetPageSacrificeTypeDB(pageSize, currentPage,
                                                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private SacrificeTypeEntity GetSacrificeTypeFromSacrificeTypeDB(SacrificeTypeEntity o)
        {
            if (o == null)
                return null;
            SacrificeTypeEntity ret = new SacrificeTypeEntity(o.SacrificeTypeId, o.SacrificeTypeTitle, o.CreationDate,
                                                              o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<SacrificeTypeEntity> GetSacrificeTypeCollectionFromSacrificeTypeDBList(
            List<SacrificeTypeEntity> lst)
        {
            List<SacrificeTypeEntity> RetLst = new List<SacrificeTypeEntity>();
            foreach (SacrificeTypeEntity o in lst)
            {
                RetLst.Add(GetSacrificeTypeFromSacrificeTypeDB(o));
            }
            return RetLst;

        }


    }




}


