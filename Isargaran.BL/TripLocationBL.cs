﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/18>
    // Description:	<محل مسافرت>
    /// </summary>

	public class TripLocationBL 
	{	
	  	 private readonly TripLocationDB _TripLocationDB;					
			
		public TripLocationBL()
		{
			_TripLocationDB = new TripLocationDB();
		}			
	
		public  void Add(TripLocationEntity  TripLocationEntityParam, out Guid TripLocationId)
		{ 
			_TripLocationDB.AddTripLocationDB(TripLocationEntityParam,out TripLocationId);			
		}

		public  void Update(TripLocationEntity  TripLocationEntityParam)
		{
			_TripLocationDB.UpdateTripLocationDB(TripLocationEntityParam);		
		}

		public  void Delete(TripLocationEntity  TripLocationEntityParam)
		{
			 _TripLocationDB.DeleteTripLocationDB(TripLocationEntityParam);			
		}

		public  TripLocationEntity GetSingleById(TripLocationEntity  TripLocationEntityParam)
		{
			TripLocationEntity o = GetTripLocationFromTripLocationDB(
			_TripLocationDB.GetSingleTripLocationDB(TripLocationEntityParam));
			
			return o;
		}

		public  List<TripLocationEntity> GetAll()
		{
			List<TripLocationEntity> lst = new List<TripLocationEntity>();
			//string key = "TripLocation_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<TripLocationEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetTripLocationCollectionFromTripLocationDBList(
				_TripLocationDB.GetAllTripLocationDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<TripLocationEntity> GetAllIsActive()
        {
            List<TripLocationEntity> lst = new List<TripLocationEntity>();
            //string key = "TripLocation_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<TripLocationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetTripLocationCollectionFromTripLocationDBList(
            _TripLocationDB.GetAllTripLocationIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<TripLocationEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "TripLocation_List_Page_" + currentPage ;
			//string countKey = "TripLocation_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<TripLocationEntity> lst = new List<TripLocationEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<TripLocationEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetTripLocationCollectionFromTripLocationDBList(
				_TripLocationDB.GetPageTripLocationDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  TripLocationEntity GetTripLocationFromTripLocationDB(TripLocationEntity o)
		{
	if(o == null)
                return null;
			TripLocationEntity ret = new TripLocationEntity(o.TripLocationId ,o.TripLocationTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<TripLocationEntity> GetTripLocationCollectionFromTripLocationDBList( List<TripLocationEntity> lst)
		{
			List<TripLocationEntity> RetLst = new List<TripLocationEntity>();
			foreach(TripLocationEntity o in lst)
			{
				RetLst.Add(GetTripLocationFromTripLocationDB(o));
			}
			return RetLst;
			
		} 
				
		
	}




}


