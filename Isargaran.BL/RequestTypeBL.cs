﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع درخواست>
    /// </summary>

	public class RequestTypeBL 
	{	
	  	 private readonly RequestTypeDB _RequestTypeDB;					
			
		public RequestTypeBL()
		{
			_RequestTypeDB = new RequestTypeDB();
		}			
	
		public  void Add(RequestTypeEntity  RequestTypeEntityParam, out Guid RequestTypeId)
		{ 
			_RequestTypeDB.AddRequestTypeDB(RequestTypeEntityParam,out RequestTypeId);			
		}

		public  void Update(RequestTypeEntity  RequestTypeEntityParam)
		{
			_RequestTypeDB.UpdateRequestTypeDB(RequestTypeEntityParam);		
		}

		public  void Delete(RequestTypeEntity  RequestTypeEntityParam)
		{
			 _RequestTypeDB.DeleteRequestTypeDB(RequestTypeEntityParam);			
		}

		public  RequestTypeEntity GetSingleById(RequestTypeEntity  RequestTypeEntityParam)
		{
			RequestTypeEntity o = GetRequestTypeFromRequestTypeDB(
			_RequestTypeDB.GetSingleRequestTypeDB(RequestTypeEntityParam));
			
			return o;
		}

		public  List<RequestTypeEntity> GetAll()
		{
			List<RequestTypeEntity> lst = new List<RequestTypeEntity>();
			//string key = "RequestType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<RequestTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetRequestTypeCollectionFromRequestTypeDBList(
				_RequestTypeDB.GetAllRequestTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<RequestTypeEntity> GetAllIsActive()
        {
            List<RequestTypeEntity> lst = new List<RequestTypeEntity>();
            //string key = "RequestType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<RequestTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetRequestTypeCollectionFromRequestTypeDBList(
            _RequestTypeDB.GetAllRequestTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<RequestTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "RequestType_List_Page_" + currentPage ;
			//string countKey = "RequestType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<RequestTypeEntity> lst = new List<RequestTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<RequestTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetRequestTypeCollectionFromRequestTypeDBList(
				_RequestTypeDB.GetPageRequestTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  RequestTypeEntity GetRequestTypeFromRequestTypeDB(RequestTypeEntity o)
		{
	if(o == null)
                return null;
			RequestTypeEntity ret = new RequestTypeEntity(o.RequestTypeId ,o.RequestTypeTitle ,o.Priority ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<RequestTypeEntity> GetRequestTypeCollectionFromRequestTypeDBList( List<RequestTypeEntity> lst)
		{
			List<RequestTypeEntity> RetLst = new List<RequestTypeEntity>();
			foreach(RequestTypeEntity o in lst)
			{
				RetLst.Add(GetRequestTypeFromRequestTypeDB(o));
			}
			return RetLst;
			
		}


        
	}


}

