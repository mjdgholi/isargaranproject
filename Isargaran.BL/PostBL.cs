﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <پست>
    /// </summary>

	public class PostBL 
	{	
	  	 private readonly PostDB _PostDB;					
			
		public PostBL()
		{
			_PostDB = new PostDB();
		}			
	
		public  void Add(PostEntity  PostEntityParam, out Guid PostId)
		{ 
			_PostDB.AddPostDB(PostEntityParam,out PostId);			
		}

		public  void Update(PostEntity  PostEntityParam)
		{
			_PostDB.UpdatePostDB(PostEntityParam);		
		}

		public  void Delete(PostEntity  PostEntityParam)
		{
			 _PostDB.DeletePostDB(PostEntityParam);			
		}

		public  PostEntity GetSingleById(PostEntity  PostEntityParam)
		{
			PostEntity o = GetPostFromPostDB(
			_PostDB.GetSinglePostDB(PostEntityParam));
			
			return o;
		}

		public  List<PostEntity> GetAll()
		{
			List<PostEntity> lst = new List<PostEntity>();
			//string key = "Post_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<PostEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetPostCollectionFromPostDBList(
				_PostDB.GetAllPostDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<PostEntity> GetAllIsActive()
        {
            List<PostEntity> lst = new List<PostEntity>();
            //string key = "Post_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PostEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetPostCollectionFromPostDBList(
            _PostDB.GetAllPostIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<PostEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Post_List_Page_" + currentPage ;
			//string countKey = "Post_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<PostEntity> lst = new List<PostEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<PostEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetPostCollectionFromPostDBList(
				_PostDB.GetPagePostDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  PostEntity GetPostFromPostDB(PostEntity o)
		{
	if(o == null)
                return null;
			PostEntity ret = new PostEntity(o.PostId ,o.PostTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<PostEntity> GetPostCollectionFromPostDBList( List<PostEntity> lst)
		{
			List<PostEntity> RetLst = new List<PostEntity>();
			foreach(PostEntity o in lst)
			{
				RetLst.Add(GetPostFromPostDB(o));
			}
			return RetLst;
			
		} 
	}

}


