﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/14>
    /// Description: <پرونده   اطلاعات پژوهشی ایثارگری>
    /// </summary>

	public class DossierSacrifice_ResearchBL 
	{	
	  	 private readonly DossierSacrifice_ResearchDB _DossierSacrifice_ResearchDB;					
			
		public DossierSacrifice_ResearchBL()
		{
			_DossierSacrifice_ResearchDB = new DossierSacrifice_ResearchDB();
		}			
	
		public  void Add(DossierSacrifice_ResearchEntity  DossierSacrifice_ResearchEntityParam, out Guid DossierSacrifice_ResearchId)
		{ 
			_DossierSacrifice_ResearchDB.AddDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntityParam,out DossierSacrifice_ResearchId);			
		}

		public  void Update(DossierSacrifice_ResearchEntity  DossierSacrifice_ResearchEntityParam)
		{
			_DossierSacrifice_ResearchDB.UpdateDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntityParam);		
		}

		public  void Delete(DossierSacrifice_ResearchEntity  DossierSacrifice_ResearchEntityParam)
		{
			 _DossierSacrifice_ResearchDB.DeleteDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntityParam);			
		}

		public  DossierSacrifice_ResearchEntity GetSingleById(DossierSacrifice_ResearchEntity  DossierSacrifice_ResearchEntityParam)
		{
			DossierSacrifice_ResearchEntity o = GetDossierSacrifice_ResearchFromDossierSacrifice_ResearchDB(
			_DossierSacrifice_ResearchDB.GetSingleDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_ResearchEntity> GetAll()
		{
			List<DossierSacrifice_ResearchEntity> lst = new List<DossierSacrifice_ResearchEntity>();
			//string key = "DossierSacrifice_Research_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_ResearchEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_ResearchCollectionFromDossierSacrifice_ResearchDBList(
				_DossierSacrifice_ResearchDB.GetAllDossierSacrifice_ResearchDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_ResearchEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_Research_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_Research_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_ResearchEntity> lst = new List<DossierSacrifice_ResearchEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_ResearchEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_ResearchCollectionFromDossierSacrifice_ResearchDBList(
				_DossierSacrifice_ResearchDB.GetPageDossierSacrifice_ResearchDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_ResearchEntity GetDossierSacrifice_ResearchFromDossierSacrifice_ResearchDB(DossierSacrifice_ResearchEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_ResearchEntity ret = new DossierSacrifice_ResearchEntity(o.DossierSacrifice_ResearchId ,o.DossierSacrificeId ,o.ResearchTitle ,o.ResearchSubject ,o.ApplicationsOfResearch ,o.StartDate ,o.Description ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<DossierSacrifice_ResearchEntity> GetDossierSacrifice_ResearchCollectionFromDossierSacrifice_ResearchDBList( List<DossierSacrifice_ResearchEntity> lst)
		{
			List<DossierSacrifice_ResearchEntity> RetLst = new List<DossierSacrifice_ResearchEntity>();
			foreach(DossierSacrifice_ResearchEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_ResearchFromDossierSacrifice_ResearchDB(o));
			}
			return RetLst;
			
		}



        public List<DossierSacrifice_ResearchEntity> GetDossierSacrifice_ResearchCollectionByDossierSacrifice(DossierSacrifice_ResearchEntity DossierSacrifice_ResearchEntityParam)
{
    return GetDossierSacrifice_ResearchCollectionFromDossierSacrifice_ResearchDBList(_DossierSacrifice_ResearchDB.GetDossierSacrifice_ResearchDBCollectionByDossierSacrificeDB(DossierSacrifice_ResearchEntityParam));
}





}

}
