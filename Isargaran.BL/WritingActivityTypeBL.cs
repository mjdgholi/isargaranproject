﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/07>
    // Description:	<نوع فعالیت نویسندگی>
    /// </summary>

	public class WritingActivityTypeBL 
	{	
	  	 private readonly WritingActivityTypeDB _WritingActivityTypeDB;					
			
		public WritingActivityTypeBL()
		{
			_WritingActivityTypeDB = new WritingActivityTypeDB();
		}			
	
		public  void Add(WritingActivityTypeEntity  WritingActivityTypeEntityParam, out Guid WritingActivityTypeId)
		{ 
			_WritingActivityTypeDB.AddWritingActivityTypeDB(WritingActivityTypeEntityParam,out WritingActivityTypeId);			
		}

		public  void Update(WritingActivityTypeEntity  WritingActivityTypeEntityParam)
		{
			_WritingActivityTypeDB.UpdateWritingActivityTypeDB(WritingActivityTypeEntityParam);		
		}

		public  void Delete(WritingActivityTypeEntity  WritingActivityTypeEntityParam)
		{
			 _WritingActivityTypeDB.DeleteWritingActivityTypeDB(WritingActivityTypeEntityParam);			
		}

		public  WritingActivityTypeEntity GetSingleById(WritingActivityTypeEntity  WritingActivityTypeEntityParam)
		{
			WritingActivityTypeEntity o = GetWritingActivityTypeFromWritingActivityTypeDB(
			_WritingActivityTypeDB.GetSingleWritingActivityTypeDB(WritingActivityTypeEntityParam));
			
			return o;
		}

		public  List<WritingActivityTypeEntity> GetAll()
		{
			List<WritingActivityTypeEntity> lst = new List<WritingActivityTypeEntity>();
			//string key = "WritingActivityType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<WritingActivityTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetWritingActivityTypeCollectionFromWritingActivityTypeDBList(
				_WritingActivityTypeDB.GetAllWritingActivityTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<WritingActivityTypeEntity> GetAllIsActive()
        {
            List<WritingActivityTypeEntity> lst = new List<WritingActivityTypeEntity>();
            //string key = "WritingActivityType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WritingActivityTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWritingActivityTypeCollectionFromWritingActivityTypeDBList(
            _WritingActivityTypeDB.GetAllWritingActivityTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<WritingActivityTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "WritingActivityType_List_Page_" + currentPage ;
			//string countKey = "WritingActivityType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<WritingActivityTypeEntity> lst = new List<WritingActivityTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<WritingActivityTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetWritingActivityTypeCollectionFromWritingActivityTypeDBList(
				_WritingActivityTypeDB.GetPageWritingActivityTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  WritingActivityTypeEntity GetWritingActivityTypeFromWritingActivityTypeDB(WritingActivityTypeEntity o)
		{
	if(o == null)
                return null;
			WritingActivityTypeEntity ret = new WritingActivityTypeEntity(o.WritingActivityTypeId ,o.WritingActivityTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<WritingActivityTypeEntity> GetWritingActivityTypeCollectionFromWritingActivityTypeDBList( List<WritingActivityTypeEntity> lst)
		{
			List<WritingActivityTypeEntity> RetLst = new List<WritingActivityTypeEntity>();
			foreach(WritingActivityTypeEntity o in lst)
			{
				RetLst.Add(GetWritingActivityTypeFromWritingActivityTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}
