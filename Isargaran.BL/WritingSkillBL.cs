﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: < مهارت نوشتن>
    /// </summary>
    public class WritingSkillBL
    {
        private readonly WritingSkillDB _WritingSkillDB;

        public WritingSkillBL()
        {
            _WritingSkillDB = new WritingSkillDB();
        }

        public void Add(WritingSkillEntity WritingSkillEntityParam, out Guid WritingSkillId)
        {
            _WritingSkillDB.AddWritingSkillDB(WritingSkillEntityParam, out WritingSkillId);
        }

        public void Update(WritingSkillEntity WritingSkillEntityParam)
        {
            _WritingSkillDB.UpdateWritingSkillDB(WritingSkillEntityParam);
        }

        public void Delete(WritingSkillEntity WritingSkillEntityParam)
        {
            _WritingSkillDB.DeleteWritingSkillDB(WritingSkillEntityParam);
        }

        public WritingSkillEntity GetSingleById(WritingSkillEntity WritingSkillEntityParam)
        {
            WritingSkillEntity o = GetWritingSkillFromWritingSkillDB(
                _WritingSkillDB.GetSingleWritingSkillDB(WritingSkillEntityParam));

            return o;
        }

        public List<WritingSkillEntity> GetAll()
        {
            List<WritingSkillEntity> lst = new List<WritingSkillEntity>();
            //string key = "WritingSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WritingSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWritingSkillCollectionFromWritingSkillDBList(
                _WritingSkillDB.GetAllWritingSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<WritingSkillEntity> GetAllIsActive()
        {
            List<WritingSkillEntity> lst = new List<WritingSkillEntity>();
            //string key = "WritingSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WritingSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWritingSkillCollectionFromWritingSkillDBList(
                _WritingSkillDB.GetAllIsActiveWritingSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<WritingSkillEntity> GetAllPaging(int currentPage, int pageSize,
                                                     string

                                                         sortExpression, out int count, string whereClause)
        {
            //string key = "WritingSkill_List_Page_" + currentPage ;
            //string countKey = "WritingSkill_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<WritingSkillEntity> lst = new List<WritingSkillEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WritingSkillEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetWritingSkillCollectionFromWritingSkillDBList(
                _WritingSkillDB.GetPageWritingSkillDB(pageSize, currentPage,
                                                      whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private WritingSkillEntity GetWritingSkillFromWritingSkillDB(WritingSkillEntity o)
        {
            if (o == null)
                return null;
            WritingSkillEntity ret = new WritingSkillEntity(o.WritingSkillId, o.WritingSkillPersianTitle, o.WritingSkillEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<WritingSkillEntity> GetWritingSkillCollectionFromWritingSkillDBList(List<WritingSkillEntity> lst)
        {
            List<WritingSkillEntity> RetLst = new List<WritingSkillEntity>();
            foreach (WritingSkillEntity o in lst)
            {
                RetLst.Add(GetWritingSkillFromWritingSkillDB(o));
            }
            return RetLst;

        }
    }
}