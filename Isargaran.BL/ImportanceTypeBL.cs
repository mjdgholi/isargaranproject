﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع اهمیت>
    /// </summary>


	public class ImportanceTypeBL 
	{	
	  	 private readonly ImportanceTypeDB _ImportanceTypeDB;					
			
		public ImportanceTypeBL()
		{
			_ImportanceTypeDB = new ImportanceTypeDB();
		}			
	
		public  void Add(ImportanceTypeEntity  ImportanceTypeEntityParam, out Guid ImportanceTypeId)
		{ 
			_ImportanceTypeDB.AddImportanceTypeDB(ImportanceTypeEntityParam,out ImportanceTypeId);			
		}

		public  void Update(ImportanceTypeEntity  ImportanceTypeEntityParam)
		{
			_ImportanceTypeDB.UpdateImportanceTypeDB(ImportanceTypeEntityParam);		
		}

		public  void Delete(ImportanceTypeEntity  ImportanceTypeEntityParam)
		{
			 _ImportanceTypeDB.DeleteImportanceTypeDB(ImportanceTypeEntityParam);			
		}

		public  ImportanceTypeEntity GetSingleById(ImportanceTypeEntity  ImportanceTypeEntityParam)
		{
			ImportanceTypeEntity o = GetImportanceTypeFromImportanceTypeDB(
			_ImportanceTypeDB.GetSingleImportanceTypeDB(ImportanceTypeEntityParam));
			
			return o;
		}

		public  List<ImportanceTypeEntity> GetAll()
		{
			List<ImportanceTypeEntity> lst = new List<ImportanceTypeEntity>();
			//string key = "ImportanceType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ImportanceTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetImportanceTypeCollectionFromImportanceTypeDBList(
				_ImportanceTypeDB.GetAllImportanceTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<ImportanceTypeEntity> GetAllIsActive()
        {
            List<ImportanceTypeEntity> lst = new List<ImportanceTypeEntity>();
            //string key = "ImportanceType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ImportanceTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetImportanceTypeCollectionFromImportanceTypeDBList(
            _ImportanceTypeDB.GetAllImportanceTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<ImportanceTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ImportanceType_List_Page_" + currentPage ;
			//string countKey = "ImportanceType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ImportanceTypeEntity> lst = new List<ImportanceTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ImportanceTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetImportanceTypeCollectionFromImportanceTypeDBList(
				_ImportanceTypeDB.GetPageImportanceTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ImportanceTypeEntity GetImportanceTypeFromImportanceTypeDB(ImportanceTypeEntity o)
		{
	if(o == null)
                return null;
			ImportanceTypeEntity ret = new ImportanceTypeEntity(o.ImportanceTypeId ,o.ImportanceTypeTitle ,o.Priority ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ImportanceTypeEntity> GetImportanceTypeCollectionFromImportanceTypeDBList( List<ImportanceTypeEntity> lst)
		{
			List<ImportanceTypeEntity> RetLst = new List<ImportanceTypeEntity>();
			foreach(ImportanceTypeEntity o in lst)
			{
				RetLst.Add(GetImportanceTypeFromImportanceTypeDB(o));
			}
			return RetLst;
			
		}


        
	}


}
