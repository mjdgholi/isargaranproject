﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <نوع تحصیل>
    /// </summary>
    public class EducationModeBL
    {
        private readonly EducationModeDB _EducationModeDB;

        public EducationModeBL()
        {
            _EducationModeDB = new EducationModeDB();
        }

        public void Add(EducationModeEntity EducationModeEntityParam, out Guid EducationModeId)
        {
            _EducationModeDB.AddEducationModeDB(EducationModeEntityParam, out EducationModeId);
        }

        public void Update(EducationModeEntity EducationModeEntityParam)
        {
            _EducationModeDB.UpdateEducationModeDB(EducationModeEntityParam);
        }

        public void Delete(EducationModeEntity EducationModeEntityParam)
        {
            _EducationModeDB.DeleteEducationModeDB(EducationModeEntityParam);
        }

        public EducationModeEntity GetSingleById(EducationModeEntity EducationModeEntityParam)
        {
            EducationModeEntity o = GetEducationModeFromEducationModeDB(
                _EducationModeDB.GetSingleEducationModeDB(EducationModeEntityParam));

            return o;
        }

        public List<EducationModeEntity> GetAll()
        {
            List<EducationModeEntity> lst = new List<EducationModeEntity>();
            //string key = "EducationMode_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationModeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationModeCollectionFromEducationModeDBList(
                _EducationModeDB.GetAllEducationModeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<EducationModeEntity> GetAllIsActive()
        {
            List<EducationModeEntity> lst = new List<EducationModeEntity>();
            //string key = "EducationMode_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationModeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationModeCollectionFromEducationModeDBList(
                _EducationModeDB.GetAllIsActiveEducationModeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<EducationModeEntity> GetAllPaging(int currentPage, int pageSize,
                                                      string

                                                          sortExpression, out int count, string whereClause)
        {
            //string key = "EducationMode_List_Page_" + currentPage ;
            //string countKey = "EducationMode_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<EducationModeEntity> lst = new List<EducationModeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationModeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetEducationModeCollectionFromEducationModeDBList(
                _EducationModeDB.GetPageEducationModeDB(pageSize, currentPage,
                                                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private EducationModeEntity GetEducationModeFromEducationModeDB(EducationModeEntity o)
        {
            if (o == null)
                return null;
            EducationModeEntity ret = new EducationModeEntity(o.EducationModeId, o.EducationModePersianTitle, o.EducationModeEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<EducationModeEntity> GetEducationModeCollectionFromEducationModeDBList(List<EducationModeEntity> lst)
        {
            List<EducationModeEntity> RetLst = new List<EducationModeEntity>();
            foreach (EducationModeEntity o in lst)
            {
                RetLst.Add(GetEducationModeFromEducationModeDB(o));
            }
            return RetLst;

        }


    }

}