﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/28>
    // Description:	<مناطق جنگی>
    /// </summary>

	public class WarZoneBL 
	{	
	  	 private readonly WarZoneDB _WarZoneDB;					
			
		public WarZoneBL()
		{
			_WarZoneDB = new WarZoneDB();
		}			
	
		public  void Add(WarZoneEntity  WarZoneEntityParam, out Guid WarZoneId)
		{ 
			_WarZoneDB.AddWarZoneDB(WarZoneEntityParam,out WarZoneId);			
		}

		public  void Update(WarZoneEntity  WarZoneEntityParam)
		{
			_WarZoneDB.UpdateWarZoneDB(WarZoneEntityParam);		
		}

		public  void Delete(WarZoneEntity  WarZoneEntityParam)
		{
			 _WarZoneDB.DeleteWarZoneDB(WarZoneEntityParam);			
		}

		public  WarZoneEntity GetSingleById(WarZoneEntity  WarZoneEntityParam)
		{
			WarZoneEntity o = GetWarZoneFromWarZoneDB(
			_WarZoneDB.GetSingleWarZoneDB(WarZoneEntityParam));
			
			return o;
		}

		public  List<WarZoneEntity> GetAll()
		{
			List<WarZoneEntity> lst = new List<WarZoneEntity>();
			//string key = "WarZone_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<WarZoneEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetWarZoneCollectionFromWarZoneDBList(
				_WarZoneDB.GetAllWarZoneDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<WarZoneEntity> GetAllIsActive()
        {
            List<WarZoneEntity> lst = new List<WarZoneEntity>();
            //string key = "WarZone_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WarZoneEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWarZoneCollectionFromWarZoneDBList(
            _WarZoneDB.GetAllWarZoneIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<WarZoneEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "WarZone_List_Page_" + currentPage ;
			//string countKey = "WarZone_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<WarZoneEntity> lst = new List<WarZoneEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<WarZoneEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetWarZoneCollectionFromWarZoneDBList(
				_WarZoneDB.GetPageWarZoneDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  WarZoneEntity GetWarZoneFromWarZoneDB(WarZoneEntity o)
		{
	if(o == null)
                return null;
			WarZoneEntity ret = new WarZoneEntity(o.WarZoneId ,o.WarZoneTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<WarZoneEntity> GetWarZoneCollectionFromWarZoneDBList( List<WarZoneEntity> lst)
		{
			List<WarZoneEntity> RetLst = new List<WarZoneEntity>();
			foreach(WarZoneEntity o in lst)
			{
				RetLst.Add(GetWarZoneFromWarZoneDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}
