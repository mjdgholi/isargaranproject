﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/14>
    // Description:	<تنظیمات پورتال>
    /// </summary>
    public class PortalSettingBL
    {
        private readonly PortalSettingDB _PortalSettingDB;

        public PortalSettingBL()
        {
            _PortalSettingDB = new PortalSettingDB();
        }

        public void Add(PortalSettingEntity PortalSettingEntityParam, out Guid PortalSettingId)
        {
            _PortalSettingDB.AddPortalSettingDB(PortalSettingEntityParam, out PortalSettingId);
        }

        public void Update(PortalSettingEntity PortalSettingEntityParam)
        {
            _PortalSettingDB.UpdatePortalSettingDB(PortalSettingEntityParam);
        }

        public void Delete(PortalSettingEntity PortalSettingEntityParam)
        {
            _PortalSettingDB.DeletePortalSettingDB(PortalSettingEntityParam);
        }

        public PortalSettingEntity GetSingleById(PortalSettingEntity PortalSettingEntityParam)
        {
            PortalSettingEntity o = GetPortalSettingFromPortalSettingDB(
                _PortalSettingDB.GetSinglePortalSettingDB(PortalSettingEntityParam));

            return o;
        }

        public List<PortalSettingEntity> GetAll()
        {
            List<PortalSettingEntity> lst = new List<PortalSettingEntity>();
            //string key = "PortalSetting_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PortalSettingEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetPortalSettingCollectionFromPortalSettingDBList(
                _PortalSettingDB.GetAllPortalSettingDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<PortalSettingEntity> GetAllIsActive()
        {
            List<PortalSettingEntity> lst = new List<PortalSettingEntity>();
            //string key = "PortalSetting_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PortalSettingEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetPortalSettingCollectionFromPortalSettingDBList(
                _PortalSettingDB.GetAllIaActivePortalSettingDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<PortalSettingEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "PortalSetting_List_Page_" + currentPage ;
            //string countKey = "PortalSetting_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<PortalSettingEntity> lst = new List<PortalSettingEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PortalSettingEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetPortalSettingCollectionFromPortalSettingDBList(
                _PortalSettingDB.GetPagePortalSettingDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private PortalSettingEntity GetPortalSettingFromPortalSettingDB(PortalSettingEntity o)
        {
            if (o == null)
                return null;
            PortalSettingEntity ret = new PortalSettingEntity(o.PortalSettingId, o.PortalSettingPersianTitle,
                o.PortalSettingEnglishTitle, o.StyleTitle, o.TelerikSkin, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<PortalSettingEntity> GetPortalSettingCollectionFromPortalSettingDBList(
            List<PortalSettingEntity> lst)
        {
            List<PortalSettingEntity> RetLst = new List<PortalSettingEntity>();
            foreach (PortalSettingEntity o in lst)
            {
                RetLst.Add(GetPortalSettingFromPortalSettingDB(o));
            }
            return RetLst;

        }
    }
}
