﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <نوع فارغ تحصیل>
    /// </summary>

//-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class GraduateTypeBL
    {
        private readonly GraduateTypeDB _GraduateTypeDB;

        public GraduateTypeBL()
        {
            _GraduateTypeDB = new GraduateTypeDB();
        }

        public void Add(GraduateTypeEntity GraduateTypeEntityParam, out Guid GraduateTypeId)
        {
            _GraduateTypeDB.AddGraduateTypeDB(GraduateTypeEntityParam, out GraduateTypeId);
        }

        public void Update(GraduateTypeEntity GraduateTypeEntityParam)
        {
            _GraduateTypeDB.UpdateGraduateTypeDB(GraduateTypeEntityParam);
        }

        public void Delete(GraduateTypeEntity GraduateTypeEntityParam)
        {
            _GraduateTypeDB.DeleteGraduateTypeDB(GraduateTypeEntityParam);
        }

        public GraduateTypeEntity GetSingleById(GraduateTypeEntity GraduateTypeEntityParam)
        {
            GraduateTypeEntity o = GetGraduateTypeFromGraduateTypeDB(
                _GraduateTypeDB.GetSingleGraduateTypeDB(GraduateTypeEntityParam));

            return o;
        }

        public List<GraduateTypeEntity> GetAll()
        {
            List<GraduateTypeEntity> lst = new List<GraduateTypeEntity>();
            //string key = "GraduateType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GraduateTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGraduateTypeCollectionFromGraduateTypeDBList(
                _GraduateTypeDB.GetAllGraduateTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<GraduateTypeEntity> GetAllIsActive()
        {
            List<GraduateTypeEntity> lst = new List<GraduateTypeEntity>();
            //string key = "GraduateType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GraduateTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGraduateTypeCollectionFromGraduateTypeDBList(
                _GraduateTypeDB.GetAllIsActiveGraduateTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<GraduateTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                     string

                                                         sortExpression, out int count, string whereClause)
        {
            //string key = "GraduateType_List_Page_" + currentPage ;
            //string countKey = "GraduateType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<GraduateTypeEntity> lst = new List<GraduateTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GraduateTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetGraduateTypeCollectionFromGraduateTypeDBList(
                _GraduateTypeDB.GetPageGraduateTypeDB(pageSize, currentPage,
                                                      whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private GraduateTypeEntity GetGraduateTypeFromGraduateTypeDB(GraduateTypeEntity o)
        {
            if (o == null)
                return null;
            GraduateTypeEntity ret = new GraduateTypeEntity(o.GraduateTypeId, o.GraduateTypeTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<GraduateTypeEntity> GetGraduateTypeCollectionFromGraduateTypeDBList(List<GraduateTypeEntity> lst)
        {
            List<GraduateTypeEntity> RetLst = new List<GraduateTypeEntity>();
            foreach (GraduateTypeEntity o in lst)
            {
                RetLst.Add(GetGraduateTypeFromGraduateTypeDB(o));
            }
            return RetLst;

        }


    }

}