﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/24>
    /// Description: <پرونده اطلاعات اقدامات درخواست ایثارگر>
    /// </summary>
	public class DossierSacrifice_RequestHistoryBL 
	{	
	  	 private readonly DossierSacrifice_RequestHistoryDB _DossierSacrifice_RequestHistoryDB;					
			
		public DossierSacrifice_RequestHistoryBL()
		{
			_DossierSacrifice_RequestHistoryDB = new DossierSacrifice_RequestHistoryDB();
		}			
	
		public  void Add(DossierSacrifice_RequestHistoryEntity  DossierSacrifice_RequestHistoryEntityParam, out Guid DossierSacrifice_RequestHistoryId)
		{ 
			_DossierSacrifice_RequestHistoryDB.AddDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntityParam,out DossierSacrifice_RequestHistoryId);			
		}

		public  void Update(DossierSacrifice_RequestHistoryEntity  DossierSacrifice_RequestHistoryEntityParam)
		{
			_DossierSacrifice_RequestHistoryDB.UpdateDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntityParam);		
		}

		public  void Delete(DossierSacrifice_RequestHistoryEntity  DossierSacrifice_RequestHistoryEntityParam)
		{
			 _DossierSacrifice_RequestHistoryDB.DeleteDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntityParam);			
		}

		public  DossierSacrifice_RequestHistoryEntity GetSingleById(DossierSacrifice_RequestHistoryEntity  DossierSacrifice_RequestHistoryEntityParam)
		{
			DossierSacrifice_RequestHistoryEntity o = GetDossierSacrifice_RequestHistoryFromDossierSacrifice_RequestHistoryDB(
			_DossierSacrifice_RequestHistoryDB.GetSingleDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_RequestHistoryEntity> GetAll()
		{
			List<DossierSacrifice_RequestHistoryEntity> lst = new List<DossierSacrifice_RequestHistoryEntity>();
			//string key = "DossierSacrifice_RequestHistory_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_RequestHistoryEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_RequestHistoryCollectionFromDossierSacrifice_RequestHistoryDBList(
				_DossierSacrifice_RequestHistoryDB.GetAllDossierSacrifice_RequestHistoryDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_RequestHistoryEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_RequestHistory_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_RequestHistory_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_RequestHistoryEntity> lst = new List<DossierSacrifice_RequestHistoryEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_RequestHistoryEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_RequestHistoryCollectionFromDossierSacrifice_RequestHistoryDBList(
				_DossierSacrifice_RequestHistoryDB.GetPageDossierSacrifice_RequestHistoryDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_RequestHistoryEntity GetDossierSacrifice_RequestHistoryFromDossierSacrifice_RequestHistoryDB(DossierSacrifice_RequestHistoryEntity o)
		{
	if(o == null)
                return null;
    DossierSacrifice_RequestHistoryEntity ret = new DossierSacrifice_RequestHistoryEntity(o.DossierSacrifice_RequestHistoryId, o.DossierSacrifice_RequestId, o.StatusId, o.ActionTypeId, o.ActionDate, o.ActionDescription, o.CreationDate, o.ModificationDate, o.IsValid);
			return ret;
		}
		
		private  List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryCollectionFromDossierSacrifice_RequestHistoryDBList( List<DossierSacrifice_RequestHistoryEntity> lst)
		{
			List<DossierSacrifice_RequestHistoryEntity> RetLst = new List<DossierSacrifice_RequestHistoryEntity>();
			foreach(DossierSacrifice_RequestHistoryEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_RequestHistoryFromDossierSacrifice_RequestHistoryDB(o));
			}
			return RetLst;
			
		}




        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryCollectionByActionType(DossierSacrifice_RequestHistoryEntity DossierSacrifice_RequestHistoryEntityParam)
{
    return GetDossierSacrifice_RequestHistoryCollectionFromDossierSacrifice_RequestHistoryDBList(_DossierSacrifice_RequestHistoryDB.GetDossierSacrifice_RequestHistoryDBCollectionByActionTypeDB(DossierSacrifice_RequestHistoryEntityParam));
}

        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryCollectionByDossierSacrifice_Request(DossierSacrifice_RequestHistoryEntity DossierSacrifice_RequestHistoryEntityParam)
{
    return GetDossierSacrifice_RequestHistoryCollectionFromDossierSacrifice_RequestHistoryDBList(_DossierSacrifice_RequestHistoryDB.GetDossierSacrifice_RequestHistoryDBCollectionByDossierSacrifice_RequestDB(DossierSacrifice_RequestHistoryEntityParam));
}

        public List<DossierSacrifice_RequestHistoryEntity> GetDossierSacrifice_RequestHistoryCollectionByStatus(DossierSacrifice_RequestHistoryEntity DossierSacrifice_RequestHistoryEntityParam)
{
    return GetDossierSacrifice_RequestHistoryCollectionFromDossierSacrifice_RequestHistoryDBList(_DossierSacrifice_RequestHistoryDB.GetDossierSacrifice_RequestHistoryDBCollectionByStatusDB(DossierSacrifice_RequestHistoryEntityParam));
}


}

}