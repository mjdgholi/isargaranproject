﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/18>
    /// Description: <تسهیلات و خدمات پزشکی>
    /// </summary>

	public class MedicalFacilitieAndServiceBL 
	{	
	  	 private readonly MedicalFacilitieAndServiceDB _MedicalFacilitieAndServiceDB;					
			
		public MedicalFacilitieAndServiceBL()
		{
			_MedicalFacilitieAndServiceDB = new MedicalFacilitieAndServiceDB();
		}			
	
		public  void Add(MedicalFacilitieAndServiceEntity  MedicalFacilitieAndServiceEntityParam, out Guid MedicalFacilitieAndServiceId)
		{ 
			_MedicalFacilitieAndServiceDB.AddMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntityParam,out MedicalFacilitieAndServiceId);			
		}

		public  void Update(MedicalFacilitieAndServiceEntity  MedicalFacilitieAndServiceEntityParam)
		{
			_MedicalFacilitieAndServiceDB.UpdateMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntityParam);		
		}

		public  void Delete(MedicalFacilitieAndServiceEntity  MedicalFacilitieAndServiceEntityParam)
		{
			 _MedicalFacilitieAndServiceDB.DeleteMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntityParam);			
		}

		public  MedicalFacilitieAndServiceEntity GetSingleById(MedicalFacilitieAndServiceEntity  MedicalFacilitieAndServiceEntityParam)
		{
			MedicalFacilitieAndServiceEntity o = GetMedicalFacilitieAndServiceFromMedicalFacilitieAndServiceDB(
			_MedicalFacilitieAndServiceDB.GetSingleMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntityParam));
			
			return o;
		}

		public  List<MedicalFacilitieAndServiceEntity> GetAll()
		{
			List<MedicalFacilitieAndServiceEntity> lst = new List<MedicalFacilitieAndServiceEntity>();
			//string key = "MedicalFacilitieAndService_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MedicalFacilitieAndServiceEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetMedicalFacilitieAndServiceCollectionFromMedicalFacilitieAndServiceDBList(
				_MedicalFacilitieAndServiceDB.GetAllMedicalFacilitieAndServiceDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<MedicalFacilitieAndServiceEntity> GetAllIsActive()
        {
            List<MedicalFacilitieAndServiceEntity> lst = new List<MedicalFacilitieAndServiceEntity>();
            //string key = "MedicalFacilitieAndService_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MedicalFacilitieAndServiceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMedicalFacilitieAndServiceCollectionFromMedicalFacilitieAndServiceDBList(
            _MedicalFacilitieAndServiceDB.GetAllMedicalFacilitieAndServiceIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<MedicalFacilitieAndServiceEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "MedicalFacilitieAndService_List_Page_" + currentPage ;
			//string countKey = "MedicalFacilitieAndService_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<MedicalFacilitieAndServiceEntity> lst = new List<MedicalFacilitieAndServiceEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MedicalFacilitieAndServiceEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetMedicalFacilitieAndServiceCollectionFromMedicalFacilitieAndServiceDBList(
				_MedicalFacilitieAndServiceDB.GetPageMedicalFacilitieAndServiceDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  MedicalFacilitieAndServiceEntity GetMedicalFacilitieAndServiceFromMedicalFacilitieAndServiceDB(MedicalFacilitieAndServiceEntity o)
		{
	if(o == null)
                return null;
			MedicalFacilitieAndServiceEntity ret = new MedicalFacilitieAndServiceEntity(o.MedicalFacilitieAndServiceId ,o.MedicalFacilitieAndServiceTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<MedicalFacilitieAndServiceEntity> GetMedicalFacilitieAndServiceCollectionFromMedicalFacilitieAndServiceDBList( List<MedicalFacilitieAndServiceEntity> lst)
		{
			List<MedicalFacilitieAndServiceEntity> RetLst = new List<MedicalFacilitieAndServiceEntity>();
			foreach(MedicalFacilitieAndServiceEntity o in lst)
			{
				RetLst.Add(GetMedicalFacilitieAndServiceFromMedicalFacilitieAndServiceDB(o));
			}
			return RetLst;
			
		} 
						
	}

}


