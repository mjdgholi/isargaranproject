﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <میزان حضور در سرکار>
    /// </summary>

	public class DurationTimeAtWorkBL 
	{	
	  	 private readonly DurationTimeAtWorkDB _DurationTimeAtWorkDB;					
			
		public DurationTimeAtWorkBL()
		{
			_DurationTimeAtWorkDB = new DurationTimeAtWorkDB();
		}			
	
		public  void Add(DurationTimeAtWorkEntity  DurationTimeAtWorkEntityParam, out Guid DurationTimeAtWorkId)
		{ 
			_DurationTimeAtWorkDB.AddDurationTimeAtWorkDB(DurationTimeAtWorkEntityParam,out DurationTimeAtWorkId);			
		}

		public  void Update(DurationTimeAtWorkEntity  DurationTimeAtWorkEntityParam)
		{
			_DurationTimeAtWorkDB.UpdateDurationTimeAtWorkDB(DurationTimeAtWorkEntityParam);		
		}

		public  void Delete(DurationTimeAtWorkEntity  DurationTimeAtWorkEntityParam)
		{
			 _DurationTimeAtWorkDB.DeleteDurationTimeAtWorkDB(DurationTimeAtWorkEntityParam);			
		}

		public  DurationTimeAtWorkEntity GetSingleById(DurationTimeAtWorkEntity  DurationTimeAtWorkEntityParam)
		{
			DurationTimeAtWorkEntity o = GetDurationTimeAtWorkFromDurationTimeAtWorkDB(
			_DurationTimeAtWorkDB.GetSingleDurationTimeAtWorkDB(DurationTimeAtWorkEntityParam));
			
			return o;
		}

		public  List<DurationTimeAtWorkEntity> GetAll()
		{
			List<DurationTimeAtWorkEntity> lst = new List<DurationTimeAtWorkEntity>();
			//string key = "DurationTimeAtWork_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DurationTimeAtWorkEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDurationTimeAtWorkCollectionFromDurationTimeAtWorkDBList(
				_DurationTimeAtWorkDB.GetAllDurationTimeAtWorkDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<DurationTimeAtWorkEntity> GetAllIsActive()
        {
            List<DurationTimeAtWorkEntity> lst = new List<DurationTimeAtWorkEntity>();
            //string key = "DurationTimeAtWork_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DurationTimeAtWorkEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDurationTimeAtWorkCollectionFromDurationTimeAtWorkDBList(
            _DurationTimeAtWorkDB.GetAllDurationTimeAtWorkIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<DurationTimeAtWorkEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DurationTimeAtWork_List_Page_" + currentPage ;
			//string countKey = "DurationTimeAtWork_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DurationTimeAtWorkEntity> lst = new List<DurationTimeAtWorkEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DurationTimeAtWorkEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDurationTimeAtWorkCollectionFromDurationTimeAtWorkDBList(
				_DurationTimeAtWorkDB.GetPageDurationTimeAtWorkDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DurationTimeAtWorkEntity GetDurationTimeAtWorkFromDurationTimeAtWorkDB(DurationTimeAtWorkEntity o)
		{
	if(o == null)
                return null;
			DurationTimeAtWorkEntity ret = new DurationTimeAtWorkEntity(o.DurationTimeAtWorkId ,o.DurationTimeAtWorkTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<DurationTimeAtWorkEntity> GetDurationTimeAtWorkCollectionFromDurationTimeAtWorkDBList( List<DurationTimeAtWorkEntity> lst)
		{
			List<DurationTimeAtWorkEntity> RetLst = new List<DurationTimeAtWorkEntity>();
			foreach(DurationTimeAtWorkEntity o in lst)
			{
				RetLst.Add(GetDurationTimeAtWorkFromDurationTimeAtWorkDB(o));
			}
			return RetLst;
			
		} 
				
		
	}
}


