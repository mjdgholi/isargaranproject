﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <پرونده  ایثارگری ایثارگری>
    /// </summary>


    public class DossierSacrifice_SacrificeBL
    {
        private readonly DossierSacrifice_SacrificeDB _DossierSacrifice_SacrificeDB;

        public DossierSacrifice_SacrificeBL()
        {
            _DossierSacrifice_SacrificeDB = new DossierSacrifice_SacrificeDB();
        }

        void CheckValidation(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            SacrificeBL _sacrificeBL = new SacrificeBL();
            var sacrificeEntity = new SacrificeEntity()
            {
                SacrificeId = new Guid(DossierSacrifice_SacrificeEntityParam.SacrificeId.ToString())
            };
            var mysacrifice = _sacrificeBL.GetSingleById(sacrificeEntity);
            if (mysacrifice.IsPercentValue && DossierSacrifice_SacrificeEntityParam.PercentValue.ToString() == "")
            {
                throw new ItcApplicationErrorManagerException("درصد را وارد نمایید");
            }
            if (mysacrifice.IsTimeValue && DossierSacrifice_SacrificeEntityParam.ValueInYear.ToString() == "" && DossierSacrifice_SacrificeEntityParam.ValueInMonth.ToString() == "" && DossierSacrifice_SacrificeEntityParam.ValueInDay.ToString() == "")
            {
                throw new ItcApplicationErrorManagerException("مقادیر تعداد سال یا تعداد ماه یا تعداد روز را وارد نمایید");
            }
        }
        public void Add(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam,
                        out Guid DossierSacrifice_SacrificeId)
        {
            CheckValidation(DossierSacrifice_SacrificeEntityParam);
            _DossierSacrifice_SacrificeDB.AddDossierSacrifice_SacrificeDB(DossierSacrifice_SacrificeEntityParam,
                                                                          out DossierSacrifice_SacrificeId);
        }

        public void Update(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            CheckValidation(DossierSacrifice_SacrificeEntityParam);
            _DossierSacrifice_SacrificeDB.UpdateDossierSacrifice_SacrificeDB(DossierSacrifice_SacrificeEntityParam);
        }

        public void Delete(DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            _DossierSacrifice_SacrificeDB.DeleteDossierSacrifice_SacrificeDB(DossierSacrifice_SacrificeEntityParam);
        }

        public DossierSacrifice_SacrificeEntity GetSingleById(
            DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            DossierSacrifice_SacrificeEntity o = GetDossierSacrifice_SacrificeFromDossierSacrifice_SacrificeDB(
                _DossierSacrifice_SacrificeDB.GetSingleDossierSacrifice_SacrificeDB(
                    DossierSacrifice_SacrificeEntityParam));

            return o;
        }

        public List<DossierSacrifice_SacrificeEntity> GetAll()
        {
            List<DossierSacrifice_SacrificeEntity> lst = new List<DossierSacrifice_SacrificeEntity>();
            //string key = "DossierSacrifice_Sacrifice_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_SacrificeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_SacrificeCollectionFromDossierSacrifice_SacrificeDBList(
                _DossierSacrifice_SacrificeDB.GetAllDossierSacrifice_SacrificeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_SacrificeEntity> GetAllPaging(int currentPage, int pageSize,
                                                                   string

                                                                       sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Sacrifice_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Sacrifice_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_SacrificeEntity> lst = new List<DossierSacrifice_SacrificeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_SacrificeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_SacrificeCollectionFromDossierSacrifice_SacrificeDBList(
                _DossierSacrifice_SacrificeDB.GetPageDossierSacrifice_SacrificeDB(pageSize, currentPage,
                                                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_SacrificeEntity GetDossierSacrifice_SacrificeFromDossierSacrifice_SacrificeDB(
            DossierSacrifice_SacrificeEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_SacrificeEntity ret = new DossierSacrifice_SacrificeEntity(o.DossierSacrifice_SacrificeId,
                                                                                        o.DossierSacrificeId,
                                                                                        o.SacrificeId, o.TotalValueInDay,
                                                                                        o.ValueInYear, o.ValueInMonth,
                                                                                        o.ValueInDay,o.DurationTypeId, o.PercentValue,
                                                                                        o.StartDate, o.Endate,
                                                                                        o.CreationDate,
                                                                                        o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<DossierSacrifice_SacrificeEntity>
            GetDossierSacrifice_SacrificeCollectionFromDossierSacrifice_SacrificeDBList(
            List<DossierSacrifice_SacrificeEntity> lst)
        {
            List<DossierSacrifice_SacrificeEntity> RetLst = new List<DossierSacrifice_SacrificeEntity>();
            foreach (DossierSacrifice_SacrificeEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_SacrificeFromDossierSacrifice_SacrificeDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_SacrificeEntity> GetDossierSacrifice_SacrificeCollectionByDossierSacrifice(
            DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            return
                GetDossierSacrifice_SacrificeCollectionFromDossierSacrifice_SacrificeDBList(
                    _DossierSacrifice_SacrificeDB.GetDossierSacrifice_SacrificeDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_SacrificeEntityParam));
        }

        public List<DossierSacrifice_SacrificeEntity> GetDossierSacrifice_SacrificeCollectionBySacrifice(
            DossierSacrifice_SacrificeEntity DossierSacrifice_SacrificeEntityParam)
        {
            return
                GetDossierSacrifice_SacrificeCollectionFromDossierSacrifice_SacrificeDBList(
                    _DossierSacrifice_SacrificeDB.GetDossierSacrifice_SacrificeDBCollectionBySacrificeDB(
                        DossierSacrifice_SacrificeEntityParam));
        }



    }

}
