﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/03/11>
    /// Description: <دسته بندی فرم>
    /// </summary>
    public class WebFormCategorizeBL
    {
        private readonly WebFormCategorizeDB _WebFormCategorizeDB;

        public WebFormCategorizeBL()
        {
            _WebFormCategorizeDB = new WebFormCategorizeDB();
        }

        public void Add(WebFormCategorizeEntity WebFormCategorizeEntityParam, out Guid WebFormCategorizeId)
        {
            _WebFormCategorizeDB.AddWebFormCategorizeDB(WebFormCategorizeEntityParam, out WebFormCategorizeId);
        }

        public void Update(WebFormCategorizeEntity WebFormCategorizeEntityParam)
        {
            _WebFormCategorizeDB.UpdateWebFormCategorizeDB(WebFormCategorizeEntityParam);
        }

        public void Delete(WebFormCategorizeEntity WebFormCategorizeEntityParam)
        {
            _WebFormCategorizeDB.DeleteWebFormCategorizeDB(WebFormCategorizeEntityParam);
        }

        public WebFormCategorizeEntity GetSingleById(WebFormCategorizeEntity WebFormCategorizeEntityParam)
        {
            WebFormCategorizeEntity o = GetWebFormCategorizeFromWebFormCategorizeDB(
                _WebFormCategorizeDB.GetSingleWebFormCategorizeDB(WebFormCategorizeEntityParam));

            return o;
        }

        public List<WebFormCategorizeEntity> GetAll()
        {
            List<WebFormCategorizeEntity> lst = new List<WebFormCategorizeEntity>();
            //string key = "WebFormCategorize_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WebFormCategorizeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWebFormCategorizeCollectionFromWebFormCategorizeDBList(
                _WebFormCategorizeDB.GetAllWebFormCategorizeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<WebFormCategorizeEntity> GetAllIsActive()
        {
            List<WebFormCategorizeEntity> lst = new List<WebFormCategorizeEntity>();
            //string key = "WebFormCategorize_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WebFormCategorizeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWebFormCategorizeCollectionFromWebFormCategorizeDBList(
                _WebFormCategorizeDB.GetAllIsActiveWebFormCategorizeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<WebFormCategorizeEntity> GetAllPaging(int currentPage, int pageSize,string sortExpression, out int count, string whereClause)
        {
            //string key = "WebFormCategorize_List_Page_" + currentPage ;
            //string countKey = "WebFormCategorize_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<WebFormCategorizeEntity> lst = new List<WebFormCategorizeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WebFormCategorizeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetWebFormCategorizeCollectionFromWebFormCategorizeDBList(
                _WebFormCategorizeDB.GetPageWebFormCategorizeDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private WebFormCategorizeEntity GetWebFormCategorizeFromWebFormCategorizeDB(WebFormCategorizeEntity o)
        {
            if (o == null)
                return null;
            WebFormCategorizeEntity ret = new WebFormCategorizeEntity(o.WebFormCategorizeId, o.WebFormCategorizeTitle,o.Priority,
                o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<WebFormCategorizeEntity> GetWebFormCategorizeCollectionFromWebFormCategorizeDBList(List<WebFormCategorizeEntity> lst)
        {
            List<WebFormCategorizeEntity> RetLst = new List<WebFormCategorizeEntity>();
            foreach (WebFormCategorizeEntity o in lst)
            {
                RetLst.Add(GetWebFormCategorizeFromWebFormCategorizeDB(o));
            }
            return RetLst;

        }

        public List<WebFormCategorizeEntity> GetAllWebformCategorizePerRowAccess(string objectTitleStr, int userId)
        {
            return _WebFormCategorizeDB.GetAllWebformCategorizePerRowAccess(objectTitleStr, userId);
        }
    }

}