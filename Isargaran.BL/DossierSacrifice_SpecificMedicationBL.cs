﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/17>
    /// Description: <اطلاعات مصرف داروهای خاص>
    /// </summary>
    public class DossierSacrifice_SpecificMedicationBL
    {
        private readonly DossierSacrifice_SpecificMedicationDB _DossierSacrifice_SpecificMedicationDB;

        public DossierSacrifice_SpecificMedicationBL()
        {
            _DossierSacrifice_SpecificMedicationDB = new DossierSacrifice_SpecificMedicationDB();
        }

        public void Add(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam, out Guid DossierSacrifice_SpecificMedicationId)
        {
            _DossierSacrifice_SpecificMedicationDB.AddDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntityParam, out DossierSacrifice_SpecificMedicationId);
        }

        public void Update(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam)
        {
            _DossierSacrifice_SpecificMedicationDB.UpdateDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntityParam);
        }

        public void Delete(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam)
        {
            _DossierSacrifice_SpecificMedicationDB.DeleteDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntityParam);
        }

        public DossierSacrifice_SpecificMedicationEntity GetSingleById(DossierSacrifice_SpecificMedicationEntity DossierSacrifice_SpecificMedicationEntityParam)
        {
            DossierSacrifice_SpecificMedicationEntity o = GetDossierSacrifice_SpecificMedicationFromDossierSacrifice_SpecificMedicationDB(
                _DossierSacrifice_SpecificMedicationDB.GetSingleDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntityParam));

            return o;
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetAll()
        {
            List<DossierSacrifice_SpecificMedicationEntity> lst = new List<DossierSacrifice_SpecificMedicationEntity>();
            //string key = "DossierSacrifice_SpecificMedication_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_SpecificMedicationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_SpecificMedicationCollectionFromDossierSacrifice_SpecificMedicationDBList(
                _DossierSacrifice_SpecificMedicationDB.GetAllDossierSacrifice_SpecificMedicationDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_SpecificMedication_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_SpecificMedication_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_SpecificMedicationEntity> lst = new List<DossierSacrifice_SpecificMedicationEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_SpecificMedicationEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_SpecificMedicationCollectionFromDossierSacrifice_SpecificMedicationDBList(
                _DossierSacrifice_SpecificMedicationDB.GetPageDossierSacrifice_SpecificMedicationDB(pageSize, currentPage,
                                                                                                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_SpecificMedicationEntity GetDossierSacrifice_SpecificMedicationFromDossierSacrifice_SpecificMedicationDB(DossierSacrifice_SpecificMedicationEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_SpecificMedicationEntity ret = new DossierSacrifice_SpecificMedicationEntity(o.DossierSacrifice_SpecificMedicationId, o.DossierSacrificeId, o.SpecificMedicationId, o.CountryId, o.MedicationConsum, o.MedicationNo, o.MedicationPrize, o.StartDate, o.EndDate, o.IsMedicationFinished, o.Description, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationCollectionFromDossierSacrifice_SpecificMedicationDBList(List<DossierSacrifice_SpecificMedicationEntity> lst)
        {
            List<DossierSacrifice_SpecificMedicationEntity> RetLst = new List<DossierSacrifice_SpecificMedicationEntity>();
            foreach (DossierSacrifice_SpecificMedicationEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_SpecificMedicationFromDossierSacrifice_SpecificMedicationDB(o));
            }
            return RetLst;

        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationCollectionByCountry(Guid CountryId)
        {
            return GetDossierSacrifice_SpecificMedicationCollectionFromDossierSacrifice_SpecificMedicationDBList(_DossierSacrifice_SpecificMedicationDB.GetDossierSacrifice_SpecificMedicationDBCollectionByCountryDB(CountryId));
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationCollectionByDossierSacrifice(Guid DossierSacrificeId)
        {
            return GetDossierSacrifice_SpecificMedicationCollectionFromDossierSacrifice_SpecificMedicationDBList(_DossierSacrifice_SpecificMedicationDB.GetDossierSacrifice_SpecificMedicationDBCollectionByDossierSacrificeDB(DossierSacrificeId));
        }

        public List<DossierSacrifice_SpecificMedicationEntity> GetDossierSacrifice_SpecificMedicationCollectionBySpecificMedication(Guid SpecificMedicationId)
        {
            return GetDossierSacrifice_SpecificMedicationCollectionFromDossierSacrifice_SpecificMedicationDBList(_DossierSacrifice_SpecificMedicationDB.GetDossierSacrifice_SpecificMedicationDBCollectionBySpecificMedicationDB(SpecificMedicationId));
        }

    }
}