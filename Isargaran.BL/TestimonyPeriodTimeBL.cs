﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/23>
    /// Description: <دوره زمانی شهادت>
    /// </summary>
    public class TestimonyPeriodTimeBL
    {
        private readonly TestimonyPeriodTimeDB _TestimonyPeriodTimeDB;

        public TestimonyPeriodTimeBL()
        {
            _TestimonyPeriodTimeDB = new TestimonyPeriodTimeDB();
        }

        public void Add(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam, out Guid TestimonyPeriodTimeId)
        {
            _TestimonyPeriodTimeDB.AddTestimonyPeriodTimeDB(TestimonyPeriodTimeEntityParam, out TestimonyPeriodTimeId);
        }

        public void Update(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam)
        {
            _TestimonyPeriodTimeDB.UpdateTestimonyPeriodTimeDB(TestimonyPeriodTimeEntityParam);
        }

        public void Delete(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam)
        {
            _TestimonyPeriodTimeDB.DeleteTestimonyPeriodTimeDB(TestimonyPeriodTimeEntityParam);
        }

        public TestimonyPeriodTimeEntity GetSingleById(TestimonyPeriodTimeEntity TestimonyPeriodTimeEntityParam)
        {
            TestimonyPeriodTimeEntity o = GetTestimonyPeriodTimeFromTestimonyPeriodTimeDB(
                _TestimonyPeriodTimeDB.GetSingleTestimonyPeriodTimeDB(TestimonyPeriodTimeEntityParam));

            return o;
        }

        public List<TestimonyPeriodTimeEntity> GetAll()
        {
            List<TestimonyPeriodTimeEntity> lst = new List<TestimonyPeriodTimeEntity>();
            //string key = "TestimonyPeriodTime_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<TestimonyPeriodTimeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetTestimonyPeriodTimeCollectionFromTestimonyPeriodTimeDBList(
                _TestimonyPeriodTimeDB.GetAllTestimonyPeriodTimeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<TestimonyPeriodTimeEntity> GetAllIsActive()
        {
            List<TestimonyPeriodTimeEntity> lst = new List<TestimonyPeriodTimeEntity>();
            //string key = "TestimonyPeriodTime_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<TestimonyPeriodTimeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetTestimonyPeriodTimeCollectionFromTestimonyPeriodTimeDBList(
                _TestimonyPeriodTimeDB.GetAllIsActiveTestimonyPeriodTimeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }


        public List<TestimonyPeriodTimeEntity> GetAllPaging(int currentPage, int pageSize,string sortExpression, out int count, string whereClause)
        {
            //string key = "TestimonyPeriodTime_List_Page_" + currentPage ;
            //string countKey = "TestimonyPeriodTime_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<TestimonyPeriodTimeEntity> lst = new List<TestimonyPeriodTimeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<TestimonyPeriodTimeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetTestimonyPeriodTimeCollectionFromTestimonyPeriodTimeDBList(
                _TestimonyPeriodTimeDB.GetPageTestimonyPeriodTimeDB(pageSize, currentPage,
                                                                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private TestimonyPeriodTimeEntity GetTestimonyPeriodTimeFromTestimonyPeriodTimeDB(TestimonyPeriodTimeEntity o)
        {
            if (o == null)
                return null;
            TestimonyPeriodTimeEntity ret = new TestimonyPeriodTimeEntity(o.TestimonyPeriodTimeId, o.TestimonyPeriodTimeTitle, o.Periority, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<TestimonyPeriodTimeEntity> GetTestimonyPeriodTimeCollectionFromTestimonyPeriodTimeDBList(List<TestimonyPeriodTimeEntity> lst)
        {
            List<TestimonyPeriodTimeEntity> RetLst = new List<TestimonyPeriodTimeEntity>();
            foreach (TestimonyPeriodTimeEntity o in lst)
            {
                RetLst.Add(GetTestimonyPeriodTimeFromTestimonyPeriodTimeDB(o));
            }
            return RetLst;

        }


    }
}