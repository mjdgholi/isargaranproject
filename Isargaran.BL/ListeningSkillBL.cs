﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <مهارت شنیداری>
    /// </summary>

    public class ListeningSkillBL
    {
        private readonly ListeningSkillDB _ListeningSkillDB;

        public ListeningSkillBL()
        {
            _ListeningSkillDB = new ListeningSkillDB();
        }

        public void Add(ListeningSkillEntity ListeningSkillEntityParam, out Guid ListeningSkillId)
        {
            _ListeningSkillDB.AddListeningSkillDB(ListeningSkillEntityParam, out ListeningSkillId);
        }

        public void Update(ListeningSkillEntity ListeningSkillEntityParam)
        {
            _ListeningSkillDB.UpdateListeningSkillDB(ListeningSkillEntityParam);
        }

        public void Delete(ListeningSkillEntity ListeningSkillEntityParam)
        {
            _ListeningSkillDB.DeleteListeningSkillDB(ListeningSkillEntityParam);
        }

        public ListeningSkillEntity GetSingleById(ListeningSkillEntity ListeningSkillEntityParam)
        {
            ListeningSkillEntity o = GetListeningSkillFromListeningSkillDB(
                _ListeningSkillDB.GetSingleListeningSkillDB(ListeningSkillEntityParam));

            return o;
        }

        public List<ListeningSkillEntity> GetAll()
        {
            List<ListeningSkillEntity> lst = new List<ListeningSkillEntity>();
            //string key = "ListeningSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ListeningSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetListeningSkillCollectionFromListeningSkillDBList(
                _ListeningSkillDB.GetAllListeningSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ListeningSkillEntity> GetAllIsActive()
        {
            List<ListeningSkillEntity> lst = new List<ListeningSkillEntity>();
            //string key = "ListeningSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ListeningSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetListeningSkillCollectionFromListeningSkillDBList(
                _ListeningSkillDB.GetAllIsActiveListeningSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ListeningSkillEntity> GetAllPaging(int currentPage, int pageSize,
                                                       string

                                                           sortExpression, out int count, string whereClause)
        {
            //string key = "ListeningSkill_List_Page_" + currentPage ;
            //string countKey = "ListeningSkill_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ListeningSkillEntity> lst = new List<ListeningSkillEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ListeningSkillEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetListeningSkillCollectionFromListeningSkillDBList(
                _ListeningSkillDB.GetPageListeningSkillDB(pageSize, currentPage,
                                                          whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ListeningSkillEntity GetListeningSkillFromListeningSkillDB(ListeningSkillEntity o)
        {
            if (o == null)
                return null;
            ListeningSkillEntity ret = new ListeningSkillEntity(o.ListeningSkillId, o.ListeningSkillPersianTitle, o.ListeningSkillEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<ListeningSkillEntity> GetListeningSkillCollectionFromListeningSkillDBList(List<ListeningSkillEntity> lst)
        {
            List<ListeningSkillEntity> RetLst = new List<ListeningSkillEntity>();
            foreach (ListeningSkillEntity o in lst)
            {
                RetLst.Add(GetListeningSkillFromListeningSkillDB(o));
            }
            return RetLst;

        }


    }
}