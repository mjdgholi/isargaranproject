﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes.ItcException;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات  عناوین قهرمانی علمی و فرهنگی>
    /// </summary>


    public class DossierSacrifice_ScientificAndCulturalChampionshipBL
    {
        private readonly DossierSacrifice_ScientificAndCulturalChampionshipDB
            _DossierSacrifice_ScientificAndCulturalChampionshipDB;

        public DossierSacrifice_ScientificAndCulturalChampionshipBL()
        {
            _DossierSacrifice_ScientificAndCulturalChampionshipDB =
                new DossierSacrifice_ScientificAndCulturalChampionshipDB();
        }

        public void Add(
            DossierSacrifice_ScientificAndCulturalChampionshipEntity
                DossierSacrifice_ScientificAndCulturalChampionshipEntityParam,
            out Guid DossierSacrifice_ScientificAndCulturalChampionshipId)
        {
            //if (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PersonId == null)
            //{
            //    throw new ItcApplicationErrorManagerException("نام و نام خانوادگی را وارد نمایید");
            //}
            _DossierSacrifice_ScientificAndCulturalChampionshipDB
                .AddDossierSacrifice_ScientificAndCulturalChampionshipDB(
                    DossierSacrifice_ScientificAndCulturalChampionshipEntityParam,
                    out DossierSacrifice_ScientificAndCulturalChampionshipId);
        }

        public void Update(
            DossierSacrifice_ScientificAndCulturalChampionshipEntity
                DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            //if (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam.PersonId == null)
            //{
            //    throw new ItcApplicationErrorManagerException("نام و نام خانوادگی را وارد نمایید");
            //}
            _DossierSacrifice_ScientificAndCulturalChampionshipDB
                .UpdateDossierSacrifice_ScientificAndCulturalChampionshipDB(
                    DossierSacrifice_ScientificAndCulturalChampionshipEntityParam);
        }

        public void Delete(
            DossierSacrifice_ScientificAndCulturalChampionshipEntity
                DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            _DossierSacrifice_ScientificAndCulturalChampionshipDB
                .DeleteDossierSacrifice_ScientificAndCulturalChampionshipDB(
                    DossierSacrifice_ScientificAndCulturalChampionshipEntityParam);
        }

        public DossierSacrifice_ScientificAndCulturalChampionshipEntity GetSingleById(
            DossierSacrifice_ScientificAndCulturalChampionshipEntity
                DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            DossierSacrifice_ScientificAndCulturalChampionshipEntity o = GetDossierSacrifice_ScientificAndCulturalChampionshipFromDossierSacrifice_ScientificAndCulturalChampionshipDB
                (
                    _DossierSacrifice_ScientificAndCulturalChampionshipDB
                        .GetSingleDossierSacrifice_ScientificAndCulturalChampionshipDB(
                            DossierSacrifice_ScientificAndCulturalChampionshipEntityParam));

            return o;
        }

        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetAll()
        {
            List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> lst =
                new List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>();
            //string key = "DossierSacrifice_ScientificAndCulturalChampionship_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ScientificAndCulturalChampionshipCollectionFromDossierSacrifice_ScientificAndCulturalChampionshipDBList
                (
                    _DossierSacrifice_ScientificAndCulturalChampionshipDB
                        .GetAllDossierSacrifice_ScientificAndCulturalChampionshipDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_ScientificAndCulturalChampionship_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_ScientificAndCulturalChampionship_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> lst =
                new List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ScientificAndCulturalChampionshipCollectionFromDossierSacrifice_ScientificAndCulturalChampionshipDBList
                (
                    _DossierSacrifice_ScientificAndCulturalChampionshipDB
                        .GetPageDossierSacrifice_ScientificAndCulturalChampionshipDB(pageSize, currentPage,
                            whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_ScientificAndCulturalChampionshipEntity
            GetDossierSacrifice_ScientificAndCulturalChampionshipFromDossierSacrifice_ScientificAndCulturalChampionshipDB
            (DossierSacrifice_ScientificAndCulturalChampionshipEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_ScientificAndCulturalChampionshipEntity ret =
                new DossierSacrifice_ScientificAndCulturalChampionshipEntity(
                    o.DossierSacrifice_ScientificAndCulturalChampionshipId, o.DossierSacrificeId,
                    o.ScientificAndCulturalFieldId, o.PointOrMedal, o.CompetitionDate, o.CompetitionTitle,
                    o.CompetitionLocation, o.PersonId, o.Description, o.CreationDate, o.ModificationDate,o.FullName);
            return ret;
        }

        private List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>
            GetDossierSacrifice_ScientificAndCulturalChampionshipCollectionFromDossierSacrifice_ScientificAndCulturalChampionshipDBList
            (List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> lst)
        {
            List<DossierSacrifice_ScientificAndCulturalChampionshipEntity> RetLst =
                new List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>();
            foreach (DossierSacrifice_ScientificAndCulturalChampionshipEntity o in lst)
            {
                RetLst.Add(
                    GetDossierSacrifice_ScientificAndCulturalChampionshipFromDossierSacrifice_ScientificAndCulturalChampionshipDB
                        (o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>
            GetDossierSacrifice_ScientificAndCulturalChampionshipCollectionByDossierSacrifice(
            DossierSacrifice_ScientificAndCulturalChampionshipEntity
                DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            return
                GetDossierSacrifice_ScientificAndCulturalChampionshipCollectionFromDossierSacrifice_ScientificAndCulturalChampionshipDBList
                    (_DossierSacrifice_ScientificAndCulturalChampionshipDB
                        .GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionByDossierSacrificeDB(
                            DossierSacrifice_ScientificAndCulturalChampionshipEntityParam));
        }

        public List<DossierSacrifice_ScientificAndCulturalChampionshipEntity>
            GetDossierSacrifice_ScientificAndCulturalChampionshipCollectionByScientificAndCulturalField(
            DossierSacrifice_ScientificAndCulturalChampionshipEntity
                DossierSacrifice_ScientificAndCulturalChampionshipEntityParam)
        {
            return
                GetDossierSacrifice_ScientificAndCulturalChampionshipCollectionFromDossierSacrifice_ScientificAndCulturalChampionshipDBList
                    (_DossierSacrifice_ScientificAndCulturalChampionshipDB
                        .GetDossierSacrifice_ScientificAndCulturalChampionshipDBCollectionByScientificAndCulturalFieldDB
                        (DossierSacrifice_ScientificAndCulturalChampionshipEntityParam));
        }

    }

}