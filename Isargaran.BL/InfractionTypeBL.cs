﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/03/12>
    // Description:	<نوع تخلف>
    /// </summary>

	public class InfractionTypeBL 
	{	
	  	 private readonly InfractionTypeDB _InfractionTypeDB;					
			
		public InfractionTypeBL()
		{
			_InfractionTypeDB = new InfractionTypeDB();
		}			
	
		public  void Add(InfractionTypeEntity  InfractionTypeEntityParam, out Guid InfractionTypeId)
		{ 
			_InfractionTypeDB.AddInfractionTypeDB(InfractionTypeEntityParam,out InfractionTypeId);			
		}

		public  void Update(InfractionTypeEntity  InfractionTypeEntityParam)
		{
			_InfractionTypeDB.UpdateInfractionTypeDB(InfractionTypeEntityParam);		
		}

		public  void Delete(InfractionTypeEntity  InfractionTypeEntityParam)
		{
			 _InfractionTypeDB.DeleteInfractionTypeDB(InfractionTypeEntityParam);			
		}

		public  InfractionTypeEntity GetSingleById(InfractionTypeEntity  InfractionTypeEntityParam)
		{
			InfractionTypeEntity o = GetInfractionTypeFromInfractionTypeDB(
			_InfractionTypeDB.GetSingleInfractionTypeDB(InfractionTypeEntityParam));
			
			return o;
		}

		public  List<InfractionTypeEntity> GetAll()
		{
			List<InfractionTypeEntity> lst = new List<InfractionTypeEntity>();
			//string key = "InfractionType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<InfractionTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetInfractionTypeCollectionFromInfractionTypeDBList(
				_InfractionTypeDB.GetAllInfractionTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<InfractionTypeEntity> GetAllIsActive()
        {
            List<InfractionTypeEntity> lst = new List<InfractionTypeEntity>();
            //string key = "InfractionType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<InfractionTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetInfractionTypeCollectionFromInfractionTypeDBList(
            _InfractionTypeDB.GetAllInfractionTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public  List<InfractionTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "InfractionType_List_Page_" + currentPage ;
			//string countKey = "InfractionType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<InfractionTypeEntity> lst = new List<InfractionTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<InfractionTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetInfractionTypeCollectionFromInfractionTypeDBList(
				_InfractionTypeDB.GetPageInfractionTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  InfractionTypeEntity GetInfractionTypeFromInfractionTypeDB(InfractionTypeEntity o)
		{
	if(o == null)
                return null;
			InfractionTypeEntity ret = new InfractionTypeEntity(o.InfractionTypeId ,o.InfractionTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<InfractionTypeEntity> GetInfractionTypeCollectionFromInfractionTypeDBList( List<InfractionTypeEntity> lst)
		{
			List<InfractionTypeEntity> RetLst = new List<InfractionTypeEntity>();
			foreach(InfractionTypeEntity o in lst)
			{
				RetLst.Add(GetInfractionTypeFromInfractionTypeDB(o));
			}
			return RetLst;
			
		} 
						
	}
}
