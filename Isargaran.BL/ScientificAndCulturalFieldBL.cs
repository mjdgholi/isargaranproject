﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/22>
    /// Description: <رشته علمی و فرهنگی>
    /// </summary>

	public class ScientificAndCulturalFieldBL 
	{	
	  	 private readonly ScientificAndCulturalFieldDB _ScientificAndCulturalFieldDB;					
			
		public ScientificAndCulturalFieldBL()
		{
			_ScientificAndCulturalFieldDB = new ScientificAndCulturalFieldDB();
		}			
	
		public  void Add(ScientificAndCulturalFieldEntity  ScientificAndCulturalFieldEntityParam, out Guid ScientificAndCulturalFieldId)
		{ 
			_ScientificAndCulturalFieldDB.AddScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntityParam,out ScientificAndCulturalFieldId);			
		}

		public  void Update(ScientificAndCulturalFieldEntity  ScientificAndCulturalFieldEntityParam)
		{
			_ScientificAndCulturalFieldDB.UpdateScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntityParam);		
		}

		public  void Delete(ScientificAndCulturalFieldEntity  ScientificAndCulturalFieldEntityParam)
		{
			 _ScientificAndCulturalFieldDB.DeleteScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntityParam);			
		}

		public  ScientificAndCulturalFieldEntity GetSingleById(ScientificAndCulturalFieldEntity  ScientificAndCulturalFieldEntityParam)
		{
			ScientificAndCulturalFieldEntity o = GetScientificAndCulturalFieldFromScientificAndCulturalFieldDB(
			_ScientificAndCulturalFieldDB.GetSingleScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntityParam));
			
			return o;
		}

		public  List<ScientificAndCulturalFieldEntity> GetAll()
		{
			List<ScientificAndCulturalFieldEntity> lst = new List<ScientificAndCulturalFieldEntity>();
			//string key = "ScientificAndCulturalField_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ScientificAndCulturalFieldEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetScientificAndCulturalFieldCollectionFromScientificAndCulturalFieldDBList(
				_ScientificAndCulturalFieldDB.GetAllScientificAndCulturalFieldDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<ScientificAndCulturalFieldEntity> GetAllIsActive()
        {
            List<ScientificAndCulturalFieldEntity> lst = new List<ScientificAndCulturalFieldEntity>();
            //string key = "ScientificAndCulturalField_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ScientificAndCulturalFieldEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetScientificAndCulturalFieldCollectionFromScientificAndCulturalFieldDBList(
            _ScientificAndCulturalFieldDB.GetAllScientificAndCulturalFieldIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<ScientificAndCulturalFieldEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ScientificAndCulturalField_List_Page_" + currentPage ;
			//string countKey = "ScientificAndCulturalField_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ScientificAndCulturalFieldEntity> lst = new List<ScientificAndCulturalFieldEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ScientificAndCulturalFieldEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetScientificAndCulturalFieldCollectionFromScientificAndCulturalFieldDBList(
				_ScientificAndCulturalFieldDB.GetPageScientificAndCulturalFieldDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ScientificAndCulturalFieldEntity GetScientificAndCulturalFieldFromScientificAndCulturalFieldDB(ScientificAndCulturalFieldEntity o)
		{
	if(o == null)
                return null;
			ScientificAndCulturalFieldEntity ret = new ScientificAndCulturalFieldEntity(o.ScientificAndCulturalFieldId ,o.ScientificAndCulturalFieldPersianTitle ,o.ScientificAndCulturalFieldEnglishTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ScientificAndCulturalFieldEntity> GetScientificAndCulturalFieldCollectionFromScientificAndCulturalFieldDBList( List<ScientificAndCulturalFieldEntity> lst)
		{
			List<ScientificAndCulturalFieldEntity> RetLst = new List<ScientificAndCulturalFieldEntity>();
			foreach(ScientificAndCulturalFieldEntity o in lst)
			{
				RetLst.Add(GetScientificAndCulturalFieldFromScientificAndCulturalFieldDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}


