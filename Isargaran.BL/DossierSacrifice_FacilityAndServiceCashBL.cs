﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/21>
    // Description:	<اطلاعات  اطلاعات خدمات و تسهیلات نقدی>
    /// </summary>


    public class DossierSacrifice_FacilityAndServiceCashBL
    {
        private readonly DossierSacrifice_FacilityAndServiceCashDB _DossierSacrifice_FacilityAndServiceCashDB;

        public DossierSacrifice_FacilityAndServiceCashBL()
        {
            _DossierSacrifice_FacilityAndServiceCashDB = new DossierSacrifice_FacilityAndServiceCashDB();
        }

        public void Add(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam,
            out Guid DossierSacrifice_FacilityAndServiceCashId)
        {
            _DossierSacrifice_FacilityAndServiceCashDB.AddDossierSacrifice_FacilityAndServiceCashDB(
                DossierSacrifice_FacilityAndServiceCashEntityParam, out DossierSacrifice_FacilityAndServiceCashId);
        }

        public void Update(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            _DossierSacrifice_FacilityAndServiceCashDB.UpdateDossierSacrifice_FacilityAndServiceCashDB(
                DossierSacrifice_FacilityAndServiceCashEntityParam);
        }

        public void Delete(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            _DossierSacrifice_FacilityAndServiceCashDB.DeleteDossierSacrifice_FacilityAndServiceCashDB(
                DossierSacrifice_FacilityAndServiceCashEntityParam);
        }

        public DossierSacrifice_FacilityAndServiceCashEntity GetSingleById(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            DossierSacrifice_FacilityAndServiceCashEntity o = GetDossierSacrifice_FacilityAndServiceCashFromDossierSacrifice_FacilityAndServiceCashDB
                (
                    _DossierSacrifice_FacilityAndServiceCashDB.GetSingleDossierSacrifice_FacilityAndServiceCashDB(
                        DossierSacrifice_FacilityAndServiceCashEntityParam));

            return o;
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity> GetAll()
        {
            List<DossierSacrifice_FacilityAndServiceCashEntity> lst =
                new List<DossierSacrifice_FacilityAndServiceCashEntity>();
            //string key = "DossierSacrifice_FacilityAndServiceCash_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilityAndServiceCashEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilityAndServiceCashCollectionFromDossierSacrifice_FacilityAndServiceCashDBList(
                _DossierSacrifice_FacilityAndServiceCashDB.GetAllDossierSacrifice_FacilityAndServiceCashDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_FacilityAndServiceCash_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_FacilityAndServiceCash_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_FacilityAndServiceCashEntity> lst =
                new List<DossierSacrifice_FacilityAndServiceCashEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilityAndServiceCashEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilityAndServiceCashCollectionFromDossierSacrifice_FacilityAndServiceCashDBList(
                _DossierSacrifice_FacilityAndServiceCashDB.GetPageDossierSacrifice_FacilityAndServiceCashDB(pageSize,
                    currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_FacilityAndServiceCashEntity
            GetDossierSacrifice_FacilityAndServiceCashFromDossierSacrifice_FacilityAndServiceCashDB(
            DossierSacrifice_FacilityAndServiceCashEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_FacilityAndServiceCashEntity ret =
                new DossierSacrifice_FacilityAndServiceCashEntity(o.DossierSacrifice_FacilityAndServiceCashId,
                    o.DossierSacrificeId, o.FacilityAndServiceCashId, o.FacilityAndServiceCashDate, o.OccasionId,
                    o.CashAmount, o.RecipientPersonName, o.RecipientPersonNationalNo, o.Description, o.CreationDate,
                    o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashCollectionFromDossierSacrifice_FacilityAndServiceCashDBList(
            List<DossierSacrifice_FacilityAndServiceCashEntity> lst)
        {
            List<DossierSacrifice_FacilityAndServiceCashEntity> RetLst =
                new List<DossierSacrifice_FacilityAndServiceCashEntity>();
            foreach (DossierSacrifice_FacilityAndServiceCashEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_FacilityAndServiceCashFromDossierSacrifice_FacilityAndServiceCashDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashCollectionByDossierSacrifice(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            return
                GetDossierSacrifice_FacilityAndServiceCashCollectionFromDossierSacrifice_FacilityAndServiceCashDBList(
                    _DossierSacrifice_FacilityAndServiceCashDB
                        .GetDossierSacrifice_FacilityAndServiceCashDBCollectionByDossierSacrificeDB(
                            DossierSacrifice_FacilityAndServiceCashEntityParam));
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashCollectionByFacilityAndServiceCash(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            return
                GetDossierSacrifice_FacilityAndServiceCashCollectionFromDossierSacrifice_FacilityAndServiceCashDBList(
                    _DossierSacrifice_FacilityAndServiceCashDB
                        .GetDossierSacrifice_FacilityAndServiceCashDBCollectionByFacilityAndServiceCashDB(
                            DossierSacrifice_FacilityAndServiceCashEntityParam));
        }

        public List<DossierSacrifice_FacilityAndServiceCashEntity>
            GetDossierSacrifice_FacilityAndServiceCashCollectionByOccasion(
            DossierSacrifice_FacilityAndServiceCashEntity DossierSacrifice_FacilityAndServiceCashEntityParam)
        {
            return
                GetDossierSacrifice_FacilityAndServiceCashCollectionFromDossierSacrifice_FacilityAndServiceCashDBList(
                    _DossierSacrifice_FacilityAndServiceCashDB
                        .GetDossierSacrifice_FacilityAndServiceCashDBCollectionByOccasionDB(
                            DossierSacrifice_FacilityAndServiceCashEntityParam));
        }


    }

}
