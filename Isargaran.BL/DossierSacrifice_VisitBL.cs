﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/02/15>
    // Description:	<اطلاعات ملاقات>
    /// </summary>
	public class DossierSacrifice_VisitBL 
	{	
	  	 private readonly DossierSacrifice_VisitDB _DossierSacrifice_VisitDB;					
			
		public DossierSacrifice_VisitBL()
		{
			_DossierSacrifice_VisitDB = new DossierSacrifice_VisitDB();
		}			
	
		public  void Add(DossierSacrifice_VisitEntity  DossierSacrifice_VisitEntityParam, out Guid DossierSacrifice_VisitId)
		{ 
            if(DossierSacrifice_VisitEntityParam.HasAction && DossierSacrifice_VisitEntityParam.ActionDescription=="")
            {
                throw new ItcApplicationErrorManagerException("شرح اقدام را وارد نمایید");
            }
            if (DossierSacrifice_VisitEntityParam.HasRequest && DossierSacrifice_VisitEntityParam.RequestContent == "")
            {
                throw new ItcApplicationErrorManagerException("متن درخواست  را وارد نمایید");
            }

			_DossierSacrifice_VisitDB.AddDossierSacrifice_VisitDB(DossierSacrifice_VisitEntityParam,out DossierSacrifice_VisitId);			
		}

		public  void Update(DossierSacrifice_VisitEntity  DossierSacrifice_VisitEntityParam)
		{
            if (DossierSacrifice_VisitEntityParam.HasAction && DossierSacrifice_VisitEntityParam.ActionDescription == "")
            {
                throw new ItcApplicationErrorManagerException("شرح اقدام را وارد نمایید");
            }
            if (DossierSacrifice_VisitEntityParam.HasRequest && DossierSacrifice_VisitEntityParam.RequestContent == "")
            {
                throw new ItcApplicationErrorManagerException("متن درخواست  را وارد نمایید");
            }
			_DossierSacrifice_VisitDB.UpdateDossierSacrifice_VisitDB(DossierSacrifice_VisitEntityParam);		
		}

		public  void Delete(DossierSacrifice_VisitEntity  DossierSacrifice_VisitEntityParam)
		{
			 _DossierSacrifice_VisitDB.DeleteDossierSacrifice_VisitDB(DossierSacrifice_VisitEntityParam);			
		}

		public  DossierSacrifice_VisitEntity GetSingleById(DossierSacrifice_VisitEntity  DossierSacrifice_VisitEntityParam)
		{
			DossierSacrifice_VisitEntity o = GetDossierSacrifice_VisitFromDossierSacrifice_VisitDB(
			_DossierSacrifice_VisitDB.GetSingleDossierSacrifice_VisitDB(DossierSacrifice_VisitEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_VisitEntity> GetAll()
		{
			List<DossierSacrifice_VisitEntity> lst = new List<DossierSacrifice_VisitEntity>();
			//string key = "DossierSacrifice_Visit_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_VisitEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_VisitCollectionFromDossierSacrifice_VisitDBList(
				_DossierSacrifice_VisitDB.GetAllDossierSacrifice_VisitDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_VisitEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_Visit_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_Visit_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_VisitEntity> lst = new List<DossierSacrifice_VisitEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_VisitEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_VisitCollectionFromDossierSacrifice_VisitDBList(
				_DossierSacrifice_VisitDB.GetPageDossierSacrifice_VisitDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_VisitEntity GetDossierSacrifice_VisitFromDossierSacrifice_VisitDB(DossierSacrifice_VisitEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_VisitEntity ret = new DossierSacrifice_VisitEntity(o.DossierSacrifice_VisitId ,o.DossierSacrificeId ,o.VisitDate ,o.OccasionId ,o.VisitLocation ,o.Visitors ,o.HasRequest ,o.RequestContent ,o.HasAction ,o.ActionDescription ,o.Description ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<DossierSacrifice_VisitEntity> GetDossierSacrifice_VisitCollectionFromDossierSacrifice_VisitDBList( List<DossierSacrifice_VisitEntity> lst)
		{
			List<DossierSacrifice_VisitEntity> RetLst = new List<DossierSacrifice_VisitEntity>();
			foreach(DossierSacrifice_VisitEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_VisitFromDossierSacrifice_VisitDB(o));
			}
			return RetLst;
			
		}



        public List<DossierSacrifice_VisitEntity> GetDossierSacrifice_VisitCollectionByDossierSacrifice(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam)
{
    return GetDossierSacrifice_VisitCollectionFromDossierSacrifice_VisitDBList(_DossierSacrifice_VisitDB.GetDossierSacrifice_VisitDBCollectionByDossierSacrificeDB(DossierSacrifice_VisitEntityParam));
}

        public List<DossierSacrifice_VisitEntity> GetDossierSacrifice_VisitCollectionByOccasion(DossierSacrifice_VisitEntity DossierSacrifice_VisitEntityParam)
{
    return GetDossierSacrifice_VisitCollectionFromDossierSacrifice_VisitDBList(_DossierSacrifice_VisitDB.GetDossierSacrifice_VisitDBCollectionByOccasionDB(DossierSacrifice_VisitEntityParam));
}




}

}
