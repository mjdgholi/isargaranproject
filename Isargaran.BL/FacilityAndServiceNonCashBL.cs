﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/22>
    /// Description: <خدمات و تسهیلات  غیرنقدی>
    /// </summary>


	public class FacilityAndServiceNonCashBL 
	{	
	  	 private readonly FacilityAndServiceNonCashDB _FacilityAndServiceNonCashDB;					
			
		public FacilityAndServiceNonCashBL()
		{
			_FacilityAndServiceNonCashDB = new FacilityAndServiceNonCashDB();
		}			
	
		public  void Add(FacilityAndServiceNonCashEntity  FacilityAndServiceNonCashEntityParam, out Guid FacilityAndServiceNonCashId)
		{ 
			_FacilityAndServiceNonCashDB.AddFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntityParam,out FacilityAndServiceNonCashId);			
		}

		public  void Update(FacilityAndServiceNonCashEntity  FacilityAndServiceNonCashEntityParam)
		{
			_FacilityAndServiceNonCashDB.UpdateFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntityParam);		
		}

		public  void Delete(FacilityAndServiceNonCashEntity  FacilityAndServiceNonCashEntityParam)
		{
			 _FacilityAndServiceNonCashDB.DeleteFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntityParam);			
		}

		public  FacilityAndServiceNonCashEntity GetSingleById(FacilityAndServiceNonCashEntity  FacilityAndServiceNonCashEntityParam)
		{
			FacilityAndServiceNonCashEntity o = GetFacilityAndServiceNonCashFromFacilityAndServiceNonCashDB(
			_FacilityAndServiceNonCashDB.GetSingleFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntityParam));
			
			return o;
		}

		public  List<FacilityAndServiceNonCashEntity> GetAll()
		{
			List<FacilityAndServiceNonCashEntity> lst = new List<FacilityAndServiceNonCashEntity>();
			//string key = "FacilityAndServiceNonCash_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<FacilityAndServiceNonCashEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetFacilityAndServiceNonCashCollectionFromFacilityAndServiceNonCashDBList(
				_FacilityAndServiceNonCashDB.GetAllFacilityAndServiceNonCashDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<FacilityAndServiceNonCashEntity> GetAllIsActive()
        {
            List<FacilityAndServiceNonCashEntity> lst = new List<FacilityAndServiceNonCashEntity>();
            //string key = "FacilityAndServiceNonCash_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<FacilityAndServiceNonCashEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetFacilityAndServiceNonCashCollectionFromFacilityAndServiceNonCashDBList(
            _FacilityAndServiceNonCashDB.GetAllFacilityAndServiceNonCashIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<FacilityAndServiceNonCashEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "FacilityAndServiceNonCash_List_Page_" + currentPage ;
			//string countKey = "FacilityAndServiceNonCash_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<FacilityAndServiceNonCashEntity> lst = new List<FacilityAndServiceNonCashEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<FacilityAndServiceNonCashEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetFacilityAndServiceNonCashCollectionFromFacilityAndServiceNonCashDBList(
				_FacilityAndServiceNonCashDB.GetPageFacilityAndServiceNonCashDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  FacilityAndServiceNonCashEntity GetFacilityAndServiceNonCashFromFacilityAndServiceNonCashDB(FacilityAndServiceNonCashEntity o)
		{
	if(o == null)
                return null;
			FacilityAndServiceNonCashEntity ret = new FacilityAndServiceNonCashEntity(o.FacilityAndServiceNonCashId ,o.FacilityAndServiceNonCashTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<FacilityAndServiceNonCashEntity> GetFacilityAndServiceNonCashCollectionFromFacilityAndServiceNonCashDBList( List<FacilityAndServiceNonCashEntity> lst)
		{
			List<FacilityAndServiceNonCashEntity> RetLst = new List<FacilityAndServiceNonCashEntity>();
			foreach(FacilityAndServiceNonCashEntity o in lst)
			{
				RetLst.Add(GetFacilityAndServiceNonCashFromFacilityAndServiceNonCashDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}

