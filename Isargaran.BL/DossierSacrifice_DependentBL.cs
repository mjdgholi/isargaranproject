﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/17>
    /// Description: <پرونده مشخصات وابستگان ایثارگری>
    /// </summary> 


	public class DossierSacrifice_DependentBL 
	{	
	  	 private readonly DossierSacrifice_DependentDB _DossierSacrifice_DependentDB;					
			
		public DossierSacrifice_DependentBL()
		{
			_DossierSacrifice_DependentDB = new DossierSacrifice_DependentDB();
		}			
	
		public  void Add(DossierSacrifice_DependentEntity  DossierSacrifice_DependentEntityParam, out Guid DossierSacrifice_DependentId)
		{ 
			_DossierSacrifice_DependentDB.AddDossierSacrifice_DependentDB(DossierSacrifice_DependentEntityParam,out DossierSacrifice_DependentId);			
		}

		public  void Update(DossierSacrifice_DependentEntity  DossierSacrifice_DependentEntityParam)
		{
			_DossierSacrifice_DependentDB.UpdateDossierSacrifice_DependentDB(DossierSacrifice_DependentEntityParam);		
		}

		public  void Delete(DossierSacrifice_DependentEntity  DossierSacrifice_DependentEntityParam)
		{
			 _DossierSacrifice_DependentDB.DeleteDossierSacrifice_DependentDB(DossierSacrifice_DependentEntityParam);			
		}

		public  DossierSacrifice_DependentEntity GetSingleById(DossierSacrifice_DependentEntity  DossierSacrifice_DependentEntityParam)
		{
			DossierSacrifice_DependentEntity o = GetDossierSacrifice_DependentFromDossierSacrifice_DependentDB(
			_DossierSacrifice_DependentDB.GetSingleDossierSacrifice_DependentDB(DossierSacrifice_DependentEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_DependentEntity> GetAll()
		{
			List<DossierSacrifice_DependentEntity> lst = new List<DossierSacrifice_DependentEntity>();
			//string key = "DossierSacrifice_Dependent_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_DependentEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_DependentCollectionFromDossierSacrifice_DependentDBList(
				_DossierSacrifice_DependentDB.GetAllDossierSacrifice_DependentDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_DependentEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_Dependent_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_Dependent_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_DependentEntity> lst = new List<DossierSacrifice_DependentEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_DependentEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_DependentCollectionFromDossierSacrifice_DependentDBList(
				_DossierSacrifice_DependentDB.GetPageDossierSacrifice_DependentDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_DependentEntity GetDossierSacrifice_DependentFromDossierSacrifice_DependentDB(DossierSacrifice_DependentEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_DependentEntity ret = new DossierSacrifice_DependentEntity(o.DossierSacrifice_DependentId ,o.DossierSacrificeId ,o.PersonId ,o.DependentTypeId ,o.FromDate ,o.Todate,o.CreationDate,o.ModificationDate,o.FullName );
			return ret;
		}
		
		private  List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentCollectionFromDossierSacrifice_DependentDBList( List<DossierSacrifice_DependentEntity> lst)
		{
			List<DossierSacrifice_DependentEntity> RetLst = new List<DossierSacrifice_DependentEntity>();
			foreach(DossierSacrifice_DependentEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_DependentFromDossierSacrifice_DependentDB(o));
			}
			return RetLst;
			
		}



        public  List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentCollectionByDependentType(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam)
{
    return GetDossierSacrifice_DependentCollectionFromDossierSacrifice_DependentDBList(_DossierSacrifice_DependentDB.GetDossierSacrifice_DependentDBCollectionByDependentTypeDB(dossierSacrificeDependentEntityParam));
}

        public  List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentCollectionByPerson(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam)
{
    return GetDossierSacrifice_DependentCollectionFromDossierSacrifice_DependentDBList(_DossierSacrifice_DependentDB.GetDossierSacrifice_DependentDBCollectionByPersonDB(dossierSacrificeDependentEntityParam));
}

        public  List<DossierSacrifice_DependentEntity> GetDossierSacrifice_DependentCollectionByDossierSacrifice(DossierSacrifice_DependentEntity dossierSacrificeDependentEntityParam)
{
    return GetDossierSacrifice_DependentCollectionFromDossierSacrifice_DependentDBList(_DossierSacrifice_DependentDB.GetDossierSacrifice_DependentDBCollectionByDossierSacrificeDB(dossierSacrificeDependentEntityParam));
}


        public void CloseTodossierSacrificeDependent(DossierSacrifice_DependentEntity dossierSacrificeDependentEntity)
        {
            _DossierSacrifice_DependentDB.CloseTodossierSacrificeDependentDB(dossierSacrificeDependentEntity);	
        }
	}

}
