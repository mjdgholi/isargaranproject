﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: < نوع مراکز تحصیلی>
    /// </summary>


	public class EducationCenterTypeBL 
	{	
	  	 private readonly EducationCenterTypeDB _EducationCenterTypeDB;					
			
		public EducationCenterTypeBL()
		{
			_EducationCenterTypeDB = new EducationCenterTypeDB();
		}			
	
		public  void Add(EducationCenterTypeEntity  EducationCenterTypeEntityParam, out Guid EducationCenterTypeId)
		{ 
			_EducationCenterTypeDB.AddEducationCenterTypeDB(EducationCenterTypeEntityParam,out EducationCenterTypeId);			
		}

		public  void Update(EducationCenterTypeEntity  EducationCenterTypeEntityParam)
		{
			_EducationCenterTypeDB.UpdateEducationCenterTypeDB(EducationCenterTypeEntityParam);		
		}

		public  void Delete(EducationCenterTypeEntity  EducationCenterTypeEntityParam)
		{
			 _EducationCenterTypeDB.DeleteEducationCenterTypeDB(EducationCenterTypeEntityParam);			
		}

		public  EducationCenterTypeEntity GetSingleById(EducationCenterTypeEntity  EducationCenterTypeEntityParam)
		{
			EducationCenterTypeEntity o = GetEducationCenterTypeFromEducationCenterTypeDB(
			_EducationCenterTypeDB.GetSingleEducationCenterTypeDB(EducationCenterTypeEntityParam));
			
			return o;
		}

		public  List<EducationCenterTypeEntity> GetAll()
		{
			List<EducationCenterTypeEntity> lst = new List<EducationCenterTypeEntity>();
			//string key = "EducationCenterType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationCenterTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetEducationCenterTypeCollectionFromEducationCenterTypeDBList(
				_EducationCenterTypeDB.GetAllEducationCenterTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<EducationCenterTypeEntity> GetAllIsActive()
        {
            List<EducationCenterTypeEntity> lst = new List<EducationCenterTypeEntity>();
            //string key = "EducationCenterType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCenterTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationCenterTypeCollectionFromEducationCenterTypeDBList(
            _EducationCenterTypeDB.GetAllEducationCenterTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<EducationCenterTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "EducationCenterType_List_Page_" + currentPage ;
			//string countKey = "EducationCenterType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<EducationCenterTypeEntity> lst = new List<EducationCenterTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationCenterTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetEducationCenterTypeCollectionFromEducationCenterTypeDBList(
				_EducationCenterTypeDB.GetPageEducationCenterTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  EducationCenterTypeEntity GetEducationCenterTypeFromEducationCenterTypeDB(EducationCenterTypeEntity o)
		{
	if(o == null)
                return null;
			EducationCenterTypeEntity ret = new EducationCenterTypeEntity(o.EducationCenterTypeId ,o.EducationCenterTypePersianTitle ,o.EducationCenterTypeEnglishTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<EducationCenterTypeEntity> GetEducationCenterTypeCollectionFromEducationCenterTypeDBList( List<EducationCenterTypeEntity> lst)
		{
			List<EducationCenterTypeEntity> RetLst = new List<EducationCenterTypeEntity>();
			foreach(EducationCenterTypeEntity o in lst)
			{
				RetLst.Add(GetEducationCenterTypeFromEducationCenterTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}

