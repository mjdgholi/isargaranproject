﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/18>
    /// Description: <پرونده مشخصات محل اقامت ایثارگری>
    /// </summary>

    public class DossierSacrifice_HabitatBL
    {
        private readonly DossierSacrifice_HabitatDB _DossierSacrifice_HabitatDB;

        public DossierSacrifice_HabitatBL()
        {
            _DossierSacrifice_HabitatDB = new DossierSacrifice_HabitatDB();
        }

        public void Add(DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam,
                        out Guid DossierSacrifice_HabitatId)
        {
            _DossierSacrifice_HabitatDB.AddDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntityParam,
                                                                      out DossierSacrifice_HabitatId);
        }

        public void Update(DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam)
        {
            _DossierSacrifice_HabitatDB.UpdateDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntityParam);
        }

        public void Delete(DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam)
        {
            _DossierSacrifice_HabitatDB.DeleteDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntityParam);
        }

        public DossierSacrifice_HabitatEntity GetSingleById(
            DossierSacrifice_HabitatEntity DossierSacrifice_HabitatEntityParam)
        {
            DossierSacrifice_HabitatEntity o = GetDossierSacrifice_HabitatFromDossierSacrifice_HabitatDB(
                _DossierSacrifice_HabitatDB.GetSingleDossierSacrifice_HabitatDB(DossierSacrifice_HabitatEntityParam));

            return o;
        }

        public List<DossierSacrifice_HabitatEntity> GetAll()
        {
            List<DossierSacrifice_HabitatEntity> lst = new List<DossierSacrifice_HabitatEntity>();
            //string key = "DossierSacrifice_Habitat_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_HabitatEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
                _DossierSacrifice_HabitatDB.GetAllDossierSacrifice_HabitatDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_HabitatEntity> GetAllPaging(int currentPage, int pageSize,
                                                                 string

                                                                     sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Habitat_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Habitat_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_HabitatEntity> lst = new List<DossierSacrifice_HabitatEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_HabitatEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
                _DossierSacrifice_HabitatDB.GetPageDossierSacrifice_HabitatDB(pageSize, currentPage,
                                                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_HabitatEntity GetDossierSacrifice_HabitatFromDossierSacrifice_HabitatDB(
            DossierSacrifice_HabitatEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_HabitatEntity ret = new DossierSacrifice_HabitatEntity(o.DossierSacrifice_HabitatId,
                                                                                    o.DossierSacrificeId,
                                                                                    o.HabitatOwnershipStatusId,
                                                                                    o.HabitatEStateTypeId,
                                                                                    o.HabitatLocationTypeId,
                                                                                    o.HabitatQualityId,
                                                                                    o.IsBasicRepairNeed, o.AmountOfRent,
                                                                                    o.HbitatArea, o.HabitatDeposit,
                                                                                    o.StartDate, o.EndDate,
                                                                                    o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_HabitatEntity>
            GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
            List<DossierSacrifice_HabitatEntity> lst)
        {
            List<DossierSacrifice_HabitatEntity> RetLst = new List<DossierSacrifice_HabitatEntity>();
            foreach (DossierSacrifice_HabitatEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_HabitatFromDossierSacrifice_HabitatDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatCollectionByDossierSacrifice(
            int DossierSacrificeId)
        {
            return
                GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
                    _DossierSacrifice_HabitatDB.GetDossierSacrifice_HabitatDBCollectionByDossierSacrificeDB(
                        DossierSacrificeId));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatCollectionByHabitatEstateType(
            int HabitatEStateTypeId)
        {
            return
                GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
                    _DossierSacrifice_HabitatDB.GetDossierSacrifice_HabitatDBCollectionByHabitatEstateTypeDB(
                        HabitatEStateTypeId));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatCollectionByHabitatLocationType(
            int HabitatLocationTypeId)
        {
            return
                GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
                    _DossierSacrifice_HabitatDB.GetDossierSacrifice_HabitatDBCollectionByHabitatLocationTypeDB(
                        HabitatLocationTypeId));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatCollectionByHabitatOwnershipStatus(
            int HabitatOwnershipStatusId)
        {
            return
                GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
                    _DossierSacrifice_HabitatDB.GetDossierSacrifice_HabitatDBCollectionByHabitatOwnershipStatusDB(
                        HabitatOwnershipStatusId));
        }

        public List<DossierSacrifice_HabitatEntity> GetDossierSacrifice_HabitatCollectionByHabitatQuality(
            int HabitatQualityId)
        {
            return
                GetDossierSacrifice_HabitatCollectionFromDossierSacrifice_HabitatDBList(
                    _DossierSacrifice_HabitatDB.GetDossierSacrifice_HabitatDBCollectionByHabitatQualityDB(
                        HabitatQualityId));
        }



    }
}

    

