﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <حوزه بسیجی>
    /// </summary>

	public class DistrictBasijiBL 
	{	
	  	 private readonly DistrictBasijiDB _DistrictBasijiDB;					
			
		public DistrictBasijiBL()
		{
			_DistrictBasijiDB = new DistrictBasijiDB();
		}			
	
		public  void Add(DistrictBasijiEntity  DistrictBasijiEntityParam, out Guid DistrictBasijiId)
		{ 
			_DistrictBasijiDB.AddDistrictBasijiDB(DistrictBasijiEntityParam,out DistrictBasijiId);			
		}

		public  void Update(DistrictBasijiEntity  DistrictBasijiEntityParam)
		{
			_DistrictBasijiDB.UpdateDistrictBasijiDB(DistrictBasijiEntityParam);		
		}

		public  void Delete(DistrictBasijiEntity  DistrictBasijiEntityParam)
		{
			 _DistrictBasijiDB.DeleteDistrictBasijiDB(DistrictBasijiEntityParam);			
		}

		public  DistrictBasijiEntity GetSingleById(DistrictBasijiEntity  DistrictBasijiEntityParam)
		{
			DistrictBasijiEntity o = GetDistrictBasijiFromDistrictBasijiDB(
			_DistrictBasijiDB.GetSingleDistrictBasijiDB(DistrictBasijiEntityParam));
			
			return o;
		}

		public  List<DistrictBasijiEntity> GetAll()
		{
			List<DistrictBasijiEntity> lst = new List<DistrictBasijiEntity>();
			//string key = "DistrictBasiji_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DistrictBasijiEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDistrictBasijiCollectionFromDistrictBasijiDBList(
				_DistrictBasijiDB.GetAllDistrictBasijiDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<DistrictBasijiEntity> GetAllIsActive()
        {
            List<DistrictBasijiEntity> lst = new List<DistrictBasijiEntity>();
            //string key = "DistrictBasiji_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DistrictBasijiEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDistrictBasijiCollectionFromDistrictBasijiDBList(
            _DistrictBasijiDB.GetAllDistrictBasijiIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<DistrictBasijiEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DistrictBasiji_List_Page_" + currentPage ;
			//string countKey = "DistrictBasiji_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DistrictBasijiEntity> lst = new List<DistrictBasijiEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DistrictBasijiEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDistrictBasijiCollectionFromDistrictBasijiDBList(
				_DistrictBasijiDB.GetPageDistrictBasijiDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DistrictBasijiEntity GetDistrictBasijiFromDistrictBasijiDB(DistrictBasijiEntity o)
		{
	if(o == null)
                return null;
			DistrictBasijiEntity ret = new DistrictBasijiEntity(o.DistrictBasijiId ,o.DistrictBasijiTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<DistrictBasijiEntity> GetDistrictBasijiCollectionFromDistrictBasijiDBList( List<DistrictBasijiEntity> lst)
		{
			List<DistrictBasijiEntity> RetLst = new List<DistrictBasijiEntity>();
			foreach(DistrictBasijiEntity o in lst)
			{
				RetLst.Add(GetDistrictBasijiFromDistrictBasijiDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}

