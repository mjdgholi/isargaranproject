﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <نوع عضویت در بسیج>
    /// </summary>


    public class BasijiMembershipTypeBL
    {
        private readonly BasijiMembershipTypeDB _BasijiMembershipTypeDB;

        public BasijiMembershipTypeBL()
        {
            _BasijiMembershipTypeDB = new BasijiMembershipTypeDB();
        }

        public void Add(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam, out Guid BasijiMembershipTypeId)
        {
            _BasijiMembershipTypeDB.AddBasijiMembershipTypeDB(BasijiMembershipTypeEntityParam,
                                                              out BasijiMembershipTypeId);
        }

        public void Update(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam)
        {
            _BasijiMembershipTypeDB.UpdateBasijiMembershipTypeDB(BasijiMembershipTypeEntityParam);
        }

        public void Delete(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam)
        {
            _BasijiMembershipTypeDB.DeleteBasijiMembershipTypeDB(BasijiMembershipTypeEntityParam);
        }

        public BasijiMembershipTypeEntity GetSingleById(BasijiMembershipTypeEntity BasijiMembershipTypeEntityParam)
        {
            BasijiMembershipTypeEntity o = GetBasijiMembershipTypeFromBasijiMembershipTypeDB(
                _BasijiMembershipTypeDB.GetSingleBasijiMembershipTypeDB(BasijiMembershipTypeEntityParam));

            return o;
        }

        public List<BasijiMembershipTypeEntity> GetAll()
        {
            List<BasijiMembershipTypeEntity> lst = new List<BasijiMembershipTypeEntity>();
            //string key = "BasijiMembershipType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiMembershipTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBasijiMembershipTypeCollectionFromBasijiMembershipTypeDBList(
                _BasijiMembershipTypeDB.GetAllBasijiMembershipTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<BasijiMembershipTypeEntity> GetAllIsActive()
        {
            List<BasijiMembershipTypeEntity> lst = new List<BasijiMembershipTypeEntity>();
            //string key = "BasijiMembershipType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiMembershipTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBasijiMembershipTypeCollectionFromBasijiMembershipTypeDBList(
                _BasijiMembershipTypeDB.GetAllBasijiMembershipTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<BasijiMembershipTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                             string

                                                                 sortExpression, out int count, string whereClause)
        {
            //string key = "BasijiMembershipType_List_Page_" + currentPage ;
            //string countKey = "BasijiMembershipType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<BasijiMembershipTypeEntity> lst = new List<BasijiMembershipTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiMembershipTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetBasijiMembershipTypeCollectionFromBasijiMembershipTypeDBList(
                _BasijiMembershipTypeDB.GetPageBasijiMembershipTypeDB(pageSize, currentPage,
                                                                      whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private BasijiMembershipTypeEntity GetBasijiMembershipTypeFromBasijiMembershipTypeDB(
            BasijiMembershipTypeEntity o)
        {
            if (o == null)
                return null;
            BasijiMembershipTypeEntity ret = new BasijiMembershipTypeEntity(o.BasijiMembershipTypeId,
                                                                            o.BasijiMembershipTypeTitle, o.CreationDate,
                                                                            o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<BasijiMembershipTypeEntity> GetBasijiMembershipTypeCollectionFromBasijiMembershipTypeDBList(
            List<BasijiMembershipTypeEntity> lst)
        {
            List<BasijiMembershipTypeEntity> RetLst = new List<BasijiMembershipTypeEntity>();
            foreach (BasijiMembershipTypeEntity o in lst)
            {
                RetLst.Add(GetBasijiMembershipTypeFromBasijiMembershipTypeDB(o));
            }
            return RetLst;

        }


    }



}
