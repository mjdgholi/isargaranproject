﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <پایگاه بسیج>
    /// </summary>


    public class BasijiBaseBL
    {
        private readonly BasijiBaseDB _BasijiBaseDB;

        public BasijiBaseBL()
        {
            _BasijiBaseDB = new BasijiBaseDB();
        }

        public void Add(BasijiBaseEntity BasijiBaseEntityParam, out Guid BasijiBaseId)
        {
            _BasijiBaseDB.AddBasijiBaseDB(BasijiBaseEntityParam, out BasijiBaseId);
        }

        public void Update(BasijiBaseEntity BasijiBaseEntityParam)
        {
            _BasijiBaseDB.UpdateBasijiBaseDB(BasijiBaseEntityParam);
        }

        public void Delete(BasijiBaseEntity BasijiBaseEntityParam)
        {
            _BasijiBaseDB.DeleteBasijiBaseDB(BasijiBaseEntityParam);
        }

        public BasijiBaseEntity GetSingleById(BasijiBaseEntity BasijiBaseEntityParam)
        {
            BasijiBaseEntity o = GetBasijiBaseFromBasijiBaseDB(
                _BasijiBaseDB.GetSingleBasijiBaseDB(BasijiBaseEntityParam));

            return o;
        }

        public List<BasijiBaseEntity> GetAll()
        {
            List<BasijiBaseEntity> lst = new List<BasijiBaseEntity>();
            //string key = "BasijiBase_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiBaseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBasijiBaseCollectionFromBasijiBaseDBList(
                _BasijiBaseDB.GetAllBasijiBaseDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<BasijiBaseEntity> GetAllIsActive()
        {
            List<BasijiBaseEntity> lst = new List<BasijiBaseEntity>();
            //string key = "BasijiBase_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiBaseEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBasijiBaseCollectionFromBasijiBaseDBList(
                _BasijiBaseDB.GetAllBasijiBaseIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<BasijiBaseEntity> GetAllPaging(int currentPage, int pageSize,
                                                   string

                                                       sortExpression, out int count, string whereClause)
        {
            //string key = "BasijiBase_List_Page_" + currentPage ;
            //string countKey = "BasijiBase_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<BasijiBaseEntity> lst = new List<BasijiBaseEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiBaseEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetBasijiBaseCollectionFromBasijiBaseDBList(
                _BasijiBaseDB.GetPageBasijiBaseDB(pageSize, currentPage,
                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private BasijiBaseEntity GetBasijiBaseFromBasijiBaseDB(BasijiBaseEntity o)
        {
            if (o == null)
                return null;
            BasijiBaseEntity ret = new BasijiBaseEntity(o.BasijiBaseId, o.BasijiBaseTitle, o.DistrictBasijiId,
                                                        o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<BasijiBaseEntity> GetBasijiBaseCollectionFromBasijiBaseDBList(List<BasijiBaseEntity> lst)
        {
            List<BasijiBaseEntity> RetLst = new List<BasijiBaseEntity>();
            foreach (BasijiBaseEntity o in lst)
            {
                RetLst.Add(GetBasijiBaseFromBasijiBaseDB(o));
            }
            return RetLst;

        }


        public List<BasijiBaseEntity> GetBasijiBaseCollectionByDistrictBasiji(BasijiBaseEntity BasijiBaseEntityParam)
        {
            return
                GetBasijiBaseCollectionFromBasijiBaseDBList(
                    _BasijiBaseDB.GetBasijiBaseDBCollectionByDistrictBasijiDB(BasijiBaseEntityParam));
        }


    }
}

