﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/14>
    /// Description: <پرونده هنری ایثارگر>
    /// </summary>

    public class DossierSacrifice_ArtBL
    {
        private readonly DossierSacrifice_ArtDB _DossierSacrifice_ArtDB;

        public DossierSacrifice_ArtBL()
        {
            _DossierSacrifice_ArtDB = new DossierSacrifice_ArtDB();
        }

        public void Add(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam, out Guid DossierSacrifice_ArtId)
        {
            _DossierSacrifice_ArtDB.AddDossierSacrifice_ArtDB(DossierSacrifice_ArtEntityParam,
                                                              out DossierSacrifice_ArtId);
        }

        public void Update(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            _DossierSacrifice_ArtDB.UpdateDossierSacrifice_ArtDB(DossierSacrifice_ArtEntityParam);
        }

        public void Delete(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            _DossierSacrifice_ArtDB.DeleteDossierSacrifice_ArtDB(DossierSacrifice_ArtEntityParam);
        }

        public DossierSacrifice_ArtEntity GetSingleById(DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            DossierSacrifice_ArtEntity o = GetDossierSacrifice_ArtFromDossierSacrifice_ArtDB(
                _DossierSacrifice_ArtDB.GetSingleDossierSacrifice_ArtDB(DossierSacrifice_ArtEntityParam));

            return o;
        }

        public List<DossierSacrifice_ArtEntity> GetAll()
        {
            List<DossierSacrifice_ArtEntity> lst = new List<DossierSacrifice_ArtEntity>();
            //string key = "DossierSacrifice_Art_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ArtEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ArtCollectionFromDossierSacrifice_ArtDBList(
                _DossierSacrifice_ArtDB.GetAllDossierSacrifice_ArtDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_ArtEntity> GetAllPaging(int currentPage, int pageSize,
                                                             string

                                                                 sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Art_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Art_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_ArtEntity> lst = new List<DossierSacrifice_ArtEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ArtEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ArtCollectionFromDossierSacrifice_ArtDBList(
                _DossierSacrifice_ArtDB.GetPageDossierSacrifice_ArtDB(pageSize, currentPage,
                                                                      whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_ArtEntity GetDossierSacrifice_ArtFromDossierSacrifice_ArtDB(
            DossierSacrifice_ArtEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_ArtEntity ret = new DossierSacrifice_ArtEntity(o.DossierSacrifice_ArtId,
                                                                            o.DossierSacrificeId, o.ArtCourseId,
                                                                            o.HasEvidence, o.PlaceOfGraduation,
                                                                            o.Description, o.CreationDate,
                                                                            o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_ArtEntity> GetDossierSacrifice_ArtCollectionFromDossierSacrifice_ArtDBList(
            List<DossierSacrifice_ArtEntity> lst)
        {
            List<DossierSacrifice_ArtEntity> RetLst = new List<DossierSacrifice_ArtEntity>();
            foreach (DossierSacrifice_ArtEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_ArtFromDossierSacrifice_ArtDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_ArtEntity> GetDossierSacrifice_ArtCollectionByArtCourse(
            DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            return
                GetDossierSacrifice_ArtCollectionFromDossierSacrifice_ArtDBList(
                    _DossierSacrifice_ArtDB.GetDossierSacrifice_ArtDBCollectionByArtCourseDB(
                        DossierSacrifice_ArtEntityParam));
        }

        public List<DossierSacrifice_ArtEntity> GetDossierSacrifice_ArtCollectionByDossierSacrifice(
            DossierSacrifice_ArtEntity DossierSacrifice_ArtEntityParam)
        {
            return
                GetDossierSacrifice_ArtCollectionFromDossierSacrifice_ArtDBList(
                    _DossierSacrifice_ArtDB.GetDossierSacrifice_ArtDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_ArtEntityParam));
        }



    }

}
