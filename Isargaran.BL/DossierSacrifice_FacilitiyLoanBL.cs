﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <majid.Gholibeygian>
    /// Create date: <1393/02/22>
    /// Description: <اطلاعات وام>
    /// </summary>

    public class DossierSacrifice_FacilitiyLoanBL
    {
        private readonly DossierSacrifice_FacilitiyLoanDB _DossierSacrifice_FacilitiyLoanDB;

        public DossierSacrifice_FacilitiyLoanBL()
        {
            _DossierSacrifice_FacilitiyLoanDB = new DossierSacrifice_FacilitiyLoanDB();
        }

        public void Add(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam, out Guid DossierSacrifice_FacilitiyLoanId)
        {
            _DossierSacrifice_FacilitiyLoanDB.AddDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntityParam, out DossierSacrifice_FacilitiyLoanId);
        }

        public void Update(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam)
        {
            _DossierSacrifice_FacilitiyLoanDB.UpdateDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntityParam);
        }

        public void Delete(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam)
        {
            _DossierSacrifice_FacilitiyLoanDB.DeleteDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntityParam);
        }

        public DossierSacrifice_FacilitiyLoanEntity GetSingleById(DossierSacrifice_FacilitiyLoanEntity DossierSacrifice_FacilitiyLoanEntityParam)
        {
            DossierSacrifice_FacilitiyLoanEntity o = GetDossierSacrifice_FacilitiyLoanFromDossierSacrifice_FacilitiyLoanDB(
                _DossierSacrifice_FacilitiyLoanDB.GetSingleDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntityParam));

            return o;
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetAll()
        {
            List<DossierSacrifice_FacilitiyLoanEntity> lst = new List<DossierSacrifice_FacilitiyLoanEntity>();
            //string key = "DossierSacrifice_FacilitiyLoan_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilitiyLoanEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilitiyLoanCollectionFromDossierSacrifice_FacilitiyLoanDBList(
                _DossierSacrifice_FacilitiyLoanDB.GetAllDossierSacrifice_FacilitiyLoanDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_FacilitiyLoan_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_FacilitiyLoan_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_FacilitiyLoanEntity> lst = new List<DossierSacrifice_FacilitiyLoanEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilitiyLoanEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilitiyLoanCollectionFromDossierSacrifice_FacilitiyLoanDBList(
                _DossierSacrifice_FacilitiyLoanDB.GetPageDossierSacrifice_FacilitiyLoanDB(pageSize, currentPage,
                                                                                          whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_FacilitiyLoanEntity GetDossierSacrifice_FacilitiyLoanFromDossierSacrifice_FacilitiyLoanDB(DossierSacrifice_FacilitiyLoanEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_FacilitiyLoanEntity ret = new DossierSacrifice_FacilitiyLoanEntity(o.DossierSacrifice_FacilitiyLoanId, o.DossierSacrificeId, o.LoanTypeId, o.StartDate, o.EndDate, o.SourceBudgetPayment, o.LoanPaymentAmount, o.LoanCommissionAmount, o.RecipientPersonName, o.RecipientPersonNationalNo, o.Description, o.IsLoanSettlement, o.ModificationDate, o.CreationDate);
            return ret;
        }

        private List<DossierSacrifice_FacilitiyLoanEntity> GetDossierSacrifice_FacilitiyLoanCollectionFromDossierSacrifice_FacilitiyLoanDBList(List<DossierSacrifice_FacilitiyLoanEntity> lst)
        {
            List<DossierSacrifice_FacilitiyLoanEntity> RetLst = new List<DossierSacrifice_FacilitiyLoanEntity>();
            foreach (DossierSacrifice_FacilitiyLoanEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_FacilitiyLoanFromDossierSacrifice_FacilitiyLoanDB(o));
            }
            return RetLst;

        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetDossierSacrifice_FacilitiyLoanCollectionByDossierSacrifice(Guid DossierSacrificeId)
        {
            return GetDossierSacrifice_FacilitiyLoanCollectionFromDossierSacrifice_FacilitiyLoanDBList(_DossierSacrifice_FacilitiyLoanDB.GetDossierSacrifice_FacilitiyLoanDBCollectionByDossierSacrificeDB(DossierSacrificeId));
        }

        public List<DossierSacrifice_FacilitiyLoanEntity> GetDossierSacrifice_FacilitiyLoanCollectionByLoanType(Guid LoanTypeId)
        {
            return GetDossierSacrifice_FacilitiyLoanCollectionFromDossierSacrifice_FacilitiyLoanDBList(_DossierSacrifice_FacilitiyLoanDB.GetDossierSacrifice_FacilitiyLoanDBCollectionByLoanTypeDB(LoanTypeId));
        }
    }
}