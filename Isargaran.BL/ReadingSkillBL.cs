﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: < مهارت خواندن>
    /// </summary>
    public class ReadingSkillBL
    {
        private readonly ReadingSkillDB _ReadingSkillDB;

        public ReadingSkillBL()
        {
            _ReadingSkillDB = new ReadingSkillDB();
        }

        public void Add(ReadingSkillEntity ReadingSkillEntityParam, out Guid ReadingSkillId)
        {
            _ReadingSkillDB.AddReadingSkillDB(ReadingSkillEntityParam, out ReadingSkillId);
        }

        public void Update(ReadingSkillEntity ReadingSkillEntityParam)
        {
            _ReadingSkillDB.UpdateReadingSkillDB(ReadingSkillEntityParam);
        }

        public void Delete(ReadingSkillEntity ReadingSkillEntityParam)
        {
            _ReadingSkillDB.DeleteReadingSkillDB(ReadingSkillEntityParam);
        }

        public ReadingSkillEntity GetSingleById(ReadingSkillEntity ReadingSkillEntityParam)
        {
            ReadingSkillEntity o = GetReadingSkillFromReadingSkillDB(
                _ReadingSkillDB.GetSingleReadingSkillDB(ReadingSkillEntityParam));

            return o;
        }

        public List<ReadingSkillEntity> GetAll()
        {
            List<ReadingSkillEntity> lst = new List<ReadingSkillEntity>();
            //string key = "ReadingSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ReadingSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetReadingSkillCollectionFromReadingSkillDBList(
                _ReadingSkillDB.GetAllReadingSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ReadingSkillEntity> GetAllIsActive()
        {
            List<ReadingSkillEntity> lst = new List<ReadingSkillEntity>();
            //string key = "ReadingSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ReadingSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetReadingSkillCollectionFromReadingSkillDBList(
                _ReadingSkillDB.GetAllIsActiveReadingSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ReadingSkillEntity> GetAllPaging(int currentPage, int pageSize,
                                                     string

                                                         sortExpression, out int count, string whereClause)
        {
            //string key = "ReadingSkill_List_Page_" + currentPage ;
            //string countKey = "ReadingSkill_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ReadingSkillEntity> lst = new List<ReadingSkillEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ReadingSkillEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetReadingSkillCollectionFromReadingSkillDBList(
                _ReadingSkillDB.GetPageReadingSkillDB(pageSize, currentPage,
                                                      whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ReadingSkillEntity GetReadingSkillFromReadingSkillDB(ReadingSkillEntity o)
        {
            if (o == null)
                return null;
            ReadingSkillEntity ret = new ReadingSkillEntity(o.ReadingSkillId, o.ReadingSkillPersianTitle, o.ReadingSkillEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<ReadingSkillEntity> GetReadingSkillCollectionFromReadingSkillDBList(List<ReadingSkillEntity> lst)
        {
            List<ReadingSkillEntity> RetLst = new List<ReadingSkillEntity>();
            foreach (ReadingSkillEntity o in lst)
            {
                RetLst.Add(GetReadingSkillFromReadingSkillDB(o));
            }
            return RetLst;

        }
    }
}