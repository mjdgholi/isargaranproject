﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <نوع استخدام>
    /// </summary>

	public class EmploymentTypeBL 
	{	
	  	 private readonly EmploymentTypeDB _EmploymentTypeDB;					
			
		public EmploymentTypeBL()
		{
			_EmploymentTypeDB = new EmploymentTypeDB();
		}			
	
		public  void Add(EmploymentTypeEntity  EmploymentTypeEntityParam, out Guid EmploymentTypeId)
		{ 
			_EmploymentTypeDB.AddEmploymentTypeDB(EmploymentTypeEntityParam,out EmploymentTypeId);			
		}

		public  void Update(EmploymentTypeEntity  EmploymentTypeEntityParam)
		{
			_EmploymentTypeDB.UpdateEmploymentTypeDB(EmploymentTypeEntityParam);		
		}

		public  void Delete(EmploymentTypeEntity  EmploymentTypeEntityParam)
		{
			 _EmploymentTypeDB.DeleteEmploymentTypeDB(EmploymentTypeEntityParam);			
		}

		public  EmploymentTypeEntity GetSingleById(EmploymentTypeEntity  EmploymentTypeEntityParam)
		{
			EmploymentTypeEntity o = GetEmploymentTypeFromEmploymentTypeDB(
			_EmploymentTypeDB.GetSingleEmploymentTypeDB(EmploymentTypeEntityParam));
			
			return o;
		}

		public  List<EmploymentTypeEntity> GetAll()
		{
			List<EmploymentTypeEntity> lst = new List<EmploymentTypeEntity>();
			//string key = "EmploymentType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EmploymentTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetEmploymentTypeCollectionFromEmploymentTypeDBList(
				_EmploymentTypeDB.GetAllEmploymentTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<EmploymentTypeEntity> GetAllIsActive()
        {
            List<EmploymentTypeEntity> lst = new List<EmploymentTypeEntity>();
            //string key = "EmploymentType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EmploymentTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEmploymentTypeCollectionFromEmploymentTypeDBList(
            _EmploymentTypeDB.GetAllEmploymentTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<EmploymentTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "EmploymentType_List_Page_" + currentPage ;
			//string countKey = "EmploymentType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<EmploymentTypeEntity> lst = new List<EmploymentTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EmploymentTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetEmploymentTypeCollectionFromEmploymentTypeDBList(
				_EmploymentTypeDB.GetPageEmploymentTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  EmploymentTypeEntity GetEmploymentTypeFromEmploymentTypeDB(EmploymentTypeEntity o)
		{
	if(o == null)
                return null;
			EmploymentTypeEntity ret = new EmploymentTypeEntity(o.EmploymentTypeId ,o.EmploymentTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<EmploymentTypeEntity> GetEmploymentTypeCollectionFromEmploymentTypeDBList( List<EmploymentTypeEntity> lst)
		{
			List<EmploymentTypeEntity> RetLst = new List<EmploymentTypeEntity>();
			foreach(EmploymentTypeEntity o in lst)
			{
				RetLst.Add(GetEmploymentTypeFromEmploymentTypeDB(o));
			}
			return RetLst;
			
		} 

}

}
