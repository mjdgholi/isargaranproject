﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <رسته بسیجی>
    /// </summary>

    public class BasijiCategoryBL
    {
        private readonly BasijiCategoryDB _BasijiCategoryDB;

        public BasijiCategoryBL()
        {
            _BasijiCategoryDB = new BasijiCategoryDB();
        }

        public void Add(BasijiCategoryEntity BasijiCategoryEntityParam, out Guid BasijiCategoryId)
        {
            _BasijiCategoryDB.AddBasijiCategoryDB(BasijiCategoryEntityParam, out BasijiCategoryId);
        }

        public void Update(BasijiCategoryEntity BasijiCategoryEntityParam)
        {
            _BasijiCategoryDB.UpdateBasijiCategoryDB(BasijiCategoryEntityParam);
        }

        public void Delete(BasijiCategoryEntity BasijiCategoryEntityParam)
        {
            _BasijiCategoryDB.DeleteBasijiCategoryDB(BasijiCategoryEntityParam);
        }

        public BasijiCategoryEntity GetSingleById(BasijiCategoryEntity BasijiCategoryEntityParam)
        {
            BasijiCategoryEntity o = GetBasijiCategoryFromBasijiCategoryDB(
                _BasijiCategoryDB.GetSingleBasijiCategoryDB(BasijiCategoryEntityParam));

            return o;
        }

        public List<BasijiCategoryEntity> GetAll()
        {
            List<BasijiCategoryEntity> lst = new List<BasijiCategoryEntity>();
            //string key = "BasijiCategory_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiCategoryEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBasijiCategoryCollectionFromBasijiCategoryDBList(
                _BasijiCategoryDB.GetAllBasijiCategoryDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<BasijiCategoryEntity> GetAllIsActive()
        {
            List<BasijiCategoryEntity> lst = new List<BasijiCategoryEntity>();
            //string key = "BasijiCategory_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiCategoryEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBasijiCategoryCollectionFromBasijiCategoryDBList(
                _BasijiCategoryDB.GetAllBasijiCategoryIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<BasijiCategoryEntity> GetAllPaging(int currentPage, int pageSize,
                                                       string

                                                           sortExpression, out int count, string whereClause)
        {
            //string key = "BasijiCategory_List_Page_" + currentPage ;
            //string countKey = "BasijiCategory_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<BasijiCategoryEntity> lst = new List<BasijiCategoryEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BasijiCategoryEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetBasijiCategoryCollectionFromBasijiCategoryDBList(
                _BasijiCategoryDB.GetPageBasijiCategoryDB(pageSize, currentPage,
                                                          whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private BasijiCategoryEntity GetBasijiCategoryFromBasijiCategoryDB(BasijiCategoryEntity o)
        {
            if (o == null)
                return null;
            BasijiCategoryEntity ret = new BasijiCategoryEntity(o.BasijiCategoryId, o.BasijiCategoryTitle,
                                                                o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<BasijiCategoryEntity> GetBasijiCategoryCollectionFromBasijiCategoryDBList(
            List<BasijiCategoryEntity> lst)
        {
            List<BasijiCategoryEntity> RetLst = new List<BasijiCategoryEntity>();
            foreach (BasijiCategoryEntity o in lst)
            {
                RetLst.Add(GetBasijiCategoryFromBasijiCategoryDB(o));
            }
            return RetLst;

        }


    }

}
