﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/25>
    /// Description: <نوع پیغام>
    /// </summary>

	public class MessageTypeBL 
	{	
	  	 private readonly MessageTypeDB _MessageTypeDB;					
			
		public MessageTypeBL()
		{
			_MessageTypeDB = new MessageTypeDB();
		}			
	
		public  void Add(MessageTypeEntity  MessageTypeEntityParam, out Guid MessageTypeId)
		{ 
			_MessageTypeDB.AddMessageTypeDB(MessageTypeEntityParam,out MessageTypeId);			
		}

		public  void Update(MessageTypeEntity  MessageTypeEntityParam)
		{
			_MessageTypeDB.UpdateMessageTypeDB(MessageTypeEntityParam);		
		}

		public  void Delete(MessageTypeEntity  MessageTypeEntityParam)
		{
			 _MessageTypeDB.DeleteMessageTypeDB(MessageTypeEntityParam);			
		}

		public  MessageTypeEntity GetSingleById(MessageTypeEntity  MessageTypeEntityParam)
		{
			MessageTypeEntity o = GetMessageTypeFromMessageTypeDB(
			_MessageTypeDB.GetSingleMessageTypeDB(MessageTypeEntityParam));
			
			return o;
		}

		public  List<MessageTypeEntity> GetAll()
		{
			List<MessageTypeEntity> lst = new List<MessageTypeEntity>();
			//string key = "MessageType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MessageTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetMessageTypeCollectionFromMessageTypeDBList(
				_MessageTypeDB.GetAllMessageTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<MessageTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "MessageType_List_Page_" + currentPage ;
			//string countKey = "MessageType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<MessageTypeEntity> lst = new List<MessageTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MessageTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetMessageTypeCollectionFromMessageTypeDBList(
				_MessageTypeDB.GetPageMessageTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  MessageTypeEntity GetMessageTypeFromMessageTypeDB(MessageTypeEntity o)
		{
	if(o == null)
                return null;
			MessageTypeEntity ret = new MessageTypeEntity(o.MessageTypeId ,o.MessageTypeTitle ,o.IsInpout ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<MessageTypeEntity> GetMessageTypeCollectionFromMessageTypeDBList( List<MessageTypeEntity> lst)
		{
			List<MessageTypeEntity> RetLst = new List<MessageTypeEntity>();
			foreach(MessageTypeEntity o in lst)
			{
				RetLst.Add(GetMessageTypeFromMessageTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}
