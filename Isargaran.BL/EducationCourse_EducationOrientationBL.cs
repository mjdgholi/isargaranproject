﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/12>
    /// Description: <رشته تحصیلی -گرایش تحصیلی >
    /// </summary>


    public class EducationCourse_EducationOrientationBL
    {
        private readonly EducationCourse_EducationOrientationDB _EducationCourse_EducationOrientationDB;

        public EducationCourse_EducationOrientationBL()
        {
            _EducationCourse_EducationOrientationDB = new EducationCourse_EducationOrientationDB();
        }

        public void Add(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam,
            out Guid EducationCourse_EducationOrientationId)
        {
            _EducationCourse_EducationOrientationDB.AddEducationCourse_EducationOrientationDB(
                EducationCourse_EducationOrientationEntityParam, out EducationCourse_EducationOrientationId);
        }

        public void Update(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            _EducationCourse_EducationOrientationDB.UpdateEducationCourse_EducationOrientationDB(
                EducationCourse_EducationOrientationEntityParam);
        }

        public void Delete(EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            _EducationCourse_EducationOrientationDB.DeleteEducationCourse_EducationOrientationDB(
                EducationCourse_EducationOrientationEntityParam);
        }

        public EducationCourse_EducationOrientationEntity GetSingleById(
            EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            EducationCourse_EducationOrientationEntity o = GetEducationCourse_EducationOrientationFromEducationCourse_EducationOrientationDB
                (
                    _EducationCourse_EducationOrientationDB.GetSingleEducationCourse_EducationOrientationDB(
                        EducationCourse_EducationOrientationEntityParam));

            return o;
        }

        public List<EducationCourse_EducationOrientationEntity> GetAll()
        {
            List<EducationCourse_EducationOrientationEntity> lst =
                new List<EducationCourse_EducationOrientationEntity>();
            //string key = "EducationCourse_EducationOrientation_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCourse_EducationOrientationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationCourse_EducationOrientationCollectionFromEducationCourse_EducationOrientationDBList(
                _EducationCourse_EducationOrientationDB.GetAllEducationCourse_EducationOrientationDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<EducationCourse_EducationOrientationEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "EducationCourse_EducationOrientation_List_Page_" + currentPage ;
            //string countKey = "EducationCourse_EducationOrientation_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<EducationCourse_EducationOrientationEntity> lst =
                new List<EducationCourse_EducationOrientationEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationCourse_EducationOrientationEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetEducationCourse_EducationOrientationCollectionFromEducationCourse_EducationOrientationDBList(
                _EducationCourse_EducationOrientationDB.GetPageEducationCourse_EducationOrientationDB(pageSize,
                    currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private EducationCourse_EducationOrientationEntity
            GetEducationCourse_EducationOrientationFromEducationCourse_EducationOrientationDB(
            EducationCourse_EducationOrientationEntity o)
        {
            if (o == null)
                return null;
            EducationCourse_EducationOrientationEntity ret =
                new EducationCourse_EducationOrientationEntity(o.EducationCourse_EducationOrientationId,
                    o.EducationCourseId, o.EducationOreintionId, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<EducationCourse_EducationOrientationEntity>
            GetEducationCourse_EducationOrientationCollectionFromEducationCourse_EducationOrientationDBList(
            List<EducationCourse_EducationOrientationEntity> lst)
        {
            List<EducationCourse_EducationOrientationEntity> RetLst =
                new List<EducationCourse_EducationOrientationEntity>();
            foreach (EducationCourse_EducationOrientationEntity o in lst)
            {
                RetLst.Add(GetEducationCourse_EducationOrientationFromEducationCourse_EducationOrientationDB(o));
            }
            return RetLst;

        }




        public DataSet
            GetEducationCourse_EducationOrientationCollectionByEducationCourse(
            EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            return
   
                    _EducationCourse_EducationOrientationDB
                        .GetEducationCourse_EducationOrientationDBCollectionByEducationCourseDB(
                            EducationCourse_EducationOrientationEntityParam);
        }

        public List<EducationCourse_EducationOrientationEntity>
            GetEducationCourse_EducationOrientationCollectionByEducationOrientation(
            EducationCourse_EducationOrientationEntity EducationCourse_EducationOrientationEntityParam)
        {
            return
                GetEducationCourse_EducationOrientationCollectionFromEducationCourse_EducationOrientationDBList(
                    _EducationCourse_EducationOrientationDB
                        .GetEducationCourse_EducationOrientationDBCollectionByEducationOrientationDB(
                            EducationCourse_EducationOrientationEntityParam));
        }




    }
}
