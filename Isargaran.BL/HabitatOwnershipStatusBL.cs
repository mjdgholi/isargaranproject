﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/17>
    /// Description: <وضیعت مالکیت مسکن>
    /// </summary>


	public class HabitatOwnershipStatusBL 
	{	
	  	 private readonly HabitatOwnershipStatusDB _HabitatOwnershipStatusDB;					
			
		public HabitatOwnershipStatusBL()
		{
			_HabitatOwnershipStatusDB = new HabitatOwnershipStatusDB();
		}			
	
		public  void Add(HabitatOwnershipStatusEntity  HabitatOwnershipStatusEntityParam, out Guid HabitatOwnershipStatusId)
		{ 
			_HabitatOwnershipStatusDB.AddHabitatOwnershipStatusDB(HabitatOwnershipStatusEntityParam,out HabitatOwnershipStatusId);			
		}

		public  void Update(HabitatOwnershipStatusEntity  HabitatOwnershipStatusEntityParam)
		{
			_HabitatOwnershipStatusDB.UpdateHabitatOwnershipStatusDB(HabitatOwnershipStatusEntityParam);		
		}

		public  void Delete(HabitatOwnershipStatusEntity  HabitatOwnershipStatusEntityParam)
		{
			 _HabitatOwnershipStatusDB.DeleteHabitatOwnershipStatusDB(HabitatOwnershipStatusEntityParam);			
		}

		public  HabitatOwnershipStatusEntity GetSingleById(HabitatOwnershipStatusEntity  HabitatOwnershipStatusEntityParam)
		{
			HabitatOwnershipStatusEntity o = GetHabitatOwnershipStatusFromHabitatOwnershipStatusDB(
			_HabitatOwnershipStatusDB.GetSingleHabitatOwnershipStatusDB(HabitatOwnershipStatusEntityParam));
			
			return o;
		}

		public  List<HabitatOwnershipStatusEntity> GetAll()
		{
			List<HabitatOwnershipStatusEntity> lst = new List<HabitatOwnershipStatusEntity>();
			//string key = "HabitatOwnershipStatus_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<HabitatOwnershipStatusEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetHabitatOwnershipStatusCollectionFromHabitatOwnershipStatusDBList(
				_HabitatOwnershipStatusDB.GetAllHabitatOwnershipStatusDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<HabitatOwnershipStatusEntity> GetAllIsActive()
        {
            List<HabitatOwnershipStatusEntity> lst = new List<HabitatOwnershipStatusEntity>();
            //string key = "HabitatOwnershipStatus_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<HabitatOwnershipStatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetHabitatOwnershipStatusCollectionFromHabitatOwnershipStatusDBList(
            _HabitatOwnershipStatusDB.GetAllHabitatOwnershipStatusIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<HabitatOwnershipStatusEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "HabitatOwnershipStatus_List_Page_" + currentPage ;
			//string countKey = "HabitatOwnershipStatus_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<HabitatOwnershipStatusEntity> lst = new List<HabitatOwnershipStatusEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<HabitatOwnershipStatusEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetHabitatOwnershipStatusCollectionFromHabitatOwnershipStatusDBList(
				_HabitatOwnershipStatusDB.GetPageHabitatOwnershipStatusDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  HabitatOwnershipStatusEntity GetHabitatOwnershipStatusFromHabitatOwnershipStatusDB(HabitatOwnershipStatusEntity o)
		{
	if(o == null)
                return null;
			HabitatOwnershipStatusEntity ret = new HabitatOwnershipStatusEntity(o.HabitatOwnershipStatusId ,o.HabitatOwnershipStatusTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<HabitatOwnershipStatusEntity> GetHabitatOwnershipStatusCollectionFromHabitatOwnershipStatusDBList( List<HabitatOwnershipStatusEntity> lst)
		{
			List<HabitatOwnershipStatusEntity> RetLst = new List<HabitatOwnershipStatusEntity>();
			foreach(HabitatOwnershipStatusEntity o in lst)
			{
				RetLst.Add(GetHabitatOwnershipStatusFromHabitatOwnershipStatusDB(o));
			}
			return RetLst;
			
		} 
				
		
	}



}
