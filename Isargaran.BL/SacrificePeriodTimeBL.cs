﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/28>
    // Description:	<دوره زمانی ایثارگر>
    /// </summary>


	public class SacrificePeriodTimeBL 
	{	
	  	 private readonly SacrificePeriodTimeDB _SacrificePeriodTimeDB;					
			
		public SacrificePeriodTimeBL()
		{
			_SacrificePeriodTimeDB = new SacrificePeriodTimeDB();
		}			
	
		public  void Add(SacrificePeriodTimeEntity  SacrificePeriodTimeEntityParam, out Guid SacrificePeriodTimeId)
		{ 
			_SacrificePeriodTimeDB.AddSacrificePeriodTimeDB(SacrificePeriodTimeEntityParam,out SacrificePeriodTimeId);			
		}

		public  void Update(SacrificePeriodTimeEntity  SacrificePeriodTimeEntityParam)
		{
			_SacrificePeriodTimeDB.UpdateSacrificePeriodTimeDB(SacrificePeriodTimeEntityParam);		
		}

		public  void Delete(SacrificePeriodTimeEntity  SacrificePeriodTimeEntityParam)
		{
			 _SacrificePeriodTimeDB.DeleteSacrificePeriodTimeDB(SacrificePeriodTimeEntityParam);			
		}

		public  SacrificePeriodTimeEntity GetSingleById(SacrificePeriodTimeEntity  SacrificePeriodTimeEntityParam)
		{
			SacrificePeriodTimeEntity o = GetSacrificePeriodTimeFromSacrificePeriodTimeDB(
			_SacrificePeriodTimeDB.GetSingleSacrificePeriodTimeDB(SacrificePeriodTimeEntityParam));
			
			return o;
		}

		public  List<SacrificePeriodTimeEntity> GetAll()
		{
			List<SacrificePeriodTimeEntity> lst = new List<SacrificePeriodTimeEntity>();
			//string key = "SacrificePeriodTime_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SacrificePeriodTimeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSacrificePeriodTimeCollectionFromSacrificePeriodTimeDBList(
				_SacrificePeriodTimeDB.GetAllSacrificePeriodTimeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
				public  List<SacrificePeriodTimeEntity> GetAllIsActive()
		{
			List<SacrificePeriodTimeEntity> lst = new List<SacrificePeriodTimeEntity>();
			//string key = "SacrificePeriodTime_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SacrificePeriodTimeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSacrificePeriodTimeCollectionFromSacrificePeriodTimeDBList(
                _SacrificePeriodTimeDB.GetAllSacrificePeriodTimeIsActiveDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		public  List<SacrificePeriodTimeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "SacrificePeriodTime_List_Page_" + currentPage ;
			//string countKey = "SacrificePeriodTime_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<SacrificePeriodTimeEntity> lst = new List<SacrificePeriodTimeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SacrificePeriodTimeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetSacrificePeriodTimeCollectionFromSacrificePeriodTimeDBList(
				_SacrificePeriodTimeDB.GetPageSacrificePeriodTimeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  SacrificePeriodTimeEntity GetSacrificePeriodTimeFromSacrificePeriodTimeDB(SacrificePeriodTimeEntity o)
		{
	if(o == null)
                return null;
			SacrificePeriodTimeEntity ret = new SacrificePeriodTimeEntity(o.SacrificePeriodTimeId ,o.SacrificePeriodTimeTitle ,o.Periority ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<SacrificePeriodTimeEntity> GetSacrificePeriodTimeCollectionFromSacrificePeriodTimeDBList( List<SacrificePeriodTimeEntity> lst)
		{
			List<SacrificePeriodTimeEntity> RetLst = new List<SacrificePeriodTimeEntity>();
			foreach(SacrificePeriodTimeEntity o in lst)
			{
				RetLst.Add(GetSacrificePeriodTimeFromSacrificePeriodTimeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}

