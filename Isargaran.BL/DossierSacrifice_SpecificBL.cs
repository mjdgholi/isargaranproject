﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;


namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/27>
    /// Description: <اطلاعات خاص>
    /// </summary>
    public class DossierSacrifice_SpecificBL
    {
        private readonly DossierSacrifice_SpecificDB _DossierSacrifice_SpecificDB;

        public DossierSacrifice_SpecificBL()
        {
            _DossierSacrifice_SpecificDB = new DossierSacrifice_SpecificDB();
        }

        private void Validation(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam)
        {
            if (DossierSacrifice_SpecificEntityParam.IsHasSupplementaryInsurance && DossierSacrifice_SpecificEntityParam.SupplementaryInsuranceNo.Trim() == "")
            {
                throw new ItcApplicationErrorManagerException("شماره بیمه را وارد نمایید");
            }
        }

        public void Add(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam, out Guid DossierSacrifice_SpecificId)
        {
            Validation(DossierSacrifice_SpecificEntityParam);
            _DossierSacrifice_SpecificDB.AddDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntityParam, out DossierSacrifice_SpecificId);
        }

        public void Update(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam)
        {
            Validation(DossierSacrifice_SpecificEntityParam);
            _DossierSacrifice_SpecificDB.UpdateDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntityParam);
        }

        public void Delete(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam)
        {
            _DossierSacrifice_SpecificDB.DeleteDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntityParam);
        }

        public DossierSacrifice_SpecificEntity GetSingleById(DossierSacrifice_SpecificEntity DossierSacrifice_SpecificEntityParam)
        {
            DossierSacrifice_SpecificEntity o = GetDossierSacrifice_SpecificFromDossierSacrifice_SpecificDB(
                _DossierSacrifice_SpecificDB.GetSingleDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntityParam));

            return o;
        }

        public List<DossierSacrifice_SpecificEntity> GetAll()
        {
            List<DossierSacrifice_SpecificEntity> lst = new List<DossierSacrifice_SpecificEntity>();
            //string key = "DossierSacrifice_Specific_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_SpecificEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_SpecificCollectionFromDossierSacrifice_SpecificDBList(
                _DossierSacrifice_SpecificDB.GetAllDossierSacrifice_SpecificDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_SpecificEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Specific_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Specific_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_SpecificEntity> lst = new List<DossierSacrifice_SpecificEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_SpecificEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_SpecificCollectionFromDossierSacrifice_SpecificDBList(
                _DossierSacrifice_SpecificDB.GetPageDossierSacrifice_SpecificDB(pageSize, currentPage,
                                                                                whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static DossierSacrifice_SpecificEntity GetDossierSacrifice_SpecificFromDossierSacrifice_SpecificDB(DossierSacrifice_SpecificEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_SpecificEntity ret = new DossierSacrifice_SpecificEntity(o.DossierSacrifice_SpecificId, o.DossierSacrificeId, o.Email, o.WebAddress, o.Description, o.BankId, o.BankBranchCode, o.BankBranchTitle, o.AccountNo, o.OpeningAccountDate, o.IsHasSupplementaryInsurance, o.SupplementaryInsuranceNo, o.IsAccountActive, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private static List<DossierSacrifice_SpecificEntity> GetDossierSacrifice_SpecificCollectionFromDossierSacrifice_SpecificDBList(List<DossierSacrifice_SpecificEntity> lst)
        {
            List<DossierSacrifice_SpecificEntity> RetLst = new List<DossierSacrifice_SpecificEntity>();
            foreach (DossierSacrifice_SpecificEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_SpecificFromDossierSacrifice_SpecificDB(o));
            }
            return RetLst;

        }

        public static List<DossierSacrifice_SpecificEntity> GetDossierSacrifice_SpecificCollectionByBank(BankEntity bankEntity)
        {
            return GetDossierSacrifice_SpecificCollectionFromDossierSacrifice_SpecificDBList(DossierSacrifice_SpecificDB.GetDossierSacrifice_SpecificDBCollectionByBankDB(bankEntity));
        }

        public static List<DossierSacrifice_SpecificEntity> GetDossierSacrifice_SpecificCollectionByDossierSacrifice(DossierSacrificeEntity dossierSacrificeEntity)
        {
            return GetDossierSacrifice_SpecificCollectionFromDossierSacrifice_SpecificDBList(DossierSacrifice_SpecificDB.GetDossierSacrifice_SpecificDBCollectionByDossierSacrificeDB(dossierSacrificeEntity));
        }
    }
}