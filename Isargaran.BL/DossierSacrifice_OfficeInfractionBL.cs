﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <پرونده تخلف>
    /// </summary>

	public class DossierSacrifice_OfficeInfractionBL 
	{	
	  	 private readonly DossierSacrifice_OfficeInfractionDB _DossierSacrifice_OfficeInfractionDB;					
			
		public DossierSacrifice_OfficeInfractionBL()
		{
			_DossierSacrifice_OfficeInfractionDB = new DossierSacrifice_OfficeInfractionDB();
		}			
	
		public  void Add(DossierSacrifice_OfficeInfractionEntity  DossierSacrifice_OfficeInfractionEntityParam, out Guid DossierSacrifice_OfficeInfractionId)
		{ 
			_DossierSacrifice_OfficeInfractionDB.AddDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntityParam,out DossierSacrifice_OfficeInfractionId);			
		}

		public  void Update(DossierSacrifice_OfficeInfractionEntity  DossierSacrifice_OfficeInfractionEntityParam)
		{
			_DossierSacrifice_OfficeInfractionDB.UpdateDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntityParam);		
		}

		public  void Delete(DossierSacrifice_OfficeInfractionEntity  DossierSacrifice_OfficeInfractionEntityParam)
		{
			 _DossierSacrifice_OfficeInfractionDB.DeleteDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntityParam);			
		}

		public  DossierSacrifice_OfficeInfractionEntity GetSingleById(DossierSacrifice_OfficeInfractionEntity  DossierSacrifice_OfficeInfractionEntityParam)
		{
			DossierSacrifice_OfficeInfractionEntity o = GetDossierSacrifice_OfficeInfractionFromDossierSacrifice_OfficeInfractionDB(
			_DossierSacrifice_OfficeInfractionDB.GetSingleDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_OfficeInfractionEntity> GetAll()
		{
			List<DossierSacrifice_OfficeInfractionEntity> lst = new List<DossierSacrifice_OfficeInfractionEntity>();
			//string key = "DossierSacrifice_OfficeInfraction_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_OfficeInfractionEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_OfficeInfractionCollectionFromDossierSacrifice_OfficeInfractionDBList(
				_DossierSacrifice_OfficeInfractionDB.GetAllDossierSacrifice_OfficeInfractionDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_OfficeInfractionEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_OfficeInfraction_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_OfficeInfraction_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_OfficeInfractionEntity> lst = new List<DossierSacrifice_OfficeInfractionEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_OfficeInfractionEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_OfficeInfractionCollectionFromDossierSacrifice_OfficeInfractionDBList(
				_DossierSacrifice_OfficeInfractionDB.GetPageDossierSacrifice_OfficeInfractionDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_OfficeInfractionEntity GetDossierSacrifice_OfficeInfractionFromDossierSacrifice_OfficeInfractionDB(DossierSacrifice_OfficeInfractionEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_OfficeInfractionEntity ret = new DossierSacrifice_OfficeInfractionEntity(o.DossierSacrifice_OfficeInfractionId ,o.DossierSacrificeId ,o.InfractionLocation ,o.InfractionDate ,o.InfractionTime ,o.InfractionTypeId ,o.InfractionContent ,o.Description ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<DossierSacrifice_OfficeInfractionEntity> GetDossierSacrifice_OfficeInfractionCollectionFromDossierSacrifice_OfficeInfractionDBList( List<DossierSacrifice_OfficeInfractionEntity> lst)
		{
			List<DossierSacrifice_OfficeInfractionEntity> RetLst = new List<DossierSacrifice_OfficeInfractionEntity>();
			foreach(DossierSacrifice_OfficeInfractionEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_OfficeInfractionFromDossierSacrifice_OfficeInfractionDB(o));
			}
			return RetLst;
			
		}


        public List<DossierSacrifice_OfficeInfractionEntity> GetDossierSacrifice_OfficeInfractionCollectionByDossierSacrifice(DossierSacrifice_OfficeInfractionEntity dossierSacrificeOfficeInfractionEntityParam)
{
    return GetDossierSacrifice_OfficeInfractionCollectionFromDossierSacrifice_OfficeInfractionDBList(_DossierSacrifice_OfficeInfractionDB.GetDossierSacrifice_OfficeInfractionDBCollectionByDossierSacrificeDB(dossierSacrificeOfficeInfractionEntityParam));
}

        public List<DossierSacrifice_OfficeInfractionEntity> GetDossierSacrifice_OfficeInfractionCollectionByInfractionType(DossierSacrifice_OfficeInfractionEntity dossierSacrificeOfficeInfractionEntityParam)
{
    return GetDossierSacrifice_OfficeInfractionCollectionFromDossierSacrifice_OfficeInfractionDBList(_DossierSacrifice_OfficeInfractionDB.GetDossierSacrifice_OfficeInfractionDBCollectionByInfractionTypeDB(dossierSacrificeOfficeInfractionEntityParam));
}


}

}
