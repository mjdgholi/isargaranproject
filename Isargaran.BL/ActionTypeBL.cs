﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/21>
    /// Description: <نوع اقدام>
    /// </summary>
	public class ActionTypeBL 
	{	
	  	 private readonly ActionTypeDB _ActionTypeDB;					
			
		public ActionTypeBL()
		{
			_ActionTypeDB = new ActionTypeDB();
		}			
	
		public  void Add(ActionTypeEntity  ActionTypeEntityParam, out Guid ActionTypeId)
		{ 
			_ActionTypeDB.AddActionTypeDB(ActionTypeEntityParam,out ActionTypeId);			
		}

		public  void Update(ActionTypeEntity  ActionTypeEntityParam)
		{
			_ActionTypeDB.UpdateActionTypeDB(ActionTypeEntityParam);		
		}

		public  void Delete(ActionTypeEntity  ActionTypeEntityParam)
		{
			 _ActionTypeDB.DeleteActionTypeDB(ActionTypeEntityParam);			
		}

		public  ActionTypeEntity GetSingleById(ActionTypeEntity  ActionTypeEntityParam)
		{
			ActionTypeEntity o = GetActionTypeFromActionTypeDB(
			_ActionTypeDB.GetSingleActionTypeDB(ActionTypeEntityParam));
			
			return o;
		}

		public  List<ActionTypeEntity> GetAll()
		{
			List<ActionTypeEntity> lst = new List<ActionTypeEntity>();
			//string key = "ActionType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ActionTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetActionTypeCollectionFromActionTypeDBList(
				_ActionTypeDB.GetAllActionTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<ActionTypeEntity> GetAllIsActive()
        {
            List<ActionTypeEntity> lst = new List<ActionTypeEntity>();
            //string key = "ActionType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ActionTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetActionTypeCollectionFromActionTypeDBList(
            _ActionTypeDB.GetAllActionTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<ActionTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ActionType_List_Page_" + currentPage ;
			//string countKey = "ActionType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ActionTypeEntity> lst = new List<ActionTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ActionTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetActionTypeCollectionFromActionTypeDBList(
				_ActionTypeDB.GetPageActionTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ActionTypeEntity GetActionTypeFromActionTypeDB(ActionTypeEntity o)
		{
	if(o == null)
                return null;
			ActionTypeEntity ret = new ActionTypeEntity(o.ActionTypeId ,o.ActionTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ActionTypeEntity> GetActionTypeCollectionFromActionTypeDBList( List<ActionTypeEntity> lst)
		{
			List<ActionTypeEntity> RetLst = new List<ActionTypeEntity>();
			foreach(ActionTypeEntity o in lst)
			{
				RetLst.Add(GetActionTypeFromActionTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}
