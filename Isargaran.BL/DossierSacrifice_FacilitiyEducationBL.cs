﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات خدمات و تسهیلات آموزشی>
    /// </summary>


    public class DossierSacrifice_FacilitiyEducationBL
    {
        private readonly DossierSacrifice_FacilitiyEducationDB _DossierSacrifice_FacilitiyEducationDB;

        public DossierSacrifice_FacilitiyEducationBL()
        {
            _DossierSacrifice_FacilitiyEducationDB = new DossierSacrifice_FacilitiyEducationDB();
        }

        public void Add(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam,
            out Guid DossierSacrifice_FacilitiyEducationId)
        {
            _DossierSacrifice_FacilitiyEducationDB.AddDossierSacrifice_FacilitiyEducationDB(
                DossierSacrifice_FacilitiyEducationEntityParam, out DossierSacrifice_FacilitiyEducationId);
        }

        public void Update(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            _DossierSacrifice_FacilitiyEducationDB.UpdateDossierSacrifice_FacilitiyEducationDB(
                DossierSacrifice_FacilitiyEducationEntityParam);
        }

        public void Delete(DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            _DossierSacrifice_FacilitiyEducationDB.DeleteDossierSacrifice_FacilitiyEducationDB(
                DossierSacrifice_FacilitiyEducationEntityParam);
        }

        public DossierSacrifice_FacilitiyEducationEntity GetSingleById(
            DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            DossierSacrifice_FacilitiyEducationEntity o = GetDossierSacrifice_FacilitiyEducationFromDossierSacrifice_FacilitiyEducationDB
                (
                    _DossierSacrifice_FacilitiyEducationDB.GetSingleDossierSacrifice_FacilitiyEducationDB(
                        DossierSacrifice_FacilitiyEducationEntityParam));

            return o;
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetAll()
        {
            List<DossierSacrifice_FacilitiyEducationEntity> lst = new List<DossierSacrifice_FacilitiyEducationEntity>();
            //string key = "DossierSacrifice_FacilitiyEducation_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilitiyEducationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilitiyEducationCollectionFromDossierSacrifice_FacilitiyEducationDBList(
                _DossierSacrifice_FacilitiyEducationDB.GetAllDossierSacrifice_FacilitiyEducationDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_FacilitiyEducationEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_FacilitiyEducation_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_FacilitiyEducation_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_FacilitiyEducationEntity> lst = new List<DossierSacrifice_FacilitiyEducationEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_FacilitiyEducationEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_FacilitiyEducationCollectionFromDossierSacrifice_FacilitiyEducationDBList(
                _DossierSacrifice_FacilitiyEducationDB.GetPageDossierSacrifice_FacilitiyEducationDB(pageSize,
                    currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_FacilitiyEducationEntity
            GetDossierSacrifice_FacilitiyEducationFromDossierSacrifice_FacilitiyEducationDB(
            DossierSacrifice_FacilitiyEducationEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_FacilitiyEducationEntity ret =
                new DossierSacrifice_FacilitiyEducationEntity(o.DossierSacrifice_FacilitiyEducationId,
                    o.DossierSacrificeId, o.EucationDegreeId, o.StartDate, o.EndDate, o.PersonId, o.CashAmount,
                    o.Description, o.CreationDate, o.ModificationDate,o.FullName,o.EducationSystemId);
            return ret;
        }

        private List<DossierSacrifice_FacilitiyEducationEntity>
            GetDossierSacrifice_FacilitiyEducationCollectionFromDossierSacrifice_FacilitiyEducationDBList(
            List<DossierSacrifice_FacilitiyEducationEntity> lst)
        {
            List<DossierSacrifice_FacilitiyEducationEntity> RetLst =
                new List<DossierSacrifice_FacilitiyEducationEntity>();
            foreach (DossierSacrifice_FacilitiyEducationEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_FacilitiyEducationFromDossierSacrifice_FacilitiyEducationDB(o));
            }
            return RetLst;

        }




        public List<DossierSacrifice_FacilitiyEducationEntity>
            GetDossierSacrifice_FacilitiyEducationCollectionByDossierSacrifice(
            DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            return
                GetDossierSacrifice_FacilitiyEducationCollectionFromDossierSacrifice_FacilitiyEducationDBList(
                    _DossierSacrifice_FacilitiyEducationDB
                        .GetDossierSacrifice_FacilitiyEducationDBCollectionByDossierSacrificeDB(DossierSacrifice_FacilitiyEducationEntityParam));
        }

        public List<DossierSacrifice_FacilitiyEducationEntity>
            GetDossierSacrifice_FacilitiyEducationCollectionByEducationDegree(
            DossierSacrifice_FacilitiyEducationEntity DossierSacrifice_FacilitiyEducationEntityParam)
        {
            return
                GetDossierSacrifice_FacilitiyEducationCollectionFromDossierSacrifice_FacilitiyEducationDBList(
                    _DossierSacrifice_FacilitiyEducationDB
                        .GetDossierSacrifice_FacilitiyEducationDBCollectionByEducationDegreeDB(DossierSacrifice_FacilitiyEducationEntityParam));
        }



    }

}
