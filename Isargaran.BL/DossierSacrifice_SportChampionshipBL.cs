﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{

    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/27>
    // Description:	<رشته ورزشی پرونده ایثارگر>
    /// </summary>
	public class DossierSacrifice_SportChampionshipBL 
	{	
	  	 private readonly DossierSacrifice_SportChampionshipDB _DossierSacrifice_SportChampionshipDB;					
			
		public DossierSacrifice_SportChampionshipBL()
		{
			_DossierSacrifice_SportChampionshipDB = new DossierSacrifice_SportChampionshipDB();
		}			
	
		public  void Add(DossierSacrifice_SportChampionshipEntity  DossierSacrifice_SportChampionshipEntityParam, out Guid DossierSacrifice_SportChampionshipId)
		{ 
			_DossierSacrifice_SportChampionshipDB.AddDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntityParam,out DossierSacrifice_SportChampionshipId);			
		}

		public  void Update(DossierSacrifice_SportChampionshipEntity  DossierSacrifice_SportChampionshipEntityParam)
		{
			_DossierSacrifice_SportChampionshipDB.UpdateDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntityParam);		
		}

		public  void Delete(DossierSacrifice_SportChampionshipEntity  DossierSacrifice_SportChampionshipEntityParam)
		{
			 _DossierSacrifice_SportChampionshipDB.DeleteDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntityParam);			
		}

		public  DossierSacrifice_SportChampionshipEntity GetSingleById(DossierSacrifice_SportChampionshipEntity  DossierSacrifice_SportChampionshipEntityParam)
		{
			DossierSacrifice_SportChampionshipEntity o = GetDossierSacrifice_SportChampionshipFromDossierSacrifice_SportChampionshipDB(
			_DossierSacrifice_SportChampionshipDB.GetSingleDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_SportChampionshipEntity> GetAll()
		{
			List<DossierSacrifice_SportChampionshipEntity> lst = new List<DossierSacrifice_SportChampionshipEntity>();
			//string key = "DossierSacrifice_SportChampionship_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_SportChampionshipEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_SportChampionshipCollectionFromDossierSacrifice_SportChampionshipDBList(
				_DossierSacrifice_SportChampionshipDB.GetAllDossierSacrifice_SportChampionshipDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_SportChampionshipEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_SportChampionship_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_SportChampionship_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_SportChampionshipEntity> lst = new List<DossierSacrifice_SportChampionshipEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_SportChampionshipEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_SportChampionshipCollectionFromDossierSacrifice_SportChampionshipDBList(
				_DossierSacrifice_SportChampionshipDB.GetPageDossierSacrifice_SportChampionshipDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_SportChampionshipEntity GetDossierSacrifice_SportChampionshipFromDossierSacrifice_SportChampionshipDB(DossierSacrifice_SportChampionshipEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_SportChampionshipEntity ret = new DossierSacrifice_SportChampionshipEntity(o.DossierSacrifice_SportChampionshipId ,o.DossierSacrificeId ,o.SportFieldId ,o.PointOrMedal ,o.CompetitionDate ,o.CompetitionTitle ,o.CompetitionLocation ,o.PersonId ,o.Description ,o.CreationDate ,o.ModificationDate,o.FullName );
			return ret;
		}
		
		private  List<DossierSacrifice_SportChampionshipEntity> GetDossierSacrifice_SportChampionshipCollectionFromDossierSacrifice_SportChampionshipDBList( List<DossierSacrifice_SportChampionshipEntity> lst)
		{
			List<DossierSacrifice_SportChampionshipEntity> RetLst = new List<DossierSacrifice_SportChampionshipEntity>();
			foreach(DossierSacrifice_SportChampionshipEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_SportChampionshipFromDossierSacrifice_SportChampionshipDB(o));
			}
			return RetLst;
			
		}



        public  List<DossierSacrifice_SportChampionshipEntity> GetDossierSacrifice_SportChampionshipCollectionByDossierSacrifice(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam)
{
	return GetDossierSacrifice_SportChampionshipCollectionFromDossierSacrifice_SportChampionshipDBList(_DossierSacrifice_SportChampionshipDB.GetDossierSacrifice_SportChampionshipDBCollectionByDossierSacrificeDB(DossierSacrifice_SportChampionshipEntityParam));
}

        public  List<DossierSacrifice_SportChampionshipEntity> GetDossierSacrifice_SportChampionshipCollectionBySportField(DossierSacrifice_SportChampionshipEntity DossierSacrifice_SportChampionshipEntityParam)
{
	return GetDossierSacrifice_SportChampionshipCollectionFromDossierSacrifice_SportChampionshipDBList(_DossierSacrifice_SportChampionshipDB.GetDossierSacrifice_SportChampionshipDBCollectionBySportFieldDB(DossierSacrifice_SportChampionshipEntityParam));
}



}

}
