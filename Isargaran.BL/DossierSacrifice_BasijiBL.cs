﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <پرونده  بسیجی ایثارگر>
    /// </summary>

    public class DossierSacrifice_BasijiBL
    {
        private readonly DossierSacrifice_BasijiDB _DossierSacrifice_BasijiDB;

        public DossierSacrifice_BasijiBL()
        {
            _DossierSacrifice_BasijiDB = new DossierSacrifice_BasijiDB();
        }

        public void Add(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam,
                        out Guid DossierSacrifice_BasijiId)
        {
            _DossierSacrifice_BasijiDB.AddDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntityParam,
                                                                    out DossierSacrifice_BasijiId);
        }

        public void Update(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            _DossierSacrifice_BasijiDB.UpdateDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntityParam);
        }

        public void Delete(DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            _DossierSacrifice_BasijiDB.DeleteDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntityParam);
        }

        public DossierSacrifice_BasijiEntity GetSingleById(
            DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            DossierSacrifice_BasijiEntity o = GetDossierSacrifice_BasijiFromDossierSacrifice_BasijiDB(
                _DossierSacrifice_BasijiDB.GetSingleDossierSacrifice_BasijiDB(DossierSacrifice_BasijiEntityParam));

            return o;
        }

        public List<DossierSacrifice_BasijiEntity> GetAll()
        {
            List<DossierSacrifice_BasijiEntity> lst = new List<DossierSacrifice_BasijiEntity>();
            //string key = "DossierSacrifice_Basiji_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_BasijiEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
                _DossierSacrifice_BasijiDB.GetAllDossierSacrifice_BasijiDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_BasijiEntity> GetAllPaging(int currentPage, int pageSize,
                                                                string

                                                                    sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Basiji_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Basiji_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_BasijiEntity> lst = new List<DossierSacrifice_BasijiEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_BasijiEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
                _DossierSacrifice_BasijiDB.GetPageDossierSacrifice_BasijiDB(pageSize, currentPage,
                                                                            whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_BasijiEntity GetDossierSacrifice_BasijiFromDossierSacrifice_BasijiDB(
            DossierSacrifice_BasijiEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_BasijiEntity ret = new DossierSacrifice_BasijiEntity(o.DossierSacrifice_BasijiId,
                                                                                  o.DossierSacrificeId, o.IsBasiji,
                                                                                  o.MembershipDate,
                                                                                  o.BasijiMembershipTypeId,
                                                                                  o.BasijiCategoryId, o.BasijiCode,
                                                                                  o.BasijiCardNo,
                                                                                  o.InsuranceSerialNumberCard,
                                                                                  o.InsuranceDateCard, o.BasijiBaseId,
                                                                                  o.CityId, o.VillageTitle,
                                                                                  o.SectionTitle, o.ZipCode, o.Address,
                                                                                  o.Email, o.WebAddress, o.TelNo,
                                                                                  o.FaxNo, o.StartDate, o.EndDate,
                                                                                  o.CreationDate, o.ModificationDate,o.ProvinceId);
            return ret;
        }

        private List<DossierSacrifice_BasijiEntity>
            GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
            List<DossierSacrifice_BasijiEntity> lst)
        {
            List<DossierSacrifice_BasijiEntity> RetLst = new List<DossierSacrifice_BasijiEntity>();
            foreach (DossierSacrifice_BasijiEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_BasijiFromDossierSacrifice_BasijiDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiCollectionByBasijiBase(
            DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            return
                GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
                    _DossierSacrifice_BasijiDB.GetDossierSacrifice_BasijiDBCollectionByBasijiBaseDB(
                        DossierSacrifice_BasijiEntityParam));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiCollectionByBasijiCategory(
            DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            return
                GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
                    _DossierSacrifice_BasijiDB.GetDossierSacrifice_BasijiDBCollectionByBasijiCategoryDB(
                        DossierSacrifice_BasijiEntityParam));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiCollectionByBasijiMembershipType(
            DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            return
                GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
                    _DossierSacrifice_BasijiDB.GetDossierSacrifice_BasijiDBCollectionByBasijiMembershipTypeDB(
                        DossierSacrifice_BasijiEntityParam));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiCollectionByCity(
            DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            return
                GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
                    _DossierSacrifice_BasijiDB.GetDossierSacrifice_BasijiDBCollectionByCityDB(
                        DossierSacrifice_BasijiEntityParam));
        }

        public List<DossierSacrifice_BasijiEntity> GetDossierSacrifice_BasijiCollectionByDossierSacrifice(
            DossierSacrifice_BasijiEntity DossierSacrifice_BasijiEntityParam)
        {
            return
                GetDossierSacrifice_BasijiCollectionFromDossierSacrifice_BasijiDBList(
                    _DossierSacrifice_BasijiDB.GetDossierSacrifice_BasijiDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_BasijiEntityParam));
        }


    }

}
