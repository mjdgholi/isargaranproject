﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <اطلاعات زبان های خارجی>
    /// </summary>
    public class DossierSacrifice_ForeignLanguageBL
    {
        private readonly DossierSacrifice_ForeignLanguageDB _DossierSacrifice_ForeignLanguageDB;

        public DossierSacrifice_ForeignLanguageBL()
        {
            _DossierSacrifice_ForeignLanguageDB = new DossierSacrifice_ForeignLanguageDB();
        }

        public void Add(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam, out Guid DossierSacrifice_ForeignLanguageId)
        {
            _DossierSacrifice_ForeignLanguageDB.AddDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntityParam, out DossierSacrifice_ForeignLanguageId);
        }

        public void Update(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam)
        {
            _DossierSacrifice_ForeignLanguageDB.UpdateDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntityParam);
        }

        public void Delete(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam)
        {
            _DossierSacrifice_ForeignLanguageDB.DeleteDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntityParam);
        }

        public DossierSacrifice_ForeignLanguageEntity GetSingleById(DossierSacrifice_ForeignLanguageEntity DossierSacrifice_ForeignLanguageEntityParam)
        {
            DossierSacrifice_ForeignLanguageEntity o = GetDossierSacrifice_ForeignLanguageFromDossierSacrifice_ForeignLanguageDB(
                _DossierSacrifice_ForeignLanguageDB.GetSingleDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntityParam));

            return o;
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetAll()
        {
            List<DossierSacrifice_ForeignLanguageEntity> lst = new List<DossierSacrifice_ForeignLanguageEntity>();
            //string key = "DossierSacrifice_ForeignLanguage_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ForeignLanguageEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(
                _DossierSacrifice_ForeignLanguageDB.GetAllDossierSacrifice_ForeignLanguageDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetAllPaging(int currentPage, int pageSize,
                                                                         string

                                                                             sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_ForeignLanguage_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_ForeignLanguage_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_ForeignLanguageEntity> lst = new List<DossierSacrifice_ForeignLanguageEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_ForeignLanguageEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(
                _DossierSacrifice_ForeignLanguageDB.GetPageDossierSacrifice_ForeignLanguageDB(pageSize, currentPage,
                                                                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_ForeignLanguageEntity GetDossierSacrifice_ForeignLanguageFromDossierSacrifice_ForeignLanguageDB(DossierSacrifice_ForeignLanguageEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_ForeignLanguageEntity ret = new DossierSacrifice_ForeignLanguageEntity(o.DossierSacrifice_ForeignLanguageId, o.DossierSacrificeId, o.ForeignLanguageId, o.ListeningSkillId, o.SpeakingSkillId, o.ReadingSkillId, o.WritingSkillId, o.Description, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(List<DossierSacrifice_ForeignLanguageEntity> lst)
        {
            List<DossierSacrifice_ForeignLanguageEntity> RetLst = new List<DossierSacrifice_ForeignLanguageEntity>();
            foreach (DossierSacrifice_ForeignLanguageEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_ForeignLanguageFromDossierSacrifice_ForeignLanguageDB(o));
            }
            return RetLst;

        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageCollectionByDossierSacrifice(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            return GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(_DossierSacrifice_ForeignLanguageDB.GetDossierSacrifice_ForeignLanguageDBCollectionByDossierSacrificeDB(dossierSacrificeForeignLanguageEntity));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageCollectionByForeignLanguage(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            return GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(_DossierSacrifice_ForeignLanguageDB.GetDossierSacrifice_ForeignLanguageDBCollectionByForeignLanguageDB(dossierSacrificeForeignLanguageEntity));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageCollectionByListeningSkill(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            return GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(_DossierSacrifice_ForeignLanguageDB.GetDossierSacrifice_ForeignLanguageDBCollectionByListeningSkillDB(dossierSacrificeForeignLanguageEntity));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageCollectionByReadingSkill(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            return GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(_DossierSacrifice_ForeignLanguageDB.GetDossierSacrifice_ForeignLanguageDBCollectionByReadingSkillDB(dossierSacrificeForeignLanguageEntity));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageCollectionBySpeakingSkill(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            return GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(_DossierSacrifice_ForeignLanguageDB.GetDossierSacrifice_ForeignLanguageDBCollectionBySpeakingSkillDB(dossierSacrificeForeignLanguageEntity));
        }

        public List<DossierSacrifice_ForeignLanguageEntity> GetDossierSacrifice_ForeignLanguageCollectionByWritingSkill(DossierSacrifice_ForeignLanguageEntity dossierSacrificeForeignLanguageEntity)
        {
            return GetDossierSacrifice_ForeignLanguageCollectionFromDossierSacrifice_ForeignLanguageDBList(_DossierSacrifice_ForeignLanguageDB.GetDossierSacrifice_ForeignLanguageDBCollectionByWritingSkillDB(dossierSacrificeForeignLanguageEntity));
        }
    }
}