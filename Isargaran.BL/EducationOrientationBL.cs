﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/12>
    /// Description: <گرایش تحصیلی>
    /// </summary>


	public class EducationOrientationBL 
	{	
	  	 private readonly EducationOrientationDB _EducationOrientationDB;					
			
		public EducationOrientationBL()
		{
			_EducationOrientationDB = new EducationOrientationDB();
		}			
	
		public  void Add(EducationOrientationEntity  EducationOrientationEntityParam, out Guid EducationOrientationId)
		{ 
			_EducationOrientationDB.AddEducationOrientationDB(EducationOrientationEntityParam,out EducationOrientationId);			
		}

		public  void Update(EducationOrientationEntity  EducationOrientationEntityParam)
		{
			_EducationOrientationDB.UpdateEducationOrientationDB(EducationOrientationEntityParam);		
		}

		public  void Delete(EducationOrientationEntity  EducationOrientationEntityParam)
		{
			 _EducationOrientationDB.DeleteEducationOrientationDB(EducationOrientationEntityParam);			
		}

		public  EducationOrientationEntity GetSingleById(EducationOrientationEntity  EducationOrientationEntityParam)
		{
			EducationOrientationEntity o = GetEducationOrientationFromEducationOrientationDB(
			_EducationOrientationDB.GetSingleEducationOrientationDB(EducationOrientationEntityParam));
			
			return o;
		}

		public  List<EducationOrientationEntity> GetAll()
		{
			List<EducationOrientationEntity> lst = new List<EducationOrientationEntity>();
			//string key = "EducationOrientation_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationOrientationEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetEducationOrientationCollectionFromEducationOrientationDBList(
				_EducationOrientationDB.GetAllEducationOrientationDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<EducationOrientationEntity> GetAllIsActive()
        {
            List<EducationOrientationEntity> lst = new List<EducationOrientationEntity>();
            //string key = "EducationOrientation_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EducationOrientationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEducationOrientationCollectionFromEducationOrientationDBList(
            _EducationOrientationDB.GetAllEducationOrientationIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<EducationOrientationEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "EducationOrientation_List_Page_" + currentPage ;
			//string countKey = "EducationOrientation_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<EducationOrientationEntity> lst = new List<EducationOrientationEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EducationOrientationEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetEducationOrientationCollectionFromEducationOrientationDBList(
				_EducationOrientationDB.GetPageEducationOrientationDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  EducationOrientationEntity GetEducationOrientationFromEducationOrientationDB(EducationOrientationEntity o)
		{
	if(o == null)
                return null;
			EducationOrientationEntity ret = new EducationOrientationEntity(o.EducationOrientationId ,o.EducationOrientationPersianTitle ,o.EducationOrientationEnglishTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<EducationOrientationEntity> GetEducationOrientationCollectionFromEducationOrientationDBList( List<EducationOrientationEntity> lst)
		{
			List<EducationOrientationEntity> RetLst = new List<EducationOrientationEntity>();
			foreach(EducationOrientationEntity o in lst)
			{
				RetLst.Add(GetEducationOrientationFromEducationOrientationDB(o));
			}
			return RetLst;
			
		}
     

				
		
	}

}

