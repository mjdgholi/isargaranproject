﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <زبان خارجی>
    /// </summary>
    public class ForeignLanguageBL
    {
        private readonly ForeignLanguageDB _ForeignLanguageDB;

        public ForeignLanguageBL()
        {
            _ForeignLanguageDB = new ForeignLanguageDB();
        }

        public void Add(ForeignLanguageEntity ForeignLanguageEntityParam, out Guid ForeignLanguageId)
        {
            _ForeignLanguageDB.AddForeignLanguageDB(ForeignLanguageEntityParam, out ForeignLanguageId);
        }

        public void Update(ForeignLanguageEntity ForeignLanguageEntityParam)
        {
            _ForeignLanguageDB.UpdateForeignLanguageDB(ForeignLanguageEntityParam);
        }

        public void Delete(ForeignLanguageEntity ForeignLanguageEntityParam)
        {
            _ForeignLanguageDB.DeleteForeignLanguageDB(ForeignLanguageEntityParam);
        }

        public ForeignLanguageEntity GetSingleById(ForeignLanguageEntity ForeignLanguageEntityParam)
        {
            ForeignLanguageEntity o = GetForeignLanguageFromForeignLanguageDB(
                _ForeignLanguageDB.GetSingleForeignLanguageDB(ForeignLanguageEntityParam));

            return o;
        }

        public List<ForeignLanguageEntity> GetAll()
        {
            List<ForeignLanguageEntity> lst = new List<ForeignLanguageEntity>();
            //string key = "ForeignLanguage_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ForeignLanguageEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetForeignLanguageCollectionFromForeignLanguageDBList(
                _ForeignLanguageDB.GetAllForeignLanguageDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ForeignLanguageEntity> GetAllIsActive()
        {
            return _ForeignLanguageDB.GetAllIsActiveForeignLanguageDB();
        }

        public List<ForeignLanguageEntity> GetAllPaging(int currentPage, int pageSize,
                                                        string

                                                            sortExpression, out int count, string whereClause)
        {
            //string key = "ForeignLanguage_List_Page_" + currentPage ;
            //string countKey = "ForeignLanguage_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ForeignLanguageEntity> lst = new List<ForeignLanguageEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ForeignLanguageEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetForeignLanguageCollectionFromForeignLanguageDBList(
                _ForeignLanguageDB.GetPageForeignLanguageDB(pageSize, currentPage,
                                                            whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ForeignLanguageEntity GetForeignLanguageFromForeignLanguageDB(ForeignLanguageEntity o)
        {
            if (o == null)
                return null;
            ForeignLanguageEntity ret = new ForeignLanguageEntity(o.ForeignLanguageId, o.ForeignLanguagePersianTitle, o.ForeignLanguageEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<ForeignLanguageEntity> GetForeignLanguageCollectionFromForeignLanguageDBList(List<ForeignLanguageEntity> lst)
        {
            List<ForeignLanguageEntity> RetLst = new List<ForeignLanguageEntity>();
            foreach (ForeignLanguageEntity o in lst)
            {
                RetLst.Add(GetForeignLanguageFromForeignLanguageDB(o));
            }
            return RetLst;

        }


    }
}