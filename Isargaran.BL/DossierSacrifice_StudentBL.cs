﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <اطلاعات تحصیلی>
    /// </summary>
    public class DossierSacrifice_StudentBL
    {
        private readonly DossierSacrifice_StudentDB _DossierSacrifice_StudentDB;

        public DossierSacrifice_StudentBL()
        {
            _DossierSacrifice_StudentDB = new DossierSacrifice_StudentDB();
        }

        public void Add(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam, out Guid DossierSacrifice_StudentId)
        {
            _DossierSacrifice_StudentDB.AddDossierSacrifice_StudentDB(DossierSacrifice_StudentEntityParam, out DossierSacrifice_StudentId);
        }

        public void Update(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam)
        {
            _DossierSacrifice_StudentDB.UpdateDossierSacrifice_StudentDB(DossierSacrifice_StudentEntityParam);
        }

        public void Delete(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam)
        {
            _DossierSacrifice_StudentDB.DeleteDossierSacrifice_StudentDB(DossierSacrifice_StudentEntityParam);
        }

        public DossierSacrifice_StudentEntity GetSingleById(DossierSacrifice_StudentEntity DossierSacrifice_StudentEntityParam)
        {
            DossierSacrifice_StudentEntity o = GetDossierSacrifice_StudentFromDossierSacrifice_StudentDB(
                _DossierSacrifice_StudentDB.GetSingleDossierSacrifice_StudentDB(DossierSacrifice_StudentEntityParam));

            return o;
        }

        public List<DossierSacrifice_StudentEntity> GetAll()
        {
            List<DossierSacrifice_StudentEntity> lst = new List<DossierSacrifice_StudentEntity>();
            //string key = "DossierSacrifice_Student_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_StudentEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(
                _DossierSacrifice_StudentDB.GetAllDossierSacrifice_StudentDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_StudentEntity> GetAllPaging(int currentPage, int pageSize,
                                                                 string

                                                                     sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Student_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Student_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_StudentEntity> lst = new List<DossierSacrifice_StudentEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_StudentEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(
                _DossierSacrifice_StudentDB.GetPageDossierSacrifice_StudentDB(pageSize, currentPage,
                                                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private static DossierSacrifice_StudentEntity GetDossierSacrifice_StudentFromDossierSacrifice_StudentDB(DossierSacrifice_StudentEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_StudentEntity ret = new DossierSacrifice_StudentEntity(o.DossierSacrifice_StudentId, o.DossierSacrificeId, o.AdmissionTypeId, o.EducationCenterId, o.EucationDegreeId, o.EducationCourseId, o.EducationOrientationId, o.CityId, o.EducationModeId, o.StartDate, o.EndDate, o.StudentNo, o.GraduateTypeId, o.InChargeOfEducation, o.TechnicalDescription, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(List<DossierSacrifice_StudentEntity> lst)
        {
            List<DossierSacrifice_StudentEntity> RetLst = new List<DossierSacrifice_StudentEntity>();
            foreach (DossierSacrifice_StudentEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_StudentFromDossierSacrifice_StudentDB(o));
            }
            return RetLst;

        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentCollectionByAdmissionType(Guid AdmissionTypeId)
        {
            return GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(DossierSacrifice_StudentDB.GetDossierSacrifice_StudentDBCollectionByAdmissionTypeDB(AdmissionTypeId));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentCollectionByDossierSacrifice(Guid DossierSacrificeId)
        {
            return GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(DossierSacrifice_StudentDB.GetDossierSacrifice_StudentDBCollectionByDossierSacrificeDB(DossierSacrificeId));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentCollectionByEducationCenter(Guid EducationCenterId)
        {
            return GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(DossierSacrifice_StudentDB.GetDossierSacrifice_StudentDBCollectionByEducationCenterDB(EducationCenterId));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentCollectionByEducationCourse(Guid EducationCourseId)
        {
            return GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(DossierSacrifice_StudentDB.GetDossierSacrifice_StudentDBCollectionByEducationCourseDB(EducationCourseId));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentCollectionByEducationDegree(Guid EucationDegreeId)
        {
            return GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(DossierSacrifice_StudentDB.GetDossierSacrifice_StudentDBCollectionByEducationDegreeDB(EucationDegreeId));
        }

        public static List<DossierSacrifice_StudentEntity> GetDossierSacrifice_StudentCollectionByEducationOrientation(Guid EducationOrientationId)
        {
            return GetDossierSacrifice_StudentCollectionFromDossierSacrifice_StudentDBList(DossierSacrifice_StudentDB.GetDossierSacrifice_StudentDBCollectionByEducationOrientationDB(EducationOrientationId));
        }
    }
}