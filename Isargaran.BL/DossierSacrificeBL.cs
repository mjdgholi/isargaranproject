﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <پرونده ایثارگری>
    /// </summary>

   
	public class DossierSacrificeBL 
	{	
	  	 private readonly DossierSacrificeDB _DossierSacrificeDB;					
			
		public DossierSacrificeBL()
		{
			_DossierSacrificeDB = new DossierSacrificeDB();
		}
        private void RequiredFieldValidation(DossierSacrificeEntity dossierSacrificeEntityParam)
        {
            if (dossierSacrificeEntityParam.HasInsurance && dossierSacrificeEntityParam.InsuranceNo.Trim() == "")
            {
                throw new ItcApplicationErrorManagerException("شماره بیمه را وارد نمایید");
            }
            if (dossierSacrificeEntityParam.HasPassport && dossierSacrificeEntityParam.PassportNo.Trim() == "")
            {
                throw new ItcApplicationErrorManagerException("شماره گذرنامه را وارد نمایید");
            }
            if (dossierSacrificeEntityParam.HasDependentSacrifice && Guid.Parse(dossierSacrificeEntityParam.DependentTypeId.ToString()) == new Guid())
            {
                throw new ItcApplicationErrorManagerException("منسومین به ایثارگران را وارد نمایید");
            }
        }
	
		public  void Add(DossierSacrificeEntity  DossierSacrificeEntityParam, out Guid DossierSacrificeId)
		{
            RequiredFieldValidation(DossierSacrificeEntityParam);
			_DossierSacrificeDB.AddDossierSacrificeDB(DossierSacrificeEntityParam,out DossierSacrificeId);			
		}

		public  void Update(DossierSacrificeEntity  DossierSacrificeEntityParam)
		{
            RequiredFieldValidation(DossierSacrificeEntityParam);
			_DossierSacrificeDB.UpdateDossierSacrificeDB(DossierSacrificeEntityParam);		
		}

		public  void Delete(DossierSacrificeEntity  DossierSacrificeEntityParam)
		{
			 _DossierSacrificeDB.DeleteDossierSacrificeDB(DossierSacrificeEntityParam);			
		}

		public  DossierSacrificeEntity GetSingleById(DossierSacrificeEntity  DossierSacrificeEntityParam)
		{
			DossierSacrificeEntity o = GetDossierSacrificeFromDossierSacrificeDB(
			_DossierSacrificeDB.GetSingleDossierSacrificeDB(DossierSacrificeEntityParam));
			
			return o;
		}

		public  List<DossierSacrificeEntity> GetAll()
		{
			List<DossierSacrificeEntity> lst = new List<DossierSacrificeEntity>();
			//string key = "DossierSacrifice_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrificeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrificeCollectionFromDossierSacrificeDBList(
				_DossierSacrificeDB.GetAllDossierSacrificeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrificeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrificeEntity> lst = new List<DossierSacrificeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrificeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrificeCollectionFromDossierSacrificeDBList(
				_DossierSacrificeDB.GetPageDossierSacrificeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrificeEntity GetDossierSacrificeFromDossierSacrificeDB(DossierSacrificeEntity o)
		{
	if(o == null)
                return null;
			DossierSacrificeEntity ret = new DossierSacrificeEntity(o.DossierSacrificeId ,o.DossierSacrificeNo ,o.SacrificeNo ,o.PersonId ,o.PersonFirstName,o.PersonLastName,o.DossierStatusId ,o.NaturalizationId ,o.NationalityId ,o.FaithId ,o.ReligionId ,o.MarriageStatusId ,o.MarriageDate ,o.HasInsurance ,o.InsuranceNo ,o.HasPassport ,o.PassportNo ,o.Email ,o.HomeAddress ,o.HomePhoneNo ,o.HomeFaxNo ,o.CellphoneNo ,o.ZipCode ,o.CityId ,o.HasDependentSacrifice ,o.DependentTypeId ,o.HasDossierSacrifice ,o.ImagePath ,o.CreationDate ,o.ModificationDate ,o.IsActive,o.ProvinceId );
			return ret;
		}
		
		private  List<DossierSacrificeEntity> GetDossierSacrificeCollectionFromDossierSacrificeDBList( List<DossierSacrificeEntity> lst)
		{
			List<DossierSacrificeEntity> RetLst = new List<DossierSacrificeEntity>();
			foreach(DossierSacrificeEntity o in lst)
			{
				RetLst.Add(GetDossierSacrificeFromDossierSacrificeDB(o));
			}
			return RetLst;
			
		}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByCity(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByCityDB(DossierSacrificeEntityParam));
}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByCountry(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByCountryDB(DossierSacrificeEntityParam));
}


        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByDependentType(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByDependentTypeDB(DossierSacrificeEntityParam));
}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByDossierStatus(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByDossierStatusDB(DossierSacrificeEntityParam));
}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByFaith(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByFaithDB(DossierSacrificeEntityParam));
}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByMarriageStatus(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByMarriageStatusDB(DossierSacrificeEntityParam));
}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByNationality(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByNationalityDB(DossierSacrificeEntityParam));
}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByPerson(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByPersonDB(DossierSacrificeEntityParam));
}

        public  List<DossierSacrificeEntity> GetDossierSacrificeCollectionByReligion(DossierSacrificeEntity DossierSacrificeEntityParam)
{
    return GetDossierSacrificeCollectionFromDossierSacrificeDBList(_DossierSacrificeDB.GetDossierSacrificeDBCollectionByReligionDB(DossierSacrificeEntityParam));
}





}



}


