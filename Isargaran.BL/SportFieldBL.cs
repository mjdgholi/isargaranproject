﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/27>
    // Description:	<رشته ورزشی>
    /// </summary>

	public class SportFieldBL 
	{	
	  	 private readonly SportFieldDB _SportFieldDB;					
			
		public SportFieldBL()
		{
			_SportFieldDB = new SportFieldDB();
		}			
	
		public  void Add(SportFieldEntity  SportFieldEntityParam, out Guid SportFieldId)
		{ 
			_SportFieldDB.AddSportFieldDB(SportFieldEntityParam,out SportFieldId);			
		}

		public  void Update(SportFieldEntity  SportFieldEntityParam)
		{
			_SportFieldDB.UpdateSportFieldDB(SportFieldEntityParam);		
		}

		public  void Delete(SportFieldEntity  SportFieldEntityParam)
		{
			 _SportFieldDB.DeleteSportFieldDB(SportFieldEntityParam);			
		}

		public  SportFieldEntity GetSingleById(SportFieldEntity  SportFieldEntityParam)
		{
			SportFieldEntity o = GetSportFieldFromSportFieldDB(
			_SportFieldDB.GetSingleSportFieldDB(SportFieldEntityParam));
			
			return o;
		}

		public  List<SportFieldEntity> GetAll()
		{
			List<SportFieldEntity> lst = new List<SportFieldEntity>();
			//string key = "SportField_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SportFieldEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSportFieldCollectionFromSportFieldDBList(
				_SportFieldDB.GetAllSportFieldDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<SportFieldEntity> GetAllIsActive()
        {
            List<SportFieldEntity> lst = new List<SportFieldEntity>();
            //string key = "SportField_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SportFieldEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSportFieldCollectionFromSportFieldDBList(
            _SportFieldDB.GetAllSportFieldIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<SportFieldEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "SportField_List_Page_" + currentPage ;
			//string countKey = "SportField_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<SportFieldEntity> lst = new List<SportFieldEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SportFieldEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetSportFieldCollectionFromSportFieldDBList(
				_SportFieldDB.GetPageSportFieldDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  SportFieldEntity GetSportFieldFromSportFieldDB(SportFieldEntity o)
		{
	if(o == null)
                return null;
			SportFieldEntity ret = new SportFieldEntity(o.SportFieldId ,o.SportFieldPersianTitle ,o.SportFieldEnglishTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<SportFieldEntity> GetSportFieldCollectionFromSportFieldDBList( List<SportFieldEntity> lst)
		{
			List<SportFieldEntity> RetLst = new List<SportFieldEntity>();
			foreach(SportFieldEntity o in lst)
			{
				RetLst.Add(GetSportFieldFromSportFieldDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}
