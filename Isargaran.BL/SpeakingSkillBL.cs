﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <مهارت گفتاری>
    /// </summary>
    public class SpeakingSkillBL
    {
        private readonly SpeakingSkillDB _SpeakingSkillDB;

        public SpeakingSkillBL()
        {
            _SpeakingSkillDB = new SpeakingSkillDB();
        }

        public void Add(SpeakingSkillEntity SpeakingSkillEntityParam, out Guid SpeakingSkillId)
        {
            _SpeakingSkillDB.AddSpeakingSkillDB(SpeakingSkillEntityParam, out SpeakingSkillId);
        }

        public void Update(SpeakingSkillEntity SpeakingSkillEntityParam)
        {
            _SpeakingSkillDB.UpdateSpeakingSkillDB(SpeakingSkillEntityParam);
        }

        public void Delete(SpeakingSkillEntity SpeakingSkillEntityParam)
        {
            _SpeakingSkillDB.DeleteSpeakingSkillDB(SpeakingSkillEntityParam);
        }

        public SpeakingSkillEntity GetSingleById(SpeakingSkillEntity SpeakingSkillEntityParam)
        {
            SpeakingSkillEntity o = GetSpeakingSkillFromSpeakingSkillDB(
                _SpeakingSkillDB.GetSingleSpeakingSkillDB(SpeakingSkillEntityParam));

            return o;
        }

        public List<SpeakingSkillEntity> GetAll()
        {
            List<SpeakingSkillEntity> lst = new List<SpeakingSkillEntity>();
            //string key = "SpeakingSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SpeakingSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSpeakingSkillCollectionFromSpeakingSkillDBList(
                _SpeakingSkillDB.GetAllSpeakingSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<SpeakingSkillEntity> GetAllIsActive()
        {
            List<SpeakingSkillEntity> lst = new List<SpeakingSkillEntity>();
            //string key = "SpeakingSkill_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SpeakingSkillEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSpeakingSkillCollectionFromSpeakingSkillDBList(
                _SpeakingSkillDB.GetAllIsActiveSpeakingSkillDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<SpeakingSkillEntity> GetAllPaging(int currentPage, int pageSize,
                                                      string

                                                          sortExpression, out int count, string whereClause)
        {
            //string key = "SpeakingSkill_List_Page_" + currentPage ;
            //string countKey = "SpeakingSkill_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<SpeakingSkillEntity> lst = new List<SpeakingSkillEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SpeakingSkillEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetSpeakingSkillCollectionFromSpeakingSkillDBList(
                _SpeakingSkillDB.GetPageSpeakingSkillDB(pageSize, currentPage,
                                                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private SpeakingSkillEntity GetSpeakingSkillFromSpeakingSkillDB(SpeakingSkillEntity o)
        {
            if (o == null)
                return null;
            SpeakingSkillEntity ret = new SpeakingSkillEntity(o.SpeakingSkillId, o.SpeakingSkillPersianTitle, o.SpeakingSkillEnglishTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<SpeakingSkillEntity> GetSpeakingSkillCollectionFromSpeakingSkillDBList(List<SpeakingSkillEntity> lst)
        {
            List<SpeakingSkillEntity> RetLst = new List<SpeakingSkillEntity>();
            foreach (SpeakingSkillEntity o in lst)
            {
                RetLst.Add(GetSpeakingSkillFromSpeakingSkillDB(o));
            }
            return RetLst;

       }
    }
}