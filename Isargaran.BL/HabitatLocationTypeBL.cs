﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/01/17>
    // Description:	<موقیعت محل اقامت>
    /// </summary>

	public class HabitatLocationTypeBL 
	{	
	  	 private readonly HabitatLocationTypeDB _HabitatLocationTypeDB;					
			
		public HabitatLocationTypeBL()
		{
			_HabitatLocationTypeDB = new HabitatLocationTypeDB();
		}			
	
		public  void Add(HabitatLocationTypeEntity  HabitatLocationTypeEntityParam, out Guid HabitatLocationTypeId)
		{ 
			_HabitatLocationTypeDB.AddHabitatLocationTypeDB(HabitatLocationTypeEntityParam,out HabitatLocationTypeId);			
		}

		public  void Update(HabitatLocationTypeEntity  HabitatLocationTypeEntityParam)
		{
			_HabitatLocationTypeDB.UpdateHabitatLocationTypeDB(HabitatLocationTypeEntityParam);		
		}

		public  void Delete(HabitatLocationTypeEntity  HabitatLocationTypeEntityParam)
		{
			 _HabitatLocationTypeDB.DeleteHabitatLocationTypeDB(HabitatLocationTypeEntityParam);			
		}

		public  HabitatLocationTypeEntity GetSingleById(HabitatLocationTypeEntity  HabitatLocationTypeEntityParam)
		{
			HabitatLocationTypeEntity o = GetHabitatLocationTypeFromHabitatLocationTypeDB(
			_HabitatLocationTypeDB.GetSingleHabitatLocationTypeDB(HabitatLocationTypeEntityParam));
			
			return o;
		}

		public  List<HabitatLocationTypeEntity> GetAll()
		{
			List<HabitatLocationTypeEntity> lst = new List<HabitatLocationTypeEntity>();
			//string key = "HabitatLocationType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<HabitatLocationTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetHabitatLocationTypeCollectionFromHabitatLocationTypeDBList(
				_HabitatLocationTypeDB.GetAllHabitatLocationTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<HabitatLocationTypeEntity> GetAllIsActive()
        {
            List<HabitatLocationTypeEntity> lst = new List<HabitatLocationTypeEntity>();
            //string key = "HabitatLocationType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<HabitatLocationTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetHabitatLocationTypeCollectionFromHabitatLocationTypeDBList(
            _HabitatLocationTypeDB.GetAllHabitatLocationTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<HabitatLocationTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "HabitatLocationType_List_Page_" + currentPage ;
			//string countKey = "HabitatLocationType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<HabitatLocationTypeEntity> lst = new List<HabitatLocationTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<HabitatLocationTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetHabitatLocationTypeCollectionFromHabitatLocationTypeDBList(
				_HabitatLocationTypeDB.GetPageHabitatLocationTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  HabitatLocationTypeEntity GetHabitatLocationTypeFromHabitatLocationTypeDB(HabitatLocationTypeEntity o)
		{
	if(o == null)
                return null;
			HabitatLocationTypeEntity ret = new HabitatLocationTypeEntity(o.HabitatLocationTypeId ,o.HabitatLocationTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<HabitatLocationTypeEntity> GetHabitatLocationTypeCollectionFromHabitatLocationTypeDBList( List<HabitatLocationTypeEntity> lst)
		{
			List<HabitatLocationTypeEntity> RetLst = new List<HabitatLocationTypeEntity>();
			foreach(HabitatLocationTypeEntity o in lst)
			{
				RetLst.Add(GetHabitatLocationTypeFromHabitatLocationTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}


