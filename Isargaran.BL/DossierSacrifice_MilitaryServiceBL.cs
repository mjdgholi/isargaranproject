﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/26>
    /// Description: <پرونده خدمات سربازی ایثارگر>
    /// </summary>

    public class DossierSacrifice_MilitaryServiceBL
    {
        private readonly DossierSacrifice_MilitaryServiceDB _DossierSacrifice_MilitaryServiceDB;

        public DossierSacrifice_MilitaryServiceBL()
        {
            _DossierSacrifice_MilitaryServiceDB = new DossierSacrifice_MilitaryServiceDB();
        }

        public void Add(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam,
                        out Guid DossierSacrifice_MilitaryServiceId)
        {
            _DossierSacrifice_MilitaryServiceDB.AddDossierSacrifice_MilitaryServiceDB(
                DossierSacrifice_MilitaryServiceEntityParam, out DossierSacrifice_MilitaryServiceId);
        }

        public void Update(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            _DossierSacrifice_MilitaryServiceDB.UpdateDossierSacrifice_MilitaryServiceDB(
                DossierSacrifice_MilitaryServiceEntityParam);
        }

        public void Delete(DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            _DossierSacrifice_MilitaryServiceDB.DeleteDossierSacrifice_MilitaryServiceDB(
                DossierSacrifice_MilitaryServiceEntityParam);
        }

        public DossierSacrifice_MilitaryServiceEntity GetSingleById(
            DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            DossierSacrifice_MilitaryServiceEntity o = GetDossierSacrifice_MilitaryServiceFromDossierSacrifice_MilitaryServiceDB
                (
                    _DossierSacrifice_MilitaryServiceDB.GetSingleDossierSacrifice_MilitaryServiceDB(
                        DossierSacrifice_MilitaryServiceEntityParam));

            return o;
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetAll()
        {
            List<DossierSacrifice_MilitaryServiceEntity> lst = new List<DossierSacrifice_MilitaryServiceEntity>();
            //string key = "DossierSacrifice_MilitaryService_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_MilitaryServiceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_MilitaryServiceCollectionFromDossierSacrifice_MilitaryServiceDBList(
                _DossierSacrifice_MilitaryServiceDB.GetAllDossierSacrifice_MilitaryServiceDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_MilitaryServiceEntity> GetAllPaging(int currentPage, int pageSize,
                                                                         string

                                                                             sortExpression, out int count,
                                                                         string whereClause)
        {
            //string key = "DossierSacrifice_MilitaryService_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_MilitaryService_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_MilitaryServiceEntity> lst = new List<DossierSacrifice_MilitaryServiceEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_MilitaryServiceEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_MilitaryServiceCollectionFromDossierSacrifice_MilitaryServiceDBList(
                _DossierSacrifice_MilitaryServiceDB.GetPageDossierSacrifice_MilitaryServiceDB(pageSize, currentPage,
                                                                                              whereClause,
                                                                                              sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_MilitaryServiceEntity
            GetDossierSacrifice_MilitaryServiceFromDossierSacrifice_MilitaryServiceDB(
            DossierSacrifice_MilitaryServiceEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_MilitaryServiceEntity ret =
                new DossierSacrifice_MilitaryServiceEntity(o.DossierSacrifice_MilitaryServiceId, o.DossierSacrificeId,
                                                           o.HasMilitaryServiceCard, o.MilitaryServiceLocation,
                                                           o.DistrictHandler, o.ClassifiedNo, o.CardIssueDate, o.CardNo,
                                                           o.MilitaryStatusId, o.CityId, o.VillageTitle, o.SectionTitle,
                                                           o.ZipCode, o.Address, o.Email, o.WebAddress, o.TelNo, o.FaxNo,
                                                           o.StartDate, o.EndDate, o.CreationDate, o.ModificationDate,o.ProvinceId);
            return ret;
        }

        private List<DossierSacrifice_MilitaryServiceEntity>
            GetDossierSacrifice_MilitaryServiceCollectionFromDossierSacrifice_MilitaryServiceDBList(
            List<DossierSacrifice_MilitaryServiceEntity> lst)
        {
            List<DossierSacrifice_MilitaryServiceEntity> RetLst = new List<DossierSacrifice_MilitaryServiceEntity>();
            foreach (DossierSacrifice_MilitaryServiceEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_MilitaryServiceFromDossierSacrifice_MilitaryServiceDB(o));
            }
            return RetLst;

        }

        public List<DossierSacrifice_MilitaryServiceEntity>
            GetDossierSacrifice_MilitaryServiceCollectionByDossierSacrifice(
            DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            return
                GetDossierSacrifice_MilitaryServiceCollectionFromDossierSacrifice_MilitaryServiceDBList(
                    _DossierSacrifice_MilitaryServiceDB.
                        GetDossierSacrifice_MilitaryServiceDBCollectionByDossierSacrificeDB(
                            DossierSacrifice_MilitaryServiceEntityParam));
        }

        public List<DossierSacrifice_MilitaryServiceEntity>
            GetDossierSacrifice_MilitaryServiceCollectionByMilitaryStatus(
            DossierSacrifice_MilitaryServiceEntity DossierSacrifice_MilitaryServiceEntityParam)
        {
            return
                GetDossierSacrifice_MilitaryServiceCollectionFromDossierSacrifice_MilitaryServiceDBList(
                    _DossierSacrifice_MilitaryServiceDB.
                        GetDossierSacrifice_MilitaryServiceDBCollectionByMilitaryStatusDB(
                            DossierSacrifice_MilitaryServiceEntityParam));
        }


    }
}
