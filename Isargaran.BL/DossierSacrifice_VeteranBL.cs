﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/01/30>
    // Description:	<اطلاعات جانبازی>
    /// </summary>


    public class DossierSacrifice_VeteranBL
    {
        private readonly DossierSacrifice_VeteranDB _DossierSacrifice_VeteranDB;

        public DossierSacrifice_VeteranBL()
        {
            _DossierSacrifice_VeteranDB = new DossierSacrifice_VeteranDB();
        }

        public void Add(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam,
                        out Guid DossierSacrifice_VeteranId)
        {
            _DossierSacrifice_VeteranDB.AddDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntityParam,
                                                                      out DossierSacrifice_VeteranId);
        }

        public void Update(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            _DossierSacrifice_VeteranDB.UpdateDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntityParam);
        }

        public void Delete(DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            _DossierSacrifice_VeteranDB.DeleteDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntityParam);
        }

        public DossierSacrifice_VeteranEntity GetSingleById(
            DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            DossierSacrifice_VeteranEntity o = GetDossierSacrifice_VeteranFromDossierSacrifice_VeteranDB(
                _DossierSacrifice_VeteranDB.GetSingleDossierSacrifice_VeteranDB(DossierSacrifice_VeteranEntityParam));

            return o;
        }

        public List<DossierSacrifice_VeteranEntity> GetAll()
        {
            List<DossierSacrifice_VeteranEntity> lst = new List<DossierSacrifice_VeteranEntity>();
            //string key = "DossierSacrifice_Veteran_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_VeteranEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
                _DossierSacrifice_VeteranDB.GetAllDossierSacrifice_VeteranDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_VeteranEntity> GetAllPaging(int currentPage, int pageSize,
                                                                 string

                                                                     sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_Veteran_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_Veteran_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_VeteranEntity> lst = new List<DossierSacrifice_VeteranEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_VeteranEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
                _DossierSacrifice_VeteranDB.GetPageDossierSacrifice_VeteranDB(pageSize, currentPage,
                                                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_VeteranEntity GetDossierSacrifice_VeteranFromDossierSacrifice_VeteranDB(
            DossierSacrifice_VeteranEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_VeteranEntity ret = new DossierSacrifice_VeteranEntity(o.DossierSacrifice_VeteranId,
                                                                                    o.DossierSacrificeId, o.VeteranCod,
                                                                                    o.WarZoneId, o.VeteranDate,
                                                                                    o.VeteranBodyRegionId,
                                                                                    o.VeteranPercent,
                                                                                    o.DurationOfPresenceInBattlefield,
                                                                                    o.VeteranFoundationDossierNo,
                                                                                    o.CityId, o.RehabilitationEquipment,
                                                                                    o.PreVeteranEucationDegreeId,
                                                                                    o.EucationDegreeId,
                                                                                    o.OrganizationPhysicalChartId,
                                                                                    o.MilitaryUnitDispatcherId, o.PostId,
                                                                                    o.Description,
                                                                                    o.SacrificePeriodTimeId,
                                                                                    o.StartDate,o.EndDate,
                                                                                    o.CreationDate, o.ModificationDate,o.ProvinceId,o.OrganizationPhysicalChartTitle);
            return ret;
        }

        private List<DossierSacrifice_VeteranEntity>
            GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
            List<DossierSacrifice_VeteranEntity> lst)
        {
            List<DossierSacrifice_VeteranEntity> RetLst = new List<DossierSacrifice_VeteranEntity>();
            foreach (DossierSacrifice_VeteranEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_VeteranFromDossierSacrifice_VeteranDB(o));
            }
            return RetLst;

        }




        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranCollectionByDossierSacrifice(
            DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            return
                GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
                    _DossierSacrifice_VeteranDB.GetDossierSacrifice_VeteranDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_VeteranEntityParam));
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranCollectionByEducationDegree(
            DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            return
                GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
                    _DossierSacrifice_VeteranDB.GetDossierSacrifice_VeteranDBCollectionByEducationDegreeDB(
                        DossierSacrifice_VeteranEntityParam));
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranCollectionBySacrificePeriodTime(
            DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            return
                GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
                    _DossierSacrifice_VeteranDB.GetDossierSacrifice_VeteranDBCollectionBySacrificePeriodTimeDB(
                        DossierSacrifice_VeteranEntityParam));
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranCollectionByVeteranBodyRegion(
            DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            return
                GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
                    _DossierSacrifice_VeteranDB.GetDossierSacrifice_VeteranDBCollectionByVeteranBodyRegionDB(
                        DossierSacrifice_VeteranEntityParam));
        }

        public List<DossierSacrifice_VeteranEntity> GetDossierSacrifice_VeteranCollectionByWarZone(
            DossierSacrifice_VeteranEntity DossierSacrifice_VeteranEntityParam)
        {
            return
                GetDossierSacrifice_VeteranCollectionFromDossierSacrifice_VeteranDBList(
                    _DossierSacrifice_VeteranDB.GetDossierSacrifice_VeteranDBCollectionByWarZoneDB(
                        DossierSacrifice_VeteranEntityParam));
        }


    }
}