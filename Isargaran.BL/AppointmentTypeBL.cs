﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/16>
    /// Description: <نوع مراجعه>
    /// </summary>

    public class AppointmentTypeBL
    {
        private readonly AppointmentTypeDB _AppointmentTypeDB;

        public AppointmentTypeBL()
        {
            _AppointmentTypeDB = new AppointmentTypeDB();
        }

        public void Add(AppointmentTypeEntity AppointmentTypeEntityParam, out Guid AppointmentTypeId)
        {
            _AppointmentTypeDB.AddAppointmentTypeDB(AppointmentTypeEntityParam, out AppointmentTypeId);
        }

        public void Update(AppointmentTypeEntity AppointmentTypeEntityParam)
        {
            _AppointmentTypeDB.UpdateAppointmentTypeDB(AppointmentTypeEntityParam);
        }

        public void Delete(AppointmentTypeEntity AppointmentTypeEntityParam)
        {
            _AppointmentTypeDB.DeleteAppointmentTypeDB(AppointmentTypeEntityParam);
        }

        public AppointmentTypeEntity GetSingleById(AppointmentTypeEntity AppointmentTypeEntityParam)
        {
            AppointmentTypeEntity o = GetAppointmentTypeFromAppointmentTypeDB(
                _AppointmentTypeDB.GetSingleAppointmentTypeDB(AppointmentTypeEntityParam));

            return o;
        }

        public List<AppointmentTypeEntity> GetAll()
        {
            List<AppointmentTypeEntity> lst = new List<AppointmentTypeEntity>();
            //string key = "AppointmentType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AppointmentTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAppointmentTypeCollectionFromAppointmentTypeDBList(
                _AppointmentTypeDB.GetAllAppointmentTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<AppointmentTypeEntity> GetAllIsActive()
        {
            List<AppointmentTypeEntity> lst = new List<AppointmentTypeEntity>();
            //string key = "AppointmentType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AppointmentTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAppointmentTypeCollectionFromAppointmentTypeDBList(
                _AppointmentTypeDB.GetAllAppointmentTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<AppointmentTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                        string

                                                            sortExpression, out int count, string whereClause)
        {
            //string key = "AppointmentType_List_Page_" + currentPage ;
            //string countKey = "AppointmentType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<AppointmentTypeEntity> lst = new List<AppointmentTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AppointmentTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetAppointmentTypeCollectionFromAppointmentTypeDBList(
                _AppointmentTypeDB.GetPageAppointmentTypeDB(pageSize, currentPage,
                                                            whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private AppointmentTypeEntity GetAppointmentTypeFromAppointmentTypeDB(AppointmentTypeEntity o)
        {
            if (o == null)
                return null;
            AppointmentTypeEntity ret = new AppointmentTypeEntity(o.AppointmentTypeId, o.AppointmentTypeTitle,
                                                                  o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<AppointmentTypeEntity> GetAppointmentTypeCollectionFromAppointmentTypeDBList(
            List<AppointmentTypeEntity> lst)
        {
            List<AppointmentTypeEntity> RetLst = new List<AppointmentTypeEntity>();
            foreach (AppointmentTypeEntity o in lst)
            {
                RetLst.Add(GetAppointmentTypeFromAppointmentTypeDB(o));
            }
            return RetLst;

        }


    }

}
