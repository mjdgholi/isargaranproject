﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: < ایثارگری>
    /// </summary>


	public class SacrificeBL 
	{	
	  	 private readonly SacrificeDB _SacrificeDB;					
			
		public SacrificeBL()
		{
			_SacrificeDB = new SacrificeDB();
		}			
	
		public  void Add(SacrificeEntity  SacrificeEntityParam, out Guid SacrificeId)
		{ 
			_SacrificeDB.AddSacrificeDB(SacrificeEntityParam,out SacrificeId);			
		}

		public  void Update(SacrificeEntity  SacrificeEntityParam)
		{
			_SacrificeDB.UpdateSacrificeDB(SacrificeEntityParam);		
		}

		public  void Delete(SacrificeEntity  SacrificeEntityParam)
		{
			 _SacrificeDB.DeleteSacrificeDB(SacrificeEntityParam);			
		}

		public  SacrificeEntity GetSingleById(SacrificeEntity  SacrificeEntityParam)
		{
			SacrificeEntity o = GetSacrificeFromSacrificeDB(
			_SacrificeDB.GetSingleSacrificeDB(SacrificeEntityParam));
			
			return o;
		}

		public  List<SacrificeEntity> GetAll()
		{
			List<SacrificeEntity> lst = new List<SacrificeEntity>();
			//string key = "Sacrifice_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SacrificeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSacrificeCollectionFromSacrificeDBList(
				_SacrificeDB.GetAllSacrificeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<SacrificeEntity> GetAllIsActive()
        {
            List<SacrificeEntity> lst = new List<SacrificeEntity>();
            //string key = "Sacrifice_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SacrificeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSacrificeCollectionFromSacrificeDBList(
            _SacrificeDB.GetAllSacrificeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<SacrificeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Sacrifice_List_Page_" + currentPage ;
			//string countKey = "Sacrifice_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<SacrificeEntity> lst = new List<SacrificeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SacrificeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetSacrificeCollectionFromSacrificeDBList(
				_SacrificeDB.GetPageSacrificeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  SacrificeEntity GetSacrificeFromSacrificeDB(SacrificeEntity o)
		{
	if(o == null)
                return null;
			SacrificeEntity ret = new SacrificeEntity(o.SacrificeId ,o.SacrificeTitle ,o.SacrificeTypeId ,o.IsPercentValue ,o.IsTimeValue ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<SacrificeEntity> GetSacrificeCollectionFromSacrificeDBList( List<SacrificeEntity> lst)
		{
			List<SacrificeEntity> RetLst = new List<SacrificeEntity>();
			foreach(SacrificeEntity o in lst)
			{
				RetLst.Add(GetSacrificeFromSacrificeDB(o));
			}
			return RetLst;
			
		}




        public List<SacrificeEntity> GetSacrificeCollectionBySacrificeType(SacrificeEntity SacrificeEntityParam)
{
    return GetSacrificeCollectionFromSacrificeDBList(_SacrificeDB.GetSacrificeDBCollectionBySacrificeTypeDB(SacrificeEntityParam));
}


}

}
