﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/27>
    // Description:	<فرم ناحیه جانبازی بدن>
    /// </summary>

    //-------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    public class VeteranBodyRegionBL
    {
        private readonly VeteranBodyRegionDB _VeteranBodyRegionDB;

        public VeteranBodyRegionBL()
        {
            _VeteranBodyRegionDB = new VeteranBodyRegionDB();
        }

        public void Add(VeteranBodyRegionEntity VeteranBodyRegionEntityParam, out Guid VeteranBodyRegionId)
        {
            _VeteranBodyRegionDB.AddVeteranBodyRegionDB(VeteranBodyRegionEntityParam, out VeteranBodyRegionId);
        }

        public void Update(VeteranBodyRegionEntity VeteranBodyRegionEntityParam)
        {
            _VeteranBodyRegionDB.UpdateVeteranBodyRegionDB(VeteranBodyRegionEntityParam);
        }

        public void Delete(VeteranBodyRegionEntity VeteranBodyRegionEntityParam)
        {
            _VeteranBodyRegionDB.DeleteVeteranBodyRegionDB(VeteranBodyRegionEntityParam);
        }

        public VeteranBodyRegionEntity GetSingleById(VeteranBodyRegionEntity VeteranBodyRegionEntityParam)
        {
            VeteranBodyRegionEntity o = GetVeteranBodyRegionFromVeteranBodyRegionDB(
                _VeteranBodyRegionDB.GetSingleVeteranBodyRegionDB(VeteranBodyRegionEntityParam));

            return o;
        }

        public List<VeteranBodyRegionEntity> GetAll()
        {
            List<VeteranBodyRegionEntity> lst = new List<VeteranBodyRegionEntity>();
            //string key = "VeteranBodyRegion_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<VeteranBodyRegionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetVeteranBodyRegionCollectionFromVeteranBodyRegionDBList(
                _VeteranBodyRegionDB.GetAllVeteranBodyRegionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<VeteranBodyRegionEntity> GetAllIsActive()
        {
            List<VeteranBodyRegionEntity> lst = new List<VeteranBodyRegionEntity>();
            //string key = "VeteranBodyRegion_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<VeteranBodyRegionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetVeteranBodyRegionCollectionFromVeteranBodyRegionDBList(
                _VeteranBodyRegionDB.GetAllIsActiveVeteranBodyRegionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<VeteranBodyRegionEntity> GetAllPaging(int currentPage, int pageSize,
                                                          string

                                                              sortExpression, out int count, string whereClause)
        {
            //string key = "VeteranBodyRegion_List_Page_" + currentPage ;
            //string countKey = "VeteranBodyRegion_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<VeteranBodyRegionEntity> lst = new List<VeteranBodyRegionEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<VeteranBodyRegionEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetVeteranBodyRegionCollectionFromVeteranBodyRegionDBList(
                _VeteranBodyRegionDB.GetPageVeteranBodyRegionDB(pageSize, currentPage,
                                                                whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private VeteranBodyRegionEntity GetVeteranBodyRegionFromVeteranBodyRegionDB(VeteranBodyRegionEntity o)
        {
            if (o == null)
                return null;
            VeteranBodyRegionEntity ret = new VeteranBodyRegionEntity(o.VeteranBodyRegionId, o.VeteranBodyRegionTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<VeteranBodyRegionEntity> GetVeteranBodyRegionCollectionFromVeteranBodyRegionDBList(List<VeteranBodyRegionEntity> lst)
        {
            List<VeteranBodyRegionEntity> RetLst = new List<VeteranBodyRegionEntity>();
            foreach (VeteranBodyRegionEntity o in lst)
            {
                RetLst.Add(GetVeteranBodyRegionFromVeteranBodyRegionDB(o));
            }
            return RetLst;

        }


    }
}