﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/14>
    /// Description: <پرونده  تحصیلی ایثارگری>
    /// </summary>


	public class DossierSacrifice_EducationBL 
	{	
	  	 private readonly DossierSacrifice_EducationDB _DossierSacrifice_EducationDB;					
			
		public DossierSacrifice_EducationBL()
		{
			_DossierSacrifice_EducationDB = new DossierSacrifice_EducationDB();
		}			
	
		public  void Add(DossierSacrifice_EducationEntity  DossierSacrifice_EducationEntityParam, out Guid DossierSacrifice_EducationId)
		{ 
			_DossierSacrifice_EducationDB.AddDossierSacrifice_EducationDB(DossierSacrifice_EducationEntityParam,out DossierSacrifice_EducationId);			
		}

		public  void Update(DossierSacrifice_EducationEntity  DossierSacrifice_EducationEntityParam)
		{
			_DossierSacrifice_EducationDB.UpdateDossierSacrifice_EducationDB(DossierSacrifice_EducationEntityParam);		
		}

		public  void Delete(DossierSacrifice_EducationEntity  DossierSacrifice_EducationEntityParam)
		{
			 _DossierSacrifice_EducationDB.DeleteDossierSacrifice_EducationDB(DossierSacrifice_EducationEntityParam);			
		}

		public  DossierSacrifice_EducationEntity GetSingleById(DossierSacrifice_EducationEntity  DossierSacrifice_EducationEntityParam)
		{
			DossierSacrifice_EducationEntity o = GetDossierSacrifice_EducationFromDossierSacrifice_EducationDB(
			_DossierSacrifice_EducationDB.GetSingleDossierSacrifice_EducationDB(DossierSacrifice_EducationEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_EducationEntity> GetAll()
		{
			List<DossierSacrifice_EducationEntity> lst = new List<DossierSacrifice_EducationEntity>();
			//string key = "DossierSacrifice_Education_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_EducationEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(
				_DossierSacrifice_EducationDB.GetAllDossierSacrifice_EducationDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_EducationEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_Education_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_Education_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_EducationEntity> lst = new List<DossierSacrifice_EducationEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_EducationEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(
				_DossierSacrifice_EducationDB.GetPageDossierSacrifice_EducationDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_EducationEntity GetDossierSacrifice_EducationFromDossierSacrifice_EducationDB(DossierSacrifice_EducationEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_EducationEntity ret = new DossierSacrifice_EducationEntity(o.DossierSacrifice_EducationId ,o.DossierSacrificeId ,o.AdmissionTypeId ,o.EducationCenterId ,o.EucationDegreeId ,o.EducationCourseId ,o.EducationOrientationId ,o.CityId ,o.StartDate ,o.EndDate ,o.StudentNo ,o.GradePointAverage ,o.FirstSupervisor ,o.SecondSupervisor ,o.FirstAdvisor ,o.SecondAdvisor ,o.ThesisSubject ,o.TechnicalDescription ,o.ModificationDate ,o.CreationDate,o.ProvinceId );
			return ret;
		}
		
		private  List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList( List<DossierSacrifice_EducationEntity> lst)
		{
			List<DossierSacrifice_EducationEntity> RetLst = new List<DossierSacrifice_EducationEntity>();
			foreach(DossierSacrifice_EducationEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_EducationFromDossierSacrifice_EducationDB(o));
			}
			return RetLst;
			
		}


        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionByAdmissionType(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
{
    return GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(_DossierSacrifice_EducationDB.GetDossierSacrifice_EducationDBCollectionByAdmissionTypeDB(DossierSacrifice_EducationEntityParam));
}

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionByCity(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
{
    return GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(_DossierSacrifice_EducationDB.GetDossierSacrifice_EducationDBCollectionByCityDB(DossierSacrifice_EducationEntityParam));
}

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionByDossierSacrifice(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
{
    return GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(_DossierSacrifice_EducationDB.GetDossierSacrifice_EducationDBCollectionByDossierSacrificeDB(DossierSacrifice_EducationEntityParam));
}

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionByEducationCenter(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
{
    return GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(_DossierSacrifice_EducationDB.GetDossierSacrifice_EducationDBCollectionByEducationCenterDB(DossierSacrifice_EducationEntityParam));
}

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionByEducationCourse(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
{
    return GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(_DossierSacrifice_EducationDB.GetDossierSacrifice_EducationDBCollectionByEducationCourseDB(DossierSacrifice_EducationEntityParam));
}

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionByEducationDegree(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
{
    return GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(_DossierSacrifice_EducationDB.GetDossierSacrifice_EducationDBCollectionByEducationDegreeDB(DossierSacrifice_EducationEntityParam));
}

        public List<DossierSacrifice_EducationEntity> GetDossierSacrifice_EducationCollectionByEducationOrientation(DossierSacrifice_EducationEntity DossierSacrifice_EducationEntityParam)
{
    return GetDossierSacrifice_EducationCollectionFromDossierSacrifice_EducationDBList(_DossierSacrifice_EducationDB.GetDossierSacrifice_EducationDBCollectionByEducationOrientationDB(DossierSacrifice_EducationEntityParam));
}



}

}
