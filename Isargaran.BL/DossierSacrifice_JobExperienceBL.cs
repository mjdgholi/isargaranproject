﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <پرونده مشخصات سوابق کاری>
    /// </summary>   

    public class DossierSacrifice_JobExperienceBL
    {
        private readonly DossierSacrifice_JobExperienceDB _DossierSacrifice_JobExperienceDB;

        public DossierSacrifice_JobExperienceBL()
        {
            _DossierSacrifice_JobExperienceDB = new DossierSacrifice_JobExperienceDB();
        }

        public void Add(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam,
                        out Guid DossierSacrifice_JobExperienceId)
        {
            _DossierSacrifice_JobExperienceDB.AddDossierSacrifice_JobExperienceDB(
                DossierSacrifice_JobExperienceEntityParam, out DossierSacrifice_JobExperienceId);
        }

        public void Update(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            _DossierSacrifice_JobExperienceDB.UpdateDossierSacrifice_JobExperienceDB(
                DossierSacrifice_JobExperienceEntityParam);
        }

        public void Delete(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            _DossierSacrifice_JobExperienceDB.DeleteDossierSacrifice_JobExperienceDB(
                DossierSacrifice_JobExperienceEntityParam);
        }

        public DossierSacrifice_JobExperienceEntity GetSingleById(
            DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            DossierSacrifice_JobExperienceEntity o = GetDossierSacrifice_JobExperienceFromDossierSacrifice_JobExperienceDB
                (
                    _DossierSacrifice_JobExperienceDB.GetSingleDossierSacrifice_JobExperienceDB(
                        DossierSacrifice_JobExperienceEntityParam));

            return o;
        }

        public List<DossierSacrifice_JobExperienceEntity> GetAll()
        {
            List<DossierSacrifice_JobExperienceEntity> lst = new List<DossierSacrifice_JobExperienceEntity>();
            //string key = "DossierSacrifice_JobExperience_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_JobExperienceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                _DossierSacrifice_JobExperienceDB.GetAllDossierSacrifice_JobExperienceDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_JobExperienceEntity> GetAllPaging(int currentPage, int pageSize,
                                                                       string

                                                                           sortExpression, out int count,
                                                                       string whereClause)
        {
            //string key = "DossierSacrifice_JobExperience_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_JobExperience_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_JobExperienceEntity> lst = new List<DossierSacrifice_JobExperienceEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_JobExperienceEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                _DossierSacrifice_JobExperienceDB.GetPageDossierSacrifice_JobExperienceDB(pageSize, currentPage,
                                                                                          whereClause, sortExpression,
                                                                                          out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_JobExperienceEntity
            GetDossierSacrifice_JobExperienceFromDossierSacrifice_JobExperienceDB(DossierSacrifice_JobExperienceEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_JobExperienceEntity ret =
                new DossierSacrifice_JobExperienceEntity(o.DossierSacrifice_JobExperienceId, o.DossierSacrificeId,
                                                         o.OrganizationPhysicalChartId, o.EmploymentTypeId,
                                                         o.EmploymentDate, o.PostId, o.PersonnelNo, o.CommandmentNo,
                                                         o.FreeJob, o.DurationTimeAtWorkId, o.DirectManager, o.CityId,
                                                         o.TelNo, o.FaxNo, o.Address, o.PostalCod, o.Email, o.WebAddress,
                                                         o.StartDate, o.EndDate, o.CreationDate, o.ModificationDate,o.ProvinceId,o.OrganizationPhysicalChartTitle);
            return ret;
        }

        private List<DossierSacrifice_JobExperienceEntity>
            GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
            List<DossierSacrifice_JobExperienceEntity> lst)
        {
            List<DossierSacrifice_JobExperienceEntity> RetLst = new List<DossierSacrifice_JobExperienceEntity>();
            foreach (DossierSacrifice_JobExperienceEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_JobExperienceFromDossierSacrifice_JobExperienceDB(o));
            }
            return RetLst;

        }



        public List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceCollectionByCity(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            return
                GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                    _DossierSacrifice_JobExperienceDB.GetDossierSacrifice_JobExperienceDBCollectionByCityDB(DossierSacrifice_JobExperienceEntityParam));
        }

        public List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceCollectionByDossierSacrifice(
            DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            return
                GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                    _DossierSacrifice_JobExperienceDB.GetDossierSacrifice_JobExperienceDBCollectionByDossierSacrificeDB(
                        DossierSacrifice_JobExperienceEntityParam));
        }

        public List<DossierSacrifice_JobExperienceEntity>
            GetDossierSacrifice_JobExperienceCollectionByDurationTimeAtWork(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            return
                GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                    _DossierSacrifice_JobExperienceDB.GetDossierSacrifice_JobExperienceDBCollectionByDurationTimeAtWorkDB
                        (DossierSacrifice_JobExperienceEntityParam));
        }

        public List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceCollectionByEmploymentType(
            DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            return
                GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                    _DossierSacrifice_JobExperienceDB.GetDossierSacrifice_JobExperienceDBCollectionByEmploymentTypeDB(
                        DossierSacrifice_JobExperienceEntityParam));
        }

        public List<DossierSacrifice_JobExperienceEntity>
            GetDossierSacrifice_JobExperienceCollectionByOrganizationPhysicalChart(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            return
                GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                    _DossierSacrifice_JobExperienceDB.
                        GetDossierSacrifice_JobExperienceDBCollectionByOrganizationPhysicalChartDB(
                            DossierSacrifice_JobExperienceEntityParam));
        }

        public List<DossierSacrifice_JobExperienceEntity> GetDossierSacrifice_JobExperienceCollectionByPost(DossierSacrifice_JobExperienceEntity DossierSacrifice_JobExperienceEntityParam)
        {
            return
                GetDossierSacrifice_JobExperienceCollectionFromDossierSacrifice_JobExperienceDBList(
                    _DossierSacrifice_JobExperienceDB.GetDossierSacrifice_JobExperienceDBCollectionByPostDB(DossierSacrifice_JobExperienceEntityParam));
        }


    }

}
