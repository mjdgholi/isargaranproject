﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/02/14>
    // Description:	<اطلاعات ایجاد زمینه اشتغال>
    /// </summary>
    public class DossierSacrifice_CreatingAreasOfEmploymentBL
    {
        private readonly DossierSacrifice_CreatingAreasOfEmploymentDB _DossierSacrifice_CreatingAreasOfEmploymentDB;

        public DossierSacrifice_CreatingAreasOfEmploymentBL()
        {
            _DossierSacrifice_CreatingAreasOfEmploymentDB = new DossierSacrifice_CreatingAreasOfEmploymentDB();
        }

        public void Add(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam, out Guid DossierSacrifice_CreatingAreasOfEmploymentId)
        {
            _DossierSacrifice_CreatingAreasOfEmploymentDB.AddDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntityParam, out DossierSacrifice_CreatingAreasOfEmploymentId);
        }

        public void Update(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam)
        {
            _DossierSacrifice_CreatingAreasOfEmploymentDB.UpdateDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntityParam);
        }

        public void Delete(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam)
        {
            _DossierSacrifice_CreatingAreasOfEmploymentDB.DeleteDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntityParam);
        }

        public DossierSacrifice_CreatingAreasOfEmploymentEntity GetSingleById(DossierSacrifice_CreatingAreasOfEmploymentEntity DossierSacrifice_CreatingAreasOfEmploymentEntityParam)
        {
            DossierSacrifice_CreatingAreasOfEmploymentEntity o = GetDossierSacrifice_CreatingAreasOfEmploymentFromDossierSacrifice_CreatingAreasOfEmploymentDB(
                _DossierSacrifice_CreatingAreasOfEmploymentDB.GetSingleDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntityParam));

            return o;
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetAll()
        {
            List<DossierSacrifice_CreatingAreasOfEmploymentEntity> lst = new List<DossierSacrifice_CreatingAreasOfEmploymentEntity>();
            //string key = "DossierSacrifice_CreatingAreasOfEmployment_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_CreatingAreasOfEmploymentEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierSacrifice_CreatingAreasOfEmploymentCollectionFromDossierSacrifice_CreatingAreasOfEmploymentDBList(
                _DossierSacrifice_CreatingAreasOfEmploymentDB.GetAllDossierSacrifice_CreatingAreasOfEmploymentDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "DossierSacrifice_CreatingAreasOfEmployment_List_Page_" + currentPage ;
            //string countKey = "DossierSacrifice_CreatingAreasOfEmployment_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierSacrifice_CreatingAreasOfEmploymentEntity> lst = new List<DossierSacrifice_CreatingAreasOfEmploymentEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierSacrifice_CreatingAreasOfEmploymentEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierSacrifice_CreatingAreasOfEmploymentCollectionFromDossierSacrifice_CreatingAreasOfEmploymentDBList(
                _DossierSacrifice_CreatingAreasOfEmploymentDB.GetPageDossierSacrifice_CreatingAreasOfEmploymentDB(pageSize, currentPage,
                                                                                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierSacrifice_CreatingAreasOfEmploymentEntity GetDossierSacrifice_CreatingAreasOfEmploymentFromDossierSacrifice_CreatingAreasOfEmploymentDB(DossierSacrifice_CreatingAreasOfEmploymentEntity o)
        {
            if (o == null)
                return null;
            DossierSacrifice_CreatingAreasOfEmploymentEntity ret = new DossierSacrifice_CreatingAreasOfEmploymentEntity(o.DossierSacrifice_CreatingAreasOfEmploymentId, o.DossierSacrificeId, o.EmployedPersonId, o.EmploymentDate, o.OrganizationId, o.DependentTypeId, o.IsItWorking, o.Description, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetDossierSacrifice_CreatingAreasOfEmploymentCollectionFromDossierSacrifice_CreatingAreasOfEmploymentDBList(List<DossierSacrifice_CreatingAreasOfEmploymentEntity> lst)
        {
            List<DossierSacrifice_CreatingAreasOfEmploymentEntity> RetLst = new List<DossierSacrifice_CreatingAreasOfEmploymentEntity>();
            foreach (DossierSacrifice_CreatingAreasOfEmploymentEntity o in lst)
            {
                RetLst.Add(GetDossierSacrifice_CreatingAreasOfEmploymentFromDossierSacrifice_CreatingAreasOfEmploymentDB(o));
            }
            return RetLst;

        }


        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetDossierSacrifice_CreatingAreasOfEmploymentCollectionByDependentType(Guid DependentTypeId)
        {
            return GetDossierSacrifice_CreatingAreasOfEmploymentCollectionFromDossierSacrifice_CreatingAreasOfEmploymentDBList(_DossierSacrifice_CreatingAreasOfEmploymentDB.GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionByDependentTypeDB(DependentTypeId));
        }

        public List<DossierSacrifice_CreatingAreasOfEmploymentEntity> GetDossierSacrifice_CreatingAreasOfEmploymentCollectionByDossierSacrifice(Guid DossierSacrificeId)
        {
            return GetDossierSacrifice_CreatingAreasOfEmploymentCollectionFromDossierSacrifice_CreatingAreasOfEmploymentDBList(_DossierSacrifice_CreatingAreasOfEmploymentDB.GetDossierSacrifice_CreatingAreasOfEmploymentDBCollectionByDossierSacrificeDB(DossierSacrificeId));
        }
    }
}