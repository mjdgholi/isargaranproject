﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/25>
    /// Description: <مدیریت پیغام>
    /// </summary>

    public class MessageBL
    {
        private readonly MessageDB _MessageDB;

        public MessageBL()
        {
            _MessageDB = new MessageDB();
        }

        public void Add(MessageEntity MessageEntityParam, out Guid MessageId)
        {
            _MessageDB.AddMessageDB(MessageEntityParam, out MessageId);
        }

        public void Update(MessageEntity MessageEntityParam)
        {
            _MessageDB.UpdateMessageDB(MessageEntityParam);
        }

        public void Delete(MessageEntity MessageEntityParam)
        {
            _MessageDB.DeleteMessageDB(MessageEntityParam);
        }

        public MessageEntity GetSingleById(MessageEntity MessageEntityParam)
        {
            MessageEntity o = GetMessageFromMessageDB(
                _MessageDB.GetSingleMessageDB(MessageEntityParam));

            return o;
        }

        public List<MessageEntity> GetAll()
        {
            List<MessageEntity> lst = new List<MessageEntity>();
            //string key = "Message_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MessageEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMessageCollectionFromMessageDBList(
                _MessageDB.GetAllMessageDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<MessageEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "Message_List_Page_" + currentPage ;
            //string countKey = "Message_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<MessageEntity> lst = new List<MessageEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MessageEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetMessageCollectionFromMessageDBList(
                _MessageDB.GetPageMessageDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private MessageEntity GetMessageFromMessageDB(MessageEntity o)
        {
            if (o == null)
                return null;
            MessageEntity ret = new MessageEntity(o.MessageId, o.DossierSacrifice_RequestId, o.MessageTypeId,
                o.MessageText, o.IsViewed, o.ViewedDate, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<MessageEntity> GetMessageCollectionFromMessageDBList(List<MessageEntity> lst)
        {
            List<MessageEntity> RetLst = new List<MessageEntity>();
            foreach (MessageEntity o in lst)
            {
                RetLst.Add(GetMessageFromMessageDB(o));
            }
            return RetLst;

        }

        public List<MessageEntity> GetMessageCollectionByMessageTypeId(MessageEntity MessageEntityParam)
        {
            return
                GetMessageCollectionFromMessageDBList(
                    _MessageDB.GetMessageDBCollectionByMessageTypeIdDB(MessageEntityParam));
        }

        public List<MessageEntity> GetMessageCollectionByDossierSacrifice_Request(MessageEntity MessageEntityParam)
        {
            return
                GetMessageCollectionFromMessageDBList(
                    _MessageDB.GetMessageDBCollectionByDossierSacrifice_RequestDB(MessageEntityParam));
        }


        public void UpdateIsView(MessageEntity messageEntity)
        {
            _MessageDB.UpdateIsViewDB(messageEntity);
        }
    }

}