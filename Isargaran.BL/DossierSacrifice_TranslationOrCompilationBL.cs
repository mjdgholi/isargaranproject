﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/09>
    /// Description: <پرونده اطلاعات تالیف و ترجمه ایثارگر>
    /// </summary>

	public class DossierSacrifice_TranslationOrCompilationBL 
	{	
	  	 private readonly DossierSacrifice_TranslationOrCompilationDB _DossierSacrifice_TranslationOrCompilationDB;					
			
		public DossierSacrifice_TranslationOrCompilationBL()
		{
			_DossierSacrifice_TranslationOrCompilationDB = new DossierSacrifice_TranslationOrCompilationDB();
		}			
	
		public  void Add(DossierSacrifice_TranslationOrCompilationEntity  DossierSacrifice_TranslationOrCompilationEntityParam, out Guid DossierSacrifice_TranslationOrCompilationId)
		{ 
			_DossierSacrifice_TranslationOrCompilationDB.AddDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntityParam,out DossierSacrifice_TranslationOrCompilationId);			
		}

		public  void Update(DossierSacrifice_TranslationOrCompilationEntity  DossierSacrifice_TranslationOrCompilationEntityParam)
		{
			_DossierSacrifice_TranslationOrCompilationDB.UpdateDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntityParam);		
		}

		public  void Delete(DossierSacrifice_TranslationOrCompilationEntity  DossierSacrifice_TranslationOrCompilationEntityParam)
		{
			 _DossierSacrifice_TranslationOrCompilationDB.DeleteDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntityParam);			
		}

		public  DossierSacrifice_TranslationOrCompilationEntity GetSingleById(DossierSacrifice_TranslationOrCompilationEntity  DossierSacrifice_TranslationOrCompilationEntityParam)
		{
			DossierSacrifice_TranslationOrCompilationEntity o = GetDossierSacrifice_TranslationOrCompilationFromDossierSacrifice_TranslationOrCompilationDB(
			_DossierSacrifice_TranslationOrCompilationDB.GetSingleDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntityParam));
			
			return o;
		}

		public  List<DossierSacrifice_TranslationOrCompilationEntity> GetAll()
		{
			List<DossierSacrifice_TranslationOrCompilationEntity> lst = new List<DossierSacrifice_TranslationOrCompilationEntity>();
			//string key = "DossierSacrifice_TranslationOrCompilation_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_TranslationOrCompilationEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDossierSacrifice_TranslationOrCompilationCollectionFromDossierSacrifice_TranslationOrCompilationDBList(
				_DossierSacrifice_TranslationOrCompilationDB.GetAllDossierSacrifice_TranslationOrCompilationDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<DossierSacrifice_TranslationOrCompilationEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DossierSacrifice_TranslationOrCompilation_List_Page_" + currentPage ;
			//string countKey = "DossierSacrifice_TranslationOrCompilation_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DossierSacrifice_TranslationOrCompilationEntity> lst = new List<DossierSacrifice_TranslationOrCompilationEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DossierSacrifice_TranslationOrCompilationEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDossierSacrifice_TranslationOrCompilationCollectionFromDossierSacrifice_TranslationOrCompilationDBList(
				_DossierSacrifice_TranslationOrCompilationDB.GetPageDossierSacrifice_TranslationOrCompilationDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DossierSacrifice_TranslationOrCompilationEntity GetDossierSacrifice_TranslationOrCompilationFromDossierSacrifice_TranslationOrCompilationDB(DossierSacrifice_TranslationOrCompilationEntity o)
		{
	if(o == null)
                return null;
			DossierSacrifice_TranslationOrCompilationEntity ret = new DossierSacrifice_TranslationOrCompilationEntity(o.DossierSacrifice_TranslationOrCompilationId ,o.DossierSacrificeId ,o.BaseBookTitle ,o.TranslatedBookTitle ,o.BookSubject ,o.Publication ,o.FirstPublicationDate ,o.TurnPublicationNo ,o.WritingActivityTypeId ,o.Description ,o.ModificationDate ,o.CreationDate );
			return ret;
		}
		
		private  List<DossierSacrifice_TranslationOrCompilationEntity> GetDossierSacrifice_TranslationOrCompilationCollectionFromDossierSacrifice_TranslationOrCompilationDBList( List<DossierSacrifice_TranslationOrCompilationEntity> lst)
		{
			List<DossierSacrifice_TranslationOrCompilationEntity> RetLst = new List<DossierSacrifice_TranslationOrCompilationEntity>();
			foreach(DossierSacrifice_TranslationOrCompilationEntity o in lst)
			{
				RetLst.Add(GetDossierSacrifice_TranslationOrCompilationFromDossierSacrifice_TranslationOrCompilationDB(o));
			}
			return RetLst;
			
		}


        public List<DossierSacrifice_TranslationOrCompilationEntity> GetDossierSacrifice_TranslationOrCompilationCollectionByDossierSacrifice(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam)
{
    return GetDossierSacrifice_TranslationOrCompilationCollectionFromDossierSacrifice_TranslationOrCompilationDBList(_DossierSacrifice_TranslationOrCompilationDB.GetDossierSacrifice_TranslationOrCompilationDBCollectionByDossierSacrificeDB(DossierSacrifice_TranslationOrCompilationEntityParam));
}

        public List<DossierSacrifice_TranslationOrCompilationEntity> GetDossierSacrifice_TranslationOrCompilationCollectionByWritingActivityType(DossierSacrifice_TranslationOrCompilationEntity DossierSacrifice_TranslationOrCompilationEntityParam)
{
    return GetDossierSacrifice_TranslationOrCompilationCollectionFromDossierSacrifice_TranslationOrCompilationDBList(_DossierSacrifice_TranslationOrCompilationDB.GetDossierSacrifice_TranslationOrCompilationDBCollectionByWritingActivityTypeDB(DossierSacrifice_TranslationOrCompilationEntityParam));
}

}

}
