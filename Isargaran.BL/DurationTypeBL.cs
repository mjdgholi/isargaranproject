﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/02>
    /// Description: <نوع مدت>
    /// </summary>
    

	public class DurationTypeBL 
	{	
	  	 private readonly DurationTypeDB _DurationTypeDB;					
			
		public DurationTypeBL()
		{
			_DurationTypeDB = new DurationTypeDB();
		}			
	
		public  void Add(DurationTypeEntity  DurationTypeEntityParam, out Guid DurationTypeId)
		{ 
			_DurationTypeDB.AddDurationTypeDB(DurationTypeEntityParam,out DurationTypeId);			
		}

		public  void Update(DurationTypeEntity  DurationTypeEntityParam)
		{
			_DurationTypeDB.UpdateDurationTypeDB(DurationTypeEntityParam);		
		}

		public  void Delete(DurationTypeEntity  DurationTypeEntityParam)
		{
			 _DurationTypeDB.DeleteDurationTypeDB(DurationTypeEntityParam);			
		}

		public  DurationTypeEntity GetSingleById(DurationTypeEntity  DurationTypeEntityParam)
		{
			DurationTypeEntity o = GetDurationTypeFromDurationTypeDB(
			_DurationTypeDB.GetSingleDurationTypeDB(DurationTypeEntityParam));
			
			return o;
		}

		public  List<DurationTypeEntity> GetAll()
		{
			List<DurationTypeEntity> lst = new List<DurationTypeEntity>();
			//string key = "DurationType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DurationTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetDurationTypeCollectionFromDurationTypeDBList(
				_DurationTypeDB.GetAllDurationTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<DurationTypeEntity> GetAllIsActive()
        {
            List<DurationTypeEntity> lst = new List<DurationTypeEntity>();
            //string key = "DurationType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DurationTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDurationTypeCollectionFromDurationTypeDBList(
            _DurationTypeDB.GetAllDurationTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<DurationTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "DurationType_List_Page_" + currentPage ;
			//string countKey = "DurationType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<DurationTypeEntity> lst = new List<DurationTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<DurationTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetDurationTypeCollectionFromDurationTypeDBList(
				_DurationTypeDB.GetPageDurationTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  DurationTypeEntity GetDurationTypeFromDurationTypeDB(DurationTypeEntity o)
		{
	if(o == null)
                return null;
			DurationTypeEntity ret = new DurationTypeEntity(o.DurationTypeId ,o.DurationTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<DurationTypeEntity> GetDurationTypeCollectionFromDurationTypeDBList( List<DurationTypeEntity> lst)
		{
			List<DurationTypeEntity> RetLst = new List<DurationTypeEntity>();
			foreach(DurationTypeEntity o in lst)
			{
				RetLst.Add(GetDurationTypeFromDurationTypeDB(o));
			}
			return RetLst;
			
		}


        
	}

}


