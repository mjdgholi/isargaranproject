﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.IsargaranProject.Isargaran.DAL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <وضیعت پرونده>
    /// </summary>


    public class DossierStatusBL
    {
        private readonly DossierStatusDB _DossierStatusDB;

        public DossierStatusBL()
        {
            _DossierStatusDB = new DossierStatusDB();
        }

        public void Add(DossierStatusEntity DossierStatusEntityParam, out Guid DossierStatusId)
        {
            _DossierStatusDB.AddDossierStatusDB(DossierStatusEntityParam, out DossierStatusId);
        }

        public void Update(DossierStatusEntity DossierStatusEntityParam)
        {
            _DossierStatusDB.UpdateDossierStatusDB(DossierStatusEntityParam);
        }

        public void Delete(DossierStatusEntity DossierStatusEntityParam)
        {
            _DossierStatusDB.DeleteDossierStatusDB(DossierStatusEntityParam);
        }

        public DossierStatusEntity GetSingleById(DossierStatusEntity DossierStatusEntityParam)
        {
            DossierStatusEntity o = GetDossierStatusFromDossierStatusDB(
                _DossierStatusDB.GetSingleDossierStatusDB(DossierStatusEntityParam));

            return o;
        }

        public List<DossierStatusEntity> GetAll()
        {
            List<DossierStatusEntity> lst = new List<DossierStatusEntity>();
            //string key = "DossierStatus_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierStatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierStatusCollectionFromDossierStatusDBList(
                _DossierStatusDB.GetAllDossierStatusDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<DossierStatusEntity> GetAllIsActive()
        {
            List<DossierStatusEntity> lst = new List<DossierStatusEntity>();
            //string key = "DossierStatus_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierStatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDossierStatusCollectionFromDossierStatusDBList(
                _DossierStatusDB.GetAllDossierStatusIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<DossierStatusEntity> GetAllPaging(int currentPage, int pageSize,
                                                      string

                                                          sortExpression, out int count, string whereClause)
        {
            //string key = "DossierStatus_List_Page_" + currentPage ;
            //string countKey = "DossierStatus_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DossierStatusEntity> lst = new List<DossierStatusEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DossierStatusEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDossierStatusCollectionFromDossierStatusDBList(
                _DossierStatusDB.GetPageDossierStatusDB(pageSize, currentPage,
                                                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DossierStatusEntity GetDossierStatusFromDossierStatusDB(DossierStatusEntity o)
        {
            if (o == null)
                return null;
            DossierStatusEntity ret = new DossierStatusEntity(o.DossierStatusId, o.DossierStatusTitle, o.CreationDate,
                                                              o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<DossierStatusEntity> GetDossierStatusCollectionFromDossierStatusDBList(
            List<DossierStatusEntity> lst)
        {
            List<DossierStatusEntity> RetLst = new List<DossierStatusEntity>();
            foreach (DossierStatusEntity o in lst)
            {
                RetLst.Add(GetDossierStatusFromDossierStatusDB(o));
            }
            return RetLst;

        }


    }
}



