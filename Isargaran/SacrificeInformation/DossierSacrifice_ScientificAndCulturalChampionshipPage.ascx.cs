﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات  عناوین قهرمانی علمی و فرهنگی>
    /// </summary>
    /// 
    public partial class DossierSacrifice_ScientificAndCulturalChampionshipPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly ScientificAndCulturalFieldBL _scientificAndCulturalFieldBL = new ScientificAndCulturalFieldBL();
        private readonly DossierSacrifice_ScientificAndCulturalChampionshipBL _dossierSacrificeScientificAndCulturalChampionshipBL = new DossierSacrifice_ScientificAndCulturalChampionshipBL();
        private readonly DossierSacrifice_DependentBL _dossierSacrificeDependentBL = new DossierSacrifice_DependentBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_ScientificAndCulturalChampionship]";
        private const string PrimaryKey = "DossierSacrifice_ScientificAndCulturalChampionshipId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";            
            txtCompetitionDate.Text = "";
            txtCompetitionLocation.Text = "";
            txtCompetitionTitle.Text = "";
            txtPointOrMedal.Text = "";
            cmbScientificAndCulturalField.ClearSelection();
            cmbDependentPerson.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeScientificAndCulturalChampionshipEntity = new DossierSacrifice_ScientificAndCulturalChampionshipEntity()
            {
                DossierSacrifice_ScientificAndCulturalChampionshipId = Guid.Parse(ViewState["DossierSacrifice_ScientificAndCulturalChampionshipId"].ToString())
            };
            var myDossierSacrifice_ScientificAndCulturalChampionship = _dossierSacrificeScientificAndCulturalChampionshipBL.GetSingleById(dossierSacrificeScientificAndCulturalChampionshipEntity);
            cmbScientificAndCulturalField.SelectedValue = myDossierSacrifice_ScientificAndCulturalChampionship.ScientificAndCulturalFieldId.ToString();
            if (cmbScientificAndCulturalField.FindItemByValue(myDossierSacrifice_ScientificAndCulturalChampionship.ScientificAndCulturalFieldId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردرشته علمی و فرهنگی انتخاب شده معتبر نمی باشد.");
            }          
            txtDescription.Text = myDossierSacrifice_ScientificAndCulturalChampionship.Description;
            txtCompetitionDate.Text = myDossierSacrifice_ScientificAndCulturalChampionship.CompetitionDate;
            txtCompetitionLocation.Text = myDossierSacrifice_ScientificAndCulturalChampionship.CompetitionLocation;
            txtCompetitionTitle.Text = myDossierSacrifice_ScientificAndCulturalChampionship.CompetitionTitle;
            txtPointOrMedal.Text = myDossierSacrifice_ScientificAndCulturalChampionship.PointOrMedal;
            cmbDependentPerson.SelectedValue = myDossierSacrifice_ScientificAndCulturalChampionship.PersonId.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_ScientificAndCulturalChampionship,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbScientificAndCulturalField.Items.Clear();
            cmbScientificAndCulturalField.Items.Add(new RadComboBoxItem(""));
            cmbScientificAndCulturalField.DataSource = _scientificAndCulturalFieldBL.GetAllIsActive();
            cmbScientificAndCulturalField.DataTextField = "ScientificAndCulturalFieldPersianTitle";
            cmbScientificAndCulturalField.DataValueField = "ScientificAndCulturalFieldId";
            cmbScientificAndCulturalField.DataBind();


            DossierSacrifice_DependentEntity dossierSacrificeDependentEntity = new DossierSacrifice_DependentEntity()
            {
                DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString())
            };
            cmbDependentPerson.Items.Clear();
            cmbDependentPerson.Items.Add(new RadComboBoxItem(""));
            cmbDependentPerson.DataSource = _dossierSacrificeDependentBL.GetDossierSacrifice_DependentCollectionByDossierSacrifice(dossierSacrificeDependentEntity);
            cmbDependentPerson.DataTextField = "FullName";
            cmbDependentPerson.DataValueField = "PersonId";
            cmbDependentPerson.DataBind();
        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeScientificAndCulturalChampionshipId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeScientificAndCulturalChampionshipEntity = new DossierSacrifice_ScientificAndCulturalChampionshipEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CompetitionDate = ItcToDate.ShamsiToMiladi(txtCompetitionDate.Text),
                    CompetitionLocation = txtCompetitionLocation.Text,
                    CompetitionTitle = txtCompetitionTitle.Text,
                    PointOrMedal = txtPointOrMedal.Text,
                    ScientificAndCulturalFieldId = Guid.Parse(cmbScientificAndCulturalField.SelectedValue),
                    PersonId = (cmbDependentPerson.SelectedIndex > 0 ? Guid.Parse(cmbDependentPerson.SelectedValue) : (Guid?)null),                                 
                };
                _dossierSacrificeScientificAndCulturalChampionshipBL.Add(dossierSacrificeScientificAndCulturalChampionshipEntity, out dossierSacrificeScientificAndCulturalChampionshipId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, dossierSacrificeScientificAndCulturalChampionshipId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtCompetitionLocation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CompetitionLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtCompetitionLocation.Text.Trim()) + "%'";
                if (txtCompetitionTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CompetitionTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtCompetitionTitle.Text.Trim()) + "%'";
                if (txtPointOrMedal
                    .Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PointOrMedal Like N'%" +
                                               FarsiToArabic.ToArabic(txtPointOrMedal.Text.Trim()) + "%'";
                if (txtCompetitionDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CompetitionDate ='" +
                                               (txtCompetitionDate.Text.Trim()) + "'";
                if (cmbDependentPerson.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId ='" +
                                               (cmbDependentPerson.SelectedValue) + "'";
                if (cmbScientificAndCulturalField.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ScientificAndCulturalFieldId='" +
                                               (cmbScientificAndCulturalField.SelectedValue.Trim()) + "'";                

                grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_ScientificAndCulturalChampionship.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeScientificAndCulturalChampionshipEntity = new DossierSacrifice_ScientificAndCulturalChampionshipEntity()
                {
                    DossierSacrifice_ScientificAndCulturalChampionshipId = Guid.Parse(ViewState["DossierSacrifice_ScientificAndCulturalChampionshipId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CompetitionDate = ItcToDate.ShamsiToMiladi(txtCompetitionDate.Text),
                    CompetitionLocation = txtCompetitionLocation.Text,
                    CompetitionTitle = txtCompetitionTitle.Text,
                    PointOrMedal = txtPointOrMedal.Text,
                    ScientificAndCulturalFieldId = Guid.Parse(cmbScientificAndCulturalField.SelectedValue),
                    PersonId = (cmbDependentPerson.SelectedIndex > 0 ? Guid.Parse(cmbDependentPerson.SelectedValue) : (Guid?)null),                    
                };
                _dossierSacrificeScientificAndCulturalChampionshipBL.Update(dossierSacrificeScientificAndCulturalChampionshipEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_ScientificAndCulturalChampionshipId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_ScientificAndCulturalChampionship_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_ScientificAndCulturalChampionshipId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    var dossierSacrificeScientificAndCulturalChampionshipEntity = new DossierSacrifice_ScientificAndCulturalChampionshipEntity()
                    {
                        DossierSacrifice_ScientificAndCulturalChampionshipId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeScientificAndCulturalChampionshipBL.Delete(dossierSacrificeScientificAndCulturalChampionshipEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_ScientificAndCulturalChampionship_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_ScientificAndCulturalChampionship_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_ScientificAndCulturalChampionship_SortCommand(object sender, GridSortCommandEventArgs e)
        {
try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ScientificAndCulturalChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

    }
}