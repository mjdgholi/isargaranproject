﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<اطلاعات  اطلاعات خدمات و تسهیلات غیر نقدی>
    /// </summary>
    public partial class DossierSacrifice_FacilityAndServiceNonCashPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly OccasionBL _occasionBL = new OccasionBL();
        private readonly FacilityAndServiceNonCashBL _facilityAndServiceNonCashBL = new FacilityAndServiceNonCashBL();
        private readonly DossierSacrifice_FacilityAndServiceNonCashBL _dossierSacrificeFacilityAndServiceNonCashBL = new DossierSacrifice_FacilityAndServiceNonCashBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_FacilityAndServiceNonCash]";
        private const string PrimaryKey = "DossierSacrifice_FacilityAndServiceNonCashId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtCashAmount.Text = "";
            txtFacilityAndServiceCashDate.Text = "";
            txtRecipientPersonName.Text = "";
            txtRecipientPersonNationalNo.Text = "";
            cmbOccasion.ClearSelection();
            cmbFacilityAndServiceNonCash.ClearSelection();


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeFacilityAndServiceNonCashEntity = new DossierSacrifice_FacilityAndServiceNonCashEntity()
            {
                DossierSacrifice_FacilityAndServiceNonCashId = Guid.Parse(ViewState["DossierSacrifice_FacilityAndServiceNonCashId"].ToString())
            };
            var mydossierSacrificeFacilityAndServiceNonCash = _dossierSacrificeFacilityAndServiceNonCashBL.GetSingleById(dossierSacrificeFacilityAndServiceNonCashEntity);
            cmbOccasion.SelectedValue = mydossierSacrificeFacilityAndServiceNonCash.OccasionId.ToString();
            if (cmbOccasion.FindItemByValue(mydossierSacrificeFacilityAndServiceNonCash.OccasionId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردعنوان مناسبت انتخاب شده معتبر نمی باشد.");
            }
            cmbFacilityAndServiceNonCash.SelectedValue = mydossierSacrificeFacilityAndServiceNonCash.FacilityAndServiceNonCashId.ToString();
            if (cmbFacilityAndServiceNonCash.FindItemByValue(mydossierSacrificeFacilityAndServiceNonCash.FacilityAndServiceNonCashId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردعنوان خدمات و تسهیلات غیر نقدی انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = mydossierSacrificeFacilityAndServiceNonCash.Description;
            txtCashAmount.Text = mydossierSacrificeFacilityAndServiceNonCash.CashAmount.ToString();
            txtFacilityAndServiceCashDate.Text = mydossierSacrificeFacilityAndServiceNonCash.FacilityAndServiceNonCashDate;
            txtRecipientPersonName.Text = mydossierSacrificeFacilityAndServiceNonCash.RecipientPersonName;
            txtRecipientPersonNationalNo.Text = mydossierSacrificeFacilityAndServiceNonCash.RecipientPersonNationalNo;




        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_FacilityAndServiceNonCash,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbOccasion.Items.Clear();
            cmbOccasion.Items.Add(new RadComboBoxItem(""));
            cmbOccasion.DataSource = _occasionBL.GetAllIsActive();
            cmbOccasion.DataTextField = "OccasionTitle";
            cmbOccasion.DataValueField = "OccasionId";
            cmbOccasion.DataBind();

            cmbFacilityAndServiceNonCash.Items.Clear();
            cmbFacilityAndServiceNonCash.Items.Add(new RadComboBoxItem(""));
            cmbFacilityAndServiceNonCash.DataSource = _facilityAndServiceNonCashBL.GetAllIsActive();
            cmbFacilityAndServiceNonCash.DataTextField = "FacilityAndServiceNonCashTitle";
            cmbFacilityAndServiceNonCash.DataValueField = "FacilityAndServiceNonCashId";
            cmbFacilityAndServiceNonCash.DataBind();
        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_FacilityAndServiceNonCashId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeFacilityAndServiceNonCashEntity = new DossierSacrifice_FacilityAndServiceNonCashEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CashAmount = (txtCashAmount.Text == "" ? (decimal?)null : decimal.Parse(txtCashAmount.Text)),
                    FacilityAndServiceNonCashDate = ItcToDate.ShamsiToMiladi(txtFacilityAndServiceCashDate.Text),
                    OccasionId = (cmbOccasion.SelectedIndex > 0 ? Guid.Parse(cmbOccasion.SelectedValue) : (Guid?)null),
                    FacilityAndServiceNonCashId = Guid.Parse(cmbFacilityAndServiceNonCash.SelectedValue),
                    RecipientPersonName = txtRecipientPersonName.Text,
                    RecipientPersonNationalNo = txtRecipientPersonNationalNo.Text,

                };
                _dossierSacrificeFacilityAndServiceNonCashBL.Add(dossierSacrificeFacilityAndServiceNonCashEntity, out DossierSacrifice_FacilityAndServiceNonCashId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, DossierSacrifice_FacilityAndServiceNonCashId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtCashAmount.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CashAmount =' " +
                                               (txtCashAmount.Text.Trim()) + "'";

                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtRecipientPersonName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and RecipientPersonName Like N'%" +
                                               FarsiToArabic.ToArabic(txtRecipientPersonName.Text.Trim()) + "%'";
                if (txtRecipientPersonNationalNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and RecipientPersonNationalNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtRecipientPersonNationalNo.Text.Trim()) + "%'";
                if (txtFacilityAndServiceCashDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FacilityAndServiceCashDate ='" +
                                               (txtFacilityAndServiceCashDate.Text.Trim()) + "'";

                if (cmbOccasion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And OccasionId='" +
                                               (cmbOccasion.SelectedValue.Trim()) + "'";
                if (cmbFacilityAndServiceNonCash.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And FacilityAndServiceNonCashId='" +
                                               (cmbFacilityAndServiceNonCash.SelectedValue.Trim()) + "'";

                grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_FacilityAndServiceNonCash.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeFacilityAndServiceNonCashEntity = new DossierSacrifice_FacilityAndServiceNonCashEntity()
                {
                    DossierSacrifice_FacilityAndServiceNonCashId = Guid.Parse(ViewState["DossierSacrifice_FacilityAndServiceNonCashId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CashAmount = (txtCashAmount.Text == "" ? (decimal?)null : decimal.Parse(txtCashAmount.Text)),
                    FacilityAndServiceNonCashDate = ItcToDate.ShamsiToMiladi(txtFacilityAndServiceCashDate.Text),
                    OccasionId = (cmbOccasion.SelectedIndex > 0 ? Guid.Parse(cmbOccasion.SelectedValue) : (Guid?)null),
                    FacilityAndServiceNonCashId = Guid.Parse(cmbFacilityAndServiceNonCash.SelectedValue),
                    RecipientPersonName = txtRecipientPersonName.Text,
                    RecipientPersonNationalNo = txtRecipientPersonNationalNo.Text,

                };
                _dossierSacrificeFacilityAndServiceNonCashBL.Update(dossierSacrificeFacilityAndServiceNonCashEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_FacilityAndServiceNonCashId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_FacilityAndServiceNonCash_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_FacilityAndServiceNonCashId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeFacilityAndServiceNonCashEntity = new DossierSacrifice_FacilityAndServiceNonCashEntity()
                    {
                        DossierSacrifice_FacilityAndServiceNonCashId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeFacilityAndServiceNonCashBL.Delete(dossierSacrificeFacilityAndServiceNonCashEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdDossierSacrifice_FacilityAndServiceNonCash_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_FacilityAndServiceNonCash_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_FacilityAndServiceNonCash_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilityAndServiceNonCash.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

    }
}