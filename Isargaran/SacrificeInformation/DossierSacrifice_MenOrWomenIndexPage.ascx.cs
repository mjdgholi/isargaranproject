﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات زن یا مرد شاخص>
    /// </summary>
    public partial class DossierSacrifice_MenOrWomenIndexPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly DossierSacrifice_MenOrWomenIndexBL _dossierSacrificeMenOrWomenIndexBL = new DossierSacrifice_MenOrWomenIndexBL();        
        private readonly OccasionBL _occasionBL = new OccasionBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_MenOrWomenIndex]";
        private const string PrimaryKey = "DossierSacrifice_MenOrWomenIndexId";
        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtIndexFactors.Text = "";
            txtOccasionDate.Text = "";           
            cmbOccasion.ClearSelection();            
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeMenOrWomenIndexEntity = new DossierSacrifice_MenOrWomenIndexEntity()
            {
                DossierSacrifice_MenOrWomenIndexId = new Guid(ViewState["DossierSacrifice_MenOrWomenIndexId"].ToString())
            };
            var mydossierSacrificeMenOrWomenIndex = _dossierSacrificeMenOrWomenIndexBL.GetSingleById(dossierSacrificeMenOrWomenIndexEntity);
            txtDescription.Text = mydossierSacrificeMenOrWomenIndex.Description;
            txtIndexFactors.Text = mydossierSacrificeMenOrWomenIndex.IndexFactors;
            txtOccasionDate.Text = mydossierSacrificeMenOrWomenIndex.OccasionDate;        
            cmbOccasion.SelectedValue = mydossierSacrificeMenOrWomenIndex.OccasionId.ToString();            
            if (cmbOccasion.FindItemByValue(mydossierSacrificeMenOrWomenIndex.OccasionId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردمناسبت انتخاب شده معتبر نمی باشد.");
            }
          
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_MenOrWomenIndex,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {


            cmbOccasion.Items.Clear();
            cmbOccasion.Items.Add(new RadComboBoxItem(""));
            cmbOccasion.DataSource = _occasionBL.GetAllIsActive();
            cmbOccasion.DataTextField = "OccasionTitle";
            cmbOccasion.DataValueField = "OccasionId";
            cmbOccasion.DataBind();

        }


        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_MenOrWomenIndexId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                var dossierSacrificeMenOrWomenIndexEntity = new DossierSacrifice_MenOrWomenIndexEntity()
                {
                    Description = txtDescription.Text,
                    IndexFactors = txtIndexFactors.Text,
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    OccasionId = Guid.Parse(cmbOccasion.SelectedValue),
                    OccasionDate = ItcToDate.ShamsiToMiladi(txtOccasionDate.Text),                    
                };

                _dossierSacrificeMenOrWomenIndexBL.Add(dossierSacrificeMenOrWomenIndexEntity, out DossierSacrifice_MenOrWomenIndexId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, DossierSacrifice_MenOrWomenIndexId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                if (txtOccasionDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDate ='" +
                                              (txtOccasionDate.Text.Trim()) + "'";


                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtIndexFactors.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and IndexFactors Like N'%" +
                                               FarsiToArabic.ToArabic(txtIndexFactors.Text.Trim()) + "%'";
                if (cmbOccasion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionId ='" +
                                               new Guid(cmbOccasion.SelectedValue) + "'";


                grdDossierSacrifice_MenOrWomenIndex.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_MenOrWomenIndex.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeMenOrWomenIndexEntity = new DossierSacrifice_MenOrWomenIndexEntity()
                {
                    DossierSacrifice_MenOrWomenIndexId = Guid.Parse(ViewState["DossierSacrifice_MenOrWomenIndexId"].ToString()),
                    Description = txtDescription.Text,
                    IndexFactors = txtIndexFactors.Text,
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    OccasionId = Guid.Parse(cmbOccasion.SelectedValue),
                    OccasionDate = ItcToDate.ShamsiToMiladi(txtOccasionDate.Text),
                };
                _dossierSacrificeMenOrWomenIndexBL.Update(dossierSacrificeMenOrWomenIndexEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_MenOrWomenIndexId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_MenOrWomenIndex_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_MenOrWomenIndexId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeMenOrWomenIndexEntity = new DossierSacrifice_MenOrWomenIndexEntity()
                    {
                        DossierSacrifice_MenOrWomenIndexId = Guid.Parse(e.CommandArgument.ToString()),                        
                    };
                    _dossierSacrificeMenOrWomenIndexBL.Delete(dossierSacrificeMenOrWomenIndexEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_MenOrWomenIndex_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_MenOrWomenIndex_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_MenOrWomenIndex_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MenOrWomenIndex.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
    }
}