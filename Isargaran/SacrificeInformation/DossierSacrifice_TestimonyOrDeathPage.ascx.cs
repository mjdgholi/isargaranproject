﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;


namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/23>
    // Description:	<اطلاعات فوت یا شهادت>
    /// </summary>
    public partial class DossierSacrifice_TestimonyOrDeathPage : ItcBaseControl
    {

        #region PublicParam:

        private readonly CityBL _cityBl = new CityBL();
        private readonly ProvinceBL _provinceBl = new ProvinceBL();
        private readonly OrganizationPhysicalChartBL _OrganizationPhysicalChartBl = new OrganizationPhysicalChartBL();
        private readonly PostBL _PostBl = new PostBL();
        private readonly CauseEventTypeBL _causeEventTypeBL = new CauseEventTypeBL();
        private readonly EducationCourseBL _educationCourseBl = new EducationCourseBL();
        private readonly EducationDegreeBL _educationDegreeBl = new EducationDegreeBL();
        private readonly TestimonyPeriodTimeBL _TestimonyPeriodTimeBL = new TestimonyPeriodTimeBL();
        private readonly MilitaryUnitDispatcherBL _MilitaryUnitDispatcherBL = new MilitaryUnitDispatcherBL();
        private readonly DossierSacrifice_TestimonyOrDeathBL _dossierSacrificeTestimonyOrDeathBL = new DossierSacrifice_TestimonyOrDeathBL();
        private readonly EducationCourse_EducationOrientationBL _educationCourseEducationOrientationBl = new EducationCourse_EducationOrientationBL();

        private const string TableName = "Isar.v_DossierSacrifice_TestimonyOrDeath";
        private const string PrimaryKey = "DossierSacrifice_TestimonyOrDeathId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            TxtDaethDate.Text = "";
            txtTombTitle.Text = "";
            txtTestimonyLocation.Text = "";
            txtVillageTitle.Text = "";
            txtPartNo.Text = "";
            txtRowNo.Text = "";
            txtGraveNo.Text = "";
            txtTestimonyLocation.Text = "";
            txtTestimonyLocation.Text = "";            
            SelectControlOrganizationPhysicalChart.KeyId = "";
            SelectControlOrganizationPhysicalChart.Title = "";
            cmbCity.ClearSelection();
            cmbPost.ClearSelection();
            cmbprovince.ClearSelection();                        
            cmbprovince.ClearSelection();
            //CmbEducationCourse.ClearSelection();
            //CmbEucationDegree.ClearSelection();
            CmbTestimonyPeriodTime.ClearSelection();
            CmbMilitaryUnitDispatcher.ClearSelection();
            cmbIsWarDisappeared.SelectedIndex = 0;
            cmbCauseEventType.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeTestimonyOrDeathEntity = new DossierSacrifice_TestimonyOrDeathEntity()
            {
                DossierSacrifice_TestimonyOrDeathId = new Guid(ViewState["DossierSacrifice_TestimonyOrDeathId"].ToString())
            };
            var myDossierSacrificeTestimonyOrDeath = _dossierSacrificeTestimonyOrDeathBL.GetSingleById(dossierSacrificeTestimonyOrDeathEntity);
            TxtDaethDate.Text = myDossierSacrificeTestimonyOrDeath.DaethDate;
            txtVillageTitle.Text = myDossierSacrificeTestimonyOrDeath.VillageTitle;
            txtTombTitle.Text = myDossierSacrificeTestimonyOrDeath.TombTitle;                     
            txtPartNo.Text = myDossierSacrificeTestimonyOrDeath.PartNo.ToString();
            txtRowNo.Text = myDossierSacrificeTestimonyOrDeath.RowNo.ToString();
            txtGraveNo.Text = myDossierSacrificeTestimonyOrDeath.GraveNo.ToString();
            txtTestimonyLocation.Text = myDossierSacrificeTestimonyOrDeath.TestimonyLocation;            
            cmbIsWarDisappeared.SelectedValue = myDossierSacrificeTestimonyOrDeath.IsWarDisappeared.ToString();
            
            if(!string.IsNullOrEmpty(myDossierSacrificeTestimonyOrDeath.OrganizationPhysicalChartId.ToString()))
            {
            var organizationPhysicalChartEntity = new OrganizationPhysicalChartEntity();
            organizationPhysicalChartEntity.OrganizationPhysicalChartId = myDossierSacrificeTestimonyOrDeath.OrganizationPhysicalChartId;
            organizationPhysicalChartEntity = _OrganizationPhysicalChartBl.GetSingleById(organizationPhysicalChartEntity);
            SelectControlOrganizationPhysicalChart.KeyId = myDossierSacrificeTestimonyOrDeath.OrganizationPhysicalChartId.ToString();
            SelectControlOrganizationPhysicalChart.Title = organizationPhysicalChartEntity.OrganizationPhysicalChartTitle;    
            }
            

            var cityId = myDossierSacrificeTestimonyOrDeath.CityId;
            var cityEntity = new CityEntity() { CityId = cityId };
            cityEntity = _cityBl.GetSingleById(cityEntity);
            var provinceId = cityEntity.ProvinceId;
            cmbprovince.SelectedValue = provinceId.ToString();
            if (cmbprovince.FindItemByValue(provinceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد استان انتخاب شده معتبر نمی باشد.");
            }
            SetCmboboxCity(provinceId);
            cmbCity.SelectedValue = myDossierSacrificeTestimonyOrDeath.CityId.ToString();
            if (cmbCity.FindItemByValue(myDossierSacrificeTestimonyOrDeath.CityId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
            }
            cmbCauseEventType.SelectedValue = myDossierSacrificeTestimonyOrDeath.CauseEventTypeId.ToString();
            if (cmbCauseEventType.FindItemByValue(myDossierSacrificeTestimonyOrDeath.CauseEventTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد علت حادثه انتخاب شده معتبر نمی باشد.");
            }
            cmbPost.SelectedValue = myDossierSacrificeTestimonyOrDeath.PostId.ToString();
            if (cmbPost.FindItemByValue(myDossierSacrificeTestimonyOrDeath.PostId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد پست قبل از شهادت انتخاب شده معتبر نمی باشد.");
            }
            //CmbEducationCourse.SelectedValue = myDossierSacrificeTestimonyOrDeath.EducationCourseId.ToString();
            //if (CmbEducationCourse.FindItemByValue(myDossierSacrificeTestimonyOrDeath.EducationCourseId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رشته تحصیلی انتخاب شده معتبر نمی باشد.");
            //}
            
            //CmbEucationDegree.SelectedValue = myDossierSacrificeTestimonyOrDeath.EucationDegreeId.ToString();
            //if (CmbEucationDegree.FindItemByValue(myDossierSacrificeTestimonyOrDeath.EucationDegreeId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مقطع تحصیلی انتخاب شده معتبر نمی باشد.");
            //}

            CmbTestimonyPeriodTime.SelectedValue = myDossierSacrificeTestimonyOrDeath.TestimonyPeriodTimeId.ToString();
            if (CmbTestimonyPeriodTime.FindItemByValue(myDossierSacrificeTestimonyOrDeath.TestimonyPeriodTimeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شناسه دوره زمانی شهادت انتخاب شده معتبر نمی باشد.");
            }

            CmbMilitaryUnitDispatcher.SelectedValue = myDossierSacrificeTestimonyOrDeath.MilitaryUnitDispatcherId.ToString();
            if (CmbMilitaryUnitDispatcher.FindItemByValue(myDossierSacrificeTestimonyOrDeath.MilitaryUnitDispatcherId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد یگان اعزام کننده انتخاب شده معتبر نمی باشد.");
            }
          
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_TestimonyOrDeath,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
       {

            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBl.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();

            cmbPost.Items.Clear();
            cmbPost.Items.Add(new RadComboBoxItem(""));
            cmbPost.DataSource = _PostBl.GetAllIsActive();
            cmbPost.DataTextField = "PostTitle";
            cmbPost.DataValueField = "PostId";
            cmbPost.DataBind();

            //CmbEucationDegree.Items.Clear();
            //CmbEucationDegree.Items.Add(new RadComboBoxItem(""));
            //CmbEucationDegree.DataSource = _educationDegreeBl.GetAllIsActive();
            //CmbEucationDegree.DataTextField = "EucationDegreePersianTitle";
            //CmbEucationDegree.DataValueField = "EucationDegreeId";
            //CmbEucationDegree.DataBind();

            //CmbEducationCourse.Items.Clear();
            //CmbEducationCourse.Items.Add(new RadComboBoxItem(""));
            //CmbEducationCourse.DataSource = _educationCourseBl.GetAllIsActive();
            //CmbEducationCourse.DataTextField = "EducationCoursePersianTitle";
            //CmbEducationCourse.DataValueField = "EducationCourseId";
            //CmbEducationCourse.DataBind();

            CmbTestimonyPeriodTime.Items.Clear();
            CmbTestimonyPeriodTime.Items.Add(new RadComboBoxItem(""));
            CmbTestimonyPeriodTime.DataTextField = "TestimonyPeriodTimeTitle";
            CmbTestimonyPeriodTime.DataValueField = "TestimonyPeriodTimeId";
            CmbTestimonyPeriodTime.DataSource = _TestimonyPeriodTimeBL.GetAllIsActive();
            CmbTestimonyPeriodTime.DataBind();

            CmbMilitaryUnitDispatcher.Items.Clear();
            CmbMilitaryUnitDispatcher.Items.Add(new RadComboBoxItem(""));
            CmbMilitaryUnitDispatcher.DataTextField = "MilitaryUnitDispatcherTitle";
            CmbMilitaryUnitDispatcher.DataValueField = "MilitaryUnitDispatcherId";
            CmbMilitaryUnitDispatcher.DataSource = _MilitaryUnitDispatcherBL.GetAllIsActive();
            CmbMilitaryUnitDispatcher.DataBind();


            cmbCauseEventType.Items.Clear();
            cmbCauseEventType.Items.Add(new RadComboBoxItem(""));
            cmbCauseEventType.DataTextField = "CauseEventTypeTitle";
            cmbCauseEventType.DataValueField = "CauseEventTypeId";
            cmbCauseEventType.DataSource = _causeEventTypeBL.GetAllIsActive();
            cmbCauseEventType.DataBind();
        }

        private void SetCmboboxCity(Guid ProvinceId)
        {
            var cityEntity = new CityEntity { ProvinceId = ProvinceId };
            cmbCity.Items.Clear();
            cmbCity.Items.Add(new RadComboBoxItem(""));
            cmbCity.DataTextField = "CityTitlePersian";
            cmbCity.DataValueField = "CityId";
            cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
            cmbCity.DataBind();
        }      

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // متد اعتبارسنجی در هنگام افزودن و ویرایش
        void AddAndEditValidation()
        {
            if (SelectControlOrganizationPhysicalChart.KeyId.Trim()=="")
            {
                throw new ItcApplicationErrorManagerException("محل خدمت سازمانی را انتخاب نمایید");
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                //AddAndEditValidation();
                var dossierSacrificeTestimonyOrDeathEntity = new DossierSacrifice_TestimonyOrDeathEntity
                                                                 {
                                                                     DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                                     DaethDate = ItcToDate.ShamsiToMiladi(TxtDaethDate.Text),
                                                                     CityId = Guid.Parse(cmbCity.SelectedValue),
                                                                     VillageTitle = txtVillageTitle.Text,
                                                                     TombTitle = txtTombTitle.Text,
                                                                     PartNo = (txtPartNo.Text.Trim() == "") ? (int?)null : Int32.Parse(txtPartNo.Text),
                                                                     RowNo = (txtRowNo.Text.Trim() == "") ? (int?)null : Int32.Parse(txtRowNo.Text),
                                                                     GraveNo = (txtGraveNo.Text.Trim() == "") ? (int?)null : Int32.Parse(txtGraveNo.Text),
                                                                     TestimonyLocation = txtTestimonyLocation.Text,
                                                                     DurationOfPresenceInBattlefield = (int?)null ,//گومانی گفت مدت حضور در جبهه نادیده گرفته شود
                                                                     CauseEventTypeId = (cmbCauseEventType.SelectedIndex>0?Guid.Parse(cmbCauseEventType.SelectedValue):(Guid?)null),
                                                                     EucationDegreeId = (Guid?)null,
                                                                     EducationCourseId = (Guid?)null,
                                                                     OrganizationPhysicalChartId =(SelectControlOrganizationPhysicalChart.KeyId!=""?Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId):(Guid?)null) ,
                                                                     PostId = (cmbPost.SelectedIndex <= 0) ? (Guid?)null : Guid.Parse(cmbPost.SelectedValue),
                                                                     MilitaryUnitDispatcherId = Guid.Parse(CmbMilitaryUnitDispatcher.SelectedValue),
                                                                     IsWarDisappeared = (cmbIsWarDisappeared.SelectedIndex > 0 ? bool.Parse(cmbIsWarDisappeared.SelectedValue) : (bool?)null),
                                                                     TestimonyPeriodTimeId = Guid.Parse(CmbTestimonyPeriodTime.SelectedValue)
                                                                 };

                _dossierSacrificeTestimonyOrDeathBL.Add(dossierSacrificeTestimonyOrDeathEntity, out dossierSacrificeEducationId);
              
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtTestimonyLocation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TestimonyLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtTestimonyLocation.Text.Trim()) + "%'";
                if (txtVillageTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VillageTitle ='" +
                                               (txtVillageTitle.Text.Trim()) + "'";
                if (txtTombTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TombTitle ='" +
                                               (txtTombTitle.Text.Trim()) + "'";
                if (txtPartNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PartNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtPartNo.Text.Trim()) + "%'";
                if (txtRowNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TestimonyLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtRowNo.Text.Trim()) + "%'";
                if (SelectControlOrganizationPhysicalChart.KeyId !="" )
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationPhysicalChartId ='" +
                                               new Guid(SelectControlOrganizationPhysicalChart.KeyId) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbPost.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PostId ='" +
                                               new Guid(cmbPost.SelectedValue) + "'";
                if (CmbMilitaryUnitDispatcher.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MilitaryUnitDispatcherId ='" +
                                               new Guid(CmbMilitaryUnitDispatcher.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and provinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                //if (CmbEducationCourse.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCourseId ='" +
                //                               new Guid(CmbEducationCourse.SelectedValue) + "'";
                //if (CmbEucationDegree.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EucationDegreeId ='" +
                //                               new Guid(CmbEucationDegree.SelectedValue) + "'";
                if (CmbTestimonyPeriodTime.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EucationModeId ='" +
                                               new Guid(CmbTestimonyPeriodTime.SelectedValue) + "'";
                if (CmbMilitaryUnitDispatcher.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MilitaryUnitDispatcherId ='" +
                                               new Guid(CmbMilitaryUnitDispatcher.SelectedValue) + "'";
                if (cmbCauseEventType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CauseEventTypeId ='" +
                                               new Guid(cmbCauseEventType.SelectedValue) + "'";

                grdDossierSacrifice_TestimonyOrDeath.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_TestimonyOrDeath.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                //AddAndEditValidation();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeStudentEntity = new DossierSacrifice_TestimonyOrDeathEntity()
                {
                    DossierSacrifice_TestimonyOrDeathId = Guid.Parse(ViewState["DossierSacrifice_TestimonyOrDeathId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    DaethDate = ItcToDate.ShamsiToMiladi(TxtDaethDate.Text),
                    CityId = Guid.Parse(cmbCity.SelectedValue),
                    VillageTitle = txtVillageTitle.Text,
                    TombTitle = txtTombTitle.Text,
                    PartNo = (txtPartNo.Text.Trim() == "") ? (int?)null : Int32.Parse(txtPartNo.Text),
                    RowNo = (txtRowNo.Text.Trim() == "") ? (int?)null : Int32.Parse(txtRowNo.Text),
                    GraveNo = (txtGraveNo.Text.Trim() == "") ? (int?)null : Int32.Parse(txtGraveNo.Text),
                    TestimonyLocation = txtTestimonyLocation.Text,
                    DurationOfPresenceInBattlefield = (int?)null,//گومانی گفت مدت حضور در جبهه نادیده گرفته شود
                    CauseEventTypeId = (cmbCauseEventType.SelectedIndex>0?Guid.Parse(cmbCauseEventType.SelectedValue):(Guid?)null),
                    EucationDegreeId = (Guid?)null,
                    EducationCourseId = (Guid?)null,
                    OrganizationPhysicalChartId = (SelectControlOrganizationPhysicalChart.KeyId != "" ? Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId) : (Guid?)null),
                    PostId = (cmbPost.SelectedIndex <= 0) ? (Guid?)null : Guid.Parse(cmbPost.SelectedValue),
                    MilitaryUnitDispatcherId = Guid.Parse(CmbMilitaryUnitDispatcher.SelectedValue),
                    IsWarDisappeared = (cmbIsWarDisappeared.SelectedIndex > 0 ? bool.Parse(cmbIsWarDisappeared.SelectedValue) : (bool?)null),
                    TestimonyPeriodTimeId = Guid.Parse(CmbTestimonyPeriodTime.SelectedValue)                
                };
                _dossierSacrificeTestimonyOrDeathBL.Update(dossierSacrificeStudentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_TestimonyOrDeathId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_TestimonyOrDeath_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_TestimonyOrDeathId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_TestimonyOrDeathEntity()
                    {
                        DossierSacrifice_TestimonyOrDeathId = new Guid(e.CommandArgument.ToString()),

                    };
                    _dossierSacrificeTestimonyOrDeathBL.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_TestimonyOrDeath_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_TestimonyOrDeath_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_TestimonyOrDeath_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TestimonyOrDeath.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }       


        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                cmbCity.Text = "";

                cmbCity.Items.Clear();
                SetCmboboxCity(Guid.Parse(e.Value.ToString()));
            }
            else
            {
                cmbCity.Items.Clear();
                cmbCity.Text = "";
            }

        }

        #endregion
    }
}