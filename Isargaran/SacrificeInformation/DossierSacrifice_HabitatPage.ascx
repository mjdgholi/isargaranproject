﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_HabitatPage.ascx.cs"
    Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_HabitatPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" ValidationGroup="DossierSacrifice_Habitat"
                                    CustomeButtonType="Add" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="DossierSacrifice_Habitat">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label runat="server" ID="lblHabitatOwnershipStatus">وضیعت مالکیت<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="15%">
                                <cc1:CustomRadComboBox ID="cmbHabitatOwnershipStatus" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" />
                                
                            </td>
                            <td width="75%">
                                <asp:RequiredFieldValidator ID="rfvHabitatOwnershipStatus" runat="server" ControlToValidate="cmbHabitatOwnershipStatus"
                                    ValidationGroup="DossierSacrifice_Habitat" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                               <tr>
                            <td >
                                <asp:Label runat="server" ID="lblHabitatEStateType">نوع سازه:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbHabitatEStateType" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"  />
      
                            </td>
                            <td>
                       <%--                                 <asp:RequiredFieldValidator ID="rfvHabitatEStateType" runat="server" ControlToValidate="cmbHabitatEStateType"
                                    ValidationGroup="DossierSacrifice_Habitat" ErrorMessage="*"></asp:RequiredFieldValidator>  --%>
                            </td>
                        </tr>
                                                <tr>
                            <td >
                                <asp:Label runat="server" ID="lblHabitatLocationType">نوع موقیعت:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbHabitatLocationType" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"  />
       
                            </td>
                            <td>
                                     <%--                    <asp:RequiredFieldValidator ID="rfvHabitatLocationType" runat="server" ControlToValidate="cmbHabitatLocationType"
                                    ValidationGroup="DossierSacrifice_Habitat" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                                                <tr>
                            <td >
                                <asp:Label runat="server" ID="lblHabitatQuality">کیفیت:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbHabitatQuality" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" />
                                
                            </td>
                            <td>
             <%--                   <asp:RequiredFieldValidator ID="rfvHabitatQuality" runat="server" ControlToValidate="cmbHabitatQuality"
                                    ValidationGroup="DossierSacrifice_Habitat" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                                                                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblAmountOfRent">مبلغ اجاره ماهیانه:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtAmountOfRent" runat="server" 
                                    ></telerik:RadTextBox>                                
                            </td>
                            <td>
                                
                            </td>
                            
                        </tr>
                                                
                                                                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblHbitatArea">متراژ:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtHbitatArea" runat="server" 
                                    ></telerik:RadTextBox>                                
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                                                                                                <tr>
                            <td>
                                <asp:Label runat="server" ID="lblHabitatDeposit">مبلغ ودیعه:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtHabitatDeposit" runat="server" 
                                    ></telerik:RadTextBox>                                
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    نیاز به تعمیر:</label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbIsBasicRepairNeed" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="دارد" Value="True" />
                                        <telerik:RadComboBoxItem Text="ندارد" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
           
                            </td>
                            <td>
            <%--                                        <asp:RequiredFieldValidator ID="rfvIsBasicRepairNeed" runat="server" ControlToValidate="cmbIsBasicRepairNeed"
                                    ValidationGroup="DossierSacrifice_Habitat" ErrorMessage="نیاز به تعمیر" InitialValue=" ">*</asp:RequiredFieldValidator> --%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblStartDate" runat="server">تاریخ شروع:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="txtStartDate" runat="server"></cc1:CustomItcCalendar>
                                
                            </td>
                            <td>
<%--                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate"
                                    ErrorMessage="*" ValidationGroup="DossierSacrifice_Habitat"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEndDate" runat="server">تاریخ پایان:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtEndDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td></td>
                        </tr>
   


                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Habitat" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" 
                                    onitemcommand="grdDossierSacrifice_Habitat_ItemCommand" 
                                    onpageindexchanged="grdDossierSacrifice_Habitat_PageIndexChanged" 
                                    onpagesizechanged="grdDossierSacrifice_Habitat_PageSizeChanged" 
                                    onsortcommand="grdDossierSacrifice_Habitat_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_HabitatId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده ایثارگری"
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HabitatEstateTypeTitle" HeaderText="عنوان نوع سازه محل اقامت"
                                                SortExpression="HabitatEstateTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HabitatLocationTypeTitle" HeaderText="عنوان موقعیت محل اقامت"
                                                SortExpression="HabitatLocationTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HabitatOwnershipStatusTitle" HeaderText="عنوان وضیعت مالکیت اقامت"
                                                SortExpression="HabitatOwnershipStatusTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HabitatQualityTitle" HeaderText="وضیعت کیفیت محل اقامت"
                                                SortExpression="HabitatQualityTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AmountOfRent" HeaderText="مبلغ اجارخ ماهیانه"
                                                SortExpression="AmountOfRent">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HabitatDeposit" HeaderText="مبلغ ودیعه" SortExpression="HabitatDeposit">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HbitatArea" HeaderText="متراژ" SortExpression="HbitatArea">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StartDate" HeaderText="تاریخ شروع" SortExpression="StartDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EndDate" HeaderText="تاریخ پایان" SortExpression="EndDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsBasicRepairNeed" HeaderText="نیاز به تعمیر"
                                                SortExpression="IsBasicRepairNeed">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                              <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_HabitatId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_HabitatId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_HabitatId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbsacrifice">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbsacrifice" />
                <telerik:AjaxUpdatedControl ControlID="pnlDuration" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="pnlPercent" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Habitat">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" >
</telerik:RadAjaxLoadingPanel>
