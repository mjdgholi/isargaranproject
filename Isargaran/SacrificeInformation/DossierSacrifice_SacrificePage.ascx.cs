﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/16>
    /// Description: <پرونده  ایثارگری ایثارگری>
    /// </summary>
    public partial class DossierSacrifice_SacrificePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly SacrificeBL _sacrificeBL = new SacrificeBL();
        private  readonly  DurationTypeBL _durationTypeBL=new DurationTypeBL();
        private readonly DossierSacrifice_SacrificeBL _dossierSacrificeSacrificeBL = new DossierSacrifice_SacrificeBL();
        private const string TableName = "Isar.V_DossierSacrifice_Sacrifice";
        private const string PrimaryKey = "DossierSacrifice_SacrificeId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEndDate.Text = "";
            txtPercentValue.Text = "";
            txtStartDate.Text = "";
            cmbsacrifice.ClearSelection();
            txtValueInDay.Text = "";
            txtValueInMonth.Text = "";
            txtValueInYear.Text = "";
            cmbIsActive.SelectedIndex = 1;
            cmbDurationtype.ClearSelection();

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeSacrificeEntity = new DossierSacrifice_SacrificeEntity()
            {
                DossierSacrifice_SacrificeId = Guid.Parse(ViewState["DossierSacrifice_SacrificeId"].ToString())
            };
            var mydossierSacrificeSacrifice = _dossierSacrificeSacrificeBL.GetSingleById(dossierSacrificeSacrificeEntity);                        
            cmbIsActive.SelectedValue = mydossierSacrificeSacrifice.IsActive.ToString();
            cmbsacrifice.SelectedValue = mydossierSacrificeSacrifice.SacrificeId.ToString();
            txtStartDate.Text = mydossierSacrificeSacrifice.StartDate;
            txtEndDate.Text = mydossierSacrificeSacrifice.Endate;
            if (cmbsacrifice.FindItemByValue(mydossierSacrificeSacrifice.SacrificeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد ایثارگر انتخاب شده معتبر نمی باشد.");
            }
            CheckVisiblePanel(mydossierSacrificeSacrifice.SacrificeId);
            txtPercentValue.Text = mydossierSacrificeSacrifice.PercentValue.ToString();
            txtValueInDay.Text = mydossierSacrificeSacrifice.ValueInDay.ToString();
            txtValueInMonth.Text = mydossierSacrificeSacrifice.ValueInMonth.ToString();
            txtValueInYear.Text = mydossierSacrificeSacrifice.ValueInYear.ToString();
            cmbDurationtype.SelectedValue = mydossierSacrificeSacrifice.DurationTypeId.ToString();

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Sacrifice,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbsacrifice.Items.Clear();
            cmbsacrifice.Items.Add(new RadComboBoxItem(""));
            cmbsacrifice.DataSource = _sacrificeBL.GetAllIsActive();
            cmbsacrifice.DataTextField = "sacrificeTitle";
            cmbsacrifice.DataValueField = "sacrificeId";
            cmbsacrifice.DataBind();

            cmbDurationtype.Items.Clear();
            cmbDurationtype.Items.Add(new RadComboBoxItem(""));
            cmbDurationtype.DataSource = _durationTypeBL.GetAllIsActive();
            cmbDurationtype.DataTextField = "DurationtypeTitle";
            cmbDurationtype.DataValueField = "DurationtypeId";
            cmbDurationtype.DataBind();



        }

        void CheckVisiblePanel(Guid SacrificeId)
        {
            SacrificeBL _sacrificeBL = new SacrificeBL();
            var sacrificeEntity = new SacrificeEntity()
            {
                SacrificeId = new Guid(SacrificeId.ToString())
            };
            var mysacrifice = _sacrificeBL.GetSingleById(sacrificeEntity);
            if (mysacrifice.IsPercentValue)
            {
                pnlPercent.Visible = true;
                pnlDuration.Visible = false;
            }
            if (mysacrifice.IsTimeValue)
            {
                pnlDuration.Visible = true;
                pnlPercent.Visible = false;
            }
        }

        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'"; 
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_SacrificeId  ;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var provinceEDossierSacrificeSacrificeEntityntity = new DossierSacrifice_SacrificeEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    SacrificeId = Guid.Parse(cmbsacrifice.SelectedValue),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    Endate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),                                 
                    PercentValue = (txtPercentValue.Text !="")?decimal.Parse(txtPercentValue.Text):(decimal?)null,
                    ValueInDay = (txtValueInDay.Text!="")?int.Parse(txtValueInDay.Text):(int?)null,
                    ValueInMonth = (txtValueInMonth.Text!="")?int.Parse(txtValueInMonth.Text):(int?)null,
                    ValueInYear = (txtValueInYear.Text!="")?int.Parse(txtValueInYear.Text):(int?)null,
                    DurationTypeId= (cmbDurationtype.SelectedIndex>0) ? Guid.Parse(cmbDurationtype.SelectedValue) : (Guid?)null,  
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),                    
                };

                _dossierSacrificeSacrificeBL.Add(provinceEDossierSacrificeSacrificeEntityntity, out DossierSacrifice_SacrificeId );
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, DossierSacrifice_SacrificeId );
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate =' " +
                                               (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate =' " +
                                               (txtEndDate.Text.Trim()) + "'";
                if (txtPercentValue.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PercentValue =' " +
                                               (txtPercentValue.Text.Trim()) + "'";
                if (txtValueInDay.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ValueInDay =' " +
                                               (txtValueInDay.Text.Trim()) + "'";
                if (txtValueInMonth.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ValueInMonth =' " +
                                               (txtValueInMonth.Text.Trim()) + "'";
                if (txtValueInYear.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ValueInYear =' " +
                                               (txtValueInYear.Text.Trim()) + "'";
                if (cmbDurationtype.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DurationtypeId ='" +
                                               new Guid(cmbDurationtype.SelectedValue) + "'";
                if (cmbsacrifice.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and sacrificeId ='" +
                                               new Guid(cmbsacrifice.SelectedValue) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdDossierSacrifice_Sacrifice.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Sacrifice.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeSacrificeEntity = new DossierSacrifice_SacrificeEntity()
                {
                    DossierSacrifice_SacrificeId = Guid.Parse(ViewState["DossierSacrifice_SacrificeId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    SacrificeId = Guid.Parse(cmbsacrifice.SelectedValue),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    Endate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    PercentValue = (txtPercentValue.Text != "") ? decimal.Parse(txtPercentValue.Text) : (decimal?)null,
                    ValueInDay = (txtValueInDay.Text != "") ? int.Parse(txtValueInDay.Text) : (int?)null,
                    ValueInMonth = (txtValueInMonth.Text != "") ? int.Parse(txtValueInMonth.Text) : (int?)null,
                    ValueInYear = (txtValueInYear.Text != "") ? int.Parse(txtValueInYear.Text) : (int?)null,
                    DurationTypeId = (cmbDurationtype.SelectedIndex > 0) ? Guid.Parse(cmbDurationtype.SelectedValue) : (Guid?)null,
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };
                _dossierSacrificeSacrificeBL.Update(dossierSacrificeSacrificeEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_SacrificeId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_Sacrifice_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_SacrificeId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                    var dossierSacrificeSacrificeEntity = new DossierSacrifice_SacrificeEntity()
                    {
                        DossierSacrifice_SacrificeId = Guid.Parse(e.CommandArgument.ToString()),                        
                    };
                    _dossierSacrificeSacrificeBL.Delete(dossierSacrificeSacrificeEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdDossierSacrifice_Sacrifice_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_Sacrifice_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_Sacrifice_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Sacrifice.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }
      
        protected void cmbsacrifice_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CheckVisiblePanel(Guid.Parse(e.Value.ToString()));
        }
        #endregion


    }
}