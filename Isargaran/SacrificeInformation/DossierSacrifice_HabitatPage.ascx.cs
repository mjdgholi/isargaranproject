﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/18>
    /// Description: <پرونده مشخصات محل اقامت ایثارگری>
    /// </summary>    
    public partial class DossierSacrifice_HabitatPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly HabitatEstateTypeBL _habitatEstateTypeBL = new HabitatEstateTypeBL();
        private readonly HabitatLocationTypeBL _habitatLocationTypeBL = new HabitatLocationTypeBL();
        private readonly HabitatOwnershipStatusBL _habitatOwnershipStatusBL = new HabitatOwnershipStatusBL();
        private readonly HabitatQualityBL _habitatQualityBL = new HabitatQualityBL();
        private readonly DossierSacrifice_HabitatBL _dossierSacrificeHabitatBL = new DossierSacrifice_HabitatBL();
        private const string TableName = "Isar.V_DossierSacrifice_Habitat";
        private const string PrimaryKey = "DossierSacrifice_HabitatId";
        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEndDate.Text = "";
            txtAmountOfRent.Text = "";
            txtEndDate.Text = "";
            txtHabitatDeposit.Text = "";
            txtHbitatArea.Text = "";
            txtStartDate.Text = "";                      
            cmbHabitatEStateType.ClearSelection();
            cmbHabitatLocationType.ClearSelection();
            cmbHabitatOwnershipStatus.ClearSelection();
            cmbHabitatQuality.ClearSelection();
            cmbIsBasicRepairNeed.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeHabitatEntity = new DossierSacrifice_HabitatEntity()
            {
                DossierSacrifice_HabitatId = new Guid(ViewState["DossierSacrifice_HabitatId"].ToString())
            };
            var myDossierSacrificeHabitat = _dossierSacrificeHabitatBL.GetSingleById(dossierSacrificeHabitatEntity);
            txtEndDate.Text = myDossierSacrificeHabitat.EndDate;
            txtStartDate.Text = myDossierSacrificeHabitat.StartDate;
            cmbIsBasicRepairNeed.SelectedValue = myDossierSacrificeHabitat.IsBasicRepairNeed.ToString();
            cmbHabitatEStateType.SelectedValue = myDossierSacrificeHabitat.HabitatEStateTypeId.ToString();
            if (cmbHabitatEStateType.FindItemByValue(myDossierSacrificeHabitat.HabitatEStateTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع سازه انتخاب شده معتبر نمی باشد.");
            }
            cmbHabitatLocationType.SelectedValue = myDossierSacrificeHabitat.HabitatLocationTypeId.ToString();
            if (cmbHabitatLocationType.FindItemByValue(myDossierSacrificeHabitat.HabitatLocationTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع موقیعت انتخاب شده معتبر نمی باشد.");
            }            
            cmbHabitatOwnershipStatus.SelectedValue = myDossierSacrificeHabitat.HabitatOwnershipStatusId.ToString();
            if (cmbHabitatOwnershipStatus.FindItemByValue(myDossierSacrificeHabitat.HabitatOwnershipStatusId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد ماکیت انتخاب شده معتبر نمی باشد.");
            }
            cmbHabitatQuality.SelectedValue = myDossierSacrificeHabitat.HabitatQualityId.ToString();
            if (cmbHabitatQuality.FindItemByValue(myDossierSacrificeHabitat.HabitatQualityId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد کیفیت انتخاب شده معتبر نمی باشد.");
            }
           


        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Habitat,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbHabitatEStateType.Items.Clear();
            cmbHabitatEStateType.Items.Add(new RadComboBoxItem(""));
            cmbHabitatEStateType.DataSource = _habitatEstateTypeBL.GetAllIsActive();
            cmbHabitatEStateType.DataTextField = "HabitatEstateTypeTitle";
            cmbHabitatEStateType.DataValueField = "HabitatEstateTypeId";
            cmbHabitatEStateType.DataBind();

            cmbHabitatLocationType.Items.Clear();
            cmbHabitatLocationType.Items.Add(new RadComboBoxItem(""));
            cmbHabitatLocationType.DataSource = _habitatLocationTypeBL.GetAllIsActive();
            cmbHabitatLocationType.DataTextField = "HabitatLocationTypeTitle";
            cmbHabitatLocationType.DataValueField = "HabitatLocationTypeId";
            cmbHabitatLocationType.DataBind();

            cmbHabitatOwnershipStatus.Items.Clear();
            cmbHabitatOwnershipStatus.Items.Add(new RadComboBoxItem(""));
            cmbHabitatOwnershipStatus.DataSource = _habitatOwnershipStatusBL.GetAllIsActive();
            cmbHabitatOwnershipStatus.DataTextField = "HabitatOwnershipStatusTitle";
            cmbHabitatOwnershipStatus.DataValueField = "HabitatOwnershipStatusId";
            cmbHabitatOwnershipStatus.DataBind();

            cmbHabitatQuality.Items.Clear();
            cmbHabitatQuality.Items.Add(new RadComboBoxItem(""));
            cmbHabitatQuality.DataSource = _habitatQualityBL.GetAllIsActive();
            cmbHabitatQuality.DataTextField = "HabitatQualityTitle";
            cmbHabitatQuality.DataValueField = "HabitatQualityId";
            cmbHabitatQuality.DataBind();

        }
        
        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_HabitatId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                var dossierSacrificeHabitatEntity = new DossierSacrifice_HabitatEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    HabitatEStateTypeId = (cmbHabitatEStateType.SelectedIndex>0?Guid.Parse(cmbHabitatEStateType.SelectedValue):new Guid()),
                    HabitatLocationTypeId = (cmbHabitatLocationType.SelectedIndex>0?Guid.Parse(cmbHabitatLocationType.SelectedValue):new Guid()),
                    HabitatOwnershipStatusId = Guid.Parse(cmbHabitatOwnershipStatus.SelectedValue),
                    HabitatQualityId = (cmbHabitatQuality.SelectedIndex>0?Guid.Parse(cmbHabitatQuality.SelectedValue):new Guid()),
                    IsBasicRepairNeed = (cmbIsBasicRepairNeed.SelectedIndex>0?bool.Parse(cmbIsBasicRepairNeed.SelectedItem.Value):(bool?)null),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    AmountOfRent = (txtAmountOfRent.Text==""?(decimal?)null:decimal.Parse(txtAmountOfRent.Text)),
                    HabitatDeposit = (txtHabitatDeposit.Text==""?(decimal?)null:decimal.Parse(txtHabitatDeposit.Text)),
                    HbitatArea = (txtHbitatArea.Text==""?(decimal?)null:decimal.Parse(txtHbitatArea.Text))                    
                };

                _dossierSacrificeHabitatBL.Add(dossierSacrificeHabitatEntity, out DossierSacrifice_HabitatId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, DossierSacrifice_HabitatId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtAmountOfRent.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and AmountOfRent ='" +
                                               Decimal.Parse(txtAmountOfRent.Text.Trim()) + "'";
                if (txtHabitatDeposit.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HabitatDeposit ='" +
                                               Decimal.Parse(txtHabitatDeposit.Text.Trim()) + "'";
                if (txtHbitatArea.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HbitatArea ='" +
                                               Decimal.Parse(txtHbitatArea.Text.Trim()) + "'";
                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                              (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                              (txtEndDate.Text.Trim()) + "'";
             
                if (cmbHabitatEStateType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HabitatEStateTypeId ='" +
                                               new Guid(cmbHabitatEStateType.SelectedValue) + "'";
                if (cmbHabitatLocationType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and LocationTypeId ='" +
                                               new Guid(cmbHabitatLocationType.SelectedValue) + "'";
                if (cmbHabitatOwnershipStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HabitatOwnershipStatusId ='" +
                                               new Guid(cmbHabitatOwnershipStatus.SelectedValue) + "'";
                if (cmbHabitatQuality.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HabitatQualityId ='" +
                                               new Guid(cmbHabitatQuality.SelectedValue) + "'";
                if (cmbHabitatEStateType.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And HabitatEStateTypeId='" +
                                               (cmbHabitatEStateType.SelectedItem.Value.Trim()) + "'";
                                
                grdDossierSacrifice_Habitat.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Habitat.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeHabitatEntity = new DossierSacrifice_HabitatEntity()
                {
                    DossierSacrifice_HabitatId = Guid.Parse(ViewState["DossierSacrifice_HabitatId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    HabitatEStateTypeId = (cmbHabitatEStateType.SelectedIndex > 0 ? Guid.Parse(cmbHabitatEStateType.SelectedValue) : new Guid()),
                    HabitatLocationTypeId = (cmbHabitatLocationType.SelectedIndex > 0 ? Guid.Parse(cmbHabitatLocationType.SelectedValue) : new Guid()),
                    HabitatOwnershipStatusId = Guid.Parse(cmbHabitatOwnershipStatus.SelectedValue),
                    HabitatQualityId = (cmbHabitatQuality.SelectedIndex > 0 ? Guid.Parse(cmbHabitatQuality.SelectedValue) : new Guid()),
                    IsBasicRepairNeed = (cmbIsBasicRepairNeed.SelectedIndex > 0 ? bool.Parse(cmbIsBasicRepairNeed.SelectedItem.Value) : (bool?)null),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    AmountOfRent = (txtAmountOfRent.Text == "" ? (decimal?)null : decimal.Parse(txtAmountOfRent.Text)),
                    HabitatDeposit = (txtHabitatDeposit.Text == "" ? (decimal?)null : decimal.Parse(txtHabitatDeposit.Text)),
                    HbitatArea = (txtHbitatArea.Text == "" ? (decimal?)null : decimal.Parse(txtHbitatArea.Text))   
                };
                _dossierSacrificeHabitatBL.Update(dossierSacrificeHabitatEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_HabitatId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_Habitat_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_HabitatId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeHabitatEntity = new DossierSacrifice_HabitatEntity()
                    {
                        DossierSacrifice_HabitatId = Guid.Parse(e.CommandArgument.ToString()),                        
                    };
                    _dossierSacrificeHabitatBL.Delete(dossierSacrificeHabitatEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_Habitat_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_Habitat_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_Habitat_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Habitat.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion



    }
}