﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_RequestHistoryPage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_RequestHistoryPage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
       
                   <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="dossierSacrifice_RequestHistory">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="dossierSacrifice_RequestHistory">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
        
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">

                                                                                                <tr>
                            <td  nowrap="nowrap" width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblStatus" runat="server">وضیعت درخواست<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="10%">
                                <cc1:CustomRadComboBox ID="cmbStatus" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                                 <asp:RequiredFieldValidator ID="rfvStatu" runat="server" ControlToValidate="cmbStatus"
                                    ValidationGroup="dossierSacrifice_RequestHistory" ErrorMessage="" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                                                        <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblActionType" runat="server">نوع اقدام<font color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbActionType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                             <asp:RequiredFieldValidator ID="rfvActionType" runat="server" ControlToValidate="cmbActionType"
                                    ValidationGroup="dossierSacrifice_RequestHistory" ErrorMessage="" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                                     <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblActionDate" runat="server">تاریخ اقدام<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar runat="server" ID="txtActionDate"/>
                            </td>
                            <td>
                                          <asp:RequiredFieldValidator ID="rfvActionDate" runat="server" ControlToValidate="txtActionDate"
                                    ValidationGroup="dossierSacrifice_RequestHistory" ErrorMessage="" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                                     <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblActionDescription" runat="server">متن اقدام<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtActionDescription" runat="server" Width="300" Height="50" TextMode="MultiLine" ></asp:TextBox>
                            </td>
                            <td>
                                          <asp:RequiredFieldValidator ID="rfvActionDescription" runat="server" ControlToValidate="txtActionDescription"
                                    ValidationGroup="dossierSacrifice_RequestHistory" ErrorMessage="" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>

                                                  
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grddossierSacrifice_RequestHistoryHistory" 
                                    runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grddossierSacrifice_RequestHistoryHistory_ItemCommand" OnPageIndexChanged="grddossierSacrifice_RequestHistoryHistory_PageIndexChanged" OnPageSizeChanged="grddossierSacrifice_RequestHistoryHistory_PageSizeChanged" OnSortCommand="grddossierSacrifice_RequestHistoryHistory_SortCommand" >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_RequestHistoryId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
           
                                            <telerik:GridBoundColumn DataField="StatusTitle" HeaderText="عنوان وضیعت"
                                                SortExpression="StatusTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ActionTypeTitle" HeaderText="عنوان نوع اقدام" SortExpression="ActionTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ActionDate" HeaderText="تاریخ اقدام" SortExpression="ActionDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            
                                              <telerik:GridTemplateColumn HeaderText="متن اقدام" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="ActionDescription">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server"  Text='<%#Eval("ActionDescription").ToString() %>'
                                                        TextMode="MultiLine" BackColor="#FFFFCC"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                                                            <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_RequestHistoryId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_RequestHistoryId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_RequestHistoryId").ToString() %>'
                                                        CommandName="_MyِDelete" ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grddossierSacrifice_RequestHistoryHistory">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
