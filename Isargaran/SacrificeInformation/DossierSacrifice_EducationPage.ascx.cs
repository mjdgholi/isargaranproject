﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/14>
    /// Description: <پرونده  تحصیلی ایثارگری>
    /// </summary>
    public partial class DossierSacrifice_EducationPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly CityBL _cityBl = new CityBL();
        private readonly ProvinceBL _provinceBl = new ProvinceBL();
        private readonly AdmissionTypeBL _admissionTypeBl = new AdmissionTypeBL();
        private readonly EducationCenterBL _educationCenterBl = new EducationCenterBL();
        private readonly EducationCourseBL _educationCourseBl = new EducationCourseBL();
        private readonly EducationDegreeBL _educationDegreeBl = new EducationDegreeBL();

        private readonly EducationSystemBL _educationSystemBL = new EducationSystemBL();
        private readonly EducationOrientationBL _educationOrientationBl = new EducationOrientationBL();
        private readonly EducationCourse_EducationOrientationBL _educationCourseEducationOrientationBl = new EducationCourse_EducationOrientationBL();
        private readonly DossierSacrifice_EducationBL _dossierSacrificeEducationBl = new DossierSacrifice_EducationBL();

        private const string TableName = "Isar.V_DossierSacrifice_Education";
        private const string PrimaryKey = "DossierSacrifice_EducationId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEndDate.Text = "";
            txtFirstAdvisor.Text = "";
            txtFirstSupervisor.Text = "";
            txtGradePointAverage.Text = "";
            txtSecondAdvisor.Text = "";
            txtSecondSupervisor.Text = "";
            txtStartDate.Text = "";
            txtStudentNo.Text = "";
            txtTechnicalDescription.Text = "";
            txtThesisSubject.Text = "";
            cmbAdmissionType.ClearSelection();
            cmbCity.ClearSelection();
            cmbCity.Items.Clear();
            cmbEducationCenter.ClearSelection();
            cmbprovince.ClearSelection();
            cmbEducationOrientation.ClearSelection();
            CmbEducationCourse.ClearSelection();
            cmbprovince.ClearSelection();
            CmbEucationDegree.ClearSelection(); 
            cmbEducationSystem.ClearSelection();
            

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeEducationEntity = new DossierSacrifice_EducationEntity()
            {
                DossierSacrifice_EducationId = new Guid(ViewState["DossierSacrifice_EducationId"].ToString())
            };
            var mydossierSacrificeEducation = _dossierSacrificeEducationBl.GetSingleById(dossierSacrificeEducationEntity);
            txtEndDate.Text = mydossierSacrificeEducation.EndDate;
            txtFirstAdvisor.Text = mydossierSacrificeEducation.FirstAdvisor;
            txtFirstSupervisor.Text = mydossierSacrificeEducation.FirstSupervisor;
            txtGradePointAverage.Text = mydossierSacrificeEducation.GradePointAverage.ToString();
            txtSecondAdvisor.Text = mydossierSacrificeEducation.SecondAdvisor;
            txtSecondSupervisor.Text = mydossierSacrificeEducation.SecondSupervisor;
            txtStartDate.Text = mydossierSacrificeEducation.StartDate;
            txtStudentNo.Text = mydossierSacrificeEducation.StudentNo;
            txtTechnicalDescription.Text = mydossierSacrificeEducation.TechnicalDescription;
            txtThesisSubject.Text = mydossierSacrificeEducation.ThesisSubject;
            cmbAdmissionType.SelectedValue = mydossierSacrificeEducation.AdmissionTypeId.ToString();
            if (cmbAdmissionType.FindItemByValue(mydossierSacrificeEducation.AdmissionTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع پذیرش انتخاب شده معتبر نمی باشد.");
            }
            cmbprovince.SelectedValue = mydossierSacrificeEducation.ProvinceId.ToString();
            if (cmbprovince.FindItemByValue(mydossierSacrificeEducation.ProvinceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
            }
            if (!string.IsNullOrEmpty(cmbprovince.SelectedValue))
            {
                SetCmboboxCity(Guid.Parse(mydossierSacrificeEducation.ProvinceId.ToString()));
                cmbCity.SelectedValue = mydossierSacrificeEducation.CityId.ToString();
                if (cmbCity.FindItemByValue(mydossierSacrificeEducation.CityId.ToString()) == null)
                {
                    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
                }  
            }
            EducationDegreeEntity myEducationDegreeEntity = new EducationDegreeEntity()
            {
                EucationDegreeId = mydossierSacrificeEducation.EucationDegreeId
            };
            myEducationDegreeEntity = _educationDegreeBl.GetSingleById(myEducationDegreeEntity);
            cmbEducationSystem.SelectedValue = myEducationDegreeEntity.EducationSystemId.ToString();
            SetCmbEucationDegree(Guid.Parse(myEducationDegreeEntity.EducationSystemId.ToString()));
            CmbEucationDegree.SelectedValue = mydossierSacrificeEducation.EucationDegreeId.ToString();
            cmbEducationCenter.SelectedValue = mydossierSacrificeEducation.EducationCenterId.ToString();
            if (cmbEducationCenter.FindItemByValue(mydossierSacrificeEducation.EducationCenterId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مراکز آموزشی انتخاب شده معتبر نمی باشد.");
            }
            CmbEducationCourse.SelectedValue = mydossierSacrificeEducation.EducationCourseId.ToString();
            if (CmbEducationCourse.FindItemByValue(mydossierSacrificeEducation.EducationCourseId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردرشته تحصیلی انتخاب شده معتبر نمی باشد.");
            }  
            SetCmboboxEducationOrientation(mydossierSacrificeEducation.EducationCourseId);
            cmbEducationOrientation.SelectedValue = mydossierSacrificeEducation.EducationOrientationId.ToString();
            if (cmbEducationOrientation.FindItemByValue(mydossierSacrificeEducation.EducationOrientationId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردگرایش تحصیلی انتخاب شده معتبر نمی باشد.");
            }        
   
           
            CmbEucationDegree.SelectedValue = mydossierSacrificeEducation.EucationDegreeId.ToString();

            if (CmbEucationDegree.FindItemByValue(mydossierSacrificeEducation.EucationDegreeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردمقطع تحصیلی انتخاب شده معتبر نمی باشد.");
            }   
      

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Education,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbAdmissionType.Items.Clear();
            cmbAdmissionType.Items.Add(new RadComboBoxItem(""));
            cmbAdmissionType.DataSource = _admissionTypeBl.GetAllIsActive();
            cmbAdmissionType.DataTextField = "AdmissionTypeTitle";
            cmbAdmissionType.DataValueField = "AdmissionTypeId";
            cmbAdmissionType.DataBind();

            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBl.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();

            cmbEducationCenter.Items.Clear();
            cmbEducationCenter.Items.Add(new RadComboBoxItem(""));
            cmbEducationCenter.DataSource = _educationCenterBl.GetAllIsActive();
            cmbEducationCenter.DataTextField ="EducationCenterPersianTitle";
            cmbEducationCenter.DataValueField = "EducationCenterId";
            cmbEducationCenter.DataBind();


            cmbEducationSystem.Items.Clear();
            cmbEducationSystem.Items.Add(new RadComboBoxItem(""));
            cmbEducationSystem.DataSource = _educationSystemBL.GetAllIsActive();
            cmbEducationSystem.DataTextField = "EducationSystemPersianTitle";
            cmbEducationSystem.DataValueField = "EducationSystemId";
            cmbEducationSystem.DataBind();

            CmbEducationCourse.Items.Clear();
            CmbEducationCourse.Items.Add(new RadComboBoxItem(""));
            CmbEducationCourse.DataSource = _educationCourseBl.GetAllIsActive();
            CmbEducationCourse.DataTextField = "EducationCoursePersianTitle";
            CmbEducationCourse.DataValueField = "EducationCourseId";
            CmbEducationCourse.DataBind();


        }
        private void SetCmboboxCity(Guid ProvinceId)
        {

            var cityEntity = new CityEntity { ProvinceId = ProvinceId };
            cmbCity.Items.Clear();
            cmbCity.Items.Add(new RadComboBoxItem(""));
            cmbCity.DataTextField = "CityTitlePersian";
            cmbCity.DataValueField = "CityId";
            cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
            cmbCity.DataBind();
        }
        private void SetCmbEucationDegree(Guid educationSystemId)
        {

            CmbEucationDegree.Items.Clear();
            CmbEucationDegree.Items.Add(new RadComboBoxItem(""));
            EducationDegreeEntity educationDegreeEntity=new EducationDegreeEntity()
            {
                EducationSystemId = educationSystemId
            };
            CmbEucationDegree.DataSource = _educationDegreeBl.GetEducationDegreeCollectionByEducationSystem(educationDegreeEntity);
            CmbEucationDegree.DataTextField = "EucationDegreePersianTitle";
            CmbEucationDegree.DataValueField = "EucationDegreeId";
            CmbEucationDegree.DataBind();
        }
        private void SetCmboboxEducationOrientation(Guid? EducationCourseId)
        {

            var educationCourseEducationOrientationEntity = new EducationCourse_EducationOrientationEntity() { EducationCourseId = (EducationCourseId==null?(Guid?)null:EducationCourseId) };
            cmbEducationOrientation.Items.Clear();
            cmbEducationOrientation.Items.Add(new RadComboBoxItem(""));
            cmbEducationOrientation.DataTextField = "EducationOrientationPersianTitle";
            cmbEducationOrientation.DataValueField = "EducationOreintionId";
            cmbEducationOrientation.DataSource = _educationCourseEducationOrientationBl.GetEducationCourse_EducationOrientationCollectionByEducationCourse(educationCourseEducationOrientationEntity);
            cmbEducationOrientation.DataBind();
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString())+"'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeEducationEntity = new DossierSacrifice_EducationEntity()
                {
                   AdmissionTypeId = (cmbAdmissionType.SelectedIndex>0?Guid.Parse(cmbAdmissionType.SelectedValue):(Guid?)null),
                   CityId = (cmbCity.SelectedIndex>0?Guid.Parse(cmbCity.SelectedValue):(Guid?)null),
                   EducationCenterId = (cmbEducationCenter.SelectedIndex>0?Guid.Parse(cmbEducationCenter.SelectedValue):(Guid?)null),
                   EducationOrientationId = (cmbEducationOrientation.SelectedIndex>0?Guid.Parse(cmbEducationOrientation.SelectedValue):(Guid?)null),
                   EucationDegreeId = Guid.Parse(CmbEucationDegree.SelectedValue),
                   DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                   EducationCourseId = (CmbEducationCourse.SelectedIndex>0?Guid.Parse(CmbEducationCourse.SelectedValue):(Guid?)null),
                   StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                   EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                   FirstAdvisor = txtFirstAdvisor.Text,
                   GradePointAverage =(txtGradePointAverage.Text!="") ? decimal.Parse(txtGradePointAverage.Text) : (decimal?)null,
                   FirstSupervisor = txtFirstSupervisor.Text,
                   SecondAdvisor = txtSecondAdvisor.Text,
                   SecondSupervisor = txtSecondSupervisor.Text,
                   StudentNo = txtStudentNo.Text,
                   TechnicalDescription = txtTechnicalDescription.Text,
                   ThesisSubject = txtThesisSubject.Text,
                   
                };

                _dossierSacrificeEducationBl.Add(dossierSacrificeEducationEntity, out dossierSacrificeEducationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtFirstAdvisor.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FirstAdvisor Like N'%" +
                                               FarsiToArabic.ToArabic(txtFirstAdvisor.Text.Trim()) + "%'";
                if (txtFirstSupervisor.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FirstSupervisor Like N'%" +
                                               FarsiToArabic.ToArabic(txtFirstSupervisor.Text.Trim()) + "%'";
                if (txtGradePointAverage.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and GradePointAverage ='" +
                                               Decimal.Parse(txtGradePointAverage.Text.Trim()) + "'";
                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                              (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                              (txtEndDate.Text.Trim()) + "'";
                if (txtSecondAdvisor.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SecondAdvisor Like N'%" +
                                               FarsiToArabic.ToArabic(txtSecondAdvisor.Text.Trim()) + "%'";
                if (txtSecondSupervisor.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SecondSupervisor Like N'%" +
                                               FarsiToArabic.ToArabic(txtSecondSupervisor.Text.Trim()) + "%'";
                if (txtStudentNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StudentNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtStudentNo.Text.Trim()) + "%'";
                if (txtTechnicalDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FirstAdvisor Like N'%" +
                                               FarsiToArabic.ToArabic(txtTechnicalDescription.Text.Trim()) + "%'";
                if (txtThesisSubject.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ThesisSubject Like N'%" +
                                               FarsiToArabic.ToArabic(txtThesisSubject.Text.Trim()) + "%'";
                if (cmbAdmissionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and AdmissionTypeId ='" +
                                               new Guid(cmbAdmissionType.SelectedValue) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbEducationCenter.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCenterId ='" +
                                               new Guid(cmbEducationCenter.SelectedValue) + "'";
                if (cmbEducationOrientation.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationOrientationId ='" +
                                               new Guid(cmbEducationOrientation.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and provinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                if (CmbEducationCourse.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCourseId ='" +
                                               new Guid(CmbEducationCourse.SelectedValue) + "'";
                if (CmbEucationDegree.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EucationDegreeId ='" +
                                               new Guid(CmbEucationDegree.SelectedValue) + "'";

                grdDossierSacrifice_Education.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Education.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeEducationEntity = new DossierSacrifice_EducationEntity()
                {
                    DossierSacrifice_EducationId = Guid.Parse(ViewState["DossierSacrifice_EducationId"].ToString()),
                    AdmissionTypeId = (cmbAdmissionType.SelectedIndex > 0 ? Guid.Parse(cmbAdmissionType.SelectedValue) : (Guid?)null),
                    CityId = (cmbCity.SelectedIndex > 0 ? Guid.Parse(cmbCity.SelectedValue) : (Guid?)null),
                    EducationCenterId = (cmbEducationCenter.SelectedIndex > 0 ? Guid.Parse(cmbEducationCenter.SelectedValue) : (Guid?)null),
                    EducationOrientationId = (cmbEducationOrientation.SelectedIndex > 0 ? Guid.Parse(cmbEducationOrientation.SelectedValue) : (Guid?)null),
                    EucationDegreeId = Guid.Parse(CmbEucationDegree.SelectedValue),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    EducationCourseId = (CmbEducationCourse.SelectedIndex > 0 ? Guid.Parse(CmbEducationCourse.SelectedValue) : (Guid?)null),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    FirstAdvisor = txtFirstAdvisor.Text,
                    GradePointAverage = (txtGradePointAverage.Text != "") ? decimal.Parse(txtGradePointAverage.Text) : (decimal?)null,
                    FirstSupervisor = txtFirstSupervisor.Text,
                    SecondAdvisor = txtSecondAdvisor.Text,
                    SecondSupervisor = txtSecondSupervisor.Text,
                    StudentNo = txtStudentNo.Text,
                    TechnicalDescription = txtTechnicalDescription.Text,
                    ThesisSubject = txtThesisSubject.Text,
                };
                _dossierSacrificeEducationBl.Update(dossierSacrificeEducationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_EducationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_Education_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_EducationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_EducationEntity()
                    {
                        DossierSacrifice_EducationId = new Guid(e.CommandArgument.ToString()),

                    };
                    _dossierSacrificeEducationBl.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_Education_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {

         try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_Education_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_Education_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Education.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CmbEducationCourse_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Value))
            {
                cmbEducationOrientation.Items.Clear();
                cmbEducationOrientation.Text = "";
                SetCmboboxEducationOrientation(Guid.Parse(e.Value.ToString()));    
            }
            else
            {
                cmbEducationOrientation.Items.Clear();
                cmbEducationOrientation.Text = "";
            }
            
        }
        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                
                cmbCity.Items.Clear();
                cmbCity.Text = "";
                SetCmboboxCity(Guid.Parse(e.Value.ToString()));    
            }
            else
            {
                cmbCity.Items.Clear();
                cmbCity.Text = "";
            }
                
            
        }


        protected void CmbEducationSystem_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {

                CmbEucationDegree.Items.Clear();
                CmbEucationDegree.Text = "";
                SetCmbEucationDegree(Guid.Parse(e.Value.ToString()));
            }
            else
            {
                CmbEucationDegree.Items.Clear();
                CmbEucationDegree.Text = "";
            }
        }



        #endregion


    }
}