﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_AppointmentPage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_AppointmentPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="DossierSacrifice_Appointment">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="DossierSacrifice_Appointment">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                    
                                                <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblAppointmentType" runat="server"> نوع مراجعه<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="20%" >
                                <cc1:CustomRadComboBox ID="cmbAppointmentType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                                
                            </td>
                            <td width="70%" >
                                      <asp:RequiredFieldValidator ID="rfvAppointmentType" runat="server" 
                                    ControlToValidate="cmbAppointmentType" ErrorMessage="*" 
                                     ValidationGroup="DossierSacrifice_Appointment"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                                <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblAppointmentDate" runat="server">تاریخ مراجعه<font color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="txtAppointmentDate" runat="server"></cc1:CustomItcCalendar>
                                
                            </td>
                            <td>
                <asp:RequiredFieldValidator ID="rfvAppointmentDate" runat="server" 
                                    ControlToValidate="txtAppointmentDate" ErrorMessage="*" 
                                     ValidationGroup="DossierSacrifice_Appointment"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                                                                                              <tr>
                            <td>
                                <asp:Label runat="server" ID="lblAppointmentPerson">ملاقات کننده:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtAppointmentPerson" runat="server"  Width="300"  
                                    ></telerik:RadTextBox>                                
                            </td>
                            <td></td>
                        </tr>
                                                                        <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblDependentType" runat="server"> نوع وابستگی:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbDependentType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="200">
                                </cc1:CustomRadComboBox>
                                
                            </td>
                            <td>
                               
                            </td>
                        </tr>
                                               <tr>
                            <td>
<%--                                <asp:CheckBox ID="chkHasRequest" runat="server" AutoPostBack="True" Checked="True"
                                    Text="آیا درخواست دارد؟" 
                                    oncheckedchanged="chkHasRequest_CheckedChanged"  />--%>
                                <asp:Label CssClass="LabelCSS" ID="lblrequesttext" runat="server"> متن درخواست<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtRequestContent" runat="server"   TextMode="MultiLine" Width="300" Height="50" >
                                </telerik:RadTextBox>
                            </td>
                            <td>
                                                <asp:RequiredFieldValidator ID="rfvRequestContent" runat="server" 
                                    ControlToValidate="txtRequestContent" ErrorMessage="*" 
                                     ValidationGroup="DossierSacrifice_Appointment"></asp:RequiredFieldValidator>
                            </td>

                        </tr>
                        <tr>
                                                        <td nowrap="nowrap">
                                <asp:CheckBox ID="chkHasAction" runat="server" AutoPostBack="True" Checked="True"
                                    Text="آیا اقدام دارد؟" oncheckedchanged="chkHasAction_CheckedChanged"  />
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtActionDescription" runat="server"  TextMode="MultiLine" Width="300" Height="50"
                                    >
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                                                                                                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblDescription">توضیح:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtDescription" runat="server"  Width="300"  Height="50" TextMode="MultiLine" 
                                    ></telerik:RadTextBox>                                
                            </td>
                            <td></td>
                        </tr>

                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Appointment" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" 
                                    onitemcommand="grdDossierSacrifice_Appointment_ItemCommand" 
                                    onpageindexchanged="grdDossierSacrifice_Appointment_PageIndexChanged" 
                                    onpagesizechanged="grdDossierSacrifice_Appointment_PageSizeChanged" 
                                    onsortcommand="grdDossierSacrifice_Appointment_SortCommand" >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_AppointmentId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                                 <telerik:GridBoundColumn DataField="AppointmentTypeTitle" HeaderText=" نوع  مراجعه"
                                                SortExpression="AppointmentTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="AppointmentDate" HeaderText="تاریخ مراجعه"
                                                SortExpression="AppointmentDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                 <telerik:GridBoundColumn DataField="AppointmentPerson" HeaderText="ملاقات کننده"
                                                SortExpression="AppointmentPerson">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                 <telerik:GridBoundColumn DataField="DependentTypeTitle" HeaderText="نوع وابستگی"
                                                SortExpression="DependentTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>     
   
                                           <telerik:GridCheckBoxColumn DataField="HasRequest" HeaderText="درخواست دارد؟" SortExpression="HasRequest">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                                                    <telerik:GridCheckBoxColumn DataField="HasAction" HeaderText="اقدام دارد؟" SortExpression="HasAction">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                                                                      <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_AppointmentId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn> 
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_AppointmentId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_AppointmentId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkHasRequest">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="chkHasAction" />
                <telerik:AjaxUpdatedControl ControlID="txtActionDescription" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkHasAction">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="chkHasAction" />
                <telerik:AjaxUpdatedControl ControlID="txtActionDescription" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Appointment">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>