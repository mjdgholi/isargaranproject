﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_RequestKartablePage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_RequestKartablePage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>


<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>                
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>

                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                                                <tr>
                            <td align="right nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="labFirstAndLastName" runat="server">نام ونام خانوادگی ایثارگر:</asp:Label>
                            </td>
                            <td align="right" dir="rtl" >
                                <cc1:SelectControl ID="SelectPerson" runat="server" PortalPathUrl="GeneralProject/General/ModalForm/PersonPage.aspx"
                                    ToolTip="جستجو پرسنل" MultiSelect="False" RadWindowWidth="1000" RadWindowHeight="700" />
                            </td>
                                                    <td></td>
                                                    </tr>
                        
                                                <tr>
                            <td align="right nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblDossierSacrificeNo" runat="server">شماره پرونده:</asp:Label>
                            </td>
                            <td align="right" dir="rtl" >
                                <cc1:NumericTextBox runat="server" ID="txtDossierSacrificeNo" />
                                
                            </td>
                                                    <td></td>
                                                    </tr>
                        

                                                                        <tr>
                            <td  nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblImportanceType" runat="server">نوع اهمیت درخواست:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbImportanceType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                            
                            </td>
                        </tr>
                                                                        <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblRequestType" runat="server">نوع درخواست:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbRequestType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                               
                            </td>
                        </tr>
                                                                                                         <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblStatus" runat="server">نوع وضیعت درخواست:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbStatus" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                    
                            </td>
                        </tr>
                                                                                 <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblActionTypeTitle" runat="server">نوع اقدام درخواست:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbActionType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                       
                            </td>
                        </tr>

                             <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblRequestDescription" runat="server">متن درخواست:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtRequestDescription" runat="server" Width="300" Height="50" TextMode="MultiLine" ></asp:TextBox>
                            </td>
                            <td>
                                   
                            </td>
                        </tr>
                                                  
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Request" 
                                    runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"  RowStyleSelector="{StaticResource stadiumStyle}"  
                                    AutoGenerateColumns="False" OnItemCommand="grdDossierSacrifice_Request_ItemCommand" OnPageIndexChanged="grdDossierSacrifice_Request_PageIndexChanged" OnPageSizeChanged="grdDossierSacrifice_Request_PageSizeChanged" OnSortCommand="grdDossierSacrifice_Request_SortCommand" OnItemDataBound="grdDossierSacrifice_Request_ItemDataBound"   >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_RequestId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                                                  <telerik:GridBoundColumn DataField="IsViewed" HeaderText=" " Visible="False"
                                                SortExpression="IsViewed">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            
                                                       <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام "
                                                SortExpression="FirstName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                                  <telerik:GridBoundColumn DataField="lastName" HeaderText=" نام و نام خانوادگی"
                                                SortExpression="lastName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                                  <telerik:GridBoundColumn DataField="ProvinceTitlePersian" HeaderText="استان"
                                                SortExpression="ProvinceTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            
                                            <telerik:GridBoundColumn DataField="ImportanceTypeTitle" HeaderText="عنوان اهمیت"
                                                SortExpression="ImportanceTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RequestTypeTitle" HeaderText="عنوان نوع درخواست" SortExpression="RequestTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                         <telerik:GridBoundColumn DataField="StatusTitle" HeaderText="عنوان وضیعت درخواست" SortExpression="StatusTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                         <telerik:GridBoundColumn DataField="ActionTypeTitle" HeaderText="عنوان نوع اقدام درخواست" SortExpression="ActionTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="مشاهده درخواست" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButtonShowRequest" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_RequestId").ToString() %>'
                                                        CommandName="_MyShowRequest" ForeColor="#000066" ImageUrl="../Images/ShowMessage.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                               <telerik:GridTemplateColumn HeaderText="اقدام" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButtonAction" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_RequestId").ToString() %>'
                                                        CommandName="_MyAction" ForeColor="#000066" ImageUrl="../Images/Action.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                                                                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="نمایش همه"
                                                UniqueName="ShowAll">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgShoaAll" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_RequestId").ToString() %>'
                                                        CommandName="_ShowAll" ForeColor="#000066" ImageUrl="../Images/ShowAllInGrid.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        
                                     
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>

  

    </table>
          <asp:Panel ID="PnlOtherInformation" runat="server" HorizontalAlign="Right" Width="100%">
    <table width="100%" dir="rtl">
        <tr>
            <td align="center" class="HeaderOfDetail">
                <font style="font-size: large; font-style: normal; font-variant: normal; color: #FFFFFF;
                    font-family: Tahoma">
                    <asp:Label ID="LblSelectedCommand" runat="server"></asp:Label></font>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <telerik:RadTabStrip ID="RadTabStrip" runat="server" SelectedIndex="0" Align="Right"
                    AutoPostBack="True" CausesValidation="False" OnTabClick="RadTabStripTabClick"
                    >
                    <Tabs>
                        <telerik:RadTab runat="server" Text="اقدام" Value="DossierSacrifice_RequestHistoryPage">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="پیغام" Value="MessagePage">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadMultiPage ID="radmultipageOtherInformation" runat="server" Height="100%"
                    Width="100%" OnPageViewCreated="radmultipageOtherInformation_PageViewCreated">
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Request">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadTabStrip">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadTabStrip" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageOtherInformation" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radmultipageOtherInformation">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="radmultipageOtherInformation" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
