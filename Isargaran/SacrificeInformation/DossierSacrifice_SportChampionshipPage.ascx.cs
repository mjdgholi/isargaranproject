﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/27>
    // Description:	<رشته ورزشی پرونده ایثارگر>
    /// </summary>
    public partial class DossierSacrifice_SportChampionshipPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly DossierSacrifice_SportChampionshipBL _dossierSacrificeSportChampionshipBL = new DossierSacrifice_SportChampionshipBL();
        private readonly DossierSacrifice_DependentBL _dossierSacrificeDependentBL = new DossierSacrifice_DependentBL();
        private readonly SportFieldBL _sportFieldBL = new SportFieldBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_SportChampionship]";
        private const string PrimaryKey = "DossierSacrifice_SportChampionshipId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtCompetitionDate.Text = "";
            txtCompetitionLocation.Text = "";
            txtCompetitionTitle.Text = "";
            txtPointOrMedal.Text = "";
            cmbSportField.ClearSelection();
            cmbDependentPerson.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var DossierSacrifice_SportChampionshipEntity = new DossierSacrifice_SportChampionshipEntity()
            {
                DossierSacrifice_SportChampionshipId = Guid.Parse(ViewState["DossierSacrifice_SportChampionshipId"].ToString())
            };
            var myDossierSacrifice_SportChampionship = _dossierSacrificeSportChampionshipBL.GetSingleById(DossierSacrifice_SportChampionshipEntity);
            cmbSportField.SelectedValue = myDossierSacrifice_SportChampionship.SportFieldId.ToString();
            if (cmbSportField.FindItemByValue(myDossierSacrifice_SportChampionship.SportFieldId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردرشته ورزشی انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = myDossierSacrifice_SportChampionship.Description;
            txtCompetitionDate.Text = myDossierSacrifice_SportChampionship.CompetitionDate;
            txtCompetitionLocation.Text = myDossierSacrifice_SportChampionship.CompetitionLocation;
            txtCompetitionTitle.Text = myDossierSacrifice_SportChampionship.CompetitionTitle;
            txtPointOrMedal.Text = myDossierSacrifice_SportChampionship.PointOrMedal;
            cmbDependentPerson.SelectedValue= myDossierSacrifice_SportChampionship.PersonId.ToString();            
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_SportChampionship,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbSportField.Items.Clear();
            cmbSportField.Items.Add(new RadComboBoxItem(""));
            cmbSportField.DataSource = _sportFieldBL.GetAllIsActive();
            cmbSportField.DataTextField = "SportFieldPersianTitle";
            cmbSportField.DataValueField = "SportFieldId";
            cmbSportField.DataBind();


            DossierSacrifice_DependentEntity dossierSacrificeDependentEntity = new DossierSacrifice_DependentEntity()
            {
                DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString())
            };
            cmbDependentPerson.Items.Clear();
            cmbDependentPerson.Items.Add(new RadComboBoxItem(""));
            cmbDependentPerson.DataSource = _dossierSacrificeDependentBL.GetDossierSacrifice_DependentCollectionByDossierSacrifice(dossierSacrificeDependentEntity);
            cmbDependentPerson.DataTextField = "FullName";
            cmbDependentPerson.DataValueField = "PersonId";
            cmbDependentPerson.DataBind();

        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_SportChampionshipId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeSportChampionshipEntity = new DossierSacrifice_SportChampionshipEntity()
                {                    
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CompetitionDate = ItcToDate.ShamsiToMiladi(txtCompetitionDate.Text),
                    CompetitionLocation = txtCompetitionLocation.Text,
                    CompetitionTitle = txtCompetitionTitle.Text,
                    PointOrMedal = txtPointOrMedal.Text,
                    SportFieldId = Guid.Parse(cmbSportField.SelectedValue),
                    PersonId = (cmbDependentPerson.SelectedIndex>0?Guid.Parse(cmbDependentPerson.SelectedValue):new Guid()),
                };
                _dossierSacrificeSportChampionshipBL.Add(dossierSacrificeSportChampionshipEntity, out DossierSacrifice_SportChampionshipId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, DossierSacrifice_SportChampionshipId );
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtCompetitionLocation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CompetitionLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtCompetitionLocation.Text.Trim()) + "%'";
                if (txtCompetitionTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CompetitionTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtCompetitionTitle.Text.Trim()) + "%'";
                if (txtPointOrMedal
                    .Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PointOrMedal Like N'%" +
                                               FarsiToArabic.ToArabic(txtPointOrMedal.Text.Trim()) + "%'";
                if (txtCompetitionDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CompetitionDate ='" +
                                               (txtCompetitionDate.Text.Trim()) + "'";
                if (cmbDependentPerson.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId ='" +
                                               (cmbDependentPerson.SelectedValue) + "'";
                if (cmbSportField.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And SportFieldId='" +
                                               (cmbSportField.SelectedValue.Trim()) + "'";

                grdDossierSacrifice_SportChampionship.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_SportChampionship.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeSportChampionshipEntity = new DossierSacrifice_SportChampionshipEntity()
                {
                    DossierSacrifice_SportChampionshipId = Guid.Parse(ViewState["DossierSacrifice_SportChampionshipId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CompetitionDate = ItcToDate.ShamsiToMiladi(txtCompetitionDate.Text),
                    CompetitionLocation = txtCompetitionLocation.Text,
                    CompetitionTitle = txtCompetitionTitle.Text,
                    PointOrMedal = txtPointOrMedal.Text,
                    SportFieldId = Guid.Parse(cmbSportField.SelectedValue),
                    PersonId = (cmbDependentPerson.SelectedIndex > 0 ? Guid.Parse(cmbDependentPerson.SelectedValue) : new Guid()),
                };
                _dossierSacrificeSportChampionshipBL.Update(dossierSacrificeSportChampionshipEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_SportChampionshipId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_SportChampionship_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_SportChampionshipId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    var dossierSacrificeSportChampionshipEntity = new DossierSacrifice_SportChampionshipEntity()
                    {
                        DossierSacrifice_SportChampionshipId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeSportChampionshipBL.Delete(dossierSacrificeSportChampionshipEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_SportChampionship_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_SportChampionship_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_SportChampionship_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SportChampionship.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

    }
}