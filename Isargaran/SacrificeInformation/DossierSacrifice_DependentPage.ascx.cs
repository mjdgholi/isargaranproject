﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/17>
    /// Description: <پرونده مشخصات وابستگان ایثارگری>
    /// </summary> 
    public partial class DossierSacrifice_DependentPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly DossierSacrifice_DependentBL _dossierSacrificeDependentBL = new DossierSacrifice_DependentBL();
        private readonly DependentTypeBL _DependentTypeBL = new DependentTypeBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_Dependent]";
        private const string PrimaryKey = "DossierSacrifice_DependentId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtFromDate.Text = "";
            cmbDependentTypeId.ClearSelection();
            SelectPerson.KeyId = "";
            SelectPerson.Title = "";
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeDependentEntity = new DossierSacrifice_DependentEntity()
            {
                DossierSacrifice_DependentId = Guid.Parse(ViewState["DossierSacrifice_DependentId"].ToString())
            };
            var mydossierSacrificeDependent = _dossierSacrificeDependentBL.GetSingleById(dossierSacrificeDependentEntity);
            cmbDependentTypeId.SelectedValue = mydossierSacrificeDependent.DependentTypeId.ToString();
            if (cmbDependentTypeId.FindItemByValue(mydossierSacrificeDependent.DependentTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردنوع وابستگی انتخاب شده معتبر نمی باشد.");
            }
            txtFromDate.Text = mydossierSacrificeDependent.FromDate;
            SelectPerson.KeyId = mydossierSacrificeDependent.PersonId.ToString();
            SelectPerson.Title = mydossierSacrificeDependent.FullName;
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Dependent,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbDependentTypeId.Items.Clear();
            cmbDependentTypeId.Items.Add(new RadComboBoxItem(""));
            cmbDependentTypeId.DataSource = _DependentTypeBL.GetAllIsActive();
            cmbDependentTypeId.DataTextField = "DependentTypeTitle";
            cmbDependentTypeId.DataValueField = "DependentTypeId";
            cmbDependentTypeId.DataBind();
        }

                private void CheckValidation()
        {
                    if (SelectPerson.KeyId == "")
                    {
                        throw new ItcApplicationErrorManagerException("نام و نام خانوادگی وابسته را وارد نمایید.");
                    }
        }
        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

         #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="checkValidation"></param>


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CheckValidation();
                Guid dossierSacrificeDependentId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeDependentEntity = new DossierSacrifice_DependentEntity()
                {
                    DependentTypeId = Guid.Parse(cmbDependentTypeId.SelectedValue),
                    FromDate = ItcToDate.ShamsiToMiladi(txtFromDate.Text),
                    PersonId = Guid.Parse(SelectPerson.KeyId),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString())
                    
                };
                _dossierSacrificeDependentBL.Add(dossierSacrificeDependentEntity, out dossierSacrificeDependentId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, dossierSacrificeDependentId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtFromDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FromDate ='" +
                                               (txtFromDate.Text.Trim()) + "'";
                if (SelectPerson.KeyId.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonId ='" +
                                               (SelectPerson.KeyId.Trim()) + "'";
                if (cmbDependentTypeId.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And DependentTypeId='" +
                                               (cmbDependentTypeId.SelectedValue.Trim()) + "'";

                grdDossierSacrifice_Dependent.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Dependent.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                CheckValidation();
                var dossierSacrificeDependentEntity = new DossierSacrifice_DependentEntity()
                {                    
                    DependentTypeId = Guid.Parse(cmbDependentTypeId.SelectedValue),
                    FromDate = ItcToDate.ShamsiToMiladi(txtFromDate.Text),
                    PersonId = Guid.Parse(SelectPerson.KeyId),          
                    DossierSacrifice_DependentId = Guid.Parse(ViewState["DossierSacrifice_DependentId"].ToString()),
                    DossierSacrificeId =  Guid.Parse(Session["DossierSacrificeId"].ToString())
                   
                };
                _dossierSacrificeDependentBL.Update(dossierSacrificeDependentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_DependentId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
                /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_Dependent_ItemCommand(object sender, GridCommandEventArgs e)
        {
         try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_DependentId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    var dossierSacrificeDependentEntity = new DossierSacrifice_DependentEntity()
                    {
                        DossierSacrifice_DependentId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeDependentBL.Delete(dossierSacrificeDependentEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }

                if (e.CommandName == "_MyCloseDate")
                {
                    var dossierSacrificeDependentId = Guid.Parse(e.CommandArgument.ToString());
                    var item = e.Item as GridDataItem;
                    var txtEndDate = (ITC.Library.Controls.CustomeRadMaskedTextBox)item.FindControl("TxtEndDate");
                    var endDate = (txtEndDate.Text.Trim() == "") ? "" : ITC.Library.Classes.ItcToDate.ShamsiToMiladi(txtEndDate.TextWithLiterals);
                    var dossierSacrificeDependentEntity = new DossierSacrifice_DependentEntity()
                    {
                        DossierSacrifice_DependentId = Guid.Parse(e.CommandArgument.ToString()),
                        Todate = endDate
                    };
                    _dossierSacrificeDependentBL.CloseTodossierSacrificeDependent(dossierSacrificeDependentEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, dossierSacrificeDependentId);
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_Dependent_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
      try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_Dependent_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
          try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_Dependent_SortCommand(object sender, GridSortCommandEventArgs e)
        {
         try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Dependent.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion



        }
    }
