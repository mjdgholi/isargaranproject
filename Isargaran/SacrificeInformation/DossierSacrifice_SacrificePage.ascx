﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_SacrificePage.ascx.cs"
    Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_SacrificePage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" ValidationGroup="DossierSacrifice_Sacrifice"
                                    CustomeButtonType="Add" OnClick="btnSave_Click"></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click"><Icon PrimaryIconCssClass="rbSearch" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click"><Icon PrimaryIconCssClass="rbRefresh" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="DossierSacrifice_Sacrifice"><Icon PrimaryIconCssClass="rbEdit" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False"><Icon PrimaryIconCssClass="rbPrevious" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label runat="server" ID="lblsacrifice">عنوان ایثارگری<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="15%">
                                <cc1:CustomRadComboBox ID="cmbsacrifice" runat="server" AutoPostBack="True" CausesValidation="False"
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" OnSelectedIndexChanged="cmbsacrifice_SelectedIndexChanged" />
                            </td>
                            <td width="75%">
                                <asp:RequiredFieldValidator ID="rfvsacrifice" runat="server" ControlToValidate="cmbsacrifice"
                                    ValidationGroup="DossierSacrifice_Sacrifice" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblStartDate" runat="server">تاریخ شروع<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="15%">
                                <cc1:CustomItcCalendar ID="txtStartDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td width="75%">
                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate"
                                    ErrorMessage="*" ValidationGroup="DossierSacrifice_Sacrifice"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td  width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblEndDate" runat="server">تاریخ پایان:</asp:Label>
                            </td>
                            <td  width="15%">
                                <cc1:CustomItcCalendar ID="txtEndDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td width="75%"></td>
                        </tr>
                        <tr>
                            <td colspan="3" width="100%">
                                <asp:Panel ID="pnlDuration" runat="server" Visible="False">
                                    <table style="background-color: #F0F8FF" width="100%">
                                        <tr>
                                            <td width="10%">
                                                <asp:Label CssClass="LabelCSS" ID="labValueInYear" runat="server" Text="تعداد سال :"></asp:Label>
                                            </td>
                                            <td width="5%">
                                                <cc1:NumericTextBox ID="txtValueInYear" runat="server" Width="50px"></cc1:NumericTextBox>
                                            </td>
                                            <td width="5%">
                                                <asp:Label CssClass="LabelCSS" ID="labValueInMonth" runat="server">تعداد ماه<font color="red">*</font>:</asp:Label>
                                            </td>
                                            <td width="5%">
                                                <cc1:NumericTextBox ID="txtValueInMonth" runat="server" Width="50px" MaxLength="2"></cc1:NumericTextBox>
                                            </td>
                                            <td width="5%">
                                                <asp:RangeValidator ID="RangeExperienceValueInMonth" runat="server" ControlToValidate="txtValueInMonth"
                                                    Type="Integer" ErrorMessage="تعداد ماه در محدوده 0-11" MaximumValue="11" MinimumValue="0"
                                                    ValidationGroup="DossierSacrifice_Sacrifice">*</asp:RangeValidator>
                                            </td>
                                            <td width="5%">
                                                <asp:Label CssClass="LabelCSS" ID="labValueInDay" runat="server">تعداد روز<font color="red">*</font>:</asp:Label>
                                            </td>
                                            <td width="5%">
                                                <cc1:NumericTextBox ID="txtValueInDay" runat="server" Width="50px" MaxLength="2"></cc1:NumericTextBox>
                                            </td>
                                            <td>
                                                <asp:RangeValidator ID="RangExperienceValueInDay" runat="server" ControlToValidate="txtValueInDay"
                                                    Type="Integer" ErrorMessage="تعداد روز درمحدوده0-29" MaximumValue="29" MinimumValue="0"
                                                    ValidationGroup="DossierSacrifice_Sacrifice">*</asp:RangeValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                        <asp:Label CssClass="LabelCSS" ID="lblDurationType" runat="server">نوع مدت:</asp:Label>
                                            </td>
                                            <td width="15%">
                                                          <cc1:CustomRadComboBox ID="cmbDurationtype" runat="server" AppendDataBoundItems="True"  />
                                            </td>
                                            <td colspan="6">
                                                                    <asp:RequiredFieldValidator ID="rfvDurationType" runat="server" ControlToValidate="cmbDurationtype"
                                    ErrorMessage="*" ValidationGroup="DossierSacrifice_Sacrifice"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"  width="100%">
                                <asp:Panel ID="pnlPercent" runat="server" Visible="False">
                                    <table width="100%">
                                        <tr>
                                            <td align="right" dir="rtl" style="height: 20px; width: 9%;" valign="top">
                                                <asp:Label CssClass="LabelCSS" ID="lblPercentValue" runat="server" Text="درصد:"></asp:Label>
                                            </td>
                                            <td style="height: 20px; width: 85%;">
                                                <cc1:NumericTextBox ID="txtPercentValue" runat="server" Text="" Width="170px"></cc1:NumericTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" width="10%">
                                <label>
                                    وضیعت رکورد<font color="red">*</font>:</label>
                            </td>
                            <td width="15%">
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                   
                            </td>
                            <td width="75%">
                                            <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                    ValidationGroup="DossierSacrifice_Sacrifice" ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator> 
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Sacrifice" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdDossierSacrifice_Sacrifice_ItemCommand"
                                    OnPageIndexChanged="grdDossierSacrifice_Sacrifice_PageIndexChanged" OnPageSizeChanged="grdDossierSacrifice_Sacrifice_PageSizeChanged"
                                    OnSortCommand="grdDossierSacrifice_Sacrifice_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_SacrificeId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده ایثارگری"
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="sacrificeTitle" HeaderText="عنوان ایثارگری" SortExpression="sacrificeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PercentValue" HeaderText="درصد ایثارگری" SortExpression="PercentValue">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TotalValueInDay" HeaderText="مدت کل ایثارگری به روز"
                                                SortExpression="TotalValueInDay">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                              <telerik:GridBoundColumn DataField="DurationTypeTitle" HeaderText="نوع مدت"
                                                SortExpression="DurationTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StartDate" HeaderText="تاریخ شروع" SortExpression="StartDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Endate" HeaderText="تاریخ پایان" SortExpression="Endate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                                                                     <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_SacrificeId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_SacrificeId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_SacrificeId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbsacrifice">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbsacrifice" />
                <telerik:AjaxUpdatedControl ControlID="pnlDuration" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="pnlPercent" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Sacrifice">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
