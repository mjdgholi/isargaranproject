﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/25>
    /// Description: <مدیریت پیغام>
    /// </summary>
    public partial class MessagePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly MessageBL _messageBL = new MessageBL();
        private const string TableName = "[Isar].[V_Message]";
        private const string PrimaryKey = "MessageId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;

        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtMessageText.Text = "";



        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var messageEntity = new MessageEntity()
            {
                MessageId = Guid.Parse(ViewState["MessageId"].ToString())
            };
            var mymessage = _messageBL.GetSingleById(messageEntity);


            txtMessageText.Text = mymessage.MessageText;
            if (mymessage.MessageTypeId.ToString() == "a05eac03-5f07-4161-a8b0-949814fbd680")
            _messageBL.UpdateIsView(messageEntity);
            //TreeMessageType.FindNodeByValue(mymessage.MessageTypeId.ToString()).Selected=true;


        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdMessage,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }




        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()) + "'";
                ViewState["SortExpression"] = "IsInpout Asc,IsViewed";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()) + "'";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid messageId;
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()) + "'";
                var messageEntity = new MessageEntity()
                {
                    DossierSacrifice_RequestId = Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()),
                    MessageText = txtMessageText.Text,
                    MessageTypeId = Guid.Parse("8b333ae0-cda5-47a9-83f8-8acb6cecd41b"),

                };
                _messageBL.Add(messageEntity, out messageId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, messageId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()) + "'";

                if (txtMessageText.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MessageText Like N'%" +
                                               FarsiToArabic.ToArabic(txtMessageText.Text.Trim()) + "%'";
                if (!String.IsNullOrEmpty(TreeMessageType.SelectedNode.Value))
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And MessageTypeId='" +
                                               (TreeMessageType.SelectedNode.Value) + "'";



                grdMessage.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdMessage.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }




        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdMessage_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyShowMessage")
                {
                    ViewState["MessageId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()) + "'";
                    var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdMessage_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdMessage_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdMessage_SortCommand(object sender, GridSortCommandEventArgs e)
        {

            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        protected void TreeMessageType_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Session["DossierSacrifice_RequestId"].ToString()) + "'";

            if (!String.IsNullOrEmpty(TreeMessageType.SelectedNode.Value))
                ViewState["WhereClause"] = ViewState["WhereClause"] + " And MessageTypeId='" +
                                           (TreeMessageType.SelectedNode.Value) + "'";



            grdMessage.MasterTableView.CurrentPageIndex = 0;
            SetPanelFirst();
            btnShowAll.Visible = true;
            var gridParamEntity = SetGridParam(grdMessage.MasterTableView.PageSize, 0, new Guid());
            DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
        }

        protected void grdMessage_ItemDataBound(object sender, GridItemEventArgs e)
        {
           if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                if (dataItem["IsViewed"].Text == "False" && dataItem["MessageTypeId"].Text == "a05eac03-5f07-4161-a8b0-949814fbd680")
                    dataItem.CssClass = "RowRadGridView";
            }
        }
        

    }

        #endregion
}