﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrificePage.ascx.cs"
    Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrificePage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="PnlMaster" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>

                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                        <tr>
                            <td align="right" width="18%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="labFirstAndLastName" runat="server">نام ونام خانوادگی ایثارگر<font color="red">*</font>:</asp:Label>
                            </td>
                            <td align="right" dir="rtl" width="20%">
                                <cc1:SelectControl ID="SelectPerson" runat="server" PortalPathUrl="GeneralProject/General/ModalForm/PersonPage.aspx"  FilterByModulId="True"
                                    ToolTip="جستجو پرسنل" MultiSelect="False" RadWindowWidth="1000" RadWindowHeight="700" />
                            </td>
                            <td width="10%">
                            </td>
                            <td width="18%">
                                <asp:Label CssClass="LabelCSS" ID="LblDocPath" runat="server" EnableTheming="False"
                                    Text="مسیر عکس :"></asp:Label>
                            </td>
                            <td width="20%">
                                <cc1:SelectControl ID="FileUpload1" runat="server" imageName="Upload" PortalPathUrl="IsargaranProject/Isargaran/ModalForm/FrmUpload.aspx"
                                    RadWindowHeight="300" RadWindowWidth="700" />
                            </td>
                            <td width="14%">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblDossierSacrificeNo" runat="server">شماره پرونده <font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtDossierSacrificeNo" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="ValidDossierSacrificeNo" runat="server" ControlToValidate="txtDossierSacrificeNo"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblSacrificeNo" runat="server">شماره ایثارگری <font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtSacrificeNo" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvSacrificeNo" runat="server" ControlToValidate="txtSacrificeNo"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblDossierStatus" runat="server">وضیعت پرونده <font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbDossierStatus" runat="server" AppendDataBoundItems="True"
                                    AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvDossierStatus" runat="server" ControlToValidate="CmbDossierStatus"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td colspan="3" nowrap="nowrap">
                                <asp:CheckBox CssClass="CheckBoxCSS" ID="chkHasDossierSacrifice" runat="server" Checked="True"
                                    Text="آیا پرونده ایثارگری در بنیاد شهید دارد؟" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblMarriageStatus" runat="server">وضیعت تاهل:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbMarriageStatus" runat="server" AppendDataBoundItems="True"
                                    AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
        
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblMarriageDate" runat="server">تاریخ ازدواج:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtMarriageDate" runat="server">
                                </cc1:CustomItcCalendar>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblNaturalization" runat="server">تابعیت<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbNaturalization" runat="server" AppendDataBoundItems="True" Height="300"
                                    AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="CmbNaturalization"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblNationality" runat="server">ملیت<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbNationality" runat="server" AppendDataBoundItems="True" Height="300"
                                    AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvNationality" runat="server" ControlToValidate="CmbNationality"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblFaith" runat="server">دین<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbFaith" runat="server" AppendDataBoundItems="True" AllowCustomText="True"
                                    Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvFaith" runat="server" ControlToValidate="cmbFaith"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblReligion" runat="server">مذهب<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbReligion" runat="server" AppendDataBoundItems="True"
                                    AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="CmbReligion"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblprovince" runat="server">استان محل تشکیل پرونده<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbprovince" runat="server" AppendDataBoundItems="True"
                                    AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" AutoPostBack="True"
                                    CausesValidation="False" OnSelectedIndexChanged="cmbprovince_SelectedIndexChanged" Height="250">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="cmbprovince"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCity" runat="server">شهر محل تشکیل پرونده<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbCity" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Height="250"
                                    Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="cmbCity"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox CssClass="CheckBoxCSS" ID="chkHasInsurance" runat="server" AutoPostBack="True"
                                    Checked="True" Text="آیا بیمه دارد؟" OnCheckedChanged="ChkHasInsurance_CheckedChanged" />
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtInsuranceNo" runat="server" DisplayText="شماره بیمه" MaxLength="10">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                            <td nowrap="nowrap">
                                <asp:CheckBox CssClass="CheckBoxCSS" ID="chkHasPassport" runat="server" AutoPostBack="True"
                                    Checked="True" Text="آیا گذرنامه دارد؟" OnCheckedChanged="chkHasPassport_CheckedChanged" />
                            </td>
                            <td>
                                <telerik:RadTextBox CssClass="TextBoxCSS" WrapperCssClass="TextBoxCSS" ID="txtPassportNo"
                                    runat="server" DisplayText="شماره گذرنامه" MaxLength="10">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:CheckBox CssClass="CheckBoxCSS" ID="chkHasDependentSacrifice" runat="server"
                                    AutoPostBack="True" Checked="True" Text="آیا منسوبین به ایثار گران است؟" OnCheckedChanged="chkHasDependentSacrifice_CheckedChanged" />
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbDependentType" runat="server" AppendDataBoundItems="True"
                                    AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEmail" runat="server">ایمیل:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtEmail" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblHomeFaxNo" runat="server">شماره فکس:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtHomeFaxNo" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblHomePhoneNo" runat="server">شماره تلفن منزل:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtHomePhoneNo" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCellphoneNo" runat="server">شماره موبایل:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtCellphoneNo" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
      
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblZipCode" runat="server">کدپستی:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtZipCode" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblHomeAddress" runat="server">آدرس منزل:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtHomeAddress" runat="server" TextMode="MultiLine">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                            <td>
                                <label>
                                    وضیعت رکورد<font color="red">*</font>:</label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                    ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlGrid" runat="server">
    <table width="100%">
        <tr>
            <td>
                <telerik:RadGrid ID="grdDossierSacrifice" runat="server" AllowCustomPaging="True"
                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                    AutoGenerateColumns="False" 
                    OnItemCommand="grdDossierSacrifice_ItemCommand" Style="margin-bottom: 0px"
                    OnPageIndexChanged="grdDossierSacrifice_PageIndexChanged" OnPageSizeChanged="grdDossierSacrifice_PageSizeChanged"
                    OnSortCommand="grdDossierSacrifice_SortCommand" 
                    ondatabound="grdDossierSacrifice_DataBound">
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                    </HeaderContextMenu>
                    <MasterTableView DataKeyNames="DossierSacrificeId" Dir="RTL" GroupsDefaultExpanded="False"
                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                        <Columns>
                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                SortExpression="DossierSacrificeNo">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SacrificeNo" HeaderText="شماره ایثارگری" SortExpression="SacrificeNo">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FirstAndLastName" HeaderText="نام و نام خانوادگی"
                                SortExpression="FirstAndLastName">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DossierStatusTitle" HeaderText="وضیعت پرونده"
                                SortExpression="DossierStatusTitle">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ProvinceTitlePersian" HeaderText="استان محل تشکیل پرونده"
                                SortExpression="ProvinceTitlePersian">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CityTitlePersian" HeaderText="شهر" SortExpression="CityTitlePersian">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FaithTitle" HeaderText="دین" SortExpression="FaithTitle">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ReligionTitle" HeaderText="مذهب" SortExpression="ReligionTitle">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="MarriageStatusTitle" HeaderText="وضیعت تاهل"
                                SortExpression="MarriageStatusTitle">
                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridTemplateColumn HeaderText="اطلاعات تکمیلی" FilterControlAltText="Filter TemplateColumn1 column"
                                UniqueName="_OtherInformation">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemTemplate>
                                    <cc1:CustomRadComboBox CausesValidation="False" AutoPostBack="True" runat="server"
                                        ID="cmbWebFormCategory" AppendDataBoundItems="True" CommandArgument='<%#Eval("DossierSacrificeId").ToString() %>'                                       
                                        OnSelectedIndexChanged="cmbWebFormCategory_SelectedIndexChanged" Filter="Contains"
                                        Width="250px" MarkFirstMatch="True">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="یک گزینه را انتخاب نمایید" />
                                        </Items>
                                    </cc1:CustomRadComboBox>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <%--Checked='<%#(Convert.IsDBNull(Eval("IsActive"))==false && Convert.ToBoolean(Eval("IsActive"))==true)?true:false %>'--%>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="دریافت عکس " SortExpression="ImagePath"
                                UniqueName="_Download">
                                <ItemTemplate>
                                    <img alt='<%#(Eval("ImagePath")==null || Eval("ImagePath").ToString()=="")?"فاقد تصویر":"شامل تصویر"%>' width="30" onclick="if (document.URL.search('localhost')=='-1') 
                        { 
                        window.open('DeskTopModules/IsargaranProject/Isargaran/SacrificeImage/'+'<%#(Eval("ImagePath").ToString()==null || Eval("ImagePath").ToString()=="")?"Noimages.jpg":Eval("ImagePath") %>');
                        } 
                         else  
                       {
                       window.open('DeskTopModules/IsargaranProject/Isargaran/SacrificeImage/'+'<%#(Eval("ImagePath").ToString()==null || Eval("ImagePath").ToString()=="")?"Noimages.jpg":Eval("ImagePath") %>');   
                       }"
                                        src='<%#(Eval("ImagePath")==null || Eval("ImagePath").ToString()=="")?"Desktopmodules/IsargaranProject/Isargaran/Images/NoImageDownload.png":"Desktopmodules/IsargaranProject/Isargaran/Images/OkImageDownload.png" %>' />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                UniqueName="TemplateColumn1">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrificeId").ToString() %>'
                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                UniqueName="TemplateColumn">
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                        CommandArgument='<%#Eval("DossierSacrificeId").ToString() %>' CommandName="_MyِDelete"
                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="نمایش همه"
                                UniqueName="ShowAll">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgShoaAll" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrificeId").ToString() %>'
                                        CommandName="_ShowAll" ForeColor="#000066" ImageUrl="../Images/ShowAllInGrid.png" />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                            VerticalAlign="Middle" />
                    </MasterTableView>
                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                        <ClientEvents></ClientEvents>
                        <Selecting AllowRowSelect="True"></Selecting>
                    </ClientSettings>
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="PnlOtherInformation" runat="server" HorizontalAlign="Right" Width="100%">
    <table width="100%" dir="rtl">
        <tr>
            <td align="center" class="HeaderOfDetail">
                <font style="font-size: large; font-style: normal; font-variant: normal; color: #FFFFFF; font-family: Tahoma">
                    <asp:Label ID="LblSelectedCommand" runat="server"></asp:Label></font>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <telerik:RadTabStrip ID="RadTabOtherInformation" runat="server" SelectedIndex="21"
                    Align="Right" AutoPostBack="True" CausesValidation="False" OnTabClick="RadTabOtherInformation_TabClick">
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadMultiPage ID="radmultipageOtherInformation" runat="server" Height="100%"
                    Width="100%" OnPageViewCreated="radmultipageOtherInformation_PageViewCreated">
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbprovince">
            <UpdatedControls>

                <telerik:AjaxUpdatedControl ControlID="cmbCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkHasInsurance">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="chkHasInsurance" />
                <telerik:AjaxUpdatedControl ControlID="txtInsuranceNo" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkHasPassport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="chkHasPassport" />
                <telerik:AjaxUpdatedControl ControlID="txtPassportNo" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkHasDependentSacrifice">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="chkHasDependentSacrifice" />
                <telerik:AjaxUpdatedControl ControlID="cmbDependentType" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PnlMaster" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadTabOtherInformation">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadTabOtherInformation" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageOtherInformation" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radmultipageOtherInformation">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="radmultipageOtherInformation" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
<asp:SqlDataSource ID="SqlDataSourceWebFormCategory" runat="server" ConnectionString="<%$ ConnectionStrings:SqlConStrSelf%>"
    SelectCommand="select * from Isar.t_WebFormCategorize where IsActive = 1 ORDER BY Priority"></asp:SqlDataSource>
