﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/07>
    /// Description: <اطلاعات زبان های خارجی>
    /// </summary>
    public partial class DossierSacrifice_ForeignLanguagePage : ItcBaseControl
    {
        #region PublicParam:

        private readonly ListeningSkillBL _listeningSkillBl = new ListeningSkillBL();
        private readonly WritingSkillBL _writingSkillBL = new WritingSkillBL();
        private readonly ForeignLanguageBL _foreignLanguageBL = new ForeignLanguageBL();
        private readonly ReadingSkillBL _readingSkillBL = new ReadingSkillBL();
        private readonly SpeakingSkillBL _speakingSkillBL = new SpeakingSkillBL();
        private readonly DossierSacrifice_ForeignLanguageBL _dossierSacrificeTestimonyOrDeathBL = new DossierSacrifice_ForeignLanguageBL();

        private const string TableName = "Isar.v_DossierSacrifice_ForeignLanguage";
        private const string PrimaryKey = "DossierSacrifice_ForeignLanguageId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            cmbForeignLanguage.ClearSelection();
            cmbListeningSkill.ClearSelection();
            cmbSpeakingSkill.ClearSelection();
            CmbReadingSkill.ClearSelection();
            CmbWritingSkill.ClearSelection();
            txtDescription.Text = "";
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeForeignLanguageEntity = new DossierSacrifice_ForeignLanguageEntity()
                                                            {
                                                                DossierSacrifice_ForeignLanguageId = new Guid(ViewState["DossierSacrifice_ForeignLanguageId"].ToString())
                                                            };
            var myDossierSacrificeForeignLanguage = _dossierSacrificeTestimonyOrDeathBL.GetSingleById(dossierSacrificeForeignLanguageEntity);
            cmbForeignLanguage.SelectedValue = myDossierSacrificeForeignLanguage.ForeignLanguageId.ToString();
            txtDescription.Text = myDossierSacrificeForeignLanguage.Description;
            cmbListeningSkill.SelectedValue = myDossierSacrificeForeignLanguage.ListeningSkillId.ToString();
            if (cmbListeningSkill.FindItemByValue(myDossierSacrificeForeignLanguage.ListeningSkillId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مهارت شنیداری انتخاب شده معتبر نمی باشد.");
            }
            cmbSpeakingSkill.SelectedValue = myDossierSacrificeForeignLanguage.SpeakingSkillId.ToString();
            if (cmbSpeakingSkill.FindItemByValue(myDossierSacrificeForeignLanguage.SpeakingSkillId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مهارت گفتاری انتخاب شده معتبر نمی باشد.");
            }
            CmbReadingSkill.SelectedValue = myDossierSacrificeForeignLanguage.ReadingSkillId.ToString();
            if (CmbReadingSkill.FindItemByValue(myDossierSacrificeForeignLanguage.ReadingSkillId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مهارت خواندن انتخاب شده معتبر نمی باشد.");
            }

            CmbWritingSkill.SelectedValue = myDossierSacrificeForeignLanguage.WritingSkillId.ToString();
            if (CmbWritingSkill.FindItemByValue(myDossierSacrificeForeignLanguage.WritingSkillId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مهارت نوشتاری انتخاب شده معتبر نمی باشد.");
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
                                      {
                                          TableName = TableName,
                                          PrimaryKey = PrimaryKey,
                                          RadGrid = grdDossierSacrifice_ForeignLanguage,
                                          PageSize = pageSize,
                                          CurrentPage = currentpPage,
                                          WhereClause = ViewState["WhereClause"].ToString(),
                                          OrderBy = ViewState["SortExpression"].ToString(),
                                          SortType = ViewState["SortType"].ToString(),
                                          RowSelectGuidId = rowSelectGuidId
                                      };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbForeignLanguage.Items.Clear();
            cmbForeignLanguage.Items.Add(new RadComboBoxItem(""));
            cmbForeignLanguage.DataSource = _foreignLanguageBL.GetAllIsActive();
            cmbForeignLanguage.DataTextField = "ForeignLanguagePersianTitle";
            cmbForeignLanguage.DataValueField = "ForeignLanguageId";
            cmbForeignLanguage.DataBind();

            cmbListeningSkill.Items.Clear();
            cmbListeningSkill.Items.Add(new RadComboBoxItem(""));
            cmbListeningSkill.DataSource = _listeningSkillBl.GetAllIsActive();
            cmbListeningSkill.DataTextField = "ListeningSkillPersianTitle";
            cmbListeningSkill.DataValueField = "ListeningSkillId";
            cmbListeningSkill.DataBind();

            cmbSpeakingSkill.Items.Clear();
            cmbSpeakingSkill.Items.Add(new RadComboBoxItem(""));
            cmbSpeakingSkill.DataSource = _speakingSkillBL.GetAllIsActive();
            cmbSpeakingSkill.DataTextField = "SpeakingSkillPersianTitle";
            cmbSpeakingSkill.DataValueField = "SpeakingSkillId";
            cmbSpeakingSkill.DataBind();

            CmbWritingSkill.Items.Clear();
            CmbWritingSkill.Items.Add(new RadComboBoxItem(""));
            CmbWritingSkill.DataSource = _writingSkillBL.GetAllIsActive();
            CmbWritingSkill.DataTextField = "WritingSkillPersianTitle";
            CmbWritingSkill.DataValueField = "WritingSkillId";
            CmbWritingSkill.DataBind();

            CmbReadingSkill.Items.Clear();
            CmbReadingSkill.Items.Add(new RadComboBoxItem(""));
            CmbReadingSkill.DataSource = _readingSkillBL.GetAllIsActive();
            CmbReadingSkill.DataTextField = "ReadingSkillPersianTitle";
            CmbReadingSkill.DataValueField = "ReadingSkillId";
            CmbReadingSkill.DataBind();

        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeTestimonyOrDeathEntity = new DossierSacrifice_ForeignLanguageEntity
                                                                 {
                                                                     DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                                     ForeignLanguageId = Guid.Parse(cmbForeignLanguage.SelectedValue),
                                                                     Description = txtDescription.Text.Trim(),
                                                                     WritingSkillId = (CmbWritingSkill.SelectedIndex > 0) ? Guid.Parse(CmbWritingSkill.SelectedValue) : (Guid?) null,
                                                                     ReadingSkillId = (CmbReadingSkill.SelectedIndex > 0) ? Guid.Parse(CmbReadingSkill.SelectedValue) : (Guid?) null,
                                                                     ListeningSkillId = (cmbListeningSkill.SelectedIndex > 0) ? Guid.Parse(cmbListeningSkill.SelectedValue) : (Guid?) null,
                                                                     SpeakingSkillId = (cmbSpeakingSkill.SelectedIndex > 0) ? Guid.Parse(cmbSpeakingSkill.SelectedValue) : (Guid?) null,
                                                                 };

                _dossierSacrificeTestimonyOrDeathBL.Add(dossierSacrificeTestimonyOrDeathEntity, out dossierSacrificeEducationId);

                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description=" + txtDescription.Text.Trim();
                if (cmbForeignLanguage.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ForeignLanguageId=" + cmbForeignLanguage.Text.Trim();
                if (cmbListeningSkill.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ListeningSkillId ='" + new Guid(cmbListeningSkill.SelectedValue) + "'";
                if (cmbSpeakingSkill.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SpeakingSkillId ='" + new Guid(cmbSpeakingSkill.SelectedValue) + "'";
                if (CmbReadingSkill.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ReadingSkillId ='" + new Guid(CmbReadingSkill.SelectedValue) + "'";
                if (CmbWritingSkill.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WritingSkillId ='" + new Guid(CmbWritingSkill.SelectedValue) + "'";

                grdDossierSacrifice_ForeignLanguage.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_ForeignLanguage.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeStudentEntity = new DossierSacrifice_ForeignLanguageEntity()
                                                        {
                                                            DossierSacrifice_ForeignLanguageId = Guid.Parse(ViewState["DossierSacrifice_ForeignLanguageId"].ToString()),
                                                            DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                            ForeignLanguageId = Guid.Parse(cmbForeignLanguage.SelectedValue),
                                                            Description = txtDescription.Text.Trim(),
                                                            WritingSkillId = (CmbWritingSkill.SelectedIndex > 0) ? Guid.Parse(CmbWritingSkill.SelectedValue) : (Guid?) null,
                                                            ReadingSkillId = (CmbReadingSkill.SelectedIndex > 0) ? Guid.Parse(CmbReadingSkill.SelectedValue) : (Guid?) null,
                                                            ListeningSkillId = (cmbListeningSkill.SelectedIndex > 0) ? Guid.Parse(cmbListeningSkill.SelectedValue) : (Guid?) null,
                                                            SpeakingSkillId = (cmbSpeakingSkill.SelectedIndex > 0) ? Guid.Parse(cmbSpeakingSkill.SelectedValue) : (Guid?) null,
                                                        };
                _dossierSacrificeTestimonyOrDeathBL.Update(dossierSacrificeStudentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_ForeignLanguageId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_ForeignLanguage_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_ForeignLanguageId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_ForeignLanguageEntity()
                                                              {
                                                                  DossierSacrifice_ForeignLanguageId = new Guid(e.CommandArgument.ToString()),

                                                              };
                    _dossierSacrificeTestimonyOrDeathBL.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_ForeignLanguage_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_ForeignLanguage_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_ForeignLanguage_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_ForeignLanguage.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #endregion
    }
}