﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_StudentPage.ascx.cs"
    Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_StudentPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="Student">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="Student">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblAdmissionType" runat="server">نوع پذیرش :</asp:Label>
                            </td>
                            <td width="20%">
                                <cc1:CustomRadComboBox ID="cmbAdmissionType" runat="server" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" 
                                    AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="5%">
                                &nbsp;</td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblEducationCenter" runat="server"> مراکز تحصیلی:</asp:Label>
                            </td>
                            <td width="20%">
                                <cc1:CustomRadComboBox ID="cmbEducationCenter" runat="server" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" 
                                    AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="35%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblprovince" runat="server">استان:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbprovince" runat="server" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="False" 
                                    OnSelectedIndexChanged="cmbprovince_SelectedIndexChanged" Height="300">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
       
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCity" runat="server"> شهر:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbCity" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
               
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEducationCourse" runat="server">رشته تحصیلی:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbEducationCourse" runat="server" 
                                    AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="False" 
                                    OnSelectedIndexChanged="CmbEducationCourse_SelectedIndexChanged" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEducationOrientation" runat="server">گرایش تحصیلی:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbEducationOrientation" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEucationDegree" runat="server"> مقطع تحصیلی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbEucationDegree" runat="server" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                <asp:RequiredFieldValidator ID="rfvEucationDegree" runat="server" 
                                    ControlToValidate="CmbEucationDegree" ErrorMessage="*" 
                                    ValidationGroup="Student"></asp:RequiredFieldValidator>
                            </td>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="LblEducationMode" runat="server"> نحوه تحصیلی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbEducationMode" runat="server" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                <asp:RequiredFieldValidator ID="rfvCmbEducationMode" runat="server" 
                                    ControlToValidate="CmbEducationMode" ErrorMessage="*" 
                                    ValidationGroup="Student"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblGraduateType" runat="server"> نوع فارغ التحصیلی:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbGraduateType" runat="server"  AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblStudentNo" runat="server">شماره دانشجویی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <telerik:RadTextBox ID="txtStudentNo" runat="server" 
                                    MaxLength="10"></telerik:RadTextBox>
                            </td>
                            <td >
                                           <asp:RequiredFieldValidator ID="rfvStudentNo" runat="server" 
                                    ControlToValidate="txtStudentNo" ErrorMessage="*" 
                                    ValidationGroup="Student"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblStartDate" runat="server">تاریخ شروع:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="txtStartDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td >
                                <%--<asp:RequiredFieldValidator ID="rfvStartDate" runat="server" 
                                    ControlToValidate="txtStartDate" ErrorMessage="*" 
                                    ValidationGroup="Student"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEndDate" runat="server">تاریخ پایان:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="txtEndDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="LblInChargeOfEducation" runat="server">نام مسئول آموزش:</asp:Label>
                            </td>
                            <td >
                                <telerik:RadTextBox ID="txtInChargeOfEducation" runat="server" 
                                    MaxLength="10"></telerik:RadTextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblTechnicalDescription" runat="server">توضیحات فنی:</asp:Label>
                            </td>
                            <td >
                                <telerik:RadTextBox ID="txtTechnicalDescription" runat="server" Width="300px" TextMode="MultiLine"></telerik:RadTextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Student" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdDossierSacrifice_Student_ItemCommand"
                                    OnPageIndexChanged="grdDossierSacrifice_Student_PageIndexChanged" OnPageSizeChanged="grdDossierSacrifice_Student_PageSizeChanged"
                                    OnSortCommand="grdDossierSacrifice_Student_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_StudentId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AdmissionTypeTitle" HeaderText="نوع پذیرش" SortExpression="AdmissionTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EducationCenterPersianTitle" HeaderText="عنوان مراکز تحصیلی"
                                                SortExpression="EducationCenterPersianTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EucationDegreePersianTitle" HeaderText="عنوان مقطع تحصیلی"
                                                SortExpression="EucationDegreePersianTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EducationCoursePersianTitle" HeaderText="رشته تحصیلی"
                                                SortExpression="EducationCoursePersianTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EducationOrientationPersianTitle" HeaderText="گرایش تحصیلی"
                                                SortExpression="EducationOrientationPersianTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CityTitlePersian" HeaderText="شهر" SortExpression="CityTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StartDateShamsi" HeaderText="تاریخ شروع" SortExpression="StartDateShamsi">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EndDateShamsi" HeaderText="تاریخ پایان" SortExpression="EndDateShamsi">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StudentNo" HeaderText="شماره دانشجو" SortExpression="StudentNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                    <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_StudentId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_StudentId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_StudentId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbprovince">
            <UpdatedControls>

                <telerik:AjaxUpdatedControl ControlID="cmbCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbEducationCourse">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="cmbEducationOrientation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Student">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
