﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<اطلاعات ظاهری>
    /// </summary>
    public partial class DossierSacrifice_AppearancePage : ItcBaseControl
    {
        #region PublicParam:  
        private readonly BloodGroupBL _BloodGroupBl = new BloodGroupBL();
        private readonly ColorBL _ColorBl = new ColorBL();     
        private readonly DossierSacrifice_AppearanceBL _dossierSacrificeTestimonyOrDeathBL = new DossierSacrifice_AppearanceBL();        

        private const string TableName = "Isar.v_DossierSacrifice_Appearance";
        private const string PrimaryKey = "DossierSacrifice_AppearanceId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {            
            txtWeight.Text = "";
            txtShoeSize.Text = "";
            txtHeight.Text = "";
            txtCostumeSize.Text = "";
            cmbBloodGroup.ClearSelection();          
            cmbEyeColor.ClearSelection();          
            CmbHairColor.ClearSelection();           
            CmbSkinColor.ClearSelection();          
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeAppearanceEntity = new DossierSacrifice_AppearanceEntity()
            {
                DossierSacrifice_AppearanceId = new Guid(ViewState["DossierSacrifice_AppearanceId"].ToString())
            };
            var myDossierSacrificeAppearance = _dossierSacrificeTestimonyOrDeathBL.GetSingleById(dossierSacrificeAppearanceEntity);            
            txtWeight.Text = myDossierSacrificeAppearance.Weight.ToString();
            txtShoeSize.Text = myDossierSacrificeAppearance.ShoeSize.ToString();
            txtHeight.Text = myDossierSacrificeAppearance.Height.ToString();
            txtCostumeSize.Text = myDossierSacrificeAppearance.CostumeSize;
            cmbBloodGroup.SelectedValue = myDossierSacrificeAppearance.BloodGroupId.ToString();
            if (cmbBloodGroup.FindItemByValue(myDossierSacrificeAppearance.BloodGroupId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع پذیرش انتخاب شده معتبر نمی باشد.");
            }           

            cmbEyeColor.SelectedValue = myDossierSacrificeAppearance.EyeColorId.ToString();
            if (cmbEyeColor.FindItemByValue(myDossierSacrificeAppearance.EyeColorId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رنگ چشم انتخاب شده معتبر نمی باشد.");
            }
            CmbHairColor.SelectedValue = myDossierSacrificeAppearance.HairColorId.ToString();
            if (CmbHairColor.FindItemByValue(myDossierSacrificeAppearance.HairColorId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رنگ مو انتخاب شده معتبر نمی باشد.");
            }

            CmbSkinColor.SelectedValue = myDossierSacrificeAppearance.SkinColorId.ToString();
            if (CmbSkinColor.FindItemByValue(myDossierSacrificeAppearance.SkinColorId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رنگ پوست انتخاب شده معتبر نمی باشد.");
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Appearance,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {
            cmbBloodGroup.Items.Clear();
            cmbBloodGroup.Items.Add(new RadComboBoxItem(""));
            cmbBloodGroup.DataSource = _BloodGroupBl.GetAllIsActive();
            cmbBloodGroup.DataTextField = "BloodGroupTitle";
            cmbBloodGroup.DataValueField = "BloodGroupId";
            cmbBloodGroup.DataBind();           

            cmbEyeColor.Items.Clear();
            cmbEyeColor.Items.Add(new RadComboBoxItem(""));
            cmbEyeColor.DataSource = _ColorBl.GetAllIsActive();
            cmbEyeColor.DataTextField = "ColorTitle";
            cmbEyeColor.DataValueField = "ColorId";
            cmbEyeColor.DataBind();

            CmbSkinColor.Items.Clear();
            CmbSkinColor.Items.Add(new RadComboBoxItem(""));
            CmbSkinColor.DataSource = _ColorBl.GetAllIsActive();
            CmbSkinColor.DataTextField = "ColorTitle";
            CmbSkinColor.DataValueField = "ColorId";
            CmbSkinColor.DataBind();

            CmbHairColor.Items.Clear();
            CmbHairColor.Items.Add(new RadComboBoxItem(""));
            CmbHairColor.DataSource = _ColorBl.GetAllIsActive();
            CmbHairColor.DataTextField = "ColorTitle";
            CmbHairColor.DataValueField = "ColorId";
            CmbHairColor.DataBind();            
        }
      
        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeTestimonyOrDeathEntity = new DossierSacrifice_AppearanceEntity
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),                   
                    Weight = (txtWeight.Text.Trim() == "") ? (decimal?)null : decimal.Parse(txtWeight.Text),
                    ShoeSize = (txtShoeSize.Text.Trim() == "") ? (int?)null : Int32.Parse(txtShoeSize.Text),
                    Height = (txtHeight.Text.Trim() == "") ? (int?)null : Int32.Parse(txtHeight.Text),                   
                    SkinColorId = (CmbSkinColor.SelectedIndex>0?Guid.Parse(CmbSkinColor.SelectedValue):new Guid()),
                    HairColorId = (CmbHairColor.SelectedIndex>0?Guid.Parse(CmbHairColor.SelectedValue):new Guid()),
                    BloodGroupId = Guid.Parse(cmbBloodGroup.SelectedValue),
                    EyeColorId = (cmbEyeColor.SelectedIndex>0?Guid.Parse(cmbEyeColor.SelectedValue):new Guid()),    
                    CostumeSize = txtCostumeSize.Text,
                    IsActive = true,
                };

                _dossierSacrificeTestimonyOrDeathBL.Add(dossierSacrificeTestimonyOrDeathEntity, out dossierSacrificeEducationId);

                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtHeight.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Height=" + txtHeight.Text.Trim();
                if (txtWeight.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Weight=" + txtWeight.Text.Trim();
                if (txtShoeSize.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ShoeSize =" + txtShoeSize.Text.Trim();
                if (txtCostumeSize.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CostumeSize like '%" + txtCostumeSize.Text.Trim()+ '%';
                if (cmbBloodGroup.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BloodGroupId ='" + new Guid(cmbBloodGroup.SelectedValue) + "'";
                if (cmbEyeColor.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EyeColorId ='" + new Guid(cmbEyeColor.SelectedValue) + "'";
                if (CmbHairColor.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HairColorId ='" + new Guid(CmbHairColor.SelectedValue) + "'";
                if (CmbSkinColor.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SkinColorId ='" + new Guid(CmbSkinColor.SelectedValue) + "'";

                grdDossierSacrifice_Appearance.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Appearance.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeStudentEntity = new DossierSacrifice_AppearanceEntity()
                {
                    DossierSacrifice_AppearanceId = Guid.Parse(ViewState["DossierSacrifice_AppearanceId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Weight = (txtWeight.Text.Trim() == "") ? (decimal?)null : decimal.Parse(txtWeight.Text),
                    ShoeSize = (txtShoeSize.Text.Trim() == "") ? (int?)null : Int32.Parse(txtShoeSize.Text),
                    Height = (txtHeight.Text.Trim() == "") ? (int?)null : Int32.Parse(txtHeight.Text),
                    SkinColorId = (CmbSkinColor.SelectedIndex > 0 ? Guid.Parse(CmbSkinColor.SelectedValue) : new Guid()),
                    HairColorId = (CmbHairColor.SelectedIndex > 0 ? Guid.Parse(CmbHairColor.SelectedValue) : new Guid()),
                    BloodGroupId = Guid.Parse(cmbBloodGroup.SelectedValue),
                    EyeColorId = (cmbEyeColor.SelectedIndex > 0 ? Guid.Parse(cmbEyeColor.SelectedValue) : new Guid()),
                    CostumeSize = txtCostumeSize.Text,
                    IsActive = true,
                };
                _dossierSacrificeTestimonyOrDeathBL.Update(dossierSacrificeStudentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_AppearanceId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_Appearance_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_AppearanceId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_AppearanceEntity()
                    {
                        DossierSacrifice_AppearanceId = new Guid(e.CommandArgument.ToString()),

                    };
                    _dossierSacrificeTestimonyOrDeathBL.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_Appearance_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_Appearance_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_Appearance_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appearance.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #endregion
    }
}