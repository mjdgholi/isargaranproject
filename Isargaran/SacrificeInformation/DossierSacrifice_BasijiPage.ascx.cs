﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;
using ItcApplicationErrorManagerException = Intranet.Common.ItcException.ItcApplicationErrorManagerException;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/27>
    /// Description: <پرونده  بسیجی ایثارگر>
    /// </summary>

    public partial class DossierSacrifice_BasijiPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly CityBL _cityBl = new CityBL();
        private readonly ProvinceBL _provinceBl = new ProvinceBL();
        private readonly BasijiBaseBL _basijiBaseBL = new BasijiBaseBL();
        private readonly BasijiCategoryBL _basijiCategoryBL = new BasijiCategoryBL();
        private readonly BasijiMembershipTypeBL _basijiMembershipTypeBL = new BasijiMembershipTypeBL();
        private readonly DossierSacrifice_BasijiBL _dossierSacrificeBasijiBL = new DossierSacrifice_BasijiBL();
        private const string TableName = "Isar.V_DossierSacrifice_Basiji";
        private const string PrimaryKey = "DossierSacrifice_BasijiId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtAddress.Text = "";
            txtBasijiCardNo.Text = "";
            txtBasijiCode.Text = "";
            txtEmail.Text = "";
            txtEndDate.Text = "";
            txtEmail.Text = "";
            txtFaxNo.Text = "";
            txtInsuranceDateCard.Text = "";
            txtWebAddress.Text = "";
            txtVillageTitle.Text = "";
            txtTelNo.Text = "";
            txtSectionTitle.Text = "";
            txtEndDate.Text = "";
            txtStartDate.Text = "";
            txtInsuranceSerialNumberCard.Text = "";            
            txtMembershipDate.Text = "";
            txtZipCode.Text = "";
            cmbCity.ClearSelection();
            cmbCity.Items.Clear();
            cmbprovince.ClearSelection();
            cmbBasijiBase.ClearSelection();
            cmbBasijiCategory.ClearSelection();
            cmbBasijiMembershipTypes.ClearSelection();
           

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeBasijiEntity = new DossierSacrifice_BasijiEntity()
            {
                DossierSacrifice_BasijiId = new Guid(ViewState["DossierSacrifice_BasijiId"].ToString())
            };
            var mydossierSacrificeBasiji = _dossierSacrificeBasijiBL.GetSingleById(dossierSacrificeBasijiEntity);
            txtEndDate.Text = mydossierSacrificeBasiji.EndDate;
            txtAddress.Text = mydossierSacrificeBasiji.Address;
            txtEmail.Text = mydossierSacrificeBasiji.Email;
            txtFaxNo.Text = mydossierSacrificeBasiji.FaxNo;
            txtSectionTitle.Text = mydossierSacrificeBasiji.SectionTitle;
            txtStartDate.Text = mydossierSacrificeBasiji.StartDate;
            txtTelNo.Text = mydossierSacrificeBasiji.TelNo;
            txtVillageTitle.Text = mydossierSacrificeBasiji.VillageTitle;
            txtWebAddress.Text = mydossierSacrificeBasiji.WebAddress;
            txtBasijiCardNo.Text = mydossierSacrificeBasiji.BasijiCardNo;
            txtBasijiCode.Text = mydossierSacrificeBasiji.BasijiCode;
            txtInsuranceSerialNumberCard.Text = mydossierSacrificeBasiji.InsuranceSerialNumberCard;
            txtInsuranceDateCard.Text = mydossierSacrificeBasiji.InsuranceDateCard;
            txtMembershipDate.Text = mydossierSacrificeBasiji.MembershipDate;
            txtZipCode.Text = mydossierSacrificeBasiji.ZipCode;
            cmbBasijiCategory.SelectedValue = mydossierSacrificeBasiji.BasijiCategoryId.ToString();
            if (cmbBasijiCategory.FindItemByValue(mydossierSacrificeBasiji.BasijiCategoryId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردرسته بسیجی انتخاب شده معتبر نمی باشد.");
            }
           cmbBasijiMembershipTypes.SelectedValue = mydossierSacrificeBasiji.BasijiMembershipTypeId.ToString();
            if (cmbBasijiMembershipTypes.FindItemByValue(mydossierSacrificeBasiji.BasijiMembershipTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع عضویت بسیج انتخاب شده معتبر نمی باشد.");
            }
            cmbBasijiBase.SelectedValue = mydossierSacrificeBasiji.BasijiBaseId.ToString();
            if (cmbBasijiBase.FindItemByValue(mydossierSacrificeBasiji.BasijiBaseId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد  پایگاه بسیج انتخاب شده معتبر نمی باشد.");
            }
            cmbprovince.SelectedValue = mydossierSacrificeBasiji.ProvinceId.ToString();
            if (cmbprovince.FindItemByValue(mydossierSacrificeBasiji.ProvinceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد استان انتخاب شده معتبر نمی باشد.");
            }
            if (mydossierSacrificeBasiji.ProvinceId.ToString()!="")
            {
                SetCmboboxCity(Guid.Parse(mydossierSacrificeBasiji.ProvinceId.ToString()));
                cmbCity.SelectedValue = mydossierSacrificeBasiji.CityId.ToString();
                if (cmbCity.FindItemByValue(mydossierSacrificeBasiji.CityId.ToString()) == null)
                {
                    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
                }
            }
            



        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Basiji,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbBasijiBase.Items.Clear();
            cmbBasijiBase.Items.Add(new RadComboBoxItem(""));
            cmbBasijiBase.DataSource = _basijiBaseBL.GetAllIsActive();
            cmbBasijiBase.DataTextField = "BasijiBaseTitle";
            cmbBasijiBase.DataValueField = "BasijiBaseId";
            cmbBasijiBase.DataBind();

            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBl.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();

            cmbBasijiCategory.Items.Clear();
            cmbBasijiCategory.Items.Add(new RadComboBoxItem(""));
            cmbBasijiCategory.DataSource = _basijiCategoryBL.GetAllIsActive();
            cmbBasijiCategory.DataTextField = "BasijiCategoryTitle";
            cmbBasijiCategory.DataValueField = "BasijiCategoryId";
            cmbBasijiCategory.DataBind();

            cmbBasijiMembershipTypes.Items.Clear();
            cmbBasijiMembershipTypes.Items.Add(new RadComboBoxItem(""));
            cmbBasijiMembershipTypes.DataSource = _basijiMembershipTypeBL.GetAllIsActive();
            cmbBasijiMembershipTypes.DataTextField = "BasijiMembershipTypeTitle";
            cmbBasijiMembershipTypes.DataValueField = "BasijiMembershipTypeId";
            cmbBasijiMembershipTypes.DataBind();
        }
        private void SetCmboboxCity(Guid ProvinceId)
        {

            var cityEntity = new CityEntity { ProvinceId = ProvinceId };
            cmbCity.Items.Clear();
            cmbCity.Items.Add(new RadComboBoxItem(""));
            cmbCity.DataTextField = "CityTitlePersian";
            cmbCity.DataValueField = "CityId";
            cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
            cmbCity.DataBind();

        }

        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>


        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {                    
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetComboBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeBasijiId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeBasijiEntity = new DossierSacrifice_BasijiEntity()
                {
                 Address = txtAddress.Text,
                 BasijiBaseId = (cmbBasijiBase.SelectedIndex>0?Guid.Parse(cmbBasijiBase.SelectedValue):(Guid?)null),
                 BasijiCardNo = txtBasijiCardNo.Text,
                 BasijiCategoryId = (cmbBasijiCategory.SelectedIndex>0?Guid.Parse(cmbBasijiCategory.SelectedValue):(Guid?)null),
                 BasijiCode = txtBasijiCode.Text,
                 BasijiMembershipTypeId = Guid.Parse(cmbBasijiMembershipTypes.SelectedValue),
                 CityId = (cmbCity.SelectedIndex>0 ?Guid.Parse(cmbCity.SelectedValue):new Guid()),
                 DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                 Email = txtEmail.Text,
                 EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                 FaxNo = txtFaxNo.Text,
                 InsuranceDateCard = ItcToDate.ShamsiToMiladi(txtInsuranceDateCard.Text),
                 InsuranceSerialNumberCard = txtInsuranceSerialNumberCard.Text,                 
                 MembershipDate = ItcToDate.ShamsiToMiladi(txtMembershipDate.Text),
                 SectionTitle = txtSectionTitle.Text,
                 StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                 TelNo = txtTelNo.Text,
                 VillageTitle = txtVillageTitle.Text,
                 WebAddress = txtWebAddress.Text,
                 ZipCode = txtZipCode.Text,
                };

                _dossierSacrificeBasijiBL.Add(dossierSacrificeBasijiEntity, out dossierSacrificeBasijiId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, dossierSacrificeBasijiId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Address Like N'%" +
                                               FarsiToArabic.ToArabic(txtAddress.Text.Trim()) + "%'";
                if (txtBasijiCardNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CardNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtBasijiCardNo.Text.Trim()) + "%'";
                if (txtBasijiCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ClassifiedNo Like N'%" +
                                               Decimal.Parse(txtBasijiCode.Text.Trim()) + "%'";

                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                              (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                              (txtEndDate.Text.Trim()) + "'";
                if (txtInsuranceDateCard.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CardIssueDate ='" +
                                              (txtInsuranceDateCard.Text.Trim()) + "'";
                if (txtMembershipDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CardIssueDate ='" +
                                              (txtMembershipDate.Text.Trim()) + "'";
                if (txtFaxNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FaxNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtFaxNo.Text.Trim()) + "%'";
                if (txtInsuranceSerialNumberCard.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MilitaryServiceLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtInsuranceSerialNumberCard
                                               .Text.Trim()) + "%'";
                if (txtSectionTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SectionTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtSectionTitle.Text.Trim()) + "%'";
                if (txtTelNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TelNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtTelNo.Text.Trim()) + "%'";
                if (txtVillageTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VillageTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtVillageTitle.Text.Trim()) + "%'";
                if (txtWebAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WebAddress Like N'%" +
                                               FarsiToArabic.ToArabic(txtWebAddress.Text.Trim()) + "%'";
                if (txtZipCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ZipCode Like N'%" +
                                               FarsiToArabic.ToArabic(txtZipCode.Text.Trim()) + "%'";
                if (cmbBasijiBase.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BasijiBaseId='" +
                                               new Guid(cmbBasijiBase.SelectedValue) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and provinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                if (cmbBasijiCategory.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BasijiCategoryId ='" +
                                               new Guid(cmbBasijiCategory.SelectedValue) + "'";
                if (cmbBasijiMembershipTypes.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BasijiMembershipTypesId ='" +
                                               new Guid(cmbBasijiMembershipTypes.SelectedValue) + "'";

                grdDossierSacrifice_Basiji.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Basiji.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeBasijiEntity = new DossierSacrifice_BasijiEntity()
                {
                    DossierSacrifice_BasijiId = Guid.Parse(ViewState["DossierSacrifice_BasijiId"].ToString()),
                    Address = txtAddress.Text,
                    BasijiBaseId = (cmbBasijiBase.SelectedIndex > 0 ? Guid.Parse(cmbBasijiBase.SelectedValue) : (Guid?)null),
                    BasijiCardNo = txtBasijiCardNo.Text,
                    BasijiCategoryId = (cmbBasijiCategory.SelectedIndex > 0 ? Guid.Parse(cmbBasijiCategory.SelectedValue) : (Guid?)null),
                    BasijiCode = txtBasijiCode.Text,
                    BasijiMembershipTypeId = Guid.Parse(cmbBasijiMembershipTypes.SelectedValue),
                    CityId = (cmbCity.SelectedIndex > 0 ? Guid.Parse(cmbCity.SelectedValue) : new Guid()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Email = txtEmail.Text,
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    FaxNo = txtFaxNo.Text,
                    InsuranceDateCard = ItcToDate.ShamsiToMiladi(txtInsuranceDateCard.Text),
                    InsuranceSerialNumberCard = txtInsuranceSerialNumberCard.Text,
                    MembershipDate = ItcToDate.ShamsiToMiladi(txtMembershipDate.Text),
                    SectionTitle = txtSectionTitle.Text,
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    TelNo = txtTelNo.Text,
                    VillageTitle = txtVillageTitle.Text,
                    WebAddress = txtWebAddress.Text,
                    ZipCode = txtZipCode.Text,
                };

                _dossierSacrificeBasijiBL.Update(dossierSacrificeBasijiEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_BasijiId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_Basiji_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_BasijiId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeBasijiEntity = new DossierSacrifice_BasijiEntity()
                    {
                        DossierSacrifice_BasijiId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeBasijiBL.Delete(dossierSacrificeBasijiEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdDossierSacrifice_Basiji_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_Basiji_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_Basiji_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Basiji.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
             if (!string.IsNullOrEmpty(e.Value))
             {
                 cmbCity.Text = "";
                cmbCity.Items.Clear();
                SetCmboboxCity(Guid.Parse(e.Value.ToString()));        
            }
                 else
             {
                 cmbCity.Text = "";
                    cmbCity.Items.Clear();
                }


            }
        #endregion
    }
}