﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/27>
    /// Description: <اطلاعات خاص>
    /// </summary>
    public partial class DossierSacrifice_SpecificPage : ItcBaseControl
    {
        #region PublicParam:       
        private readonly BankBL _bankBl = new BankBL();
        private readonly DossierSacrifice_SpecificBL _dossierSacrificeSpecificBl = new DossierSacrifice_SpecificBL();

        private const string TableName = "Isar.v_DossierSacrifice_Specific";
        private const string PrimaryKey = "DossierSacrifice_SpecificId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {            
            txtBankBranchCode.Text = "";
            txtEmail.Text = "";
            txtOpeningAccountDate.Text = "";
            txtWebAddress.Text = "";
            txtDescription.Text = "";
            txtAccountNo.Text = "";
            txtBankBranchTitle.Text = "";
            txtSupplementaryInsuranceNo.Text = "";
            cmbBank.ClearSelection();
            chkIsAccountActive.Checked = true;
            chkIsHasSupplementaryInsurance.Checked = true;
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeSpecificEntity = new DossierSacrifice_SpecificEntity()
            {
                DossierSacrifice_SpecificId = new Guid(ViewState["DossierSacrifice_SpecificId"].ToString())
            };
            var mydossierSacrificeSpecific = _dossierSacrificeSpecificBl.GetSingleById(dossierSacrificeSpecificEntity);
            txtBankBranchCode.Text = mydossierSacrificeSpecific.BankBranchCode;
            txtEmail.Text = mydossierSacrificeSpecific.Email;
            txtOpeningAccountDate.Text = mydossierSacrificeSpecific.OpeningAccountDate;
            txtWebAddress.Text = mydossierSacrificeSpecific.WebAddress;
            txtDescription.Text = mydossierSacrificeSpecific.Description;
            txtAccountNo.Text = mydossierSacrificeSpecific.AccountNo;
            txtBankBranchTitle.Text = mydossierSacrificeSpecific.BankBranchTitle;
            txtSupplementaryInsuranceNo.Text = mydossierSacrificeSpecific.SupplementaryInsuranceNo;            
            cmbBank.SelectedValue = mydossierSacrificeSpecific.BankId.ToString();
            chkIsAccountActive.Checked = mydossierSacrificeSpecific.IsAccountActive;
            chkIsHasSupplementaryInsurance.Checked = mydossierSacrificeSpecific.IsHasSupplementaryInsurance;
            txtSupplementaryInsuranceNo.Visible = mydossierSacrificeSpecific.IsHasSupplementaryInsurance;

            if (cmbBank.FindItemByValue(mydossierSacrificeSpecific.BankId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع پذیرش انتخاب شده معتبر نمی باشد.");
            }          
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Specific,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbBank.Items.Clear();
            cmbBank.Items.Add(new RadComboBoxItem(""));
            cmbBank.DataSource = _bankBl.GetAllIsActive();
            cmbBank.DataTextField = "BankTitle";
            cmbBank.DataValueField = "BankId";
            cmbBank.DataBind();
           

        }
      
        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeEducationEntity = new DossierSacrifice_SpecificEntity()
                                                          {
                                                              BankId = Guid.Parse(cmbBank.SelectedValue),
                                                              DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                              OpeningAccountDate = ItcToDate.ShamsiToMiladi(txtOpeningAccountDate.Text),
                                                              BankBranchCode = txtBankBranchCode.Text,
                                                              Email = txtEmail.Text,
                                                              WebAddress = txtWebAddress.Text,
                                                              Description = txtDescription.Text,
                                                              AccountNo = txtAccountNo.Text,
                                                              BankBranchTitle = txtBankBranchTitle.Text,
                                                              IsActive = true,
                                                              IsAccountActive = chkIsAccountActive.Checked,
                                                              IsHasSupplementaryInsurance = chkIsHasSupplementaryInsurance.Checked,
                                                              SupplementaryInsuranceNo = txtSupplementaryInsuranceNo.Text
                                                          };

                _dossierSacrificeSpecificBl.Add(dossierSacrificeEducationEntity, out dossierSacrificeEducationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtEmail.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Email Like N'%" +
                                               FarsiToArabic.ToArabic(txtEmail.Text.Trim()) + "%'";
                if (txtOpeningAccountDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OpeningAccountDate ='" +
                                               (txtOpeningAccountDate.Text.Trim()) + "'";
                if (txtBankBranchCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BankBranchCode ='" +
                                               (txtBankBranchCode.Text.Trim()) + "'";
                if (txtWebAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WebAddress Like N'%" +
                                               FarsiToArabic.ToArabic(txtWebAddress.Text.Trim()) + "%'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Email Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (cmbBank.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BankId ='" +
                                               new Guid(cmbBank.SelectedValue) + "'";                
                grdDossierSacrifice_Specific.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Specific.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeSpecificEntity = new DossierSacrifice_SpecificEntity()
                {
                    DossierSacrifice_SpecificId = new Guid(ViewState["DossierSacrifice_SpecificId"].ToString()),
                    BankId = Guid.Parse(cmbBank.SelectedValue),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    OpeningAccountDate = ItcToDate.ShamsiToMiladi(txtOpeningAccountDate.Text),
                    BankBranchCode = txtBankBranchCode.Text,
                    Email = txtEmail.Text,
                    WebAddress = txtWebAddress.Text,
                    Description = txtDescription.Text,
                    AccountNo = txtAccountNo.Text,
                    BankBranchTitle = txtBankBranchTitle.Text,
                    IsActive = true,
                    IsAccountActive = chkIsAccountActive.Checked,
                    IsHasSupplementaryInsurance = chkIsHasSupplementaryInsurance.Checked,
                    SupplementaryInsuranceNo = txtSupplementaryInsuranceNo.Text
                };
                _dossierSacrificeSpecificBl.Update(dossierSacrificeSpecificEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_SpecificId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_Specific_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_SpecificId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_SpecificEntity()
                    {
                        DossierSacrifice_SpecificId = new Guid(e.CommandArgument.ToString()),

                    };
                    _dossierSacrificeSpecificBl.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_Specific_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_Specific_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_Specific_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Specific.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
       
        #endregion

        protected void chkIsHasSupplementaryInsurance_CheckedChanged(object sender, EventArgs e)
        {
            txtSupplementaryInsuranceNo.Visible = chkIsHasSupplementaryInsurance.Checked;
            txtSupplementaryInsuranceNo.Text = "";
        }
    }
}