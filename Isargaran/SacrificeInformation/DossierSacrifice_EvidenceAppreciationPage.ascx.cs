﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/06>
    // Description:	<اطلاعات تقدیر کتبی>
    /// </summary>
    public partial class DossierSacrifice_EvidenceAppreciationPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly DossierSacrifice_EvidenceAppreciationBL _dossierSacrificeEvidenceAppreciationBL = new DossierSacrifice_EvidenceAppreciationBL();
        private readonly OrganizationBL _organizationBL = new OrganizationBL();
        private readonly OccasionBL _occasionBL = new OccasionBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_EvidenceAppreciation]";
        private const string PrimaryKey = "DossierSacrifice_EvidenceAppreciationId";
        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEvidenceAppreciationContent.Text = "";
            txtEvidenceAppreciationDescription.Text = "";
            txtOccasionDate.Text = "";
            cmbIsForeignissued.ClearSelection();
            cmbOccasion.ClearSelection();
            cmbOrganization.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeEvidenceAppreciationEntity = new DossierSacrifice_EvidenceAppreciationEntity()
            {
                DossierSacrifice_EvidenceAppreciationId = new Guid(ViewState["DossierSacrifice_EvidenceAppreciationId"].ToString())
            };
            var mydossierSacrificeEvidenceAppreciation = _dossierSacrificeEvidenceAppreciationBL.GetSingleById(dossierSacrificeEvidenceAppreciationEntity);
            txtEvidenceAppreciationContent.Text = mydossierSacrificeEvidenceAppreciation.EvidenceAppreciationContent;
            txtEvidenceAppreciationDescription.Text = mydossierSacrificeEvidenceAppreciation.EvidenceAppreciationDescription;
            txtOccasionDate.Text = mydossierSacrificeEvidenceAppreciation.OccasionDate;
            cmbIsForeignissued.SelectedValue = mydossierSacrificeEvidenceAppreciation.IsForeignissued.ToString();
            cmbOccasion.SelectedValue = mydossierSacrificeEvidenceAppreciation.OccasionId.ToString();
            cmbOrganization.SelectedValue = mydossierSacrificeEvidenceAppreciation.OrganizationId.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_EvidenceAppreciation,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbOrganization.Items.Clear();
            cmbOrganization.Items.Add(new RadComboBoxItem(""));
            cmbOrganization.DataSource = _organizationBL.GetAllIsActive();
            cmbOrganization.DataTextField = "OrganizationPersianTitle";
            cmbOrganization.DataValueField = "OrganizationId";
            cmbOrganization.DataBind();

            cmbOccasion.Items.Clear();
            cmbOccasion.Items.Add(new RadComboBoxItem(""));
            cmbOccasion.DataSource = _occasionBL.GetAllIsActive();
            cmbOccasion.DataTextField = "OccasionTitle";
            cmbOccasion.DataValueField = "OccasionId";
            cmbOccasion.DataBind();

        }


        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_EvidenceAppreciationId ;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                var dossierSacrificeEvidenceAppreciationEntity = new DossierSacrifice_EvidenceAppreciationEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    EvidenceAppreciationContent = FarsiToArabic.ToArabic(txtEvidenceAppreciationContent.Text),
                    OccasionDate = ItcToDate.ShamsiToMiladi(txtOccasionDate.Text),
                    OccasionId = Guid.Parse(cmbOccasion.SelectedValue),
                    OrganizationId = (cmbOrganization.SelectedIndex > 0 ? Guid.Parse(cmbOrganization.SelectedValue) : new Guid()),
                    EvidenceAppreciationDescription = txtEvidenceAppreciationDescription.Text,
                    IsForeignissued = bool.Parse(cmbIsForeignissued.SelectedItem.Value),                    
                };

                _dossierSacrificeEvidenceAppreciationBL.Add(dossierSacrificeEvidenceAppreciationEntity, out DossierSacrifice_EvidenceAppreciationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, DossierSacrifice_EvidenceAppreciationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                if (txtOccasionDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDate ='" +
                                              (txtOccasionDate.Text.Trim()) + "'";


                if (txtEvidenceAppreciationContent.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EvidenceAppreciationContent Like N'%" +
                                               FarsiToArabic.ToArabic(txtEvidenceAppreciationContent.Text.Trim()) + "%'";
                if (txtEvidenceAppreciationDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EvidenceAppreciationDescription Like N'%" +
                                               FarsiToArabic.ToArabic(txtEvidenceAppreciationDescription.Text.Trim()) + "%'";
                if (cmbOccasion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionId ='" +
                                               new Guid(cmbOccasion.SelectedValue) + "'";
                if (cmbOrganization.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationId ='" +
                                               new Guid(cmbOrganization.SelectedValue) + "'";
                if (cmbIsForeignissued.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and IsForeignissued ='" +
                                               new Guid(cmbIsForeignissued.SelectedItem.Value) + "'";

                grdDossierSacrifice_EvidenceAppreciation.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_EvidenceAppreciation.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeEvidenceAppreciationEntity = new DossierSacrifice_EvidenceAppreciationEntity()
                {
                    DossierSacrifice_EvidenceAppreciationId = Guid.Parse(ViewState["DossierSacrifice_EvidenceAppreciationId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    EvidenceAppreciationContent = FarsiToArabic.ToArabic(txtEvidenceAppreciationContent.Text),
                    OccasionDate = ItcToDate.ShamsiToMiladi(txtOccasionDate.Text),
                    OccasionId = Guid.Parse(cmbOccasion.SelectedValue),
                    OrganizationId = (cmbOrganization.SelectedIndex > 0 ? Guid.Parse(cmbOrganization.SelectedValue) : new Guid()),
                    EvidenceAppreciationDescription = txtEvidenceAppreciationDescription.Text,
                    IsForeignissued = bool.Parse(cmbIsForeignissued.SelectedItem.Value),
                };
                _dossierSacrificeEvidenceAppreciationBL.Update(dossierSacrificeEvidenceAppreciationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_EvidenceAppreciationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_EvidenceAppreciation_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_EvidenceAppreciationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEvidenceAppreciationEntity = new DossierSacrifice_EvidenceAppreciationEntity()
                    {
                        DossierSacrifice_EvidenceAppreciationId = Guid.Parse(e.CommandArgument.ToString()),                        
                    };
                    _dossierSacrificeEvidenceAppreciationBL.Delete(dossierSacrificeEvidenceAppreciationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_EvidenceAppreciation_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_EvidenceAppreciation_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_EvidenceAppreciation_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_EvidenceAppreciation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
    }
}