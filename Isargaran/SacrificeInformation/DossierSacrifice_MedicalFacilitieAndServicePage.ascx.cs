﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/18>
    /// Description: <پرونده مشخصات تسهیلات و خدمات پزشکی>
    /// </summary>  
    public partial class DossierSacrifice_MedicalFacilitieAndServicePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly MedicalFacilitieAndServiceBL _medicalFacilitieAndServiceBL = new MedicalFacilitieAndServiceBL();
        private readonly DossierSacrifice_MedicalFacilitieAndServiceBL _dossierSacrificeMedicalFacilitieAndService = new DossierSacrifice_MedicalFacilitieAndServiceBL();
        private const string TableName = "Isar.V_DossierSacrifice_MedicalFacilitieAndService";
        private const string PrimaryKey = "DossierSacrifice_MedicalFacilitieAndServiceId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtFinancialCost.Text = "";
            txtMedicalFacilitieAndServiceDate.Text = "";
            cmbMedicalFacilitieAndService.ClearSelection();


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeMedicalFacilitieAndServiceEntity = new DossierSacrifice_MedicalFacilitieAndServiceEntity()
            {
                DossierSacrifice_MedicalFacilitieAndServiceId = Guid.Parse(ViewState["DossierSacrifice_MedicalFacilitieAndServiceId"].ToString())
            };
            var mydossierSacrificeMedicalFacilitieAndService = _dossierSacrificeMedicalFacilitieAndService.GetSingleById(dossierSacrificeMedicalFacilitieAndServiceEntity);
            cmbMedicalFacilitieAndService.SelectedValue = mydossierSacrificeMedicalFacilitieAndService.MedicalFacilitieAndServiceId.ToString();
            if (cmbMedicalFacilitieAndService.FindItemByValue(mydossierSacrificeMedicalFacilitieAndService.MedicalFacilitieAndServiceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد عنوان تسهیلات و خدمات پزشکی انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = mydossierSacrificeMedicalFacilitieAndService.Description;
            txtFinancialCost.Text = mydossierSacrificeMedicalFacilitieAndService.FinancialCost.ToString();
            txtMedicalFacilitieAndServiceDate.Text = mydossierSacrificeMedicalFacilitieAndService.MedicalFacilitieAndServiceDate;

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_MedicalFacilitieAndService,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbMedicalFacilitieAndService.Items.Clear();
            cmbMedicalFacilitieAndService.Items.Add(new RadComboBoxItem(""));
            cmbMedicalFacilitieAndService.DataSource = _medicalFacilitieAndServiceBL.GetAllIsActive();
            cmbMedicalFacilitieAndService.DataTextField = "MedicalFacilitieAndServiceTitle";
            cmbMedicalFacilitieAndService.DataValueField = "MedicalFacilitieAndServiceId";
            cmbMedicalFacilitieAndService.DataBind();



        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeMedicalFacilitieAndServiceId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeMedicalFacilitieAndServiceEntity = new DossierSacrifice_MedicalFacilitieAndServiceEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    FinancialCost = (txtFinancialCost.Text==""?(decimal?)null:decimal.Parse(txtFinancialCost.Text)),
                    Description = txtDescription.Text,
                    MedicalFacilitieAndServiceDate = ItcToDate.ShamsiToMiladi(txtMedicalFacilitieAndServiceDate.Text),
                    MedicalFacilitieAndServiceId = Guid.Parse(cmbMedicalFacilitieAndService.SelectedValue),                    
                };
                _dossierSacrificeMedicalFacilitieAndService.Add(dossierSacrificeMedicalFacilitieAndServiceEntity, out dossierSacrificeMedicalFacilitieAndServiceId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, dossierSacrificeMedicalFacilitieAndServiceId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtFinancialCost.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FinancialCost =' " +
                                               (txtFinancialCost.Text.Trim()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtMedicalFacilitieAndServiceDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MedicalFacilitieAndServiceDate ='" +
                                               (txtMedicalFacilitieAndServiceDate.Text.Trim()) + "'";

                if (cmbMedicalFacilitieAndService.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And MedicalFacilitieAndServiceId='" +
                                               (cmbMedicalFacilitieAndService.SelectedValue.Trim()) + "'";
                grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_MedicalFacilitieAndService.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeMedicalFacilitieAndServiceEntity = new DossierSacrifice_MedicalFacilitieAndServiceEntity()
                {
                    DossierSacrifice_MedicalFacilitieAndServiceId = Guid.Parse(ViewState["DossierSacrifice_MedicalFacilitieAndServiceId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    FinancialCost = (txtFinancialCost.Text == "" ? (decimal?)null : decimal.Parse(txtFinancialCost.Text)),
                    Description = txtDescription.Text,
                    MedicalFacilitieAndServiceDate = ItcToDate.ShamsiToMiladi(txtMedicalFacilitieAndServiceDate.Text),
                    MedicalFacilitieAndServiceId = Guid.Parse(cmbMedicalFacilitieAndService.SelectedValue),
                };
                _dossierSacrificeMedicalFacilitieAndService.Update(dossierSacrificeMedicalFacilitieAndServiceEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_MedicalFacilitieAndServiceId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_MedicalFacilitieAndService_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_MedicalFacilitieAndServiceId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeMedicalFacilitieAndServiceEntity = new DossierSacrifice_MedicalFacilitieAndServiceEntity()
                    {
                        DossierSacrifice_MedicalFacilitieAndServiceId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeMedicalFacilitieAndService.Delete(dossierSacrificeMedicalFacilitieAndServiceEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_MedicalFacilitieAndService_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_MedicalFacilitieAndService_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_MedicalFacilitieAndService_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MedicalFacilitieAndService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}