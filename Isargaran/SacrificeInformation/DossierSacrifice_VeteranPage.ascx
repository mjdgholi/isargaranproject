﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_VeteranPage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_VeteranPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="DossierSacrifice_Veteran">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="DossierSacrifice_Veteran">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblWarZone" runat="server">مناطق جنگی :</asp:Label>
                            </td>
                            <td width="20%">
                                <cc1:CustomRadComboBox ID="cmbWarZone" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="5%">
                                &nbsp;</td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblVeteranCod" runat="server"> کد جانبازی:</asp:Label>
                            </td>
                            <td width="20%">
                                                            <telerik:RadTextBox ID="txtVeteranCod" runat="server" 
                                    MaxLength="10"></telerik:RadTextBox>
                            </td>
                            <td width="35%">
                                &nbsp;</td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblVeteranDate" runat="server">تاریخ جانبازی:</asp:Label>
                            </td>
                            <td >
       <cc1:CustomItcCalendar ID="txtVeteranDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblVeteranPercent" runat="server">درصد جانبازی<font color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                              <telerik:RadTextBox ID="txtVeteranPercent" runat="server" 
                                    MaxLength="2"></telerik:RadTextBox>
                            </td>
                            <td >
                                                               <asp:RequiredFieldValidator ID="rfvVeteranPercent" runat="server" 
                                    ControlToValidate="txtVeteranPercent" ErrorMessage="*" 
                                    ValidationGroup="DossierSacrifice_Veteran"></asp:RequiredFieldValidator></td>
                        </tr>
                         <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblSacrificePeriodTime" runat="server">دوره زمانی ایثارگری<font color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbSacrificePeriodTime" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                          <asp:RequiredFieldValidator ID="rfvSacrificePeriodTime" runat="server" 
                                    ControlToValidate="cmbSacrificePeriodTime" ErrorMessage="*" 
                                    ValidationGroup="DossierSacrifice_Veteran"></asp:RequiredFieldValidator>  
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblRehabilitationEquipment" runat="server"> تجهیزات توانبخشی:</asp:Label>
                            </td>
                            <td >
                                                     <telerik:RadTextBox ID="txtRehabilitationEquipment" runat="server" 
                                    ></telerik:RadTextBox>
                            </td>
                            <td >                                
                            </td>
                        </tr>
                                                <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblVeteranBodyRegion" runat="server">ناحیه جانبازی بدن:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbVeteranBodyRegion" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                               
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblMilitaryUnitDispatcher" runat="server"> یگان اعزام کننده:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbMilitaryUnitDispatcher" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >                                
                            </td>
                        </tr>
                                                                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblPost" runat="server">پست:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbPost" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                               
                            </td>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblOrganizationPhysicalChart" runat="server"> محل خدمت سازمانی قبل از جانبازی:</asp:Label>
                            </td>
                            <td >
       <cc1:SelectControl ID="SelectControlOrganizationPhysicalChart" runat="server" 
                                    PortalPathUrl="GeneralProject/General/ModalForm/OrganizationPhysicalChartPage.aspx" ToolTip="ساختار سازمانی" imageName="OrganizationPhysicalChart"
                                    MultiSelect="False" RadWindowWidth="950" RadWindowHeight="700" />
                            </td>
                            <td >                                
                            </td>
                        </tr>
<%--                        <tr>
                                                        <td>
                                <asp:Label CssClass="LabelCSS" ID="lblPreVeteranEucationDegree" runat="server">مقطع تحصیلی قبلی  
                        </asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbPreVeteranEucationDegree" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >

                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEucationDegree" runat="server"> مقطع تحصیلی فعلی:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbEucationDegree" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
               
                            </td>
                            
                        </tr>--%>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblVeteranFoundationDossierNo" runat="server"> شماره پرونده بنیاد جانبازان:</asp:Label>
                            </td>
                            <td >
                                        <telerik:RadTextBox ID="txtVeteranFoundationDossierNo" runat="server" 
                                    MaxLength="10"></telerik:RadTextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblDurationOfPresenceInBattlefield" runat="server">مدت حضور در جبهه:</asp:Label>
                            </td>
                            <td >
                                <telerik:RadTextBox ID="txtDurationOfPresenceInBattlefield" runat="server" 
                                    MaxLength="10"></telerik:RadTextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                                                <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblprovince" runat="server">استان:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbprovince" runat="server" 
                                    AppendDataBoundItems="True" AutoPostBack="True" CausesValidation="False"  AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300"
                                    OnSelectedIndexChanged="cmbprovince_SelectedIndexChanged">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
   
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCity" runat="server"> شهر:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbCity" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
  
                            </td>
                        </tr>
                        <tr>
<%--                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblStartDate" runat="server">تاریخ شروع:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="txtStartDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td >

                            </td>--%>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEndDate" runat="server">تاریخ پایان:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="txtEndDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td >
                                &nbsp;</td>
                            
                                                        <td>
                                <asp:Label CssClass="LabelCSS" ID="LblDescription" runat="server">توضیحات:</asp:Label>
                            </td>
                            <td >
                                <telerik:RadTextBox ID="txtDescription" runat="server"  TextMode="MultiLine"
                                    MaxLength="10"></telerik:RadTextBox>
                            </td>
                            
                            <td >
                                &nbsp;</td>
                        </tr>

                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Veteran" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" 
                                    onitemcommand="grdDossierSacrifice_Veteran_ItemCommand" 
                                    onpageindexchanged="grdDossierSacrifice_Veteran_PageIndexChanged" 
                                    onpagesizechanged="grdDossierSacrifice_Veteran_PageSizeChanged" 
                                    onsortcommand="grdDossierSacrifice_Veteran_SortCommand" >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_VeteranId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="VeteranCod" HeaderText="کد جانبازی" SortExpression="VeteranCod">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="WarZoneTitle" HeaderText="عنوان  منطقه جنگی"
                                                SortExpression="WarZoneTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                             <telerik:GridBoundColumn DataField="VeteranDate" HeaderText="تاریخ جانبازی"
                                                SortExpression="VeteranDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                      <telerik:GridBoundColumn DataField="VeteranBodyRegionTitle" HeaderText="ناحیه جانبازی بدن"
                                                SortExpression="VeteranBodyRegionTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="VeteranPercent" HeaderText="درصد جانبازی"
                                                SortExpression="VeteranPercent">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                               <telerik:GridBoundColumn DataField="DurationOfPresenceInBattlefield" HeaderText="مدت حضور در جبهه"
                                                SortExpression="DurationOfPresenceInBattlefield">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                               <telerik:GridBoundColumn DataField="VeteranFoundationDossierNo" HeaderText="شماره پرونده بنیاد جانباز"
                                                SortExpression="VeteranFoundationDossierNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                                                                     
                                              
                                            <telerik:GridBoundColumn DataField="PreVeteranEucationDegreePersianTitle" HeaderText="مقطع تحصیلی قبل از جانبازی"
                                                SortExpression="PreVeteranEucationDegreePersianTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                                        <telerik:GridBoundColumn DataField="EucationDegreePersianTitle" HeaderText="مقطع تحصیلی فعلی"
                                                SortExpression="EucationDegreePersianTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="OrganizationPhysicalChartTitle" HeaderText="ساختار سازمانی"
                                                SortExpression="OrganizationPhysicalChartTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MilitaryUnitDispatcherTitle" HeaderText="یگان اعزام کننده"
                                                SortExpression="MilitaryUnitDispatcherTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                          <telerik:GridBoundColumn DataField="SacrificePeriodTimeTitle" HeaderText="دوره زمانی ایثارگری"
                                                SortExpression="SacrificePeriodTimeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CityTitlePersian" HeaderText="شهر" SortExpression="CityTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StartDate" HeaderText="تاریخ شروع" SortExpression="StartDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EndDate" HeaderText="تاریخ پایان" SortExpression="EndDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn> 
                                                                                          <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_VeteranId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                             <telerik:GridTemplateColumn HeaderText="کپی" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnopy" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_VeteranId").ToString() %>'
                                                        CommandName="_MyCopy" ForeColor="#000066" ImageUrl="../Images/copy.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>    
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_VeteranId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_VeteranId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbprovince">
            <UpdatedControls>
 
                <telerik:AjaxUpdatedControl ControlID="cmbCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbEducationCourse">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="cmbEducationOrientation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Veteran">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
