﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_AppearancePage.ascx.cs"
    Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_AppearancePage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="Appearance">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="Appearance">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                        <tr>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblBloodGroup" runat="server">گروه خونی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="20%">
                                <cc1:CustomRadComboBox ID="cmbBloodGroup" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="rfvBloodGroup" runat="server" ControlToValidate="cmbBloodGroup"
                                    ErrorMessage="*" ValidationGroup="Appearance"></asp:RequiredFieldValidator>
                            </td>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblEyeColor" runat="server"> رنگ چشم:</asp:Label>
                            </td>
                            <td width="20%">
                                <cc1:CustomRadComboBox ID="cmbEyeColor" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="35%">
             <%--                   <asp:RequiredFieldValidator ID="rfvEyeColor" runat="server" ControlToValidate="cmbEyeColor"
                                    ErrorMessage="*" ValidationGroup="Appearance"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" >
                                <asp:Label CssClass="LabelCSS" ID="lblHairColor" runat="server">رنگ مو:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbHairColor" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    CausesValidation="False">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
          <%--                      <asp:RequiredFieldValidator ID="rfvHairColor" runat="server" ControlToValidate="CmbHairColor"
                                    ErrorMessage="*" ValidationGroup="Appearance"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblSkinColor" runat="server"> رنگ پوست:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbSkinColor" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
<%--                                <asp:RequiredFieldValidator ID="rfvSkinColor" runat="server" ControlToValidate="CmbSkinColor"
                                    ErrorMessage="*" ValidationGroup="Appearance"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="LblHeight" runat="server">قد:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="txtHeight" runat="server" DataType="System.Int32">
                                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="LblWeight" runat="server">وزن:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="txtWeight" runat="server" DataType="System.Decimal">
                                    <NumberFormat DecimalDigits="2" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblShoeSize" runat="server">سایز کفش:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="txtShoeSize" runat="server" DataType="System.Int32">
                                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblCostumeSize" runat="server">سایز لباس:</asp:Label>
                            </td>
                            <td dir="rtl">
                                <telerik:RadNumericTextBox ID="txtCostumeSize" runat="server" DataType="System.Int32">
                                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Appearance" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdDossierSacrifice_Appearance_ItemCommand"
                                    OnPageIndexChanged="grdDossierSacrifice_Appearance_PageIndexChanged" OnPageSizeChanged="grdDossierSacrifice_Appearance_PageSizeChanged"
                                    OnSortCommand="grdDossierSacrifice_Appearance_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_AppearanceId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="BloodGroupTitle" HeaderText="گروه خونی" SortExpression="BloodGroupTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EyeColorTitle" HeaderText="رنگ چشم" SortExpression="EyeColorTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="HairColorTitle" HeaderText="رنگ مو" SortExpression="HairColorTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="SkinColorTitle" HeaderText="رنگ پوست" SortExpression="SkinColorTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Height" HeaderText="قد" SortExpression="Height">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Weight" HeaderText="وزن" SortExpression="Weight">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ShoeSize" HeaderText="سایز کفش" SortExpression="ShoeSize">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CostumeSize" HeaderText="سایز لباس" SortExpression="CostumeSize">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                                       <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_AppearanceId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_AppearanceId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_AppearanceId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbprovince">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="cmbCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbHairColor">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="CmbMilitaryUnitDispatcher" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Appearance">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
<div id="dd" style="top: 5px; right: 5px"></div>