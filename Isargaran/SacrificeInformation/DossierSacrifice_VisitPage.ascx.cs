﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/02/15>
    // Description:	<اطلاعات ملاقات>
    /// </summary>
    public partial class DossierSacrifice_VisitPage : ItcBaseControl
    {

        #region PublicParam:
        private readonly DossierSacrifice_VisitBL _dossierSacrificeVisitBL = new DossierSacrifice_VisitBL();
        private readonly OccasionBL _occasionBL = new OccasionBL();       
        private const string TableName = "[Isar].[V_DossierSacrifice_Visit]";
        private const string PrimaryKey = "DossierSacrifice_VisitId";
        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";            
            
            txtVisitDate.Text = "";
            txtVisitLocation.Text = "";
            txtVisitors.Text = "";
            chkHasAction.Checked = false;            
            chkHasAction.BackColor = Color.White;
            txtActionDescription.Enabled = false;
            txtActionDescription.BackColor = Color.Silver;
            txtActionDescription.Text = "";            
            
            
            cmbOccasion.ClearSelection();
            chkHasRequest.Checked = false;
            chkHasRequest.BackColor = Color.White;  
            txtRequestContent.Enabled = false;
            txtRequestContent.BackColor = Color.Silver;
            txtRequestContent.Text = "";
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeVisitEntity = new DossierSacrifice_VisitEntity()
            {
                DossierSacrifice_VisitId = new Guid(ViewState["DossierSacrifice_VisitId"].ToString())
            };
            var mydossierSacrificeVisit = _dossierSacrificeVisitBL.GetSingleById(dossierSacrificeVisitEntity);
            txtDescription.Text = mydossierSacrificeVisit.Description;
            txtActionDescription.Text = mydossierSacrificeVisit.ActionDescription;
            txtRequestContent.Text = mydossierSacrificeVisit.RequestContent;
            txtVisitDate.Text = mydossierSacrificeVisit.VisitDate;
            txtVisitLocation.Text = mydossierSacrificeVisit.VisitLocation;
            txtVisitors.Text = mydossierSacrificeVisit.Visitors;
            chkHasAction.Checked = mydossierSacrificeVisit.HasAction;
            chkHasAction_CheckedChanged(chkHasAction,new EventArgs());
            chkHasRequest.Checked = mydossierSacrificeVisit.HasRequest;
            chkHasRequest_CheckedChanged(chkHasRequest, new EventArgs());
            cmbOccasion.SelectedValue = mydossierSacrificeVisit.OccasionId.ToString();
            if (cmbOccasion.FindItemByValue(mydossierSacrificeVisit.OccasionId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردمناسبت انتخاب شده معتبر نمی باشد.");
            }

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Visit,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {


            cmbOccasion.Items.Clear();
            cmbOccasion.Items.Add(new RadComboBoxItem(""));
            cmbOccasion.DataSource = _occasionBL.GetAllIsActive();
            cmbOccasion.DataTextField = "OccasionTitle";
            cmbOccasion.DataValueField = "OccasionId";
            cmbOccasion.DataBind();

        }


        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_VisitId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                var dossierSacrificeVisitEntity = new DossierSacrifice_VisitEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    OccasionId = (cmbOccasion.SelectedIndex>0?Guid.Parse(cmbOccasion.SelectedValue):new Guid()),
                    HasAction = bool.Parse(chkHasAction.Checked.ToString()),
                    HasRequest = bool.Parse(chkHasRequest.Checked.ToString()),
                    RequestContent = txtRequestContent.Text,
                    VisitDate = ItcToDate.ShamsiToMiladi(txtVisitDate.Text),
                    VisitLocation = txtVisitLocation.Text,
                    Visitors = txtVisitors.Text,
                    ActionDescription = txtActionDescription.Text
                };

                _dossierSacrificeVisitBL.Add(dossierSacrificeVisitEntity, out DossierSacrifice_VisitId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, DossierSacrifice_VisitId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                if (txtVisitDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VisitDate ='" +
                                              (txtVisitDate.Text.Trim()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtRequestContent.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and RequestContent Like N'%" +
                                               FarsiToArabic.ToArabic(txtRequestContent.Text.Trim()) + "%'";
                if (txtVisitLocation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VisitLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtVisitLocation.Text.Trim()) + "%'";
                if (txtVisitors.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Visitors Like N'%" +
                                               FarsiToArabic.ToArabic(txtVisitors.Text.Trim()) + "%'";

                if (txtActionDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ActionDescription Like N'%" +
                                               FarsiToArabic.ToArabic(txtActionDescription.Text.Trim()) + "%'";
                if (cmbOccasion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionId ='" +
                                               new Guid(cmbOccasion.SelectedValue) + "'";


                grdDossierSacrifice_Visit.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Visit.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeVisitEntity = new DossierSacrifice_VisitEntity()
                {
                    DossierSacrifice_VisitId = Guid.Parse(ViewState["DossierSacrifice_VisitId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    OccasionId = (cmbOccasion.SelectedIndex > 0 ? Guid.Parse(cmbOccasion.SelectedValue) : new Guid()),
                    HasAction = bool.Parse(chkHasAction.Checked.ToString()),
                    HasRequest = bool.Parse(chkHasRequest.Checked.ToString()),
                    RequestContent = txtRequestContent.Text,
                    VisitDate = ItcToDate.ShamsiToMiladi(txtVisitDate.Text),
                    VisitLocation = txtVisitLocation.Text,
                    Visitors = txtVisitors.Text,
                    ActionDescription = txtActionDescription.Text
                };
                _dossierSacrificeVisitBL.Update(dossierSacrificeVisitEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_VisitId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_Visit_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_VisitId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetClearToolBox();
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeVisitEntity = new DossierSacrifice_VisitEntity()
                    {
                        DossierSacrifice_VisitId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeVisitBL.Delete(dossierSacrificeVisitEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdDossierSacrifice_Visit_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_Visit_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_Visit_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Visit.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion

        protected void chkHasRequest_CheckedChanged(object sender, EventArgs e)
        {

            if (chkHasRequest.Checked)
            {
                txtRequestContent.Enabled = true;
                txtRequestContent.BackColor = Color.White;
            }
            else
            {
                txtRequestContent.Enabled = false;
                txtRequestContent.BackColor = Color.Silver;
                txtRequestContent.Text = "";
            }
        }

        protected void chkHasAction_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHasAction.Checked)
            {
                txtActionDescription.Enabled = true;
                txtActionDescription.BackColor = Color.White;
            }
            else
            {
                txtActionDescription.Enabled = false;
                txtActionDescription.BackColor = Color.Silver;
                txtActionDescription.Text = "";
            }
        }
    }
}