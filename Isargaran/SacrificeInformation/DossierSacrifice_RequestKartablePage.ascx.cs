﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    public partial class DossierSacrifice_RequestKartablePage : ItcBaseControl
    {

        #region PublicParam:
        private readonly DossierSacrifice_RequestBL _dossierSacrificeRequestBL = new DossierSacrifice_RequestBL();        
        private readonly RequestTypeBL _requestTypeBL = new RequestTypeBL();
        private readonly ImportanceTypeBL _importanceTypeBL = new ImportanceTypeBL();
        private readonly StatusBL _statusBL = new StatusBL();
        private readonly ActionTypeBL _actionTypeBL = new ActionTypeBL();        
        private const string TableName = "[Isar].[V_DossierSacrifice_RequestKartable]";
        private const string PrimaryKey = "DossierSacrifice_RequestId";

        #endregion

        #region Procedure:


        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtRequestDescription.Text = "";
            cmbImportanceType.ClearSelection();
            cmbRequestType.ClearSelection();
            cmbStatus.ClearSelection();
            cmbActionType.ClearSelection();
            txtDossierSacrificeNo.Text = "";
            SelectPerson.KeyId = "";
            SelectPerson.Title = "";
        }




        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            const string objectTitlesForRowAcess = "[General].[t_Province]";
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Request,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId,
                UserId = userId,
                ObjectTitlesForRowAcess = objectTitlesForRowAcess
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {
            cmbImportanceType.Items.Clear();
            cmbImportanceType.Items.Add(new RadComboBoxItem(""));
            cmbImportanceType.DataSource = _importanceTypeBL.GetAllIsActive();
            cmbImportanceType.DataTextField = "ImportanceTypeTitle";
            cmbImportanceType.DataValueField = "ImportanceTypeId";
            cmbImportanceType.DataBind();

            cmbRequestType.Items.Clear();
            cmbRequestType.Items.Add(new RadComboBoxItem(""));
            cmbRequestType.DataSource = _requestTypeBL.GetAllIsActive();
            cmbRequestType.DataTextField = "RequestTypeTitle";
            cmbRequestType.DataValueField = "RequestTypeId";
            cmbRequestType.DataBind();


            cmbActionType.Items.Clear();
            cmbActionType.Items.Add(new RadComboBoxItem(""));
            cmbActionType.DataSource = _actionTypeBL.GetAllIsActive();
            cmbActionType.DataTextField = "ActionTypeTitle";
            cmbActionType.DataValueField = "ActionTypeId";
            cmbActionType.DataBind();

            cmbStatus.Items.Clear();
            cmbStatus.Items.Add(new RadComboBoxItem(""));
            cmbStatus.DataSource = _statusBL.GetAllIsActive();
            cmbStatus.DataTextField = "StatusTitle";
            cmbStatus.DataValueField = "StatusId";
            cmbStatus.DataBind();

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeRequestEntity = new DossierSacrifice_RequestEntity()
            {
                DossierSacrifice_RequestId = Guid.Parse(ViewState["DossierSacrifice_RequestId"].ToString())
            };
            var mydossierSacrificeRequest = _dossierSacrificeRequestBL.GetSingleById(dossierSacrificeRequestEntity);
            cmbRequestType.SelectedValue = mydossierSacrificeRequest.RequestTypeId.ToString();
            if (cmbRequestType.FindItemByValue(mydossierSacrificeRequest.RequestTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع درخواست انتخاب شده معتبر نمی باشد.");
            }
            cmbImportanceType.SelectedValue = mydossierSacrificeRequest.ImportanceTypeId.ToString();
            if (cmbImportanceType.FindItemByValue(mydossierSacrificeRequest.ImportanceTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع اهمیت درخواست انتخاب شده معتبر نمی باشد.");
            }

            txtDossierSacrificeNo.Text = mydossierSacrificeRequest.DossierSacrificeNo;
            SelectPerson.KeyId = mydossierSacrificeRequest.PersonId.ToString();
            SelectPerson.Title = mydossierSacrificeRequest.FullName;
            txtRequestDescription.Text = mydossierSacrificeRequest.RequestDescription;


        }
        /// <summary>
        /// صفحه مورد نظر که از طریق کلیک بر روی سربرگ انتخاب شده است به مالتی پیج اضافه میگردد
        /// </summary>
        private void AddPageView()
        {
            var pageView = new RadPageView
            {
                ID = ViewState["MenuPageName"].ToString()
            };
            radmultipageOtherInformation.PageViews.Add(pageView);
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSearch.Visible = true;
            btnShowAll.Visible = false;
            PnlOtherInformation.Visible = false;
            grdDossierSacrifice_Request.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {            
            btnSearch.Visible = false;                     
            PnlOtherInformation.Visible = false;
            grdDossierSacrifice_Request.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }
        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                //System.Web.UI.HtmlControls.HtmlLink rowCss;
                //rowCss = new System.Web.UI.HtmlControls.HtmlLink();
                //rowCss.Href = Intranet.Configuration.Settings.PortalSettings.PortalPath +
                //              "/DeskTopModules/IsargaranProject/Isargaran/Style/RowRadGridView.CSS";
                //rowCss.Attributes.Add("rel", "stylesheet");
                //rowCss.Attributes.Add("type", "text/css");
                //Page.Header.Controls.Add(rowCss);
                ViewState["WhereClause"] = "";
                ViewState["SortExpression"] = "IsViewed";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Request.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);                
                SetClearToolBox();
                SetComboBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1=1 ";

                if (txtRequestDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and RequestDescription Like N'%" +
                                               FarsiToArabic.ToArabic(txtRequestDescription.Text.Trim()) + "%'";
                if (cmbImportanceType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ImportanceTypeId='" +
                                               (cmbImportanceType.SelectedValue.Trim()) + "'";
                if (cmbRequestType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And RequestTypeId='" +
                                               (cmbRequestType.SelectedValue.Trim()) + "'";
                if (cmbStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And StatusId='" +
                                               (cmbStatus.SelectedValue.Trim()) + "'";
                if (cmbActionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ActionTypeId='" +
                                               (cmbActionType.SelectedValue.Trim()) + "'";
                if(SelectPerson.KeyId!="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And PersonId='" +
                                               (SelectPerson.KeyId.Trim()) + "'";
                if(txtDossierSacrificeNo.Text!="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And DossierSacrificeNo Like N'%" +
                                               (txtDossierSacrificeNo.Text.Trim()) + "%'";

                grdDossierSacrifice_Request.MasterTableView.CurrentPageIndex = 0;              
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Request.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                if (grdDossierSacrifice_Request.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {                
                SetClearToolBox();
                ViewState["WhereClause"] = "1=1 ";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Request.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
          /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_Request_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyShowRequest")
                {
                    ViewState["DossierSacrifice_RequestId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    DossierSacrifice_RequestEntity dossierSacrificeRequestEntity=new DossierSacrifice_RequestEntity()
                    {
                        DossierSacrifice_RequestId = Guid.Parse(ViewState["DossierSacrifice_RequestId"].ToString())
                    };
                    _dossierSacrificeRequestBL.UpDateIsView(dossierSacrificeRequestEntity);
                }

                if (e.CommandName == "_MyAction")
                {
                    PnlOtherInformation.Visible = true;
                    var dossierSacrificeRequestId = Guid.Parse(e.CommandArgument.ToString());                                                       
                    ViewState["WhereClause"] = "DossierSacrifice_RequestId='" + dossierSacrificeRequestId + "'";
                    Session["DossierSacrifice_RequestId"] = dossierSacrificeRequestId;
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Request.MasterTableView.PageSize, 0, dossierSacrificeRequestId);
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                    e.Item.Selected = true;
                    grdDossierSacrifice_Request.MasterTableView.GetColumnSafe("ShowAll").Visible = true;
                    ViewState["MenuPageName"] = "DossierSacrifice_RequestHistoryPage";
                    RadTabStripTabClick(new object(), new RadTabStripEventArgs(RadTabStrip.FindTabByValue("DossierSacrifice_RequestHistoryPage")));
                }

                
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_Request_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Request.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_Request_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Request.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_Request_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Request.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

        protected void grdDossierSacrifice_Request_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = e.Item as GridDataItem;
                if (dataItem["IsViewed"].Text == "False")
                    dataItem.CssClass = "RowRadGridView";
            }
        }

        protected void RadTabStripTabClick(object sender, RadTabStripEventArgs e)
        {
            ViewState["MenuPageName"] = e.Tab.Value;
            radmultipageOtherInformation.PageViews.Clear();

            switch (e.Tab.Value)
            {
                case "DossierSacrifice_RequestHistoryPage":
                    {
                        ViewState["control"] = @"DesktopModules\IsargaranProject\Isargaran\SacrificeInformation\DossierSacrifice_RequestHistoryPage.ascx";
                        AddPageView();
                        e.Tab.Selected = true;
                        break;
                    }
                case "MessagePage":
                    {
                        ViewState["control"] = @"DesktopModules\IsargaranProject\Isargaran\SacrificeInformation\MessagePage.ascx";
                        AddPageView();
                        e.Tab.Selected = true;
                        break;
                    }

                default:
                    return;
            }
        }

        protected void radmultipageOtherInformation_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            if (ViewState["control"] != null)
            {
                string userControlName = ViewState["control"].ToString();
                var userControl = (Page.LoadControl(userControlName));
                if (userControl != null)
                {
                    userControl.ID = ViewState["MenuPageName"] + "_userControl";
                }
                if (userControl != null) e.PageView.Controls.Add(userControl);
                var tabedControl = (radmultipageOtherInformation.PageViews[0]).Controls[0] as ITC.Library.Classes.ITabedControl;
                if (tabedControl != null) tabedControl.InitControl();
                e.PageView.Selected = true;
            }
        }

    }
}