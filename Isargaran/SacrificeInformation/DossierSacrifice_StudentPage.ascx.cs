﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/19>
    /// Description: <اطلاعات تحصیلی ایثارگر>
    /// </summary>
    public partial class DossierSacrifice_StudentPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly CityBL _cityBl = new CityBL();
        private readonly ProvinceBL _provinceBl = new ProvinceBL();
        private readonly AdmissionTypeBL _admissionTypeBl = new AdmissionTypeBL();
        private readonly EducationCenterBL _educationCenterBl = new EducationCenterBL();
        private readonly EducationCourseBL _educationCourseBl = new EducationCourseBL();
        private readonly EducationDegreeBL _educationDegreeBl = new EducationDegreeBL();
        private readonly EducationModeBL _educationModeBL = new EducationModeBL();
        private readonly GraduateTypeBL _graduateTypeBL = new GraduateTypeBL();
        private readonly EducationCourse_EducationOrientationBL _educationCourseEducationOrientationBl = new EducationCourse_EducationOrientationBL();
        private readonly DossierSacrifice_StudentBL _dossierSacrificeStudentBl = new DossierSacrifice_StudentBL();

        private const string TableName = "Isar.v_DossierSacrifice_Student";
        private const string PrimaryKey = "DossierSacrifice_StudentId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEndDate.Text = "";
            txtInChargeOfEducation.Text = "";
            txtStartDate.Text = "";
            txtStudentNo.Text = "";
            txtTechnicalDescription.Text = "";
            cmbAdmissionType.ClearSelection();
            cmbCity.ClearSelection();
            cmbEducationCenter.ClearSelection();
            cmbprovince.ClearSelection();
            cmbEducationOrientation.ClearSelection();
            CmbEducationCourse.ClearSelection();
            cmbprovince.ClearSelection();
            CmbEucationDegree.ClearSelection();
            CmbEducationMode.ClearSelection();
            CmbGraduateType.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeStudentEntity = new DossierSacrifice_StudentEntity()
                                                    {
                                                        DossierSacrifice_StudentId = new Guid(ViewState["DossierSacrifice_StudentId"].ToString())
                                                    };
            var mydossierSacrificeStudent = _dossierSacrificeStudentBl.GetSingleById(dossierSacrificeStudentEntity);
            txtEndDate.Text = mydossierSacrificeStudent.EndDate;
            txtInChargeOfEducation.Text = mydossierSacrificeStudent.InChargeOfEducation;
            txtStartDate.Text = mydossierSacrificeStudent.StartDate;
            txtStudentNo.Text = mydossierSacrificeStudent.StudentNo;
            txtTechnicalDescription.Text = mydossierSacrificeStudent.TechnicalDescription;
            cmbAdmissionType.SelectedValue = mydossierSacrificeStudent.AdmissionTypeId.ToString();
            if (cmbAdmissionType.FindItemByValue(mydossierSacrificeStudent.AdmissionTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع پذیرش انتخاب شده معتبر نمی باشد.");
            }

            var cityId = (mydossierSacrificeStudent.CityId);
            if (mydossierSacrificeStudent.CityId.ToString()!="")
            {
                var cityEntity = new CityEntity() { CityId = cityId };
                cityEntity = _cityBl.GetSingleById(cityEntity);
                var provinceId = cityEntity.ProvinceId;
                cmbprovince.SelectedValue = provinceId.ToString();
                if (cmbprovince.FindItemByValue(provinceId.ToString()) == null)
                {
                    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
                }
                SetCmboboxCity(provinceId);
                cmbCity.SelectedValue = mydossierSacrificeStudent.CityId.ToString();
                if (cmbCity.FindItemByValue(mydossierSacrificeStudent.CityId.ToString()) == null)
                {
                    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
                }
            }
        

            cmbEducationCenter.SelectedValue = mydossierSacrificeStudent.EducationCenterId.ToString();
            if (cmbEducationCenter.FindItemByValue(mydossierSacrificeStudent.EducationCenterId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مراکز آموزشی انتخاب شده معتبر نمی باشد.");
            }
            CmbEducationCourse.SelectedValue = mydossierSacrificeStudent.EducationCourseId.ToString();
            if (CmbEducationCourse.FindItemByValue(mydossierSacrificeStudent.EducationCourseId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رشته تحصیلی انتخاب شده معتبر نمی باشد.");
            }
            SetCmboboxEducationOrientation(mydossierSacrificeStudent.EducationCourseId.GetValueOrDefault());
            cmbEducationOrientation.SelectedValue = mydossierSacrificeStudent.EducationOrientationId.ToString();
            if (cmbEducationOrientation.FindItemByValue(mydossierSacrificeStudent.EducationOrientationId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد گرایش تحصیلی انتخاب شده معتبر نمی باشد.");
            }

            CmbEucationDegree.SelectedValue = mydossierSacrificeStudent.EucationDegreeId.ToString();
            if (CmbEucationDegree.FindItemByValue(mydossierSacrificeStudent.EucationDegreeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مقطع تحصیلی انتخاب شده معتبر نمی باشد.");
            }

            CmbEducationMode.SelectedValue = mydossierSacrificeStudent.EducationModeId.ToString();
            if (CmbEducationMode.FindItemByValue(mydossierSacrificeStudent.EducationModeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نحوه تحصیلی انتخاب شده معتبر نمی باشد.");
            }

            CmbGraduateType.SelectedValue = mydossierSacrificeStudent.GraduateTypeId.ToString();
            if (CmbGraduateType.FindItemByValue(mydossierSacrificeStudent.GraduateTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع فارغ التحصیلی انتخاب شده معتبر نمی باشد.");
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
                                      {
                                          TableName = TableName,
                                          PrimaryKey = PrimaryKey,
                                          RadGrid = grdDossierSacrifice_Student,
                                          PageSize = pageSize,
                                          CurrentPage = currentpPage,
                                          WhereClause = ViewState["WhereClause"].ToString(),
                                          OrderBy = ViewState["SortExpression"].ToString(),
                                          SortType = ViewState["SortType"].ToString(),
                                          RowSelectGuidId = rowSelectGuidId
                                      };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbAdmissionType.Items.Clear();
            cmbAdmissionType.Items.Add(new RadComboBoxItem(""));
            cmbAdmissionType.DataSource = _admissionTypeBl.GetAllIsActive();
            cmbAdmissionType.DataTextField = "AdmissionTypeTitle";
            cmbAdmissionType.DataValueField = "AdmissionTypeId";
            cmbAdmissionType.DataBind();

            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBl.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();

            cmbEducationCenter.Items.Clear();
            cmbEducationCenter.Items.Add(new RadComboBoxItem(""));
            cmbEducationCenter.DataSource = _educationCenterBl.GetAllIsActive();
            cmbEducationCenter.DataTextField = "EducationCenterPersianTitle";
            cmbEducationCenter.DataValueField = "EducationCenterId";
            cmbEducationCenter.DataBind();

            CmbEucationDegree.Items.Clear();
            CmbEucationDegree.Items.Add(new RadComboBoxItem(""));
            CmbEucationDegree.DataSource = _educationDegreeBl.GetAllIsActive();
            CmbEucationDegree.DataTextField = "EucationDegreePersianTitle";
            CmbEucationDegree.DataValueField = "EucationDegreeId";
            CmbEucationDegree.DataBind();

            CmbEducationCourse.Items.Clear();
            CmbEducationCourse.Items.Add(new RadComboBoxItem(""));
            CmbEducationCourse.DataSource = _educationCourseBl.GetAllIsActive();
            CmbEducationCourse.DataTextField = "EducationCoursePersianTitle";
            CmbEducationCourse.DataValueField = "EducationCourseId";
            CmbEducationCourse.DataBind();

            CmbEducationMode.Items.Clear();
            CmbEducationMode.Items.Add(new RadComboBoxItem(""));
            CmbEducationMode.DataTextField = "EducationModePersianTitle";
            CmbEducationMode.DataValueField = "EducationModeId";
            CmbEducationMode.DataSource = _educationModeBL.GetAllIsActive();
            CmbEducationMode.DataBind();

            CmbGraduateType.Items.Clear();
            CmbGraduateType.Items.Add(new RadComboBoxItem(""));
            CmbGraduateType.DataTextField = "GraduateTypeTitle";
            CmbGraduateType.DataValueField = "GraduateTypeId";
            CmbGraduateType.DataSource = _graduateTypeBL.GetAllIsActive();
            CmbGraduateType.DataBind();

        }

        private void SetCmboboxCity(Guid ProvinceId)
        {
            var cityEntity = new CityEntity {ProvinceId = ProvinceId};
            cmbCity.Items.Clear();
            cmbCity.Items.Add(new RadComboBoxItem(""));
            cmbCity.DataTextField = "CityTitlePersian";
            cmbCity.DataValueField = "CityId";
            cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
            cmbCity.DataBind();
        }

        private void SetCmboboxEducationOrientation(Guid EducationCourseId)
        {
            var educationCourseEducationOrientationEntity = new EducationCourse_EducationOrientationEntity() {EducationCourseId = EducationCourseId};
            cmbEducationOrientation.Items.Clear();
            cmbEducationOrientation.Items.Add(new RadComboBoxItem(""));
            cmbEducationOrientation.DataTextField = "EducationOrientationPersianTitle";
            cmbEducationOrientation.DataValueField = "EducationOreintionId";
            cmbEducationOrientation.DataSource = _educationCourseEducationOrientationBl.GetEducationCourse_EducationOrientationCollectionByEducationCourse(educationCourseEducationOrientationEntity);
            cmbEducationOrientation.DataBind();
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeEducationEntity = new DossierSacrifice_StudentEntity()
                                                          {
                                                              AdmissionTypeId = (cmbAdmissionType.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(cmbAdmissionType.SelectedValue),
                                                              CityId = (cmbCity.SelectedIndex>0 ? Guid.Parse(cmbCity.SelectedValue):(Guid?)null),
                                                              EducationCenterId = (cmbEducationCenter.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(cmbEducationCenter.SelectedValue),
                                                              EducationOrientationId = (cmbEducationOrientation.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(cmbEducationOrientation.SelectedValue),
                                                              EucationDegreeId = Guid.Parse(CmbEucationDegree.SelectedValue),
                                                              DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                              EducationCourseId = (CmbEducationCourse.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(CmbEducationCourse.SelectedValue),
                                                              StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                                                              EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                                                              InChargeOfEducation = txtInChargeOfEducation.Text,
                                                              StudentNo = txtStudentNo.Text,
                                                              TechnicalDescription = txtTechnicalDescription.Text,
                                                              EducationModeId = Guid.Parse(CmbEducationMode.SelectedValue),
                                                              GraduateTypeId = (CmbGraduateType.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(CmbGraduateType.SelectedValue),
                                                          };

                _dossierSacrificeStudentBl.Add(dossierSacrificeEducationEntity, out dossierSacrificeEducationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtInChargeOfEducation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and InChargeOfEducation Like N'%" +
                                               FarsiToArabic.ToArabic(txtInChargeOfEducation.Text.Trim()) + "%'";
                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                               (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                               (txtEndDate.Text.Trim()) + "'";
                if (txtStudentNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StudentNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtStudentNo.Text.Trim()) + "%'";
                if (txtTechnicalDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and InChargeOfEducation Like N'%" +
                                               FarsiToArabic.ToArabic(txtTechnicalDescription.Text.Trim()) + "%'";
                if (cmbAdmissionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and AdmissionTypeId ='" +
                                               new Guid(cmbAdmissionType.SelectedValue) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbEducationCenter.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCenterId ='" +
                                               new Guid(cmbEducationCenter.SelectedValue) + "'";
                if (cmbEducationOrientation.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationOrientationId ='" +
                                               new Guid(cmbEducationOrientation.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and provinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                if (CmbEducationCourse.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCourseId ='" +
                                               new Guid(CmbEducationCourse.SelectedValue) + "'";
                if (CmbEucationDegree.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EucationDegreeId ='" +
                                               new Guid(CmbEucationDegree.SelectedValue) + "'";
                if (CmbEducationMode.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EucationModeId ='" +
                                               new Guid(CmbEducationMode.SelectedValue) + "'";
                if (CmbGraduateType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and GraduateTypeId ='" +
                                               new Guid(CmbGraduateType.SelectedValue) + "'";
                grdDossierSacrifice_Student.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Student.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeStudentEntity = new DossierSacrifice_StudentEntity()
                                                        {
                                                            DossierSacrifice_StudentId = Guid.Parse(ViewState["DossierSacrifice_StudentId"].ToString()),
                                                            AdmissionTypeId = (cmbAdmissionType.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(cmbAdmissionType.SelectedValue),
                                                            CityId = (cmbCity.SelectedIndex > 0 ? Guid.Parse(cmbCity.SelectedValue) : (Guid?)null),
                                                            EducationCenterId = (cmbEducationCenter.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(cmbEducationCenter.SelectedValue),
                                                            EducationOrientationId = (cmbEducationOrientation.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(cmbEducationOrientation.SelectedValue),
                                                            EucationDegreeId = Guid.Parse(CmbEucationDegree.SelectedValue),
                                                            DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                            EducationCourseId = (CmbEducationCourse.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(CmbEducationCourse.SelectedValue),
                                                            StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                                                            EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                                                            InChargeOfEducation = txtInChargeOfEducation.Text,
                                                            StudentNo = txtStudentNo.Text,
                                                            TechnicalDescription = txtTechnicalDescription.Text,
                                                            EducationModeId = Guid.Parse(CmbEducationMode.SelectedValue),
                                                            GraduateTypeId = (CmbGraduateType.SelectedIndex <= 0) ? (Guid?) null : Guid.Parse(CmbGraduateType.SelectedValue),
                                                        };
                _dossierSacrificeStudentBl.Update(dossierSacrificeStudentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_StudentId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_Student_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_StudentId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_StudentEntity()
                                                              {
                                                                  DossierSacrifice_StudentId = new Guid(e.CommandArgument.ToString()),

                                                              };
                    _dossierSacrificeStudentBl.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_Student_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_Student_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_Student_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Student.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// لود کردن گرایش براساس شناسه رشته تحصیلی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CmbEducationCourse_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            cmbEducationOrientation.Items.Clear();
            SetCmboboxEducationOrientation(Guid.Parse(e.Value.ToString()));
        }

        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                cmbCity.Items.Clear();
                cmbCity.Text = "";
                SetCmboboxCity(Guid.Parse(e.Value.ToString()));    
            }
            else
            {
                cmbCity.Items.Clear();
                cmbCity.Text = "";
            }
            
        }

        #endregion
    }
}