﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/10>
    /// Description: <پرونده  ااطلاعات مهارت علمی و فنی ایثارگری>
    /// </summary>
    public partial class DossierSacrifice_ScientificAndTechnicalSkillPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly DossierSacrifice_ScientificAndTechnicalSkillBL _dossierSacrificeScientificAndTechnicalSkillBL =
            new DossierSacrifice_ScientificAndTechnicalSkillBL();

        private const string TableName = "Isar.V_DossierSacrifice_ScientificAndTechnicalSkill";
        private const string PrimaryKey = "DossierSacrifice_ScientificAndTechnicalSkillId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtExperienceYearNo.Text = "";
            txtPlaceOfGraduation.Text = "";
            txtScientificAndTechnicalSkillTitle.Text = "";
            cmbHasEvidence.ClearSelection();


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeScientificAndTechnicalSkillEntity = new DossierSacrifice_ScientificAndTechnicalSkillEntity
                ()
                                                                        {
                                                                            DossierSacrifice_ScientificAndTechnicalSkillId
                                                                                =
                                                                                Guid.Parse(
                                                                                    ViewState[
                                                                                        "DossierSacrifice_ScientificAndTechnicalSkillId"
                                                                                        ].ToString())
                                                                        };
            var mydossierSacrificeScientificAndTechnicalSkill =
                _dossierSacrificeScientificAndTechnicalSkillBL.GetSingleById(
                    dossierSacrificeScientificAndTechnicalSkillEntity);

            txtDescription.Text = mydossierSacrificeScientificAndTechnicalSkill.Description;
            txtExperienceYearNo.Text = mydossierSacrificeScientificAndTechnicalSkill.ExperienceYearNo.ToString();
            txtPlaceOfGraduation.Text = mydossierSacrificeScientificAndTechnicalSkill.PlaceOfGraduation;
            txtScientificAndTechnicalSkillTitle.Text =
                mydossierSacrificeScientificAndTechnicalSkill.ScientificAndTechnicalSkillTitle;
            cmbHasEvidence.SelectedValue = mydossierSacrificeScientificAndTechnicalSkill.HasEvidence.ToString();

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
                                      {
                                          TableName = TableName,
                                          PrimaryKey = PrimaryKey,
                                          RadGrid = grdDossierSacrifice_ScientificAndTechnicalSkill,
                                          PageSize = pageSize,
                                          CurrentPage = currentpPage,
                                          WhereClause = ViewState["WhereClause"].ToString(),
                                          OrderBy = ViewState["SortExpression"].ToString(),
                                          SortType = ViewState["SortType"].ToString(),
                                          RowSelectGuidId = rowSelectGuidId
                                      };
            return gridParamEntity;
        }





        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" +
                                           Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_ScientificAndTechnicalSkillId;
                ViewState["WhereClause"] = " DossierSacrificeId='" +
                                           Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeScientificAndTechnicalSkillEntity = new DossierSacrifice_ScientificAndTechnicalSkillEntity
                    ()
                                                                            {
                                                                                ScientificAndTechnicalSkillTitle =
                                                                                    txtScientificAndTechnicalSkillTitle.
                                                                                    Text,
                                                                                DossierSacrificeId =
                                                                                    Guid.Parse(
                                                                                        Session["DossierSacrificeId"].
                                                                                            ToString()),
                                                                                Description = txtDescription.Text,
                                                                                ExperienceYearNo =
                                                                                    (txtExperienceYearNo.Text == ""
                                                                                         ? (int?) null
                                                                                         : int.Parse(
                                                                                             txtExperienceYearNo.Text)),
                                                                                HasEvidence =(cmbHasEvidence.SelectedIndex>0?bool.Parse(cmbHasEvidence.SelectedItem.Value):(bool?)null)
                                                                              ,
                                                                                PlaceOfGraduation =
                                                                                    txtPlaceOfGraduation.Text,
                                                                            };
                _dossierSacrificeScientificAndTechnicalSkillBL.Add(dossierSacrificeScientificAndTechnicalSkillEntity,
                                                                   out DossierSacrifice_ScientificAndTechnicalSkillId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0,
                                 DossierSacrifice_ScientificAndTechnicalSkillId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" +
                                           Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtExperienceYearNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ExperienceYearNo =' " +
                                               (txtExperienceYearNo.Text.Trim()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtPlaceOfGraduation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PlaceOfGraduation Like N'%" +
                                               FarsiToArabic.ToArabic(txtPlaceOfGraduation.Text.Trim()) + "%'";
                if (txtScientificAndTechnicalSkillTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] +
                                               " and ScientificAndTechnicalSkillTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtScientificAndTechnicalSkillTitle.Text.Trim()) +
                                               "%'";

                if (cmbHasEvidence.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And HasEvidence='" +
                                               (cmbHasEvidence.SelectedItem.Value.Trim()) + "'";
                grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_ScientificAndTechnicalSkill.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" +
                                           Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeScientificAndTechnicalSkillEntity = new DossierSacrifice_ScientificAndTechnicalSkillEntity
                    ()
                                                                            {
                                                                                DossierSacrifice_ScientificAndTechnicalSkillId
                                                                                    =
                                                                                    Guid.Parse(
                                                                                        ViewState[
                                                                                            "DossierSacrifice_ScientificAndTechnicalSkillId"
                                                                                            ].ToString()),
                                                                                ScientificAndTechnicalSkillTitle =
                                                                                    txtScientificAndTechnicalSkillTitle.
                                                                                    Text,
                                                                                DossierSacrificeId =
                                                                                    Guid.Parse(
                                                                                        Session["DossierSacrificeId"].
                                                                                            ToString()),
                                                                                Description = txtDescription.Text,
                                                                                ExperienceYearNo =
                                                                                    (txtExperienceYearNo.Text == ""
                                                                                         ? (int?) null
                                                                                         : int.Parse(
                                                                                             txtExperienceYearNo.Text)),
                                                               HasEvidence =(cmbHasEvidence.SelectedIndex>0?bool.Parse(cmbHasEvidence.SelectedItem.Value):(bool?)null),
                                                                                PlaceOfGraduation =
                                                                                    txtPlaceOfGraduation.Text,
                                                                            };
                _dossierSacrificeScientificAndTechnicalSkillBL.Update(dossierSacrificeScientificAndTechnicalSkillEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0,
                                 new Guid(ViewState["DossierSacrifice_ScientificAndTechnicalSkillId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_ScientificAndTechnicalSkill_ItemCommand(object sender,
                                                                                   Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_ScientificAndTechnicalSkillId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" +
                                               Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeScientificAndTechnicalSkillEntity = new DossierSacrifice_ScientificAndTechnicalSkillEntity
                        ()
                                                                                {
                                                                                    DossierSacrifice_ScientificAndTechnicalSkillId
                                                                                        =
                                                                                        Guid.Parse(
                                                                                            e.CommandArgument.ToString()),
                                                                                };
                    _dossierSacrificeScientificAndTechnicalSkillBL.Delete(
                        dossierSacrificeScientificAndTechnicalSkillEntity);
                    var gridParamEntity =
                        SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0,
                                     new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>



        protected void grdDossierSacrifice_ScientificAndTechnicalSkill_PageIndexChanged(object sender,
                                                                                        Telerik.Web.UI.
                                                                                            GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize,
                                 e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_ScientificAndTechnicalSkill_PageSizeChanged(object sender,
                                                                                       Telerik.Web.UI.
                                                                                           GridPageSizeChangedEventArgs
                                                                                           e)
        {
            try
            {
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_ScientificAndTechnicalSkill_SortCommand(object sender,
                                                                                   Telerik.Web.UI.
                                                                                       GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity =
                    SetGridParam(grdDossierSacrifice_ScientificAndTechnicalSkill.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}