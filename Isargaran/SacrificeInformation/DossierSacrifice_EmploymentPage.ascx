﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_EmploymentPage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_EmploymentPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="DossierSacrificeEmployment">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="DossierSacrificeEmployment">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblOrganizationPhysicalChart" runat="server">ساختا سازمانی :</asp:Label>
                            </td>
                            <td width="25%">
                                    <cc1:SelectControl ID="SelectControlOrganizationPhysicalChart" runat="server" 
                                    PortalPathUrl="GeneralProject/General/ModalForm/OrganizationPhysicalChartPage.aspx" ToolTip="ساختار سازمانی" imageName="OrganizationPhysicalChart"
                                    MultiSelect="False" RadWindowWidth="950" RadWindowHeight="700" />
                       
                            </td>
                            <td width="5%"></td>
                            <td width="10%"></td>
                            <td width="20%"></td>
                            <td width="30%"></td>
                            

                        </tr>
                                                <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblPost" runat="server"> پست:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbPost" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                
                            </td>
                            
                            <td nowrap="nowrap" >
                                <asp:Label CssClass="LabelCSS" ID="lblDurationTimeAtWorkId" runat="server"> ساعت حضور در سرکار:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbDurationTimeAtWorkId" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
         
                            </td>
                        </tr>
                                                <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblEmploymentType" runat="server"> نوع استخدام:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbEmploymentType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                
                            </td>
                           <td >
                                <asp:Label CssClass="LabelCSS" ID="lblEmploymentDate" runat="server"> تاریخ استخدام:</asp:Label>
                            </td>
                            <td >
                              <cc1:CustomItcCalendar ID="txtEmploymentDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td>

                            </td>
                           
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblprovince" runat="server">استان<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbprovince" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300"
                                    AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cmbprovince_SelectedIndexChanged">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="cmbprovince" 
                                    ValidationGroup="DossierSacrificeEmployment" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCity" runat="server"> شهر<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbCity" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="CmbCity"
                                    ValidationGroup="DossierSacrificeEmployment" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblPersonnelNo" runat="server">شماره پرسنلی:</asp:Label>
                            </td>
                            <td align="right">
                            <telerik:RadTextBox ID="txtPersonnelNo" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
              
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCommandmentNo" runat="server">شماره حکم:</asp:Label>
                            </td>
                            <td align="right">
                                          <telerik:RadTextBox ID="txtCommandmentNo" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                                                <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblFreeJob" runat="server">عنوان شغل آزاد:</asp:Label>
                            </td>
                            <td align="right">
                            <telerik:RadTextBox ID="txtFreeJob" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
              
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblDirectManager" runat="server">مدیر مستقیم:</asp:Label>
                            </td>
                            <td align="right">
                                          <telerik:RadTextBox ID="txtDirectManager" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                                
                            </td>
                        </tr>
                        <tr>

                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblTelNo" runat="server">شماره تلفن محل کار:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtTelNo" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                                                       <td>
                                <asp:Label CssClass="LabelCSS" ID="lblFaxNo" runat="server">شماره فکس:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtFaxNo" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                                                <tr>

                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblPostalCod" runat="server">کد پستی:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtPostalCod" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                                                       <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEmail" runat="server">ایمیل:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtEmail" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                                                                        <tr>

                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblWebAddress" runat="server">وب سایت:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtWebAddress" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                                                       <td>
                                <asp:Label CssClass="LabelCSS" ID="lblAddress" runat="server">آدرس:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtAddress" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblStartDate" runat="server">تاریخ شروع<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtStartDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate"
                                    ValidationGroup="DossierSacrificeEmployment" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEndDate" runat="server">تاریخ پایان:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtEndDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td>
                            </td>
                        </tr>

                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_Employment" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" 
                                    onitemcommand="grdDossierSacrifice_Employment_ItemCommand" 
                                    onpageindexchanged="grdDossierSacrifice_Employment_PageIndexChanged" 
                                    onpagesizechanged="grdDossierSacrifice_Employment_PageSizeChanged" 
                                    onsortcommand="grdDossierSacrifice_Employment_SortCommand" >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_EmploymentId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="OrganizationPhysicalChartTitle" HeaderText="عنوان ساختار سازمانی" SortExpression="OrganizationPhysicalChartTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EmploymentTypeTitle" HeaderText="عنوان نوع استخدام"
                                                SortExpression="EmploymentTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EmploymentDate" HeaderText="تاریخ استخدام"
                                                SortExpression="EmploymentDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PostTitle" HeaderText="پست"
                                                SortExpression="PostTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PersonnelNo" HeaderText="شماره پرسنلی"
                                                SortExpression="PersonnelNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                             <telerik:GridBoundColumn DataField="CommandmentNo" HeaderText="شماره حکم"
                                                SortExpression="CommandmentNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                             <telerik:GridBoundColumn DataField="DurationTimeAtWorkTitle" HeaderText="تعداد ساعت حضور "
                                                SortExpression="DurationTimeAtWorkTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CityTitlePersian" HeaderText="شهر" SortExpression="CityTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StartDate" HeaderText="تاریخ شروع" SortExpression="StartDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EndDate" HeaderText="تاریخ پایان" SortExpression="EndDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                         <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_EmploymentId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_EmploymentId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_EmploymentId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbprovince">
            <UpdatedControls>
         
                <telerik:AjaxUpdatedControl ControlID="cmbCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_Employment">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" >
</telerik:RadAjaxLoadingPanel>
