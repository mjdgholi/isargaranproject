﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/02/17>
    /// Description: <اطلاعات مصرف داروهای خاص>
    /// </summary>
    public partial class DossierSacrifice_SpecificMedicationPage :ItcBaseControl
    {

        #region PublicParam:
        private readonly DossierSacrifice_SpecificMedicationBL _dossierSacrificeSpecificMedicationBL = new DossierSacrifice_SpecificMedicationBL();
        private readonly SpecificMedicationBL _SpecificMedicationBL = new SpecificMedicationBL();
        private readonly CountryBL _CountryBL = new CountryBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_SpecificMedication]";
        private const string PrimaryKey = "DossierSacrifice_SpecificMedicationId";
        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtMedicationConsum.Text = "";
            txtDescription.Text = "";
            txtMedicationNo.Text = "";
            txtMedicationPrize.Text = "";
            txtDescription.Text = "";
            txtStartDate.Text = "";
            txtEndDate.Text = "";
            chkIsMedicationFinished.Checked = false;            
            chkIsMedicationFinished.BackColor = Color.White;          
            cmbSpecificMedication.ClearSelection();
            cmbCountry.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeSpecificMedicationEntity = new DossierSacrifice_SpecificMedicationEntity()
            {
                DossierSacrifice_SpecificMedicationId = new Guid(ViewState["DossierSacrifice_SpecificMedicationId"].ToString())
            };
            var mydossierSacrificeSpecificMedication = _dossierSacrificeSpecificMedicationBL.GetSingleById(dossierSacrificeSpecificMedicationEntity);
            txtDescription.Text = mydossierSacrificeSpecificMedication.Description;
            txtMedicationNo.Text = mydossierSacrificeSpecificMedication.MedicationNo.ToString();
            txtMedicationPrize.Text = mydossierSacrificeSpecificMedication.MedicationPrize.ToString();
            txtMedicationConsum.Text = mydossierSacrificeSpecificMedication.MedicationConsum;
            txtStartDate.Text = mydossierSacrificeSpecificMedication.StartDate;
            txtEndDate.Text = mydossierSacrificeSpecificMedication.EndDate;
            chkIsMedicationFinished.Checked = mydossierSacrificeSpecificMedication.IsMedicationFinished;            
            cmbSpecificMedication.SelectedValue = mydossierSacrificeSpecificMedication.SpecificMedicationId.ToString();
            if (cmbSpecificMedication.FindItemByValue(mydossierSacrificeSpecificMedication.SpecificMedicationId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع اجرایی انتخاب شده معتبر نمی باشد.");
            }
            cmbCountry.SelectedValue = mydossierSacrificeSpecificMedication.CountryId.ToString();
            if (cmbCountry.FindItemByValue(mydossierSacrificeSpecificMedication.CountryId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع وابستگی انتخاب شده معتبر نمی باشد.");
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_SpecificMedication,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {


            cmbSpecificMedication.Items.Clear();
            cmbSpecificMedication.Items.Add(new RadComboBoxItem(""));
            cmbSpecificMedication.DataSource = _SpecificMedicationBL.GetAllIsActive();
            cmbSpecificMedication.DataTextField = "SpecificMedicationPersianTitle";
            cmbSpecificMedication.DataValueField = "SpecificMedicationId";
            cmbSpecificMedication.DataBind();


            cmbCountry.Items.Clear();
            cmbCountry.Items.Add(new RadComboBoxItem(""));
            cmbCountry.DataSource = _CountryBL.GetAllIsActive();
            cmbCountry.DataTextField = "CountryTitlePersian";
            cmbCountry.DataValueField = "CountryId";
            cmbCountry.DataBind();
        }


        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_SpecificMedicationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                var dossierSacrificeSpecificMedicationEntity = new DossierSacrifice_SpecificMedicationEntity()
                {
                    MedicationNo = (txtMedicationNo.Text.Trim()=="")?(int?)null:Int32.Parse(txtMedicationNo.Text),
                    MedicationConsum = txtMedicationConsum.Text,
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),                    
                    CountryId = (cmbCountry.SelectedIndex>0?Guid.Parse(cmbCountry.SelectedValue):new Guid()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    IsMedicationFinished = bool.Parse(chkIsMedicationFinished.Checked.ToString()),                  
                    SpecificMedicationId = Guid.Parse(cmbSpecificMedication.SelectedValue),                    
                    MedicationPrize = (txtMedicationPrize.Text.Trim()=="")? (decimal?)null:decimal.Parse(txtMedicationPrize.Text)               
                };

                _dossierSacrificeSpecificMedicationBL.Add(dossierSacrificeSpecificMedicationEntity, out DossierSacrifice_SpecificMedicationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, DossierSacrifice_SpecificMedicationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                              (txtStartDate.Text.Trim()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtMedicationPrize.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MedicationPrize Like N'%" +
                                               FarsiToArabic.ToArabic(txtMedicationPrize.Text.Trim()) + "%'";
                if (txtMedicationConsum.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MedicationConsum Like N'%" +
                                               FarsiToArabic.ToArabic(txtMedicationConsum.Text.Trim()) + "%'";


                if (txtMedicationNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MedicationNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtMedicationNo.Text.Trim()) + "%'";
                if (cmbSpecificMedication.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SpecificMedicationId ='" +
                                                Guid.Parse(cmbSpecificMedication.SelectedValue) + "'";
                if (cmbCountry.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CountryId ='" +
                                                Guid.Parse(cmbCountry.SelectedValue) + "'";


                grdDossierSacrifice_SpecificMedication.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_SpecificMedication.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeSpecificMedicationEntity = new DossierSacrifice_SpecificMedicationEntity()
                {
                    DossierSacrifice_SpecificMedicationId = Guid.Parse(ViewState["DossierSacrifice_SpecificMedicationId"].ToString()),
                    MedicationNo = (txtMedicationNo.Text.Trim()=="")?(int?)null:Int32.Parse(txtMedicationNo.Text),
                    MedicationConsum = txtMedicationConsum.Text,
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    CountryId = (cmbCountry.SelectedIndex > 0 ? Guid.Parse(cmbCountry.SelectedValue) : new Guid()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    IsMedicationFinished = bool.Parse(chkIsMedicationFinished.Checked.ToString()),                  
                    SpecificMedicationId = Guid.Parse(cmbSpecificMedication.SelectedValue),                    
                    MedicationPrize = (txtMedicationPrize.Text.Trim()=="")? (decimal?)null:decimal.Parse(txtMedicationPrize.Text)   

                };
                _dossierSacrificeSpecificMedicationBL.Update(dossierSacrificeSpecificMedicationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_SpecificMedicationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_SpecificMedication_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_SpecificMedicationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeSpecificMedicationEntity = new DossierSacrifice_SpecificMedicationEntity()
                    {
                        DossierSacrifice_SpecificMedicationId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeSpecificMedicationBL.Delete(dossierSacrificeSpecificMedicationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_SpecificMedication_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_SpecificMedication_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_SpecificMedication_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_SpecificMedication.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
      
        #endregion
    }
}