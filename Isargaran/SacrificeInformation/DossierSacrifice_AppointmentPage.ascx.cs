﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/16>
    /// Description: < اطلاعات ملاقات>
    /// </summary>
    public partial class DossierSacrifice_AppointmentPage : ItcBaseControl
    {

        #region PublicParam:
        private readonly DossierSacrifice_AppointmentBL _dossierSacrificeAppointmentBL = new DossierSacrifice_AppointmentBL();
        private readonly AppointmentTypeBL _appointmentTypeBL = new AppointmentTypeBL();
        private readonly DependentTypeBL _dependentTypeBL = new DependentTypeBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_Appointment]";
        private const string PrimaryKey = "DossierSacrifice_AppointmentId";
        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtAppointmentPerson.Text = "";
            txtDescription.Text = "";
            txtActionDescription.Text = "";
            txtRequestContent.Text = "";
            txtDescription.Text = "";
            txtAppointmentDate.Text = "";
     
            cmbAppointmentType.ClearSelection();
            cmbDependentType.ClearSelection();


            chkHasAction.Checked = false;
            chkHasAction.BackColor = Color.White;
            txtActionDescription.Enabled = false;
            txtActionDescription.BackColor = Color.Silver;
            txtActionDescription.Text = "";

            
            //chkHasRequest.Checked = false;
            //chkHasRequest.BackColor = Color.White;
            //txtRequestContent.Enabled = false;
            //txtRequestContent.BackColor = Color.Silver;
            txtRequestContent.Text = "";


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeAppointmentEntity = new DossierSacrifice_AppointmentEntity()
            {
                DossierSacrifice_AppointmentId = new Guid(ViewState["DossierSacrifice_AppointmentId"].ToString())
            };
            var mydossierSacrificeAppointment = _dossierSacrificeAppointmentBL.GetSingleById(dossierSacrificeAppointmentEntity);
            txtDescription.Text = mydossierSacrificeAppointment.Description;
            txtActionDescription.Text = mydossierSacrificeAppointment.ActionDescription;
            txtRequestContent.Text = mydossierSacrificeAppointment.RequestContent;
            txtAppointmentPerson.Text = mydossierSacrificeAppointment.AppointmentPerson;
            txtAppointmentDate.Text = mydossierSacrificeAppointment.AppointmentDate;        
            
            chkHasAction.Checked = mydossierSacrificeAppointment.HasAction;
            chkHasAction_CheckedChanged(chkHasAction, new EventArgs());
            //chkHasRequest.Checked = mydossierSacrificeAppointment.HasRequest;
            //chkHasRequest_CheckedChanged(chkHasRequest, new EventArgs());
            //chkHasRequest.Checked = mydossierSacrificeAppointment.HasRequest;
            cmbAppointmentType.SelectedValue = mydossierSacrificeAppointment.AppointmentTypeId.ToString();
            if (cmbAppointmentType.FindItemByValue(mydossierSacrificeAppointment.AppointmentTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع اجرایی انتخاب شده معتبر نمی باشد.");
            }
            cmbDependentType.SelectedValue = mydossierSacrificeAppointment.DependentTypeId.ToString();
            if (cmbDependentType.FindItemByValue(mydossierSacrificeAppointment.DependentTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع وابستگی انتخاب شده معتبر نمی باشد.");
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Appointment,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {


            cmbAppointmentType.Items.Clear();
            cmbAppointmentType.Items.Add(new RadComboBoxItem(""));
            cmbAppointmentType.DataSource = _appointmentTypeBL.GetAllIsActive();
            cmbAppointmentType.DataTextField = "AppointmentTypeTitle";
            cmbAppointmentType.DataValueField = "AppointmentTypeId";
            cmbAppointmentType.DataBind();


            cmbDependentType.Items.Clear();
            cmbDependentType.Items.Add(new RadComboBoxItem(""));
            cmbDependentType.DataSource = _dependentTypeBL.GetAllIsActive();
            cmbDependentType.DataTextField = "DependentTypeTitle";
            cmbDependentType.DataValueField = "DependentTypeId";
            cmbDependentType.DataBind();
        }


        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_AppointmentId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                var dossierSacrificeAppointmentEntity = new DossierSacrifice_AppointmentEntity()
                {
                    ActionDescription = txtActionDescription.Text,
                    AppointmentPerson = txtAppointmentPerson.Text,
                    AppointmentDate = ItcToDate.ShamsiToMiladi(txtAppointmentDate.Text),
                    DependentTypeId = (cmbDependentType.SelectedIndex>0?Guid.Parse(cmbDependentType.SelectedValue):new Guid()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    HasAction = bool.Parse(chkHasAction.Checked.ToString()),                    
                    AppointmentTypeId = Guid.Parse(cmbAppointmentType.SelectedValue),
                    RequestContent = txtRequestContent.Text,
                    HasRequest = true

                };

                _dossierSacrificeAppointmentBL.Add(dossierSacrificeAppointmentEntity, out DossierSacrifice_AppointmentId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, DossierSacrifice_AppointmentId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                if (txtAppointmentDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and AppointmentDate ='" +
                                              (txtAppointmentDate.Text.Trim()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtRequestContent.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and RequestContent Like N'%" +
                                               FarsiToArabic.ToArabic(txtRequestContent.Text.Trim()) + "%'";
                if (txtAppointmentPerson.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and AppointmentPerson Like N'%" +
                                               FarsiToArabic.ToArabic(txtAppointmentPerson.Text.Trim()) + "%'";
     

                if (txtActionDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ActionDescription Like N'%" +
                                               FarsiToArabic.ToArabic(txtActionDescription.Text.Trim()) + "%'";
                if (cmbAppointmentType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and AppointmentTypeId ='" +
                                                Guid.Parse(cmbAppointmentType.SelectedValue) + "'";
                if (cmbDependentType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DependentTypeId ='" +
                                                Guid.Parse(cmbDependentType.SelectedValue) + "'";


                grdDossierSacrifice_Appointment.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Appointment.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeAppointmentEntity = new DossierSacrifice_AppointmentEntity()
                {
                    DossierSacrifice_AppointmentId = Guid.Parse(ViewState["DossierSacrifice_AppointmentId"].ToString()),
                    ActionDescription = txtActionDescription.Text,
                    AppointmentPerson = txtAppointmentPerson.Text,
                    AppointmentDate = ItcToDate.ShamsiToMiladi(txtAppointmentDate.Text),
                    DependentTypeId = (cmbDependentType.SelectedIndex > 0 ? Guid.Parse(cmbDependentType.SelectedValue) : new Guid()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    HasAction = bool.Parse(chkHasAction.Checked.ToString()),
                    HasRequest =true,
                    AppointmentTypeId = Guid.Parse(cmbAppointmentType.SelectedValue),
                    RequestContent = txtRequestContent.Text

                };
                _dossierSacrificeAppointmentBL.Update(dossierSacrificeAppointmentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_AppointmentId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_Appointment_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_AppointmentId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeAppointmentEntity = new DossierSacrifice_AppointmentEntity()
                    {
                        DossierSacrifice_AppointmentId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeAppointmentBL.Delete(dossierSacrificeAppointmentEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_Appointment_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_Appointment_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_Appointment_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Appointment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void chkHasRequest_CheckedChanged(object sender, EventArgs e)
        {
            //if (chkHasRequest.Checked)
            //{
            //    txtRequestContent.Enabled = true;
            //    txtRequestContent.BackColor = Color.White;
            //}
            //else
            //{
            //    txtRequestContent.Enabled = false;
            //    txtRequestContent.BackColor = Color.Silver;
            //    txtRequestContent.Text = "";
            //}
        }

        protected void chkHasAction_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHasAction.Checked)
            {
                txtActionDescription.Enabled = true;
                txtActionDescription.BackColor = Color.White;
            }
            else
            {
                txtActionDescription.Enabled = false;
                txtActionDescription.BackColor = Color.Silver;
                txtActionDescription.Text = "";
            }
        }
        #endregion
    }
}