﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/07>
    /// Description: <پرونده مشخصات سوابق کاری>
    /// </summary>   
    public partial class DossierSacrifice_JobExperiencePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly CityBL _cityBl = new CityBL();
        private readonly ProvinceBL _provinceBl = new ProvinceBL();
        private readonly PostBL _postBL = new PostBL();
        private readonly DurationTimeAtWorkBL _durationTimeAtWorkBL = new DurationTimeAtWorkBL();
        private readonly EmploymentTypeBL _employmentTypeBL = new EmploymentTypeBL();
        private readonly DossierSacrifice_JobExperienceBL _dossierSacrificeJobExperienceBL = new DossierSacrifice_JobExperienceBL();
        private const string TableName = "Isar.V_DossierSacrifice_JobExperience";
        private const string PrimaryKey = "DossierSacrifice_JobExperienceId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEndDate.Text = "";
            txtAddress.Text = "";
            txtCommandmentNo.Text = "";
            txtDirectManager.Text = "";
            txtEmail.Text = "";
            txtEmploymentDate.Text = "";
            txtStartDate.Text = "";
            txtFaxNo.Text = "";
            txtFreeJob.Text = "";
            txtPersonnelNo.Text = "";
            cmbPost.ClearSelection();
            cmbCity.ClearSelection();
            cmbCity.Items.Clear();
            cmbprovince.ClearSelection();
            cmbEmploymentType.ClearSelection();
            cmbDurationTimeAtWorkId.ClearSelection();
            SelectControlOrganizationPhysicalChart.KeyId = "";
            SelectControlOrganizationPhysicalChart.Title = "";

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeJobExperienceEntity = new DossierSacrifice_JobExperienceEntity()
            {
                DossierSacrifice_JobExperienceId = new Guid(ViewState["DossierSacrifice_JobExperienceId"].ToString())
            };
            var mydossierSacrificeJobExperience = _dossierSacrificeJobExperienceBL.GetSingleById(dossierSacrificeJobExperienceEntity);
            txtEndDate.Text = mydossierSacrificeJobExperience.EndDate;
            txtAddress.Text = mydossierSacrificeJobExperience.Address;
            txtCommandmentNo.Text = mydossierSacrificeJobExperience.CommandmentNo;
            txtDirectManager.Text = mydossierSacrificeJobExperience.DirectManager;
            txtEmail.Text = mydossierSacrificeJobExperience.Email;
            txtEmploymentDate.Text = mydossierSacrificeJobExperience.EmploymentDate;
            txtFaxNo.Text = mydossierSacrificeJobExperience.FaxNo;
            txtFreeJob.Text = mydossierSacrificeJobExperience.FreeJob;
            txtPersonnelNo.Text = mydossierSacrificeJobExperience.PersonnelNo;
            txtPostalCod.Text = mydossierSacrificeJobExperience.PostalCod;
            txtStartDate.Text = mydossierSacrificeJobExperience.StartDate;
            txtTelNo.Text = mydossierSacrificeJobExperience.TelNo;
            txtWebAddress.Text = mydossierSacrificeJobExperience.WebAddress;
            cmbDurationTimeAtWorkId.SelectedValue = mydossierSacrificeJobExperience.DurationTimeAtWorkId.ToString();
            cmbEmploymentType.SelectedValue = mydossierSacrificeJobExperience.EmploymentTypeId.ToString();
            cmbPost.SelectedValue = mydossierSacrificeJobExperience.PostId.ToString();
            cmbDurationTimeAtWorkId.SelectedValue = mydossierSacrificeJobExperience.DurationTimeAtWorkId.ToString();
            if (cmbDurationTimeAtWorkId.FindItemByValue(mydossierSacrificeJobExperience.DurationTimeAtWorkId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد تعداد ساعت حضور انتخاب شده معتبر نمی باشد.");
            }
            cmbprovince.SelectedValue = mydossierSacrificeJobExperience.ProvinceId.ToString();
            if (cmbprovince.FindItemByValue(mydossierSacrificeJobExperience.ProvinceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
            }
            SetCmboboxCity(Guid.Parse(mydossierSacrificeJobExperience.ProvinceId.ToString()));
            cmbCity.SelectedValue = mydossierSacrificeJobExperience.CityId.ToString();
            if (cmbCity.FindItemByValue(mydossierSacrificeJobExperience.CityId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
            }
            cmbEmploymentType.SelectedValue = mydossierSacrificeJobExperience.EmploymentTypeId.ToString();
            if (cmbEmploymentType.FindItemByValue(mydossierSacrificeJobExperience.EmploymentTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع استخدام انتخاب شده معتبر نمی باشد.");
            }
            cmbPost.SelectedValue = mydossierSacrificeJobExperience.PostId.ToString();
            if (cmbPost.FindItemByValue(mydossierSacrificeJobExperience.PostId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردپست انتخاب شده معتبر نمی باشد.");
            }
            SelectControlOrganizationPhysicalChart.KeyId = mydossierSacrificeJobExperience.OrganizationPhysicalChartId.ToString();
            SelectControlOrganizationPhysicalChart.Title = mydossierSacrificeJobExperience.OrganizationPhysicalChartTitle;


        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_JobExperience,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbDurationTimeAtWorkId.Items.Clear();
            cmbDurationTimeAtWorkId.Items.Add(new RadComboBoxItem(""));
            cmbDurationTimeAtWorkId.DataSource = _durationTimeAtWorkBL.GetAllIsActive();
            cmbDurationTimeAtWorkId.DataTextField = "DurationTimeAtWorkTitle";
            cmbDurationTimeAtWorkId.DataValueField = "DurationTimeAtWorkId";
            cmbDurationTimeAtWorkId.DataBind();

            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBl.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();

            cmbPost.Items.Clear();
            cmbPost.Items.Add(new RadComboBoxItem(""));
            cmbPost.DataSource = _postBL.GetAllIsActive();
            cmbPost.DataTextField = "PostTitle";
            cmbPost.DataValueField = "PostId";
            cmbPost.DataBind();

            cmbEmploymentType.Items.Clear();
            cmbEmploymentType.Items.Add(new RadComboBoxItem(""));
            cmbEmploymentType.DataSource = _employmentTypeBL.GetAllIsActive();
            cmbEmploymentType.DataTextField = "EmploymentTypeTitle";
            cmbEmploymentType.DataValueField = "EmploymentTypeId";
            cmbEmploymentType.DataBind();




        }
        private void SetCmboboxCity(Guid ProvinceId)
        {
            if (ProvinceId != new Guid())
            {
                var cityEntity = new CityEntity { ProvinceId = ProvinceId };
                cmbCity.Items.Clear();
                cmbCity.Items.Add(new RadComboBoxItem(""));
                cmbCity.DataTextField = "CityTitlePersian";
                cmbCity.DataValueField = "CityId";
                cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
                cmbCity.DataBind();
            }

        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_JobExperienceId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeJobExperienceEntity = new DossierSacrifice_JobExperienceEntity()
                {                    
                    Address = txtAddress.Text,
                    CommandmentNo = txtCommandmentNo.Text,
                    DirectManager = txtDirectManager.Text,
                    Email = txtEmail.Text,
                    CityId = Guid.Parse(cmbCity.SelectedValue),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    FaxNo = txtFaxNo.Text,
                    FreeJob = txtFreeJob.Text,
                    TelNo = txtTelNo.Text,
                    PostalCod = txtPostalCod.Text,
                    WebAddress = txtWebAddress.Text,
                    PersonnelNo = txtPersonnelNo.Text,
                    EmploymentDate = ItcToDate.ShamsiToMiladi(txtEmploymentDate.Text),
                    EmploymentTypeId = (cmbEmploymentType.SelectedIndex > 0 ? Guid.Parse(cmbEmploymentType.SelectedValue) : new Guid()),
                    PostId = (cmbPost.SelectedIndex > 0 ? Guid.Parse(cmbPost.SelectedValue) : new Guid()),
                    OrganizationPhysicalChartId = (SelectControlOrganizationPhysicalChart.KeyId != "" ? Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId) : new Guid()),
                    DurationTimeAtWorkId = (cmbDurationTimeAtWorkId.SelectedIndex > 0 ? Guid.Parse(cmbDurationTimeAtWorkId.SelectedValue) : new Guid()),

                };

                _dossierSacrificeJobExperienceBL.Add(dossierSacrificeJobExperienceEntity, out DossierSacrifice_JobExperienceId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, DossierSacrifice_JobExperienceId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Address Like N'%" +
                                               FarsiToArabic.ToArabic(txtAddress.Text.Trim()) + "%'";
                if (txtCommandmentNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CommandmentNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtCommandmentNo.Text.Trim()) + "%'";
                if (txtDirectManager.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DirectManager ='" +
                                               Decimal.Parse(txtDirectManager.Text.Trim()) + "'";
                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                              (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                              (txtEndDate.Text.Trim()) + "'";
                if (txtEmploymentDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EmploymentDate ='" +
                                              (txtEmploymentDate.Text.Trim()) + "'";
                if (txtEmail.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Email Like N'%" +
                                               FarsiToArabic.ToArabic(txtEmail.Text.Trim()) + "%'";
                if (txtFaxNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FaxNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtFaxNo.Text.Trim()) + "%'";
                if (txtFreeJob.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FreeJob Like N'%" +
                                               FarsiToArabic.ToArabic(txtFreeJob.Text.Trim()) + "%'";
                if (txtPersonnelNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PersonnelNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtPersonnelNo.Text.Trim()) + "%'";
                if (txtPostalCod.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PostalCod Like N'%" +
                                               FarsiToArabic.ToArabic(txtPostalCod.Text.Trim()) + "%'";
                if (txtTelNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TelNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtTelNo.Text.Trim()) + "%'";
                if (txtWebAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WebAddress Like N'%" +
                                               FarsiToArabic.ToArabic(txtWebAddress.Text.Trim()) + "%'";
                if (txtPostalCod.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PostalCod Like N'%" +
                                               FarsiToArabic.ToArabic(txtPostalCod.Text.Trim()) + "%'";
                if (cmbDurationTimeAtWorkId.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DurationTimeAtWorkId ='" +
                                               new Guid(cmbDurationTimeAtWorkId.SelectedValue) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbEmploymentType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EmploymentTypeId ='" +
                                               new Guid(cmbEmploymentType.SelectedValue) + "'";
                if (cmbPost.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PostId ='" +
                                               new Guid(cmbPost.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and provinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                grdDossierSacrifice_JobExperience.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_JobExperience.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeJobExperienceEntity = new DossierSacrifice_JobExperienceEntity()
                {  
                    DossierSacrifice_JobExperienceId = Guid.Parse(ViewState["DossierSacrifice_JobExperienceId"].ToString()),
                    Address = txtAddress.Text,
                    CommandmentNo = txtCommandmentNo.Text,
                    DirectManager = txtDirectManager.Text,
                    Email = txtEmail.Text,
                    CityId = Guid.Parse(cmbCity.SelectedValue),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    FaxNo = txtFaxNo.Text,
                    FreeJob = txtFreeJob.Text,
                    TelNo = txtTelNo.Text,
                    PostalCod = txtPostalCod.Text,
                    WebAddress = txtWebAddress.Text,
                    PersonnelNo = txtPersonnelNo.Text,
                    EmploymentDate = ItcToDate.ShamsiToMiladi(txtEmploymentDate.Text),
                    EmploymentTypeId = (cmbEmploymentType.SelectedIndex > 0 ? Guid.Parse(cmbEmploymentType.SelectedValue) : new Guid()),
                    PostId = (cmbPost.SelectedIndex > 0 ? Guid.Parse(cmbPost.SelectedValue) : new Guid()),
                    OrganizationPhysicalChartId = (SelectControlOrganizationPhysicalChart.KeyId != "" ? Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId) : new Guid()),
                    DurationTimeAtWorkId = (cmbDurationTimeAtWorkId.SelectedIndex > 0 ? Guid.Parse(cmbDurationTimeAtWorkId.SelectedValue) : new Guid()),

                };

                _dossierSacrificeJobExperienceBL.Update(dossierSacrificeJobExperienceEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_JobExperienceId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_JobExperience_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_JobExperienceId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                    var dossierSacrificeJobExperienceEntity = new DossierSacrifice_JobExperienceEntity()
                    {
                        DossierSacrifice_JobExperienceId = Guid.Parse(e.CommandArgument.ToString())
                    };
                    _dossierSacrificeJobExperienceBL.Delete(dossierSacrificeJobExperienceEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_JobExperience_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_JobExperience_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_JobExperience_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_JobExperience.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                cmbCity.Text = "";
                cmbCity.Items.Clear();
                SetCmboboxCity(e.Value.ToString() == "" ? new Guid() : Guid.Parse(e.Value.ToString()));
            }
            else
            {
                cmbCity.Items.Clear();
                cmbCity.Text = "";
            }

        
        }

        #endregion
    }
}