﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<Narges.Kamran>
    // Create date: <1393/02/22>
    // Description:	<  اطلاعات خدمات و تسهیلات آموزشی>
    /// </summary>
    public partial class DossierSacrifice_FacilitiyEducationPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly EducationDegreeBL _educationDegreeBL = new EducationDegreeBL();
        private  readonly DossierSacrifice_DependentBL  _dossierSacrificeDependentBL=new DossierSacrifice_DependentBL();
        private readonly DossierSacrifice_FacilitiyEducationBL _dossierSacrificeFacilitiyEducationBL = new DossierSacrifice_FacilitiyEducationBL();
        private readonly EducationDegreeBL _educationDegreeBl = new EducationDegreeBL();
        private readonly EducationSystemBL _educationSystemBL = new EducationSystemBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_FacilitiyEducation]";
        private const string PrimaryKey = "DossierSacrifice_FacilitiyEducationId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtCashAmount.Text = "";
            txtEndDate.Text = "";
            txtStartDate.Text = "";
            cmbEucationDegree.ClearSelection();
            cmbDependentPerson.ClearSelection();
            cmbEducationSystem.ClearSelection();
            //SelectPerson.KeyId = "";
            //SelectPerson.Title = "";


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeFacilitiyEducationEntity = new DossierSacrifice_FacilitiyEducationEntity()
            {
                DossierSacrifice_FacilitiyEducationId = Guid.Parse(ViewState["DossierSacrifice_FacilitiyEducationId"].ToString())
            };
            var mydossierSacrificeFacilitiyEducation = _dossierSacrificeFacilitiyEducationBL.GetSingleById(dossierSacrificeFacilitiyEducationEntity);
            cmbEducationSystem.SelectedValue = mydossierSacrificeFacilitiyEducation.EducationSystemId.ToString();
            SetCmbEucationDegree(mydossierSacrificeFacilitiyEducation.EducationSystemId);
            cmbEucationDegree.SelectedValue = mydossierSacrificeFacilitiyEducation.EucationDegreeId.ToString();
            if (cmbEucationDegree.FindItemByValue(mydossierSacrificeFacilitiyEducation.EucationDegreeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردمقطع تحصیلی انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = mydossierSacrificeFacilitiyEducation.Description;
            txtCashAmount.Text = mydossierSacrificeFacilitiyEducation.CashAmount.ToString();
            txtStartDate.Text = mydossierSacrificeFacilitiyEducation.StartDate;
            txtEndDate.Text = mydossierSacrificeFacilitiyEducation.EndDate;
            //SelectPerson.KeyId = mydossierSacrificeFacilitiyEducation.PersonId.ToString();
            //SelectPerson.Title = mydossierSacrificeFacilitiyEducation.FullName;
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_FacilitiyEducation,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbEducationSystem.Items.Clear();
            cmbEducationSystem.Items.Add(new RadComboBoxItem(""));
            cmbEducationSystem.DataSource = _educationSystemBL.GetAllIsActive();
            cmbEducationSystem.DataTextField = "EducationSystemPersianTitle";
            cmbEducationSystem.DataValueField = "EducationSystemId";
            cmbEducationSystem.DataBind();


            DossierSacrifice_DependentEntity dossierSacrificeDependentEntity=new DossierSacrifice_DependentEntity()
            {
                DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString())
            };
            cmbDependentPerson.Items.Clear();
            cmbDependentPerson.Items.Add(new RadComboBoxItem(""));
            cmbDependentPerson.DataSource = _dossierSacrificeDependentBL.GetDossierSacrifice_DependentCollectionByDossierSacrifice(dossierSacrificeDependentEntity);
            cmbDependentPerson.DataTextField = "FullName";
            cmbDependentPerson.DataValueField = "PersonId";
            cmbDependentPerson.DataBind();

        }

        private void SetCmbEucationDegree(Guid? educationSystemId)
        {

            cmbEucationDegree.Items.Clear();
            cmbEucationDegree.Items.Add(new RadComboBoxItem(""));
            EducationDegreeEntity educationDegreeEntity = new EducationDegreeEntity()
            {
                EducationSystemId = educationSystemId
            };
            cmbEucationDegree.DataSource = _educationDegreeBl.GetEducationDegreeCollectionByEducationSystem(educationDegreeEntity);
            cmbEucationDegree.DataTextField = "EucationDegreePersianTitle";
            cmbEucationDegree.DataValueField = "EucationDegreeId";
            cmbEucationDegree.DataBind();
        }
        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_FacilityAndServiceCashId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeFacilitiyEducationEntity = new DossierSacrifice_FacilitiyEducationEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CashAmount = (txtCashAmount.Text == "" ? (decimal?)null : decimal.Parse(txtCashAmount.Text)),
                    PersonId   = (cmbDependentPerson.SelectedIndex>0?Guid.Parse(cmbDependentPerson.SelectedValue):new Guid()),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    EucationDegreeId = Guid.Parse(cmbEucationDegree.SelectedValue),                    
                };
                _dossierSacrificeFacilitiyEducationBL.Add(dossierSacrificeFacilitiyEducationEntity, out DossierSacrifice_FacilityAndServiceCashId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, DossierSacrifice_FacilityAndServiceCashId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtCashAmount.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CashAmount =' " +
                                               (txtCashAmount.Text.Trim()) + "'";

                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
     
                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                               (txtStartDate.Text.Trim()) + "'";

                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                               (txtEndDate.Text.Trim()) + "'";
                if (cmbEucationDegree.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And EucationDegreeId='" +
                                               (cmbEucationDegree.SelectedValue.Trim()) + "'";
     

                grdDossierSacrifice_FacilitiyEducation.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_FacilitiyEducation.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeFacilitiyEducationEntity = new DossierSacrifice_FacilitiyEducationEntity()
                {
                    DossierSacrifice_FacilitiyEducationId = Guid.Parse(ViewState["DossierSacrifice_FacilitiyEducationId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    CashAmount = (txtCashAmount.Text == "" ? (decimal?)null : decimal.Parse(txtCashAmount.Text)),
                    PersonId = (cmbDependentPerson.SelectedIndex > 0 ? Guid.Parse(cmbDependentPerson.SelectedValue) : new Guid()),
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    EucationDegreeId = Guid.Parse(cmbEucationDegree.SelectedValue),
                };
                _dossierSacrificeFacilitiyEducationBL.Update(dossierSacrificeFacilitiyEducationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_FacilitiyEducationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_FacilitiyEducation_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_FacilitiyEducationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeFacilitiyEducationEntity = new DossierSacrifice_FacilitiyEducationEntity()
                    {
                        DossierSacrifice_FacilitiyEducationId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeFacilitiyEducationBL.Delete(dossierSacrificeFacilitiyEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_FacilitiyEducation_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_FacilitiyEducation_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_FacilitiyEducation_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_FacilitiyEducation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion


                    protected void CmbEducationSystem_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {

                cmbEucationDegree.Items.Clear();
                cmbEucationDegree.Text = "";
                SetCmbEucationDegree(Guid.Parse(e.Value.ToString()));
            }
            else
            {
                cmbEucationDegree.Items.Clear();
                cmbEucationDegree.Text = "";
            }
        }
        
    }
}