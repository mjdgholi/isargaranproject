﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/09>
    /// Description: <پرونده اطلاعات تالیف و ترجمه ایثارگر>
    /// </summary>
    public partial class DossierSacrifice_TranslationOrCompilationPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly WritingActivityTypeBL _writingActivityTypeBL = new WritingActivityTypeBL();
        private readonly DossierSacrifice_TranslationOrCompilationBL _dossierSacrificeTranslationOrCompilationBL = new DossierSacrifice_TranslationOrCompilationBL();
        private const string TableName = "Isar.V_DossierSacrifice_TranslationOrCompilation";
        private const string PrimaryKey = "DossierSacrifice_TranslationOrCompilationId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtBaseBookTitle.Text = "";
            txtBookSubject.Text = "";
            txtFirstPublicationDate.Text = "";
            txtPublication.Text = "";
            txtTranslatedBookTitle.Text = "";
            txtTurnPublicationNo.Text = "";
            cmbWritingActivityType.ClearSelection();
            

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeTranslationOrCompilationEntity = new DossierSacrifice_TranslationOrCompilationEntity()
            {
                DossierSacrifice_TranslationOrCompilationId = Guid.Parse(ViewState["DossierSacrifice_TranslationOrCompilationId"].ToString())
            };
            var mydossierSacrificeTranslationOrCompilation = _dossierSacrificeTranslationOrCompilationBL.GetSingleById(dossierSacrificeTranslationOrCompilationEntity);
            cmbWritingActivityType.SelectedValue = mydossierSacrificeTranslationOrCompilation.WritingActivityTypeId.ToString();
            if (cmbWritingActivityType.FindItemByValue(mydossierSacrificeTranslationOrCompilation.WritingActivityTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع فعالیت نویسندگی انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = mydossierSacrificeTranslationOrCompilation.Description;
            txtBaseBookTitle.Text = mydossierSacrificeTranslationOrCompilation.BaseBookTitle;
            txtBookSubject.Text = mydossierSacrificeTranslationOrCompilation.BookSubject;
            txtFirstPublicationDate.Text = mydossierSacrificeTranslationOrCompilation.FirstPublicationDate;
            txtPublication.Text = mydossierSacrificeTranslationOrCompilation.Publication;
            txtTranslatedBookTitle.Text = mydossierSacrificeTranslationOrCompilation.TranslatedBookTitle;
            txtTurnPublicationNo.Text = mydossierSacrificeTranslationOrCompilation.TurnPublicationNo.ToString();            

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_TranslationOrCompilation,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbWritingActivityType.Items.Clear();
            cmbWritingActivityType.Items.Add(new RadComboBoxItem(""));
            cmbWritingActivityType.DataSource = _writingActivityTypeBL.GetAllIsActive();
            cmbWritingActivityType.DataTextField = "WritingActivityTypeTitle";
            cmbWritingActivityType.DataValueField = "WritingActivityTypeId";
            cmbWritingActivityType.DataBind();



        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeTranslationOrCompilationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeTranslationOrCompilationEntity = new DossierSacrifice_TranslationOrCompilationEntity()
                {
                    BaseBookTitle = txtBaseBookTitle.Text,
                    BookSubject = txtBookSubject.Text,
                    Description = txtDescription.Text,
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    FirstPublicationDate = ItcToDate.ShamsiToMiladi(txtFirstPublicationDate.Text),
                    TurnPublicationNo = (txtTurnPublicationNo.Text==""?(int?)null:int.Parse(txtTurnPublicationNo.Text)),
                    WritingActivityTypeId = Guid.Parse(cmbWritingActivityType.SelectedValue),
                    Publication = txtPublication.Text,
                    TranslatedBookTitle = txtTranslatedBookTitle.Text
                };
                _dossierSacrificeTranslationOrCompilationBL.Add(dossierSacrificeTranslationOrCompilationEntity, out dossierSacrificeTranslationOrCompilationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, dossierSacrificeTranslationOrCompilationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtTurnPublicationNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TurnPublicationNo =' " +
                                               (txtTurnPublicationNo.Text.Trim()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtBaseBookTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BaseBookTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtBaseBookTitle.Text.Trim()) + "%'";
                if (txtBookSubject.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BookSubject Like N'%" +
                                               FarsiToArabic.ToArabic(txtBookSubject.Text.Trim()) + "%'";
                if (txtPublication.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Publication Like N'%" +
                                               FarsiToArabic.ToArabic(txtPublication.Text.Trim()) + "%'";
                if (txtTranslatedBookTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TranslatedBookTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtTranslatedBookTitle.Text.Trim()) + "%'";
                if (txtFirstPublicationDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FirstPublicationDate ='" +
                                               (txtFirstPublicationDate.Text.Trim()) + "'";
              
                if (cmbWritingActivityType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And WritingActivityTypeId='" +
                                               (cmbWritingActivityType.SelectedValue.Trim()) + "'";
                grdDossierSacrifice_TranslationOrCompilation.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_TranslationOrCompilation.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeTranslationOrCompilationEntity = new DossierSacrifice_TranslationOrCompilationEntity()
                {
                    DossierSacrifice_TranslationOrCompilationId = Guid.Parse(ViewState["DossierSacrifice_TranslationOrCompilationId"].ToString()),
                    BaseBookTitle = txtBaseBookTitle.Text,
                    BookSubject = txtBookSubject.Text,
                    Description = txtDescription.Text,
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    FirstPublicationDate = ItcToDate.ShamsiToMiladi(txtFirstPublicationDate.Text),
                    TurnPublicationNo = (txtTurnPublicationNo.Text == "" ? (int?)null : int.Parse(txtTurnPublicationNo.Text)),
                    WritingActivityTypeId = Guid.Parse(cmbWritingActivityType.SelectedValue),
                    Publication = txtPublication.Text,
                    TranslatedBookTitle = txtTranslatedBookTitle.Text
                };
                _dossierSacrificeTranslationOrCompilationBL.Update(dossierSacrificeTranslationOrCompilationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_TranslationOrCompilationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_TranslationOrCompilation_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_TranslationOrCompilationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeTranslationOrCompilationEntity = new DossierSacrifice_TranslationOrCompilationEntity()
                    {
                        DossierSacrifice_TranslationOrCompilationId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeTranslationOrCompilationBL.Delete(dossierSacrificeTranslationOrCompilationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdDossierSacrifice_TranslationOrCompilation_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_TranslationOrCompilation_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>



        protected void grdDossierSacrifice_TranslationOrCompilation_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TranslationOrCompilation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}