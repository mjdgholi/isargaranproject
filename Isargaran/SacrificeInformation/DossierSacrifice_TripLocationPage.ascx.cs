﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/20>
    /// Description: <پرونده اطلاعات اردو یا مسافرت ایثارگر>
    /// </summary>
    public partial class DossierSacrifice_TripLocationPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly VehicleTypeBL _vehicleTypeBL = new VehicleTypeBL();
        private readonly OccasionBL _occasionBL = new OccasionBL();
        private readonly TripLocationBL _tripLocationBL = new TripLocationBL();
        private readonly DossierSacrifice_TripLocationBL _dossierSacrificeTripLocationBL = new DossierSacrifice_TripLocationBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_TripLocation]";
        private const string PrimaryKey = "DossierSacrifice_TravelLocationId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtAlongPersonName.Text = "";
            txtFinancialCost.Text = "";
            txtHotelName.Text = "";
            txtTravelerNo.Text = "";
            txtTripStartDate.Text = "";
            txtTripStartDate.Text = "";
            txtlblTripEndDate.Text = "";
            cmbOccasion.ClearSelection();
            cmbTripLocation.ClearSelection();
            cmbVehicleType.ClearSelection();


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeTripLocationEntity = new DossierSacrifice_TripLocationEntity()
            {
                DossierSacrifice_TravelLocationId = Guid.Parse(ViewState["DossierSacrifice_TravelLocationId"].ToString())
            };
            var mydossierSacrificeTripLocation = _dossierSacrificeTripLocationBL.GetSingleById(dossierSacrificeTripLocationEntity);
            cmbOccasion.SelectedValue = mydossierSacrificeTripLocation.OccasionId.ToString();
            if (cmbOccasion.FindItemByValue(mydossierSacrificeTripLocation.OccasionId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردعنوان مناسبت انتخاب شده معتبر نمی باشد.");
            }
            cmbTripLocation.SelectedValue = mydossierSacrificeTripLocation.TripLocationId.ToString();
            if (cmbTripLocation.FindItemByValue(mydossierSacrificeTripLocation.TripLocationId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردعنوان مکان مسافرت انتخاب شده معتبر نمی باشد.");
            }
            cmbVehicleType.SelectedValue = mydossierSacrificeTripLocation.VehicleType.ToString();
            if (cmbVehicleType.FindItemByValue(mydossierSacrificeTripLocation.VehicleType.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردعنوان نوع وسیله حمل ونقل انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = mydossierSacrificeTripLocation.Description;
            txtAlongPersonName.Text = mydossierSacrificeTripLocation.AlongPersonName;
            txtFinancialCost.Text = mydossierSacrificeTripLocation.FinancialCost.ToString();
            txtHotelName.Text = mydossierSacrificeTripLocation.HotelName;
            txtTravelerNo.Text = mydossierSacrificeTripLocation.TravelerNo.ToString();
            txtTripStartDate.Text = mydossierSacrificeTripLocation.TripStartDate;
            txtlblTripEndDate.Text = mydossierSacrificeTripLocation.TripEndDate;



        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_TripLocation,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbOccasion.Items.Clear();
            cmbOccasion.Items.Add(new RadComboBoxItem(""));
            cmbOccasion.DataSource = _occasionBL.GetAllIsActive();
            cmbOccasion.DataTextField = "OccasionTitle";
            cmbOccasion.DataValueField = "OccasionId";
            cmbOccasion.DataBind();


            cmbTripLocation.Items.Clear();
            cmbTripLocation.Items.Add(new RadComboBoxItem(""));
            cmbTripLocation.DataSource = _tripLocationBL.GetAllIsActive();
            cmbTripLocation.DataTextField = "TripLocationTitle";
            cmbTripLocation.DataValueField = "TripLocationId";
            cmbTripLocation.DataBind();


            cmbVehicleType.Items.Clear();
            cmbVehicleType.Items.Add(new RadComboBoxItem(""));
            cmbVehicleType.DataSource = _vehicleTypeBL.GetAllIsActive();
            cmbVehicleType.DataTextField = "VehicleTypeTitle";
            cmbVehicleType.DataValueField = "VehicleTypeId";
            cmbVehicleType.DataBind();

        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeTravelLocationId ;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeTripLocationEntity = new DossierSacrifice_TripLocationEntity()
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    AlongPersonName = txtAlongPersonName.Text,
                    Description = txtDescription.Text,
                    FinancialCost = (txtFinancialCost.Text==""?(decimal?)null:decimal.Parse(txtFinancialCost.Text)),
                    HotelName = txtHotelName.Text,
                    OccasionId =(cmbOccasion.SelectedIndex>0?Guid.Parse(cmbOccasion.SelectedValue):(Guid?)null),
                    VehicleType = (cmbVehicleType.SelectedIndex > 0 ? Guid.Parse(cmbVehicleType.SelectedValue) : (Guid?)null),
                    TripLocationId = Guid.Parse(cmbTripLocation.SelectedValue),
                    TravelerNo = (txtTravelerNo.Text==""?(int?)null:int.Parse(txtTravelerNo.Text)),
                    TripEndDate = ItcToDate.ShamsiToMiladi(txtlblTripEndDate.Text),
                    TripStartDate = ItcToDate.ShamsiToMiladi(txtTripStartDate.Text),                    
                    
                };
                _dossierSacrificeTripLocationBL.Add(dossierSacrificeTripLocationEntity, out dossierSacrificeTravelLocationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, dossierSacrificeTravelLocationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtTravelerNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TravelerNo =' " +
                                               (txtTravelerNo.Text.Trim()) + "'";
                if (txtFinancialCost.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FinancialCost =' " +
                                               (txtFinancialCost.Text.Trim()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtAlongPersonName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and AlongPersonName Like N'%" +
                                               FarsiToArabic.ToArabic(txtAlongPersonName.Text.Trim()) + "%'";
                if (txtHotelName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HotelName Like N'%" +
                                               FarsiToArabic.ToArabic(txtHotelName.Text.Trim()) + "%'";
                if (txtTripStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TripStartDate ='" +
                                               (txtTripStartDate.Text.Trim()) + "'";
                if (txtlblTripEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TripEndDate ='" +
                                               (txtlblTripEndDate.Text.Trim()) + "'";


                if (cmbOccasion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And OccasionId='" +
                                               (cmbOccasion.SelectedValue.Trim()) + "'";
                if (cmbTripLocation.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And TripLocationId='" +
                                               (cmbTripLocation.SelectedValue.Trim()) + "'";
                if (cmbVehicleType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And VehicleType='" +
                                               (cmbVehicleType.SelectedValue.Trim()) + "'";
                grdDossierSacrifice_TripLocation.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_TripLocation.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeTripLocationEntity = new DossierSacrifice_TripLocationEntity()
                {
                    DossierSacrifice_TravelLocationId = Guid.Parse(ViewState["DossierSacrifice_TravelLocationId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    AlongPersonName = txtAlongPersonName.Text,
                    Description = txtDescription.Text,
                    FinancialCost = (txtFinancialCost.Text == "" ? (decimal?)null : decimal.Parse(txtFinancialCost.Text)),
                    HotelName = txtHotelName.Text,
                    OccasionId = (cmbOccasion.SelectedIndex > 0 ? Guid.Parse(cmbOccasion.SelectedValue) : (Guid?)null),
                    VehicleType = (cmbVehicleType.SelectedIndex > 0 ? Guid.Parse(cmbVehicleType.SelectedValue) : (Guid?)null),
                    TripLocationId = Guid.Parse(cmbTripLocation.SelectedValue),
                    TravelerNo = (txtTravelerNo.Text == "" ? (int?)null : int.Parse(txtTravelerNo.Text)),
                    TripEndDate = ItcToDate.ShamsiToMiladi(txtlblTripEndDate.Text),
                    TripStartDate = ItcToDate.ShamsiToMiladi(txtTripStartDate.Text),

                };
                _dossierSacrificeTripLocationBL.Update(dossierSacrificeTripLocationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_TravelLocationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_TripLocation_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_TravelLocationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeTripLocationEntity = new DossierSacrifice_TripLocationEntity()
                    {
                        DossierSacrifice_TravelLocationId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeTripLocationBL.Delete(dossierSacrificeTripLocationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_TripLocation_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_TripLocation_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDossierSacrifice_TripLocation_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_TripLocation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}