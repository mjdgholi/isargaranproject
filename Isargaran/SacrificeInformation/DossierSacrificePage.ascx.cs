﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Common;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Intranet.Security;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using ITC.Library.Controls;
using Telerik.Web.UI;




namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <پرونده ایثارگری>
    /// </summary>
    public partial class DossierSacrificePage : ItcBaseControl
    {
        #region PublicParam:

        private readonly ProvinceBL _provinceBL = new ProvinceBL();
        private readonly CountryBL _countryBL = new CountryBL();
        private readonly DossierSacrificeBL _dossierSacrificeBl = new DossierSacrificeBL();
        private readonly CityBL _cityBl=new CityBL();
        private readonly NationalityBL _nationalityBl = new NationalityBL();
        private readonly FaithBL _faithBl = new FaithBL();
        private readonly ReligionBL _religionBl = new ReligionBL();
        private readonly DependentTypeBL _dependentTypeBl = new DependentTypeBL();
        private readonly DossierStatusBL _dossierStatusBl = new DossierStatusBL();
        private readonly MarriageStatusBL _marriageStatusBl = new MarriageStatusBL();
        private readonly WebFormCategorizeBL _webFormCategorizeBL= new WebFormCategorizeBL();
        private readonly WebFormBL _webFormBL =new WebFormBL();
        private readonly AccessSystemGeneralBL _accessSystemGeneralBl = new AccessSystemGeneralBL();


        private const string TableName = "Isar.v_DossierSacrifice";
        private const string PrimaryKey = "DossierSacrificeId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            PnlOtherInformation.Visible = false;
            grdDossierSacrifice.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
            PnlMaster.Visible = true;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            PnlOtherInformation.Visible = false;
            grdDossierSacrifice.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtCellphoneNo.Text = "";
            txtDossierSacrificeNo.Text = "";
            txtEmail.Text = "";
            txtHomeAddress.Text = "";
            txtHomeFaxNo.Text = "";
            txtHomePhoneNo.Text = "";
            txtInsuranceNo.Text = "";
            txtMarriageDate.Text = "";
            txtPassportNo.Text = "";
            txtSacrificeNo.Text = "";
            txtZipCode.Text = "";
            cmbCity.ClearSelection();
            cmbCity.Items.Clear();
            cmbDependentType.ClearSelection();
            cmbFaith.ClearSelection();
            cmbReligion.ClearSelection();
            cmbprovince.ClearSelection();
            CmbDossierStatus.ClearSelection();
            CmbMarriageStatus.ClearSelection();
            CmbNationality.ClearSelection();
            CmbNaturalization.ClearSelection();
            chkHasDependentSacrifice.Checked = true;
            cmbDependentType.Enabled = true;
            cmbDependentType.BackColor = Color.White;
            chkHasInsurance.Checked = true;
            txtInsuranceNo.Enabled = true;
            txtInsuranceNo.BackColor = Color.White;
            chkHasPassport.Checked = true;
            txtPassportNo.Enabled = true;
            txtPassportNo.BackColor = Color.White;
            chkHasDossierSacrifice.Checked = true;
            SelectPerson.KeyId = "";
            SelectPerson.Title = "";
            cmbIsActive.SelectedIndex = 1;
            FileUpload1.KeyId = "";
            FileUpload1.Title = "";
            chkHasInsurance.Checked = false;
            txtInsuranceNo.Enabled = false;
            txtInsuranceNo.BackColor = Color.Silver;
            txtInsuranceNo.Text = "";

            chkHasPassport.Checked = false;
            txtPassportNo.Enabled = false;
            txtPassportNo.BackColor = Color.Silver;
            txtPassportNo.Text = "";

            chkHasDependentSacrifice.Checked = false;
            cmbDependentType.Enabled = false;
            cmbDependentType.BackColor = Color.Silver;
            cmbDependentType.ClearSelection();
            if (CmbNationality.FindItemByValue("5acc823f-5947-4f31-8b2d-df5ce6719ca0")!=null )
            {
                CmbNationality.SelectedValue = "5acc823f-5947-4f31-8b2d-df5ce6719ca0";
            }
            if (CmbNaturalization.FindItemByValue("eb36db7a-ad41-4ee8-b87a-b242434b97cd") != null)
            {
                CmbNaturalization.SelectedValue = "eb36db7a-ad41-4ee8-b87a-b242434b97cd";
            }
            if (cmbReligion.FindItemByValue("8d058b4c-91ee-45dc-91fe-6da38c3c3b6d") != null)
            {
                cmbReligion.SelectedValue = "8d058b4c-91ee-45dc-91fe-6da38c3c3b6d";
            }
            if (cmbFaith.FindItemByValue("9652c2fd-1132-482b-9b0d-2cfa6123bb53") != null)
            {
                cmbFaith.SelectedValue = "9652c2fd-1132-482b-9b0d-2cfa6123bb53";
            }
            if (CmbDossierStatus.FindItemByValue("43fad04f-031f-43d5-9b42-01ed94bd1cf8") != null)
            {
                CmbDossierStatus.SelectedValue = "43fad04f-031f-43d5-9b42-01ed94bd1cf8";
            }
            chkHasDependentSacrifice.Checked = false;


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeEntity = new DossierSacrificeEntity()
            {
                DossierSacrificeId =
                    Guid.Parse(ViewState["DossierSacrificeId"].ToString())
            };
            var myDossierSacrifice = _dossierSacrificeBl.GetSingleById(dossierSacrificeEntity);
            SelectPerson.KeyId = myDossierSacrifice.PersonId.ToString();
            SelectPerson.Title = myDossierSacrifice.PersonFirstName + " " + myDossierSacrifice.PersonLastName;
            txtCellphoneNo.Text = myDossierSacrifice.CellphoneNo;
            txtDossierSacrificeNo.Text = myDossierSacrifice.DossierSacrificeNo;
            txtEmail.Text = myDossierSacrifice.Email;
            txtHomeAddress.Text = myDossierSacrifice.HomeAddress;
            txtHomeFaxNo.Text = myDossierSacrifice.HomeFaxNo;
            txtHomePhoneNo.Text = myDossierSacrifice.HomePhoneNo;
            txtInsuranceNo.Text = myDossierSacrifice.InsuranceNo;
            txtPassportNo.Text = myDossierSacrifice.PassportNo;
            txtSacrificeNo.Text = myDossierSacrifice.SacrificeNo;
            txtZipCode.Text = myDossierSacrifice.ZipCode;
            txtMarriageDate.Text = myDossierSacrifice.MarriageDate;
            cmbDependentType.SelectedValue = myDossierSacrifice.DependentTypeId.ToString();
            cmbFaith.SelectedValue = myDossierSacrifice.FaithId.ToString();
            cmbReligion.SelectedValue = myDossierSacrifice.ReligionId.ToString();
            cmbIsActive.SelectedValue = myDossierSacrifice.IsActive.ToString();
            CmbDossierStatus.SelectedValue = myDossierSacrifice.DossierStatusId.ToString();
            CmbMarriageStatus.SelectedValue = myDossierSacrifice.MarriageStatusId.ToString();
            CmbNationality.SelectedValue = myDossierSacrifice.NationalityId.ToString();
            CmbNaturalization.SelectedValue = myDossierSacrifice.NaturalizationId.ToString();
            cmbprovince.SelectedValue = myDossierSacrifice.ProvinceId.ToString();
            SetCmboboxCity(new Guid(myDossierSacrifice.ProvinceId.ToString()));
            cmbCity.SelectedValue = myDossierSacrifice.CityId.ToString();
            chkHasDependentSacrifice.Checked = myDossierSacrifice.HasDependentSacrifice;
            chkHasDependentSacrifice_CheckedChanged(chkHasDependentSacrifice, new EventArgs());
            chkHasInsurance.Checked = myDossierSacrifice.HasInsurance;
            ChkHasInsurance_CheckedChanged(chkHasInsurance,new EventArgs() );
            chkHasPassport.Checked = myDossierSacrifice.HasPassport;
            chkHasPassport_CheckedChanged(chkHasPassport, new EventArgs());
            chkHasDossierSacrifice.Checked = myDossierSacrifice.HasDossierSacrifice;
            
        
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            const string objectTitlesForRowAcess = "[General].[t_Province]";
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId,
                UserId = userId,
                ObjectTitlesForRowAcess = objectTitlesForRowAcess
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            CmbNaturalization.Items.Clear();
            CmbNaturalization.Items.Add(new RadComboBoxItem(""));
            CmbNaturalization.DataSource = _countryBL.GetAllIsActive();
            CmbNaturalization.DataTextField = "CountryTitlePersian";
            CmbNaturalization.DataValueField = "CountryId";
            CmbNaturalization.DataBind();

            int userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(userId)) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
            {
                cmbprovince.DataSource = _provinceBL.GetAllIsActive();
            }
            else
            {                
                cmbprovince.DataSource = _provinceBL.GetAllProvinceIsActivePerRowAccessDB(userId,"[General].[t_Province]");                
            }
            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();

            CmbMarriageStatus.Items.Clear();
            CmbMarriageStatus.Items.Add(new RadComboBoxItem(""));
            CmbMarriageStatus.DataSource = _marriageStatusBl.GetAllIsActive();
            CmbMarriageStatus.DataTextField = "MarriageStatusTitle";
            CmbMarriageStatus.DataValueField = "MarriageStatusId";
            CmbMarriageStatus.DataBind();

            CmbDossierStatus.Items.Clear();
            CmbDossierStatus.Items.Add(new RadComboBoxItem(""));
            CmbDossierStatus.DataSource = _dossierStatusBl.GetAllIsActive();
            CmbDossierStatus.DataTextField = "DossierStatusTitle";
            CmbDossierStatus.DataValueField = "DossierStatusId";
            CmbDossierStatus.DataBind();

            CmbNationality.Items.Clear();
            CmbNationality.Items.Add(new RadComboBoxItem(""));
            CmbNationality.DataSource = _nationalityBl.GetAllIsActive();
            CmbNationality.DataTextField = "NationalityTitlePersian";
            CmbNationality.DataValueField = "NationalityId";
            CmbNationality.DataBind();

            cmbReligion.Items.Clear();
            cmbReligion.Items.Add(new RadComboBoxItem(""));
            cmbReligion.DataSource = _religionBl.GetAllIsActive();
            cmbReligion.DataTextField = "ReligionTitle";
            cmbReligion.DataValueField = "ReligionId";
            cmbReligion.DataBind();

            cmbFaith.Items.Clear();
            cmbFaith.Items.Add(new RadComboBoxItem(""));
            cmbFaith.DataSource = _faithBl.GetAllIsActive();
            cmbFaith.DataTextField = "FaithTitle";
            cmbFaith.DataValueField = "FaithId";
            cmbFaith.DataBind();

            cmbDependentType.Items.Clear();
            cmbDependentType.Items.Add(new RadComboBoxItem(""));
            cmbDependentType.DataSource = _dependentTypeBl.GetAllIsActive();
            cmbDependentType.DataTextField = "DependentTypeTitle";
            cmbDependentType.DataValueField = "DependentTypeId";
            cmbDependentType.DataBind();



        }

        private void SetCmboboxCity(Guid ProvinceId)
        {

            var cityEntity = new CityEntity {ProvinceId = ProvinceId};
            cmbCity.Items.Clear();
            cmbCity.Items.Add(new RadComboBoxItem(""));
            cmbCity.DataTextField = "CityTitlePersian";
            cmbCity.DataValueField = "CityId";
            cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
            cmbCity.DataBind();
        }
        private void CheckValidation()
        {
            if (SelectPerson.KeyId == "")
            {
                throw new ItcApplicationErrorManagerException("نام و نام خانوادگی ایثارگر را وارد نمایید.");
            }
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid());

                var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(userId)) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(userId);
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }                
                              
                SetPanelFirst();
                SetComboBox();
                SetClearToolBox();
                
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            FileUpload1.WhereClause = "jpg,jpeg,png";
            string serverPath = Path.Combine(Server.MapPath("~"), @"DeskTopModules\IsargaranProject\Isargaran\SacrificeImage");
            Session["path"] = serverPath;            
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrificeId;
                ViewState["WhereClause"] = "";
                CheckValidation();
                SetPanelFirst();
                var dossierSacrificeEntity = new DossierSacrificeEntity()
                {
                    Email = txtEmail.Text,
                    HomeAddress = txtHomeAddress.Text,
                    HomeFaxNo = txtHomeFaxNo.Text,
                    HomePhoneNo = txtHomePhoneNo.Text,
                    CellphoneNo = txtCellphoneNo.Text,
                    ZipCode = txtZipCode.Text,
                    SacrificeNo = txtSacrificeNo.Text,
                    DossierSacrificeNo = txtDossierSacrificeNo.Text,
                    FaithId = Guid.Parse(cmbFaith.SelectedValue.ToString()),
                    MarriageStatusId = (CmbMarriageStatus.SelectedIndex>0?Guid.Parse(CmbMarriageStatus.SelectedValue):new Guid()),
                    NationalityId = Guid.Parse(CmbNationality.SelectedValue),
                    NaturalizationId = Guid.Parse(CmbNaturalization.SelectedValue),
                    CityId = Guid.Parse(cmbCity.SelectedValue),
                    ReligionId = Guid.Parse(cmbReligion.SelectedValue),
                    DossierStatusId = Guid.Parse(CmbDossierStatus.SelectedValue),
                    PersonId = Guid.Parse(SelectPerson.KeyId),
                    HasDependentSacrifice = bool.Parse(chkHasDependentSacrifice.Checked.ToString()),
                    DependentTypeId = ((cmbDependentType.SelectedIndex > 0) ? Guid.Parse(cmbDependentType.SelectedValue) : new Guid()),
                    HasInsurance = bool.Parse(chkHasInsurance.Checked.ToString()),
                    InsuranceNo = txtInsuranceNo.Text,
                    HasPassport = bool.Parse(chkHasPassport.Checked.ToString()),
                    PassportNo = txtPassportNo.Text,
                    MarriageDate = ItcToDate.ShamsiToMiladi(txtMarriageDate.Text),
                    ImagePath = FileUpload1.KeyId,
                    HasDossierSacrifice = bool.Parse(chkHasDossierSacrifice.Checked.ToString()),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };


                _dossierSacrificeBl.Add(dossierSacrificeEntity, out DossierSacrificeId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, DossierSacrificeId);

                var userId = HttpContext.Current.User.Identity.Name;
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }
              
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " 1=1 ";
                
                ViewState["WhereClause"] = " 1=1 ";
                if (SelectPerson.KeyId != "")
                    ViewState["WhereClause"] =ViewState["WhereClause"] + " and PersonId ='" + Guid.Parse(SelectPerson.KeyId) + "'";
                if (txtCellphoneNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CellphoneNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtCellphoneNo.Text.Trim()) + "%'";
                if (txtDossierSacrificeNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DossierSacrificeNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtDossierSacrificeNo.Text.Trim()) + "%'";
                if (txtEmail.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Email Like N'%" +
                                               FarsiToArabic.ToArabic(txtEmail.Text.Trim()) + "%'";
                if (txtHomeAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HomeAddress Like N'%" +
                                               FarsiToArabic.ToArabic(txtHomeAddress.Text.Trim()) + "%'";
                if (txtHomeFaxNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HomeFaxNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtHomeFaxNo.Text.Trim()) + "%'";
                if (txtHomePhoneNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and HomePhoneNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtHomePhoneNo.Text.Trim()) + "%'";
                if (txtInsuranceNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and InsuranceNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtInsuranceNo.Text.Trim()) + "%'";
                if (txtPassportNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PassportNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtPassportNo.Text.Trim()) + "%'";
                if (txtSacrificeNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SacrificeNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtSacrificeNo.Text.Trim()) + "%'";
                if (txtZipCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ZipCode Like N'%" +
                                               FarsiToArabic.ToArabic(txtZipCode.Text.Trim()) + "%'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ProvinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                if (cmbDependentType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DependentTypeId ='" +
                                               new Guid(cmbDependentType.SelectedValue) + "'";
                if (cmbFaith.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FaithId ='" +
                                               new Guid(cmbFaith.SelectedValue) + "'";
                if (cmbReligion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ReligionId ='" +
                                               new Guid(cmbReligion.SelectedValue) + "'";
                if (CmbDossierStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DossierStatusId ='" +
                                               new Guid(CmbDossierStatus.SelectedValue) + "'";
                if (CmbMarriageStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MarriageStatusId ='" +
                                               new Guid(CmbMarriageStatus.SelectedValue) + "'";
                if (CmbNationality.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and NationalityId ='" +
                                               new Guid(CmbNationality.SelectedValue) + "'";
                if (CmbNaturalization.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CountryId ='" +
                                               new Guid(CmbNaturalization.SelectedValue) + "'";

                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdDossierSacrifice.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid());

                var userId = HttpContext.Current.User.Identity.Name;
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }
               
                if (grdDossierSacrifice.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid());
                var userId = HttpContext.Current.User.Identity.Name;
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "";
                CheckValidation();
                var dossierSacrificeEntity = new DossierSacrificeEntity()
                {
                    DossierSacrificeId = Guid.Parse(ViewState["DossierSacrificeId"].ToString()),
                    Email = txtEmail.Text,
                    HomeAddress = txtHomeAddress.Text,
                    HomeFaxNo = txtHomeFaxNo.Text,
                    HomePhoneNo = txtHomePhoneNo.Text,
                    CellphoneNo = txtCellphoneNo.Text,
                    ZipCode = txtZipCode.Text,
                    SacrificeNo = txtSacrificeNo.Text,
                    DossierSacrificeNo = txtDossierSacrificeNo.Text,
                    FaithId = Guid.Parse(cmbFaith.SelectedValue.ToString()),
                    MarriageStatusId = (CmbMarriageStatus.SelectedIndex > 0 ? Guid.Parse(CmbMarriageStatus.SelectedValue) : new Guid()),
                    NationalityId = Guid.Parse(CmbNationality.SelectedValue),
                    NaturalizationId = Guid.Parse(CmbNaturalization.SelectedValue),
                    DossierStatusId = Guid.Parse(CmbDossierStatus.SelectedValue),
                    CityId = Guid.Parse(cmbCity.SelectedValue),
                    ReligionId = Guid.Parse(cmbReligion.SelectedValue),
                    PersonId = Guid.Parse(SelectPerson.KeyId),
                    HasDependentSacrifice = bool.Parse(chkHasDependentSacrifice.Checked.ToString()),
                    DependentTypeId = ((cmbDependentType.SelectedIndex > 0) ? Guid.Parse(cmbDependentType.SelectedValue) : new Guid()),
                    HasInsurance = bool.Parse(chkHasInsurance.Checked.ToString()),
                    InsuranceNo = txtInsuranceNo.Text,
                    HasPassport = bool.Parse(chkHasPassport.Checked.ToString()),
                    PassportNo = txtPassportNo.Text,
                    MarriageDate = ItcToDate.ShamsiToMiladi(txtMarriageDate.Text),
                    ImagePath = FileUpload1.KeyId,
                    HasDossierSacrifice = bool.Parse(chkHasDossierSacrifice.Checked.ToString()),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)

                };
                _dossierSacrificeBl.Update(dossierSacrificeEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrificeId"].ToString()));
                var userId = HttpContext.Current.User.Identity.Name;
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrificeId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                    PnlMaster.Visible = true;
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";


                    var dossierSacrificeEntity = new DossierSacrificeEntity()
                    {
                        DossierSacrificeId = new Guid(e.CommandArgument.ToString()),

                    };
                    _dossierSacrificeBl.Delete(dossierSacrificeEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid());

                    var userId = HttpContext.Current.User.Identity.Name;
                    if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                    {
                        var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                        gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                        DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    }
                    else
                    {
                        DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                    }

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }

                if (e.CommandName == "OtherInformation")
                {
                    PnlMaster.Visible = false;
                    radmultipageOtherInformation.PageViews.Clear();
                    PnlOtherInformation.Visible = true;
                    var DossierSacrificeId = Guid.Parse(e.CommandArgument.ToString());
                    Session["DossierSacrificeId"] = DossierSacrificeId;
                    
                    ViewState["WhereClause"] = "DossierSacrificeId='" + DossierSacrificeId + "'";
                    var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, DossierSacrificeId);
                    var userId = HttpContext.Current.User.Identity.Name;
                    if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                    {
                        var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                        gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                        DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    }
                    else
                    {
                        DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                    }
                  
                    e.Item.Selected = true;
                    grdDossierSacrifice.MasterTableView.GetColumnSafe("ShowAll").Visible = true;                   
                    RadTabOtherInformation_TabClick(new object(), new RadTabStripEventArgs(RadTabOtherInformation.FindTabByValue("DossierSacrificeEducation")));
                }
                if (e.CommandName == "_ShowAll")
                {
                    try
                    {
                        ViewState["WhereClause"] = "";
                        var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid());

                        var userId = HttpContext.Current.User.Identity.Name;
                        if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                        {
                            var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                            gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                            DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                        }
                        else
                        {
                            DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                        }

                        SetPanelFirst();
                    }
                    catch (Exception ex)
                    {
                        CustomMessageErrorControl.ShowErrorMessage(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                var userId = HttpContext.Current.User.Identity.Name;
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid());
                var userId = HttpContext.Current.User.Identity.Name;
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, new Guid());
                var userId = HttpContext.Current.User.Identity.Name;
                if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                {
                    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                else
                {
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                }

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بعد از لود کامل اطلاعات در گرید اجرا میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdDossierSacrifice_DataBound(object sender, EventArgs e)
        {
            //--------------------- لود کردن اطلاعات کمبوباکس جزئیات پرونده ایثارگری بر اساس سامانه دسترسی ها
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            var categorizeDataList = _webFormCategorizeBL.GetAllWebformCategorizePerRowAccess("Isar.t_WebFormCategorize", userId);
            foreach (GridDataItem gridDataItem in grdDossierSacrifice.Items)
            {
                var cmbCategory = (CustomRadComboBox)gridDataItem["_OtherInformation"].FindControl("cmbWebFormCategory");
                foreach (var webformCategorize in categorizeDataList)
                {
                    var radComboBoxItem = new RadComboBoxItem(webformCategorize.WebFormCategorizeTitle, webformCategorize.WebFormCategorizeId.ToString());
                    cmbCategory.Items.Add(radComboBoxItem);
                }
            }
            //------------------------------------------------------------------------------------
        }

        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            if (!string.IsNullOrEmpty(e.Value))
            {
                cmbCity.Text = "";
                cmbCity.Items.Clear();
                SetCmboboxCity(Guid.Parse(e.Value.ToString()));
            }
            else
            {
                cmbCity.Items.Clear();
                cmbCity.Text = "";
            }


        }

        /// <summary>
        /// اگر منسوبین به ایثارگران ندارد جعبه متن غیر فعال می گردد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       
        protected void chkHasDependentSacrifice_CheckedChanged(object sender, EventArgs e)
        {

            if (chkHasDependentSacrifice.Checked)
            {
                cmbDependentType.Enabled = true;
                cmbDependentType.BackColor = Color.White;
            }
            else
            {
                cmbDependentType.Enabled = false;
                cmbDependentType.BackColor = Color.Silver;
                cmbDependentType.ClearSelection();
            }

        }

        /// <summary>
        /// اگر داری بیمه ندارد جعبه متن غیر فعال می گردد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void ChkHasInsurance_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHasInsurance.Checked)
            {
                txtInsuranceNo.Enabled = true;
                txtInsuranceNo.BackColor = Color.White;
            }
            else
            {
                txtInsuranceNo.Enabled = false;
                txtInsuranceNo.BackColor = Color.Silver;
                txtInsuranceNo.Text = "";
            }
        }

        protected void chkHasPassport_CheckedChanged(object sender, EventArgs e)
        {
            if (chkHasPassport.Checked)
            {
                txtPassportNo.Enabled = true;
                txtPassportNo.BackColor = Color.White;
            }
            else
            {
                txtPassportNo.Enabled = false;
                txtPassportNo.BackColor = Color.Silver;
                txtPassportNo.Text = "";
            }
        }

        protected void RadTabOtherInformation_TabClick(object sender, RadTabStripEventArgs e)
        {
            radmultipageOtherInformation.PageViews.Clear();
            const string baseUrl = @"DesktopModules\IsargaranProject\Isargaran\";
            var pageView = new RadPageView
            {
                ID = baseUrl + e.Tab.Value,
            };
            radmultipageOtherInformation.PageViews.Add(pageView);
            e.Tab.Selected = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void radmultipageOtherInformation_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            //Session["DossierSacrificeId"] = null;
            if (Session["DossierSacrificeId"] == null)
            {
                Session["MessageError"] = "کاربر گرامی، به علت بدون استفاده ماندن بیش از حد صفحه برای امنیت اطلاعات، عملیات را از ابتدا آغاز نمایید. ";                                
                //Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=4253" + "&page=IsargaranProject/Isargaran/SacrificeInformation/DossierSacrificePage"));
                Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);

            }
            if (!string.IsNullOrEmpty(e.PageView.ID))
            {
                string userControlName = e.PageView.ID;
                var userControl = (Page.LoadControl(userControlName));
                if (userControl != null)
                {
                    userControl.ID = ViewState["MenuPageName"] + "_userControl";
                }
                if (userControl != null) e.PageView.Controls.Add(userControl);
                var tabedControl = (radmultipageOtherInformation.PageViews[0]).Controls[0] as ITC.Library.Classes.ITabedControl;
                if (tabedControl != null) tabedControl.InitControl();
                e.PageView.Selected = true;
            }
        }

        /// <summary>
        /// بعد از انتخاب نوع دسته بندی فرم، فرم های آن دسته نمایش داده میشود 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbWebFormCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                var cmbFormCategorize = (RadComboBox)sender;               
                if (cmbFormCategorize.SelectedIndex > 0)
                {
                    PnlMaster.Visible = false;
                    var row = (GridDataItem)cmbFormCategorize.NamingContainer;                   
                    var dossierSacrificeId = Guid.Parse(row.GetDataKeyValue("DossierSacrificeId").ToString());
                    radmultipageOtherInformation.PageViews.Clear();
                    PnlOtherInformation.Visible = true;
                    LblSelectedCommand.Text = cmbFormCategorize.SelectedItem.Text;
                    Session["DossierSacrificeId"] = dossierSacrificeId;
                    ViewState["WhereClause"] = "DossierSacrificeId='" + dossierSacrificeId + "'";
                    var gridParamEntity = SetGridParam(grdDossierSacrifice.MasterTableView.PageSize, 0, dossierSacrificeId);

                    var userId = HttpContext.Current.User.Identity.Name;
                    if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId))) // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                    {
                        var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                        gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "") ? " NationalNo='" + user.UserName + "'" : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                        DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    }
                    else
                    {
                        DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                    }                  
                    row.Selected = true;
                    grdDossierSacrifice.MasterTableView.GetColumnSafe("ShowAll").Visible = true; 
                                
                    // لود کردن تب ها
                    var formCategoryId = Guid.Parse(cmbFormCategorize.SelectedValue);
                    var webFormList = _webFormBL.GetWebFormCollectionByWebFormCategorizePerRowAccess(formCategoryId, userId, "Isar.t_WebForm");
                    FillTabStrip(webFormList);

                    if (RadTabOtherInformation.Tabs.Count>0)
                    {
                        RadTabOtherInformation_TabClick(new object(), new RadTabStripEventArgs(RadTabOtherInformation.Tabs[0]));    
                    }                    
                }
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(exception.Message);
            }
        }

        private void FillTabStrip(List<WebFormEntity> webFormList)
        {           
            RadTabOtherInformation.DataTextField = "WebFormTitle";
            RadTabOtherInformation.DataValueField = "WebFormURL";
            RadTabOtherInformation.DataSource = webFormList;
            RadTabOtherInformation.DataBind();
        }

        #endregion

        

        
        
    }
}