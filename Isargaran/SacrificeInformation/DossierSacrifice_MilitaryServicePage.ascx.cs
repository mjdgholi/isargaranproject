﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;


namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/26>
    /// Description: <پرونده خدمات سربازی ایثارگر>
    /// </summary>
    public partial class DossierSacrifice_MilitaryServicePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly CityBL _cityBl = new CityBL();
        private readonly ProvinceBL _provinceBl = new ProvinceBL();
        private readonly MilitaryStatusBL _militaryStatusBL = new MilitaryStatusBL();
        private readonly DossierSacrifice_MilitaryServiceBL _dossierSacrificeMilitaryServiceBL = new DossierSacrifice_MilitaryServiceBL();
        private const string TableName = "Isar.V_DossierSacrifice_MilitaryService";
        private const string PrimaryKey = "DossierSacrifice_MilitaryServiceId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtAddress.Text = "";
            txtCardIssueDate.Text = "";
            txtCardNo.Text = "";
            txtClassifiedNo.Text = "";
            txtDistrictHandler.Text = "";
            txtEmail.Text = "";
            txtFaxNo.Text = "";
            txtMilitaryServiceLocation.Text = "";            
            txtWebAddress.Text = "";
            txtVillageTitle.Text = "";
            txtTelNo.Text = "";
            txtSectionTitle.Text = "";            
            txtEndDate.Text = "";
            txtStartDate.Text = "";            
            cmbCity.ClearSelection();
            cmbCity.Items.Clear();
            cmbprovince.ClearSelection();  
            cmbMilitaryStatus.ClearSelection();
            txtZipCode.Text = "";

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeMilitaryServiceEntity = new DossierSacrifice_MilitaryServiceEntity()
            {
                DossierSacrifice_MilitaryServiceId = new Guid(ViewState["DossierSacrifice_MilitaryServiceId"].ToString())
            };
            var mydossierSacrificeMilitaryServic = _dossierSacrificeMilitaryServiceBL.GetSingleById(dossierSacrificeMilitaryServiceEntity);
            txtEndDate.Text = mydossierSacrificeMilitaryServic.EndDate;
            txtAddress.Text = mydossierSacrificeMilitaryServic.Address;
            txtCardIssueDate.Text = mydossierSacrificeMilitaryServic.CardIssueDate;
            txtCardNo.Text = mydossierSacrificeMilitaryServic.CardNo;
            txtClassifiedNo.Text = mydossierSacrificeMilitaryServic.ClassifiedNo;
            txtDistrictHandler.Text = mydossierSacrificeMilitaryServic.DistrictHandler;
            txtEmail.Text = mydossierSacrificeMilitaryServic.Email;
            txtFaxNo.Text = mydossierSacrificeMilitaryServic.FaxNo;
            txtMilitaryServiceLocation.Text = mydossierSacrificeMilitaryServic.MilitaryServiceLocation;
            txtSectionTitle.Text = mydossierSacrificeMilitaryServic.SectionTitle;
            txtStartDate.Text = mydossierSacrificeMilitaryServic.StartDate;
            txtTelNo.Text = mydossierSacrificeMilitaryServic.TelNo;
            txtVillageTitle.Text = mydossierSacrificeMilitaryServic.VillageTitle;
            txtWebAddress.Text = mydossierSacrificeMilitaryServic.WebAddress;
            cmbMilitaryStatus.SelectedValue = mydossierSacrificeMilitaryServic.MilitaryStatusId.ToString();
            if (cmbMilitaryStatus.FindItemByValue(mydossierSacrificeMilitaryServic.MilitaryStatusId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد وضیعت نظام وظیفه انتخاب شده معتبر نمی باشد.");
            }
            cmbprovince.SelectedValue = mydossierSacrificeMilitaryServic.ProvinceId.ToString();
            if (cmbprovince.FindItemByValue(mydossierSacrificeMilitaryServic.ProvinceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد استان انتخاب شده معتبر نمی باشد.");
            }
            SetCmboboxCity(Guid.Parse(mydossierSacrificeMilitaryServic.ProvinceId.ToString()));
            cmbCity.SelectedValue = mydossierSacrificeMilitaryServic.CityId.ToString();
            if (cmbCity.FindItemByValue(mydossierSacrificeMilitaryServic.CityId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
            }



        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_MilitaryService,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbMilitaryStatus.Items.Clear();
            cmbMilitaryStatus.Items.Add(new RadComboBoxItem(""));
            cmbMilitaryStatus.DataSource = _militaryStatusBL.GetAllIsActive();
            cmbMilitaryStatus.DataTextField = "MilitaryStatusTitle";
            cmbMilitaryStatus.DataValueField = "MilitaryStatusId";
            cmbMilitaryStatus.DataBind();

            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBl.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();


        }
        private void SetCmboboxCity(Guid ProvinceId)
        {

            var cityEntity = new CityEntity { ProvinceId = ProvinceId };
            cmbCity.Items.Clear();
            cmbCity.Items.Add(new RadComboBoxItem(""));
            cmbCity.DataTextField = "CityTitlePersian";
            cmbCity.DataValueField = "CityId";
            cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
            cmbCity.DataBind();
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_MilitaryServiceId  ;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeMilitaryServiceEntity = new DossierSacrifice_MilitaryServiceEntity()
                {                    
                    Address = txtAddress.Text,
                    CardIssueDate = ItcToDate.ShamsiToMiladi(txtCardIssueDate.Text),
                    CardNo = txtCardNo.Text,
                    CityId = Guid.Parse(cmbCity.SelectedValue),
                    ClassifiedNo = txtClassifiedNo.Text,
                    DistrictHandler = txtDistrictHandler.Text,
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Email = txtEmail.Text,
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    FaxNo = txtFaxNo.Text,                    
                    MilitaryServiceLocation = txtMilitaryServiceLocation.Text,
                    SectionTitle = txtSectionTitle.Text,
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    TelNo = txtTelNo.Text,
                    VillageTitle = txtVillageTitle.Text,
                    WebAddress = txtAddress.Text,
                    ZipCode = txtZipCode.Text,
                    MilitaryStatusId = (cmbMilitaryStatus.SelectedIndex > 0 ? Guid.Parse(cmbMilitaryStatus.SelectedValue) : new Guid())

                    
                };

                _dossierSacrificeMilitaryServiceBL.Add(dossierSacrificeMilitaryServiceEntity, out DossierSacrifice_MilitaryServiceId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, DossierSacrifice_MilitaryServiceId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Address Like N'%" +
                                               FarsiToArabic.ToArabic(txtAddress.Text.Trim()) + "%'";
                if (txtCardNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CardNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtCardNo.Text.Trim()) + "%'";
                if (txtClassifiedNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ClassifiedNo Like N'%" +
                                               Decimal.Parse(txtClassifiedNo.Text.Trim()) + "%'";
                if (txtStartDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                                              (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                              (txtEndDate.Text.Trim()) + "'";
                if (txtCardIssueDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CardIssueDate ='" +
                                              (txtCardIssueDate.Text.Trim()) + "'";
                if (txtFaxNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FaxNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtFaxNo.Text.Trim()) + "%'";
                if (txtMilitaryServiceLocation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MilitaryServiceLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtMilitaryServiceLocation
                                               .Text.Trim()) + "%'";
                if (txtSectionTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SectionTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtSectionTitle.Text.Trim()) + "%'";
                if (txtTelNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TelNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtTelNo.Text.Trim()) + "%'";
                if (txtVillageTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VillageTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtVillageTitle.Text.Trim()) + "%'";
                if (txtWebAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WebAddress Like N'%" +
                                               FarsiToArabic.ToArabic(txtWebAddress.Text.Trim()) + "%'";
                if (txtZipCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ZipCode Like N'%" +
                                               FarsiToArabic.ToArabic(txtZipCode.Text.Trim()) + "%'";
                if (cmbMilitaryStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MilitaryStatusId ='" +
                                               new Guid(cmbMilitaryStatus.SelectedValue) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and provinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
             
                grdDossierSacrifice_MilitaryService.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_MilitaryService.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeMilitaryServiceEntity = new DossierSacrifice_MilitaryServiceEntity()
                {
                    DossierSacrifice_MilitaryServiceId = Guid.Parse(ViewState["DossierSacrifice_MilitaryServiceId"].ToString()),
                    Address = txtAddress.Text,
                    CardIssueDate = ItcToDate.ShamsiToMiladi(txtCardIssueDate.Text),
                    CardNo = txtCardNo.Text,
                    CityId = Guid.Parse(cmbCity.SelectedValue),
                    ClassifiedNo = txtClassifiedNo.Text,
                    DistrictHandler = txtDistrictHandler.Text,
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Email = txtEmail.Text,
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    FaxNo = txtFaxNo.Text,
                    //HasMilitaryServiceCard = bool.Parse(chkHasMilitaryServiceCard.Checked.ToString()),
                    MilitaryServiceLocation = txtMilitaryServiceLocation.Text,
                    SectionTitle = txtSectionTitle.Text,
                    StartDate = ItcToDate.ShamsiToMiladi(txtStartDate.Text),
                    TelNo = txtTelNo.Text,
                    VillageTitle = txtVillageTitle.Text,
                    WebAddress = txtAddress.Text,
                    ZipCode = txtZipCode.Text,
                    MilitaryStatusId = (cmbMilitaryStatus.SelectedIndex > 0 ? Guid.Parse(cmbMilitaryStatus.SelectedValue) : new Guid())
                };
                _dossierSacrificeMilitaryServiceBL.Update(dossierSacrificeMilitaryServiceEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_MilitaryServiceId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdDossierSacrifice_MilitaryService_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_MilitaryServiceId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeMilitaryServiceEntity = new DossierSacrifice_MilitaryServiceEntity()
                    {
                        DossierSacrifice_MilitaryServiceId = Guid.Parse(e.CommandArgument.ToString()),   
                    };
                    _dossierSacrificeMilitaryServiceBL.Delete(dossierSacrificeMilitaryServiceEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDossierSacrifice_MilitaryService_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_MilitaryService_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_MilitaryService_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_MilitaryService.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                cmbCity.Text = "";
                cmbCity.Items.Clear();
                SetCmboboxCity(Guid.Parse(e.Value.ToString()));
            }
            else
            {
                cmbCity.Items.Clear();
                cmbCity.Text = "";
            }


        }
        #endregion

        
    }
}