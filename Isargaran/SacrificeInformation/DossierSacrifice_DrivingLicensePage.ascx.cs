﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/26>
    // Description:	<محدودیت رانندگی>
    /// </summary>
    public partial class DossierSacrifice_DrivingLicensePage : ItcBaseControl
    {

        #region PublicParam:
     
        private readonly DrivingRestrictionBL _DrivingRestrictionBl = new DrivingRestrictionBL();      
        private readonly DrivingLicenseTypeBL _DrivingLicenseTypeBl = new DrivingLicenseTypeBL();       
        private readonly DossierSacrifice_DrivingLicenseBL _DossierSacrifice_DrivingLicenseBL = new DossierSacrifice_DrivingLicenseBL();       

        private const string TableName = "Isar.v_DossierSacrifice_DrivingLicense";
        private const string PrimaryKey = "DossierSacrifice_DrivingLicenseId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            TxtCardIssueDate.Text = "";
            TxtCardExpiredDate.Text = "";
            
            txtCardNo.Text = "";   
                             
            CmbDrivingLicenseType.ClearSelection();
            CmbDrivingRestriction.ClearSelection();
   
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeDrivingLicenseEntity = new DossierSacrifice_DrivingLicenseEntity()
            {
                DossierSacrifice_DrivingLicenseId = new Guid(ViewState["DossierSacrifice_DrivingLicenseId"].ToString())
            };
            var myDossierSacrificeDrivingLicense = _DossierSacrifice_DrivingLicenseBL.GetSingleById(dossierSacrificeDrivingLicenseEntity);
            TxtCardIssueDate.Text = myDossierSacrificeDrivingLicense.CardIssueDate;
            TxtCardExpiredDate.Text = myDossierSacrificeDrivingLicense.CardExpiredDate;
            
            txtCardNo.Text = myDossierSacrificeDrivingLicense.CardNo;
            CmbDrivingRestriction.SelectedValue = myDossierSacrificeDrivingLicense.DrivingRestrictionId.ToString();
            if (CmbDrivingRestriction.FindItemByValue(myDossierSacrificeDrivingLicense.DrivingRestrictionId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد استان انتخاب شده معتبر نمی باشد.");
            }
            
            CmbDrivingLicenseType.SelectedValue = myDossierSacrificeDrivingLicense.DrivingLicenseTypeId.ToString();
            if (CmbDrivingLicenseType.FindItemByValue(myDossierSacrificeDrivingLicense.DrivingLicenseTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رشته تحصیلی انتخاب شده معتبر نمی باشد.");
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_DrivingLicense,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            CmbDrivingRestriction.Items.Clear();
            CmbDrivingRestriction.Items.Add(new RadComboBoxItem(""));
            CmbDrivingRestriction.DataSource = _DrivingRestrictionBl.GetAllIsActive();
            CmbDrivingRestriction.DataTextField = "DrivingRestrictionTitlePersian";
            CmbDrivingRestriction.DataValueField = "DrivingRestrictionId";
            CmbDrivingRestriction.DataBind();

            CmbDrivingLicenseType.Items.Clear();
            CmbDrivingLicenseType.Items.Add(new RadComboBoxItem(""));
            CmbDrivingLicenseType.DataSource = _DrivingLicenseTypeBl.GetAllIsActive();
            CmbDrivingLicenseType.DataTextField = "DrivingLicenseTypeTitlePersian";
            CmbDrivingLicenseType.DataValueField = "DrivingLicenseTypeId";
            CmbDrivingLicenseType.DataBind();          

        }       

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();               
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }       

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();               
                var dossierSacrificeDrivingLicenseEntity = new DossierSacrifice_DrivingLicenseEntity
                {
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    CardIssueDate = ItcToDate.ShamsiToMiladi(TxtCardIssueDate.Text),
                    CardExpiredDate = ItcToDate.ShamsiToMiladi(TxtCardExpiredDate.Text),                       
                    CardNo = txtCardNo.Text,                 
                    DrivingLicenseTypeId = Guid.Parse(CmbDrivingLicenseType.SelectedValue),
                    DrivingRestrictionId = (CmbDrivingRestriction.SelectedIndex>0?Guid.Parse(CmbDrivingRestriction.SelectedValue):new Guid()),
                };

                _DossierSacrifice_DrivingLicenseBL.Add(dossierSacrificeDrivingLicenseEntity, out dossierSacrificeEducationId);

                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                
                if (txtCardNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CardNo ='" +
                                               (txtCardNo.Text.Trim()) + "'";
               
                if (CmbDrivingRestriction.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DrivingRestrictionId ='" +
                                               new Guid(CmbDrivingRestriction.SelectedValue) + "'";
                if (CmbDrivingLicenseType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DrivingLicenseTypeId ='" +
                                               new Guid(CmbDrivingLicenseType.SelectedValue) + "'";               
                grdDossierSacrifice_DrivingLicense.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_DrivingLicense.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeStudentEntity = new DossierSacrifice_DrivingLicenseEntity()
                {
                    DossierSacrifice_DrivingLicenseId = Guid.Parse(ViewState["DossierSacrifice_DrivingLicenseId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    CardIssueDate = ItcToDate.ShamsiToMiladi(TxtCardIssueDate.Text),
                    CardExpiredDate = ItcToDate.ShamsiToMiladi(TxtCardExpiredDate.Text),
                    
                    CardNo = txtCardNo.Text,
                    DrivingLicenseTypeId = Guid.Parse(CmbDrivingLicenseType.SelectedValue),
                    DrivingRestrictionId = (CmbDrivingRestriction.SelectedIndex > 0 ? Guid.Parse(CmbDrivingRestriction.SelectedValue) : new Guid()),
                };
                _DossierSacrifice_DrivingLicenseBL.Update(dossierSacrificeStudentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_DrivingLicenseId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDossierSacrifice_DrivingLicense_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_DrivingLicenseId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_DrivingLicenseEntity()
                    {
                        DossierSacrifice_DrivingLicenseId = new Guid(e.CommandArgument.ToString()),

                    };
                    _DossierSacrifice_DrivingLicenseBL.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_DrivingLicense_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_DrivingLicense_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_DrivingLicense_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_DrivingLicense.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }        

        #endregion


    }
}