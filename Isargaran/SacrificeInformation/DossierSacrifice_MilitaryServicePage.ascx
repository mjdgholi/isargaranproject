﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_MilitaryServicePage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_MilitaryServicePage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8F
        F;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="DossierSacrificeMilitaryService">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="DossierSacrificeMilitaryService">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%" >
                    <table width="100%">
                        <tr>
                                     <td width="10%" nowrap="nowrap">
                      <%--          <asp:CheckBox ID="chkHasMilitaryServiceCard" runat="server" AutoPostBack="True" Checked="True"
                                    Text="آیا کارت پایان خدمت دارد؟" 
                                             oncheckedchanged="chkHasMilitaryServiceCard_CheckedChanged"  />--%>
                                         
                                          <asp:Label CssClass="LabelCSS" ID="lblCardNo" runat="server"> شماره کارت پایان خدمت:</asp:Label>
                            </td>
                            <td width="20%">
                                  <telerik:RadTextBox ID="txtCardNo" runat="server"  MaxLength="20"></telerik:RadTextBox>
                            </td>

                            <td width="5%"></td>
                            <td width="10%">
                                 <asp:Label CssClass="LabelCSS" ID="lblCardIssueDate" runat="server"> تاریخ صدور کارت:</asp:Label>
                            </td>
                            <td width="20%">         <cc1:CustomItcCalendar ID="txtCardIssueDate" runat="server"></cc1:CustomItcCalendar></td>
                            <td width="35%"></td>
                            

                        </tr>
                                                <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblClassifiedNo" runat="server"> کلاسه پرونده:</asp:Label>
                            </td>
                            <td>
                    <telerik:RadTextBox ID="txtClassifiedNo" runat="server" MaxLength="10"></telerik:RadTextBox>
                            </td>
                            <td >
                                
                            </td>
                            
                             <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblDistrictHandler" runat="server"> حوزه رسیدگی کننده:</asp:Label>
                            </td>
                            <td>
                    <telerik:RadTextBox ID="txtDistrictHandler" runat="server" MaxLength="10"></telerik:RadTextBox>
                            </td>
                            <td>
         
                            </td>
                        </tr>
                                                <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblMilitaryStatus" runat="server"> وضیعت نظام وظیفه:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbMilitaryStatus" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                
                            </td>
                           <td nowrap="nowrap" >
                                <asp:Label CssClass="LabelCSS" ID="lblMilitaryServiceLocation" runat="server"> محل خدمت سربازی<font color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                          <telerik:RadTextBox ID="txtMilitaryServiceLocation" runat="server" ></telerik:RadTextBox>
                            </td>
                            <td>
                                   <asp:RequiredFieldValidator ID="rfvtxtMilitaryServiceLocation" runat="server" ControlToValidate="txtMilitaryServiceLocation"
                                    ValidationGroup="DossierSacrificeMilitaryService" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                           
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblprovince" runat="server">استان<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbprovince" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300"
                                    AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cmbprovince_SelectedIndexChanged">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="cmbprovince"
                                    ValidationGroup="DossierSacrificeMilitaryService" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCity" runat="server"  > شهر<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbCity" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="CmbCity"
                                    ValidationGroup="DossierSacrificeMilitaryService" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblVillageTitle" runat="server">روستا:</asp:Label>
                            </td>
                            <td align="right">
                            <telerik:RadTextBox ID="txtVillageTitle" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
              
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblSectionTitle" runat="server">بخش:</asp:Label>
                            </td>
                            <td align="right">
                                          <telerik:RadTextBox ID="txtSectionTitle" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                                
                            </td>
                        </tr>

                        <tr>

                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblTelNo" runat="server">شماره تلفن :</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtTelNo" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                                                       <td>
                                <asp:Label CssClass="LabelCSS" ID="lblFaxNo" runat="server">شماره فکس:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtFaxNo" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                                                <tr>

                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblZipCode" runat="server">کد پستی:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtZipCode" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                                                       <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEmail" runat="server">ایمیل:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtEmail" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                                                                        <tr>

                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblWebAddress" runat="server">وب سایت:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtWebAddress" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                                                       <td>
                                <asp:Label CssClass="LabelCSS" ID="lblAddress" runat="server">آدرس:</asp:Label>
                            </td>
                            <td align="right">
                                <telerik:RadTextBox ID="txtAddress" runat="server"></telerik:RadTextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblStartDate" runat="server">تاریخ شروع<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtStartDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDate"
                                    ValidationGroup="DossierSacrificeMilitaryService" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblEndDate" runat="server">تاریخ پایان:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtEndDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td>
                            </td>
                        </tr>

                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_MilitaryService" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" 
                                    onitemcommand="grdDossierSacrifice_MilitaryService_ItemCommand" 
                                    onpageindexchanged="grdDossierSacrifice_MilitaryService_PageIndexChanged" 
                                    onpagesizechanged="grdDossierSacrifice_MilitaryService_PageSizeChanged" 
                                    onsortcommand="grdDossierSacrifice_MilitaryService_SortCommand" >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_MilitaryServiceId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
        
                                            <telerik:GridBoundColumn DataField="MilitaryServiceLocation" HeaderText="محل خدمت سربازی" SortExpression="MilitaryServiceLocation">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DistrictHandler" HeaderText="حوزه رسیدگی"
                                                SortExpression="DistrictHandler">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClassifiedNo" HeaderText="کلاسه پرونده"
                                                SortExpression="ClassifiedNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CardIssueDate" HeaderText="تاریخ صدور کارت"
                                                SortExpression="CardIssueDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CardNo" HeaderText="شماره کارت"
                                                SortExpression="CardNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                             <telerik:GridBoundColumn DataField="MilitaryStatusTitle" HeaderText="وضیعت سربازی"
                                                SortExpression="MilitaryStatusTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="ProvinceTitlePersian" HeaderText="استان" SortExpression="ProvinceTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                         <telerik:GridBoundColumn DataField="CityTitlePersian" HeaderText="شهر" SortExpression="CityTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                             <telerik:GridBoundColumn DataField="VillageTitle" HeaderText="روستا"
                                                SortExpression="VillageTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                               <telerik:GridBoundColumn DataField="SectionTitle" HeaderText="بخش"
                                                SortExpression="SectionTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                               
                                            <telerik:GridBoundColumn DataField="StartDate" HeaderText="تاریخ شروع" SortExpression="StartDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EndDate" HeaderText="تاریخ پایان" SortExpression="EndDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                                                                                     <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_MilitaryServiceId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_MilitaryServiceId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_MilitaryServiceId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkHasMilitaryServiceCard">
            <UpdatedControls>
          
                <telerik:AjaxUpdatedControl ControlID="chkHasMilitaryServiceCard" />
                <telerik:AjaxUpdatedControl ControlID="txtCardNo" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbprovince">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbCity" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_MilitaryService">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" >
</telerik:RadAjaxLoadingPanel>
