﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <پرونده تخلف>
    /// </summary>
    public partial class DossierSacrifice_OfficeInfractionPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly DossierSacrifice_OfficeInfractionBL _dossierSacrificeOfficeInfractionBL = new DossierSacrifice_OfficeInfractionBL();
        private readonly InfractionTypeBL _infractionTypeBL = new InfractionTypeBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_OfficeInfraction]";
        private const string PrimaryKey = "DossierSacrifice_OfficeInfractionId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtInfractionContent.Text = "";
            txtInfractionDate.Text = "";
            txtInfractionLocation.Text = "";
            txtInfractionTime.Text = "";
            cmbInfractionType.ClearSelection();            

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeOfficeInfractionEntity = new DossierSacrifice_OfficeInfractionEntity()
            {
                DossierSacrifice_OfficeInfractionId = Guid.Parse(ViewState["DossierSacrifice_OfficeInfractionId"].ToString())
            };
            var myDossierSacrifice_OfficeInfraction = _dossierSacrificeOfficeInfractionBL.GetSingleById(dossierSacrificeOfficeInfractionEntity);
            cmbInfractionType.SelectedValue = myDossierSacrifice_OfficeInfraction.InfractionTypeId.ToString();
            if (cmbInfractionType.FindItemByValue(myDossierSacrifice_OfficeInfraction.InfractionTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع تخلف انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = myDossierSacrifice_OfficeInfraction.Description;
            txtInfractionContent.Text = myDossierSacrifice_OfficeInfraction.InfractionContent;
            txtInfractionDate.Text = myDossierSacrifice_OfficeInfraction.InfractionDate;
            txtInfractionLocation.Text = myDossierSacrifice_OfficeInfraction.InfractionLocation;
            txtInfractionTime.Text = myDossierSacrifice_OfficeInfraction.InfractionTime;
            
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_OfficeInfraction,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbInfractionType.Items.Clear();
            cmbInfractionType.Items.Add(new RadComboBoxItem(""));
            cmbInfractionType.DataSource = _infractionTypeBL.GetAllIsActive();
            cmbInfractionType.DataTextField = "InfractionTypeTitle";
            cmbInfractionType.DataValueField = "InfractionTypeId";
            cmbInfractionType.DataBind();
        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid dossierSacrificeOfficeInfractionId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeOfficeInfractionEntity = new DossierSacrifice_OfficeInfractionEntity()
                {                    
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    InfractionDate = ItcToDate.ShamsiToMiladi(txtInfractionDate.Text),
                    InfractionLocation= txtInfractionLocation.Text,
                    InfractionTime= txtInfractionTime.TextWithLiterals,
                    InfractionTypeId = Guid.Parse(cmbInfractionType.SelectedValue),
                    InfractionContent = txtInfractionContent.Text,                      
                };
                _dossierSacrificeOfficeInfractionBL.Add(dossierSacrificeOfficeInfractionEntity, out dossierSacrificeOfficeInfractionId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, dossierSacrificeOfficeInfractionId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";

                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtInfractionLocation.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and InfractionLocation Like N'%" +
                                               FarsiToArabic.ToArabic(txtInfractionLocation.Text.Trim()) + "%'";
                if (txtInfractionContent.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and InfractionContent Like N'%" +
                                               FarsiToArabic.ToArabic(txtInfractionContent.Text.Trim()) + "%'";
                if (txtInfractionTime.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and InfractionTime='" +
                                               (txtInfractionTime.Text.Trim()) + "'";
                if (txtInfractionDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and InfractionDate ='" +
                                               (txtInfractionDate.Text.Trim()) + "'";                
                if (cmbInfractionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And InfractionTypeId='" +
                                               (cmbInfractionType.SelectedValue.Trim()) + "'";

                grdDossierSacrifice_OfficeInfraction.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_OfficeInfraction.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var dossierSacrificeOfficeInfractionEntity = new DossierSacrifice_OfficeInfractionEntity()
                {
                    DossierSacrifice_OfficeInfractionId = Guid.Parse(ViewState["DossierSacrifice_OfficeInfractionId"].ToString()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    Description = txtDescription.Text,
                    InfractionDate = ItcToDate.ShamsiToMiladi(txtInfractionDate.Text),
                    InfractionLocation = txtInfractionLocation.Text,
                    InfractionTime = txtInfractionTime.TextWithLiterals,
                    InfractionTypeId = Guid.Parse(cmbInfractionType.SelectedValue),
                    InfractionContent = txtInfractionContent.Text,
                };
                _dossierSacrificeOfficeInfractionBL.Update(dossierSacrificeOfficeInfractionEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_OfficeInfractionId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_OfficeInfraction_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_OfficeInfractionId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    var dossierSacrificeOfficeInfractionEntity = new DossierSacrifice_OfficeInfractionEntity()
                    {
                        DossierSacrifice_OfficeInfractionId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeOfficeInfractionBL.Delete(dossierSacrificeOfficeInfractionEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdDossierSacrifice_OfficeInfraction_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDossierSacrifice_OfficeInfraction_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_OfficeInfraction_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_OfficeInfraction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

        
    }
}