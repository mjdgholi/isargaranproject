﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;


namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/02/14>
    // Description:	<اطلاعات ایجاد زمینه اشتغال>
    /// </summary>
    public partial class DossierSacrifice_CreatingAreasOfEmploymentPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly PersonBL _personBL = new PersonBL();
        private readonly OrganizationBL _organizationBl = new OrganizationBL();
        private readonly DependentTypeBL _dependentType = new DependentTypeBL();
        private readonly DossierSacrifice_CreatingAreasOfEmploymentBL _dossierSacrificeTestimonyOrDeathBL = new DossierSacrifice_CreatingAreasOfEmploymentBL();

        private const string TableName = "Isar.v_DossierSacrifice_CreatingAreasOfEmployment";
        private const string PrimaryKey = "DossierSacrifice_CreatingAreasOfEmploymentId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtEmploymentDate.Text = "";
            SelectPerson.KeyId="";
            SelectPerson.Title = "";
            cmbOrganization.ClearSelection();
            CmbDependentType.ClearSelection();
            CmbIsItWorking.ClearSelection();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeAppearanceEntity = new DossierSacrifice_CreatingAreasOfEmploymentEntity()
                                                       {
                                                           DossierSacrifice_CreatingAreasOfEmploymentId = new Guid(ViewState["DossierSacrifice_CreatingAreasOfEmploymentId"].ToString())
                                                       };
            var myDossierSacrificeAppearance = _dossierSacrificeTestimonyOrDeathBL.GetSingleById(dossierSacrificeAppearanceEntity);
            txtDescription.Text = myDossierSacrificeAppearance.Description.ToString();
            txtEmploymentDate.Text = myDossierSacrificeAppearance.EmploymentDate.ToString();
            SelectPerson.KeyId = myDossierSacrificeAppearance.EmployedPersonId.ToString();
            var personEntity = new PersonEntity() { PersonId = new Guid(SelectPerson.KeyId) };
            personEntity = _personBL.GetSingleById(personEntity);
            SelectPerson.Title = personEntity.FirstName + " " + personEntity.LastName;

            cmbOrganization.SelectedValue = myDossierSacrificeAppearance.OrganizationId.ToString();
            if (cmbOrganization.FindItemByValue(myDossierSacrificeAppearance.OrganizationId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رنگ چشم انتخاب شده معتبر نمی باشد.");
            }
            CmbDependentType.SelectedValue = myDossierSacrificeAppearance.DependentTypeId.ToString();
            if (CmbDependentType.FindItemByValue(myDossierSacrificeAppearance.DependentTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رنگ مو انتخاب شده معتبر نمی باشد.");
            }

            CmbIsItWorking.SelectedValue = myDossierSacrificeAppearance.IsItWorking.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
                                      {
                                          TableName = TableName,
                                          PrimaryKey = PrimaryKey,
                                          RadGrid = grdDossierSacrifice_CreatingAreasOfEmployment,
                                          PageSize = pageSize,
                                          CurrentPage = currentpPage,
                                          WhereClause = ViewState["WhereClause"].ToString(),
                                          OrderBy = ViewState["SortExpression"].ToString(),
                                          SortType = ViewState["SortType"].ToString(),
                                          RowSelectGuidId = rowSelectGuidId
                                      };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {           
            cmbOrganization.Items.Clear();
            cmbOrganization.Items.Add(new RadComboBoxItem(""));
            cmbOrganization.DataSource = _organizationBl.GetAllIsActive();
            cmbOrganization.DataTextField = "OrganizationPersianTitle";
            cmbOrganization.DataValueField = "OrganizationId";
            cmbOrganization.DataBind();

            CmbDependentType.Items.Clear();
            CmbDependentType.Items.Add(new RadComboBoxItem(""));
            CmbDependentType.DataSource = _dependentType.GetAllIsActive();
            CmbDependentType.DataTextField = "DependentTypeTitle";
            CmbDependentType.DataValueField = "DependentTypeId";
            CmbDependentType.DataBind();
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void Validation()
        {
            if (SelectPerson.KeyId=="")
            {
                throw new ItcApplicationErrorManagerException("فرد استخدام شده را انتهاب نمایید");
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Validation();
                Guid dossierSacrificeEducationId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();
                var dependentTypeId = (CmbDependentType.SelectedIndex > 0) ? Guid.Parse(CmbDependentType.SelectedValue):(Guid?)null;
                var dossierSacrificeTestimonyOrDeathEntity = new DossierSacrifice_CreatingAreasOfEmploymentEntity
                                                                 {
                                                                     DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                                     Description = txtDescription.Text,
                                                                     EmploymentDate = ItcToDate.ShamsiToMiladi(txtEmploymentDate.Text),
                                                                     IsItWorking = bool.Parse(CmbIsItWorking.SelectedItem.Value),
                                                                     DependentTypeId = dependentTypeId,
                                                                     EmployedPersonId = Guid.Parse(SelectPerson.KeyId),
                                                                     OrganizationId = Guid.Parse(cmbOrganization.SelectedValue),
                                                                 };

                _dossierSacrificeTestimonyOrDeathBL.Add(dossierSacrificeTestimonyOrDeathEntity, out dossierSacrificeEducationId);

                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, dossierSacrificeEducationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (CmbIsItWorking.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and IsItWorking=" + CmbIsItWorking.SelectedValue;
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description=" + txtDescription.Text.Trim();
                if (txtEmploymentDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EmploymentDate =" + txtEmploymentDate.Text.Trim();
                if (SelectPerson.KeyId.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EmployedPersonId ='" + new Guid(SelectPerson.KeyId) + "'";
                if (cmbOrganization.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationId ='" + new Guid(cmbOrganization.SelectedValue) + "'";
                if (CmbDependentType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DependentTypeId ='" + new Guid(CmbDependentType.SelectedValue) + "'";
                if (CmbIsItWorking.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and IsItWorkingId ='" + new Guid(CmbIsItWorking.SelectedValue) + "'";

                grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_CreatingAreasOfEmployment.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                Validation();
                var dependentTypeId = (CmbDependentType.SelectedIndex > 0) ? Guid.Parse(CmbDependentType.SelectedValue) : (Guid?)null;
                var dossierSacrificeStudentEntity = new DossierSacrifice_CreatingAreasOfEmploymentEntity()
                                                        {
                                                            DossierSacrifice_CreatingAreasOfEmploymentId = Guid.Parse(ViewState["DossierSacrifice_CreatingAreasOfEmploymentId"].ToString()),
                                                            DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                                                            Description = txtDescription.Text,
                                                            EmploymentDate = ItcToDate.ShamsiToMiladi(txtEmploymentDate.Text),
                                                            IsItWorking = bool.Parse(CmbIsItWorking.SelectedItem.Value),
                                                            DependentTypeId = dependentTypeId,
                                                            EmployedPersonId = Guid.Parse(SelectPerson.KeyId),
                                                            OrganizationId = Guid.Parse(cmbOrganization.SelectedValue),
                                                        };
                _dossierSacrificeTestimonyOrDeathBL.Update(dossierSacrificeStudentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_CreatingAreasOfEmploymentId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_CreatingAreasOfEmployment_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_CreatingAreasOfEmploymentId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";


                    var dossierSacrificeEducationEntity = new DossierSacrifice_CreatingAreasOfEmploymentEntity()
                                                              {
                                                                  DossierSacrifice_CreatingAreasOfEmploymentId = new Guid(e.CommandArgument.ToString()),

                                                              };
                    _dossierSacrificeTestimonyOrDeathBL.Delete(dossierSacrificeEducationEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDossierSacrifice_CreatingAreasOfEmployment_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdDossierSacrifice_CreatingAreasOfEmployment_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdDossierSacrifice_CreatingAreasOfEmployment_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_CreatingAreasOfEmployment.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #endregion
    }
}