﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation
{
    /// <summary>
    // Author:		 <Narges.Kamran>
    // Create date: <1393/01/30>
    // Description:	<اطلاعات جانبازی>
    /// </summary>
    public partial class DossierSacrifice_VeteranPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly CityBL _cityBl = new CityBL();
        private readonly ProvinceBL _provinceBl = new ProvinceBL();
        private readonly WarZoneBL _warZoneBL = new WarZoneBL();
        private readonly EducationDegreeBL _educationDegreeBl = new EducationDegreeBL();
        private readonly VeteranBodyRegionBL _veteranBodyRegionBL = new VeteranBodyRegionBL();
        private readonly TestimonyPeriodTimeBL _testimonyPeriodTimeBL = new TestimonyPeriodTimeBL();
        private readonly MilitaryUnitDispatcherBL _militaryUnitDispatcherBL = new MilitaryUnitDispatcherBL();
        private readonly SacrificePeriodTimeBL _sacrificePeriodTimeBL = new SacrificePeriodTimeBL();
        private readonly PostBL _postBL = new PostBL();
        private readonly DossierSacrifice_VeteranBL _dossierSacrificeVeteranBL = new DossierSacrifice_VeteranBL();

        private const string TableName = "Isar.v_DossierSacrifice_Veteran";
        private const string PrimaryKey = "DossierSacrifice_VeteranId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEndDate.Text = "";
            txtDescription.Text = "";
            //txtStartDate.Text = "";
            txtDurationOfPresenceInBattlefield.Text = "";
            txtRehabilitationEquipment.Text = "";
            txtVeteranCod.Text = "";
            txtVeteranDate.Text = "";
            txtVeteranFoundationDossierNo.Text = "";
            txtVeteranPercent.Text = "";            
            cmbMilitaryUnitDispatcher.ClearSelection();
            cmbCity.ClearSelection();
            //cmbPreVeteranEucationDegree.ClearSelection();
            cmbSacrificePeriodTime.ClearSelection();
            cmbprovince.ClearSelection();
            cmbVeteranBodyRegion.ClearSelection();
            cmbWarZone.ClearSelection();
            cmbprovince.ClearSelection();
            //CmbEucationDegree.ClearSelection();
            CmbPost.ClearSelection();
            //CmbEucationDegree.ClearSelection();
            SelectControlOrganizationPhysicalChart.KeyId = "";
            SelectControlOrganizationPhysicalChart.Title = "";

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeVeteranEntity = new DossierSacrifice_VeteranEntity()
            {
                DossierSacrifice_VeteranId = new Guid(ViewState["DossierSacrifice_VeteranId"].ToString())
            };
            var mydossierSacrificeVeteran = _dossierSacrificeVeteranBL.GetSingleById(dossierSacrificeVeteranEntity);
            txtEndDate.Text = mydossierSacrificeVeteran.EndDate;
            txtDescription.Text = mydossierSacrificeVeteran.EndDate;
            //txtStartDate.Text = mydossierSacrificeVeteran.StartDate;
            txtEndDate.Text = mydossierSacrificeVeteran.EndDate;
            txtRehabilitationEquipment.Text = mydossierSacrificeVeteran.RehabilitationEquipment;
            txtVeteranCod.Text = mydossierSacrificeVeteran.VeteranCod;
            txtVeteranDate.Text = mydossierSacrificeVeteran.VeteranDate;
            txtVeteranFoundationDossierNo.Text = mydossierSacrificeVeteran.VeteranFoundationDossierNo;
            txtVeteranPercent.Text = mydossierSacrificeVeteran.VeteranPercent.ToString();                       
            cmbMilitaryUnitDispatcher.SelectedValue = mydossierSacrificeVeteran.MilitaryUnitDispatcherId.ToString();
            //if (cmbMilitaryUnitDispatcher.FindItemByValue(mydossierSacrificeVeteran.MilitaryUnitDispatcherId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع پذیرش انتخاب شده معتبر نمی باشد.");
            //}
            cmbprovince.SelectedValue = mydossierSacrificeVeteran.ProvinceId.ToString();
            //if (cmbprovince.FindItemByValue(mydossierSacrificeVeteran.ProvinceId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد استان انتخاب شده معتبر نمی باشد.");
            //}
            if (mydossierSacrificeVeteran.ProvinceId.ToString() != "")
            {
                SetCmboboxCity(Guid.Parse(mydossierSacrificeVeteran.ProvinceId.ToString()));
                cmbCity.SelectedValue = mydossierSacrificeVeteran.CityId.ToString();
                //if (cmbCity.FindItemByValue(mydossierSacrificeVeteran.CityId.ToString()) == null)
                //{
                //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد شهر انتخاب شده معتبر نمی باشد.");
                //}
            }

            //CmbEucationDegree.SelectedValue = mydossierSacrificeVeteran.EucationDegreeId.ToString();
            //if (CmbEucationDegree.FindItemByValue(mydossierSacrificeVeteran.EucationDegreeId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مقطع تحصیلی انتخاب شده معتبر نمی باشد.");
            //}
            //cmbPreVeteranEucationDegree.SelectedValue = mydossierSacrificeVeteran.PreVeteranEucationDegreeId.ToString();
            //if (cmbPreVeteranEucationDegree.FindItemByValue(mydossierSacrificeVeteran.PreVeteranEucationDegreeId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مقطع تحصیلی انتخاب شده معتبر نمی باشد.");
            //}
          
          

            cmbSacrificePeriodTime.SelectedValue = mydossierSacrificeVeteran.SacrificePeriodTimeId.ToString();
            //if (cmbSacrificePeriodTime.FindItemByValue(mydossierSacrificeVeteran.SacrificePeriodTimeId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد دوره زمانی انتخاب شده معتبر نمی باشد.");
            //}

            cmbVeteranBodyRegion.SelectedValue = mydossierSacrificeVeteran.VeteranBodyRegionId.ToString();
            //if (cmbVeteranBodyRegion.FindItemByValue(mydossierSacrificeVeteran.VeteranBodyRegionId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد ناحیه جانبازی بدن انتخاب شده معتبر نمی باشد.");
            //}

            cmbWarZone.SelectedValue = mydossierSacrificeVeteran.WarZoneId.ToString();
            //if (cmbWarZone.FindItemByValue(mydossierSacrificeVeteran.WarZoneId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد مناطق جنگی انتخاب شده معتبر نمی باشد.");
            //}

            CmbPost.SelectedValue = mydossierSacrificeVeteran.PostId.ToString();
            //if (CmbPost.FindItemByValue(mydossierSacrificeVeteran.PostId.ToString()) == null)
            //{
            //    CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد پست انتخاب شده معتبر نمی باشد.");
            //}
            SelectControlOrganizationPhysicalChart.KeyId = mydossierSacrificeVeteran.OrganizationPhysicalChartId.ToString();
            SelectControlOrganizationPhysicalChart.Title = mydossierSacrificeVeteran.OrganizationPhysicalChartTitle;
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDossierSacrifice_Veteran,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbMilitaryUnitDispatcher.Items.Clear();
            cmbMilitaryUnitDispatcher.Items.Add(new RadComboBoxItem(""));
            cmbMilitaryUnitDispatcher.DataSource = _militaryUnitDispatcherBL.GetAllIsActive();
            cmbMilitaryUnitDispatcher.DataTextField = "MilitaryUnitDispatcherTitle";
            cmbMilitaryUnitDispatcher.DataValueField = "MilitaryUnitDispatcherId";
            cmbMilitaryUnitDispatcher.DataBind();

            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBl.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();

            //cmbPreVeteranEucationDegree.Items.Clear();
            //cmbPreVeteranEucationDegree.Items.Add(new RadComboBoxItem(""));
            //cmbPreVeteranEucationDegree.DataSource = _educationDegreeBl.GetAllIsActive();
            //cmbPreVeteranEucationDegree.DataTextField = "EucationDegreePersianTitle";
            //cmbPreVeteranEucationDegree.DataValueField = "EucationDegreeId";
            //cmbPreVeteranEucationDegree.DataBind();

            //CmbEucationDegree.Items.Clear();
            //CmbEucationDegree.Items.Add(new RadComboBoxItem(""));
            //CmbEucationDegree.DataSource = _educationDegreeBl.GetAllIsActive();
            //CmbEucationDegree.DataTextField = "EucationDegreePersianTitle";
            //CmbEucationDegree.DataValueField = "EucationDegreeId";
            //CmbEucationDegree.DataBind();

            cmbSacrificePeriodTime.Items.Clear();
            cmbSacrificePeriodTime.Items.Add(new RadComboBoxItem(""));
            cmbSacrificePeriodTime.DataSource = _sacrificePeriodTimeBL.GetAllIsActive();
            cmbSacrificePeriodTime.DataTextField = "SacrificePeriodTimeTitle";
            cmbSacrificePeriodTime.DataValueField = "SacrificePeriodTimeId";
            cmbSacrificePeriodTime.DataBind();

            cmbVeteranBodyRegion.Items.Clear();
            cmbVeteranBodyRegion.Items.Add(new RadComboBoxItem(""));
            cmbVeteranBodyRegion.DataTextField = "VeteranBodyRegionTitle";
            cmbVeteranBodyRegion.DataValueField = "VeteranBodyRegionId";
            cmbVeteranBodyRegion.DataSource = _veteranBodyRegionBL.GetAllIsActive();
            cmbVeteranBodyRegion.DataBind();

            cmbWarZone.Items.Clear();
            cmbWarZone.Items.Add(new RadComboBoxItem(""));
            cmbWarZone.DataTextField = "WarZoneTitle";
            cmbWarZone.DataValueField = "WarZoneId";
            cmbWarZone.DataSource = _warZoneBL.GetAllIsActive();
            cmbWarZone.DataBind();

            CmbPost.Items.Clear();
            CmbPost.Items.Add(new RadComboBoxItem(""));
            CmbPost.DataTextField = "PostTitle";
            CmbPost.DataValueField = "PostId";
            CmbPost.DataSource = _postBL.GetAllIsActive();
            CmbPost.DataBind();
        }

        private void SetCmboboxCity(Guid ProvinceId)
        {
            var cityEntity = new CityEntity { ProvinceId = ProvinceId };
            cmbCity.Items.Clear();
            cmbCity.Items.Add(new RadComboBoxItem(""));
            cmbCity.DataTextField = "CityTitlePersian";
            cmbCity.DataValueField = "CityId";
            cmbCity.DataSource = _cityBl.GetCityCollectionByProvince(cityEntity);
            cmbCity.DataBind();
        }



        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DossierSacrifice_VeteranId;
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                SetPanelFirst();

                var dossierSacrificeVeteranEntity = new DossierSacrifice_VeteranEntity()
                {
                    
                    CityId = (cmbCity.SelectedIndex > 0 ? Guid.Parse(cmbCity.SelectedValue) : new Guid()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),                    
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),                    
                    StartDate = "",
                    Description = txtDescription.Text,
                    DurationOfPresenceInBattlefield = (txtDurationOfPresenceInBattlefield.Text==""?(int?)null:int.Parse(txtDurationOfPresenceInBattlefield.Text)),
                    EucationDegreeId = (new Guid()),
                    MilitaryUnitDispatcherId = (cmbMilitaryUnitDispatcher.SelectedIndex > 0 ? Guid.Parse(cmbMilitaryUnitDispatcher.SelectedValue) : new Guid()), 
                    OrganizationPhysicalChartId = (SelectControlOrganizationPhysicalChart.KeyId==""?new Guid() :Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId) ),
                    PostId = (CmbPost.SelectedIndex > 0 ? Guid.Parse(CmbPost.SelectedValue) : new Guid()),
                    PreVeteranEucationDegreeId = (new Guid()),
                    RehabilitationEquipment = txtRehabilitationEquipment.Text,
                    SacrificePeriodTimeId = (cmbSacrificePeriodTime.SelectedIndex > 0 ? Guid.Parse(cmbSacrificePeriodTime.SelectedValue) : new Guid()),
                    VeteranBodyRegionId = (cmbVeteranBodyRegion.SelectedIndex > 0 ? Guid.Parse(cmbVeteranBodyRegion.SelectedValue) : new Guid()),
                    VeteranCod = txtVeteranCod.Text,
                    VeteranDate = ItcToDate.ShamsiToMiladi(txtVeteranDate.Text),
                    VeteranFoundationDossierNo = txtVeteranFoundationDossierNo.Text,
                    VeteranPercent = (txtVeteranPercent.Text==""?(decimal?)null:decimal.Parse(txtVeteranPercent.Text)),
                    WarZoneId = (cmbWarZone.SelectedIndex > 0 ? Guid.Parse(cmbWarZone.SelectedValue) : new Guid()),                    
          
                };

                _dossierSacrificeVeteranBL.Add(dossierSacrificeVeteranEntity, out DossierSacrifice_VeteranId );
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, DossierSacrifice_VeteranId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtDurationOfPresenceInBattlefield.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DurationOfPresenceInBattlefield Like N'%" +
                                               FarsiToArabic.ToArabic(txtDurationOfPresenceInBattlefield.Text.Trim()) + "%'";
                if (txtRehabilitationEquipment.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ClassifiedNo Like N'%" +
                                               Decimal.Parse(txtRehabilitationEquipment.Text.Trim()) + "%'";

                //if (txtStartDate.Text.Trim() != "")
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StartDate ='" +
                //                              (txtStartDate.Text.Trim()) + "'";
                if (txtEndDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDate ='" +
                                              (txtEndDate.Text.Trim()) + "'";
                if (txtVeteranDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VeteranDate ='" +
                                              (txtVeteranDate.Text.Trim()) + "'";
                if (txtVeteranFoundationDossierNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VeteranFoundationDossierNo ='" +
                                              (txtVeteranFoundationDossierNo.Text.Trim()) + "'";
                if (txtVeteranCod.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VeteranCod Like N'%" +
                                               FarsiToArabic.ToArabic(txtVeteranCod.Text.Trim()) + "%'";
                if (txtVeteranPercent.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VeteranPercent ='" +
                                              (txtVeteranPercent.Text.Trim()) + "'";
                                              
               
                if (cmbMilitaryUnitDispatcher.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MilitaryUnitDispatcherId='" +
                                               new Guid(cmbMilitaryUnitDispatcher.SelectedValue) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and provinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                //if (cmbPreVeteranEucationDegree.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PreVeteranEucationDegreeId ='" +
                //                               new Guid(cmbPreVeteranEucationDegree.SelectedValue) + "'";
                if (cmbSacrificePeriodTime.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SacrificePeriodTimeId ='" +
                                               new Guid(cmbSacrificePeriodTime.SelectedValue) + "'";
                //if (CmbEucationDegree.SelectedIndex > 0)
                //    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EucationDegreeId ='" +
                //                               new Guid(CmbEucationDegree.SelectedValue) + "'";
                if (cmbWarZone.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WarZoneId ='" +
                                               new Guid(cmbWarZone.SelectedValue) + "'";
                if (cmbVeteranBodyRegion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and VeteranBodyRegionId ='" +
                                               new Guid(cmbVeteranBodyRegion.SelectedValue) + "'";

                if (CmbPost.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PostId ='" +
                                               new Guid(CmbPost.SelectedValue) + "'";

                grdDossierSacrifice_Veteran.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDossierSacrifice_Veteran.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {                
                ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";     
                var dossierSacrificeVeteranEntity = new DossierSacrifice_VeteranEntity()
                {
                    DossierSacrifice_VeteranId = Guid.Parse(ViewState["DossierSacrifice_VeteranId"].ToString()),
                    CityId = (cmbCity.SelectedIndex > 0 ? Guid.Parse(cmbCity.SelectedValue) : new Guid()),
                    DossierSacrificeId = Guid.Parse(Session["DossierSacrificeId"].ToString()),
                    EndDate = ItcToDate.ShamsiToMiladi(txtEndDate.Text),
                    StartDate = "",
                    Description = txtDescription.Text,
                    DurationOfPresenceInBattlefield = (txtDurationOfPresenceInBattlefield.Text == "" ? (int?)null : int.Parse(txtDurationOfPresenceInBattlefield.Text)),
                    EucationDegreeId = (new Guid()),
                    MilitaryUnitDispatcherId = (cmbMilitaryUnitDispatcher.SelectedIndex > 0 ? Guid.Parse(cmbMilitaryUnitDispatcher.SelectedValue) : new Guid()),
                    OrganizationPhysicalChartId = (SelectControlOrganizationPhysicalChart.KeyId == "" ? new Guid() : Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId)),
                    PostId = (CmbPost.SelectedIndex > 0 ? Guid.Parse(CmbPost.SelectedValue) : new Guid()),
                    PreVeteranEucationDegreeId = (new Guid()),
                    RehabilitationEquipment = txtRehabilitationEquipment.Text,
                    SacrificePeriodTimeId = (cmbSacrificePeriodTime.SelectedIndex > 0 ? Guid.Parse(cmbSacrificePeriodTime.SelectedValue) : new Guid()),
                    VeteranBodyRegionId = (cmbVeteranBodyRegion.SelectedIndex > 0 ? Guid.Parse(cmbVeteranBodyRegion.SelectedValue) : new Guid()),
                    VeteranCod = txtVeteranCod.Text,
                    VeteranDate = ItcToDate.ShamsiToMiladi(txtVeteranDate.Text),
                    VeteranFoundationDossierNo = txtVeteranFoundationDossierNo.Text,
                    VeteranPercent = (txtVeteranPercent.Text == "" ? (decimal?)null : decimal.Parse(txtVeteranPercent.Text)),
                    WarZoneId = (cmbWarZone.SelectedIndex > 0 ? Guid.Parse(cmbWarZone.SelectedValue) : new Guid()),

                };

                _dossierSacrificeVeteranBL.Update(dossierSacrificeVeteranEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, new Guid(ViewState["DossierSacrifice_VeteranId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdDossierSacrifice_Veteran_ItemCommand(object sender, GridCommandEventArgs e)
        {

            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DossierSacrifice_VeteranId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = " DossierSacrificeId='" + Guid.Parse(Session["DossierSacrificeId"].ToString()) + "'";
                    var dossierSacrificeVeteranEntity = new DossierSacrifice_VeteranEntity()
                    {
                        DossierSacrifice_VeteranId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _dossierSacrificeVeteranBL.Delete(dossierSacrificeVeteranEntity);
                    var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
                if (e.CommandName == "_MyCopy")
                {
                    ViewState["DossierSacrifice_VeteranId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelFirst();
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>



        protected void grdDossierSacrifice_Veteran_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdDossierSacrifice_Veteran_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDossierSacrifice_Veteran_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDossierSacrifice_Veteran.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// لود کردن شهر براساس شناسه استان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Value))
            {
                cmbCity.Text = "";
                cmbCity.Items.Clear();
                SetCmboboxCity(Guid.Parse(e.Value.ToString()));    
            }
            else
            {
                cmbCity.Text = "";
                cmbCity.Items.Clear();
            }
            
        }
        #endregion


    }
}