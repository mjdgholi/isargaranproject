﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_TestimonyOrDeathPage.ascx.cs"
    Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.SacrificeInformation.DossierSacrifice_TestimonyOrDeathPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"
                                    ValidationGroup="TestimonyOrDeath">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"
                                    ValidationGroup="TestimonyOrDeath">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"
                                    CausesValidation="False">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" valign="top">
                <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="LblCauseEventType" runat="server"> علت حادثه<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="20%">
                                        <cc1:CustomRadComboBox ID="cmbCauseEventType" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="5%">
                                   <asp:RequiredFieldValidator ID="rfvCauseEventType" runat="server" 
                                    ControlToValidate="cmbCauseEventType" ErrorMessage="*" 
                                    ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator></td>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="LblTestimonyPeriodTime0" runat="server"> تاریخ وفات یا شهادت<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="20%">
                                <cc1:CustomItcCalendar ID="TxtDaethDate" runat="server"></cc1:CustomItcCalendar>
                            </td>
                            <td width="35%">
                                <asp:RequiredFieldValidator ID="rfvDeathDate" runat="server" 
                                    ControlToValidate="TxtDaethDate" ErrorMessage="*" 
                                    ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" >
                                <asp:Label CssClass="LabelCSS" ID="lblOrganizationPhysicalChart" runat="server">محل خدمت سازمانی قبل از شهادت :</asp:Label>
                            </td>
                            <td >
                                <cc1:SelectControl ID="SelectControlOrganizationPhysicalChart" runat="server" 
                                    MultiSelect="False"  imageName="OrganizationPhysicalChart"
                                    PortalPathUrl="GeneralProject/General/ModalForm/OrganizationPhysicalChartPage.aspx" 
                                    RadWindowHeight="700" RadWindowWidth="950" ToolTip="ساختار سازمانی" />
                            </td>
                            <td >
                                &nbsp;</td>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblPost" runat="server"> پست قبل از شهادت:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbPost" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
<%--                        <tr>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblEducationCourse" runat="server">رشته تحصیلی قبل از شهادت<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="25%">
                                <cc1:CustomRadComboBox ID="CmbEducationCourse" runat="server" 
                                    AppendDataBoundItems="True" CausesValidation="False" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="15%">
                                <asp:RequiredFieldValidator ID="rfvEducationCourse" runat="server" 
                                    ControlToValidate="CmbEducationCourse" ErrorMessage="*" 
                                    ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator>
                            </td>
                            <td nowrap="nowrap" width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblEucationDegree" runat="server"> مقطع تحصیلی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="25%">
                                <cc1:CustomRadComboBox ID="CmbEucationDegree" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="15%">
                                <asp:RequiredFieldValidator ID="rfvEucationDegree" runat="server" 
                                    ControlToValidate="CmbEucationDegree" ErrorMessage="*" 
                                    ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblprovince" runat="server">استان محل تدفین<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbprovince" runat="server" AppendDataBoundItems="True" Height="300"
                                    AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cmbprovince_SelectedIndexChanged" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="cmbprovince"
                                    ErrorMessage="*" ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblCity" runat="server"> شهر محل تدفین<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbCity" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="300">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="CmbCity"
                                    ErrorMessage="*" ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblVillageTitle" runat="server">روستا محل تدفین:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtVillageTitle" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblMilitaryUnitDispatcher" runat="server"> یگان اعزام کننده<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbMilitaryUnitDispatcher" runat="server" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" 
                                    AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvMilitaryUnitDispatcher" runat="server" 
                                    ControlToValidate="CmbMilitaryUnitDispatcher" ErrorMessage="*" 
                                    ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblIsWarDisappeared" runat="server">آیا مفقود الاثر است:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbIsWarDisappeared" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Owner="cmbIsWarDisappeared" Text="" Value="" />
                                        <telerik:RadComboBoxItem Owner="cmbIsWarDisappeared" Text="بلی" Value="True" />
                                        <telerik:RadComboBoxItem Owner="cmbIsWarDisappeared" Text="خیر" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="LblTestimonyPeriodTime" runat="server"> دوره زمانی شهادت<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="CmbTestimonyPeriodTime" runat="server" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" 
                                    AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvCmbTestimonyPeriodTime" runat="server" 
                                    ControlToValidate="CmbTestimonyPeriodTime" ErrorMessage="*" 
                                    ValidationGroup="TestimonyOrDeath"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="LblTestimonyLocation" runat="server">محل شهادت:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtTestimonyLocation" runat="server" MaxLength="10">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="LblPartNo" runat="server">شماره قطعه:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="txtPartNo" runat="server" 
                                    DataType="System.Int32">
                                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblRowNo" runat="server">شماره ردیف:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="txtRowNo" runat="server" DataType="System.Int32">
                                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="LblGraveNo" runat="server">شماره قبر:</asp:Label>
                            </td>
                            <td dir="rtl">
                                <telerik:RadNumericTextBox ID="txtGraveNo" runat="server" 
                                    DataType="System.Int32">
                                    <NumberFormat DecimalDigits="0" ZeroPattern="n" />
                                </telerik:RadNumericTextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblTombTitle" runat="server">نام آرامگاه:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtTombTitle" runat="server">
                                </telerik:RadTextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdDossierSacrifice_TestimonyOrDeath" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdDossierSacrifice_TestimonyOrDeath_ItemCommand"
                                    OnPageIndexChanged="grdDossierSacrifice_TestimonyOrDeath_PageIndexChanged" OnPageSizeChanged="grdDossierSacrifice_TestimonyOrDeath_PageSizeChanged"
                                    OnSortCommand="grdDossierSacrifice_TestimonyOrDeath_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_TestimonyOrDeathId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>                                          
                                            <telerik:GridBoundColumn DataField="OrganizationPhysicalChartTitle" HeaderText="محل خدمت سازمانی قبل از شهادت"
                                                SortExpression="OrganizationPhysicalChartTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PostTitle" HeaderText="پست قبل از شهادت" SortExpression="PostTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CauseEventTypeTitle" HeaderText="علت حادثه"
                                                SortExpression="CauseEventTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MilitaryUnitDispatcherTitle" HeaderText="یگان اعزام کننده"
                                                SortExpression="MilitaryUnitDispatcherTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CityTitlePersian" HeaderText="شهر" SortExpression="CityTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DaethDateShamsi" HeaderText="تاریخ شهادت" SortExpression="DaethDateShamsi">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CityTitlePersian" HeaderText="محل شهادت" SortExpression="CityTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PartNo" HeaderText="شماره قطعه" SortExpression="PartNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                               <telerik:GridTemplateColumn HeaderText="مدیریت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                <cc1:SelectControl ID="SelectControlUploadFile" runat="server" Width="1" WhereClause='<%#Eval("DossierSacrifice_TestimonyOrDeathId").ToString() %>' PortalPathUrl="EArchiveProject/EArchive/ModalForm/EDocumentContentFileGeneralPage.aspx"  imageName="Upload"> </cc1:SelectControl>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                 </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_TestimonyOrDeathId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("DossierSacrifice_TestimonyOrDeathId").ToString() %>'
                                                        CommandName="_MyِDelete" ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbEducationCourse">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="CmbMilitaryUnitDispatcher" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="cmbprovince">
            <UpdatedControls>
   
               
                <telerik:AjaxUpdatedControl ControlID="cmbprovince" />
                <telerik:AjaxUpdatedControl ControlID="cmbCity" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdDossierSacrifice_TestimonyOrDeath">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
