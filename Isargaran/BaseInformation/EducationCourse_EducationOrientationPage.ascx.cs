﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/12>
    /// Description: <رشته تحصیلی -گرایش تحصیلی >
    /// </summary>
    public partial class EducationCourse_EducationOrientationPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly EducationCourseBL _educationCourseBl = new EducationCourseBL();
        private readonly EducationOrientationBL _educationOrientationBl = new EducationOrientationBL();
        private readonly EducationCourse_EducationOrientationBL _educationCourseEducationOrientationBl= new EducationCourse_EducationOrientationBL();
        private const string TableName = "Isar.V_EducationCourse_EducationOrientation";
        private const string PrimaryKey = "EducationCourse_EducationOrientationId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            //cmbEducationCourse.ClearSelection();
            cmbEducationOreintion.ClearSelection();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var educationCourseEducationOrientationEntity = new EducationCourse_EducationOrientationEntity()
            {
                EducationCourse_EducationOrientationId =
                    Guid.Parse(ViewState["EducationCourse_EducationOrientationId"].ToString())
            };
            var myeducationCourseEducationOrientation = _educationCourseEducationOrientationBl.GetSingleById(educationCourseEducationOrientationEntity);
  
            cmbIsActive.SelectedValue = myeducationCourseEducationOrientation.IsActive.ToString();
            cmbEducationCourse.SelectedValue = myeducationCourseEducationOrientation.EducationCourseId.ToString();
            if (cmbEducationCourse.FindItemByValue(myeducationCourseEducationOrientation.EducationCourseId.ToString())==null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد رشته انتخاب شده معتبر نمی باشد.");
            }
            cmbEducationOreintion.SelectedValue = myeducationCourseEducationOrientation.EducationOreintionId.ToString();
            if (cmbEducationOreintion.FindItemByValue(myeducationCourseEducationOrientation.EducationOreintionId.ToString())==null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد گرایش انتخاب شده معتبر نمی باشد.");
            }

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdEducationCourse_EducationOrientation,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbEducationCourse.Items.Clear();
            cmbEducationCourse.Items.Add(new RadComboBoxItem(""));
            cmbEducationCourse.DataSource = _educationCourseBl.GetAllIsActive();
            cmbEducationCourse.DataTextField = "EducationCoursePersianTitle";
            cmbEducationCourse.DataValueField = "EducationCourseId";
            cmbEducationCourse.DataBind();


            cmbEducationOreintion.Items.Clear();
            cmbEducationOreintion.Items.Add(new RadComboBoxItem(""));
            cmbEducationOreintion.DataSource = _educationOrientationBl.GetAllIsActive();
            cmbEducationOreintion.DataTextField = "EducationOrientationPersianTitle";
            cmbEducationOreintion.DataValueField = "EducationOrientationId";
            cmbEducationOreintion.DataBind();

        }

        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid EducationCourse_EducationOrientationId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var educationCourseEducationOrientationEntity = new EducationCourse_EducationOrientationEntity()
                {
                    EducationCourseId = Guid.Parse(cmbEducationCourse.SelectedValue),
                    EducationOreintionId = Guid.Parse(cmbEducationOreintion.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    

                };

                _educationCourseEducationOrientationBl.Add(educationCourseEducationOrientationEntity, out EducationCourse_EducationOrientationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, EducationCourse_EducationOrientationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";

                if (cmbEducationCourse.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCourseId ='" +
                                               new Guid(cmbEducationCourse.SelectedValue) + "'";
                if (cmbEducationOreintion.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationOreintionId ='" +
                                               new Guid(cmbEducationOreintion.SelectedValue) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdEducationCourse_EducationOrientation.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdEducationCourse_EducationOrientation.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = "";
                var educationCourseEducationOrientationEntity = new EducationCourse_EducationOrientationEntity()
                {
                    EducationCourse_EducationOrientationId = Guid.Parse(ViewState["EducationCourse_EducationOrientationId"].ToString()),
                    EducationCourseId = Guid.Parse(cmbEducationCourse.SelectedValue),
                    EducationOreintionId = Guid.Parse(cmbEducationOreintion.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };
                _educationCourseEducationOrientationBl.Update(educationCourseEducationOrientationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, new Guid(ViewState["EducationCourse_EducationOrientationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>       
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdEducationCourse_EducationOrientation_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["EducationCourse_EducationOrientationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                var educationCourseEducationOrientationEntity = new EducationCourse_EducationOrientationEntity()
                {
                    EducationCourse_EducationOrientationId = Guid.Parse(e.CommandArgument.ToString())      
                };
                    _educationCourseEducationOrientationBl.Delete(educationCourseEducationOrientationEntity);
                    var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdEducationCourse_EducationOrientation_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdEducationCourse_EducationOrientation_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdEducationCourse_EducationOrientation_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdEducationCourse_EducationOrientation.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}