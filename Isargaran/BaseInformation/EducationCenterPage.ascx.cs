﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/11>
    /// Description: <مراکزتحصیلی>
    /// </summary>
    public partial class EducationCenterPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly EducationCenterTypeBL _educationCenterTypeBL = new EducationCenterTypeBL();
        private readonly EducationCenterBL _educationCenterBL = new EducationCenterBL();
        private const string TableName = "Isar.V_EducationCenter";
        private const string PrimaryKey = "EducationCenterId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEducationCenterEnglishTitle.Text = "";
            txtEducationCenterPersianTitle.Text = "";
            txtAddress.Text = "";
            txtEmail.Text = "";
            txtFaxNo.Text = "";
            txtTelNo.Text = "";
            txtZipCode.Text = "";
            cmbEducationCenterType.ClearSelection();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var educationCenterEntity = new EducationCenterEntity()
                                            {
                                                EducationCenterId = new Guid(ViewState["EducationCenterId"].ToString())
                                            };
            var myeducationCenter = _educationCenterBL.GetSingleById(educationCenterEntity);
            txtEducationCenterEnglishTitle.Text = myeducationCenter.EducationCenterEnglishTitle;
            txtEducationCenterPersianTitle.Text = myeducationCenter.EducationCenterPersianTitle;
            cmbIsActive.SelectedValue = myeducationCenter.IsActive.ToString();            
            txtEmail.Text = myeducationCenter.Email;
            txtAddress.Text = myeducationCenter.Address;
            txtFaxNo.Text = myeducationCenter.FaxNo;
            txtTelNo.Text = myeducationCenter.TelNo;
            txtZipCode.Text = myeducationCenter.ZipCode;
            cmbEducationCenterType.SelectedValue = myeducationCenter.EducationCenterTypeId.ToString();
            if (cmbEducationCenterType.FindItemByValue(myeducationCenter.EducationCenterTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردنوع مراکز تحصیلی انتخاب شده معتبر نمی باشد.");
            }


        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdEducationCenter,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbEducationCenterType.Items.Clear();
            cmbEducationCenterType.Items.Add(new RadComboBoxItem(""));
            cmbEducationCenterType.DataSource = _educationCenterTypeBL.GetAllIsActive();
            cmbEducationCenterType.DataTextField = "EducationCenterTypePersianTitle";
            cmbEducationCenterType.DataValueField = "EducationCenterTypeId";
            cmbEducationCenterType.DataBind();



        }

        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid EducationCenterId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();
            var educationCenterEntity = new EducationCenterEntity()
                                            {
                                                EducationCenterPersianTitle = txtEducationCenterPersianTitle.Text,
                                                EducationCenterEnglishTitle = txtEducationCenterEnglishTitle.Text,
                                                Email = txtEmail.Text,
                                                FaxNo = txtFaxNo.Text,
                                                Address = txtAddress.Text,
                                                TelNo = txtTelNo.Text,
                                                ZipCode = txtZipCode.Text,
                                                IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                                                EducationCenterTypeId = Guid.Parse(cmbEducationCenterType.SelectedValue),
                                                
                                                
                                            };

            _educationCenterBL.Add(educationCenterEntity, out EducationCenterId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, EducationCenterId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtEducationCenterEnglishTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCenterEnglishTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtEducationCenterEnglishTitle.Text.Trim()) + "%'";
                if (txtEducationCenterPersianTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCenterPersianTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtEducationCenterPersianTitle.Text.Trim()) + "%'";
                if (txtEmail.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Email Like N'%" +
                                               FarsiToArabic.ToArabic(txtEmail.Text.Trim()) + "%'";
                if (txtAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Address Like N'%" +
                                               FarsiToArabic.ToArabic(txtAddress.Text.Trim()) + "%'";
                if (txtFaxNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FaxNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtFaxNo.Text.Trim()) + "%'";
                if (txtTelNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and TelNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtTelNo.Text.Trim()) + "%'";
                if (txtZipCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ZipCode Like N'%" +
                                               FarsiToArabic.ToArabic(txtZipCode.Text.Trim()) + "%'";
                if (cmbEducationCenterType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCenterTypeId ='" +
                                               new Guid(cmbEducationCenterType.SelectedValue) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdEducationCenter.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;                
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdEducationCenter.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = "";
                var educationCenterEntity = new EducationCenterEntity()
                {
                    EducationCenterId = Guid.Parse(ViewState["EducationCenterId"].ToString()),
                    EducationCenterPersianTitle = txtEducationCenterPersianTitle.Text,
                    EducationCenterEnglishTitle = txtEducationCenterEnglishTitle.Text,
                    Email = txtEmail.Text,
                    FaxNo = txtFaxNo.Text,
                    Address = txtAddress.Text,
                    TelNo = txtTelNo.Text,
                    ZipCode = txtZipCode.Text,
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    EducationCenterTypeId = Guid.Parse(cmbEducationCenterType.SelectedValue),


                };
                _educationCenterBL.Update(educationCenterEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, new Guid(ViewState["EducationCenterId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdEducationCenter_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["EducationCenterId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var educationCenterEntity = new EducationCenterEntity()
                    {
                        EducationCenterId = Guid.Parse(e.CommandArgument.ToString())


                    };
                    _educationCenterBL.Delete(educationCenterEntity);
                    var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdEducationCenter_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdEducationCenter_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdEducationCenter_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdEducationCenter.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }
        #endregion
    }
}