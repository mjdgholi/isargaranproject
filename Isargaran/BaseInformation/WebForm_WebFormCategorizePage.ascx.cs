﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BaseInformation
{
    /// <summary>
    /// Author:		 <narges.kamran>
    /// Create date: <1393/08/20>
    /// Description: <ارتباطی وب با دسته بندی فرم>
    /// </summary>
    public partial class WebForm_WebFormCategorizePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly WebFormBL _WebFormBL = new WebFormBL();
        private readonly WebFormCategorizeBL _webFormCategorizeBL = new WebFormCategorizeBL();
        private  readonly  WebForm_WebFormCategorizeBL _webFormWebFormCategorizeBL=new WebForm_WebFormCategorizeBL();
        private const string TableName = "Isar.v_WebForm_WebFormCategorize";
        private const string PrimaryKey = "WebForm_WebFormCategorizeId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            cmbWebForm.CheckBoxes = true;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            cmbWebForm.CheckBoxes = false;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtFormPriority.Text = "";
            cmbWebFormCategorize.ClearSelection();
            cmbIsActive.SelectedIndex = 1;
            cmbWebForm.Items.Clear();
            cmbWebForm.Text = "";


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var webFormWebFormCategorizeEntity = new WebForm_WebFormCategorizeEntity()
            {
                WebForm_WebFormCategorizeId = Guid.Parse(ViewState["WebForm_WebFormCategorizeId"].ToString())
            };
            var myWebFormWebFormCategorize = _webFormWebFormCategorizeBL.GetSingleById(webFormWebFormCategorizeEntity);            
            cmbIsActive.SelectedValue = myWebFormWebFormCategorize.IsActive.ToString();            
            cmbWebFormCategorize.SelectedValue = myWebFormWebFormCategorize.WebFormCategorizeId.ToString();
            SetComboWebForm(Guid.Parse(myWebFormWebFormCategorize.WebFormCategorizeId.ToString()),Guid.Parse(myWebFormWebFormCategorize.WebFormId.ToString()));
            cmbWebForm.SelectedValue = myWebFormWebFormCategorize.WebFormId.ToString();
            txtFormPriority.Text = myWebFormWebFormCategorize.FormPriority.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }

        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdWebForm_WebFormCategorize,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

    /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
    /// </summary>
        private void SetWebFormCategorizeComboBox()
        {

            cmbWebFormCategorize.Items.Clear();
            cmbWebFormCategorize.Items.Add(new RadComboBoxItem(""));
            cmbWebFormCategorize.DataSource = _webFormCategorizeBL.GetAllIsActive();
            cmbWebFormCategorize.DataTextField = "WebFormCategorizeTitle";
            cmbWebFormCategorize.DataValueField = "WebFormCategorizeId";
            cmbWebFormCategorize.DataBind();
        }
        private void SetComboWebForm(Guid webFormCategorizeId, Guid WebFormid)
        {
             cmbWebForm.Items.Clear();
            if (!cmbWebForm.CheckBoxes)
            {
                cmbWebForm.Items.Add(new RadComboBoxItem(""));   
            }            
            cmbWebForm.DataSource = _webFormWebFormCategorizeBL.GetWebForm_WebFormCategorizeCollectionByWebForm_WebFormCategorizeWithoutDuplicate(webFormCategorizeId,WebFormid);
            cmbWebForm.DataTextField = "WebFormTitle";
            cmbWebForm.DataValueField = "WebFormId";
            cmbWebForm.DataBind();
           
        }  

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetWebFormCategorizeComboBox();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid webFormWebFormCategorizeId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var webFormWebFormCategorizeEntity = new WebForm_WebFormCategorizeEntity()
                {
                    WebFormCategorizeId = Guid.Parse(cmbWebFormCategorize.SelectedValue),
                    WebFormIdStr = cmbWebForm.KeyId,
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    ImagePath = "",
                    FormPriority = int.Parse(txtFormPriority.Text)
                    
                };

                _webFormWebFormCategorizeBL.Add(webFormWebFormCategorizeEntity, out webFormWebFormCategorizeId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if(cmbWebForm.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And WebFormId='" +
                                               (cmbWebForm.SelectedValue.Trim()) + "'";
                if (cmbWebFormCategorize.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And WebFormCategorizeId='" +
                                               (cmbWebFormCategorize.SelectedValue.Trim()) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdWebForm_WebFormCategorize.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdWebForm_WebFormCategorize.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "";
                var webFormWebFormCategorizeEntity = new WebForm_WebFormCategorizeEntity()
                {
                    WebFormCategorizeId = Guid.Parse(cmbWebFormCategorize.SelectedValue),
                    WebFormId = Guid.Parse(cmbWebForm.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    WebForm_WebFormCategorizeId = Guid.Parse(ViewState["WebForm_WebFormCategorizeId"].ToString()),
                    ImagePath = "",
                    FormPriority = int.Parse(txtFormPriority.Text)
                };

                _webFormWebFormCategorizeBL.Update(webFormWebFormCategorizeEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid(ViewState["WebForm_WebFormCategorizeId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {

            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdWebForm_WebFormCategorize_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["WebForm_WebFormCategorizeId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetPanelLast();
                    SetDataShow();
                    
                }
                if (e.CommandName == "_MyِDelete")
                {
      

                    var webFormWebFormCategorizeEntity = new WebForm_WebFormCategorizeEntity()
                    {
                        WebForm_WebFormCategorizeId = new Guid(e.CommandArgument.ToString()),

                    };
                    _webFormWebFormCategorizeBL.Delete(webFormWebFormCategorizeEntity);
                    var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdWebForm_WebFormCategorize_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdWebForm_WebFormCategorize_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdWebForm_WebFormCategorize_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdWebForm_WebFormCategorize.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        protected void cmbWebFormCategorize_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Value))
            {
                SetComboWebForm(Guid.Parse(e.Value),new Guid());
            }
            else
            {
                cmbWebForm.Items.Clear();
                cmbWebForm.Text = "";
            }
        }



        #endregion

        
    }
}