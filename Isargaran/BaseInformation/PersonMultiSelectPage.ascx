﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonMultiSelectPage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.BaseInformation.PersonMultiSelectPage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadScriptBlock ID="RadScriptBlock" runat="server">
    <script type="text/javascript">
        var myArrayKey = new Array();
        var myArrayTitle = new Array();
        var hiddenfieldvalue = '';
        var hiddenfieldtitle = '';
        var str = '';
        function RowSelected(sender, eventArgs) {

            KeyNamevalue = eventArgs._tableView.get_clientDataKeyNames()[0];
            KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];

            hiddenfieldvalue = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
            hiddenfieldtitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
            var textKeyvalue = eventArgs.getDataKeyValue(KeyNamevalue);
            var txtTitlevalue = (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1]) + ' ' + eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[2])).trim();
            if (include(myArrayKey, textKeyvalue) != true) {
                if (myArrayKey.length > 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + textKeyvalue;
                hiddenfieldtitle = hiddenfieldtitle + str + (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1]) + ' ' + eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[2])).trim();
                myArrayKey.push(textKeyvalue);
                myArrayTitle.push(txtTitlevalue);
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
            document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
        }

        function RowDeselected(sender, eventArgs) {

            str = '';
            hiddenfieldvalue = '';
            hiddenfieldtitle = '';
            KeyName = eventArgs._tableView.get_clientDataKeyNames()[0];
            KeyNameTitle = eventArgs._tableView.get_clientDataKeyNames()[1];
            for (var i = 0; i < myArrayKey.length; i++) {
                if (myArrayKey[i] == eventArgs.getDataKeyValue(KeyName))
                    myArrayKey.splice(i, 1);
            }
            for (var j = 0; j < myArrayTitle.length; j++) {
                if (myArrayTitle[j] == (eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[1]) + ' ' + eventArgs.getDataKeyValue(eventArgs._tableView.get_clientDataKeyNames()[2])).trim())
                    myArrayTitle.splice(j, 1);
            }

            for (var i = 0; i < myArrayKey.length; i++) {
                if (i != 0)
                    str = ',';
                hiddenfieldvalue = hiddenfieldvalue + str + myArrayKey[i];
            }
            str = '';
            for (var j = 0; j < myArrayTitle.length; j++) {
                if (j != 0)
                    str = ',';
                hiddenfieldtitle = hiddenfieldtitle + str + myArrayTitle[j];
            }
            document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value = hiddenfieldvalue;
            document.getElementById('<%=hiddenfieldTitle.ClientID %>').value = hiddenfieldtitle;
        }

        function MasterTableViewCreated(sender, eventArgs) {
            if (document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value != '')
                myArrayKey = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value.split(',');
            if (document.getElementById('<%=hiddenfieldTitle.ClientID %>').value != '')
                myArrayTitle = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value.split(',');
            KeyName = sender.MasterTableView.get_clientDataKeyNames()[0];
            for (var j = 0; j < sender.get_masterTableView().get_dataItems().length; j++)
                for (var i = 0; i < myArrayKey.length; i++) {
                    if (myArrayKey[i] == sender.get_masterTableView().get_dataItems()[j].getDataKeyValue(KeyName))
                        sender.get_masterTableView().get_dataItems()[j].set_selected(true);
                }
        }

        function include(arr, obj) {
            if (arr.length == 0)
                return false;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == obj)
                    return true;
            }
        }

        function CloseRadWindow() {

            var oArg = new Object();
            oArg.Title = document.getElementById('<%=hiddenfieldTitle.ClientID %>').value;
            oArg.KeyId = document.getElementById('<%=hiddenfieldKeyId.ClientID %>').value;
            var oWnd = GetRadWindow();
            oWnd.close(oArg);
        }


        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
</telerik:RadScriptBlock>
<asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
    <table width="100%" dir="rtl">
        <tr>
            <td dir="rtl" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <%--            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>--%>
                            <td>
                                <cc1:customradbutton id="btnSearch" runat="server" customebuttontype="Search" causesvalidation="False"
                                    onclick="btnSearch_Click">
                                    <icon primaryiconcssclass="rbSearch" />
                                </cc1:customradbutton>
                            </td>
                            <td>
                                <cc1:customradbutton id="btnShowAll" runat="server" customebuttontype="ShowAll" causesvalidation="False"
                                    onclick="btnShowAll_Click">
                                    <icon primaryiconcssclass="rbRefresh" />
                                </cc1:customradbutton>
                            </td>
                            <%--                      <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>--%>
                            <td>
                                <cc1:customradbutton id="btnBack" runat="server" customebuttontype="Back" 
                                    onclientclicked="CloseRadWindow">
                                    <icon primaryiconcssclass="rbPrevious" />
                                </cc1:customradbutton>
                            </td>
                            <td>
                                <cc1:custommessageerrorcontrol id="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%" HorizontalAlign="Right">
                    <table width="100%">
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName" runat="server">نام<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtFirstName" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidPersonTitle" runat="server" ControlToValidate="txtFirstName"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblBirthDate" runat="server">تاریخ تولد<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:customitccalendar id="TxtBirthDate" runat="server" width="200px">
                                </cc1:customitccalendar>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidBirthDate" runat="server" ControlToValidate="TxtBirthDate"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName0" runat="server">نام خانوادگی فعلی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtLastName" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblDeathDate" runat="server">تاریخ فوت:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:customitccalendar id="TxtDeathDate" runat="server" width="100px">
                                </cc1:customitccalendar>
                            </td>
                            <td width="5%">
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName1" runat="server">نام خانوادگی قبلی:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtPreviousLastName" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateNo" runat="server">شماره شناسنامه<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtCertificateNo" runat="server" Width="50px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:CheckBox ID="ChkHasNationalNo" runat="server" AutoPostBack="True" Checked="True"
                                    OnCheckedChanged="ChkHasNationalNo_CheckedChanged" Text="آیا کد ملی دارد؟" />
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtNationalNo" runat="server" DisplayText="کد ملی" MaxLength="10"
                                    Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="Label1" runat="server"> استان محل تولد:<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <cc1:customradcombobox id="CmbBirthPlaceProvince" runat="server" appenddatabounditems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                                autopostback="True" onselectedindexchanged="CmbBirthPlaceProvince_SelectedIndexChanged"
                                                width="100px" causesvalidation="False">
                                            </cc1:customradcombobox>
                                        </td>
                                        <td align="left">
                                            <asp:Label CssClass="LabelCSS" ID="lblBirthPlaceCity" runat="server">شهر<font 
                                    color="red">*</font>:</asp:Label>
                                        </td>
                                        <td align="right">
                                            <cc1:customradcombobox id="CmbBirthPlaceCity" runat="server" width="100px" appenddatabounditems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                            </cc1:customradcombobox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidBirthPlaceProvince" runat="server" ControlToValidate="CmbBirthPlaceCity"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblGender" runat="server">جنسیت<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:customradcombobox id="CmbGender" runat="server" appenddatabounditems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    width="100px">
                                </cc1:customradcombobox>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidGender" runat="server" ControlToValidate="CmbGender"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblBirthPlaceSection" runat="server">بخش محل تولد<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtBirthPlaceSection" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblBloodGroup" runat="server">گروه خونی:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:customradcombobox id="CmbBloodGroup" runat="server" appenddatabounditems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    width="100px">
                                </cc1:customradcombobox>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidBloodGroup" runat="server" ControlToValidate="CmbBloodGroup"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblIssuePlaceProvince" runat="server"> استان محل صدور شناسنامه<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <cc1:customradcombobox id="CmbIssuePlaceProvince" runat="server" appenddatabounditems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                                autopostback="True" width="100px" causesvalidation="False" onselectedindexchanged="CmbIssuePlaceProvince_SelectedIndexChanged">
                                            </cc1:customradcombobox>
                                        </td>
                                        <td align="left">
                                            <asp:Label CssClass="LabelCSS" ID="lblIssuePlaceCity" runat="server"> شهر<font color="red">*</font>:</asp:Label>
                                        </td>
                                        <td>
                                            <cc1:customradcombobox id="CmbIssuePlaceCity" runat="server" width="100px" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                            </cc1:customradcombobox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidIssuePlaceProvince" runat="server" ControlToValidate="CmbIssuePlaceCity"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblEnglishFirstName" runat="server">نام به انگلیسی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtEnglishFirstName" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblIssuePlaceSection" runat="server"> بخش محل صدور شناسنامه<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtIssuePlaceSection" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblEnglishLastName" runat="server">نام خانوادگی به انگلیسی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtEnglishLastName" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblIssueDate" runat="server">تاریخ صدور شناسنامه<font 
                                     color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:customitccalendar id="TxtIssueDate" runat="server" width="100px">
                                </cc1:customitccalendar>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidIssueDate" runat="server" ControlToValidate="TxtIssueDate"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName2" runat="server">نام پدر<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtFatherName" runat="server" Width="100px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateAlphabet" runat="server">سری حرفی شناسنامه<font 
                                     color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%">
                                <cc1:customradcombobox id="CmbCertificateAlphabet" runat="server" appenddatabounditems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                    width="50px">
                                </cc1:customradcombobox>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="ValidCertificateAlphabet" runat="server" ControlToValidate="CmbCertificateAlphabet"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <label>
                                    وضیعت رکورد<font color="red">*</font>:</label>&nbsp;
                            </td>
                            <td width="35%">
                                <cc1:customradcombobox id="cmbIsActive" runat="server" width="100px">
                                    <items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </items>
                                </cc1:customradcombobox>
                            </td>
                            <td width="5%">
                                <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                    ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                            <td width="10%" nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateNumericSeries" runat="server">سریال عددی شناسنامه:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtCertificateNumericSeries" runat="server" Width="50px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                &nbsp;
                            </td>
                            <td width="35%">
                                &nbsp;
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                            <td width="10%">
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateSerialNo" runat="server">سریال شناسنامه:</asp:Label>
                            </td>
                            <td width="35%">
                                <telerik:RadTextBox ID="txtCertificateSerialNo" runat="server" Width="50px">
                                </telerik:RadTextBox>
                            </td>
                            <td width="5%">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdPerson" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                    AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdPerson_ItemCommand" OnPageIndexChanged="grdPerson_PagePersonChanged"
                                    OnPageSizeChanged="grdPerson_PageSizeChanged" OnSortCommand="grdPerson_SortCommand" AllowMultiRowSelection="True">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007"  >
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="PersonId" Dir="RTL" GroupsDefaultExpanded="False"
                                        ClientDataKeyNames="PersonId,FirstName,LastName" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام " SortExpression="FirstName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LastName" HeaderText="نام خانوادگی" SortExpression="LastName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="NationalNo" HeaderText="کد ملی" SortExpression="NationalNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FatherName" HeaderText="نام پدر" SortExpression="FatherName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="BirthDate" HeaderText="تاریخ تولد" SortExpression="BirthDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="BirthPlaceCityTitle" HeaderText="شهر محل تولد"
                                                SortExpression="BirthPlaceCityTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                                       <telerik:GridClientSelectColumn UniqueName="Select">
                        </telerik:GridClientSelectColumn>
                                         
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents OnRowSelected="RowSelected" OnRowDeselected="RowDeselected" OnMasterTableViewCreated="MasterTableViewCreated">
                                        </ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID="hiddenfieldKeyId" runat="server" />
<asp:HiddenField ID="hiddenfieldTitle" runat="server" />
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ChkHasNationalNo">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="txtNationalNo" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbBirthPlaceProvince">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="CmbBirthPlaceCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbIssuePlaceProvince">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="CmbIssuePlaceCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdPerson">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
