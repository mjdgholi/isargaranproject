﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ITC.Library.Classes;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BaseInformation
{
    public partial class DrivingRestrictionPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly DrivingRestrictionBL _drivingRestrictionBL = new DrivingRestrictionBL();
        private const string TableName = "Isar.t_DrivingRestriction";
        private const string PrimaryKey = "DrivingRestrictionId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDrivingRestrictionTitleEnglish.Text = "";
            txtDrivingRestrictionTitlePersian.Text = "";
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var DrivingRestrictionEntity = new DrivingRestrictionEntity();
            {
                DrivingRestrictionEntity.DrivingRestrictionId = new Guid(ViewState["DrivingRestrictionId"].ToString());
            }
            var myDrivingRestrictionBL = _drivingRestrictionBL.GetSingleById(DrivingRestrictionEntity);
            txtDrivingRestrictionTitleEnglish.Text = myDrivingRestrictionBL.DrivingRestrictionTitleEnglish;
            txtDrivingRestrictionTitlePersian.Text = myDrivingRestrictionBL.DrivingRestrictionTitlePersian;
            cmbIsActive.SelectedValue = myDrivingRestrictionBL.IsActive.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
                                      {
                                          TableName = TableName,
                                          PrimaryKey = PrimaryKey,
                                          RadGrid = grdDrivingRestriction,
                                          PageSize = pageSize,
                                          CurrentPage = currentpPage,
                                          WhereClause = ViewState["WhereClause"].ToString(),
                                          OrderBy = ViewState["SortExpression"].ToString(),
                                          SortType = ViewState["SortType"].ToString(),
                                          RowSelectGuidId = rowSelectGuidId
                                      };
            return gridParamEntity;
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid DrivingRestrictionId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var DrivingRestrictionEntity = new DrivingRestrictionEntity()
                                        {
                                            DrivingRestrictionTitleEnglish = FarsiToArabic.ToArabic(txtDrivingRestrictionTitleEnglish.Text.Trim()),
                                            DrivingRestrictionTitlePersian = FarsiToArabic.ToArabic(txtDrivingRestrictionTitlePersian.Text.Trim()),
                                            IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                                        };

                _drivingRestrictionBL.Add(DrivingRestrictionEntity, out DrivingRestrictionId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, DrivingRestrictionId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtDrivingRestrictionTitleEnglish.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DrivingRestrictionTitleEnglish Like N'%" +
                                               FarsiToArabic.ToArabic(txtDrivingRestrictionTitleEnglish.Text.Trim()) + "%'";
                if (txtDrivingRestrictionTitlePersian.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DrivingRestrictionTitlePersian Like N'%" +
                                               FarsiToArabic.ToArabic(txtDrivingRestrictionTitlePersian.Text.Trim()) + "%'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdDrivingRestriction.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDrivingRestriction.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "";
                var DrivingRestrictionEntity = new DrivingRestrictionEntity()
                                        {
                                            DrivingRestrictionId = new Guid(ViewState["DrivingRestrictionId"].ToString()),
                                            DrivingRestrictionTitleEnglish = FarsiToArabic.ToArabic(txtDrivingRestrictionTitleEnglish.Text.Trim()),
                                            DrivingRestrictionTitlePersian = FarsiToArabic.ToArabic(txtDrivingRestrictionTitlePersian.Text.Trim()),
                                            IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                                        };
                _drivingRestrictionBL.Update(DrivingRestrictionEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, new Guid(ViewState["DrivingRestrictionId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDrivingRestriction_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DrivingRestrictionId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var DrivingRestrictionEntity = new DrivingRestrictionEntity()
                                            {
                                                DrivingRestrictionId = new Guid(e.CommandArgument.ToString()),

                                            };
                    _drivingRestrictionBL.Delete(DrivingRestrictionEntity);
                    var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdDrivingRestriction_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDrivingRestriction_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdDrivingRestriction_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {

            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDrivingRestriction.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}