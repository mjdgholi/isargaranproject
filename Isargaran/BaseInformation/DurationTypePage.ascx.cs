﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/05/02>
    /// Description: <نوع مدت>
    /// </summary>
    public partial class DurationTypePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly DurationTypeBL _durationTypeBL = new DurationTypeBL();

        private const string TableName = "Isar.t_DurationType";
        private const string PrimaryKey = "DurationTypeId";

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;

            Panel1.DefaultButton = "btnSave";
            btnSave.Focus();
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            Panel1.DefaultButton = "btnEdit";
            btnEdit.Focus();
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDurationTypeTitle.Text = "";
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var durationTypeEntity = new DurationTypeEntity()
            {
                DurationTypeId = new Guid(ViewState["DurationTypeId"].ToString())
            };
            var mydurationType = _durationTypeBL.GetSingleById(durationTypeEntity);
            txtDurationTypeTitle.Text = mydurationType.DurationTypeTitle.ToString();
            cmbIsActive.SelectedValue = mydurationType.IsActive.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdDurationType,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    try
            //    {
            //        ViewState["WhereClause"] = "  ";
            //        ViewState["SortExpression"] = " ";
            //        ViewState["SortType"] = "Asc";
            //        var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
            //        DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            //        SetPanelFirst();
            //        SetClearToolBox();
            //    }
            //    catch (Exception ex)
            //    {

            //        CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            //    }
            //}
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid durationTypeId;
                ViewState["WhereClause"] = "";


                var durationTypeEntity = new DurationTypeEntity()
                {                    
                    DurationTypeTitle = FarsiToArabic.ToArabic(txtDurationTypeTitle.Text.Trim()),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                };

                _durationTypeBL.Add(durationTypeEntity, out durationTypeId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, durationTypeId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtDurationTypeTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DurationTypeTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtDurationTypeTitle.Text.Trim()) + "%'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdDurationType.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdDurationType.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "";
                var durationTypeEntity = new DurationTypeEntity()
                {
                    DurationTypeId = Guid.Parse(ViewState["DurationTypeId"].ToString()),
                    DurationTypeTitle = FarsiToArabic.ToArabic(txtDurationTypeTitle.Text.Trim()),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                };
                _durationTypeBL.Update(durationTypeEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, new Guid(ViewState["DurationTypeId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdDurationType_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["DurationTypeId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var durationTypeEntity = new DurationTypeEntity()
                    {
                        DurationTypeId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _durationTypeBL.Delete(durationTypeEntity);
                    var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdDurationType_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdDurationType_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdDurationType_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdDurationType.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}