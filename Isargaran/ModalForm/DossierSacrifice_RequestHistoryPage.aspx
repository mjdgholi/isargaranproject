﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DossierSacrifice_RequestHistoryPage.aspx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.ModalForm.DossierSacrifice_RequestHistoryPage" %>

<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager Runat="server"></telerik:RadScriptManager>
        <asp:Panel ID="Panel1" runat="server" BackColor="#F0F8FF">
            <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
                <tr>
                    <td dir="rtl" style="width: 100%;" valign="top">
                        <asp:Panel ID="pnlButton" runat="server">
                            <table>
                                <tr>

                                    <td>
                                        <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                            OnClick="btnSearch_Click">
                                            <Icon PrimaryIconCssClass="rbSearch" />
                                        </cc1:CustomRadButton>
                                    </td>
                                    <td>
                                        <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                            OnClick="btnShowAll_Click">
                                            <Icon PrimaryIconCssClass="rbRefresh" />
                                        </cc1:CustomRadButton>
                                    </td>

                                    <td>
                                        <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        <asp:Panel ID="pnlDetail" runat="server" HorizontalAlign="Right" Width="100%">
                            <table width="100%">




                                                                                                                                         <tr>
                            <td width="10%" >
                                <asp:Label CssClass="LabelCSS" ID="lblStatus" runat="server">نوع وضیعت درخواست:</asp:Label>
                            </td>
                            <td  width="20%">
                                <cc1:CustomRadComboBox ID="cmbStatus" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td width="70%">
                                    
                            </td>
                        </tr>
                                                                                 <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblActionTypeTitle" runat="server">نوع اقدام درخواست:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbActionType" runat="server" AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                       
                            </td>
                        </tr>
                                                                                     <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblActionDate" runat="server">تاریخ اقدام:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar runat="server" ID="txtActionDate"/>
                            </td>
                            <td>
                       
                            </td>
                        </tr>
                                            <tr>
                            <td>
                                <asp:Label CssClass="LabelCSS" ID="lblActionDescription" runat="server">متن اقدام:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtActionDescription" runat="server" Width="300" Height="50" TextMode="MultiLine" ></asp:TextBox>
                            </td>
                            <td>
                               
                            </td>
                        </tr>

                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td valign="top" bgcolor="White">
                        <asp:Panel ID="pnlGrid" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                                 <telerik:RadGrid ID="grddossierSacrifice_RequestHistoryHistory" 
                                    runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grddossierSacrifice_RequestHistoryHistory_ItemCommand" OnPageIndexChanged="grddossierSacrifice_RequestHistoryHistory_PageIndexChanged" OnPageSizeChanged="grddossierSacrifice_RequestHistoryHistory_PageSizeChanged" OnSortCommand="grddossierSacrifice_RequestHistoryHistory_SortCommand" >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="DossierSacrifice_RequestHistoryId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="DossierSacrificeNo" HeaderText="شماره پرونده "
                                                SortExpression="DossierSacrificeNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
           
                                            <telerik:GridBoundColumn DataField="StatusTitle" HeaderText="عنوان وضیعت"
                                                SortExpression="StatusTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ActionTypeTitle" HeaderText="عنوان نوع اقدام" SortExpression="ActionTypeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ActionDate" HeaderText="تاریخ اقدام" SortExpression="ActionDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                            
                                              <telerik:GridTemplateColumn HeaderText="متن اقدام" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="ActionDescription">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server"  Text='<%#Eval("ActionDescription").ToString() %>'
                                                        TextMode="MultiLine" BackColor="#FFFFCC"></asp:TextBox>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
          
                                                     <telerik:GridTemplateColumn HeaderText="مشاهده اقدام" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="MyAction">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButtonShowRequest" runat="server" CausesValidation="false" CommandArgument='<%#Eval("DossierSacrifice_RequestHistoryId").ToString() %>'
                                                        CommandName="MyAction" ForeColor="#000066" ImageUrl="../Images/ShowMessage.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <telerik:RadAjaxManager ID="RadAjaxManagerProxy1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSearch">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                        <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                        <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnShowAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                        <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                        <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="grddossierSacrifice_RequestHistoryHistory">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                        <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                        <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
        </telerik:RadAjaxLoadingPanel>
    </div>
    </form>
    <p style="direction: ltr">
        &nbsp;</p>
</body>
</html>
