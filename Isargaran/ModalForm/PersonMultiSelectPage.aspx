﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonMultiSelectPage.aspx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.ModalForm.PersonMultiSelectPage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register src="../BaseInformation/PersonMultiSelectPage.ascx" tagname="PersonMultiSelectPage" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
        </telerik:RadScriptManager>

    </div>
    <uc1:PersonMultiSelectPage ID="PersonMultiSelectPage1" runat="server" />
    </form>
</body>
</html>
