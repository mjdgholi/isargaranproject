﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Services;

using Intranet.Configuration.Settings;


namespace Intranet.DesktopModules.Tenders
{
    public partial class FrmUpload : PortalPage
    {
        private string _strPath;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //_strPath = Request["WhereClause"];
                lblStatus.ToolTip = "";
                lblStatus.Text = Request["WhereClause"];
                //lblStatus.Text = Session["path"].ToString();
                //Response.Write(@Request["WhereClause"].ToString());
            }

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (RadUpload1.UploadedFiles.Count > 0)
            {
                if (CheckExtention())
                {
                    var fileName = Guid.NewGuid().ToString().Substring(0, 5) + "__" + DateTime.Now.Year +
                                   DateTime.Now.Month + DateTime.Now.Day + "__" + RadUpload1.UploadedFiles[0].GetName().Replace(" ","");
                    _strPath = Path.Combine(Session["path"].ToString(), fileName);
                    RadUpload1.UploadedFiles[0].SaveAs(Path.Combine(Session["path"].ToString(), fileName));

                    lblStatus.Text = File.Exists(_strPath) ? "فایل بالاگذاری گردید" : "خطا در بالاگذاری";
                    lblStatus.ToolTip = File.Exists(_strPath) ? fileName : "";
                }
                else
                {
                    lblStatus.Text = "فرمت فایل :" + Request["WhereClause"];
                    lblStatus.ForeColor = Color.DarkOrange;
                }
            }
            else
            {
                lblStatus.Text = "فایل را انتخاب نمایید";
            }
        }

        protected bool CheckExtention()
        {
            string[] myExt = Request["WhereClause"].Split(',');

            foreach (string ext in myExt)
            {
                if (ext.ToLower() == RadUpload1.UploadedFiles[0].GetExtension().Replace(".", "").ToLower())
                {
                    return true;
                }
            }
            return false;
        }
    }
}