﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmUpload.aspx.cs" Inherits="Intranet.DesktopModules.Tenders.FrmUpload" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #btnSubmit
        {
            height: 26px;
        }
    </style>
</head>
<body>
    <script language="javascript" type="text/javascript">
        function btnSubmit_onclick() {           
            var filename = document.getElementById('lblStatus').title;            
            var oArg = new Object();
            if (filename == '') {
                oArg.Title = '';
                oArg.KeyId = '';
            }
            else {
                oArg.Title = 'فایل بالا گذاری گردید';
                oArg.KeyId = filename;
            }


            var oWnd = GetRadWindow();
            oWnd.close(oArg);
        }


        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>
    <form id="form1" runat="server">
    <div>
        <center>
            <table bgcolor="#DFEAEA" width="70%" cellpadding="0" cellspacing="0">
                <tr>
                    <td dir="rtl" height="120" style="background-image: url('Images/BackGround2.jpg'); background-repeat: repeat-x;"
                        align="center">
                        <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None"
                            InputSize="20" Width="50%" OverwriteExistingFiles="True"  
                            Height="25px">
                        </telerik:RadUpload>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#049574" dir="rtl" 
                        style="background-image: url('Images/BackGround2.jpg'); background-repeat: repeat-x;">
                        &nbsp;<asp:Button ID="btnUpload" runat="server" Height="25px" OnClick="btnUpload_Click"
                            Text="بالاگذاری" Width="70px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label CssClass="LabelCSS" ID="lblStatus" runat="server" Font-Bold="True" ForeColor="#E8F3FF"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="btnSubmit" type="button" value="بـازگــشـت" onclick="return btnSubmit_onclick()" />
                    </td>
                </tr>
            </table>
        </center>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    </form>
</body>
</html>
