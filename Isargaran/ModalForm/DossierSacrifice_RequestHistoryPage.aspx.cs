﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.IsargaranProject.Isargaran.BL;
using Intranet.DesktopModules.IsargaranProject.Isargaran.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran.ModalForm
{
    public partial class DossierSacrifice_RequestHistoryPage : PortalPage
    {
        #region PublicParam:
        private readonly DossierSacrifice_RequestHistoryBL _dossierSacrificeRequestHistoryBL = new DossierSacrifice_RequestHistoryBL();
        private readonly StatusBL _statusBL = new StatusBL();
        private readonly ActionTypeBL _actionTypeBL = new ActionTypeBL();
        private const string TableName = "[Isar].[V_DossierSacrifice_RequestHistory]";
        private const string PrimaryKey = "DossierSacrifice_RequestHistoryId";

        #endregion

        #region Procedure:

        

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtActionDate.Text = "";
            txtActionDescription.Text = "";
            cmbActionType.ClearSelection();
            cmbStatus.ClearSelection();


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var dossierSacrificeRequestHistoryEntity = new DossierSacrifice_RequestHistoryEntity()
            {
                DossierSacrifice_RequestHistoryId = Guid.Parse(ViewState["DossierSacrifice_RequestHistoryId"].ToString())
            };
            var mydossierSacrificeRequestHistory = _dossierSacrificeRequestHistoryBL.GetSingleById(dossierSacrificeRequestHistoryEntity);
            cmbActionType.SelectedValue = mydossierSacrificeRequestHistory.ActionTypeId.ToString();
            if (cmbActionType.FindItemByValue(mydossierSacrificeRequestHistory.ActionTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع اقدام انتخاب شده معتبر نمی باشد.");
            }
            cmbStatus.SelectedValue = mydossierSacrificeRequestHistory.StatusId.ToString();
            if (cmbStatus.FindItemByValue(mydossierSacrificeRequestHistory.StatusId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع وضیعت درخواست انتخاب شده معتبر نمی باشد.");
            }


            txtActionDate.Text = mydossierSacrificeRequestHistory.ActionDate;
            txtActionDescription.Text = mydossierSacrificeRequestHistory.ActionDescription;


        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grddossierSacrifice_RequestHistoryHistory,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {


            cmbActionType.Items.Clear();
            cmbActionType.Items.Add(new RadComboBoxItem(""));
            cmbActionType.DataSource = _actionTypeBL.GetAllIsActive();
            cmbActionType.DataTextField = "ActionTypeTitle";
            cmbActionType.DataValueField = "ActionTypeId";
            cmbActionType.DataBind();

            cmbStatus.Items.Clear();
            cmbStatus.Items.Add(new RadComboBoxItem(""));
            cmbStatus.DataSource = _statusBL.GetAllIsActive();
            cmbStatus.DataTextField = "StatusTitle";
            cmbStatus.DataValueField = "StatusId";
            cmbStatus.DataBind();

        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Request["WhereClause"].ToString()) + "'";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grddossierSacrifice_RequestHistoryHistory.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);                    
                    SetClearToolBox();
                    SetComboBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Request["WhereClause"].ToString()) + "'";

                if (txtActionDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ActionDescription Like N'%" +
                                               FarsiToArabic.ToArabic(txtActionDescription.Text.Trim()) + "%'";
                if (cmbStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And StatusId='" +
                                               (cmbStatus.SelectedValue.Trim()) + "'";
                if (cmbActionType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ActionTypeId='" +
                                               (cmbActionType.SelectedValue.Trim()) + "'";
                if (txtActionDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ActionDate ='" +
                                                                   txtActionDate.Text + "'";

                grddossierSacrifice_RequestHistoryHistory.MasterTableView.CurrentPageIndex = 0;                
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grddossierSacrifice_RequestHistoryHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grddossierSacrifice_RequestHistoryHistory.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {                
                SetClearToolBox();
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Request["WhereClause"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grddossierSacrifice_RequestHistoryHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void grddossierSacrifice_RequestHistoryHistory_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "MyAction")
                {
                    ViewState["DossierSacrifice_RequestHistoryId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                
                }

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grddossierSacrifice_RequestHistoryHistory_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Request["WhereClause"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grddossierSacrifice_RequestHistoryHistory.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grddossierSacrifice_RequestHistoryHistory_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Request["WhereClause"].ToString()) + "'";
                var gridParamEntity = SetGridParam(grddossierSacrifice_RequestHistoryHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grddossierSacrifice_RequestHistoryHistory_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = " DossierSacrifice_RequestId='" + Guid.Parse(Request["WhereClause"].ToString()) + "'";
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grddossierSacrifice_RequestHistoryHistory.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

    }
}