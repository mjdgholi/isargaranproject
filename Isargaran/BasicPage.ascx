﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicPage.ascx.cs" Inherits="Intranet.DesktopModules.IsargaranProject.Isargaran.BasicPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<style type="text/css">
    .style1
    {
        color: #CC0000;
        font-family: Tahoma;
        font-size: 12pt;       
    }
    .style2
    {
        text-decoration: none;
    }
</style>
<telerik:RadScriptBlock runat="server">
    <link href='<%=Intranet.Configuration.Settings.PortalSettings.PortalPath %>/DeskTopModules/IsargaranProject/Isargaran/Style/RowRadGridView.css'
        rel="stylesheet" />
</telerik:RadScriptBlock>
<table dir="rtl" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="30%" class="HeaderOfHomePage">
        </td>
        <td width="40%" align="center" class="HeaderOfHomePage" height="30">
            <asp:Label CssClass="LabelCSS" ID="lblSubControlTitle" runat="server" Font-Names="B Titr"
                Font-Size="12pt" ForeColor="White">سامانه ایثارگران</asp:Label>
        </td>
        <td align="left" width="30%" class="HeaderOfHomePage" style="font-weight: 700; color: #FF0000">
            <a href="Images/isarhelp.pdf" class="style2"><span class="style1">دریافت راهنمای سامانه ایثارگران</span></a>&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
</table>
<asp:Panel ID="pnlMessageErorr" runat="server" Visible="False" BackColor="White">
    <table>
        <tr>
            <td>
                <asp:Image ID="imgMessage" runat="server" ImageUrl="Images\Error.png" />
            </td>
            <td>
                <asp:Label ID="lblMessageError" runat="server" Text="Label" Font-Bold="True" Font-Size="10"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="1600px"
    CssClass="MainTableOfASCX">
    <telerik:RadPane ID="RadPane1" runat="server" Width="22px" Scrolling="None">
        <telerik:RadSlidingZone ID="RadSlidingZone1" runat="server" Width="22px" DockedPaneId="RadSlidingPane1"
            ExpandedPaneId="RadSlidingPane1" ClickToOpen="True">
            <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server" Title="مـنـوی اصلی" Width="250px"
                DockOnOpen="True" Font-Names="Tahoma">
                <cc1:CustomItcMenuWithRowAccess ID="CustomItcMenuWithRowAccess1" runat="server" MenuTableName="[pub].[t_PubItcMenu]"
                    MenuImageUrl="Isargaran\Images" OnItemClick="CustomItcMenu1_ItemClick" CausesValidation="False"
                    ExpandMode="SingleExpandedItem" AllowCollapseAllItems="True">
                </cc1:CustomItcMenuWithRowAccess>
                <%--                 <cc1:CustomItcMenu ID="ItcMenu" runat="server" MenuTableName="[pub].[t_PubItcMenu]"
                    MenuImageUrl="Training\Images" OnItemClick="CustomItcMenu1_ItemClick" CausesValidation="False"
                    ExpandMode="SingleExpandedItem" AllowCollapseAllItems="True" Width="100%" >
                    <ExpandAnimation Duration="600" Type="InCubic" />
                    <CollapseAnimation Duration="600" Type="Linear" />
                </cc1:CustomItcMenu>--%>
            </telerik:RadSlidingPane>
        </telerik:RadSlidingZone>
    </telerik:RadPane>
    <telerik:RadPane ID="RadPane2" runat="server" Height="1600px" Scrolling="None">
        <telerik:RadMultiPage ID="radmultipageMenu" runat="server" Width="100%" OnPageViewCreated="radmultipageMenu_PageViewCreated"
            Height="1600px">
        </telerik:RadMultiPage>
    </telerik:RadPane>
</telerik:RadSplitter>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="CustomItcMenuWithRowAccess1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="CustomItcMenuWithRowAccess1" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
                <telerik:AjaxUpdatedControl ControlID="pnlMessageErorr" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radmultipageMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="CustomItcMenuWithRowAccess1" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
<cc1:ResourceJavaScript ID="ResourceJavaScript1" runat="server" />
