﻿using System;
using Intranet.Configuration.Settings;

namespace Intranet.DesktopModules.IsargaranProject.Isargaran
{
    public partial class LoadPage : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=" + ModuleConfiguration.ModuleID + "&page=IsargaranProject/Isargaran/BasicPage"));
        }
    }
}